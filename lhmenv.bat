@setlocal enabledelayedexpansion
@echo off
@CALL :normalizepath scripts_path "%~dp0"
@set global_path="%scripts_path%lhmenv.txt"

:: read the active environment name
@set /p CONDA_NEW_ENV=<%global_path%

@set CONDA_SKIPCHECK=1
@set "CONDA_PREFIX=%CONDA_NEW_ENV%"

:: change directory if started from shortcut
set shortcut=0
for %%x in (%cmdcmdline%) do if /i "%%~x"=="/c" set shortcut=1

@SET "activate_path="%scripts_path%activate.bat" "%CONDA_NEW_ENV%""

:: pass activate path through endlocal barrier
@for /f "delims=" %%A in (""!activate_path!"") do (
    if %shortcut% == 0 (
        @endlocal & @set "CONDA_SKIPCHECK=1" & call %%~A
    ) else (
        @endlocal & @set "CONDA_SKIPCHECK=1" & @cd /d %CONDA_PREFIX% & cmd.exe /k "%%~A"
    )
)

@goto :eof

:normalizepath
    @SET "%1=%~dpfn2"
    @EXIT /B
