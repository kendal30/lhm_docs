Installation
=============
Prior to installation, check to see that you have installed all of the LHM
frontend dependencies (listed below).

1. Clone the git repository using: ``git@gitlab.msu.edu:hydrogeology/lhm.git``
   A directory where you organize you code is recommended as a location for cloning
   LHM.

   - This is a secret repository so you must have access to the Hydrogeology
     group.

2. Now, in a command prompt with Python on the path (for instance the Anaconda
   command prompt), navigate to the root of your cloned repository
   and type::

      python setup.py

3. This will prompt you for a setting for the ``lhmHome`` variable, point it to
   a folder path where you would like for LHM to be installed (for instance
   ``F:/Users/Anthony/Modeling/LHM``)

   - This path need not already exist


Frontend Dependencies
---------------------
The setup routine will also check to make sure that certain dependencies are
satisfied, which include:

- Python versions 2.7+ or 3.6+
- MATLAB version 2016b+
- ArcGIS with the arcpy module
- Jupyter Notebook with the following extensions:
  - Freeze

Upgrade
=============
From the command prompt, run::

   python upgrade.py
