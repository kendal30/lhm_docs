%--------------------------------------------------------------------------
%Specify these for climate change and land use change scenarios
%--------------------------------------------------------------------------
%Specify whether to use a climate or landuse scenario to generate LAI
use.climateScenario = true;

%Specify the name of the climate file to use for the climate or land use scenarios
scenarioClim = 'CLIM_EBCW-LPMI-30yr_1986-2015_20180424.h5';
calibrationClim = 'CLIM_EBCW-LPMI-submodel-nldas_2000-2014_20150604.h5'; %The period of data here will be used to calibrate the LAI model

%Specify whether this will be a land use replacement scenario
use.replaceLu = false;

%--------------------------------------------------------------------------
%Must change every run
%--------------------------------------------------------------------------
%Output landscape file
landscapeName = 'EBCW-1986-2015'; %don't use underbars in the name
landscapeVersion = 'LP-submodel-3';
IDType = 'LAND';
compileDate = now;

%Specify whether to execute parts of this
process.loadLai = false;
process.loadLu = true;
process.processLai = false;
process.downscaleLai = false; %must be true for replaceLu to be used
process.calibrateLaiModel = true; %only need to do this once for your climate/landuse scenarios
process.writeLand = false;

%Specify whether irrigation should be used
use.irrig = false; %currently this doesn't work with the LAI model

%Specify model time period
startDate = '1/1/2000';
endDate = '12/31/2014';

%Specify the base LHM prep directory
baseDir = 'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model';

%Specify submodel name
nameSubmodel = 'EBCW'; %set to false if not a submodel

%--------------------------------------------------------------------------
%Specified common inputs -- shouldn't need to change these
%--------------------------------------------------------------------------
%Names of working files
files.cleaned = 'cleaned_lai.h5';
files.downscaled = 'downscaled_lai.h5';
files.urbfix = 'urban_fix_lai.h5';
files.filled = 'filled_lai.h5';

%Climate change files
files.laimodel = 'lai_model_params.mat';
files.climScenario = 'lai_climate_change.h5';

%--------------------------------------------------------------------------
%Calculated common inputs -- shouldn't need to change these
%--------------------------------------------------------------------------
modelHours = (datenum(startDate):1/24:datenum(endDate)+23/24)';

%Standardized directories
workingDir = [baseDir,filesep,'Import_Working'];
laiDir = [baseDir,filesep,'LAI_Clipped'];
luDirBase = [baseDir,filesep,'LU_Clipped_Reclass'];

%Determine if working with newer subdirectories of luDir
testDirs = {'Landuse','Impervious','Flowtimes','Irrigation_limits','Irrigation_technology'};
useDirs = cell(5,1);
for m = 1:length(testDirs)
    thisDir = testDirs{m};
    %If the directory exists, use that, otherwise use the parent directory
    if exist([luDirBase,filesep,thisDir],'dir')>0
        useDirs{m} = [luDirBase,filesep,thisDir];
    else
        useDirs{m} = inBaseDir;
    end
end
[luDir,impervDir,flowtimesDir,limDir,techDir] = useDirs{:};

%Test if using a submodel
if islogical(nameSubmodel)
    layersDir = [baseDir,filesep,'Model_Layers',filesep,'Surface'];
else
    layersDir = [baseDir,filesep,'Submodel_Layers',filesep,nameSubmodel,filesep,'Surface'];
end
%%
%--------------------------------------------------------------------------
%Specify data information -- will need to change inputs
%--------------------------------------------------------------------------
%Specify the grids array
grids.model = [layersDir,filesep,'model_grid.img'];
grids.modis.inDir = laiDir;
grids.modis.clip = [laiDir,filesep,'2000',filesep,'MOD15A2.A2000049.Lai_1km.img']; %CHANGE THIS: enter one of the LAI grid names
grids.latitude = [layersDir,filesep,'latitude.tif'];
grids.longitude = [layersDir,filesep,'longitude.tif'];
grids.elevation = [layersDir,filesep,'elevation_mean.tif'];

grids.lu.model = grids.model; %copy here for use in lu_load
grids.lu.lu.inDir = luDir;
grids.lu.lu.grids = {'lhm_reclass_nlcd2006_landcover_4-20-11_se5.tif'}; %CHANGE THIS: comma-separated list
grids.lu.lu.start = {'1/1/2000 00:00:00'}; %CHANGE THIS: comma separated list, each a separate string
grids.lu.lu.end = {'12/31/2014 23:00:00'}; %CHANGE THIS: like above
grids.lu.lu.map = struct('integer',{'1','2','3','4','5','6','7','8','9','10'},'name',...
    {'urban','agriculture','grassland','deciduous','water','wetland','barren','coniferous','shrubland','woody_wetland'},...
    'interp',{true,true,true,true,false,true,true,true,true,true});

grids.lu.flowtimes.inDir = flowtimesDir;
grids.lu.flowtimes.grids = {'flowtime_mannings_nlcd2006_landcover_4-20-11_se5.tif'};  %CHANGE THIS: comma-separated list
grids.lu.flowtimes.start = {'1/1/2000 00:00:00'}; %CHANGE THIS: comma separated list, each a separate string
grids.lu.flowtimes.end = {'12/31/2014 23:00:00'}; %CHANGE THIS: like above

grids.lu.imperv.use = true;
grids.lu.imperv.inDir = impervDir;
grids.lu.imperv.grids.percent = {'impervpercent_nlcd2006_impervious_5-4-11.img'}; %CHANGE THIS: like LU
grids.lu.imperv.grids.type = {'impervtype_nlcd2006_impervious_5-4-11.img'}; %CHANGE THIS: matches the impervious grids
grids.lu.imperv.start = {'1/1/2000 00:00:00'}; %CHANGE THIS: as above
grids.lu.imperv.end = {'12/31/2014 23:00:00'}; %CHANGE THIS: as above
grids.lu.imperv.map = struct('integer',{'1','2','3'},'name',{'upland','wetland','runoff'});

if use.replaceLu
    grids.luReplace.model = grids.model;
    grids.luReplace.lu.inDir = luDir;
    grids.luReplace.lu.grids = {'my_2001_landuse.tif','NLCD06_CHP_ARM_B30km_ILHMreclass_reproj.tif'}; %CHANGE THIS: comma-separated list
    grids.luReplace.lu.start = {'1/1/2001 00:00:00','1/1/2009 00:00:00'}; %CHANGE THIS: comma separated list, each a separate string
    grids.luReplace.lu.end = {'12/31/2008 23:00:00','8/31/2014 23:00:00'}; %CHANGE THIS: like abo
    grids.luReplace.lu.map = grids.lu.lu.map;

    grids.luReplace.imperv.use = false; %for LU change scenarios, just set impervious layer to be the changed scenarios

    grids.luReplace.irrig.use = false; %no irrigation files here
end

grids.lu.irrig.use = use.irrig;
if use.irrig
    grids.lu.irrig.inDir.tech = techDir;
    grids.lu.irrig.inDir.limit = limDir;
    grids.lu.irrig.grids.tech = {'clip_irr_fields_single_tech_92.shp.tif'}; %CHANGE THIS: comma-separated list
    grids.lu.irrig.grids.limit = {'clip_CHP_uniform_limitation.shp.tif'}; %CHANGE THIS:, comma-separated list
    grids.lu.irrig.start.tech = {'1/1/2000 00:00:00'}; %CHANGE THIS: matches the tech grids
    grids.lu.irrig.end.tech = {'8/31/2014 23:00:00'}; %CHANGE THIS: likewise
    grids.lu.irrig.start.limit = {'1/1/2000 00:00:00'}; %CHANGE THIS: matches the limit grids
    grids.lu.irrig.end.limit = {'8/31/2014 23:00:00'}; %CHANGE THIS: likewise
    grids.lu.irrig.map = struct('integer',{'1','2','3','4','5','6','7','8'},'name',...
        {'high_pressure_spray','low_pressure_med_elev_spray',...
        'low_pressure_in_canopy_spray','low_pressure_low_elev_spray','lepa','subsurface_drip','ditch/furrow','flood'});
end

%Specify the sources array
sources.modis.file = 'modis.h5';
sources.modis.lai = struct('name','lai','class','uint8','units','1','scale','10','offset','0','nan','255');
sources.lu.file = 'lu.h5';
sources.lu.lu = struct('name','landuse','class','uint16','units','1','scale','1000','offset','0','nan','65535');
sources.lu.flowtimes = struct('name','flowtimes','class','uint32','units','s','scale','1','offset','0','nan','4294967295');
sources.lu.imperv = struct('name','impervious','class','uint16','units','1','scale','1000','offset','0','nan','65535');
sources.lu.irrig.lim = struct('name','irrigation_limits','class','uint32','units','m/yr','scale','10000','offset','0','nan','4294967295');
sources.lu.irrig.tech = struct('name','irrigation_technology','class','uint16','units','1','scale','1000','offset','0','nan','65535');
if use.replaceLu
    sources.luReplace.file = 'lu_replace.h5';
    sources.luReplace.lu = sources.lu.lu;
end
%% Module parameters
%--------------------------------------------------------------------------
%qa_qc_landscape
%--------------------------------------------------------------------------
qaqc.lai = sources.modis.lai;
qaqc.lai.file = sources.modis.file;
qaqc.lai.scaleTrans = false;
qaqc.lai.method{1}.name = 'diff_outlier';
%Specify automated method thresholds, this is the percentage difference
%between neighboring values allowed before a value is removed
qaqc.lai.method{1}.threshold = 8;
qaqc.lai.method{1}.min = 0;
qaqc.lai.method{1}.fill = true;
qaqc.lai.method{1}.remove = ''; %dates to manually remove
qaqc.lai.removeNaNCols = false; %keep columns that are all NaNs

%--------------------------------------------------------------------------
%lai_ndvi_downscaling
%--------------------------------------------------------------------------
downscale.modelLuFile = sources.lu.file;
downscale.datatypes = {'lai'};

downscale.lai.data = {files.cleaned};
downscale.lai.name.in = {sources.modis.lai.name};
downscale.lai.name.out = {sources.modis.lai.name};
downscale.lai.grid = {grids.modis.clip};
downscale.lai.threshold = {[0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75]};

if use.replaceLu
    downscale.modelLuRepFile = sources.luReplace.file;
end

%--------------------------------------------------------------------------
%urban_fix
%--------------------------------------------------------------------------
if process.downscaleLai
    urbfix.lai.data = files.downscaled;
else
    urbfix.lai.data = files.cleaned;
end
urbfix.lai.name = sources.modis.lai.name;
urbfix.lai.class = sources.modis.lai.class;

urbfix.lu.data = sources.lu.file;
urbfix.lu.name = sources.lu.lu.name;
urbfix.lu.urban = 1;
urbfix.lu.water = 5;
urbfix.imperv.data = sources.lu.file;
urbfix.imperv.name = sources.lu.imperv.name;

urbfix.threshold.impervMin = 0.2; %Minimum impervious level to use for filling urban areas
urbfix.threshold.laiBadMax = 0.2; %Maximum mean LAI value indicating incorrect urban LAI areas
urbfix.threshold.waterMax = 0.7; %Maximum water fraction in a cell for correction to be applied
urbfix.threshold.urbanMin = 0.5; %Minimum urban fraction in a cell to use for prototype points

%--------------------------------------------------------------------------
%julian_fill_landscape
%--------------------------------------------------------------------------
julian.lai.data = files.urbfix;
julian.lai.name = sources.modis.lai.name;
julian.lai.class = sources.modis.lai.class;

julian.averageDays = (1:8:366); %julian days to calculate LAI

%--------------------------------------------------------------------------
%irrigation_lai
%--------------------------------------------------------------------------
if use.irrig
    irrigLai.lai.data = files.filled;
    irrigLai.lai.name = sources.modis.lai.name;
    irrigLai.lai.class = sources.modis.lai.class;
    irrigLai.irrigTech.data = sources.lu.file;
    irrigLai.irrigTech.name = sources.lu.irrig.tech.name;
    irrigLai.lu.data = sources.lu.file;
    irrigLai.lu.name = sources.lu.lu.name;
    irrigLai.lu.include = [2,3]; %only use ag and grassland for now

    irrigLai.threshold.irrigMin = 0.75;
    irrigLai.threshold.nonIrrigAgMin = 0.75;
end

%--------------------------------------------------------------------------
%lai_model_calibrate
%--------------------------------------------------------------------------
if use.climateScenario
    laiModel.lai.data = files.urbfix;
    laiModel.lu.data = sources.lu.file;
    laiModel.clim.data = calibrationClim;

    %Initial parameter values [gddMature,riseExp,riseSlope,riseLinSlope,fallExp,fallSlope,T_GDD,T_SDD,ppFactor]
    laiModel.params.x0 = [1e3,0.002,1e3,0.002,1e3,-2,15,0.5];
    laiModel.params.lb = [0,0,0,0,0,-5,-5,0];
    laiModel.params.ub = [1e4,1,1e4,1,1e4,30,30,1];
    laiModel.params.scaleParams = [1e-3,1e3, 1e-3, 1e3, 1e-2,1, 1,1];

    %Specify which LU types to optimize values for, otherwise will be 0
    laiModel.params.luOptim = {'urban','agriculture','grassland','deciduous','wetland',...
    'barren','coniferous','shrubland','woody_wetland'};
    laiModel.params.luWeightSheet = [1,2,3,4,6,7,8,9,10];

    %initial and minimum threshold weights for each LU prototype cell
    laiModel.params.threshInit = 0.95;
    laiModel.params.threshMin = 0.5;

    %define the rise and fall periods in the LAI curves
    laiModel.params.risePeriod = [50,180]; %in DOY
    laiModel.params.fallPeriod = [225,325]; %in DOY
end

%--------------------------------------------------------------------------
%lai_climate_change
%--------------------------------------------------------------------------
if use.climateScenario
    climScenario.laiModel = files.laimodel;
    climScenario.laiDays = (1:4:365); %julian days to calculate LAI

    climScenario.lai = sources.modis.lai;
    climScenario.lu.data = sources.lu.file;
    climScenario.lu.name = sources.lu.lu.name;
    climScenario.lu.weightNames = {'urban','agriculture','grassland','deciduous','wetland',...
    'baren','coniferous','shrubland','woody_wetland'};
    climScenario.lu.weightSheets = [1,2,3,4,6,7,8,9,10];

    climScenario.airTemp.data = scenarioClim;
    climScenario.airTemp.name = 'airTemp';
end
%--------------------------------------------------------------------------
%landscape_hdf_write
%--------------------------------------------------------------------------
%Specify data sources
luHDF.lu.data           = sources.lu.file;
luHDF.lu.name           = sources.lu.lu.name;
luHDF.lu.weightNames    = {grids.lu.lu.map(:).integer};
luHDF.flowtimes.data    = sources.lu.file;
luHDF.flowtimes.name    = sources.lu.flowtimes.name;
luHDF.imperv.data       = sources.lu.file;
luHDF.imperv.name       = sources.lu.imperv.name;
luHDF.imperv.weightNames = {grids.lu.imperv.map(:).integer};

if use.climateScenario
    luHDF.lai.name          = sources.modis.lai.name;
else
    luHDF.lai.data          = files.filled;
    luHDF.lai.name          = sources.modis.lai.name;
end

if use.irrig
    luHDF.laiIrrig.data     = files.filled;
    luHDF.laiIrrig.name     = luHDF.lai.name;
    luHDF.irrigTech.data    = sources.lu.file;
    luHDF.irrigTech.name    = sources.lu.irrig.tech.name;
    luHDF.irrigTech.weightNames = {grids.lu.irrig.map(:).integer};
    luHDF.irrigLim.data     = sources.lu.file;
    luHDF.irrigLim.name     = sources.lu.irrig.lim.name;
    luHDF.laiIrrig.use     = true;
    luHDF.irrigTech.use     = true;
    luHDF.irrigLim.use      = true;
else
    luHDF.laiIrrig.data     = false;
    luHDF.irrigTech.use     = false;
    luHDF.irrigLim.use      = false;
end

%%
%--------------------------------------------------------------------------
%Run
%--------------------------------------------------------------------------
if process.loadLai
    disp('Loading and qaqc LAI data');
    MODIS_load(workingDir,modelHours,sources.modis.file,sources.modis.lai,grids);
    qa_qc(workingDir,files.cleaned,qaqc);
end

if process.loadLu
    if use.irrig
        disp('Loading landuse, flowtimes, impervious, and irrigation data');
    else
        disp('Loading landuse, flowtimes, and impervious data');
    end
    lu_load(workingDir,sources.lu.file,sources.lu,grids.lu)
    if use.replaceLu
        disp('Loading replacement landuse data');
        lu_load(workingDir,sources.luReplace.file,sources.luReplace,grids.luReplace);
    end
end

if process.processLai && ~use.climateScenario
    disp('Downscaling LAI')
    if process.downscaleLai && use.replaceLu
        lai_downscaling_replace_lu(workingDir,files.downscaled,downscale,grids);
    elseif process.downscaleLai
        lai_downscaling(workingDir,files.downscaled,downscale,grids);
    end
end

if process.processLai
    disp('Correcting Urban Cells')
    urban_fix(workingDir,files.urbfix,urbfix);
end

if process.calibrateLaiModel && use.climateScenario
    disp('Calibrating the LAI Model')
    lai_model_calibrate(workingDir,files.laimodel,laiModel,grids);
end

if process.processLai && ~use.climateScenario
    disp('Filling LAI Data');
    julian_fill_landscape(workingDir,files.filled,modelHours,julian);
    if use.irrig
        lai_irrig(workingDir,files.filled,irrigLai);
    end
elseif process.processLai && use.climateScenario && ~process.calibrateLaiModel
    disp('Running the LAI climate change model');
    lai_climate_change(workingDir,files.climScenario,modelHours,climScenario,grids);
end

if process.writeLand
    disp('Generating the final LAND file');
    landscape_hdf_write(workingDir,modelHours,landscapeName,landscapeVersion,IDType,compileDate,luHDF);
end
