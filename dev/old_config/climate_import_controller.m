%--------------------------------------------------------------------------
%Must change every run
%--------------------------------------------------------------------------
%Output climate file
climateName = 'MRW-1980-2007'; %don't use underbars in the name
climateVersion = 'new-precip-weighting-partialNexrad';%'dailyWeight-partialNexrad';
IDType = 'CLIM';
compileDate = now;

%Specify broad components
use.nexrad = true;
use.climateChange = true;

%Specify portions to run
process.reload = false;

%Initial preparation
process.qaqc = true;
process.sun = true;
process.weightPrecip = true;
process.compositeClimate = true;
process.compositePrecip = true;

%Process climate change
process.climateChange = false;

%Finish steps
process.finishClimate = true;
process.partitionPrecip = true;
process.final = true;

%Specify climate forecast scenario
forecastPeriod = 'Forecast2050'; %, 'Forecast2095'
forecastScenario = 'A2'; %, 'A1B', 'B1'

%--------------------------------------------------------------------------
%Specified common inputs
%--------------------------------------------------------------------------
startDate = '1/1/1980';
endDate = '12/31/2007';

%Specify the conversion from the model time zone to UTC, model is in UTC
%This may need more care when model spans time zones
UTC = -5;

%Specify the working directory
workingDir = 'C:\Users\Anthony\Code\workspace';

%mysql information
mysql = struct('host','192.168.0.55','user','ILHM','pass','dataaccess','db','ilhm_inputs');

%Names of working files for climate
files.qaqc = 'qaqc_climate.h5';
files.sun = 'sun_climate.h5';
files.compositeClimate = 'composite_climate.h5';
files.derived = 'derived_climate.h5';
files.filled = 'filled_climate.h5';
files.lookupClimate = 'lookup_climate.h5';

%Names of working files for precip
files.lookupPrecip = 'lookup_precip.h5';
files.weightedPrecip = 'weighted_precip.h5';
files.compositePrecip = 'composite_precip.h5';
files.partPrecip = 'partition_precip.h5';

%Climate change files
files.anomaly = 'gcm_anomalies.mat'; %contains two variables: anomalies, baseVal
files.ensemble = 'ensemble_shifted_climate.h5'; %ensemble GCM-averaged climate change

%--------------------------------------------------------------------------
%Calculated common inputs
%--------------------------------------------------------------------------
modelHours = (datenum(startDate):1/24:datenum(endDate)+23/24)';

%--------------------------------------------------------------------------
%Specify data information
%--------------------------------------------------------------------------
%Specify the sources array
sources.awos.file = 'awos.h5';
sources.awos.skyCov = struct('name','skyCov','class','single','units','1','scale','1','offset','0','nan','-99999','tableCol','sky_cov');
sources.awos.windspeed = struct('name','windspeed','class','single','units','m/s','scale','1','offset','0','nan','-99999','tableCol','wind_spd');
sources.awos.airTemp = struct('name','airTemp','class','single','units','C','scale','1','offset','0','nan','-99999','tableCol','temp');
sources.awos.relHum = struct('name','relHumDewp','class','single','units','%','scale','1','offset','0','nan','-99999','tableCol','dewp');
sources.awos.pressure = struct('name','pressure','class','single','units','Pa','scale','1','offset','0','nan','-99999','tableCol','station_press');

sources.mawn.file = 'mawn.h5';
sources.mawn.airTemp = struct('name','airTemp','class','single','units','C','scale','1','offset','0','nan','-99999','tableCol','air_temp');
sources.mawn.relHum = struct('name','relHum','class','single','units','%','scale','1','offset','0','nan','-99999','tableCol','rel_hum');
sources.mawn.windspeed = struct('name','windspeed','class','single','units','m/s','scale','1','offset','0','nan','-99999','tableCol','wind_avg');
sources.mawn.radShort = struct('name','radShort','class','single','units','W/m^2','scale','1','offset','0','nan','-99999','tableCol','solar_rad');
sources.mawn.soilTempShallow = struct('name','soilTempShallow','class','single','units','C','scale','1','offset','0','nan','-99999','tableCol','soil_temp_2in');
sources.mawn.soilTempDeep = struct('name','soilTempDeep','class','single','units','C','scale','1','offset','0','nan','-99999','tableCol','soil_temp_4in');

sources.nexrad.file = 'nexrad.h5';
sources.nexrad.precip = struct('name','precip_nexrad','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');

sources.ncdc_hourly.file = 'ncdc_hourly.h5';
sources.ncdc_hourly.precip = struct('name','precip_hourly','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
sources.ncdc_daily.file = 'ncdc_daily.h5';
sources.ncdc_daily.precip = struct('name','precip_daily','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');

%Modeled solar radiation, also model used to partition measured solar
%radiation
sources.modeled.radBeamMod = struct('name','radBeamMod','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');
sources.modeled.radBeamMeas = struct('name','radBeamMeas','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');
sources.modeled.radDiffMod = struct('name','radDiffMod','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');
sources.modeled.radDiffMeas = struct('name','radDiffMeas','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');
sources.modeled.sunZenith = struct('name','sunZenith','class','single','units','deg','scale','1','offset','0','nan','-99999');
sources.modeled.radLong = struct('name','solarLongwave','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');

%Composited sources information
sources.composite.radBeam = struct('name','radBeam','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');
sources.composite.radDiff = struct('name','radDiff','class','single','units','W/m^2','scale','1','offset','0','nan','-99999');

%Derived and composited sources information
sources.derived.airVapPress = struct('name','airVapPress','class','single','units','Pa','scale','1','offset','0','nan','-99999');
sources.derived.wetBulb = struct('name','wetBulb','class','single','units','C','scale','1','offset','0','nan','-99999');
sources.derived.precip = struct('name','precip','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
% sources.derived.pressure = struct('name','pressure','class','single','units','Pa','scale','1','offset','0','nan','-99999');
sources.derived.rain = struct('name','rain','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
sources.derived.snow = struct('name','snow','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
sources.derived.snowDensity = struct('name','snowDensity','class','uint32','units','kg/m^3','scale','10000','offset','0','nan','4294967295');

%Specify MySQL table data
tables.awos.data.table = 'awos_data';
tables.awos.data.stationID = 'usaf_call';
tables.awos.data.date = 'date';
tables.awos.stations.table = 'awos_stations';
tables.awos.stations.stationID = 'USAF_ID';
tables.awos.stations.modelX = 'georef_x';
tables.awos.stations.modelY = 'georef_y';
tables.awos.stations.lat = 'latitude';
tables.awos.stations.lon = 'longitude';
tables.awos.stations.elev = 'elevation';

tables.mawn.data.table = 'mawn_data_standard';
tables.mawn.data.stationID = 'MAWN_ID';
tables.mawn.data.date = 'date';
tables.mawn.stations.table = 'mawn_stations';
tables.mawn.stations.stationID = 'MAWN_ID';
tables.mawn.stations.numID = 'ID';
tables.mawn.stations.modelX = 'georef_x';
tables.mawn.stations.modelY = 'georef_y';
tables.mawn.stations.lat = 'latitude';
tables.mawn.stations.lon = 'longitude';
tables.mawn.stations.elev = 'elevation';

tables.ncdc_hourly.data.table = 'watershed_precip_data';
tables.ncdc_hourly.data.stationID = 'COOPID';
tables.ncdc_hourly.data.cols = {'UN','YEAR','MO','DA','COOPID',...
    'TIME01','TIME02','TIME03','TIME04','TIME05','TIME06','TIME07','TIME08',...
    'TIME09','TIME10','TIME11','TIME12','TIME13','TIME14','TIME15','TIME16',...
    'TIME17','TIME18','TIME19','TIME20','TIME21','TIME22','TIME23','TIME24',...
    'HOUR01','HOUR02','HOUR03','HOUR04','HOUR05','HOUR06','HOUR07','HOUR08',...
    'HOUR09','HOUR10','HOUR11','HOUR12','HOUR13','HOUR14','HOUR15','HOUR16',...
    'HOUR17','HOUR18','HOUR19','HOUR20','HOUR21','HOUR22','HOUR23','HOUR24',...
    'FLAG01','FLAG02','FLAG03','FLAG04','FLAG05','FLAG06','FLAG07','FLAG08',...
    'FLAG09','FLAG10','FLAG11','FLAG12','FLAG13','FLAG14','FLAG15','FLAG16',...
    'FLAG17','FLAG18','FLAG19','FLAG20','FLAG21','FLAG22','FLAG23','FLAG24'};
tables.ncdc_hourly.data.vecs = {'unit','year','month','day','stationID',...
    'hr1','hr2','hr3','hr4','hr5','hr6','hr7','hr8',...
    'hr9','hr10','hr11','hr12','hr13','hr14','hr15','hr16',...
    'hr17','hr18','hr19','hr20','hr21','hr22','hr23','hr24',...
    'hour1','hour2','hour3','hour4','hour5','hour6','hour7','hour8',...
    'hour9','hour10','hour11','hour12','hour13','hour14','hour15','hour16',...
    'hour17','hour18','hour19','hour20','hour21','hour22','hour23','hour24',...
    'flag1','flag2','flag3','flag4','flag5','flag6','flag7','flag8',...
    'flag9','flag10','flag11','flag12','flag13','flag14','flag15','flag16',...
    'flag17','flag18','flag19','flag20','flag21','flag22','flag23','flag24'};
tables.ncdc_hourly.stations.table = 'watershed_precip_stations';
tables.ncdc_hourly.stations.stationID = 'COOP_ID';
tables.ncdc_hourly.stations.modelX = 'georef_x';
tables.ncdc_hourly.stations.modelY = 'georef_y';

tables.ncdc_daily.data.table = 'watershed_daily_precip_data';
tables.ncdc_daily.data.stationID = 'COOPID';
tables.ncdc_daily.data.cols = {'COOPID','UN','YEARMO',...
    'DAHR01','DAHR02','DAHR03','DAHR04','DAHR05','DAHR06','DAHR07','DAHR08',...
    'DAHR09','DAHR10','DAHR11','DAHR12','DAHR13','DAHR14','DAHR15','DAHR16',...
    'DAHR17','DAHR18','DAHR19','DAHR20','DAHR21','DAHR22','DAHR23','DAHR24',...
    'DAHR25','DAHR26','DAHR27','DAHR28','DAHR29','DAHR30','DAHR31',...
    'DAY01','DAY02','DAY03','DAY04','DAY05','DAY06','DAY07','DAY08',...
    'DAY09','DAY10','DAY11','DAY12','DAY13','DAY14','DAY15','DAY16',...
    'DAY17','DAY18','DAY19','DAY20','DAY21','DAY22','DAY23','DAY24',...
    'DAY25','DAY26','DAY27','DAY28','DAY29','DAY30','DAY31',...
    'FLAG01','FLAG02','FLAG03','FLAG04','FLAG05','FLAG06','FLAG07','FLAG08',...
    'FLAG09','FLAG10','FLAG11','FLAG12','FLAG13','FLAG14','FLAG15','FLAG16',...
    'FLAG17','FLAG18','FLAG19','FLAG20','FLAG21','FLAG22','FLAG23','FLAG24',...
    'FLAG25','FLAG26','FLAG27','FLAG28','FLAG29','FLAG30','FLAG31'};
tables.ncdc_daily.data.vecs = {'stationID','unit','yearmonth',...
    'dayhr1','dayhr2','dayhr3','dayhr4','dayhr5','dayhr6','dayhr7','dayhr8',...
    'dayhr9','dayhr10','dayhr11','dayhr12','dayhr13','dayhr14','dayhr15','dayhr16',...
    'dayhr17','dayhr18','dayhr19','dayhr20','dayhr21','dayhr22','dayhr23','dayhr24',...
    'dayhr25','dayhr26','dayhr27','dayhr28','dayhr29','dayhr30','dayhr31',...
    'day1','day2','day3','day4','day5','day6','day7','day8',...
    'day9','day10','day11','day12','day13','day14','day15','day16',...
    'day17','day18','day19','day20','day21','day22','day23','day24',...
    'day25','day26','day27','day28','day29','day30','day31',...
    'flag1','flag2','flag3','flag4','flag5','flag6','flag7','flag8',...
    'flag9','flag10','flag11','flag12','flag13','flag14','flag15','flag16',...
    'flag17','flag18','flag19','flag20','flag21','flag22','flag23','flag24',...
    'flag25','flag26','flag27','flag28','flag29','flag30','flag31'};
tables.ncdc_daily.stations.table = 'watershed_daily_precip_stations';
tables.ncdc_daily.stations.stationID = 'COOPID';
tables.ncdc_daily.stations.modelX = 'georef_x';
tables.ncdc_daily.stations.modelY = 'georef_y';
tables.ncdc_daily.where.elemCol = 'ELEM';
tables.ncdc_daily.where.elemVal = 'PRCP';

%Specify grid storage data
grids.model = 'C:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Model_Layers\ILHM\ibound';
grids.nexrad.inDir = 'C:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Precip\NEXRAD\Clipped_Jul_08';
grids.nexrad.clip = 'C:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Precip\NEXRAD\buffbndgrd';

%--------------------------------------------------------------------------
%QA/QC Specification
%--------------------------------------------------------------------------
%Currently, this function doesn't handle multiple sources for one type, it
%could easily be modified to do so, however
qaqc.radShort = sources.mawn.radShort;
qaqc.radShort.file = sources.mawn.file;
qaqc.radShort.scaleTrans = true; %option to scale, offset, and transform the data on read in so units are as specified
%setting this to false will save memory on read in if the data is written
%to HDF5 in an integer-compacted format.  If false, make sure any threshold
%values are appropriate for the unscaled data

qaqc.radShort.method{1}.name = 'manual_outlier'; 
qaqc.radShort.method{1}.stations = [1,4]; %stations ID #s to remove data from
qaqc.radShort.method{1}.intervals = {[datenum('1/1/2004'),datenum('1/1/2008')],...
    [datenum('1/1/2004'),datenum('1/1/2008')]};  %here, enter intervals as rows in each array, array position in cell
%array corresponds to the column indeces specified in the *.stations in the
%previous line
qaqc.radShort.method{2}.name = 'correct_range';
qaqc.radShort.method{2}.range = [0,1366];
qaqc.radShort.method{2}.discard = 'true';

qaqc.relHum = sources.mawn.relHum;
qaqc.relHum.file = sources.mawn.file;
qaqc.relHum.scaleTrans = true;
qaqc.relHum.method{1}.name = 'correct_range';
qaqc.relHum.method{1}.range = [0,100]; %Specify correct range information
qaqc.relHum.method{1}.discard = true; %specify whether to discard bad values, or simply truncate (specify false)
qaqc.relHum.method{2}.name = 'manual_outlier';
qaqc.relHum.method{2}.stations = 5;
qaqc.relHum.method{2}.intervals = {[datenum('1/1/1996'),datenum('1/1/2008')]}; %this is the entire data period, essentially

qaqc.relHumAwos = sources.awos.relHum;
qaqc.relHumAwos.file = sources.awos.file;
qaqc.relHumAwos.scaleTrans = true;
qaqc.relHumAwos.method{1}.name = 'manual_outlier';
qaqc.relHumAwos.method{1}.stations = [726385,725416,722093,726364];
qaqc.relHumAwos.method{1}.intervals = {[datenum('12/31/1979'),datenum('1/1/2008')],...
    [datenum('6/10/2001'),datenum('12/10/2002')],...
    [datenum('7/11/2004'),datenum('11/1/2004')],[datenum('11/1/2005'),datenum('6/1/2006')]};

qaqc.soilTempDeep = sources.mawn.soilTempDeep;
qaqc.soilTempDeep.file = sources.mawn.file;
qaqc.soilTempDeep.scaleTrans = true;
qaqc.soilTempDeep.method{1}.name = 'manual_outlier';
qaqc.soilTempDeep.method{1}.stations = 5;
qaqc.soilTempDeep.method{1}.intervals = {[datenum('1/1/2006'),datenum('5/1/2006')]}; %Specify manual outlier removal information, this is a range

if use.nexrad
    qaqc.nexrad = sources.nexrad.precip; %#ok<*UNRCH>
    qaqc.nexrad.file = sources.nexrad.file;
    qaqc.nexrad.scaleTrans = false;
    qaqc.nexrad.method{1}.name = 'exceeds_threshold';
    qaqc.nexrad.method{1}.threshold = 0.09*1e5; %m scaled by 100000 to match nexrad
    qaqc.nexrad.method{1}.discardHour = true; %if true, all values in an hour with values exceeding threshold will be discarded, if false
    %just bad values themselves will be discarded
end

qaqc.ncdc_hourly = sources.ncdc_hourly.precip;
qaqc.ncdc_hourly.file = sources.ncdc_hourly.file;
qaqc.ncdc_hourly.scaleTrans = true;
qaqc.ncdc_hourly.method{1}.name = 'exceeds_threshold';
qaqc.ncdc_hourly.method{1}.threshold = 0.09; %m
qaqc.ncdc_hourly.method{1}.discardHour = false;
qaqc.ncdc_hourly.method{2}.name = 'precision_threshold';
qaqc.ncdc_hourly.method{2}.threshold = unit_conversions(0.1,'in','m');
qaqc.ncdc_hourly.method{2}.discardAll = false;
%If discardAll is true, all observations taken at precision below threshold
%will be omitted, if false, then only those values below the precision
%threshold (i.e. presever large values even at low precision stations)

qaqc.ncdc_daily = sources.ncdc_daily.precip;
qaqc.ncdc_daily.file = sources.ncdc_daily.file;
qaqc.ncdc_daily.scaleTrans = true;
qaqc.ncdc_daily.method{1}.name = 'station_compare_zeros';
qaqc.ncdc_daily.method{1}.threshold = 0.02; %maximum fraction of observations (weekly aggregated) are 0 when all other stations are not

%--------------------------------------------------------------------------
%Solar Insolation
%--------------------------------------------------------------------------
sun.airTemp.data = sources.awos.file;
sun.airTemp.name = sources.awos.airTemp.name;
sun.relHum.data = sources.awos.file;
sun.relHum.name =  sources.awos.relHum.name;
sun.pressure.data = sources.awos.file;
sun.pressure.name = sources.awos.pressure.name;
sun.skyCov.data = sources.awos.file;
sun.skyCov.name = sources.awos.skyCov.name;
sun.radShort.data = files.qaqc;
sun.radShort.name = sources.mawn.radShort.name;
sun.model = true;
sun.output = sources.modeled;

%--------------------------------------------------------------------------
%Composite Sources Climate Data
%--------------------------------------------------------------------------
%Use positive or 0 weights only for priority
%Climate
compositeClim.windspeed.data = {sources.awos.file,sources.mawn.file};
compositeClim.windspeed.name = {sources.awos.windspeed.name,sources.mawn.windspeed.name};
compositeClim.windspeed.priority = [0,0];
compositeClim.airTemp.data = {sources.awos.file,sources.mawn.file};
compositeClim.airTemp.name = {sources.awos.airTemp.name,sources.mawn.airTemp.name};
compositeClim.airTemp.priority = [0,0];
compositeClim.relHum.data = {sources.awos.file,files.qaqc};
compositeClim.relHum.name = {sources.awos.relHum.name,sources.mawn.relHum.name};
compositeClim.relHum.priority = [0,0];
compositeClim.pressure.data = {sources.awos.file};
compositeClim.pressure.name = {sources.awos.pressure.name};
compositeClim.pressure.priority = 0;

%Solar
compositeClim.radBeam.data = {files.sun,files.sun};
compositeClim.radBeam.name = {sources.modeled.radBeamMod.name,sources.modeled.radBeamMeas.name};
compositeClim.radBeam.priority = [0,0];
compositeClim.radDiff.data = {files.sun,files.sun};
compositeClim.radDiff.name = {sources.modeled.radDiffMod.name,sources.modeled.radDiffMeas.name};
compositeClim.radDiff.priority = [0,0];
compositeClim.sunZenith.data = {files.sun};
compositeClim.sunZenith.name = {sources.modeled.sunZenith.name};
compositeClim.sunZenith.priority = 0;
compositeClim.radLong.data = {files.sun};
compositeClim.radLong.name = {sources.modeled.radLonge};
compositeClim.radLong.priority = 0;

%soil temp
compositeClim.soilTempShallow.data = {sources.mawn.file};
compositeClim.soilTempShallow.name = {sources.mawn.soilTempShallow.name};
compositeClim.soilTempShallow.priority = 0;
compositeClim.soilTempDeep.data = {files.qaqc};
compositeClim.soilTempDeep.name = {sources.mawn.soilTempDeep.name};
compositeClim.soilTempDeep.priority = 0;


%--------------------------------------------------------------------------
%Lookup map generation precip data
%--------------------------------------------------------------------------
lookupPrecip.precip.data = files.qaqc;
lookupPrecip.precip.name = sources.ncdc_hourly.precip.name;

%--------------------------------------------------------------------------
%Daily gauge precipitation weighting
%--------------------------------------------------------------------------
dailyWeighting.precip_hourly.data   = files.lookupPrecip;
dailyWeighting.precip_hourly.name   = sources.ncdc_hourly.precip.name;
dailyWeighting.precip_daily.data    = files.qaqc;
dailyWeighting.precip_daily.name    = sources.ncdc_daily.precip.name;
dailyWeighting.method.maxNeighbors  = 6; %for IDW, maximum of 6 neighbors
dailyWeighting.UTC = UTC;

%--------------------------------------------------------------------------
%Gauge/NEXRAD Compositing Function
%--------------------------------------------------------------------------
if use.nexrad
    compositePrecip.gauge.data = files.weightedPrecip;
    compositePrecip.gauge.name = sources.ncdc_hourly.precip.name;
    compositePrecip.radar.data = files.qaqc;
    compositePrecip.radar.name = sources.nexrad.precip.name;
    
    %Specify the source attributes of the ouput dataset
    compositePrecip.precip = sources.derived.precip;
    
    %Specify the method information
    compositePrecip.method.rescale = false; %Note, be careful that the scale/offset parameters
    %are identical among sources because if set to false this will ignore those
    %first column are start of overlap periods
    %second is end of overlap periods.  For all dates >= start and < end, radar
    compositePrecip.method.useRadar = [datenum({'4/1/2000';'4/1/2001';'4/1/2002';'4/1/2003'}),...
        datenum({'10/1/2000';'11/1/2001';'11/1/2002';'1/1/2010'})]; 
    %will be used
end

%--------------------------------------------------------------------------
%Climate change ensemble Gauge Rescaling
%--------------------------------------------------------------------------
ensemble.forecast = forecastPeriod;
ensemble.scenario = forecastScenario;
ensemble.anomaly = files.anomaly;
ensemble.precip.data = files.compositePrecip;
ensemble.precip.name = sources.derived.precip.name;
% ensemble.basePeriod = datenum({'1/1/1990','12/31/1999'});
ensemble.airTemp.data = files.compositeClimate;
ensemble.airTemp.name = sources.mawn.airTemp.name;

%--------------------------------------------------------------------------
%Derived Inputs Climate
%--------------------------------------------------------------------------
if use.climateChange
    derived.airTemp.data            = files.ensemble;
    derived.airTemp.name            = sources.mawn.airTemp.name;
else
    derived.airTemp.data            = files.compositeClimate;
    derived.airTemp.name            = sources.mawn.airTemp.name;
end
derived.relHum.data             = files.compositeClimate;
derived.relHum.name             = sources.mawn.relHum.name;
derived.airVapPress             = sources.derived.airVapPress;
derived.wetBulb                 = sources.derived.wetBulb;
derived.snowDensity             = sources.derived.snowDensity;
derived.pressure                = sources.derived.pressure;

%--------------------------------------------------------------------------
%Julian Fill Climate
%--------------------------------------------------------------------------
if use.climateChange
    filled.airTemp.data             = files.ensemble;
    filled.airTemp.name             = sources.mawn.airTemp.name;
else
    filled.airTemp.data             = files.compositeClimate;
    filled.airTemp.name             = sources.mawn.airTemp.name;
end
filled.windspeed.data           = files.compositeClimate;
filled.windspeed.name           = sources.mawn.windspeed.name;
filled.soilTempShallow.data     = files.compositeClimate;
filled.soilTempShallow.name     = sources.mawn.soilTempShallow.name;
filled.soilTempDeep.data        = files.compositeClimate;
filled.soilTempDeep.name        = sources.mawn.soilTempDeep.name;
filled.radBeam.data           = files.compositeClimate;
filled.radBeam.name           = sources.composite.radBeam.name;
filled.radDiff.data           = files.compositeClimate;
filled.radDiff.name           = sources.composite.radDiff.name;
filled.radLong.data       = files.compositeClimate;
filled.radLong.name       = sources.modeled.radLong.name;
filled.sunZenith.data         = files.compositeClimate;
filled.sunZenith.name         = sources.modeled.sunZenith.name;

filled.pressure.data            = files.compositeClimate;
filled.pressure.name            = sources.derived.pressure.name;

filled.airVapPress.data         = files.derived;
filled.airVapPress.name         = sources.derived.airVapPress.name;
filled.wetBulb.data             = files.derived;
filled.wetBulb.name             = sources.derived.wetBulb.name;
filled.snowDensity.data         = files.derived;
filled.snowDensity.name         = sources.derived.snowDensity.name;

%--------------------------------------------------------------------------
%Lookup map generation climate data
%--------------------------------------------------------------------------
lookupClim.pressure.data           = files.filled;
lookupClim.pressure.name           = sources.derived.pressure.name;
lookupClim.airTemp.data            = files.filled;
lookupClim.airTemp.name            = sources.mawn.airTemp.name;
lookupClim.windspeed.data          = files.filled;
lookupClim.windspeed.name          = sources.mawn.windspeed.name;
lookupClim.soilTempShallow.data    = files.filled;
lookupClim.soilTempShallow.name    = sources.mawn.soilTempShallow.name;
lookupClim.soilTempDeep.data       = files.filled;
lookupClim.soilTempDeep.name       = sources.mawn.soilTempDeep.name;
lookupClim.radBeam.data          = files.filled;
lookupClim.radBeam.name          = sources.composite.radBeam.name;
lookupClim.radDiff.data          = files.filled;
lookupClim.radDiff.name          = sources.composite.radDiff.name;
lookupClim.radLong.data      = files.filled;
lookupClim.radLong.name      = sources.modeled.radLong.name;
lookupClim.sunZenith.data        = files.filled;
lookupClim.sunZenith.name        = sources.modeled.sunZenith.name;
lookupClim.airVapPress.data        = files.filled;
lookupClim.airVapPress.name        = sources.derived.airVapPress.name;
lookupClim.wetBulb.data            = files.filled;
lookupClim.wetBulb.name            = sources.derived.wetBulb.name;
lookupClim.snowDensity.data        = files.filled;
lookupClim.snowDensity.name        = sources.derived.snowDensity.name;

%--------------------------------------------------------------------------
%Rain/Snow Partition
%--------------------------------------------------------------------------
if use.nexrad && ~use.climateChange
    snowPart.precip.data = files.compositePrecip;
    snowPart.precip.name = sources.derived.precip.name;
elseif use.climateChange
    snowPart.precip.data = files.ensemble;
    snowPart.precip.name = sources.derived.precip.name;
else
    snowPart.precip.data = files.weightedPrecip;
    snowPart.precip.name = sources.ncdc_hourly.precip.name;
end
snowPart.rain.name = sources.derived.rain.name;
snowPart.snow.name = sources.derived.snow.name;
snowPart.airTemp.data = files.lookupClimate;
snowPart.airTemp.name = sources.mawn.airTemp.name;

snowPart.method.frozenThreshold = 3; %This should be the same as in the helper function rain_snow_partition
snowPart.method.modelGrid = grids.model; %This is optional, and only needed if the precip data are not spatially explicit
snowPart.method.rescale = false; %This specifies whether the precipitation data should be rescaled on read-in (if false, will save memory)

%--------------------------------------------------------------------------
%Climate HDF Write
%--------------------------------------------------------------------------
%Specify data sources climate
climHDF.airTemp.data            = files.lookupClimate;
climHDF.airTemp.name            = sources.mawn.airTemp.name;
climHDF.wetBulb.data            = files.lookupClimate;
climHDF.wetBulb.name            = sources.derived.wetBulb.name;
climHDF.airVapPress.data        = files.lookupClimate;
climHDF.airVapPress.name        = sources.derived.airVapPress.name;
climHDF.windspeed.data          = files.lookupClimate;
climHDF.windspeed.name          = sources.mawn.windspeed.name;
climHDF.radLong.data      = files.lookupClimate;
climHDF.radLong.name      = sources.modeled.radLong.name;
climHDF.radDiff.data          = files.lookupClimate;
climHDF.radDiff.name          = sources.composite.radDiff.name;
climHDF.radBeam.data          = files.lookupClimate;
climHDF.radBeam.name          = sources.composite.radBeam.name;
climHDF.sunZenith.data        = files.lookupClimate;
climHDF.sunZenith.name        = sources.modeled.sunZenith.name;
climHDF.pressure.data           = files.lookupClimate;
climHDF.pressure.name           = sources.derived.pressure.name;
climHDF.soilTempShallow.data    = files.lookupClimate;
climHDF.soilTempShallow.name    = sources.mawn.soilTempShallow.name;
climHDF.soilTempDeep.data       = files.lookupClimate;
climHDF.soilTempDeep.name       = sources.mawn.soilTempDeep.name;
climHDF.snowDensity.data        = files.lookupClimate;
climHDF.snowDensity.name        = sources.derived.snowDensity.name;

%Specify data sources precip
climHDF.rain.data               = files.partPrecip;
climHDF.rain.name               = sources.derived.rain.name;
climHDF.snow.data               = files.partPrecip;
climHDF.snow.name               = sources.derived.snow.name;

%--------------------------------------------------------------------------
%Run
%--------------------------------------------------------------------------
%Load Data
if process.reload
    NEXRAD_load(workingDir,sources.nexrad.file,sources.nexrad,grids) %#ok<UNRCH>
% %     AWOS_load(modelHours,mysql,UTC,workingDir,sources.awos.file,sources.awos,tables.awos)
%     MAWN_load(modelHours,mysql,UTC,workingDir,sources.mawn.file,sources.mawn,tables.mawn)
%     NCDC_hourly_load(modelHours,mysql,UTC,workingDir,sources.ncdc_hourly.file,sources.ncdc_hourly,tables.ncdc_hourly)
%     NCDC_daily_load(modelHours,mysql,UTC,workingDir,sources.ncdc_daily.file,sources.ncdc_daily,tables.ncdc_daily)
% %     weather_station_load(modelHours,mysql,UTC,workingDir,sources.station.file,sources.station,tables.station)
end

%QA/QC
if process.qaqc
    qa_qc(workingDir,files.qaqc,qaqc);
end

%Prepare solar radiation
if process.solar
    solar_insolation(modelHours,workingDir,files.solar,solar);
end

%Prepare hourly and daily precipitation
if process.weightPrecip
%     lookup_map_generation(workingDir,files.lookupPrecip,lookupPrecip,grids.model);
%     ncdc_daily_weighting(workingDir,files.weightedPrecip,dailyWeighting,grids.model,grids.nexrad.clip);  
end

%Compsite data sources
if process.compositeClimate
    composite_sources_climate(modelHours,workingDir,files.compositeClimate,compositeClim);
end
if process.compositePrecip && use.nexrad
    composite_sources_precip(modelHours,workingDir,files.compositePrecip,compositePrecip);
end

%Climate change
if process.climateChange
    ensemble_gauge_rescaling(workingDir,files.ensemble,ensemble);
end

%Prepare derived climate inputs, fill in gaps, and generate lookup maps
if process.finishClimate  
    derived_inputs_climate(workingDir,files.derived,derived);
    julian_fill_climate(workingDir,files.filled,filled);
    lookup_map_generation(workingDir,files.lookupClimate,lookupClim,grids.model);
end

%Rain/snow partitioning
if process.partitionPrecip
    ncdc_snow_partition(modelHours,workingDir,files.partPrecip,snowPart);
end

%Prepare final file
if process.final
    climate_hdf_write(workingDir,climateName,climateVersion,IDType,compileDate,climHDF);
end