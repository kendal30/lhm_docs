%   Loads in static grids and saves them to the STAT file
%
%   LHM assumes that NaN values for grids are given by the
%   maximum values of unsigned integer, and minimum/maximum values of float
%   or signed integer.


%--------------------------------------------------------------------------
%Specify Path Information
%--------------------------------------------------------------------------
%Specify path to the base preparation directory
baseDir = 'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model';

%Specify submodel name
nameSubmodel = 'EBCW'; %set to false if not a submodel, otherwise a string matching the name of the folder immediately beneath Submodel_Layers

%Specify the name of this static input dataset, and the version
%Use only alphanumeric characters and dashes in the name
staticName = 'EBCW-LPMI-submodel';
staticVersion = '1';

%Specify number of layers for multilayer datasets
soilNumLay = 4;
gndwaterNumLay = 1;

%Specify which groups to read in, move them to doNotReadGroups if not
%wanted, this is really just to keep track of them
%Note: if grids are not present (especially for groundwater) then a warning
%will be thrown but no error, this can be okay depending on how modflow is
%specified later.
readGroups = {'structure', 'topography','runoff','soil','params','state','groundwater'};
doNotReadGroups = {};

%--------------------------------------------------------------------------
%Run stat_file_load for the rest
%--------------------------------------------------------------------------
%Call the function
stat_file_load({baseDir,nameSubmodel},{staticName,staticVersion},{soilNumLay,gndwaterNumLay},readGroups)