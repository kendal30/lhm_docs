function output = v1_specify_output()
%SPECIFY_OUTPUT  Specify output datatypes.
%   output = specify_output()
%
%   Descriptions of Input Variables:
%   none
%
%   Descriptions of Output Variables:
%   output: a structured array containing the information needed by
%           intialize_output
%
%   Example(s):
%   >> specify_output
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Define grids that define zones, points, and grid outputs
%--------------------------------------------------------------------------
%Grids can have integer values, the fieldnames of the structured array
%are the names of the grids referenced below in zones.grids, the field values are
%the name of the grids saved by observations_import_controller in the grids_out structure
%of the OBS file
zoneGrids = struct('model','model_grid');
gridGrids = struct('model','model_grid');
pointGrids = struct(); %#ok<NASGU>

%--------------------------------------------------------------------------
%Define custom output and stats sets
%--------------------------------------------------------------------------
%Below are complete datasets broken down by group, surface runoff is not
%included, nor are groundwater components
hydroSet = {'water_balance',...
    'deep_percolation_upland','deep_percolation_wetland','evap_canopy',...
    'evap_depression','evap_ice_pack','evap_post_demand','evap_snow_upland','evap_soil',...
    'evap_wetland','precip','precip_excess_upland','precip_excess_wetland','rain',...
	'runon_internal_upland','runon_internal_wetland',...
    'snow','transp_upland','transp_wetland','transp_wetland_phreatic','water_canopy','water_depression',...
    'water_ice_pack','water_snow_upland','water_soil','water_wetland'};
irrigationSet = {'irrigation_accum_et','irrigation_active','irrigation_annual_water',...
    'irrigation_canopy_partition','irrigation_event_size','irrigation_event_water',...
    'irrigation_drift_evap','irrigation_intensity','irrigation_last_apply',...
    'irrigation_wait_days','irrigation_water_applied','lai','irrigation_present'};
energyStoreSet = {'snow_heat_storage_upland'};
energyFluxSet = {'q_cond_ground'};
radSet = {'rad_short_canopy_net','rad_short_beam_in','rad_short_diff_in','rad_short_surface_up',...
    'rad_short_upland_net','rad_short_wetland_net'};
tempSet = {'temp_air','temp_wetland'};
energySet = {energyStoreSet{:},energyFluxSet{:},radSet{:},tempSet{:}};
physicalSet = {'albedo_canopy_beam','albedo_canopy_diff','albedo_upland_beam',...
    'albedo_upland_diff','albedo_wetland_beam','albedo_wetland_diff','canopy_capacity',...
    'canopy_gap_fraction','canopy_height','water_table_depth_wetland','ice_pack_thickness','lai',...
    'snow_surf_age_upland','sun_zenith_cos','stem_area',...
    'vap_pressure_air','fraction_wetland','fraction_upland','fraction_stream',...
    'wetland_water_density','windspeed','pressure_range'};
internalSet = {'drip_rain','drip_snow','ice_pack_melt','intercept_rain','intercept_snow',...
    'ice_pack_precip_store','ice_pack_precip_release','snow_melt_upland',...
    'soil_air_exchange_depth','soil_infiltration','soil_water_flux',...
    'soil_evap_depth','soil_unsat_conductivity','soil_air_conductivity',...
    'throughfall_rain','throughfall_snow','total_throughfall','total_throughfall_rain',...
    'total_throughfall_snow'};
zoneStats = {'mean','min','max','std'};%must take inputs: newData,[],dim

%Grid set
gridSet = {hydroSet{:},irrigationSet{:},'intercept_rain','intercept_snow','temp_air','soil_evap_depth'};
gridStats = {'plus','min','max'};%must take inputs: newData,prevData

%--------------------------------------------------------------------------
%Define custom time interval sets
%--------------------------------------------------------------------------
dateFormat = 'yyyy/mm/dd HH:MM:SS';
zoneStart = '2001/01/01 00:00:00';
zoneEnd = '2013/12/31 23:00:00';
zoneIntervalStart = (datenum(zoneStart,dateFormat):1/24:datenum(zoneEnd,dateFormat))';
zoneIntervalEnd = zoneIntervalStart;

numYears = 13;
templateYears = repmat((2001:2013)',1,12);
templateMonth = repmat((1:12),numYears,1);
templateDays = ones(size(templateYears));

gridIntervalStart = datenum(reshape(templateYears',[],1),reshape(templateMonth',[],1),reshape(templateDays',[],1));
gridIntervalEnd = datenum(reshape(templateYears',[],1),reshape(templateMonth',[],1)+1,reshape(templateDays',[],1)) - 1/24; %the end of the previous month

%--------------------------------------------------------------------------
%Define the zones, points, and grids to write out
%--------------------------------------------------------------------------
zone.names = {'hydro','energy','physical','internal','irrigation'};
zone.zones = {{'all'},{'all'},{'all'},{'all'},{'all'},};
zone.grids = {zoneGrids.model,zoneGrids.model,zoneGrids.model,zoneGrids.model,zoneGrids.model};
zone.vals = {1,1,1,1,1}; %these are values in the zone.grids
zone.outputs = {hydroSet,energySet,physicalSet,internalSet,irrigationSet};
zone.stats = {zoneStats,zoneStats,zoneStats,zoneStats,zoneStats};
zone.intervalStart = {zoneIntervalStart,zoneIntervalStart,zoneIntervalStart,zoneIntervalStart,zoneIntervalStart};
zone.intervalEnd = {zoneIntervalEnd,zoneIntervalEnd,zoneIntervalEnd,zoneIntervalEnd,zoneIntervalEnd};
zone.sampleSize = {3000,3000,3000,3000,10000}; %this is used to determine the number of points within each zone to sample
%if there aren't enough points in the zone, then all the zone's points will
%be used
zone.rescale = {1,1,1,1,1};

grid.names = {'model'};
grid.grids = {gridGrids.model};
grid.vals = {1}; %these are values in the grid.grids
grid.outputs = {gridSet};
grid.stats = {gridStats};
grid.intervalStart = {gridIntervalStart};
grid.intervalEnd = {gridIntervalEnd};
grid.rescale = {0};

point.names = {};
point.grids = {};
point.vals = {};
point.outputs = {};
point.stats = {};
point.intervalStart = {};
point.intervalEnd = {};

%--------------------------------------------------------------------------
%Collect the arrays for output
%--------------------------------------------------------------------------
output.zone = zone;
output.grid = grid;
output.point = point;
end