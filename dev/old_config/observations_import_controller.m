%   Loads in observations tables and grids and saves them to the OBS file,
%   also loads output specification grids
%
%   LHM assumes that NaN values for grids are given by the
%   maximum values of unsigned integer, and minimum/maximum values of float
%   or signed integer.


%--------------------------------------------------------------------------
%Specify Path Information
%--------------------------------------------------------------------------
%Specify path to the base preparation directory
baseDir = 'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model';

%Specify submodel name
nameSubmodel = false; %set to false if not a submodel, otherwise a string matching the name of the folder immediately beneath Submodel_Layers

%Specify the name of this observations input dataset, and the version
%Use only alphanumeric characters and dashes in the name
obsName = 'LPMI';
obsVersion = '1-noObs';

%Specify input observations output grids as a cell list of name, value
%pairs. For instance {'model_grid','model_grid.tif'} will pull in the grid
%'model_grid.tif' from the inputDirGrids folder and assign it the internal
%name 'model_grid'. You may choose any internal name you want, but it must
%be a valid structured array field name (cannot start with a number, no
%dashes or spaces). This name is then used by you in specify_output and
%nowhere else in the model.
outputGrids = {'model_grid','model_grid.tif'};

%Specify the input tables to read, this is a comma-separated list of dbf
%files in the inputDirObs directory, leave as an empty cell array {} if not
%needed
obsTables = {};

%--------------------------------------------------------------------------
%Run obs_file_load for the rest
%--------------------------------------------------------------------------
obs_file_load({baseDir,nameSubmodel},{obsName,obsVersion},outputGrids,obsTables)
