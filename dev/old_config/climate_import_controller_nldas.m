more off

%--------------------------------------------------------------------------
%Specify these for climate change scenarios
%--------------------------------------------------------------------------
%Specify whether climate change is active
use.climateChange = false;

%Specify whether to process climate change
process.climateChange = false;

%Specify climate forecast scenario
forecastPeriod = 'Forecast2095'; %, 'Forecast2050'
forecastScenario = 'rcp60'; % 'rcp85', 'rcp60'

%Filename of the gcm ensemble
files.anomaly = 'cmip5_anomalies.mat'; %contains one master variable: ensemble

%--------------------------------------------------------------------------
%Specify these for all models
%--------------------------------------------------------------------------
%Output climate file
climateName = 'CHP-2000-aug2014'; %don't use underbars in the name
climateVersion = forecastScenario; %needs to be a string character variable%
IDType = 'CLIM';
compileDate = now;

%Specify portions to run
process.reload = false;

%Finish steps
process.final = true;

%Specify model time period
startDate = '1/1/2000'; %Times are UTC
endDate = '8/31/2014';

%Specify the base LHM prep directory
baseDir = 'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model';

%Specify submodel name
nameSubmodel = 'EBCW'; %set to false if not a submodel

%--------------------------------------------------------------------------
%Calculated common inputs -- shouldn't need to change these
%--------------------------------------------------------------------------
modelHours = (datenum(startDate):1/24:datenum(endDate)+23/24)';

%Standardized directories
workingDir = [baseDir,filesep,'Import_Working'];
if islogical(nameSubmodel)
    layersDir = [baseDir,filesep,'Model_Layers',filesep,'Surface'];
else
    layersDir = [baseDir,filesep,'Submodel_Layers',filesep,nameSubmodel,filesep,'Surface']; 
end

%--------------------------------------------------------------------------
%Model defaults for NLDAS -- shouldn't need to change these
%--------------------------------------------------------------------------
%All directly or calculated from NLDAS, temperature independent
sources.nldas.file = 'nldas.h5';
sources.nldas.fileHeader = 'nldas_header.mat';
sources.nldas.precip = struct('name','precip_nldas','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
sources.nldas.airTemp = struct('name','airTemp','class','int16','units','C','scale','100','offset','0','nan','32767');
sources.nldas.relHum = struct('name','relHum','class','uint16','units','%','scale','100','offset','0','nan','65535'); %for now, assume that this will not change with climate
sources.nldas.windspeed = struct('name','windspeed','class','uint16','units','m/s','scale','100','offset','0','nan','65535');
sources.nldas.radShort = struct('name','radShort','class','uint16','units','W/m^2','scale','10','offset','0','nan','65535');
sources.nldas.radLong = struct('name','solarLongwave','class','uint16','units','W/m^2','scale','10','offset','0','nan','65535');
sources.nldas.radBeam = struct('name','radBeam','class','uint16','units','W/m^2','scale','10','offset','0','nan','65535');
sources.nldas.radDiff = struct('name','radDiff','class','uint16','units','W/m^2','scale','10','offset','0','nan','65535');
sources.nldas.pressure = struct('name','pressure','class','uint32','units','Pa','scale','1','offset','0','nan','4294967295');
sources.nldas.sunZenithCos = struct('name','sunZenithCos','class','uint16','units','1','scale','10000','offset','0','nan','65535');

%Temperature-dependent derived quantities
sources.derived.airVapPress = struct('name','airVapPress','class','uint16','units','Pa','scale','1','offset','0','nan','65535');
sources.derived.wetBulb = struct('name','wetBulb','class','int16','units','C','scale','100','offset','0','nan','32767');
sources.derived.rain = struct('name','rain','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
sources.derived.snow = struct('name','snow','class','uint16','units','m/hr','scale','100000','offset','0','nan','65535');
sources.derived.snowDensity = struct('name','snowDensity','class','uint32','units','kg/m^3','scale','10000','offset','0','nan','4294967295');

%Specify grid storage data
grids.model = [layersDir,filesep,'model_grid.tif'];
grids.latitude = [layersDir,filesep,'latitude.tif'];
grids.longitude = [layersDir,filesep,'longitude.tif'];
grids.elevation = [layersDir,filesep,'elevation_mean.tif'];

%Specify shapefile storage data
shapeFiles.nldas = [layersDir,filesep,'boundary_surface_buffer_WGS84.shp'];

%% Specify module parameters
%--------------------------------------------------------------------------
%Climate change ensemble Gauge Rescaling -- shouldn't need to change these
%--------------------------------------------------------------------------
ensemble.outFile = 'ensemble_shifted_climate.h5'; %ensemble GCM-averaged climate change

ensemble.forecast = forecastPeriod;
ensemble.scenario = forecastScenario;
ensemble.anomaly = files.anomaly;
ensemble.dataTypes = {'precip','airTemp','radBeam','radDiff'};
ensemble.gcmVars = {'pr','tas','rsds','rsds'};

ensemble.precip.grid = sources.nldas.fileHeader;
ensemble.precip.data = sources.nldas.file;
ensemble.precip.name = sources.nldas.precip.name;

ensemble.airTemp.grid = sources.nldas.fileHeader;
ensemble.airTemp.data = sources.nldas.file;
ensemble.airTemp.name = sources.nldas.airTemp.name;

ensemble.radBeam.grid = sources.nldas.fileHeader;
ensemble.radBeam.data = sources.nldas.file;
ensemble.radBeam.name = sources.nldas.radBeam.name;

ensemble.radDiff.grid = sources.nldas.fileHeader;
ensemble.radDiff.data = sources.nldas.file;
ensemble.radDiff.name = sources.nldas.radDiff.name;

%--------------------------------------------------------------------------
%Derived Inputs Climate -- shouldn't need to change these
%--------------------------------------------------------------------------
derived                         = sources.derived;
derived.airTemp.name            = sources.nldas.airTemp.name;
derived.precip.name             = sources.nldas.precip.name;
if use.climateChange
    [derived.airTemp.data,...
        derived.precip.data,...
        derived.outFile]        = deal(ensemble.outFile);
else
    [derived.airTemp.data,...
        derived.precip.data,...
        derived.outFile]        = deal(sources.nldas.file);
end
derived.relHum.data             = sources.nldas.file;
derived.relHum.name             = sources.nldas.relHum.name;
derived.airVapPress             = sources.derived.airVapPress;
derived.wetBulb                 = sources.derived.wetBulb;
derived.snowDensity             = sources.derived.snowDensity;

%--------------------------------------------------------------------------
%Climate HDF Write -- shouldn't need to change these
%--------------------------------------------------------------------------
%Specify data source names
climHDF.airTemp.name            = sources.nldas.airTemp.name;
climHDF.radDiff.name          = sources.nldas.radDiff.name;
climHDF.radBeam.name          = sources.nldas.radBeam.name;
climHDF.windspeed.name          = sources.nldas.windspeed.name;
climHDF.radLong.name      = sources.nldas.radLong.name;
climHDF.sunZenithCos.name     = sources.nldas.sunZenithCos.name;
climHDF.pressure.name           = sources.nldas.pressure.name;
climHDF.rain.name               = sources.derived.rain.name;
climHDF.snow.name               = sources.derived.snow.name;
climHDF.wetBulb.name            = sources.derived.wetBulb.name;
climHDF.airVapPress.name        = sources.derived.airVapPress.name;
climHDF.snowDensity.name        = sources.derived.snowDensity.name;

%Specify data source locations
if use.climateChange
    [climHDF.airTemp.data,...
        climHDF.radDiff.data,...
        climHDF.radBeam.data,...
        climHDF.rain.data,...
        climHDF.snow.data,...
        climHDF.wetBulb.data,...
        climHDF.airVapPress.data,...
        climHDF.snowDensity.data]   = deal(ensemble.outFile);
else
    [climHDF.airTemp.data,...
        climHDF.radDiff.data,...
        climHDF.radBeam.data,...
        climHDF.rain.data,...
        climHDF.snow.data,...
        climHDF.wetBulb.data,...
        climHDF.airVapPress.data,...
        climHDF.snowDensity.data]   = deal(sources.nldas.file);
end
climHDF.windspeed.data          = sources.nldas.file;
climHDF.radLong.data      = sources.nldas.file;
climHDF.sunZenithCos.data     = sources.nldas.file;
climHDF.pressure.data           = sources.nldas.file;
% climHDF.soilTempShallow.data    = files.lookupClimate;
% climHDF.soilTempShallow.name    = sources.mawn.soilTempShallow.name;
% climHDF.soilTempDeep.data       = files.lookupClimate;
% climHDF.soilTempDeep.name       = sources.mawn.soilTempDeep.name;


%%
%--------------------------------------------------------------------------
%Run -- shouldn't need to change these
%--------------------------------------------------------------------------
%Load Data
if process.reload
    disp('Loading NLDAS data')
    NLDAS_load(modelHours,workingDir,sources.nldas.file,sources.nldas,grids,shapeFiles)
end

%Climate change
if process.climateChange
    disp('Rescaling Gauge Data for Climate Change Scenario');
    ensemble_gauge_rescaling(workingDir,ensemble.outFile,ensemble);
end

%Calculating temperature-dependent derived quantities
if process.reload || process.climateChange
    disp('Calculating Temperature Dependent Quantities')
    derived_inputs_nldas(workingDir,derived);
end 

%Prepare final file
if process.final
    disp('Creating Final CLIM File');
    climate_hdf_write(workingDir,climateName,climateVersion,IDType,compileDate,climHDF);
end
more on