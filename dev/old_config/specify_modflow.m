function [modflow] = specify_modflow()
%Specify: MODEL_INFO
%--------------------------------------------------------------------------
structure.num_lay = 3; %specify the number of model layers
structure.num_row = 1865;
structure.num_col = 1798;
structure.xllcorner = 465519.09375;
structure.yllcorner = 262103.09375;
structure.cellsize  = 106.22423664;
structure.delr = structure.cellsize;
structure.delc = structure.cellsize;
structure.hnoflo = -999.000;
structure.hdry   = -888.0;

structure.timeunits = 'days'; %options are seconds, minutes, hours, days, years
structure.lenunits = 'meters'; %options are feet, meters, and centimeters

structure.simulation_type = 'TR'; %three options, 'SS', 'TR', and 'RAMP'
%('RAMP' means a ramp-up simulation with a SS stress period followed by a
%TR sequence)

structure.perlen = 'weeks'; %specify either a string, single value, or an array
%if a string is specified, the code will build the perlen array
%automatically, options are
%'years','months','weeks','days','hours','minutes','seconds', currently
%only 'years' and 'weeks' work, though the others can easily be added
%if a single value is specified, the code will assume that
%this is the length of a uniform-length stress period in the units of
%timeunits (unless the model is steady state, then this value will become
%the model length in units of the timeunits), if an array is specified, it
%must have a value in units of the timeunits for each modeled stress period
structure.nstp = 'days'; %this works identically to perlen above, and the data type must match that specified for perlen
structure.nper = 'automatic'; %will be determined from the start and end time above, unless overridden by an array in perlen, 
%in that case, the nper will be calculated based on the length of perlen
structure.tsmult = 'automatic'; %this array is of very little value for most simulations, and will be set to "1" with the 'automatic' string

%For these two, there are several means of specifying outputs:
%1) 'all', 'none', 'first', and 'last' OR
%2) Explictly specify all outputs with a cell array:
%   {stress_period1,stress_period_2,...stress_period_n} where
%   stress_period_1=[timestep1_save,timestep2_save,...,timestepn_save] OR
%3) An array of datenums, must be a vector.
structure.save_budget = 'all'; 
structure.save_head = datenum({'1/31/2007','2/28/2007','3/31/2007','4/30/2007','5/31/2007',...
    '6/30/2007','7/31/2007','8/31/2007','9/30/2007','10/31/2007','11/30/2007','12/31/2007'},...
    'mm/dd/yyyy');

%Specify the MODFLOW executable to run, if using LMG, must use an AMG
%solver executable
structure.modflow_exec = 'mf2k_ak_fast_xeon64.exe';
%--------------------------------------------------------------------------

%Specify: PARAMS
%Specify invidiual named parameters here. Options are "hk" and "vani",
%structure is like:
%params.hk.code = [] %list of all param codes, matches hk grid
%params.hk.val = [] %list of all param values
%params.hk.lay = [] %layer for parameters
%If not using parameters, comment out any params statements not needed,
%except for the params = struct(); line
%(this will tell the code to use the hk grids directly)
%--------------------------------------------------------------------------
params = struct();
params.hk.code = [[-1,-2,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14],[-21,-22,-23,-20],[-31,-32,-33,-30]];
params.hk.val = [[1000,5,50,5,20,15,20,8,8,8,8,8,8],[2.5,2.5,2.5,100],[2.5,2.5,2.5,100]];
params.hk.lay = [[1,1,1,1,1,1,1,1,1,1,1,1,1],[2,2,2,2],[3,3,3,3]];

params.vani.code = [[-200,-201,-202,-203,-204,-205,-206],[-221,-222,-223],[-231,-232,-233]];
params.vani.val = [[1,2,5,5,2,5,2],[5,5,5],[5,5,5]];
params.vani.lay = [[1,1,1,1,1,1,1],[2,2,2],[3,3,3]];
%--------------------------------------------------------------------------

%Specify: CONSTANTS
%Specify individual named constants here. Options are "hk", "vani",
%"spec_yield", "spec_stor"
%Example:
%constants.spec_yield = 0.2;
%If a param is specified, do not include it in constants. If you wish to
%override a stored grid for each type, then include it here.
%--------------------------------------------------------------------------
constants = struct();
constants.spec_stor = 1e-5;
%--------------------------------------------------------------------------

%Specify: Packages
%--------------------------------------------------------------------------
%Activate packages in the package array
package.bas.active        = true;
package.lpf.active        = true;
package.dis.active        = true;
package.zon.active        = true;
package.rch.active        = true;
package.rch_bin.active    = true;
package.evt.active        = true;
package.evt_bin.active    = true;
package.drn.active        = true;
package.riv.active        = false;
package.nam.active        = true;
package.oc.active         = true;
package.pcg.active        = true;
package.lmg.active        = false;
package.wel.active        = false;
%These are output files
package.glo.active        = true;
package.list.active       = true;
package.hed.active        = true;
package.ccf.active        = true;
%These are observation files
package.obs.active        = true;
package.hob.active        = true;

package.bas.extension     = '.ba6';
package.lpf.extension     = '.lpf';
package.dis.extension     = '.dis';
package.zon.extension     = '.zon';
package.rch.extension     = '.rch';
package.rch_bin.extension = '_rch.dat';
package.evt.extension     = '.evt';
package.evt_bin.extension = '_evt.dat';
package.drn.extension     = '.drn';
package.riv.extension     = '.riv';
package.nam.extension     = '.nam';
package.oc.extension      = '.oc';
package.pcg.extension     = '.pcg';
package.lmg.extension     = '.lmg';
package.wel.extension     = '.wel';
%These are output files
package.glo.extension     = '.glo';
package.list.extension    = '.out';
package.hed.extension     = '.hed';
package.ccf.extension     = '.ccf';
%These are observation files
package.obs.extension     = '.obs';
package.hob.extension     = '.hob';

package.bas.unit          = 3;
package.lpf.unit          = 4;
package.dis.unit          = 5;
package.zon.unit          = 8;
package.rch.unit          = 10;
package.rch_bin.unit      = 45;
package.evt.unit          = 11;
package.evt_bin.unit      = 46;
package.drn.unit          = 12;
package.riv.unit          = 13;
package.nam.unit          = [];
package.oc.unit           = 21;
package.pcg.unit          = 22;
package.lmg.unit          = 22;
package.wel.unit          = 23;
%These are output files
package.glo.unit          = 1;
package.list.unit         = 2;
package.hed.unit          = 30;
package.ccf.unit          = 40;
%These are observation files
package.obs.unit          = 51;
package.hob.unit          = 52;

%Specify the type for the NAM file
package.bas.type          = 'BAS6';
package.lpf.type          = 'LPF';
package.dis.type          = 'DIS';
package.zon.type          = 'ZONE';
package.rch.type          = 'RCH';
package.rch_bin.type      = 'DATA(BINARY)';
package.evt.type          = 'EVT';
package.evt_bin.type      = 'DATA(BINARY)';
package.drn.type          = 'DRN';
package.riv.type          = 'RIV';
package.nam.type          = [];
package.oc.type           = 'OC';
package.pcg.type          = 'PCG';
package.lmg.type          = 'LMG';
package.wel.type          = 'WEL';
%These are output files
package.glo.type          = 'GLOBAL';
package.list.type         = 'LIST';
package.hed.type          = 'DATA';
package.ccf.type          = 'DATA(BINARY)';
%These are observation files
package.obs.type          = 'OBS';
package.hob.type          = 'HOB';

%Specify whether each type should be written out by the ILHM_Start
%initialization routine
package.bas.init          = true;
package.lpf.init          = true;
package.dis.init          = true;
package.zon.init          = true;
package.rch.init          = true;
package.rch_bin.init      = false;
package.evt.init          = true;
package.evt_bin.init      = false;
package.drn.init          = true;
package.riv.init          = true;
package.nam.init          = true;
package.oc.init           = true;
package.pcg.init          = true;
package.lmg.init          = true;
package.wel.init          = true;
%These are output files
package.glo.init          = false;
package.list.init         = false;
package.hed.init          = false;
package.ccf.init          = false;
%These are observation files
package.obs.init          = true;
package.hob.init          = true;

%Specify the function name to use for initialization
package.bas.initFunc          = 'bas_write';
package.lpf.initFunc          = 'lpf_write';
package.dis.initFunc          = 'dis_write';
package.zon.initFunc          = 'zon_write';
package.rch.initFunc          = 'rch_write';
package.rch_bin.initFunc      = '';
package.evt.initFunc          = 'evt_write';
package.evt_bin.initFunc      = '';
package.drn.initFunc          = 'drn_write';
package.riv.initFunc          = 'riv_write';
package.nam.initFunc          = 'nam_write';
package.oc.initFunc           = 'oc_write';
package.pcg.initFunc          = 'pcg_write';
package.lmg.initFunc          = 'lmg_write';
package.wel.initFunc          = 'wel_write';
%These are output files
package.glo.initFunc          = '';
package.list.initFunc         = '';
package.hed.initFunc          = '';
package.ccf.initFunc          = '';
%These are observation files
package.obs.initFunc          = 'obs_write';
package.hob.initFunc          = 'hob_write';

%--------------------------------------------------------------------------

%Specify: BAS_INTERNAL
%--------------------------------------------------------------------------
bas.progress_option = 'FREE SHOWPROGRESS DETAILED'; %showprogress detailed is a custom modification I made to the code
bas.array_header = 'INTERNAL 1 (free) -1';
%--------------------------------------------------------------------------

%Specify: LPF_INTERNAL
%--------------------------------------------------------------------------
lpf.lpf_budget=0;
for i=1:structure.num_lay
    lpf.laytyp(i) = 1;
    lpf.layavg(i) = 0; %harmonic mean
    lpf.chani(i) = 1; %set to 0 if horizontal anistropy is used
    lpf.vka(i) = 1; %specify via parameters, is the ratio of HK/VK
    lpf.laywet(i) = 0; %wetting inactive
end
%If allowing wetting in first layer only
lpf.laywet(1) = 0;
lpf.wetfct = 0.3; %factor used to re-establish head in a rewetted cell
lpf.iwetit = 5; %interval of iterations used to rewet cells, these are outer iterations
lpf.ihdwet = 0; %flag used to select equation for re-establishing head, see docs.
lpf.partype_hk='HK';
lpf.partype_vani='VANI';
lpf.nclu=1; %check manual
lpf.mltarr='NONE';
lpf.zonarr_name_hk='HKLAY';
lpf.zonarr_name_vani='VANILAY';
lpf.hk_printcode=-1;
lpf.vani_printcode=-1;
lpf.constant_header = 'CONSTANT';
lpf.array_header = 'INTERNAL 1 (free) -1';
lpf.wetdry = 3; %this is a flag/value combination, indicates which cells can
%cause rewetting (positive is below only) and what value head - botdry is 
%required to rewet the cell, so a value of 3 means 3m of head above the
%bottom of the dry cell.  This can be made as an array if desired, but some
%code mod. will be required
%--------------------------------------------------------------------------

%Specify: DIS_INTERNAL
%--------------------------------------------------------------------------
for j=1:structure.num_lay
    dis.laycbd(j)=0; %see manual, quasi-3d confining bed
end
dis.constant_header='CONSTANT';
dis.array_header = 'INTERNAL 1 (free) -1';
%--------------------------------------------------------------------------

%Specify: ZON_INTERNAL
%--------------------------------------------------------------------------
zon.nzn= 2 * structure.num_lay; %because one zone array for each conductivity layer and vani layer,1 recharge param array
zon.zonarr_name_hk = lpf.zonarr_name_hk;
zon.zonarr_name_vani = lpf.zonarr_name_vani;
zon.array_header = 'INTERNAL 1 (free) -1';
%--------------------------------------------------------------------------

%Specify: RCH_INTERNAL
%--------------------------------------------------------------------------
rch.param_string = 'PARAMETER';
rch.nrchop = 3; %recharge to highest active layer
rch.inrech = 1; %see modflow docs, specifies that a new grid will be written for each stress period
rch.inirch = 0; %see modflow docs, if nrchop == 2, this will be read, otherwise it is ignored by modflow
rch.rch_budget = 0;
rch.partype_rech= ' RCH';
rch.zonarr_name_rech = 'RCHZONE';
rch.array_header_ext = ['EXTERNAL ',num2str(package.rch_bin.unit),' 1 (BINARY) -1'];
%--------------------------------------------------------------------------

%Specify: DRN_INTERNAL
%--------------------------------------------------------------------------
drn.drn_npar = 0; %number of drains parameters in use
drn.drn_print_string = 'NOPRINT';
drn.drn_budget = package.ccf.unit;
%--------------------------------------------------------------------------

%Specify: RIV_INTERNAL
%--------------------------------------------------------------------------
riv.riv_npar = 0; %number of drains parameters in use
riv.riv_print_string = 'NOPRINT';
riv.riv_budget = 0; %do not print RIV fluxes
%--------------------------------------------------------------------------

%Specify: EVT_INTERNAL
%--------------------------------------------------------------------------
evt.et_layer_option = 1; %ET option code, NEVTOP in MODFLOW, if 1 - then all cells first layer, if 2 - specify layer in a new variables
%right now, 1 is the only functioning option
evt.evt_budget = 0; %do not print ET fluxes, this is IEVTCB in MODFLOW, this is the only option right now
evt.array_header = 'INTERNAL 1 (free) -1';
evt.array_header_ext=['EXTERNAL ',num2str(package.evt_bin.unit),' 1 (BINARY) -1'];
%--------------------------------------------------------------------------

%Specify: PCG_INTERNAL
%--------------------------------------------------------------------------
pcg.MXITER = 150; %maximum outer iterations
pcg.ITER1 = 50; %maximum inner iterations
pcg.NPCOND = 1; %1=Modified Incomplete Cholesky (use this one), 2=polynomial (vector computers)
pcg.HCLOSE = 0.75; %head change criterion for convergence, in length units
pcg.RCLOSE = 75; %residual (flux) for convergence, in units of length^3/time
pcg.RELAX = 0.95; %relaxation parameter for Cholesky,=1 usually, sometimes values down to 0.96 can reduce convergence times
pcg.NPBOL = 0; %used for NPCOND=2
pcg.IPRPCG = 0; %printout interval for PCG
pcg.MUTPCG = 3; %0-print tables of max head change and residual, 1- print only total iterations, 2- no printing, 3- print only if convergence fails
pcg.DAMP = 0.5; %usually set to 1, a value less than 1 but greater than 0 causes damping between outer iterations
%--------------------------------------------------------------------------

%Specify: LMG_INTERNAL
%--------------------------------------------------------------------------
%First, check to see if the PCG packge is active, if both it and LMG are
%active, throw an error -- only one solver can be chosen
assert(~(package.pcg.active && package.lmg.active),'Only one solver package can be active')
lmg.STOR1 = 3.2; %storage allocation array 1, see documentation
lmg.STOR2 = 2.2; %storage allocation array 2, see documentation
lmg.STOR3 = 5.4; %storage allocation array 3, see documentation
lmg.ICG = 0; %indicates whether conjugate gradient iterations will be used, improves stability (set to 1 for active)
lmg.MXITER = 50; %maximum outer iterations
lmg.MXCYC = 25; %maximum inner iterations
lmg.BCLOSE = 75; %residual (flux) for convergence, in units of length^3/time
lmg.DAMP = 1; %If -2, relative residual adaptive damping used, need to specify DUP and DFLOW,
%If -1, Cooley's method for adaptive damping used (See documentation),
%If >0, this value of damping used for all iterations (a number close to 1)
lmg.IOUTAMG = 0; %Flag to specify amount of detail printed, see documentation
lmg.DUP = 1.0; %Maximum damping value for relative residual damping method
lmg.DFLOW = 0.2; %Minimum damping value, suggested by documentation
%--------------------------------------------------------------------------

%Specify: WEL_INTERNAL
%--------------------------------------------------------------------------
%There is nothing here yet because it is not finished, but basically pass
%an input schedule read from the LAND file
wel.IWELCB = 0; %unit number to which cell-by-cell flows should be written
%--------------------------------------------------------------------------

%Specify: OC_INTERNAL
%--------------------------------------------------------------------------
oc.head_format='HEAD SAVE FORMAT (1798F10.3) LABEL';
oc.head_save_unit_text='HEAD SAVE UNIT';
oc.bud_format='COMPACT BUDGET';
oc.per_label='PERIOD';
oc.step_label='STEP';
oc.save_budget_string='SAVE BUDGET';
oc.save_head_string='SAVE HEAD';
oc.hed_unit = package.hed.unit;
%--------------------------------------------------------------------------

%Specify: OBS_INTERNAL
%--------------------------------------------------------------------------
%For the OBS file
obs.iscals = 0; %see documentation, this relates to printing of weighted sensitivities

%For the HOB file
hob.mobs = 0; %the number of multi-layer head observations
hob.maxm = 0; %the maximum number of layers for multi-layer observations
hob.tomulth = 1; %the time units multiplier
hob.evh = 1; %the statistic multiplier
hob.stat_flag = 0; %the statistic flag, 0 means that the weight statistic is divided by the observation value
%--------------------------------------------------------------------------

%Update Package array
package.bas.inputs      = bas;
package.lpf.inputs      = lpf;
package.dis.inputs      = dis;
package.zon.inputs      = zon;
package.rch.inputs      = rch;
package.evt.inputs      = evt;
package.drn.inputs      = drn;
package.riv.inputs      = riv;
package.wel.inputs      = wel;
package.oc.inputs       = oc;
package.pcg.inputs      = pcg;
package.lmg.inputs      = lmg;
package.obs.inputs      = obs;
package.hob.inputs      = hob;

%Compile the modflow output structured array
modflow.structure = structure;
modflow.package = package;
modflow.params = params;
modflow.constants = constants;
end

