%--------------------------------------------------------------------------
%Must change every run
%--------------------------------------------------------------------------
%Output landscape file
landscapeName = 'MRW-AGU-2012-2050-A2'; %don't use underbars in the name
%Completed:
%AAAS-modern-A1B-2050, AAAS-modern-actual-2000, AAAS-modern-A1B-2095,
%AAAS-urban-A1B-2050, AAAS-urban-A2-2050, AAAS-urban-B1-2050
%AAAS-reforest-A1B-2050, AAAS-reforest-A1B-2095
%AAAS-urban-A1B-2095, AAAS-urban-A2-2095, AAAS-urban-actual-2095
%AAAS-biofuel-A1B-2050,

landscapeVersion = '1';
IDType = 'LAND';
compileDate = now;

%Specify whether to execute parts of this
loadLai = false;
loadLu = false;
processLai = true;
writeLand = true;

%Specify climate change
climateChange = true;
forecastClim = 'CLIM_MRW-1980-2007_AGU-2012-2050-A2_20121122.h5';
%CLIM_AAAS-A1B-2050_1_20090127.h5,
%CLIM_AAAS-A2-2050_1_20090128.h5, CLIM_AAAS-A2-2095_1_20090128.h5,
%CLIM_AAAS-B1-2050_1_20090128.h5, CLIM_AAAS-B1-2095_1_20090128.h5,
%CLIM_MRW-noNexrad_2_20090105.h5

%Specify input LU grid
% grids.lu.inDir = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Land_Use\AAAS_Biofuels';
%Others are:
%F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Land_Use
%F:\Data\GIS_Data\Forecast_Scenarios_from_Bryan\slowfix
%
% grids.lu.grids = {'biofuel2100'};
%Others are:
%,'urbslow2040','urbslow2100','forslow2040','forslow2100','biofuel2050',
%'biofuel2050'

%--------------------------------------------------------------------------
%Specified common inputs
%--------------------------------------------------------------------------
startDate = '1/1/1980';
endDate = '12/31/2007';

%Specify the working directory
workingDir = 'F:\Users\Anthony\Code\workspace\EMRW';

%Names of working files
files.cleaned = 'cleaned_landscape.h5';
files.adjust = 'adjust_ndvi.h5';
files.function = 'ndvi_lai_function.mat';
files.downscaled = 'downscaled_lai.h5';
files.spatial = 'spatial_corrected_lai.h5';
files.composite = 'composite_lai.h5';
files.urbfix = 'urban_fix_lai.h5';
files.filled = 'filled_lai.h5';

%Climate change files
files.laimodel = 'lai_model_params.mat';
files.climchange = 'lai_climate_change.h5';

%--------------------------------------------------------------------------
%Calculated common inputs
%--------------------------------------------------------------------------
modelHours = (datenum(startDate):1/24:datenum(endDate)+23/24)';

%--------------------------------------------------------------------------
%Specify data information
%--------------------------------------------------------------------------
%Specify the sources array
sources.avhrr.file = 'avhrr.h5';
sources.avhrr.ndvi = struct('name','ndvi','class','uint8','units','1','scale','100','offset','100','nan','255');
sources.gimms.file = 'gimms.h5';
sources.gimms.ndvi = struct('name','ndvi','class','uint8','units','1','scale','100','offset','100','nan','255');
sources.modis.file = 'modis.h5';
sources.modis.lai = struct('name','lai','class','uint8','units','1','scale','10','offset','0','nan','255');
sources.lu.file = 'lu.h5';
sources.lu.lu = struct('name','landuse','class','uint16','units','1','scale','1000','offset','0','nan','65535');
sources.lu.imperv = struct('name','impervious','class','uint16','units','1','scale','1000','offset','0','nan','65535');

sources.avhrr.cleaned = struct('name','ndvi','class','uint8','units','1','scale','100','offset','100','nan','255');
sources.gimms.cleaned = struct('name','gimms','class','uint8','units','1','scale','100','offset','100','nan','255');

sources.composite.lai = struct('name','lai','class','uint8','units','1','scale','10','offset','0','nan','255');

%Specify the grids array
grids.model = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Model_Layers\ILHM\ibound';
grids.avhrr.inDir = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\NDVI\Clipped_Jun_08';
grids.avhrr.clip = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\NDVI\buffbndgrd';
grids.gimms.inDir = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\NDVI\GIMMS_Sep_08';
grids.gimms.clip = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\NDVI\gimmsbndgrd';
grids.modis.inDir = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\LAI\Clipped_Sep_08';
grids.modis.clip = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\LAI\buffbndgrd';
grids.lu.inDir = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Land_Use\ILHM_Landuse';
grids.lu.grids = {'ilhm_reclass'};
grids.lu.start = {'1/1/1980 00:00:00'};
grids.lu.end = {'12/31/2007 23:00:00'};
grids.lu.map = struct('integer',{'1','2','3','4','5','6','7','8','9'},'name',...
    {'urban','agriculture','grassland','deciduous','water','wetland','barren','coniferous','shrubland'},...
    'interp',{true,true,true,true,false,true,false,true,true});
%grids.lu.fill = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Land_Use\mgdl_lvl2';
grids.imperv.inDir = 'C:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Land_Use\ILHM_Urban_Layers';
grids.imperv.grids.percent = {'impervpercent'};
grids.imperv.grids.type = {'impervtype'};
grids.imperv.start = {'1/1/1980 00:00:00'};
grids.imperv.end = {'12/31/2007 23:00:00'};
grids.imperv.map = struct('integer',{'1','2','3'},'name',{'upland','wetland','runoff'});

%--------------------------------------------------------------------------
%qa_qc_landscape
%--------------------------------------------------------------------------
qaqc.ndviAvhrr = sources.avhrr.ndvi;
qaqc.ndviAvhrr.file = sources.avhrr.file;
qaqc.ndviAvhrr.scaleTrans = false;
qaqc.ndviAvhrr.method{1}.name = 'diff_outlier';
qaqc.ndviAvhrr.method{1}.threshold = 15;
qaqc.ndviAvhrr.method{1}.min = 100;
qaqc.ndviAvhrr.method{1}.fill = true;
qaqc.ndviAvhrr.method{1}.remove = ''; %dates to manually remove


qaqc.ndviGimms = sources.gimms.ndvi;
qaqc.ndviGimms.file = sources.gimms.file;
qaqc.ndviGimms.scaleTrans = false;
qaqc.ndviGimms.method{1}.name = 'diff_outlier';
qaqc.ndviGimms.method{1}.threshold = 15;
qaqc.ndviGimms.method{1}.min = 100;
qaqc.ndviGimms.method{1}.fill = true;
qaqc.ndviGimms.method{1}.remove = ''; %dates to manually remove

qaqc.lai = sources.modis.lai;
qaqc.lai.file = sources.modis.file;
qaqc.lai.scaleTrans = false;
qaqc.lai.method{1}.name = 'diff_outlier';
%Specify automated method thresholds, this is the percentage difference
%between neighboring values allowed before a value is removed
qaqc.lai.method{1}.threshold = 8;
qaqc.lai.method{1}.min = 0;
qaqc.lai.method{1}.fill = true;
qaqc.lai.method{1}.remove = ''; %dates to manually remove

%--------------------------------------------------------------------------
%ndvi_adjust_gimms
%--------------------------------------------------------------------------
adjust.ndvi.data = files.cleaned;
adjust.ndvi.class = sources.avhrr.cleaned.class;
adjust.ndvi.name.in = sources.avhrr.cleaned.name;
adjust.ndvi.name.out = sources.avhrr.cleaned.name;
adjust.ndvi.clipGrid = grids.avhrr.clip;
adjust.gimms.data = files.cleaned;
adjust.gimms.class = sources.gimms.cleaned.class;
adjust.gimms.name.in = sources.gimms.cleaned.name;
adjust.gimms.name.out = sources.gimms.cleaned.name;
adjust.gimms.clipGrid = grids.gimms.clip;

%--------------------------------------------------------------------------
%ndvi_lai_function
%--------------------------------------------------------------------------
ndviFunc.ndvi.data = files.adjust;
ndviFunc.ndvi.name = sources.avhrr.cleaned.name;
ndviFunc.lai.data = files.cleaned;
ndviFunc.lai.name = sources.modis.lai.name;
ndviFunc.protoThresh = 0.90;
ndviFunc.luFile = [grids.lu.inDir,filesep,grids.lu.grids{1}]; %specify which landuse grid to develop the function

%--------------------------------------------------------------------------
%lai_ndvi_downscaling
%--------------------------------------------------------------------------
downscale.modelLuFile = sources.lu.file;
downscale.luFile = [grids.lu.inDir,filesep,grids.lu.grids{1}]; %specify which landuse grid to use for downscaling
downscale.ndviLaiFile = files.function;
% downscale.datatypes = {'lai'};
downscale.datatypes = {'ndvi','lai'};

downscale.ndvi.data = {files.adjust,files.adjust};
downscale.ndvi.name.in = {sources.avhrr.cleaned.name,sources.gimms.cleaned.name};
downscale.ndvi.name.out = {sources.avhrr.cleaned.name,sources.gimms.cleaned.name};
downscale.ndvi.grid = {grids.avhrr.clip,grids.gimms.clip};
downscale.ndvi.threshold = {[0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75],...
    [0.4,0.65,0.20,0.7,0,0.2,0,0.5,0.20]};

downscale.lai.data = {files.cleaned};
downscale.lai.name.in = {sources.modis.lai.name};
downscale.lai.name.out = {sources.modis.lai.name};
downscale.lai.grid = {grids.modis.clip};
downscale.lai.threshold = {[0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75]};

%--------------------------------------------------------------------------
%lai_spatial_correction
%--------------------------------------------------------------------------
spatial.ndvi.data = {files.downscaled,files.downscaled};
spatial.ndvi.name = {sources.avhrr.cleaned.name,sources.gimms.cleaned.name};
spatial.lai.data = {files.downscaled};
spatial.lai.name = {sources.modis.lai.name};
spatial.standardDays = (1:8:366); %specify a standard set of julian days to use for calculating the correction maps

%--------------------------------------------------------------------------
%lai_composite
%--------------------------------------------------------------------------
composite.lai.data = {files.downscaled,files.spatial,files.spatial};
composite.lai.name = {sources.modis.lai.name,sources.avhrr.cleaned.name,sources.gimms.cleaned.name};
composite.lai.priority = [2,1,0];
composite.composite.lai = sources.composite.lai;

%--------------------------------------------------------------------------
%urban_fix
%--------------------------------------------------------------------------
urbfix.lai.data = files.composite;
urbfix.lai.name = sources.composite.lai.name;
urbfix.lai.class = sources.composite.lai.class;

urbfix.lu.data = sources.lu.file;
urbfix.lu.name = sources.lu.lu.name;
urbfix.lu.urban = 1;
urbfix.lu.water = 5;
urbfix.imperv.data = sources.lu.file;
urbfix.imperv.name = sources.lu.imperv.name;

urbfix.threshold.impervMin = 0.2; %Minimum impervious level to use for filling urban areas
urbfix.threshold.laiBadMax = 0.2; %Maximum mean LAI value indicating incorrect urban LAI areas
urbfix.threshold.waterMax = 0.7; %Maximum water fraction in a cell for correction to be applied
urbfix.threshold.urbanMin = 0.5; %Minimum urban fraction in a cell to use for prototype points

%--------------------------------------------------------------------------
%julian_fill_landscape
%--------------------------------------------------------------------------
julian.lai.data = files.urbfix;
julian.lai.name = sources.composite.lai.name;
julian.lai.class = sources.composite.lai.class;

julian.averageDays = (1:14:366); %julian days to calculate LAI

%--------------------------------------------------------------------------
%landscape_hdf_write
%--------------------------------------------------------------------------
%Specify data sources
luHDF.lu.data           = sources.lu.file;
luHDF.lu.name           = sources.lu.lu.name;
luHDF.lu.weightNames    = {grids.lu.map(:).integer};
luHDF.imperv.data       = sources.lu.file;
luHDF.imperv.name       = sources.lu.imperv.name;
luHDF.imperv.weightNames = {grids.imperv.map(:).integer};
if climateChange
    luHDF.lai.data          = files.climchange;
    luHDF.lai.name          = sources.composite.lai.name;
else
    luHDF.lai.data          = files.filled;
    luHDF.lai.name          = sources.composite.lai.name;
end

%--------------------------------------------------------------------------
%lai_climate_change
%--------------------------------------------------------------------------
climChange.laiModel = files.laimodel;
climChange.laiDays = (1:8:365); %julian days to calculate LAI

climChange.lai = sources.composite.lai;
climChange.lu.data = sources.lu.file;
climChange.lu.name = sources.lu.lu.name;
climChange.lu.weightNames = {'urban','agriculture','grassland','deciduous','wetland','coniferous','shrubland'};
climChange.lu.weightSheets = [1,2,3,4,6,8,9];

climChange.airTemp.data = forecastClim;
climChange.airTemp.name = 'airTemp';

%%
%--------------------------------------------------------------------------
%Run
%--------------------------------------------------------------------------
if loadLai
    AVHRR_load(workingDir,sources.avhrr.file,sources.avhrr.ndvi,grids); %#ok<*UNRCH>
    GIMMS_load(workingDir,sources.gimms.file,sources.gimms.ndvi,grids);
    MODIS_load(workingDir,sources.modis.file,sources.modis.lai,grids);
end
if loadLu
    lu_load(workingDir,sources.lu.file,sources.lu,grids)
end
if processLai && ~climateChange
    qa_qc(workingDir,files.cleaned,qaqc);
    ndvi_adjust_gimms(workingDir,files.adjust,adjust);
    ndvi_lai_function(workingDir,files.function,ndviFunc,grids);
    lai_ndvi_downscaling(workingDir,files.downscaled,downscale,grids);
    lai_spatial_correction(workingDir,files.spatial,spatial);
    lai_downscaling(workingDir,files.downscaled,downscale,grids);
    lai_composite(workingDir,files.composite,modelHours,composite);
    urban_fix(workingDir,files.urbfix,urbfix);
    julian_fill_landscape(workingDir,files.filled,modelHours,julian);
elseif processLai && climateChange
   lai_climate_change(workingDir,files.climchange,modelHours,climChange);
end
if writeLand
    landscape_hdf_write(workingDir,modelHours,landscapeName,landscapeVersion,IDType,compileDate,luHDF);
end
