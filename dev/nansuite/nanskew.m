function skewness = nanskew(data,dim)
%NANSKEW  Calculates the skewness of a distribution, ignores NaNs
%   skewness = nanskew(data,dim)
%
%   Descriptions of Input Variables:
%   data:  input n-dimensional vector
%   dim:   dimension to calculate skewness along, if [] or not specified,
%          will flatten n-dimensional vector
%
%   Descriptions of Output Variables:
%   skewness: output skewness value or array
%
%   Example(s):
%   >> skewness = nanskew(data,1);
%
%   See also: skew
 
% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-02-26
% Copyright 2010 Michigan State University.

if nargin < 2
    data = data(:);
    dim = 1;
elseif nargin == 2
    if isempty(dim)
        data = data(:);
        dim = 1;
    else
        assert((rem(dim,1)==0) && (dim <= ndims(data)),...
            'Integer dimension must be specified smaller than the dataset max dimension');
    end
end

%Determine how the dataset mean will need to be replicated for the vector
%calculation
arraySize = size(data);
repSize = ones(size(arraySize));
repSize(dim) = arraySize(dim);

%Calculate the mean of the dataset along the specified dimension
inMean = repmat(nanmean(data,dim),repSize);

%Calculate the skewness
skewness = nanmean((data - inMean).^3,dim) ./ (nanmean((data - inMean).^2,dim).^(3/2));