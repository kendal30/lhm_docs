function [cCrop,Cum_slow_dev_days] = crop_simple(cCrop,Cum_slow_dev_days,Solar_Rad,Tmax,Tmin,...
    CO2_Mod_Flag,CO2_Effect_on_C3,CO2_Effect_on_C4)
%* CropSimp:  Simple crop model based on ALMANAC (Kiniry et al. 1996, 1997)
%  Created by aris gerakis, 26 jul 1999
%  Modified by aris gerakis, 09 nov 1999:  Incorporated "new" root module.
%  Converted to MATLAB by Anthony Kendall, 02 October 2009.

%ADK: Maybe add Cum_slow_dev_days to cCrop array

%Fetch from data controller
[zerosGrid] = data_controller('structure','pass','zeros_grid');

%Error checks, perhaps move these elsewhere
assert_LHM(all(cCrop.rel_TT_at_Senesce < 1), ...
    'Relative TT at start of senescence must be < 1 [cropSimp]');  % to avoid division by zero later
    
%Define constants (move these elsewhere)
gpm2_to_kgpha = 10;
kgpha_to_gpm2 = 0.1;
Frac_of_New_Root_Biomass_4_Root_Growth = zerosGrid + 0.9;

%Calculate the CO2 effect on crop growth
cCrop = CO2_effect(cCrop,CO2_Mod_Flag,CO2_Effect_on_C3,CO2_Effect_on_C4,zerosGrid);

%Increment the days after emergence counter
indEmerged = (cCrop.DAE > -1);
if any(indEmerged)
    cCrop.DAE(indEmerged) = cCrop.DAE(indEmerged) + 1;
end

% Calculate todays thermal time units for development based on JTR:
% without taking into account stress factors -- bruno and valentina 17 may 2003

cCrop.dTT = Daily_Thermal_Time(cCrop.TbaseDev, Tmax, Tmin, cCrop.ToptDev, zerosGrid);

%cCrop.dTT = Daily_Thermal_Time(cCrop.coldFac, cCrop.droughtFac, cCrop.heatFac, ...
%                               cCrop.NFac, cCrop.PhotoperiodFac, cCrop.PFac, ...
%                               cCrop.TbaseDev, Tmax, Tmin, cCrop.ToptDev)

% Do development and growth if temperature is adequate:
indTemp = (cCrop.dTT > 0);
if any(indTemp)
    % Check some things only before emergence:
    if any(~indEmerged)
        indNotGerm = ~cCrop.germinated;
        if any(indNotGerm)
            % Check if crop germinated based on thermal time to germination:
            [cCrop.germinated(indNotGerm),cCrop.Killed(indNotGerm),cCrop.TTaccumulator] = ...
                Germination_Status(cCrop.dTT(indNotGerm), ...
                cCrop.emgInt(indNotGerm), cCrop.emgSlp(indNotGerm), ...
                cCrop.Killed(indNotGerm), cCrop.Sow_Depth(indNotGerm), ...
                cCrop.TTaccumulator(indNotGerm), cCrop.TTtoEmerge(indNotGerm), ...
                cCrop.TTtoGerminate(indNotGerm), cCrop.TTtoMaturity(indNotGerm));
        end 
        
        indGerm = ~indNotGerm; %these are germinated, but not yet emerged
        if any(indGerm)
            % Check if crop emerged based on thermal time to emergence:
            [Emerged,cCrop.TTaccumulator] = Emergence_Status(cCrop.dTT(indGerm), ...
                cCrop.TTaccumulator(indGerm), cCrop.TTtoEmerge(indGerm));
            if any(Emerged)
                cCrop.DAE(Emerged) = 0;
            end 
        end
    end
    
    % if crop has emerged
    if any(indEmerged)
        %Precalculate quantities
        rel_TT = min(1, cCrop.rel_TT(indEmerged) + cCrop.dTT(indEmerged) ./ ...
            cCrop.TTtoMaturity(indEmerged));
        
        % Calculate Leaf Area Index based on ALMANAC's simple functions of relative LAI
        % vs. relative development time:
        cCrop.LAI(indEmerged) = Leaf_Area_Index(cCrop.LAI_max(indEmerged), cCrop.LAI_yest(indEmerged), ...
            cCrop.rel_LAI_max(indEmerged), cCrop.rel_LAI_param1(indEmerged), ...
            cCrop.rel_LAI_param2(indEmerged), rel_TT, cCrop.rel_TT_at_Senesc(indEmerged), ...
            cCrop.Senesce_Param_LAI(indEmerged), cCrop.droughtFac(indEmerged), cCrop.heatFac(indEmerged), ...
            cCrop.coldFac(indEmerged), cCrop.NFac(indEmerged), cCrop.PFac(indEmerged));
        
        % Calculate Radiation Use Efficiency based on ALMANAC's simple function of relative
        % RUE vs. relative development time:
        cCrop.Rad_Use_Eff(indEmerged) = Radiation_Use_Efficiency(cCrop.Rad_Use_Eff_max(indEmerged), ...
            cCrop.Rad_Use_Eff_yest(indEmerged), rel_TT, ...
            cCrop.rel_TT_at_Senesc(indEmerged), cCrop.Senesce_Param_RUE(indEmerged));
        
        
        % Calculate daily biomass increment (C fixed) in g m-2 based on ALMANAC and
        % CERES:
        cCrop.dBiomass = Biomass_Increment(cCrop.coldFac(indEmerged), cCrop.droughtFac(indEmerged), ...
            cCrop.heatFac(indEmerged), cCrop.LAI(indEmerged), cCrop.NFac(indEmerged), ...
            cCrop.PFac(indEmerged), cCrop.Plant_Pop(indEmerged), cCrop.Rad_Use_Eff(indEmerged), ...
            cCrop.Row_Spacing(indEmerged), Solar_Rad(indEmerged), ...
            cCrop.CO2_Effect(indEmerged));
        
        % Partition C fixed to various organs, for the moment only tops and roots:
        
        [cCrop.Root_Part_Coeff(indEmerged),cCrop.dBiomassRoot(indEmerged),cCrop.rootWt(indEmerged),cCrop.topWt(indEmerged)] = ...
            Partitioning(cCrop.dBiomass(indEmerged),cCrop.rel_TT(indEmerged),cCrop.rootWt(indEmerged),...
            cCrop.topWt(indEmerged),Frac_of_New_Root_Biomass_4_Root_Growth(indEmerged));
        
        % Maturity check will prevent crop from running tomorrow if it matures today,
        % and may trigger harvest:
        cCrop.Matured = (rel_TT >= 1);
%         cCrop.Matured = Maturity_Status(cCrop.rel_TT + cCrop.dTT / cCrop.TTtoMaturity)
        
    end % end if crop has emerged
    
end% end if dTT > 0

% Stop crop model if any environmental extreme such as frost-kill, long-
% term cold, drought, or nutrient stress, etc.:
[cCrop.Killed,Cum_slow_dev_days] = environmental_extremes('S', Cum_slow_dev_days, cCrop.dTT, cCrop.Killed, Tmin, cCrop.rel_TT, ...
    cCrop.rel_TT_at_Senesc);
end

function [biomassIncrement] = Biomass_Increment(coldFac,droughtFac,heatFac,LAI,NFac,PFac,Plant_Pop,...
    Rad_Use_Eff,Row_Spacing,Solar_Rad,CO2_Effect)
%* Biomass_Increment: Calculate daily biomass increment in g m-2.
%  Created by aris gerakis, 26 jul 1999
%  Modified by aris gerakis, 23 may 2000:  Made sure biomass does not become negative
%  Converted to MATLAB by Anthony Kendall, 02 October 2009.

%Define constants (move elsewhere)
Photo_Active_Rad_Fr = 0.5; % Assume that half of solar radiation is useable by plants

%Extinction coefficient from Beer's Law:
%Make sure row spacing is cm on input
Extinct_Coeff = 1.5 - 0.768 .* ((Row_Spacing .* 0.01) .^ 2 .* Plant_Pop) .^ 0.1;
biomassIncrement = Rad_Use_Eff * Photo_Active_Rad_Fr .* Solar_Rad * ...
    (1 - exp(-Extinct_Coeff .* LAI)) .* min(cat(2,droughtFac, heatFac, coldFac, ...
    NFac, PFac),[],2); %g m^-2

% There should also be a LAI increment. Bruno 4/Sept/03
% The above function gives negative biomass when row spacing is very high, e.g. 100 m: 'AG
biomassIncrement = max(0, biomassIncrement) * CO2_Effect;

end

function [cCrop] = CO2_effect(cCrop,CO2_Mod_Flag,CO2_Effect_on_C3,CO2_Effect_on_C4,zerosGrid)
%* CO2_effect: Calculate the CO2 effect on crops, allow to change over time
% Moved here from crop_simple by Anthony Kendall

%Calculate the CO2 effect, add to the cCrop structure, this really should
%only be done as often as the CO2 concentrations change, once per year at
%most, put somewhere else, or simply avoid calculating unecessarily
if CO2_Mod_Flag
    indCO2C3 = strcmpi(cCrop.PhotoSynID,'C3');
    indCO2C4 = strcmpi(cCrop.PhotoSynID,'C4');
    cCrop.CO2_Effect = populate_array(zerosGrid,CO2_Effect_on_C3,CO2_Effect_on_C4,...
        indCO2C3,indCO2C4);
else
    cCrop.CO2_Effect = zerosGrid + 1;
end

end

function [Root_Part_Coeff,dBiomassRoot,rootWt,topWt] = Partitioning(dBiomass,rel_TT,...
    rootWt,topWt,Frac_of_New_Root_Biomass_4_Root_Growth)
%* Partitioning:  Partition assimilate C among organs, for the moment only tops and roots.
%  Created by aris gerakis, 27 jul 1999

Root_Part_Coeff = 1.5 * rel_TT .^ 2 - 1.9 * rel_TT + 0.8; % Swinnen et al., 1994
% the rest of new root C goes to soluble FOM:
dBiomassRoot = dBiomass .* Root_Part_Coeff .* Frac_of_New_Root_Biomass_4_Root_Growth;
rootWt = rootWt + dBiomassRoot;
topWt = topWt + dBiomass .* (1 - Root_Part_Coeff);
end

function [Rad_Use_Eff] = Radiation_Use_Efficiency(Rad_Use_Eff_max,Rad_Use_Eff_yest,...
    rel_TT,rel_TT_at_Senesce,Senesce_Param_RUE,zerosGrid)
%* Radiation_Use_Efficiency: Calculate Radiation_Use_Efficiency based on the simple
%* two-part function in ALMANAC.
%  Created by aris gerakis, 12 jul 1999

[rel_Rad_Use_Eff,Rad_Use_Eff] = deal(zerosGrid);

% Constant part of relative RUE function, before senescence:
indBeforeSenesce = (rel_TT < rel_TT_at_Senesce);
if any(ind)
%     rel_Rad_Use_Eff(indBeforeSenesce) = 1;
    Rad_Use_Eff(indBeforeSenesce) = Rad_Use_Eff_max;
end

% after senescence starts
indSenesce = ~indBeforeSenesce;
if any(indSenesce)
     % Declining RUE due to senescence:
    rel_Rad_Use_Eff(indSenesce) = ((1.001 - rel_TT(indSenesce)) ./ ...
        (1 - rel_TT_at_Senesce(indSenesce))) .^ Senesce_Param_RUE(indSenesce);
    
    % Cannot exceed yesterdays true RUE, otherwise RUE may increase again:
   Rad_Use_Eff = min(Rad_Use_Eff_yest(indSenesce), rel_Rad_Use_Eff(indSenesce) .* Rad_Use_Eff_max(indSenesce));
end

end

function [dailyThermalTime] = Daily_Thermal_Time(TbaseDev,Tmax,Tmin,ToptDev,zerosGrid)

% Function Daily_Thermal_Time(ByVal coldFac As Single, ByVal droughtFac As Single, _
%                             ByVal heatFac As Single, ByVal NFac As Single, _
%                             ByVal PhotoperiodFac As Single, ByVal PFac As Single, _
%                             ByVal TbaseDev As Single, ByVal Tmax As Single, _
%                             ByVal Tmin As Single, ByVal ToptDev As Single) As Single

% Old function commented because stress factors are not included in the Simple model,
% being considered in the Complex one.
% From conversation with JTR in Belton, Nov. 29, 2002 -- Bruno and Valentina

%*=====================================================================
%* Daily_Thermal_Time: Compute thermal time based on new method
%* developed by J.T.R.
%  By aris gerakis, dec 1998
%  Modified by:  aris gerakis 26 jul 1999:  Translated to VB
%  Modified by:  bruno basso e valentina maddalena 17 may 2003
%======================================================================

%Initialize the array
dailyThermalTime = zerosGrid;

% if maximum temperature less than base temperature for development, nothing happens:
indNoDev = (Tmax < TbaseDev);
dailyThermalTime(indNoDev) = 0;

% if maximum temperature greater than base temperature
indDev = ~indNoDev;
if any(indDev)
    ind1 = indDev & (Tmin < TbaseDev);
    ind2 = indDev & (Tmax > ToptDev) & (Tmin < ToptDev);
    ind3 = indDev & ~ind1 & ~ind2;
    
    Teff(ind1) = (TbaseDev(ind1) + min(ToptDev(ind1), Tmax(ind1))) / 2;
    Teff(ind2) = (Tmin(ind2) + ToptDev(ind2)) / 2;
    %   Teff = (Min(Tmin, ToptDev) + ToptDev) / 2#   modified by VB to account for the rare case Tmin > ToptDev
    Teff(ind3) = (Tmin(ind3) + Tmax(ind3)) / 2;
    
    
    % Stress factors not included here, being considered in the Complex model.
    % From conversation with JTR in Belton, Nov. 29, 2002 -- Bruno and Valentina
    dailyThermalTime(indDev) = Teff(indDev) - TbaseDev(indDev);
    
    % Add stress factors (drought Factor must be < 0.2 to have an effect):
    
    %   Daily_Thermal_Time = (Teff - TbaseDev) * PhotoperiodFac * _
    %                        Min(1#, droughtFac / 0.2, heatFac, coldFac, NFac, PFac)
    
end % end if maximum temperature greater than base temperature

end

function [Emergence_Status,TTaccumulator] = Emergence_Status(dTT,TTaccumulator, ...
    TTtoEmerge,zerosGrid)

%*=====================================================================
%* Emergence_Status: determine seedling emergence based on thermal time
%* to emergence.  At some point we will want to consider soil temperature
%  Modified by:  aris gerakis 26 jul 1999:  Translated to VB
%======================================================================

%initialize arrays
Emergence_Status = logical(zerosGrid);

% Accumulate thermal time since germination:
TTaccumulator = TTaccumulator + dTT;

%Check to see if the crop has emerged
indEmerge = (TTaccumulator >= TTtoEmerge);
if any(indEmerge)
    Emergence_Status(indEmerge) = true;
    TTaccumulator(indEmerge) = 0;
end

end

function [Germination_Status,Killed,TTaccumulator] = Germination_Status(dTT,emgInt,emgSlp,Killed,...
    Sow_Depth,TTaccumulator,TTtoEmerge,TTtoGerminate,TTtoMaturity)

%*=====================================================================
%* Germination_Status :  determine germination based on thermal time
%* to germination.
%  Modified by:  aris gerakis 26 jul 1999:  Translated to VB
%======================================================================

Germination_Status = logical(zerosGrid);

% Accumulate thermal time since planting:
TTaccumulator = TTaccumulator + dTT;

indGerminate = (TTaccumulator >= TTtoGerminate);
if any(indGerminate)
   Germination_Status(indGerminate) = true;
   % Zero accumulator to re-use:
   TTaccumulator(indGerminate) = 0;
   % Calculate thermal time to emerge based on Urs' equation:
   %ADK: want to only calculate this once
   TTtoEmerge(indGerminate) = emgInt(indGerminate) + emgSlp(indGerminate) .* Sow_Depth(indGerminate);
   %' If it takes too long to emerge (e.g., if it's sown too deep), kill crop:
   indKill = (TTtoGerminate + TTtoEmerge) > TTtoMaturity;
   if any(indKill)
       Killed(indKill) = true;
   end
end

end

function [LAI, rel_LAI_max] = Leaf_Area_Index(LAI_max,LAI_yest,rel_LAI_max,rel_LAI_param1,rel_LAI_param2,...
    rel_TT,rel_TT_at_Senesce,Senesce_Param_LAI,droughtFac,heatFac,coldFac,NFac,PFac)
%* Leaf_Area_Index: calculate leaf area index based on ALMANAC curve and LAI_max
%  Created by aris gerakis, 26 jul 1999, modified by VM to add the stress factor, Nov.3, 2003

%Pre-senescence
indPre = (rel_TT <= rel_TT_at_Senesce);
if any(indPre)
    % Do sigmoidal part of relative LAI function:
    rel_LAI = rel_TT(indPre) ./ (rel_TT(indPre) + exp(rel_LAI_param1(indPre) - ...
        rel_LAI_param2(indPre) .* rel_TT(indPre)));
    rel_LAI(isnan(rel_LAI)) = 0; %avoid division by 0
    deltaLAI = rel_LAI .* LAI_max(indPre) - LAI_yest(indPre);  %New LAI - yesterday%s VM
    rel_LAI_S = (LAI_yest(indPre) + deltaLAI .* min(droughtFac(indPre), heatFac(indPre), coldFac(indPre), ...
        NFac(indPre), PFac(indPre))) ./ LAI_max(indPre);   % VM
    % save the rel_LAI on the day before leaf area begins to decrease:
    rel_LAI_max(indPre) = rel_LAI_S;
    
    %Calculate the new LAI value
    LAI(indPre) = rel_LAI_S .* LAI_max(indPre);
end

indPost = ~indPre;
if any(indPost)% LAI declines:
   % Declining LAI due to senescence:
   rel_LAI = ((1.001 - rel_TT(indPost)) ./ (1 - rel_TT_at_Senesce(indPost))) .^ Senesce_Param_LAI(indPost);
   
   % Cannot exceed yesterdays true LAI, otherwise LAI will increase again.
   % Note that this prevents pot. LAI from realizing fully:
   LAI(indPost) = min(LAI_yest(indPost), rel_LAI .* LAI_max(indPost) .* rel_LAI_max(indPost));
   
  % deltaLAI = LAI_yest - Min(LAI_yest, rel_LAI * LAI_max * rel_LAI_max)  % VM
  %      If deltaLAI > 0 Then   % VM
  %          rel_LAI_S = (LAI_yest - _
  %          deltaLAI * Min(droughtFac, heatFac, coldFac, NFac, PFac)) / LAI_max % VM
  %          Leaf_Area_Index = rel_LAI_S * LAI_max * rel_LAI_max  % VM
  %      Else
  %          Leaf_Area_Index = LAI_yest - LAI_yest * Min(droughtFac, heatFac, coldFac, NFac, PFac) % VM
  %      End If

end

end

%ADK: This is a public function, used only in Integrat.bas, probably move
%there
function [cCrop,Sen_rootWt] = UpdateSimpCropVars(cCrop, Sen_rootWt)
%*=====================================================================
%* updateSimpCropVars: update the state variables of current crop
%  Created by:  aris gerakis 26 jul 1999
%======================================================================

indNoMaturity = (cCrop.TTtoMaturity <= 0);
%with the one above, which seems to me equivalent
%Increment matured crops
cCrop.rel_TT(indNoMaturity) = cCrop.rel_TT(indNoMaturity) + 0.001;

%Increment immature crops
indMaturing = ~indNoMaturity;
cCrop.rel_TT(indMaturing) = cCrop.rel_TT(indMaturing) + cCrop.dTT(indMaturing) / cCrop.TTtoMaturity(indMaturing);

%Update other cCrop variables
cCrop.LAI_yest = cCrop.LAI;
cCrop.Rad_Use_Eff_yest = cCrop.Rad_Use_Eff;
cCrop.rootWt = cCrop.rootWt - Sen_rootWt;
Sen_rootWt = 0;
end