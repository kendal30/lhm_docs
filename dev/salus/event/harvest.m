function [] = harvest()
% +-----------------------------------------------------------------------
% | Harvest.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 14 jan 2000
% +-----------------------------------------------------------------------


% Private Sub Harvest(ByVal speciesID As String, ByVal RootC As Single, ...
%                     ByVal RootSloC As Single, ByVal RootIntC As Single, ...
%                     ByVal RootSloN As Single, ByVal VegC As Single, ...
%                     ByVal VegSloC As Single, ByVal VegIntC As Single, ...
%                     ByVal VegSloN As Single, ByVal cropmod As String, ...
%                     ByVal gppl_to_gpm2 As Single, ByVal Harvest_Index As Single, ...
%                     ByVal Matured As Boolean, ...
%                     ByVal rootWt As Single, ByVal topWt As Single, ...
%                     ByVal Root_Front_Layer As Integer, ByVal Root_Depth As Single, ...
%                     ByRef Root_Layer_Frac() As Single, ...
%                     ByRef C As Soil_Bio_Element_Type, ByRef n As Soil_Bio_Element_Type, ...
%                     ByRef P As Soil_Bio_Element_Type, ByRef DC_Live_2_Harvest_kg As Double, ...
%                     ByRef DN_Live_2_Harvest_kg As Double, ...
%                     ByRef DP_Live_2_Harvest_kg As Double, ...
%                     ByRef Nitro As Plant_Nutrient_Type, ...
%                     ByRef Phos As Plant_Nutrient_Type, ByRef cCult As Cult_Type, ...
%                     ByRef cSoil As SOIL_TYPE, ByRef csoil_bio As Soil_Bio_Type, ...
%                     ByVal Harvest_ByProd_fraction As Single, ...
%                     ByVal Knock_Down_Fraction_Harv As Single, ...
%                     ByRef newRootFOM_kg_C As Double, ByRef newRootFOM_kg_N As Double, ...
%                     ByRef newRootFOM_kg_P As Double, ByRef newKnockFOM_kg_C As Double, ...
%                     ByRef newKnockFOM_kg_N As Double, ByRef newKnockFOM_kg_P As Double, ...
%                     ByRef newStandFOM_kg_C As Double, ByRef newStandFOM_kg_N As Double, ...
%                     ByRef newStandFOM_kg_P As Double)


% Declare local variables:

% Dim err_str As String, file As String, sql As String
% Dim RsGlobal As New ADODB.Recordset %global parameters
% Dim FOM_kg As Double, IncorporationPercent As Double, ...
%     IncorporationDepth As Single, ...
%     Percent_C As Double, Percent_N As Double, Percent_P As Double, ...
%     Percent_Slow_C As Double, Percent_Inter_C As Double, ...
%     Percent_Slow_N As Double, Fraction_Vol_N As Double, N_Opt_Grain_Fraction As Double, ...
%     P_Opt_Grain_Fraction As Double, newResFOM_kg_C As Double, newResFOM_kg_N As Double, ...
%     newResFOM_kg_P As Double
% Dim k_Slo_FOM_X(-1 : MAX_LAYER) As Double, ...
%     k_Int_FOM_X(-1 : MAX_LAYER) As Double, k_Rap_FOM_X(-1 : MAX_LAYER) As Double
% Dim layer As Integer
% 
% err_str = ''

%****** HARVEST NUTRIENTS ******

% Harvested nutrient (kg ha-1) is the nutrient in harvested grain and/or harvested
% portion of vegetative tops, if there was harvest today.  Harvested nutrient is the
% live nutrient in kg ha-1 times the fraction of nutrient harvested relative to
% total nutrient:  (Mb Cb + Mg Cg) / Mt Ct, where M = mass, C = concentration,
% b = vegetative by-product harvested, g = grain, t = total plant, incl. roots.
% Varies from 0-1.  Cb is equal to Cv = (Mt Ct - Mg Cg) / Mv, where v = vegetative part,
% incl. roots.  Assumptions:  Before maturity in the simple crop model we harvest only
% vegetative plant tops, because the harvest index information is valid for maturity.
% Concentration is uniform in entire plant except grain, because that's the data we
% are most likely to have, and because grain has significantly different concentration.

if ~Matured && cropmod == 'S'
    % Before maturity we assume that mass of grain is zero and that the concentration
    % in vegetative by-product harvested is the same as in the whole plant:
    if (topWt + rootWt) > 0
        DN_Live_2_Harvest_kg = n.Live_kg * Harvest_ByProd_fraction * topWt / ...
            (topWt + rootWt);
        DP_Live_2_Harvest_kg = P.Live_kg * Harvest_ByProd_fraction * topWt / ...
            (topWt + rootWt);
    else
        DN_Live_2_Harvest_kg = 0;
        DP_Live_2_Harvest_kg = 0;
    end
    
else
    if cropmod == 'S'
        N_Opt_Grain_Fraction = Nitro.Opt_Grain_Fraction;
        P_Opt_Grain_Fraction = Phos.Opt_Grain_Fraction;
    elseif cropmod == 'C'
        N_Opt_Grain_Fraction = Nitro.MxKr * Nitro.RlKr;
        P_Opt_Grain_Fraction = Phos.MxKr * Phos.RlKr;
    end
    % At maturity, the whole mess evolves into this.  This may overestimate the
    % chemical harvested, because we use the optimum concentration for grain (we don't
    % keep track of actual nutrient concentration in grain).  So place an upper limit
    % equal to total tops nutrient:
    if ((topWt + rootWt) > 0)
        DN_Live_2_Harvest_kg = min(n.Live_kg * topWt / (topWt + rootWt), ...
            topWt * gpm2_to_kgpha * ((1 - Harvest_Index) * Harvest_ByProd_fraction * ...
            max(0, (n.Live_kg - topWt * gpm2_to_kgpha * Harvest_Index * ...
            N_Opt_Grain_Fraction)) / ((topWt + rootWt) * gpm2_to_kgpha) + ...
            Harvest_Index * N_Opt_Grain_Fraction));
        DP_Live_2_Harvest_kg = min(P.Live_kg * topWt / (topWt + rootWt), ...
            topWt * gpm2_to_kgpha * ((1 - Harvest_Index) * Harvest_ByProd_fraction * ...
            max(0, (P.Live_kg - topWt * gpm2_to_kgpha * Harvest_Index * ...
            P_Opt_Grain_Fraction)) / ((topWt + rootWt) * gpm2_to_kgpha) + ...
            Harvest_Index * P_Opt_Grain_Fraction));
    else
        DN_Live_2_Harvest_kg = 0;
        DP_Live_2_Harvest_kg = 0;
    end
    
end % matured or complex crop

%****** KILL AND INCORPORATE ROOTS ******

% : kill off roots at maturity, call FOM_Placement to partition the nutrients
% into pools.  The model calculates the root N and P concentrations, but it does not
% know % C, % Slow C, % Intermediate C, or % Slow N in the live roots, so they
% have to be read from the Crop_par table.  Volatile N is assumed zero for roots:

if Matured
    FOM_kg = rootWt * gpm2_to_kgpha;
    IncorporationPercent = 100;
    IncorporationDepth = Root_Depth;
    Percent_C = RootC;
    Percent_Slow_C = RootSloC;
    Percent_Inter_C = RootIntC;
    Percent_Slow_N = RootSloN;
    Fraction_Vol_N = 0;
    
    % Root N and P concentrations are whatever live kg has remained after
    % harvest divided by whatever biomass has remained after harvest.  The
    % assumption is that roots have the same concentration as non-harvested
    % tops (for lack of better data in the ALMANAC tables).
    if (topWt > 0 && rootWt > 0)
        Percent_N = (n.Live_kg - DN_Live_2_Harvest_kg) / ...
            gpm2_to_kgpha * (topWt * (1 - Harvest_Index) * ...
            (1 - Harvest_ByProd_fraction) + rootWt) * 100;
        Percent_P = (P.Live_kg - DP_Live_2_Harvest_kg) / ...
            gpm2_to_kgpha * (topWt * (1 - Harvest_Index) * ...
            (1 - Harvest_ByProd_fraction) + rootWt) * 100;
    else
        Percent_N = 0;
        Percent_P = 0;
    end
    for layer = -1 : cSoil.nlayr
        k_Rap_FOM_X(layer) = 0;
        k_Int_FOM_X(layer) = 0;
        k_Slo_FOM_X(layer) = 0;
    end
    [C, n, P, csoil_bio, newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P, MassError] = ...
        FOM_Placement(FOM_kg, k_Slo_FOM_X, k_Int_FOM_X, k_Rap_FOM_X, IncorporationPercent,...
        Root_Front_Layer, Root_Depth, Root_Layer_Frac, IncorporationDepth,...
        Percent_C, Percent_N, Percent_P, Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, Fraction_Vol_N,...
        C, n, P, csoil_bio, cSoil.dlayr, cSoil.nlayr, newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P);
end % matured

%****** KNOCK-DOWN RESIDUE AND STANDING DEAD ******

% if less than the entire tops is harvested, call FOM_Placement to partition the
% remaining tops into knocked-down residue and, at maturity, standing dead.  The model
% calculates the root N and P concentrations, but it does not know % C, % Slow C,
% % Intermediate C, or % Slow N in the tops, so they have to be read from the Crop_par
% table.  Volatile N is assumed zero for knocked-down and standing dead.
% FOM_Placement knows whether residue is standing dead from the negative DepRes:

if Harvest_ByProd_fraction < 1 % less than the entire tops is harvested
    % Some plant is knocked to the surface:
    if ~Matured
        FOM_kg = topWt * gpm2_to_kgpha * (1 - ...
            Harvest_ByProd_fraction) * Knock_Down_Fraction_Harv;
    else
        FOM_kg = topWt * gpm2_to_kgpha * (1 - Harvest_Index) * ...
            (1 - Harvest_ByProd_fraction) * Knock_Down_Fraction_Harv;
    end % matured
    IncorporationPercent = 0; % surface-applied
    Root_Front_Layer = 0;
    IncorporationDepth = 0;
    Percent_C = VegC;
    Percent_Slow_C = VegSloC;
    Percent_Inter_C = VegIntC;
    Percent_Slow_N = VegSloN;
    Fraction_Vol_N = 0;
    
    % Tops N and P concentrations are whatever live kg has remained after
    % harvest divided by whatever biomass has remained after harvest:
    if (topWt > 0 && rootWt > 0)
        Percent_N = (n.Live_kg - DN_Live_2_Harvest_kg) / ...
            gpm2_to_kgpha * (topWt * (1 - Harvest_Index) * ...
            (1 - Harvest_ByProd_fraction) + rootWt) * 100;
        Percent_P = (P.Live_kg - DP_Live_2_Harvest_kg) / ...
            gpm2_to_kgpha * (topWt * (1 - Harvest_Index) * ...
            (1 - Harvest_ByProd_fraction) + rootWt) * 100;
    else
        Percent_N = 0;
        Percent_P = 0;
    end
    for layer = -1 : cSoil.nlayr
        k_Rap_FOM_X(layer) = 0;
        k_Int_FOM_X(layer) = 0;
        k_Slo_FOM_X(layer) = 0;
    end
    [C, n, P, csoil_bio, newKnockFOM_kg_C, newKnockFOM_kg_N, newKnockFOM_kg_P, MassError] = ...
        FOM_Placement(FOM_kg, k_Slo_FOM_X, k_Int_FOM_X, k_Rap_FOM_X, IncorporationPercent,...
        Root_Front_Layer, Root_Depth, Root_Layer_Frac, IncorporationDepth,...
        Percent_C, Percent_N, Percent_P, Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, Fraction_Vol_N,...
        C, n, P, csoil_bio, cSoil.dlayr, cSoil.nlayr, newKnockFOM_kg_C, newKnockFOM_kg_N, newKnockFOM_kg_P);
    
    % Some plant remains as standing dead, if matured:
    if Matured
        FOM_kg = topWt * gpm2_to_kgpha * (1 - Harvest_Index) * ...
            (1 - Harvest_ByProd_fraction) * (1 - Knock_Down_Fraction_Harv);
        RInP = 0;
        IncorporationPercent = 0;
        Root_Front_Layer = 0;
        IncorporationDepth = -1; % That's how the model knows this is standing dead
        Percent_C = VegC;
        Percent_Slow_C = VegSloC;
        Percent_Inter_C = VegIntC;
        Percent_Slow_N = VegSloN;
        Fraction_Vol_N = 0;
        
        % Tops N and P concentrations are whatever live kg has remained after
        % harvest divided by whatever biomass has remained after harvest:
        if (topWt > 0 && rootWt > 0)
            Percent_N = (n.Live_kg - DN_Live_2_Harvest_kg) / gpm2_to_kgpha * ...
                (topWt * (1 - Harvest_Index) * (1 - Harvest_ByProd_fraction) + ...
                rootWt) * 100;
            Percent_P = (P.Live_kg - DP_Live_2_Harvest_kg) / gpm2_to_kgpha * ...
                (topWt * (1 - Harvest_Index) * (1 - Harvest_ByProd_fraction) + ...
                rootWt) * 100;
        else
            Percent_N = 0;
            Percent_P = 0;
        end
        
        for layer = -1 : cSoil.nlayr
            k_Rap_FOM_X(layer) = 0;
            k_Int_FOM_X(layer) = 0;
            k_Slo_FOM_X(layer) = 0;
        end
        [C, n, P, csoil_bio, newStandFOM_kg_C, newStandFOM_kg_N, newStandFOM_kg_P, MassError] = ...
            FOM_Placement(FOM_kg, k_Slo_FOM_X, k_Int_FOM_X, k_Rap_FOM_X, IncorporationPercent,...
            Root_Front_Layer, Root_Depth, Root_Layer_Frac, IncorporationDepth,...
            Percent_C, Percent_N, Percent_P, Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, Fraction_Vol_N,...
            C, n, P, csoil_bio, cSoil.dlayr, cSoil.nlayr, newStandFOM_kg_C, newStandFOM_kg_N, newStandFOM_kg_P);
    end % matured   
end % harvest byproduct > 1

%****** INTEGRATION OF HARVESTED AND LIVE PLANT POOLS ******

% Update cumulative harvested and live plant pools.  I 'd rather do this in the integration
% routine at the end of the day.  Brian would rather update management variables on the spot:
C.Harvested_kg = C.Harvested_kg + DC_Live_2_Harvest_kg;
n.Harvested_kg = n.Harvested_kg + DN_Live_2_Harvest_kg;
P.Harvested_kg = P.Harvested_kg + DP_Live_2_Harvest_kg;

if ~Matured
    n.Live_kg = n.Live_kg - DN_Live_2_Harvest_kg - newKnockFOM_kg_N;
    P.Live_kg = P.Live_kg - DP_Live_2_Harvest_kg - newKnockFOM_kg_P;
else % nutrients in live plants must zero
    n.Live_kg = n.Live_kg - DN_Live_2_Harvest_kg - newRootFOM_kg_N - ...
        newKnockFOM_kg_N - newStandFOM_kg_N;
    
    if abs(n.Live_kg) > 0.0001
        warning_LHM('Live Plant N has not zeroed after maturity');
    else
        n.Live_kg = 0; %round off small error so it doesn't transfer to next planting
    end
    
    P.Live_kg = P.Live_kg - DP_Live_2_Harvest_kg - newRootFOM_kg_P - ...
        newKnockFOM_kg_P - newStandFOM_kg_P;
    
    if abs(P.Live_kg) > 0.0001
        warning_LHM('Live Plant P has not zeroed after maturity');
    else
        P.Live_kg = 0; %round off small error so it doesn't transfer to next planting
    end
end % crop matured

% Done with harvest, I think - AG

%Bruno and Iurii modified it for the treatment with no residue left on 09/05/28
if ~bResidue
    %     Dim ResiduePercentageLeft As Single
    ResiduePercentageLeft = 0.02;
    FOM_kg = 0;
    C.TotMassYest_kg = C.TotMassYest_kg - (1 - ResiduePercentageLeft) * ...
        (C.Rapid_FOM_kg(-1) + C.Inter_FOM_kg(-1) + C.Slow_FOM_kg(-1) - newKnockFOM_kg_C - newStandFOM_kg_C);
    C.Rapid_FOM_kg(-1) = C.Rapid_FOM_kg(-1) * ResiduePercentageLeft;
    C.Inter_FOM_kg(-1) = C.Inter_FOM_kg(-1) * ResiduePercentageLeft;
    C.Slow_FOM_kg(-1) = C.Slow_FOM_kg(-1) * ResiduePercentageLeft;
    newKnockFOM_kg_C = newKnockFOM_kg_C * ResiduePercentageLeft;
    newStandFOM_kg_C = newStandFOM_kg_C * ResiduePercentageLeft;
    
    if ISwNit
        n.TotMassYest_kg = n.TotMassYest_kg - (1 - ResiduePercentageLeft) * ...
            (n.Rapid_FOM_kg(-1) + n.Inter_FOM_kg(-1) + n.Slow_FOM_kg(-1));
        n.Rapid_FOM_kg(-1) = n.Rapid_FOM_kg(-1) * ResiduePercentageLeft;
        n.Inter_FOM_kg(-1) = n.Inter_FOM_kg(-1) * ResiduePercentageLeft;
        n.Slow_FOM_kg(-1) = n.Slow_FOM_kg(-1) * ResiduePercentageLeft;
        newKnockFOM_kg_N = newKnockFOM_kg_N * ResiduePercentageLeft;
        newStandFOM_kg_N = newStandFOM_kg_N * ResiduePercentageLeft;
    end
    
    if ISwPho
        P.TotMassYest_kg = P.TotMassYest_kg - (1 - ResiduePercentageLeft) * ...
            (P.Rapid_FOM_kg(-1) + P.Inter_FOM_kg(-1) + P.Slow_FOM_kg(-1));
        P.Rapid_FOM_kg(-1) = P.Rapid_FOM_kg(-1) * ResiduePercentageLeft;
        P.Inter_FOM_kg(-1) = P.Inter_FOM_kg(-1) * ResiduePercentageLeft;
        P.Slow_FOM_kg(-1) = P.Slow_FOM_kg(-1) * ResiduePercentageLeft;
        newKnockFOM_kg_P = newKnockFOM_kg_P * ResiduePercentageLeft;
        newStandFOM_kg_P = newStandFOM_kg_P * ResiduePercentageLeft;
    end
end

end % Harvest