function [C, n, P, csoil_bio, newFOM_kg_C, newFOM_kg_N, newFOM_kg_P, MassError] = ...
    FOM_Placement(FOM_kg, k_Slo_FOM_X, k_Int_FOM_X, k_Rap_FOM_X, IncorporationPercent,...
    Root_Front_Layer, Root_Depth, Root_Layer_Frac, IncorporationDepth,...
    Percent_C, Percent_N, Percent_P, Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, Fraction_Vol_N,...
    C, n, P, csoil_bio, dlayr, nlayr, newFOM_kg_C, newFOM_kg_N, newFOM_kg_P)
% +-----------------------------------------------------------------------
% | FOM placement.  Equivalent to DSSAT RPLACE but also equipped to
% | handle surface residue and standing dead.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 24 nov 1999
% | Modified by: aris gerakis, 27 jul 2001:  Set mulch labile N & P to an
% |              initial value otherwise it would not start decomposing.
% +-----------------------------------------------------------------------

% Decide whether to use the experimental parameters or the default parameters:
if n.Fraction_FOM_Vol > 0
    Fraction_FOM_Vol_N = n.Fraction_FOM_Vol;
else
    Fraction_FOM_Vol_N = Fraction_Vol_N;
end

Fraction_FOM_Lab_N = 0;

for layer = -1 : nlayr
    if k_Slo_FOM_X(layer) == 0
        k_Slo_FOM_X(layer) = C.k_Slo_FOM(layer);
    end
    if k_Int_FOM_X(layer) == 0
        k_Int_FOM_X(layer) = C.k_Int_FOM(layer);
    end
    if k_Rap_FOM_X(layer) == 0
        k_Rap_FOM_X(layer) = C.k_Rap_FOM(layer);
    end
end

% Check whether Slow + Intermediate C > 100%:
if Percent_Slow_C + Percent_Inter_C > 100
    warning_LHM(['%Slow + %Intermediate FOM pool C > 100 on ',CurDate.DateStr,' [FOM_Placement]']);
    MassError = True;
end

% Save total C mass for mass balance check:
Tot_FOM_kg_C_Before = sumd(C.Slow_FOM_kg(), -1, nlayr) + ...
    sumd(C.Inter_FOM_kg(), -1, nlayr) + sumd(C.Rapid_FOM_kg(), -1, nlayr);

% Save total N mass for mass balance check:
Tot_FOM_kg_N_Before = sumd(n.Slow_FOM_kg(), -1, nlayr) + sumd(n.Rapid_FOM_kg(), -1, nlayr) + ...
    n.Labile_kg(0) + n.Vol_FOM_kg;

% Save total P mass for mass balance check:
Tot_FOM_kg_P_Before = sumd(P.Rapid_FOM_kg(), -1, nlayr) + P.Labile_kg(0);

% Save total applied CNP of this material for mass balance:
newFOM_kg_C = newFOM_kg_C + FOM_kg * Percent_C / 100; %kg ha-1
newFOM_kg_N = newFOM_kg_N + FOM_kg * Percent_N / 100; %kg ha-1
newFOM_kg_P = newFOM_kg_P + FOM_kg * Percent_P / 100; %kg ha-1

% Partition FOM between standing dead, surface and buried:
if IncorporationDepth > 0 % if surface or buried
    
    if IncorporationPercent < 100 % if some is applied on surface
        
        newMULCHDW = FOM_kg * (100 - IncorporationPercent) / 100;  %kg ha-1
        
        % Partition mulch C into FOM pools:
        
        % SLOW FOM POOL:
        oldstuff = C.Slow_FOM_kg(0);
        newstuff = newMULCHDW * Percent_C / 100 * Percent_Slow_C / 100;
        % Existing plus new:
        sum = oldstuff + newstuff;
        % Average out the rate constants for Slow FOM decomposition.  The weights are the
        % relative mass in each fraction:
        if sum > 0
            C.k_Slo_FOM(0) = (C.k_Slo_FOM(0) * oldstuff + k_Slo_FOM_X(0) * newstuff) / sum;
            C.Slow_FOM_kg(0) = sum;
        end
        
        % INTERMEDIATE FOM POOL:
        oldstuff = C.Inter_FOM_kg(0);
        newstuff = newMULCHDW * Percent_C / 100 * Percent_Inter_C / 100;
        sum = oldstuff + newstuff;
        % Average out the rate constants for Intermediate FOM decomposition.  The weights are the
        % relative mass in each fraction:
        if sum > 0
            C.k_Int_FOM(0) = (C.k_Int_FOM(0) * oldstuff + k_Int_FOM_X(0) * newstuff) / sum;
            C.Inter_FOM_kg(0) = sum;
        end
        
        % RAPID FOM POOL:
        oldstuff = C.Rapid_FOM_kg(0);
        newstuff = newMULCHDW * Percent_C / 100 * (100 - Percent_Slow_C - Percent_Inter_C) / 100;
        sum = oldstuff + newstuff;
        % Average out the rate constants for Rapid FOM decomposition.  The weights are the
        % relative mass in each fraction:
        if sum > 0
            C.k_Rap_FOM(0) = (C.k_Rap_FOM(0) * oldstuff + k_Rap_FOM_X(0) * newstuff) / sum;
            C.Rapid_FOM_kg(0) = sum;
        end
        
        % Partition mulch N into FOM pools:
        
        % First take out N for volatile and labile pool, divide the rest into Slow and Rapid:
        
        % VOLATILE POOL:
        n.Vol_FOM_kg = n.Vol_FOM_kg + newMULCHDW * Percent_N / 100 * ...
            Fraction_FOM_Vol_N;
        
        % Got to maintain a minimum value for labile N because otherwise surface residues will
        % never start decomposing.  Give it a value equal to minimum mass in any pool for
        % now.  But first check that volatile + labile N does not exceed total N in FOM
        % (26 jul 2001):
        
        % LABILE N POOL:
        if n.Labile_kg(0) < Min_kg_in_any_pool && n.Vol_FOM_kg + Min_kg_in_any_pool < ...
                newMULCHDW * Percent_N / 100
            new_Labile_kg = Min_kg_in_any_pool - n.Labile_kg(0);
            n.Labile_kg(0) = Min_kg_in_any_pool;
            Fraction_FOM_Lab_N = new_Labile_kg / (newMULCHDW * Percent_N / 100);
        end
        
        % SLOW FOM POOL:
        n.Slow_FOM_kg(0) = n.Slow_FOM_kg(0) + newMULCHDW * Percent_N / 100 * ...
            Percent_Slow_N / 100 * (1 - Fraction_FOM_Vol_N - Fraction_FOM_Lab_N);
        
        % RAPID FOM POOL:
        n.Rapid_FOM_kg(0) = n.Rapid_FOM_kg(0) + newMULCHDW * Percent_N / 100 * (100 - ...
            Percent_Slow_N) / 100 * (1 - Fraction_FOM_Vol_N - Fraction_FOM_Lab_N);
        
        % Partition mulch P into pools:
        
        % Got to maintain a minimum value for labile P because otherwise surface residues will
        % never start decomposing.  Give it a value equal to minimum mass in any pool for
        % now.  But first check that labile P does not exceed total P in FOM (26 jul 2001):
        new_Labile_kg = 0;
        
        if P.Labile_kg(0) < Min_kg_in_any_pool && Min_kg_in_any_pool < ...
                newMULCHDW * Percent_P / 100
            new_Labile_kg = Min_kg_in_any_pool - P.Labile_kg(0);
            P.Labile_kg(0) = Min_kg_in_any_pool;
        end
        
        % RAPID FOM POOL:
        P.Rapid_FOM_kg(0) = P.Rapid_FOM_kg(0) + newMULCHDW * Percent_P / 100 - new_Labile_kg;
        
    end % surface
    
    % Incorporate buried FOM by layer:
    ResInc = FOM_kg * IncorporationPercent / 100; %kg ha-1
    if Root_Front_Layer > 0
        ResDep = Root_Depth;
    else
        ResDep = IncorporationDepth; %cm
    end
    depth = 0;
    iout = 1;
    layer = 1;
    
    while layer <= nlayr && iout ~= 2
        hold = depth;
        depth = depth + dlayr(layer);
        if ResDep <= depth
            if ResDep > 0
                if Root_Front_Layer > 0
                    fr = Root_Layer_Frac(layer);
                else
                    fr = (ResDep - hold) / ResDep;
                end
            end
            if layer == 1
                fr = 1;
            end
            iout = 2;
        else
            if ResDep > 0
                if Root_Front_Layer > 0
                    fr = Root_Layer_Frac(layer);
                else
                    fr = dlayr(layer) / ResDep;
                end
            end
        end
        add = ResInc * fr;
        
        % Partition buried C into FOM pools:
        
        % SLOW FOM POOL:
        oldstuff = C.Slow_FOM_kg(layer);
        newstuff = add * Percent_C / 100 * Percent_Slow_C / 100;
        sum = oldstuff + newstuff;
        % Average out the rate constants for Slow FOM decomposition.  The weights are the
        % relative mass in each fraction:
        if sum > 0
            C.k_Slo_FOM(layer) = (C.k_Slo_FOM(layer) * oldstuff + k_Slo_FOM_X(layer) * newstuff) / sum;
            C.Slow_FOM_kg(layer) = sum;
        end
        
        % INTERMEDIATE FOM POOL:
        oldstuff = C.Inter_FOM_kg(layer);
        newstuff = add * Percent_C / 100 * Percent_Inter_C / 100;
        sum = oldstuff + newstuff;
        % Average out the rate constants for Intermediate FOM decomposition.  The weights are the
        % relative mass in each fraction:
        if sum > 0
            C.k_Int_FOM(layer) = (C.k_Int_FOM(layer) * oldstuff + k_Int_FOM_X(layer) * newstuff) / sum;
            C.Inter_FOM_kg(layer) = sum;
        end
        
        % RAPID FOM POOL:
        oldstuff = C.Rapid_FOM_kg(layer);
        newstuff = add * Percent_C / 100 * (100 - Percent_Slow_C - Percent_Inter_C) / 100;
        sum = oldstuff + newstuff;
        % Average out the rate constants for Rapid FOM decomposition.  The weights are the
        % relative mass in each fraction:
        if sum > 0
            C.k_Rap_FOM(layer) = (C.k_Rap_FOM(layer) * oldstuff + k_Rap_FOM_X(layer) * newstuff) / sum;
            C.Rapid_FOM_kg(layer) = sum;
        end
        
        % Partition buried N into FOM pools:
        n.Slow_FOM_kg(layer) = n.Slow_FOM_kg(layer) + add * Percent_N / 100 * ...
            Percent_Slow_N / 100;
        n.Rapid_FOM_kg(layer) = n.Rapid_FOM_kg(layer) + add * Percent_N / 100 * ...
            (100 - Percent_Slow_N) / 100;
        
        % Partition buried P into FOM pools:
        P.Rapid_FOM_kg(layer) = P.Rapid_FOM_kg(layer) + add * Percent_P / 100;
        
        layer = layer + 1;
    end
    
else % all is allocated to standing dead
    
    newStand_Dead_kg = FOM_kg  ;  %kg ha-1
    
    % Partition standing dead C into FOM pools:
    
    % SLOW FOM POOL:
    oldstuff = C.Slow_FOM_kg(-1);
    newstuff = newStand_Dead_kg * Percent_C / 100 * Percent_Slow_C / 100;
    sum = oldstuff + newstuff;
    % Average out the rate constants for Slow FOM decomposition.  The weights are the
    % relative mass in each fraction:
    if sum > 0
        C.k_Slo_FOM(-1) = (C.k_Slo_FOM(-1) * oldstuff + k_Slo_FOM_X(-1) * newstuff) / sum;
        C.Slow_FOM_kg(-1) = sum;
    end
    
    % INTERMEDIATE FOM POOL:
    oldstuff = C.Inter_FOM_kg(-1);
    newstuff = newStand_Dead_kg * Percent_C / 100 * Percent_Inter_C / 100;
    sum = oldstuff + newstuff;
    % Average out the rate constants for Intermediate FOM decomposition.  The weights are the
    % relative mass in each fraction:
    if sum > 0
        C.k_Int_FOM(-1) = (C.k_Int_FOM(-1) * oldstuff + k_Int_FOM_X(-1) * newstuff) / sum;
        C.Inter_FOM_kg(-1) = sum;
    end
    
    % RAPID FOM POOL:
    oldstuff = C.Rapid_FOM_kg(-1);
    newstuff = newStand_Dead_kg * Percent_C / 100 * (100 - Percent_Slow_C - ...
        Percent_Inter_C) / 100;
    sum = oldstuff + newstuff;
    % Average out the rate constants for Rapid FOM decomposition.  The weights are the
    % relative mass in each fraction:
    if sum > 0
        C.k_Rap_FOM(-1) = (C.k_Rap_FOM(-1) * oldstuff + k_Rap_FOM_X(-1) * newstuff) / sum;
        C.Rapid_FOM_kg(-1) = sum;
    end
    
    % Partition standing dead N into pools:
    n.Slow_FOM_kg(-1) = n.Slow_FOM_kg(-1) + newStand_Dead_kg * Percent_N / 100 * ...
        Percent_Slow_N / 100;
    n.Rapid_FOM_kg(-1) = n.Rapid_FOM_kg(-1) + newStand_Dead_kg * ...
        Percent_N / 100 * (100 - Percent_Slow_N) / 100;
    
    % Partition standing dead P into pools:
    P.Rapid_FOM_kg(-1) = P.Rapid_FOM_kg(-1) + newStand_Dead_kg * Percent_P / 100;
    
end % standing dead


% C Mass balance check:
Tot_FOM_kg_C_After = 0;

for layer = -1 : nlayr
    Tot_FOM_kg_C_After = Tot_FOM_kg_C_After + C.Slow_FOM_kg(layer) + ...
        C.Inter_FOM_kg(layer) + C.Rapid_FOM_kg(layer);
end

err_val = Tot_FOM_kg_C_After - Tot_FOM_kg_C_Before - FOM_kg * ...
    Percent_C / 100;

if Abs(err_val) > 0.001
    warning_LHM(['FOM C mass balance error: ',err_val,' [kg ha-1] [FOM_Placement]']);
    MassError = True;
end

% N Mass balance check:
Tot_FOM_kg_N_After = 0;

for layer = -1 : nlayr
    Tot_FOM_kg_N_After = Tot_FOM_kg_N_After + n.Slow_FOM_kg(layer) + n.Rapid_FOM_kg(layer);
end

Tot_FOM_kg_N_After = Tot_FOM_kg_N_After + n.Vol_FOM_kg + n.Labile_kg(0);

err_val = Tot_FOM_kg_N_After - Tot_FOM_kg_N_Before - FOM_kg * ...
    Percent_N / 100;

if Abs(err_val) > 0.0001
    warning_LHM(['FOM N mass balance error: ',err_val,' [kg ha-1] [FOM_Placement]']);
    MassError = True;
end

% P Mass balance check:
Tot_FOM_kg_P_After = 0;

for layer = -1 : nlayr
    Tot_FOM_kg_P_After = Tot_FOM_kg_P_After + P.Rapid_FOM_kg(layer);
end

Tot_FOM_kg_P_After = Tot_FOM_kg_P_After + P.Labile_kg(0);

err_val = Tot_FOM_kg_P_After - Tot_FOM_kg_P_Before - FOM_kg * ...
    Percent_P / 100;

if Abs(err_val) > 0.0001
    warning_LHM(['FOM P mass balance error: ',err_val,' [kg ha-1] [FOM_Placement]'])
    MassError = True;
end

% Calculate new N:C, P:C ratios for FOM pools, which may be different from
% those already existing in the FOM pools:
for layer = -1 : nlayr
    if C.Slow_FOM_kg(layer) > 0
        csoil_bio.N2C_Slow_FOM(layer) = n.Slow_FOM_kg(layer) / C.Slow_FOM_kg(layer);
    end
    if C.Rapid_FOM_kg(layer) > 0
        csoil_bio.N2C_Rapid_FOM(layer) = n.Rapid_FOM_kg(layer) / C.Rapid_FOM_kg(layer);
        csoil_bio.P2C_Rapid_FOM(layer) = P.Rapid_FOM_kg(layer) / C.Rapid_FOM_kg(layer);
    end
    csoil_bio.N2C_Inter_FOM(layer) = 0;
    csoil_bio.P2C_Slow_FOM(layer) = 0;
    csoil_bio.P2C_Inter_FOM(layer) = 0;
end

end % FOM_Placement