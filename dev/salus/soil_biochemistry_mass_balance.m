function [C,n,P,MassError] = soil_biochemistry_mass_balance(C,n,P,Dc,DN,DP,FOM_C,FOM_N,FOM_P,...
    Fertilizer_C,Fertilizer_N,Fertilizer_P,DAE)
% +-----------------------------------------------------------------------
% | Mass balance check for soil and plant C, N, and P.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 15 oct 1999
% +-----------------------------------------------------------------------
%
% Tot_Fertilizer_kg  % Total Fertilizer in profile [kg ha-1]
% Tot_Act_Inorg_kg   % Total Active inorganic in profile [kg ha-1]
% Tot_Slo_Inorg_kg   % Total Slow inorganic in profile [kg ha-1]
% Tot_Labile_kg      % Total Labile [includes Solution] in profile [kg ha-1]
% Tot_Solution_kg    % Total Solution [included in Labile] in profile [kg ha-1]
% Tot_Act_Org_kg     % Total Active organic in profile [kg ha-1]
% Tot_Slo_Org_kg     % Total Slow organic in profile [kg ha-1]
% Tot_Res_Org_kg     % Total Resistant organic in profile [kg ha-1]
% Tot_Rapid_FOM_kg   % Total Rapid decomposing FOM in profile [kg ha-1]
% Tot_Inter_FOM_kg   % Total Intermediate decomposing FOM in profile [kg ha-1]
% Tot_Slow_FOM_kg    % Total Slow decomposing FOM in profile [kg ha-1]
% Tot_Atm_Loss_kg    % Total loss from profile to atmosphere [kg ha-1]
% Tot_Leaching_kg    % Total loss from profile to leaching [kg ha-1]
% Tot_Uptake_kg      % Total uptake from live plants [kg ha-1]
% Tot_Vol_N_kg       % Total volatile N [kg ha-1]
% TotMass_kg         % local accumulator of total mass [kg ha-1]

[zerosGrid] = data_controller('structure','pass','zeros_grid');
[ISwPho] = data_controller('salus','pass','ISwPho');

%%C
% calculate total chemical mass (kg ha-1) at the end of the day:
%Sum down layers
Tot_Fertilizer_kg = cumsum(C.Fertilizer_kg_kg,2);
Tot_Act_Inorg_kg = cumsum(C.Act_Inorg_kg_kg,2);
Tot_Slo_Inorg_kg = cumsum(C.Slo_Inorg_kg,2);
Tot_Act_Org_kg = cumsum(C.Act_Org_kg,2);
Tot_Slo_Org_kg = cumsum(C.Slo_Org_kg,2);
Tot_Res_Org_kg = cumsum(C.Res_Org_kg,2);
Tot_Rapid_FOM_kg = cumsum(C.Rapid_FOM_kg,2);
Tot_Inter_FOM_kg = cumsum(C.Inter_FOM_kg,2);
Tot_Slow_FOM_kg = cumsum(C.Slow_FOM_kg,2);
Tot_Atm_Loss_kg = cumsum(Dc.Act_Inorg_2_CO2_kg + Dc.Act_Org_2_CO2_kg + Dc.Slo_Org_2_CO2_kg + ...
        Dc.Res_Org_2_CO2_kg + Dc.Rapid_FOM_2_CO2_kg + Dc.Inter_FOM_2_CO2_kg + Dc.Slow_FOM_2_CO2_kg);

%Calculate the total
TotMass_kg = Tot_Fertilizer_kg + Tot_Act_Inorg_kg + Tot_Slo_Inorg_kg + ...
    Tot_Act_Org_kg + Tot_Slo_Org_kg + Tot_Res_Org_kg + ...
    Tot_Rapid_FOM_kg + Tot_Inter_FOM_kg + Tot_Slow_FOM_kg;

%Now check the balance
err_val = C.TotMassYest_kg + FOM_C + Fertilizer_C - TotMass_kg - Tot_Atm_Loss_kg;
if any(abs(err_val) > 0.001)
    warning_LHM(['Soil C Mass Balance error: ',err_val, ' [kg ha-1] [Soil_Biochemistry_MassBalance]']);
    MassError = true;
end

%Save this as the previous days' mass
C.TotMassYest_kg = TotMass_kg;


%%N
%Clear for N
clear Tot*

% calculate total chemical mass (kg ha-1) at the end of the day:
Tot_Fertilizer_kg = cumsum(n.Fertilizer_kg,2);
Tot_Labile_kg = cumsum(n.Labile_kg,2);
%Tot_Solution_kg = cumsum(n.Solution_kg,2); %ADK: Included in Labile
Tot_Act_Org_kg = cumsum(n.Act_Org_kg,2);
Tot_Slo_Org_kg = cumsum(n.Slo_Org_kg,2);
Tot_Res_Org_kg = cumsum(n.Res_Org_kg,2);
Tot_Rapid_FOM_kg = cumsum(n.Rapid_FOM_kg,2);
Tot_Inter_FOM_kg = cumsum(n.Inter_FOM_kg,2);
Tot_Slow_FOM_kg = cumsum(n.Slow_FOM_kg,2);
Tot_Atm_Loss_kg = cumsum(DN.Labile_2_Atmosphere_kg,2);

Tot_Leaching_kg = DN.Labile_2_Leaching_kg(:,end);
Tot_Vol_N_kg = n.Vol_FOM_kg + n.Vol_Fert_kg;
Tot_Atm_Loss_kg = Tot_Atm_Loss_kg + DN.Vol_Fert_2_Atm_kg + DN.Vol_FOM_2_Atm_kg;

%Calculate the total
TotMass_kg = Tot_Fertilizer_kg + Tot_Labile_kg + Tot_Act_Org_kg + Tot_Slo_Org_kg + ...
    Tot_Res_Org_kg + Tot_Rapid_FOM_kg + Tot_Inter_FOM_kg + Tot_Slow_FOM_kg + ...
    Tot_Vol_N_kg + n.Live_kg;

%Add seed nutrients to total
indEmerge = (DAE == 1);
if any(indEmerge)% the very first plant nutrient is added from the seed at emergence
    n.TotMassYest_kg(indEmerge) = n.TotMassYest_kg(indEmerge) + n.Live_kg(indEmerge);
end

%Now check the balance
err_val = n.TotMassYest_kg + FOM_N + Fertilizer_N + DN.Fix_kg - ...
    TotMass_kg - Tot_Atm_Loss_kg - Tot_Leaching_kg - DN.Live_2_Harvest_kg;
if any(abs(err_val) > 0.0001)
    warning_LHM(['Soil N Mass Balance error: ',err_val, ' [kg ha-1] [Soil_Biochemistry_MassBalance]']);
    MassError = true;
end

%Save this as the previous days' mass
n.TotMassYest_kg = TotMass_kg;

%%P


if any(ISwPho)
    %Clear for P
    clear Tot*
    
    %Initialize
    [Tot_Fertilizer_kg, Tot_Act_Inorg_kg, Tot_Slo_Inorg_kg, Tot_Labile_kg, ...
        Tot_Act_Org_kg, Tot_Slo_Org_kg, Tot_Res_Org_kg, Tot_Rapid_FOM_kg, ...
        Tot_Inter_FOM_kg, Tot_Slow_FOM_kg] = deal(zerosGrid);

    % calculate total chemical mass (kg ha-1) at the end of the day:
    Tot_Fertilizer_kg(ISwPho) = cumsum(P.Fertilizer_kg(ISwPho,:),2);
    Tot_Act_Inorg_kg(ISwPho) = cumsum(P.Act_Inorg_kg(ISwPho,:),2);
    Tot_Slo_Inorg_kg(ISwPho) = cumsum(P.Slo_Inorg_kg(ISwPho,:),2);
    Tot_Labile_kg(ISwPho) = cumsum(P.Labile_kg(ISwPho,:),2);
    %Tot_Solution_kg(ISwPho) = cumsum(P.Solution_kg(ISwPho,:),2); %ADK: Included in Labile
    Tot_Act_Org_kg(ISwPho) = cumsum(P.Act_Org_kg(ISwPho,:),2);
    Tot_Slo_Org_kg(ISwPho) = cumsum(P.Slo_Org_kg(ISwPho,:),2);
    Tot_Res_Org_kg(ISwPho) = cumsum(P.Res_Org_kg(ISwPho,:),2);
    Tot_Rapid_FOM_kg(ISwPho) = cumsum(P.Rapid_FOM_kg(ISwPho,:),2);
    Tot_Inter_FOM_kg(ISwPho) = cumsum(P.Inter_FOM_kg(ISwPho,:),2);
    Tot_Slow_FOM_kg(ISwPho) = cumsum(P.Slow_FOM_kg(ISwPho,:),2);
        
    %Calculate the total
    TotMass_kg = Tot_Fertilizer_kg + Tot_Act_Inorg_kg + Tot_Slo_Inorg_kg + Tot_Labile_kg + ...
        Tot_Act_Org_kg + Tot_Slo_Org_kg + Tot_Res_Org_kg + Tot_Rapid_FOM_kg + ...
        Tot_Inter_FOM_kg + Tot_Slow_FOM_kg + P.Live_kg;
    
    %Add seed nutrients to total
    indEmerge = (DAE == 1) & ISwPho;
    if any(indEmerge)% the very first plant nutrient is added from the seed at emergence
        P.TotMassYest_kg(indEmerge) = P.TotMassYest_kg(indEmerge) + P.Live_kg(indEmerge);
    end
    
    %Now check the balance
    err_val = P.TotMassYest_kg + FOM_P + Fertilizer_P - TotMass_kg - DP.Live_2_Harvest_kg;
    if any(abs(err_val) > 0.0001)
        warning_LHM(['Soil P Mass Balance error: ',err_val, ' [kg ha-1] [Soil_Biochemistry_MassBalance]'])
        MassError = true;
    end
    
    %Save this as the previous days' mass
    P.TotMassYest_kg = TotMass_kg;
end % end if ISwPho

end