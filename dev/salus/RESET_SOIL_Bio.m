function [C,n,P,csoil_bio,result] = RESET_SOIL_Bio(Soil,C,n,P,csoil_bio)
% function [] = RESET_SOIL_Bio(ByRef Soil As SOIL_TYPE, ByRef C As Soil_Bio_Element_Type, ...
%                ByRef n As Soil_Bio_Element_Type, ByRef P As Soil_Bio_Element_Type, ...
%                ByRef csoil_bio As Soil_Bio_Type, Optional result As Boolean)

%* RESET_SOIL_Bio : Resets soil parameters (soil must be read-in when called)
%*                  Called once at beginning of run
%*                  Based on fortran77 SOILINIT subroutine
%*
%               Modified:  01-June-1998, B.D. Baer and A. Gerakis
%                          Removed the INIT_DLAYR variable and corrected
%                          the soil water initialization
%  Modified by aris gerakis on 12 jul 1999:  Translated to Visual Basic.

[USDATexture] = data_controller('params','pass','soil_USDA_Texture');
[zerosGrid] = data_controller('structure','pass','zeros_grid');



%CONSTANTS
%ADK: Move elsewhere?
% Convert % to mg kg-1 (ppm):
percent2ppm = 10000;


result = false;

% Dim orig_ZLAYR(0 : MAX_LAYER) As Single, STAND_ZLAYR(0 : MAX_LAYER) As Single, ...
%     Zlayr(0 : MAX_LAYER) As Single
% Dim l As Integer
% Dim err_str As String * 80, Texture As String * 42
% Dim Fraction_Act_Org_in_Tot_Org_C As Double
% Dim P_ActIno_ppm(0 : MAX_LAYER) As Double, P_SloIno_ppm(0 : MAX_LAYER) As Double, ...
%     P_Labile_ppm(0 : MAX_LAYER) As Double
    
err_str = '';

%  NOTE: this data for STAND_ZLYR works for MAX_LAYER > 7
%   the twenty nominal values are: (/0,2,7,15,26,40,57,77,100,125,150,175, &
%                                    200,225, 250,275,300,325,350,375,400/)

STAND_ZLAYR = [0,2,7,15,26,40,57,77];
% STAND_ZLAYR(0) = 0
% STAND_ZLAYR(1) = 2
% STAND_ZLAYR(2) = 7
% STAND_ZLAYR(3) = 15
% STAND_ZLAYR(4) = 26
% STAND_ZLAYR(5) = 40
% STAND_ZLAYR(6) = 57
% STAND_ZLAYR(7) = 77
for l = 1 : MAX_LAYER - 7
   STAND_ZLAYR(l + 7) = 75 + 25 * l;
end

% Copy zlayr and dlayr values from soil structure to local arrays

% Set up new DLAYR and ZLAYR values by calling LYRSET

Soil.DepMax = Soil.orig_ZLAYR(Soil.ORIG_NLAYR);

%ADK: This function needs to be revised, it's currently within WATERBAL
[Soil.Zlayr, Soil.dlayr, Soil.nlayr] = LYRSET(MAX_LAYER, STAND_ZLAYR, Soil.DepMax);

orig_ZLAYR = Soil.orig_ZLAYR(1:MAX_LAYER);
Zlayr = Soil.Zlayr(1:MAX_LAYER);

% Re-calculate Soil Biochemistry variables, using rescaled layer sizes:

% Redimension dynamic arrays:

% ReDim C.Fertilizer_kg(-1 : Soil.nlayr)
% ReDim C.Act_Inorg_kg(-1 : Soil.nlayr)
% ReDim C.Slo_Inorg_kg(-1 : Soil.nlayr)
% ReDim C.Act_Org_kg(-1 : Soil.nlayr)
% ReDim C.Slo_Org_kg(-1 : Soil.nlayr)
% ReDim C.Res_Org_kg(-1 : Soil.nlayr)
% ReDim C.Rapid_FOM_kg(-1 : Soil.nlayr)
% ReDim C.Inter_FOM_kg(-1 : Soil.nlayr)
% ReDim C.Slow_FOM_kg(-1 : Soil.nlayr)
% 
% ReDim n.Fertilizer_kg(-1 : Soil.nlayr)
% ReDim n.Labile_kg(-1 : Soil.nlayr)
% ReDim n.Solution_kg(-1 : Soil.nlayr)
% ReDim n.Act_Org_kg(-1 : Soil.nlayr)
% ReDim n.Slo_Org_kg(-1 : Soil.nlayr)
% ReDim n.Res_Org_kg(-1 : Soil.nlayr)
% ReDim n.Rapid_FOM_kg(-1 : Soil.nlayr)
% ReDim n.Inter_FOM_kg(-1 : Soil.nlayr)
% ReDim n.Slow_FOM_kg(-1 : Soil.nlayr)
% ReDim n.Uptake_kg(-1 : Soil.nlayr)
% 
% ReDim P.Fertilizer_kg(-1 : Soil.nlayr)
% ReDim P.Act_Inorg_kg(-1 : Soil.nlayr)
% ReDim P.Slo_Inorg_kg(-1 : Soil.nlayr)
% ReDim P.Labile_kg(-1 : Soil.nlayr)
% ReDim P.Solution_kg(-1 : Soil.nlayr)
% ReDim P.Act_Org_kg(-1 : Soil.nlayr)
% ReDim P.Slo_Org_kg(-1 : Soil.nlayr)
% ReDim P.Res_Org_kg(-1 : Soil.nlayr)
% ReDim P.Rapid_FOM_kg(-1 : Soil.nlayr)
% ReDim P.Inter_FOM_kg(-1 : Soil.nlayr)
% ReDim P.Slow_FOM_kg(-1 : Soil.nlayr)
% ReDim P.Uptake_kg(-1 : Soil.nlayr)
% 
% ReDim csoil_bio.depthFac(-1 : Soil.nlayr)
% ReDim csoil_bio.TillDecompFac(-1 : Soil.nlayr)
% ReDim csoil_bio.Settled_TillDecompFac(-1 : Soil.nlayr)
% ReDim csoil_bio.Tilled_TillDecompFac(-1 : Soil.nlayr)
% 
% % Redimension C/N, C/P ratios in the FOM pools, except those set as constants:
% 
% ReDim csoil_bio.C2N_Live(-1 : Soil.nlayr)
% ReDim csoil_bio.C2N_Slow_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.C2N_Inter_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.C2N_Rapid_FOM(-1 : Soil.nlayr)
% 
% ReDim csoil_bio.C2P_Live(-1 : Soil.nlayr)
% ReDim csoil_bio.C2P_Slow_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.C2P_Inter_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.C2P_Rapid_FOM(-1 : Soil.nlayr)
% 
% % Redimension Inverse of C/N, C/P ratios:
% 
% ReDim csoil_bio.N2C_Live(-1 : Soil.nlayr)
% ReDim csoil_bio.N2C_Slow_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.N2C_Inter_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.N2C_Rapid_FOM(-1 : Soil.nlayr)
% 
% ReDim csoil_bio.P2C_Live(-1 : Soil.nlayr)
% ReDim csoil_bio.P2C_Slow_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.P2C_Inter_FOM(-1 : Soil.nlayr)
% ReDim csoil_bio.P2C_Rapid_FOM(-1 : Soil.nlayr)
% 
% % Redimension factors that convert between mass and concentration:
% 
% ReDim csoil_bio.ppm2kg(-1 : Soil.nlayr)
% ReDim csoil_bio.kg2ppm(-1 : Soil.nlayr)

%P:
%ADK: This function needs to be revised, it's in WATERBAL
[Soil.nlayr, Zlayr, P_ActIno_ppm] = LMATCHD(Soil.ORIG_NLAYR, orig_ZLAYR, P.ORIG_Act_Inorg_ppm, true);
[Soil.nlayr, Zlayr, P_SloIno_ppm] = LMATCHD(Soil.ORIG_NLAYR, orig_ZLAYR, P.ORIG_Slo_Inorg_ppm, true);
[Soil.nlayr, Zlayr, P_Labile_ppm] = LMATCHD(Soil.ORIG_NLAYR, orig_ZLAYR, P.ORIG_Labile_ppm, true);

[Soil.nlayr, Zlayr, csoil_bio.depthFac] = LMATCHD(Soil.ORIG_NLAYR, orig_ZLAYR, csoil_bio.ORIG_depthFac, true);

for l = 1 : Soil.nlayr
  
  % Initialize the soil biochemistry *layer* arrays that are *not* inputs:

  % Factors that convert between mass and concentration:

  csoil_bio.ppm2kg(l) = 0.1 * Soil.dlayr(l) * Soil.BD(l);
  if csoil_bio.ppm2kg(l) > 0
     csoil_bio.kg2ppm(l) = csoil_bio.ppm2kg(l) ^ -1;
  else
     csoil_bio.kg2ppm(l) = 0;
  end

  % C arrays:

  % Organic C:
  
  % First find the USDA textural class from ILHM's Integer USDA Textures
  indSand = ismember(USDATexture,[1,2]);
  indLoam = ismember(USDATexture,[3,4,5,7,8,9]);
  indSilt = ismember(USDATexture,6);
  indClay = ismember(USDATexture,[10,11,12]);
  
  %Assign USDA class into light, medium, or heavy soil group for Active C initialization:
  Fraction_Act_Org_in_Tot_Org_C = populate_array(zerosGrid,0.02,0.031,0.031,0.04,...
      indSand,indLoam,indSilt,indClay);
  
     
  C.Act_Org_kg(l) = Soil.OC(l) * percent2ppm * csoil_bio.ppm2kg(l) * ...
                    Fraction_Act_Org_in_Tot_Org_C;
  C.Res_Org_kg(l) = Soil.OC(l) * percent2ppm * csoil_bio.ppm2kg(l) * ...
                    Fraction_Res_Org_in_Tot_Org_C;
  C.Slo_Org_kg(l) = Soil.OC(l) * percent2ppm * csoil_bio.ppm2kg(l) * max(0, CDbl(1) - ...
                    Fraction_Act_Org_in_Tot_Org_C - Fraction_Res_Org_in_Tot_Org_C);
  % Inorganic C, i.e., free CaCO3:
  C.Act_Inorg_kg(l) = Soil.CaCO3(l) * percent2ppm * csoil_bio.ppm2kg(l) * ...
                      Fraction_Act_Inorg_in_Tot_Inorg_C;
  
  C.Slo_Inorg_kg(l) = Soil.CaCO3(l) * percent2ppm * csoil_bio.ppm2kg(l) * max(0, CDbl(1) - ...
                      Fraction_Act_Inorg_in_Tot_Inorg_C);

  % N arrays:

  n.Act_Org_kg(l) = C.Act_Org_kg(l) * N2C_Act_Org;
  n.Slo_Org_kg(l) = C.Slo_Org_kg(l) * N2C_Slo_Org;
  n.Res_Org_kg(l) = C.Res_Org_kg(l) * N2C_Res_Org;
  n.Labile_kg(l) = max(0.000001, Soil.TOTN(l) * percent2ppm * csoil_bio.ppm2kg(l) - ...
                   n.Act_Org_kg(l) - n.Slo_Org_kg(l) - n.Res_Org_kg(l));

  % P arrays:

  % Convert default soil inorganic concentrations to mass:
  
  P.Act_Inorg_kg(l) = max(0.000001, P_ActIno_ppm(l) * csoil_bio.ppm2kg(l));
  P.Slo_Inorg_kg(l) = max(0.000001, P_SloIno_ppm(l) * csoil_bio.ppm2kg(l));
  P.Labile_kg(l) = max(0.000001, P_Labile_ppm(l) * csoil_bio.ppm2kg(l));
  
  % Organic mass is based on P:C ratio:
  
  P.Act_Org_kg(l) = C.Act_Org_kg(l) * P2C_Act_Org;
  P.Slo_Org_kg(l) = C.Slo_Org_kg(l) * P2C_Slo_Org;
  P.Res_Org_kg(l) = C.Res_Org_kg(l) * P2C_Res_Org;

  % C/N, C/P ratios:
   
  csoil_bio.C2N_Live(l) = 0;
  csoil_bio.C2N_Slow_FOM(l) = 0;
  csoil_bio.C2N_Inter_FOM(l) = 0;
  csoil_bio.C2N_Rapid_FOM(l) = 0;

  csoil_bio.C2P_Live(l) = 0;
  csoil_bio.C2P_Slow_FOM(l) = 0;
  csoil_bio.C2P_Inter_FOM(l) = 0;
  csoil_bio.C2P_Rapid_FOM(l) = 0;

  % Inverse of C/N, C/P ratios:

  csoil_bio.N2C_Live(l) = 0;
  csoil_bio.N2C_Slow_FOM(l) = 0;
  csoil_bio.N2C_Inter_FOM(l) = 0;
  csoil_bio.N2C_Rapid_FOM(l) = 0;

  csoil_bio.P2C_Live(l) = 0;
  csoil_bio.P2C_Slow_FOM(l) = 0;
  csoil_bio.P2C_Inter_FOM(l) = 0;
  csoil_bio.P2C_Rapid_FOM(l) = 0;
    
  % Soil biochemistry pools tillage decomposition factor:
  
  csoil_bio.TillDecompFac(l) = 1;
  csoil_bio.Settled_TillDecompFac(l) = 1;
  csoil_bio.Tilled_TillDecompFac(l) = 1;

end

% Initialize the first two array elements that are above the soil:

for l = -1 : 0

    C.Fertilizer_kg(l) = 0;
    C.Act_Inorg_kg(l) = 0;
    C.Slo_Inorg_kg(l) = 0;
    C.Act_Org_kg(l) = 0;
    C.Slo_Org_kg(l) = 0;
    C.Res_Org_kg(l) = 0;
    C.Rapid_FOM_kg(l) = 0;
    C.Inter_FOM_kg(l) = 0;
    C.Slow_FOM_kg(l) = 0;

    n.Fertilizer_kg(l) = 0;
    n.Labile_kg(l) = 0;
    n.Solution_kg(l) = 0;
    n.Act_Org_kg(l) = 0;
    n.Slo_Org_kg(l) = 0;
    n.Res_Org_kg(l) = 0;
    n.Rapid_FOM_kg(l) = 0;
    n.Inter_FOM_kg(l) = 0;
    n.Slow_FOM_kg(l) = 0;
    n.Uptake_kg(l) = 0;

    P.Fertilizer_kg(l) = 0;
    P.Act_Inorg_kg(l) = 0;
    P.Slo_Inorg_kg(l) = 0;
    P.Labile_kg(l) = 0;
    P.Solution_kg(l) = 0;
    P.Act_Org_kg(l) = 0;
    P.Slo_Org_kg(l) = 0;
    P.Res_Org_kg(l) = 0;
    P.Rapid_FOM_kg(l) = 0;
    P.Inter_FOM_kg(l) = 0;
    P.Slow_FOM_kg(l) = 0;
    P.Uptake_kg(l) = 0;
    
    csoil_bio.depthFac(l) = 1;
    csoil_bio.TillDecompFac(l) = 1;
    csoil_bio.Settled_TillDecompFac(l) = 1;
    csoil_bio.Tilled_TillDecompFac(l) = 1;
    
    % C/N, C/P ratios:
     
    csoil_bio.C2N_Live(l) = 0;
    csoil_bio.C2N_Slow_FOM(l) = 0;
    csoil_bio.C2N_Inter_FOM(l) = 0;
    csoil_bio.C2N_Rapid_FOM(l) = 0;

    csoil_bio.C2P_Live(l) = 0;
    csoil_bio.C2P_Slow_FOM(l) = 0;
    csoil_bio.C2P_Inter_FOM(l) = 0;
    csoil_bio.C2P_Rapid_FOM(l) = 0;

    % Inverse of C/N, C/P ratios:

    csoil_bio.N2C_Live(l) = 0;
    csoil_bio.N2C_Slow_FOM(l) = 0;
    csoil_bio.N2C_Inter_FOM(l) = 0;
    csoil_bio.N2C_Rapid_FOM(l) = 0;

    csoil_bio.P2C_Live(l) = 0;
    csoil_bio.P2C_Slow_FOM(l) = 0;
    csoil_bio.P2C_Inter_FOM(l) = 0;
    csoil_bio.P2C_Rapid_FOM(l) = 0;

    csoil_bio.ppm2kg(l) = 0;
    csoil_bio.kg2ppm(l) = 0;
    
end
 
result = true;

end %RESET_SOIL_Bio