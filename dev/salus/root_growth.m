function [Root_Depth,Root_Front_Layer,Root_Lay_Wt,Root_Layer_Frac,Root_Presence] = ...
    root_growth(alyr,BD,dBiomassRoot,DepMax,dlayr,dTT,LL,...
    Root_Depth,Root_Front_Layer,Root_Lay_Wt,Roots_Grow_Down,SHF,ST,SW,Zlayr)
% +-----------------------------------------------------------------------
% | "New" root growth to replace the one from DSSAT 3.5.
% | Called by both the simple and the complex crop, therefore should exist
% | in its own source file.  Based on the stand-alone program ROOTALON.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 09 nov 1999, after JTR
% | Modified by: aris gerakis, 04 apr 2001
% +-----------------------------------------------------------------------

%*** CONSTANTS
%ADK: Move these elsewhere
Root_Depth_Inc_Per_DD = 0.1; % Root depth increment per degree day [cm]

%Fetch from data controller
[zerosGridSoils] = data_controller('structure','pass','zeros_grid_soil');


%  ******************** B E G I N   I N I T I A L I Z A T I O N *************************
%ADK: This needs to be integrated with ILHM's capabilities to have
%different exponential depth distributions, and with adjustable layer
%thicknesses
Root_Depth_Fac = [1,1,1,0.85,0.58,0.37,0.21,0.11,0.05,0.03,0.01,0.01];

%Want Root_Depth_Fac defined for each cell and layer
[numRows,numLay] = size(SHF);
rows = (1:numRows)';
lays = (1:numLay);
Root_Depth_Fac = repmat(Root_Depth_Fac,numRows,1);
%  ******************** E N D   I N I T I A L I Z A T I O N ****************************



%  ******************** R O O T   G R O W T H   M O D U L E ****************************

%Calculate the impeding factors for all layers
%Some of these can be calculated elsewhere, or already are
% Saturation stress as soon as water-filled porosity exceeds 80%:
Porosity = 1 - BD / Rock_Density;
Wat_Fill_Porosity = SW ./ Porosity;
Sat_fac = min(1, 5 * (1 - Wat_Fill_Porosity));

% Dry soil stress as soon as soil dries to lower limit plus 0.04 cm3 cm-3:
Dry_Fac = max(0, min(1, (SW - LL) * 1 / 0.04));

% Cold stress as soon as soil temperature drops to 4 C:
Cold_Fac = max(0, min(1, ST * 1 / 4));

% First step:  grow roots downwards.  The new depth that the roots reached
% today is a function of the thermal time that drives growth, and potentially
% impeding factors: Soil Hospitality Factor, saturated soil, dry soil,
% and low temperature.
indRootDown = Roots_Grow_Down;
if any(indRootDown)
    %First, get the indices of the front layer of roots
    indFront = sub2ind([numRows,numLay],rows(indRootDown),Root_Front_Layer(indRootDown));
    
    % Root depth increment for the day:
    Root_Depth_Inc = Root_Depth_Inc_Per_DD(indRootDown) .* dTT(indRootDown) .* ...
        min(cat(2,SHF(indFront), Sat_fac(indFront), Dry_Fac(indFront), Cold_Fac(indFront)),[],2);
    
    % Add to existing root depth but do not exceed soil depth:
    Root_Depth(indRootDown) = min(Root_Depth(indRootDown) + Root_Depth_Inc, DepMax(indRootDown));
    
    % Check if the roots have grown into the next (or subsequent) layer:
    testRootFront = repmat(Root_Depth(indRootDown),1,numLay) > Zlayr(indRootDown,:);
    Root_Front_Layer(indRootDown) = sum(testRootFront,2) + 1;
%ADK  Check that the code above works just like the code below
%     while Root_Depth > Zlayr(Root_Front_Layer)
%         Root_Front_Layer = Root_Front_Layer + 1;
%     end
    
end % Roots Grow Down


% Second step:  Calculate the root dry matter for each layer
% What is the fraction of root presence in each layer?
%Get indexes for Root_Presence calculations
laysMat = repmat(lays,numRows,1);
indBehindFront = (laysMat < Root_Front_Layer);
indAtFront = (laysMat == Root_Front_Layer);

%Fill Root_Presence, first ahead of and behind front
Root_Presence = zerosGridSoils; %for cells ahead of root front
Root_Presence(indBehindFront) = 1;

%Then, root presence at the front
rootDepthMat = repmat(Root_Depth,1,numLay);
Root_Presence(indAtFront) = (rootDepthMat(indAtFront) - alyr(indAtFront)) ./ dlayr(indAtFront);


%Now, calculate the relative root weight fraction
Root_Wt_Fac = Root_Depth_Fac .* Root_Presence .* SHF .* min(cat(3,Sat_fac, Dry_Fac, Cold_Fac),[],3);
Sum_of_Factors = sum(Root_Wt_Fac,2);

%Initialize mass fraction of roots in each layer
Root_Layer_Frac = zerosGridSoil;

%Now, grow roots
indFacPos = (Sum_Of_Factors > 0);
if any(indFacPos)
    %Determine the new root weight in each layer
    dRoot_layer_Frac = repmat(dBiomassRoot(indFacPos),1,numLay) .* Root_Wt_Fac(indFacPos,:) ./ ...
        repmat(Sum_of_Factors(indFacPos),1,numLay);
    
    %Add the new root weight to the existing root weight
    Root_Lay_Wt(indFacPos,:) = Root_Lay_Wt(indFacPos,:) + dRoot_layer_Frac;
    Sum_of_Weight = sum(Root_Lay_Wt,2);
    
    %Calculate the mass fraction of roots in each layer
    indSumPos = indFacPos & (Sum_of_Weight > 0);
    Root_Layer_Frac(indSumPos,:) = Root_Lay_Wt(indSumPos,:) ./ repmat(Sum_of_Weight(indSumPos),1,numLay);
end
    

% In case that on the first day of growth there is no root growth, Root_Layer_Frac
% must be initialized to something other than zero: - AG
indInit = (sum(Root_Layer_Frac,2) <= 0) & (Root_Depth > 0);
if any(indInit)
    Root_Layer_Frac(indInit,:) = dlayr(indInit,:) .* Root_Presence(indInit,:) ./ ...
        repmat(Root_Depth(indInit),1,numLay);
end

end % Root growth