% -------------------------------------------------------------------------------------------
%	Started Translation on 1/23/2012
%	Nflux is called in soil_biochemistry.m
%	Brad Garrod
%	Only minor changes were made to the code translating into Matlab format.  
%	All of the previous lines that needed to be changed were commented out instead of deleted
%	in the case that VB format was needed for reanalyzing or for reassurance.
%	
%	This might be able to be inserted into soil_biochemistry under Chem_Par since that is the
%	only place that uses this and is only used to access variables(?) within that function.
% -------------------------------------------------------------------------------------------
function [] = Nflux(dlayr, DUL, Flowd, FlowU, kg2ppm, ByVal, NNDown,...
	NNOut, SAT, spec, ST, SW)
	
	
% Attribute VB_Name = "N_Flux"

% Option Explicit

% Public Sub Nflux(ByRef dlayr() As Single, ByRef DUL() As Single, _
                  % ByRef FlowD() As Single, ByRef FlowU() As Single, _
                  % ByRef kg2ppm() As Double, ByVal nlayr As Integer, _
                  % ByRef NNDown() As Double, ByRef NNOut() As Double, ByRef SAT() As Single, _
                  % ByRef spec() As Double, ByRef ST() As Single, ByRef SW() As Single)

% '+-----------------------------------------------------------------------
% '| Downward and upward N movement in soil.
% '+-----------------------------------------------------------------------
% '| Created by:  aris gerakis - mar 1994
% '| Modified by: aris gerakis - 22 dec 1999:  Translated to VB
% '| Modified by: aris gerakis - 23 may 2000:  moved layerAbove & layerBelow
% '|              calculation outside the downward movement loop
% '| Reference:  Gerakis, A. 1994. Simulation and measurement of atrazine
% '|             and nitrate losses as influenced by water table management.
% '|             Ph. D. diss. Michigan State Univ., E. Lansing. 214 p.
% '+-----------------------------------------------------------------------

%Const DisDen As Double = 1.2  ' Dispersivity denominator subject to calibration
DisDen = 1.2;
%Const Factor2kg As Double = 0.1  ' convert mg/(Lcm) * cm2/d to kg/ha
Factor2kg = 0.1;
%Const MolVol As Double = 50#  ' Molal volume [cm3/g-mol]
MolVol = 50;
%Const Tort As Double = 0.6    ' Tortuosity of pores
Tort = 0.6;

% Dim layerAbove As Integer, layerBelow As Integer, thisLayer As Integer
% Dim D As Double, Disprs As Double, Dm As Double, Dmax As Double, _
    % DmSE As Double, DmWC As Double, err_val As Double, Fast As Double, OutND As Double, _
    % OutNP As Double, OutNU As Double, Slow As Double, sppm(1 To MAX_LAYER) As Double, _
    % Tot_N_kg_After As Double, Tot_N_kg_Before As Double, Viscos As Double
% Dim err_str As String
% Dim MassError As Boolean

%     Initialize local variables:

%D = 0# ' Dispersion coefficient
D = 0;
%Disprs = 0# ' Dispersivity
Disprs = 0;
%Dm = 0# ' Molecular diffusion coefficient
Dm = 0;
%Dmax = 0# ' Max. drainage
Dmax = 0;
%DmSE = 0# ' Molecular diffusion coefficient
DmSE = 0;
%DmWC = 0# ' Molecular diffusion coefficient
DmWC = 0;
%Fast = 0# ' Fast flow
Fast = 0;
%Slow = 0# ' Slow flow
Slow = 0;
%OutND = 0# ' N out of layer down
OutND = 0;
%OutNP = 0# ' N out of previous layer down
OutNP = 0;
%OutNU = 0# ' N out of layer up
OutNU = 0;
%Viscos = 0.01 ' Viscosity
Viscos = 0.01;

%' Save total N for mass balance check:

Tot_N_kg_Before = sumd(spec(), 1, nlayr) % not sure exactly what sumd does, need to look into

%'     Leaching

%'     OutND is the amount leaving layer downward
                                                                                
%For thisLayer = 1 To nlayr
For thisLayer = 1:nlayr  
   %OutNP = max(0#, OutND)  ' whatever leached out of the previous layer
   OutNP = max(0, OutND);
   
   layerAbove = thisLayer - 1;
   layerBelow = thisLayer + 1;

   %If FlowD(thisLayer) > 0# And spec(thisLayer) > Min_kg_in_any_pool Then
   If FlowD(thisLayer) > 0 && spec(thisLayer) > Min_kg_in_any_pool
   
      Dmax = (SAT(thisLayer) - DUL(thisLayer)) * dlayr(thisLayer); %'max drainage

      %'  Partition Flow into slow and fast

      %If FlowD(thisLayer) > Dmax Then
	  If FlowD(thisLayer) > Dmax
         Fast = FlowD(thisLayer) - Dmax;
         Slow = Dmax;
      Else
         %Fast = 0#
		 Fast = 0;
         Slow = FlowD(thisLayer);
      %End If
	  End

      %'  Molecular diffusion coefficient as a f(T[K], viscosity[poise],
      %'  molar volume of solute[cm3/g-mol]) (Wilke & Chang, quoted in
      %'  Satterfield, 1970)

      Viscos = ST(thisLayer) * (-0.000215) + 0.0148;  %' in poise (Marshall & Holmes)
      %DmWC = 0.00000000506 * (ST(thisLayer) + 273#) / (Viscos * MolVol ^ 0.6)
	  DmWC = 0.00000000506 * (ST(thisLayer) + 273) / (Viscos * MolVol ^ 0.6);

      %'  Molecular diffusion coefficient as a f(T[K], viscosity[poise],
      %'  molar volume of solute[cm3/g-mol]) (Stokes-Einstein, quoted in
      %'  Satterfield, 1970).  Take the minimum.

      %DmSE = 0.00000000105 * (ST(thisLayer) + 273#) / (Viscos * MolVol ^ 0.33)
	  DmSE = 0.00000000105 * (ST(thisLayer) + 273) / (Viscos * MolVol ^ 0.33);
      %Dm = min(DmWC, DmSE) * 3600# * 24#   ' convert to cm2/d
	  Dm = min(DmWC, DmSE) * 3600 * 24   %' convert to cm2/d

      %'  Acc. to Wagenet & Rao, disp. is scale dependent.  Enfield & Yates
      %'  fit disp. values from 2e-1 to 2e+2 cm. More like a fitting parameter.

      %If thisLayer < nlayr Then
	  If thisLayer < nlayr
         Disprs = (dlayr(thisLayer) + dlayr(layerBelow)) / DisDen;
      Else
         %Disprs = dlayr(thisLayer) * 2# / DisDen
		 Disprs = dlayr(thisLayer) * 2 / DisDen;
      %End If
	  End

      %'  Dispersion coefficient as a f(Dm, dispersivity, flow velocity,
      %'  SW, tortuosity) (Marshall & Holmes, 1988).  Should be in the order of
      %'  0.3 cm2/d.

      D = Dm * Tort * SW(thisLayer) + Disprs * Fast / SW(thisLayer);

      sppm(thisLayer) = (spec(thisLayer) + OutNP) * kg2ppm(thisLayer);
   
      %If thisLayer < nlayr Then
	  If thisLayer < nlayr
         sppm(layerBelow) = spec(layerBelow) * kg2ppm(layerBelow);
      Else
         %sppm(layerBelow) = 0#
		 sppm(layerBelow = 0;
      %End If
	  End

      %'   Leach out:

      %OutND = (spec(thisLayer) + OutNP) * Slow / dlayr(thisLayer) - _
      %        Factor2kg * D * (sppm(layerBelow) - sppm(thisLayer)) / _
      %        ((dlayr(layerBelow) + dlayr(thisLayer)) / 2#)
	  OutND = (spec(thisLayer) + OutNP * Slow / dlayr(thisLayer) - ...
			   Factor2kg * D * (sppm(layerBelow) - sppm(thisLayer)) / ...
			   ((dlayr(LayerBelow) + dlayr(thisLayer)) / 2);

      %'  Can't leach the Min_kg_in_any_pool:

%'      If thisLayer > 1 And spec(thisLayer) <= spec(layerAbove) _
%'         And SW(thisLayer) <= DUL(thisLayer) Then
%'         OutND = max(0#, min(OutND, 0.9 * OutNP))
%'      Else
%         OutND = max(0#, min(OutND, spec(thisLayer) + OutNP - Min_kg_in_any_pool))
		OutND = max(0, min(OutND, spec(thisLayer) + OutNP - Min_kg_in_any_pool));
%'      End If

   Else %' if no downward flow
   
      %OutND = 0#
	  OutND = 0
   
   %End If ' downward flow
   End
   
   %'     Correct for any upward fluxes
   %'     OutNU is the amount leaving layer upward:
   %If thisLayer >= 2 And FlowU(layerAbove) > 0.000001 Then
   If thisLayer >= 2 && FlowU(layerAbove) > 0.000001
      %OutNU = max(0#, min((spec(thisLayer) + OutNP - OutND) * FlowU(layerAbove) / dlayr(thisLayer), _
      %        spec(thisLayer) + OutNP - OutND - Min_kg_in_any_pool))
	  OutNU = max(0, min((spec(thisLayer) + OutNP - OutND) * FlowU(layerAbove) / dlayr(thisLayer), ...
			  spec(thisLayer) + OutNP 0 OutND - Min_kg_in_any_pool));
      NNOut(layerAbove) = NNOut(layerAbove) - OutNU;
   Else
      %OutNU = 0#
	  OutNU = 0;
   %End If
   End
   
   NNDown(thisLayer) = OutND; %' Net N flowing downward
   NNOut(thisLayer) = OutND - OutNP + OutNU %' Net N out of this layer
    
% Next thisLayer
End


Tot_N_kg_After = sumd(spec(), 1, nlayr) - sumd(NNOut(), 1, nlayr)

err_val = Tot_N_kg_After + NNDown(nlayr) - Tot_N_kg_Before
%err_str = ""
err_str = ''

%If Abs(err_val) > 0.0001 Then
If abs(err_val) > 0.0001
  %err_str = "N mass balance error: " & Format(err_val, "##.######") & " [kg ha-1] [Nflux]"
  err_str = ['N mass balance error: ' err_val ' [kg ha-1] [NFlux]'];
  Call PRINTERR(WARNING, err_str);
  MassError = True;
%End If
End

%End Sub ' Nflux
End

