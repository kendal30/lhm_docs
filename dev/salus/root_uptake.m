function [SwdifR,Roots_Not_Activated] = root_uptake(dlayr,LL,Root_Presence,Roots_Not_Activated,SHF,SW)
% +-----------------------------------------------------------------------
% | "New" root water uptake to replace the one from DSSAT 3.5.
% | Called by both the simple and the complex crop, therefore should exist
% | in its own source file.  Based on the stand-alone program ROOTALON.
% | Must be called before WATBAL in order to calculate SwdifR.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 09 nov 1999, after JTR
% | Modified by: aris gerakis, 04 apr 2001
% +-----------------------------------------------------------------------

%ADK: Move this elsewhere?
SHF_scale_factor = 0.1;      % Scale-down factor for soil hospitality factor

%Fetch from data controller
[zerosGridSoils] = data_controller('structure','pass','zeros_grid_soil');


% Calculate coefficient for water extraction, extract water:
Scaled_SHF = SHF * SHF_scale_factor;

% To calculate the factor for the upper layers SW, first calculate
% PESW of soil above in cm, then check if roots have been activated.
% Roots are activated when the factor PESW_above_Fac reaches 1, which
% means the PESW in layers above has dropped below 7 cm.

PESW = max(0,SW - LL);
PESW_above_cm = cumsum(PESW .* dlayr,2);

%Now, calculate the factor of extraction based on PESW above the layer
PESW_above_Fac = zerosGridSoils;
indNotActive = Roots_Not_Activated;
if any(indNotActive(:))
    PESW_above_Fac(indNotActive) = max(0, exp(3.7 - 0.52 * PESW_above_cm(indNotActive)) - 0.07);
    
    indNewActive = indNotActive & (PESW_above_Fac >=1);
    if any(indNewActive(:))
        PESW_above_Fac(indNewActive) = 1;
        Roots_Not_Activated(indNewActive) = false;
    end
end
indActive = ~indNotActive;
if any(indActive(:))
    PESW_above_Fac(indActive) = min(1, max(0, -exp(-4.65 + 0.4 * PESW_above_cm(indActive)) + 1.16));
end

%Now, calculate the coefficient of water extraction
Coeff_Wat_Extr = Scaled_SHF .* Root_Presence .* PESW_above_Fac;

% Difference in soil water due to root extraction:
SwdifR = Coeff_Wat_Extr .* PESW; % per day per layer in cm3 cm-3

end % Root uptake