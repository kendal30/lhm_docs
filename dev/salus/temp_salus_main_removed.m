Public Sub ShowExpResults(RdbFile As String, xdbFile As String, result, ByRef Exps() As Variant)
% On Local Error GoTo errmsg
% check if already shown...
result = False

%ADK Dim ExpID As Double
%ADK Dim ExpTitle As String
%ADK Dim fcap As String

ExpID = Exps(0, 0)
ExpTitle = Exps(1, 0)

GoSub FindForm

if Not FrmFnd%
    % use experiment title for the form caption
    fcap = 'Results: ' + ExpTitle
    %ADK Dim nForm As New frmResults
    nForm.Caption = fcap
    nForm.Show
    nForm.ZOrder 0
    DoEvents
    
    nForm.lblRdbFile = RdbFile
    nForm.lblXdbFile = xdbFile
    nForm.lblExpID = Trim(Str(ExpID))
    GoSub FindForm
else
    if Val(Forms(fnum).lblExpID) <> ExpID
        Forms(fnum).lblExpID = Str(ExpID)
    end
end

% set expfile and expid, see change event in lblexpid for setup code
if Forms(fnum).ListExpID.ListCount <= 0
    % add the experiment
    lbr = LBound(Exps, 2)
    ubr = UBound(Exps, 2)
    %ADK Dim node() As node
    %ADK Dim q As Integer
    for T = lbr : ubr
        if (IsEmpty(Exps(1, T)) = False)
            q = Forms(fnum).trvinfo.Nodes.count + 1
            %ADK ReDim node(q)
            %ADK Set node(q) = Forms(fnum).trvinfo.Nodes.add(Forms(fnum).trvinfo.Nodes(1), tvwChild, CStr(Exps(0, T)), , 2)
            % add it
            % node(q).Text = Trim(Str(Forms(fnum).trvinfo.Nodes(2).Children + 1)) + ': ' + Exps(1, T)%Joshua and Bruno, graph number not working
            node(q).Text = Str(T + 1) + ': ' + Exps(1, T)
            Forms(fnum).trvinfo.Nodes(1).Expanded = True
            Forms(fnum).ListExpID.AddItem Trim(Str(Exps(0, T)))
            Forms(fnum).ListXdbFile.AddItem xdbFile
            [DUMMY] = Forms(fnum).AddMsdData(xdbFile, CDbl(Exps(0, T)))
            
            % reset ...
            [DUMMY] = Forms(fnum).lblExpID_Change
        end
    end
end

%  if Forms(fnum%).ListY.ListCount > 0
%    Forms(fnum%).ListYLong.SetFocus
%  end

result = True
DoingSetup = False
Screen.MousePointer = 0

return


FindForm:
FrmFnd = False
for F = 0 : Forms.count - 1
    if Forms(F).Tag = 'frmResults'
        if Val(Forms(F).lblExpID) = ExpID && Forms(F).lblRdbFile = RdbFile
            FrmFnd = True
            fnum = F
            if Forms(fnum).WindowState <> 0
                Forms(fnum).WindowState = 0
            end
            Forms(fnum).ZOrder 0
            Forms(fnum).Show
            Exit For
        end
    end
end
return

errmsg:

MsgBox 'An Error has occurred.' & ' - ' & Err.Number & ' - ' & Err.Description

end


Public Sub SetWeatherData(RsWth As ADODB.Recordset, RsWthHour As ADODB.Recordset, WTH As WTH_TYPE, ...
                          WthMod As WthModType, CurDate As DATE_TYPE, Tmin As Single, ...
                          Tmax As Single, Rain As Single, srad As Single, ...
                          PPrecip() As Single, hrlt As Single, CumeRain As Long, ...
                          FatalError As Boolean)

  Dim hour As Integer
  Dim WthFileDate As Date
  Dim DailyRain As Single
  
% checks weather for current day...
  if (RsWth('DOY') <> CurDate.DOY) Or (RsWth('YEAR') <> CurDate.year) Then
    [DUMMY] = PRINTERR(FATAL, 'Weather data not found for date =  ' + CurDate.DateStr)
    FatalError = True
    
  else
      % set weather variables
    if GetSingle(RsWth, 'TMIN') < -90 Then
      [DUMMY] = PRINTERR(FATAL, 'NULL value for minimum temperature, date =  ' + CurDate.DateStr)
    else
      Tmin = GetSingle(RsWth, 'TMIN')
    end
    
    if GetSingle(RsWth, 'TMAX') < -90 Then
      [DUMMY] = PRINTERR(FATAL, 'NULL value for maximum temperature,  date =  ' + CurDate.DateStr)
    else
      Tmax = GetSingle(RsWth, 'TMAX')
    end
    
    if GetSingle(RsWth, 'RAIN') < 0 Then
      [DUMMY] = PRINTERR(WARNING, 'Negative value (' + Format(RsWth('RAIN'), '0.0') ...
           + ') for precipitation, date =  ' + CurDate.DateStr)
      Rain = 0
    else
      Rain = GetSingle(RsWth, 'RAIN') * mm_to_cm
      CumeRain = CumeRain + RsWth('Rain')    % add cumulative rain, write to output
    end

    if GetSingle(RsWth, 'SRAD') < 0 Then
      [DUMMY] = PRINTERR(WARNING, 'Negative value (' + Format(RsWth('SRAD'), '0.0') ...
           + ') for solar radiation, date =  ' + CurDate.DateStr)
      srad = 1
    else
      srad = GetSingle(RsWth, 'SRAD')
    end
  end

  if (WTH.THERE_IS_RAIN_FILE) Then
    for hour = 1 : 24
      PPrecip(hour) = 0
    Next hour
    DailyRain = 0
    
    WthFileDate = DateSerial(GetInt(RsWthHour, 'Year'), 1, GetInt(RsWthHour, 'DOY'))
    While (CurDate.JulDate = WthFileDate) && (Not RsWthHour.EOF)
      PPrecip(GetInt(RsWthHour, 'Hour')) = GetSingle(RsWthHour, 'HrPrecip') * mm_to_cm
      DailyRain = DailyRain + PPrecip(GetInt(RsWthHour, 'Hour'))
      if Not RsWthHour.EOF Then
        RsWthHour.MoveNext
        WthFileDate = DateSerial(GetInt(RsWthHour, 'Year'), 1, GetInt(RsWthHour, 'DOY'))
      end
    Wend
    
    if (Abs(DailyRain - Rain) > 0.25) Then
      [DUMMY] = PRINTERR(WARNING, 'Hour & daily rain ' + ...
                    'differ by more than 2.5 mm on ' + CurDate.DateStr)
    end
    
  end

  if FatalError Then Exit Sub

  %   if necessary modify the weather

  if (WthMod.Mod_Flag) Then

    [DUMMY] = weather_mod(srad, WthMod.RADFAC, WthMod.RADADJ)
    [DUMMY] = weather_mod(Tmax, WthMod.TXFAC, WthMod.TXADJ)
    [DUMMY] = weather_mod(Tmin, WthMod.TNFAC, WthMod.TNADJ)

    % if the user wants to Add, Subtract or Replace existing precipitation
    % values the factor must be converted to cm

    if (UpCase(WthMod.PRCFAC) = 'A') Or (UpCase(WthMod.PRCFAC) = 'R') Or ...
       (UpCase(WthMod.PRCFAC) = 'S') Then
      [DUMMY] = weather_mod(Rain, WthMod.PRCFAC, (WthMod.PRCADJ * mm_to_cm))
    else
      [DUMMY] = weather_mod(Rain, WthMod.PRCFAC, WthMod.PRCADJ)
    end

    % Modify hourly rain as well:
    if (WTH.THERE_IS_RAIN_FILE) Then
      for i = 1 : 24
      
        % divide PRFac by 24 so that Additve, subtractive and substitution make sense
        
        if ((UpCase(WthMod.PRCFAC) = 'A') Or (UpCase(WthMod.PRCFAC) = 'S') ...
             Or (UpCase(WthMod.PRCFAC) = 'R")) Then
          [DUMMY] = weather_mod(PPrecip(i), WthMod.PRCFAC, ...
                           (WthMod.PRCADJ * mm_to_cm / 24))
        else
          [DUMMY] = weather_mod(PPrecip(i), WthMod.PRCFAC, WthMod.PRCADJ)
        end
      Next i
    end
  end

  hrlt = GET_dayLength(WTH, CurDate, WthMod.Mod_Flag, WthMod.DAYFAC, ...
                        WthMod.DAYADJ)


End Sub


% Uncomment these lines when running random test routines:
    % Display % progress in status bar:
    %FrmStatus2.StatusBar.Value = (sim_loop / max_loops) * 100
    %pct = Trim(Str(Int((FrmStatus2.StatusBar.Value - FrmStatus2.StatusBar.min / ...
    %      FrmStatus2.StatusBar.max - FrmStatus2.StatusBar.min))))
    %FrmStatus2.lblPct = pct + '% completed.'
    %FrmStatus2.Refresh
    % End uncomment
    
    EndRun:
    
    Screen.MousePointer = 0
    
    if CancelRun
        FrmStatus.Caption = 'Status: Run cancelled before completion.'
    elseif FatalError
        FrmStatus.Caption = 'Status: Run terminated with fatal error.'
    else
        FrmStatus.Caption = 'Status: Run completed.'
    end
    
    EndTime = time
    DoingRun = False
    CancelRun = False
    FrmStatus.CmdCancelRun.Enabled = False
    FrmStatus.cmdViewResults.Enabled = True
    FrmStatus.cmdPrint.Enabled = True
    FrmStatus.cmdSaveAs.Enabled = True
    FrmStatus.cmdClose.Enabled = True
    FrmStatus.Show
    
    [DUMMY] = FrmStatus.EnableForms    % enable all forms
    FrmStatus.SetFocus
    FrmStatus.cmdViewResults.SetFocus
    
    % show total time for the run ... (min, seconds..)
    NumSeconds = DateDiff('s', StartTime, EndTime)
    NumHours = Int(NumSeconds / 3600)
    
    temp = ''
    if NumHours > 0
        if NumHours = 1
            temp = Trim(Str(NumHours)) & ' hour,'
        else
            temp = Trim(Str(NumHours)) & ' hours,'
        end
        NumSeconds = NumSeconds - (NumHours * 3600)
    end
    
    NumMinutes = Int(NumSeconds / 60)
    if NumMinutes > 0
        if NumMinutes = 1
            temp = RTrim(temp) & BlSp & Trim(Str(NumMinutes)) & ' minute,'
        else
            temp = RTrim(temp) & BlSp & Trim(Str(NumMinutes)) & ' minutes,'
        end
        NumSeconds = NumSeconds - (NumMinutes * 60)
    end
    
    if NumSeconds >= 0
        if NumSeconds = 1
            temp = RTrim(temp) & BlSp & Trim(Str(NumSeconds)) & ' second'
        else
            temp = RTrim(temp) & BlSp & Trim(Str(NumSeconds)) & ' seconds'
        end
    end
    
    FrmStatus.lblTime = 'Total Time: ' & RTrim(temp)
    
    msg = ''
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
    
    if FatalError
        msg = 'Run terminated due to fatal error: ' & Str(time) & ', ' & Str(Date)
        FatalError = False
    else
        msg = 'Run completed: ' & Str(time) & ', ' & Str(Date)
    end
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
    
    msg = 'Total run time: ' & RTrim(temp)
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
    msg = 'Number of warnings: ' & FrmStatus.lblNumWarnings
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
    msg = 'Number of errors: ' & FrmStatus.lblNumErrors
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
    msg = ''
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
    
    % Uncomment these lines when running random test routines:
    %Next sim_loop % finished a 1000-d run, start another one
    % End uncomment
    
    return
    
    % Uncomment these lines when running random test routines:
    %Cancel:
    %Unload frmMDI
    %Unload FrmStatus
    %Unload FrmStatus2
    % End uncomment
    
    
    Public Sub DeleteCurrentResults(ConRdb As ADODB.Connection, _
     RsOut As ADODB.Recordset, RsOut2 As ADODB.Recordset, _
     rsoutlay As ADODB.Recordset, ExpID As Double)
  
 Dim result As Boolean
  
  sql = "Select * from [Results] WHERE [ExpID] = " + Str(ExpID)
  RsOut.Open sql, ConRdb, adOpenDynamic, adLockOptimistic, adCmdText
'  Call ADO_DeleteRecords(RsOut, result)
'
'
  sql = "Select * from [Results_Layer] WHERE [ExpID] = " + Str(ExpID)
  rsoutlay.Open sql, ConRdb, adOpenDynamic, adLockOptimistic, adCmdText
'  Do
'  Call ADO_DeleteRecords(rsoutlay, result)
'  Loop While result = False
'
'
  sql = "Select * from [Results_Seasonal] WHERE [ExpID] = " + Str(ExpID)
  RsOut2.Open sql, ConRdb, adOpenDynamic, adLockOptimistic, adCmdText
'  Call ADO_DeleteRecords(RsOut2, result)
'
'
'   If (Not result) Then
'    MsgBox "Could not delete previous layer results", vbOKOnly
'  End If

    sql = "delete * from [Results] WHERE [ExpID] = " + Str(ExpID)
    ConRdb.Execute sql

    sql = "delete * from [Results_Layer] WHERE [ExpID] = " + Str(ExpID)
    ConRdb.Execute sql

    sql = "delete * from [Results_Seasonal] WHERE [ExpID] = " + Str(ExpID)
    ConRdb.Execute sql

End Sub


Public Sub InitWeatherData(ConWth As ADODB.Connection, RsExp As ADODB.Recordset, _
                           WthStationID As String, WTH As WTH_TYPE, WthMod As WthModType, _
                           RunoffParm() As Single, FatalError As Boolean)

  ' (station level weather data ...)
  Dim sql As String
  Dim msg As String
  Dim RsWthStn As New ADODB.Recordset
  
  sql = "Select * from [Stations] WHERE [StationID] = '" + RsExp("StationID") + "'"
  
  RsWthStn.Open sql, ConWth, adOpenDynamic, adLockOptimistic, adCmdText
  If ADO_RecordCount(RsWthStn) = 0 Then
    msg = "*Weather station data not found: SQL = '" + sql + "'"
    Call PRINTERR(FATAL, msg)
    Call Printbox(True, msg)
    FatalError = True
    Exit Sub
  End If
  
  ' set weather variables...
  Call init_wth(WTH)
  Call InitWthMod(WthMod)
  
  WTH.xlat = RsWthStn("Lat")
  WTH.XLONG = RsWthStn("Long")
  WTH.tav = RsWthStn("tav")
  WTH.tamp = RsWthStn("Amp")
  WTH.ELEV = RsWthStn("Elev")
  WTH.Press = 0
  WthStationID = RsWthStn("StationID")
  RsWthStn.Close

  sql = "Select * from [HourlyRain] WHERE [StationID] = '" + _
        WthStationID + "'"
  RsWthStn.Open sql, ConWth, adOpenDynamic, adLockOptimistic, adCmdText
  If ADO_RecordCount(RsWthStn) > 0 Then
    WTH.THERE_IS_RAIN_FILE = True
    RsWthStn.Close
  Else
    WTH.THERE_IS_RAIN_FILE = False
    RsWthStn.Close
    
    sql = "Select * from [Storm_Intensity] WHERE [StationID] = '" + _
        WthStationID + "' ORDER BY [Month] ASC"
    RsWthStn.Open sql, ConWth, adOpenDynamic, adLockOptimistic, adCmdText
    If ADO_RecordCount(RsWthStn) = 0 Then
      msg = "Storm intensity values not found: SQL = '" + sql + _
            "'.  Default values will be used."
      Call PRINTERR(WARNING, msg)
      For i = 1 To UBound(RunoffParm)
        RunoffParm(i) = -0.1
      Next i
    Else
      i = 1
      Do Until RsWthStn.EOF Or i > UBound(RunoffParm)
        RunoffParm(i) = RsWthStn("SIValue")
        i = i + 1
        RsWthStn.MoveNext
      Loop
    End If
  End If
End Sub

Public Sub UnloadResultsForm(ExpID As Double, RdbFile$)

  For j = 0 To Forms.count - 1
    If Forms(j).Tag = "frmResults" Then
      If Val(Forms(j).lblExpID) = ExpID And Forms(j).lblRdbFile = RdbFile$ Then
        Unload Forms(j)
        Exit For
      End If
    End If
  Next j
  

End Sub



Public Sub CheckDataBases(wdbFile$, sdbfile$, cdbfile$, RdbFile$, FatalError As Boolean)

  If Not FileExists(wdbFile$) Then
    msg = "Weather database not found: '" + wdbFile$ + "'"
    Call PRINTERR(FATAL, msg)
    FatalError = True
  End If
  
  If Not FileExists(sdbfile$) Then
    msg = "Soil database not found: '" + sdbfile$ + "'"
    Call PRINTERR(FATAL, msg)
    FatalError = True
  End If
  
  If Not FileExists(cdbfile$) Then
    msg = "Crop database not found: '" + cdbfile$ + "'"
    Call PRINTERR(FATAL, msg)
    FatalError = True
  End If
  
  If Not FileExists(RdbFile$) Then
    msg = "Results database not found: '" + RdbFile$ + "'"
    Call PRINTERR(FATAL, msg)
    FatalError = True
  End If
  
  

End Sub



Private Sub SetupStatusForm(xdbFile$, RdbFile$, ExpID As Double, ExpTitle$)

  Load FrmStatus
  FrmStatus.CmdCancelRun.Caption = "Cancel Run"
  FrmStatus.CmdCancelRun.Enabled = True
  FrmStatus.cmdViewResults.Enabled = False
  FrmStatus.cmdPrint.Enabled = False
  FrmStatus.cmdSaveAs.Enabled = False
  FrmStatus.cmdClose.Enabled = False
  FrmStatus.Show
  FrmStatus.ZOrder 0
  FrmStatus.SetFocus
  DoEvents
  FrmStatus.lblNumErrors = "0"      ' reset to 0
  FrmStatus.lblNumWarnings = "0"
  Call FrmStatus.DisableForms    ' disable all forms except the status form ...
    
  ' set databases
  FrmStatus.lblXdbFile = xdbFile$
  FrmStatus.lblRdbFile = RdbFile$
  FrmStatus.lblExpID = Str(ExpID)
  
  FrmStatus.StatusBar.Min = 0
  FrmStatus.StatusBar.Max = 100
  FrmStatus.StatusBar.Value = 0
  
  FrmStatus.lblExpTitle = ExpTitle$
  Call FrmStatus.DisableForms
  
' Uncomment these lines when running random test routines:
'  Load FrmStatus2
'  FrmStatus2.Show
'  FrmStatus2.Caption = " Running test iterations for " + ExpTitle + "..."
' End uncomment

End Sub