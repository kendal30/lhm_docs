function [] = salus_main()
% Attribute VB_Name = "Sal_Main"

% SALUS model routines
% Public Sub Main()

% Startup code for SALUS
%ADK Dim Sysfile As String
Screen.MousePointer = 11

%ADK Load frmMDI
%ADK frmMDI.Show

% check for all system files here, set up data connections
Sysfile = MakeFullName(App.path, 'Salus.Gdb')

if FileExists(Sysfile)
    GdbFile = Sysfile    % set global variable
    
    %Load frmSetup
    
    Load frmOpenFile
    Screen.MousePointer = 0
    frmOpenFile.Show
    
    if frmOpenFile.SSTab1.Tab == 1 && frmOpenFile.ListRecent.ListCount > 0
        frmOpenFile.ListRecent.SetFocus
    end
    
    %initialize global variables for writing layer results
    
    curr_lay_dim = 0
    max_lay_dim = 0
    
else
    %ADK MsgBox 'System file not found: ' + Sysfile + ''.  Cannot continue.', vbExclamation
end

end



function [] = RunExp()
% Public Sub RunExp(RsExp As ADODB.Recordset, xdbFile, wdbFile, sdbfile, cdbfile, RdbFile, ...
%                   ExpID As Double, simresult As Boolean)
%{ added by iurii
bRedistributeFom = false
bResidue = false
%} added by iurii

% This is the main control for a SALUS run
% a simulation id is passed in, read records from the current sim file and
% loop through simulation, calling all appropriate modules

% Modified by B.D. Baer:  07-September-2006
%                         Added code to support changes in atmospheric CO2

%ADK Dim max_loops As Integer, sim_loop As Integer
%ADK Dim msg As String, temp As String * 80
%ADK Dim StartTime As Date
%ADK Dim i As Integer
%ADK Dim DoOutput As Boolean, result As Boolean

simresult = false
Screen.MousePointer = 11
DoingRun = true

[DUMMY] = SetupStatusForm(xdbFile, RdbFile, ExpID, RsExp('Title'))

% write messages to status form to indicate progress
msg = 'Experiment: ' + RsExp('Title')
[DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
msg = ''
[DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
msg = 'Run started: ' + Str(time) + ', ' + Str(Date)
StartTime = time    % store the start time for use later
[DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)

% Check for all databases -------------------------------------------
[DUMMY] = CheckDataBases(wdbFile, sdbfile, cdbfile, RdbFile, FatalError)
% if FatalError
%     return
% end
% if results form for current experiment is open, close before running
[DUMMY] = UnloadResultsForm(ExpID, RdbFile)

% BEGIN DIM Statements -------------------------------------------
%ADK Dim ConExp As New ADODB.Connection
%ADK Dim ConSoil As New ADODB.Connection
%ADK Dim ConWth As New ADODB.Connection
%ADK Dim ConCrop As New ADODB.Connection
%ADK Dim ConGlobal As New ADODB.Connection
%ADK Dim ConRdb As New ADODB.Connection

%ADK Dim RsComp As New ADODB.Recordset
%ADK Dim RsWth As New ADODB.Recordset
%ADK Dim RsWthHour As New ADODB.Recordset  % Hourly weather data
%ADK Dim RsWthStn As New ADODB.Recordset   % weather station data
%ADK Dim RsOut As New ADODB.Recordset     % the results recordset
%ADK Dim RsOut2 As New ADODB.Recordset     % the seasonal results recordset

%ADK Dim rsoutlay As New ADODB.Recordset   % the Layer results recordset
%ADK Dim RsTemp As New ADODB.Recordset

%plant records:
%ADK Dim RsCrop As New ADODB.Recordset %AG

% soil records
%ADK Dim RsBase As New ADODB.Recordset
%ADK Dim RsLayer As New ADODB.Recordset
%ADK Dim cSoil As SOIL_TYPE

% soil biochemistry records
%ADK Dim csoil_bio As Soil_Bio_Type
%ADK Dim C As Soil_Bio_Element_Type
%ADK Dim n As Soil_Bio_Element_Type
%ADK Dim P As Soil_Bio_Element_Type
%ADK Dim Dc As Soil_Bio_Delta_Element_Type
%ADK Dim DN As Soil_Bio_Delta_Element_Type
%ADK Dim DP As Soil_Bio_Delta_Element_Type
%ADK Dim Nitro As Plant_Nutrient_Type
%ADK Dim Phos As Plant_Nutrient_Type

% Variable switch for selecting crop model: %AG

%ADK Dim cropmod As String * 1

% Number of continuous days with crop stress:

%ADK Dim Cum_slow_dev_days As Integer

% Declare simple crop type:

%ADK Dim cCrop As CropSimp_Type  %AG

% Declare complex crop types and related variables:

%ADK Dim cSpe As SPECIES_TYPE
%ADK Dim cPhase_Tab() As Phase_Tab_Type
%ADK Dim cCult As Cult_Type
%ADK Dim vdays As Single
%ADK Dim cultivarID As String * 6, incg As String * 2, ...
varname As String * 16, varno As String * 6
%   Convert plant measures from g plant-1 d-1 (complex crop) to g m-2 d-1
%   (simple crop):
%ADK Dim gppl_to_gpm2 As Single, gpm2_to_gppl As Single

% Crop variables shared between the two crop models:

%ADK Dim DAE As Integer, LAI As Single, dBiomass As Single, ...
%    Root_Layer_Frac(0 : MAX_LAYER) As Single, ...
%    Harvest_Index As Single, Lag_Req_4_NFix_Is_Met As Boolean, ...
%    Live_Root_2_FOM_Frac As Single, Matured As Boolean, NFac As Single, ...
%    PFac As Single, Nfixing As String * 1, DAMat As Integer, ...
%    topWt As Single, rootWt As Single, Sen_rootWt As Single, grainWt As Single, ...
%    Root_Part_Coeff As Single, Root_Depth As Single, ...
%    Root_Front_Layer As Integer, Roots_Grow_Down As Boolean, RootC As Single, ...
%    RootSloC As Single, RootIntC As Single, ...
%    RootSloN As Single, VegC As Single, ...
%    VegSloC As Single, VegIntC As Single, ...
%    VegSloN As Single, rel_TT As Single, ...
%    C_to_Nod_Tot_kg As Double, Resp_Param_to_Fix_N As Double, ...
%    Species As String * 2

% Other local variables:

%ADK Dim CO2Mod As CO2ModType   %BDB
%ADK Dim DOY As Integer    % day of year
%ADK Dim cDay As Long
%ADK Dim CHeight As Single  %BDB
%ADK Dim CumeRain As Long
%ADK Dim Cum_rel_TT  As Single      % Cum. relative thermal time during rotation %AG
%ADK Dim PhotoSynID As String   %BDB
%ADK Dim DAP As Long
%ADK Dim BeginDate As DATE_TYPE
%ADK Dim EndDate As DATE_TYPE
%ADK Dim EndTime As Date
%ADK Dim LastYr As Long

%ADK Dim StartYr As Long
%ADK Dim EndYr As Long
%ADK Dim StartDOY As Long
%ADK Dim EndDOY As Long
%ADK Dim YearOffSet As Long
%ADK Dim VirtYear As Long

%ADK Dim WTH As WTH_TYPE
%ADK Dim WthMod As WthModType

%ADK Dim Ambient_Temperature As Single
%ADK Dim Td As Single, Tn As Single  %AG
%ADK Dim tamp As Single, tav As Single, xlat As Single   %AG
%ADK Dim Tmax As Single
%ADK Dim Tmin As Single
%ADK Dim srad As Single
%ADK Dim Rain As Single
%ADK Dim sldp As Single % soil depth   %AG
%ADK Dim hrlt As Single

%ADK Dim EList() As EventList
%ADK Dim DoHarvest As Boolean
%ADK Dim OutputInterval As Long
%ADK Dim IntervalCnt As Long
%ADK Dim NumHours As Long
%ADK Dim NumMinutes As Long
%ADK Dim NumSeconds As Long
%ADK Dim WthStationID As String

%ADK Dim err_val As Single
%ADK Dim PPrecip(1 : 24) As Single
%ADK Dim sfdrn As Single

%ADK Dim SwdifR(0 : MAX_LAYER) As Single % Soil water content difference resulting from root uptake [cm3 cm-3] %AG

% Declare management variables:

%ADK Dim Harvest_ByProd_fraction As Single
%ADK Dim Knock_Down_Fraction_Harv As Single % knock-down fraction of plant at harvest
%ADK Dim Knock_Down_Fraction_Till As Single % knock-down fraction of tillage implement

% Local soil biochemistry output variables
%ADK Dim Cum_C_CO2 As Double, Cum_C_In As Double, Cum_C_Out As Double, Cum_N_In As Double, ...
%    Cum_N_Out As Double, Cum_P_In As Double, Cum_P_Out As Double
% Total new Fertilizer today:
%ADK Dim newFert_kg_C As Double, newFert_kg_N As Double, ...
%    newFert_kg_P As Double
% Total new Residue FOM today:
%ADK Dim newResFOM_kg_C As Double, newResFOM_kg_N As Double, ...
%    newResFOM_kg_P As Double
% Total new Root FOM today:
%ADK Dim newRootFOM_kg_C As Double, newRootFOM_kg_N As Double, ...
%    newRootFOM_kg_P As Double
% Total new Knocked-down material FOM today:
%ADK Dim newKnockFOM_kg_C As Double, newKnockFOM_kg_N As Double, ...
%    newKnockFOM_kg_P As Double
% Total new Standing dead FOM today:
%ADK Dim newStandFOM_kg_C As Double, newStandFOM_kg_N As Double, ...
%    newStandFOM_kg_P As Double
% Total new FOM today:
%ADK Dim newFOM_kg_C As Double, newFOM_kg_N As Double, ...
%    newFOM_kg_P As Double

% END DIM Statements -------------------------------------------

% Uncomment these lines when running random test routines:
% Input how many iterations you want to run (default=1):
%On Local Error GoTo Cancel
%max_loops = InputBox('How many iterations?', 'SALUS', 1)
%On Local Error GoTo 0
%for sim_loop = 1 : max_loops
%FrmStatus2.Show
% End uncomment

% BEGIN initialize -------------------------------------------

Ambient_Temperature = 0
CancelRun = false
CHeight = 0
C_to_Nod_Tot_kg = 0
Cum_C_CO2 = 0
Cum_C_In = 0
Cum_C_Out = 0
Cum_N_In = 0
Cum_N_Out = 0
Cum_P_In = 0
Cum_P_Out = 0
Cum_slow_dev_days = 0
DAE = -1
DoHarvest = false
for layer = 1 : MAX_LAYER
    Root_Layer_Frac(layer) = 0
end
FatalError = false
gppl_to_gpm2 = 0
gpm2_to_gppl = 0
grainWt = 0
Harvest_Index = 0
Nfixing = 'N'
Lag_Req_4_NFix_Is_Met = false
LAI = 0
Live_Root_2_FOM_Frac = 0
MassError = false
Matured = false
DAMat = -99
newResFOM_kg_C = 0
newResFOM_kg_N = 0
newResFOM_kg_P = 0
newRootFOM_kg_C = 0
newRootFOM_kg_N = 0
newRootFOM_kg_P = 0
newKnockFOM_kg_C = 0
newKnockFOM_kg_N = 0
newKnockFOM_kg_P = 0
newStandFOM_kg_C = 0
newStandFOM_kg_N = 0
newStandFOM_kg_P = 0
newFOM_kg_C = 0
newFOM_kg_N = 0
newFOM_kg_P = 0
newFert_kg_C = 0
newFert_kg_N = 0
newFert_kg_P = 0
NFac = 1
PFac = 1
PhotoSynID = ''
Rain = 0
rel_TT = 0
Resp_Param_to_Fix_N = 0
topWt = 0
rel_TT = 0
rootWt = 0
Root_Part_Coeff = 0
Root_Depth = 0
Root_Front_Layer = 0
Roots_Grow_Down = false
RootC = 0
RootSloC = 0
RootIntC = 0
RootSloN = 0
VegC = 0
VegSloC = 0
VegIntC = 0
VegSloN = 0
Species = ''
Sen_rootWt = 0
vdays = 0
CurDate.DateStr = 'Setup' %used in error routines before start of run


% END initialize -------------------------------------------

% BEGIN Open Connections -------------------------------------------
% Open a connection using the Microsoft Jet provider.
%ADK ConExp.Open 'Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=' + xdbFile
%ADK ConSoil.Open 'Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=' + sdbfile
%ADK ConWth.Open 'Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=' + wdbFile
%ADK ConCrop.Open 'Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=' + cdbfile
%ADK ConGlobal.Open 'Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=' + GdbFile
%ADK ConRdb.Open 'Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;Data Source=' + RdbFile
% END Open Connections -------------------------------------------
%ConRdb.BeginTrans
% experiment information ---------------------------------------
% get output interval, set temporary variable
if ~IsNull(RsExp('FROP'))
    OutputInterval = RsExp('FROP')
else
    msg = 'Output interval not specified.'
    [DUMMY] = PRINTERR(FATAL, msg)
    FatalError = true
    return
end


%  Select Case Format(RsExp('ISimI'), '>')
%  Case 'S'

% specified start date... get year, doy
if IsNull(RsExp('SYear')) || IsNull(RsExp('SDOY')) || ...
        IsNull(RsExp('NYRS'))
    
    msg = 'Start date and/or number of years is missing.'
    [DUMMY] = PRINTERR(FATAL, msg)
    FatalError = true
    return
    
else
    StartYr = RsExp('SYear')
    StartDOY = RsExp('SDOY')
    EndYr = StartYr + RsExp('NYRS')
end

%  Case else
%    if ~IsNull(RsExp('ISimI'))
%      [DUMMY] = PRINTERR(FATAL, 'Start type ' + Trim(RsExp('ISimI')) + ...
%                    ' not supported yet.')
%    else
%      [DUMMY] = PRINTERR(FATAL, 'Variable ISimI not set')
%    end
%    StartYr = 1
%    StartDOY = 1
%    EndYr = 2
%  End Select


% Come up with a final estimate of the end DOY and year

if (StartDOY > 1)
    EndDOY = StartDOY - 1
else
    EndDOY = 365
    EndYr = EndYr - 1
end



% delete any current results for this experiment
[DUMMY] = DeleteCurrentResults(ConRdb, RsOut, RsOut2, rsoutlay, ExpID)

% Setup weather data  ------------------------------------------------
% runoff parameters for each month..
%ADK ReDim RunoffParm(1 : 12) As Single
[DUMMY] = InitWeatherData(ConWth, RsExp, WthStationID, WTH, WthMod, RunoffParm(), FatalError)
% if FatalError
%     return
% end

% Setup soil data ... -----------------------------------------------------
[DUMMY] = SetupSoil(ConSoil, RsBase, RsLayer, sdbfile, RsExp('soilID'), cSoil, result)

% Setup soil biochemistry:
[DUMMY] = Setup_Soil_Biochemistry(ConSoil, sdbfile, RsExp('soilID'), cSoil, C, n, P, ...
    Dc, DN, DP, csoil_bio)

% Uncomment these lines when running random test routines:
%[DUMMY] = Random_Soil(cSoil)
%[DUMMY] = Random_Soil_Biochemistry(C, n, P, csoil_bio, cSoil.nlayr)
% End uncomment

%  Reset soil temperature: %AG
[DUMMY] = RESET_ST(cSoil, WTH.tamp, WTH.tav, WTH.xlat, DOY)   %AG

% Reset tillage variables: %AG
[DUMMY] = RESET_TILL(cSoil, csoil_bio.TillDecompFac(), csoil_bio.Settled_TillDecompFac(), ...
    csoil_bio.Tilled_TillDecompFac())

% if available set SW, inorg. N, and P to initial conditions.
[DUMMY] = SetInitialConditions(ConExp, RsTemp, cSoil, C, n, P, Dc, DN, DP, csoil_bio, ...
    RsExp('ExpID'), ISwNit, sfdrn, newRootFOM_kg_C, ...
    newRootFOM_kg_N, newRootFOM_kg_P)

% BEGIN Read Rotation_Components -----------------------------------------------
%%%Debug.Print RsExp('expid')
[DUMMY] = SetRotationComponents(ConExp, RsComp, RsExp('ExpID'), FatalError)
% if FatalError
%     return
% end

% END Read Rotation_Components -----------------------------------------------


%BEGIN run setup ------------------------------------------------------------
% determine the start day and length of the run...

% for the moment assume that user will always choose a start date, later we
% may allow the use to use the soil analysis date, first planting date, etc

cDay = 1
CumeRain = 0
DAP = -1      % no planting yet
Cum_rel_TT = 0 %AG

% Set the current date
% Convert start and stop dates into DATE_TYPE stuctures
[DUMMY] = DOY2DateType(StartYr, StartDOY, BeginDate)
[DUMMY] = DOY2DateType(EndYr, EndDOY, EndDate)
CurDate = BeginDate


[DUMMY] = CalcVirtYear(ConExp, RsExp, RsComp, YearOffSet)
VirtYear = CurDate.year + YearOffSet

[DUMMY] = InitCO2Mod(CO2Mod, cdbfile) %BDB

[DUMMY] = Init_CropSimp(cCrop)

[DUMMY] = SetEventList(EList(), RsComp, ConExp, RsExp, YearOffSet)    % set event list for the first rotation component


FrmStatus.Caption = 'Status: Running ' + RsExp('Title') + '...'
if FrmStatus.ListEvents.ListCount > 0
    msg = ''
    [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
end

msg = CurDate.DateStr + ': Started component ' + GetString(RsComp, 'title')
[DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)

%END run setup ------------------------------------------------------------


%============================================================================
% BEGIN simulation run ------------------------------------------------------

% BEGIN set weather data -------------------
% the daily weather recordset for the simulation period...
%   sql = "Select * from [Weather] WHERE [StationID] = '" + WthStationID + ...
%     "' AND (([Year] = " + Str(StartYr) + " AND [DOY] >= " + Str(StartDOY) + ...
%     ") or ([Year] > " + Str(StartYr) + " and [Year] <= " + Str(EndYr) + ...
%     "))  ORDER BY [Year],[DOY] ASC"

%ADK RsWth.Open sql, ConWth, adOpenDynamic, adLockOptimistic, adCmdText
if ADO_RecordCount(RsWth) == 0
    msg = '*FATAL ERROR: Weather not found: SQL = '' + sql + ''.'
    [DUMMY] = Printbox(true, msg)
    FatalError = true
    return
end

if WTH.THERE_IS_RAIN_FILE
    %ADK     sql = "Select * from [HourlyRain] WHERE [StationID] = '" + WthStationID + ...
    %       "' AND (([Year] = " + Str(StartYr) + " AND [DOY] >= " + Str(StartDOY) + ...
    %       ") or ([Year] > " + Str(StartYr) + " and [Year] <= " + Str(EndYr) + ...
    %       "))  ORDER BY [Year],[DOY] ASC"
    
    %ADK RsWthHour.Open sql, ConWth, adOpenDynamic, adLockOptimistic, adCmdText
    
end
% END set weather data -------------------

CancelRun = false
DoOutput = false
IntervalCnt = OutputInterval - 1 % forces output on day 1


% BEGIN MAIN LOOP --------------------------------------------------->>>
while (DateDiff('d', CurDate.JulDate, EndDate.JulDate) > 0) && (~FatalError)
    
    FrmStatus.StatusBar.Value = (DateDiff('d', BeginDate.JulDate, CurDate.JulDate) ...
        / DateDiff('d', BeginDate.JulDate, EndDate.JulDate)) * 100
    
    pct = Trim(Str(Int((FrmStatus.StatusBar.Value - FrmStatus.StatusBar.Min / FrmStatus.StatusBar.Max - FrmStatus.StatusBar.Min)) + 1))
    FrmStatus.lblPct = pct + '% completed.'
    DoEvents
    
    % BEGIN output record setup -------------------------------
    if OutputInterval > 0
        IntervalCnt = IntervalCnt + 1
        
        if IntervalCnt == OutputInterval
            DoOutput = true
            IntervalCnt = 0
            RsOut.AddNew
            [DUMMY] = PutIn(RsOut, 'ExpID', RsExp('ExpID'), WARNING)
            [DUMMY] = PutIn(RsOut, 'RcID', RsComp('RcID'), WARNING)
            [DUMMY] = PutIn(RsOut, 'Year', CurDate.year, WARNING)
            [DUMMY] = PutIn(RsOut, 'DOY', CurDate.DOY, WARNING)
            [DUMMY] = PutIn(RsOut, 'cDay', cDay, WARNING)
            [DUMMY] = PutIn(RsOut, 'Date', CurDate.JulDate, WARNING)
            [DUMMY] = PutIn(RsOut, 'DAP', DAP, WARNING)
        else
            DoOutput = false
        end
    end
    % END output record setup -------------------------------
    
    
    %    % BEGIN set weather ------------------------------------->
    %    % set weather record set record for the current day ...
    %
    %    if (RsWth('DOY') < CurDate.DOY || RsWth('YEAR') < CurDate.Year)
    %      RsWth.MoveNext   % go to the next RECORD (day)
    %      cnt = cnt + 1
    %      if RsWth.EOF
    %         [DUMMY] = PRINTERR(WARNING, 'Weather file ended on: ' + CurDate.DateStr)
    %         break
    %      end
    %    end
    %
    %    [DUMMY] = SetWeatherData(RsWth, WTH, WthMod, CurDate, Tmin, Tmax, Rain, srad, ...
    %                        PPrecip(), hrlt, CumeRain, FatalError)
    %    if FatalError GoTo EndRun
    %
    %    % END set weather ------------------------------------->
    
    % BEGIN Check events ---------------------------------->
    [DUMMY] = AutoManage(ConExp, ConGlobal, RsExp, RsComp, cSoil, cropmod, cCrop, ...
        cSpe, cPhase_Tab(), cCult, Nitro, Phos, DAP, VirtYear, ...
        Matured, DAMat, DoHarvest, topWt, Harvest_Index, ...
        gppl_to_gpm2 * gpm2_to_kgpha, cdbfile, C, n, P, newFert_kg_C, newFert_kg_N, ...
        newFert_kg_P, csoil_bio, CurDate.DOY, BeginDate, newResFOM_kg_C, newResFOM_kg_N, ...
        newResFOM_kg_P)
    
    
    % Debug.Print 'Elist UBound:' & UBound(EList, 1)
    
    for evindex = 1 : UBound(EList, 1) %Step 1
        if ((EList(evindex).year == CurDate.year || EList(evindex).year <= 0) ...
                && EList(evindex).DOY == CurDate.DOY && EList(evindex).DOYFlg) ...
                || (EList(evindex).DAP >= 0 ...
                && EList(evindex).DAP == DAP && ~EList(evindex).DOYFlg)
            
            %Debug.Print 'evindex:' & evindex & ' eventtype=' & EList(evindex).EventType
            [DUMMY] = ProcessEvent(EList(evindex), ConExp, RsExp, RsComp, ...
                ConGlobal, DAP, cSoil, csoil_bio, cropmod, cCrop, ...
                cSpe, cPhase_Tab(), cCult, topWt, Harvest_Index, ...
                Nitro, Phos, VirtYear, DoHarvest, WthMod, CO2Mod, ...
                cdbfile, ...
                C, n, P, Dc, DN, DP, newFert_kg_C, newFert_kg_N, ...
                newFert_kg_P, newResFOM_kg_C, newResFOM_kg_N, ...
                newResFOM_kg_P)
            
        end
    end%
    
    % END Check events ---------------------------------->
    
    % BEGIN set weather ------------------------------------->
    % set weather record set record for the current day ...
    
    %     if (RsWth('DOY') < CurDate.DOY || RsWth('YEAR') < CurDate.year)
    %         RsWth.MoveNext   % go to the next RECORD (day)
    %         cnt = cnt + 1
    %         if RsWth.EOF
    %             [DUMMY] = PRINTERR(WARNING, 'Weather file ended')
    %             break
    %         end
    %     end
    %
    %     [DUMMY] = SetWeatherData(RsWth, RsWthHour, WTH, WthMod, CurDate, Tmin, Tmax, Rain, srad, ...
    %         PPrecip(), hrlt, CumeRain, FatalError)
    %
    
    %     if FatalError
    %         return
    %     end
    
    % END set weather ------------------------------------->
    
    % Uncomment these lines when running random test routines:
    %if DAP = 0
    %   [DUMMY] = Random_Management(cSoil, cCrop, cSpe, cCult, n, P, Nitro, Phos, cropmod, sfdrn)
    %end
    % End uncomment
    
    % BEGIN Run models ---------------------------------->
    
    
    % Crop and root modules:
    for layer = 0 : cSoil.nlayr
        SwdifR(layer) = 0;
    end
    
    % if simple crop
    
    if cropmod == 'S' && cCrop.isSown && cCrop.Plant_Pop > 0
        % Pass all the stress factors that affect crop:   %AG
        if ISwWat
            cCrop.droughtFac = cSoil.SWDF;  %AG
        else
            cCrop.droughtFac = 1;
        end
        [cCrop, Cum_slow_dev_days] = Crop_Simp(cCrop, Cum_slow_dev_days, srad, Tmax, Tmin, ...
            CO2Mod.Mod_Flag, CO2Mod.Effect_on_C3, CO2Mod.Effect_on_C4);  %AG
        LAI = cCrop.LAI;
        CHeight = cCrop.CHeight;
        PhotoSynID = cCrop.PhotoSynID;
        % Root growth and water uptake:
        if cCrop.DAE >= 1
            if cCrop.dTT > 0
                if LAI <= cCrop.LAI_max
                    Roots_Grow_Down = true; % roughly until flowering
                else
                    Roots_Grow_Down = false;
                end
                % 'New' root growth to replace the one from DSSAT 3.5:
                [cCrop.Root_Depth, cCrop.Root_Front_Layer, cCrop.Root_Lay_Wt, ...
                    cCrop.Root_Layer_Frac,cCrop.Root_Presence] = ...
                    root_growth(cSoil.alyr, cSoil.BD, cCrop.dBiomassRoot, cSoil.Zlayr(cSoil.nlayr),...
                    cSoil.dlayr, cCrop.dTT, cSoil.LL, cCrop.Root_Depth, ...
                    cCrop.Root_Front_Layer, cCrop.Root_Lay_Wt, Roots_Grow_Down, ...
                    cSoil.SHF, cSoil.ST, cSoil.SW, cSoil.Zlayr);
%                 [DUMMY] = RootGrowth(cSoil.alyr(), cSoil.BD(), cCrop.dBiomassRoot, ...
%                     cSoil.Zlayr(cSoil.nlayr), cSoil.dlayr(), cCrop.dTT, ...
%                     cSoil.LL(), cCrop.Root_Depth, cCrop.Root_Front_Layer, ...
%                     cCrop.Root_Lay_Wt(), cCrop.Root_Layer_Frac(), ...
%                     cCrop.Root_Presence(), Roots_Grow_Down, cSoil.SHF(), ...
%                     cSoil.ST(), cSoil.SW(), cSoil.Zlayr());
            end
            if ISwWat
                % 'New' root water uptake to replace the one from DSSAT 3.5:
                [SwdifR, cCrop.Roots_Not_Activated] = root_uptake(cSoil.dlayr, ...
                    cSoil.LL, cCrop.Root_Presence, cCrop.Roots_Not_Activated, ...
                    cSoil.SHF, cSoil.SW);
%                 [DUMMY] = RootUptake(cSoil.alyr(), cSoil.dlayr(), cSoil.LL(), cCrop.Root_Depth, ...
%                     cCrop.Root_Front_Layer, cCrop.Root_Presence(), ...
%                     cCrop.Roots_Not_Activated(), cSoil.SHF(), cSoil.SW(), SwdifR())
            end
        end
        % if complex crop
    elseif cropmod == 'C' && cCult.isSown && cCult.Plant_Pop > 0
        % Basic link from water balance:
        if ISwWat
            cCult.droughtFac = cSoil.SWDF
        else
            cCult.droughtFac = 1
        end
        [DUMMY] = Crop_Comp(cSpe, cCult, Nitro, Phos, cPhase_Tab(), Cum_slow_dev_days, hrlt, srad, ...
            Tmax, Tmin, Td, Tn, vdays, cSoil.ST(), CO2Mod)
        LAI = cCult.LAI
        CHeight = cSpe.CHeight
        PhotoSynID = cSpe.PhotoSynID
        % Root growth and water uptake:
        if cCult.gPhase > 0
            if cCult.dLeafEq > 0
                if cCult.gPhase <= 2 % until flowering
                    Roots_Grow_Down = true
                else
                    Roots_Grow_Down = false
                end
                % 'New' root growth to replace the one from DSSAT 3.5:
                [DUMMY] = RootGrowth(cSoil.alyr(), cSoil.BD(), ...
                    sum(cCult.dGrowRoot(), 1, 2) * gppl_to_gpm2, ...
                    cSoil.Zlayr(cSoil.nlayr), cSoil.dlayr(), ...
                    cCult.dLeafEq * cCult.phyll, cSoil.LL(), cCult.Root_Depth, ...
                    cCult.Root_Front_Layer, cCult.Root_Lay_Wt(), ...
                    cCult.Root_Layer_Frac(), cCult.Root_Presence(), Roots_Grow_Down, ...
                    cSoil.SHF(), cSoil.ST(), cSoil.SW(), cSoil.Zlayr())
            end
            if ISwWat
                % 'New' root water uptake to replace the one from DSSAT 3.5:
                [DUMMY] = RootUptake(cSoil.alyr(), cSoil.dlayr(), cSoil.LL(), cCult.Root_Depth, ...
                    cCult.Root_Front_Layer, cCult.Root_Presence(), ...
                    cCult.Roots_Not_Activated(), cSoil.SHF(), cSoil.SW(), SwdifR())
            end
        end
    end
    
    % Water balance:
    if ISwWat
        [DUMMY] = Watbal(RunoffParm(), CHeight, CO2Mod.CO2Level, cSoil, err_val, ...
            DAE, LAI, PhotoSynID, PPrecip(), WTH.Press, ...
            Rain, srad, sfdrn, SwdifR(), WTH.THERE_IS_RAIN_FILE, Tmax, ...
            Tmin, ...
            csoil_bio.TillDecompFac(), csoil_bio.Settled_TillDecompFac(), ...
            csoil_bio.Tilled_TillDecompFac())
    end
    
    % Calculate soil temperature:
    [DUMMY] = soiltemp(cSoil, CurDate.DOY, hrlt, WTH.xlat, Tmin, Tmax, srad, WTH.tamp, WTH.tav, Td, Tn)   %AG
    
    % Soil biochemistry:
    
    % Soil biochemistry must be called AFTER the crop module because it needs emergence
    % status maturity status, and the Root Partitioning Coefficient from crop:
    Ambient_Temperature = (Tmin + Tmax) / 2;
    
    % Crop inputs to the soil biochemistry module depend on which crop model
    % has been selected.  All plant mass must be g m-2 d-1:
    
    if cropmod == 'S' && cCrop.DAE >= 1 % if simple crop
        DAE = cCrop.DAE;
        dBiomass = max(0, cCrop.dBiomass);
        % Thermal time lag requirement for N fixation:
        if cCrop.TTtoMaturity > 0
            if (Upper(cCrop.Nfixing) == 'Y') && ...
                    (cCrop.rel_TT + cCrop.dTT / cCrop.TTtoMaturity > ...
                    cCrop.TTFix / cCrop.TTtoMaturity)
                Lag_Req_4_NFix_Is_Met = true;
            end
        end
        Live_Root_2_FOM_Frac = 0.001 * exp(4.6 * cCrop.rel_TT);
        NFac = cCrop.NFac;
        PFac = cCrop.PFac;
        Nfixing = cCrop.Nfixing;
        topWt = cCrop.topWt;
        rootWt = cCrop.rootWt;
        Sen_rootWt = 0;
        Harvest_Index = cCrop.Harvest_Index;
        Root_Part_Coeff = cCrop.Root_Part_Coeff;
        Root_Depth = cCrop.Root_Depth;
        Root_Front_Layer = cCrop.Root_Front_Layer;
        RootC = cCrop.RootC;
        RootSloC = cCrop.RootSloC;
        RootIntC = cCrop.RootIntC;
        RootSloN = cCrop.RootSloN;
        rel_TT = cCrop.rel_TT;
        C_to_Nod_Tot_kg = cCrop.C_to_Nod_Tot_kg;
        Resp_Param_to_Fix_N = cCrop.Resp_Param_to_Fix_N;
        for layer = 1 : cSoil.nlayr
            Root_Layer_Frac(layer) = cCrop.Root_Layer_Frac(layer);
        end
    elseif cropmod == 'C' && cCult.DAE >= 1 % if complex crop
        gppl_to_gpm2 = cCult.Plant_Pop
        if gppl_to_gpm2 > 0
            gpm2_to_gppl = gppl_to_gpm2 ^ -1
        end
        DAE = cCult.DAE
        dBiomass = max(0, (sum(cCult.dGrowLeaf(), 1, 2) + sum(cCult.dGrowStem(), 1, 2) + ...
            sum(cCult.dGrowKr(), 1, 2) + sum(cCult.dGrowRoot(), 1, 2) + ...
            sum(cCult.dReserves(), 1, 2)) * gppl_to_gpm2)
        % Rel. thermal time, thermal time lag requirement for N fixation:
        if cCult.gPhase == 1 || cCult.gPhase == 2
            if Upper(cSpe.Nfixing) == 'Y' && (cCult.leafEq + cCult.dLeafEq) * ...
                    cCult.phyll > cSpe.TTFix
                Lag_Req_4_NFix_Is_Met = true
            end
        else
            % Add +cCult.finleafEq because leafEq was zeroed at end of Phase 2:
            if Upper(cSpe.Nfixing) == 'Y' && (cCult.leafEq + cCult.FinLeafEq + ...
                    cCult.dLeafEq) * cCult.phyll > cSpe.TTFix
                Lag_Req_4_NFix_Is_Met = true
            end
        end
        % This is necessarily very simplistic but al we can do without a
        % knowledge of TT to maturity in the complex crop:
        if cCult.gPhase == 1 || cCult.gPhase == 2
            Live_Root_2_FOM_Frac = 0.01
        else
            Live_Root_2_FOM_Frac = 0.05
        end
        NFac = cCult.NFac
        PFac = cCult.PFac
        Nfixing = cSpe.Nfixing
        topWt = (sum(cCult.WtLeaf(), 1, 2) + sum(cCult.WtStem(), 1, 2) + ...
            sum(cCult.Wtkr(), 1, 2)) * gppl_to_gpm2
        rootWt = sum(cCult.WtRoot(), 1, 2) * gppl_to_gpm2
        grainWt = sum(cCult.Wtkr(), 1, 2) * gppl_to_gpm2
        Sen_rootWt = 0
        if topWt > 0
            cCult.Harvest_Index = grainWt / topWt
        end
        Harvest_Index = cCult.Harvest_Index
        Root_Part_Coeff = cCult.pcRoot
        Root_Depth = cCult.Root_Depth
        Root_Front_Layer = cCult.Root_Front_Layer
        RootC = cCult.RootC
        RootSloC = cCult.RootSloC
        RootIntC = cCult.RootIntC
        RootSloN = cCult.RootSloN
        C_to_Nod_Tot_kg = cSpe.C_to_Nod_Tot_kg
        Resp_Param_to_Fix_N = cSpe.Resp_Param_to_Fix_N
        for layer = 1 : cSoil.nlayr
            Root_Layer_Frac(layer) = cCult.Root_Layer_Frac(layer)
        end
    end
    
    if ISwNit
        [DUMMY] = Soil_Biochemistry(C, n, P, Dc, DN, DP, cSoil, csoil_bio, Nitro, Phos, ...
            cropmod, DAE, dBiomass, Root_Layer_Frac(), ...
            Live_Root_2_FOM_Frac, ...
            NFac, PFac, Nfixing, Lag_Req_4_NFix_Is_Met, rel_TT, ...
            topWt, rootWt, Sen_rootWt, grainWt, Root_Part_Coeff, ...
            Root_Depth, Root_Front_Layer, RootC, ...
            RootSloC, RootIntC, RootSloN, ...
            C_to_Nod_Tot_kg, Resp_Param_to_Fix_N, ...
            DoHarvest, Ambient_Temperature, Rain, newRootFOM_kg_C, ...
            newRootFOM_kg_N, newRootFOM_kg_P)
    end
    
    if cropmod == 'S' && DAE >= 1 % if simple crop
        cCrop.NFac = NFac;
        cCrop.PFac = PFac;
    elseif cropmod == 'C' && DAE >= 1 % if complex crop
        cCult.NFac = NFac
        cCult.PFac = PFac
    end
    % This code works at planting, placing 90% of standing dead into the layer 1
    %{added by iurii
    % bRedistributeFom = false
    if bRedistributeFom
        C.Slow_FOM_kg(1) = C.Slow_FOM_kg(1) + 0.9 * C.Slow_FOM_kg(-1)
        C.Slow_FOM_kg(-1) = 0.1 * C.Slow_FOM_kg(-1)
        C.Inter_FOM_kg(1) = C.Inter_FOM_kg(1) + 0.9 * C.Inter_FOM_kg(-1)
        C.Inter_FOM_kg(-1) = 0.1 * C.Inter_FOM_kg(-1)
        C.Rapid_FOM_kg(1) = C.Rapid_FOM_kg(1) + 0.9 * C.Rapid_FOM_kg(-1)
        C.Rapid_FOM_kg(-1) = 0.1 * C.Rapid_FOM_kg(-1)
        bRedistributeFom = false
    end
    %}added by iurii
    
    % Integrate state variables from water, nutrient, C cycles:  %AG
    
    [DUMMY] = Integrate(cSoil, csoil_bio, C, n, P, Dc, DN, DP, cCrop, cCult, ...
        cropmod, Cum_rel_TT, Sen_rootWt, gpm2_to_gppl, ...
        newFOM_kg_C, newFOM_kg_N, ...
        newFOM_kg_P, newFert_kg_C, newFert_kg_N, newFert_kg_P, ...
        newRootFOM_kg_N, newRootFOM_kg_P, Cum_C_CO2, Cum_C_In, ...
        Cum_C_Out, Cum_N_In, Cum_N_Out, Cum_P_In, Cum_P_Out, DAE, ...
        DoHarvest)
    
    % Do harvest if necessary
    
    if DoHarvest
        %ADK sql = "Select * FROM [Mgt_Harvest_App] WHERE [ExpID] = " + ...
        %Str(RsExp("ExpID")) + " AND [RcID] = " + Str(RsComp("RcID"))
        
        %ADK RsTemp.Open sql, ConExp, adOpenDynamic, adLockOptimistic, adCmdText
        
        Harvest_ByProd_fraction = GetSingle(RsTemp, 'HBPc', WARNING)
        if Harvest_ByProd_fraction > -90
            Harvest_ByProd_fraction = Harvest_ByProd_fraction / 100
        else
            Harvest_ByProd_fraction = 0
        end
        Knock_Down_Fraction_Harv = GetSingle(RsTemp, 'HKnDnPc', WARNING)
        if Knock_Down_Fraction_Harv > -90
            Knock_Down_Fraction_Harv = Knock_Down_Fraction_Harv / 100
        else
            Knock_Down_Fraction_Harv = 0
        end
        
        % Crop inputs to the harvest routine depend on which crop model
        % has been selected.  All plant mass must be g m-2 d-1:
        
        if cropmod == 'S' % if simple crop
            Species = cCrop.Species
            VegC = cCrop.VegC
            VegSloC = cCrop.VegSloC
            VegIntC = cCrop.VegIntC
            VegSloN = cCrop.VegSloN
        elseif cropmod == 'C' % if complex crop
            Species = cSpe.CG
            VegC = cCult.VegC
            VegSloC = cCult.VegSloC
            VegIntC = cCult.VegIntC
            VegSloN = cCult.VegSloN
        end
        
        [DUMMY] = Harvest(Species, RootC, RootSloC, RootIntC, ...
            RootSloN, VegC, VegSloC, VegIntC, ...
            VegSloN, cropmod, gppl_to_gpm2, Harvest_Index, Matured, rootWt, ...
            topWt, Root_Front_Layer, Root_Depth, ...
            Root_Layer_Frac(), ...
            C, n, P, Dc.Live_2_Harvest_kg, DN.Live_2_Harvest_kg, ...
            DP.Live_2_Harvest_kg, Nitro, Phos, cCult, cSoil, csoil_bio, ...
            Harvest_ByProd_fraction, Knock_Down_Fraction_Harv, ...
            newRootFOM_kg_C, newRootFOM_kg_N, ...
            newRootFOM_kg_P, newKnockFOM_kg_C, ...
            newKnockFOM_kg_N, newKnockFOM_kg_P, ...
            newStandFOM_kg_C, newStandFOM_kg_N, ...
            newStandFOM_kg_P)
    end % Do harvest
    
    % END Run models ---------------------------------->
    
    % Complex crop C mass balance check:
    
    if cropmod == 'C' && cCult.isSown
        %    if cropmod == 'C' && cCult.gPhase >= 1
        [DUMMY] = C_MassBalance(cCult, Sen_rootWt, gpm2_to_gppl, gppl_to_gpm2 * gpm2_to_kgpha)
    end
    
    % Soil biochemistry mass balance check at the end of the day:
    
    % Integrate today's external material applications.  Because the model does not keep
    % track of plant elemental C, it does not know that newRootFOM_kg_C (root respiration,
    % exhudation, and senescence), newKnockFOM_kg_C (knocked-down C) and newStandFOM_kg_C
    % (standing dead C) are just internal recycling of nutrients.  So have to add them
    % explicitly to maintain mass balance:
    
    if ISwNit
        newFOM_kg_C = newResFOM_kg_C + newRootFOM_kg_C + ...
            newKnockFOM_kg_C + newStandFOM_kg_C
        
        % On the first day after emergence, we need to add root senescence N and P
        % as an external addition because they don't not come from inside the
        % system but from the seed.  Same on initial conditions date (=begindate)
        % when there may be root residue added from previous crop:
        
        if DAE ~= 1 && CurDate.DateStr ~= BeginDate.DateStr
            newFOM_kg_N = newResFOM_kg_N
            newFOM_kg_P = newResFOM_kg_P
        else
            newFOM_kg_N = newResFOM_kg_N + newRootFOM_kg_N
            newFOM_kg_P = newResFOM_kg_P + newRootFOM_kg_P
        end
        
        [DUMMY] = Soil_Biochemistry_MassBalance(C, n, P, Dc, DN, DP, cSoil.nlayr, ...
            newFOM_kg_C, newFOM_kg_N, ...
            newFOM_kg_P, newFert_kg_C, newFert_kg_N, ...
            newFert_kg_P, DAE)
    end
    
    % BEGIN write output records ------------------------>
    
    % Write output to database:    %AG
    if DoOutput
        
        % Write soil and weather output to database:    %AG
        
        [DUMMY] = WriteResults_Soil(cSoil, RsOut, Rain, srad, Tmax, Tmin)
        
        
        if cropmod == 'S' && cCrop.isSown  % if simple crop     %AG
            [DUMMY] = WriteResults_CropSimp(cCrop, RsOut, Cum_rel_TT, cSoil.nlayr)
        elseif cropmod == 'C' && cCult.isSown % if complex crop
            [DUMMY] = WriteResults_CropComp(cCult, RsOut, gppl_to_gpm2 * gpm2_to_kgpha, ...
                cSoil.nlayr)
        end
        
        [DUMMY] = WriteResults_Soil_Biochemistry(RsOut, csoil_bio, Dc, DN, ...
            DP, C, n, P, Cum_C_CO2, ...
            Cum_C_In, Cum_C_Out, Cum_N_In, ...
            Cum_N_Out, Cum_P_In, ...
            Cum_P_Out, cSoil.nlayr, ...
            CO2Mod.CO2Level)
        
        RsOut.UpdateBatch
        [DUMMY] = write_layer_info(rsoutlay, RsExp('ExpID'), RsComp('RcID'))
    end
    
    % END write output records -------------------------->
    
    % Zero these variables to maintain mass balance:
    
    newResFOM_kg_C = 0
    newResFOM_kg_N = 0
    newResFOM_kg_P = 0
    newRootFOM_kg_C = 0
    newRootFOM_kg_N = 0
    newRootFOM_kg_P = 0
    newKnockFOM_kg_C = 0
    newKnockFOM_kg_N = 0
    newKnockFOM_kg_P = 0
    newStandFOM_kg_C = 0
    newStandFOM_kg_N = 0
    newStandFOM_kg_P = 0
    newFOM_kg_C = 0
    newFOM_kg_N = 0
    newFOM_kg_P = 0
    newFert_kg_C = 0
    newFert_kg_N = 0
    newFert_kg_P = 0
    DN.Live_2_Harvest_kg = 0
    DP.Live_2_Harvest_kg = 0
    DN.Fix_kg = 0
    dBiomass = 0
    Sen_rootWt = 0
    
    % The DoHarvest section is to rotate to the next crop in the sequence
    % some other code has crept into this loop and will need to be moved
    
    if DoHarvest
        
        RsComp.MoveNext
        if RsComp.EOF
            RsComp.MoveFirst
        end
        
        % write results to seasonal totals ....
        
        % Crop outputs depend on which crop model has been selected.
        % All plant mass must be g m-2 d-1:
        
        if cropmod == 'S' && cCrop.isSown % if simple crop
            RsOut2.AddNew
            [DUMMY] = PutIn(RsOut2, 'PlntYear', cCrop.PlntYear, WARNING)
            [DUMMY] = PutIn(RsOut2, 'PlntDOY', cCrop.PlntDOY, WARNING)
            %RsOut2('TopWt') = cCrop.topWt * gpm2_to_kgpha     % kg
            %RsOut2('HarvestIndex') = cCrop.Harvest_Index
            %RsOut2('GrainWt') = RsOut2('TopWt') * cCrop.Harvest_Index
            %RsOut2('RootDep') = cCrop.Root_Depth
            %RsOut2('RootWt') = cCrop.rootWt * 10
            %RsOut2('LAI') = cCrop.LAI
        elseif cropmod == 'C' && cCult.isSown % if complex crop
            [DUMMY] = PutIn(RsOut2, 'PlntYear', cCult.PlntYear, WARNING)
            [DUMMY] = PutIn(RsOut2, 'PlntDOY', cCult.PlntDOY, WARNING)
        end
        RsOut2.AddNew
        [DUMMY] = PutIn(RsOut2, 'ExpID', RsExp('ExpID'), WARNING)
        [DUMMY] = PutIn(RsOut2, 'RcID', RsComp('RcID'), WARNING)
        %RsOut2('Year') = CurDate.year
        %RsOut2('DOY') = CurDate.DOY
        [DUMMY] = PutIn(RsOut2, 'HrvYear', CurDate.year, WARNING)
        [DUMMY] = PutIn(RsOut2, 'HrvDOY', CurDate.DOY, WARNING)
        [DUMMY] = PutIn(RsOut2, 'cDay', cDay, WARNING)
        %RsOut2('Date') = CurDate.JulDate
        [DUMMY] = PutIn(RsOut2, 'DAP', DAP, WARNING)
        [DUMMY] = PutIn(RsOut2, 'IRRC', cSoil.TOTIR, WARNING)
        RsOut2.UpdateBatch
        
        [DUMMY] = WriteResults_CropCompSea(cCult, RsOut2, gppl_to_gpm2 * gpm2_to_kgpha)
        [DUMMY] = WriteResults_Soil_Biochemistry_Sea(RsOut, RsOut2)
        
        DAP = -1
        DAE = -1
        Lag_Req_4_NFix_Is_Met = false
        Live_Root_2_FOM_Frac = 0
        NFac = 1
        PFac = 1
        Nfixing = 'N'
        topWt = 0
        rootWt = 0
        grainWt = 0
        Harvest_Index = 0
        Root_Part_Coeff = 0
        Root_Depth = 0
        Root_Front_Layer = 0
        RootC = 0
        RootSloC = 0
        RootIntC = 0
        RootSloN = 0
        rel_TT = 0
        C_to_Nod_Tot_kg = 0
        Resp_Param_to_Fix_N = 0
        for layer = 1 : cSoil.nlayr
            Root_Layer_Frac(layer) = 0
        end
        [DUMMY] = Init_CropSimp(cCrop)
        [DUMMY] = Init_Cult(cCult, cSpe, 0, 0, 0, 0, 0)
        [DUMMY] = CalcVirtYear(ConExp, RsExp, RsComp, YearOffSet)
        [DUMMY] = SetEventList(EList(), RsComp, ConExp, RsExp, YearOffSet)
        
        msg = CurDate.DateStr + ': Started component ' + ...
            GetString(RsComp, 'title')
        [DUMMY] = AddMsgToStatus(FrmStatus.ListEvents, msg)
        
        DoHarvest = false
        cCrop.Matured = false
        cCult.Matured = false
        % cCult.Killed = false
        % Matured = false
        RsTemp.Close % How does the program know which harvest information to use next time?  %AG
        
    end % do harvest
    
    % On the day of maturity, after writing the last output, reset growing flag:
    
    if cropmod == 'S' && (cCrop.Matured || cCrop.Killed) % simple crop
        Matured = true
        if DAMat < 0
            DAMat = 0
        else
            DAMat = DAMat + 1
        end
        cCrop.isSown = false
        Cum_slow_dev_days = 0
    elseif cropmod == 'C' && (cCult.Matured || cCult.Killed)
        Matured = true
        if DAMat < 0
            DAMat = 0
        else
            DAMat = DAMat + 1
        end
        cCult.isSown = false
        Cum_slow_dev_days = 0
    else
        Matured = false
        DAMat = -99
    end
    
    cDay = cDay + 1  % increment cumulative day
    [DUMMY] = Incr_CurrentDate
    VirtYear = CurDate.year + YearOffSet
    if DAP > -1
        DAP = DAP + 1      % set days after planting
    end
    
    if CancelRun
        break
    end
end
% END MAIN LOOP --------------------------------------------------->>>


% BEGIN close connections --------------------------------------------------
%ConRdb.CommitTrans
ConExp.Close
ConWth.Close
ConSoil.Close
ConCrop.Close
ConGlobal.Close
ConRdb.Close

% END close connections --------------------------------------------------



end

function [] = LoadInitialCond()
% Private Sub LoadInitialCond(ByRef RsTemp As ADODB.Recordset, ByRef cSoil As SOIL_TYPE, ...
%                             ByRef n As Soil_Bio_Element_Type, ...
%                             ByRef P As Soil_Bio_Element_Type, ...
%                             ByRef ppm2kg() As Double)

% +-----------------------------------------------------------------------
% | Read in initial conditions in Soil and Soil Biochemistry structures
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by:
% | Modified by: aris gerakis, 20 dec 1999:  Conformed to soil biochemistry
% | module:  Lumped NO3+NH4 into Ninorg, added P init. cond.
% +-----------------------------------------------------------------------

%ADK Dim IC_NLayr As Integer, l As Integer
%ADK Dim IC_ZLayr(0 : MAX_LAYER) As Single, IC_SW(0 : MAX_LAYER) As Single
%ADK Dim IC_Ninorg_ppm(0 : MAX_LAYER) As Double, IC_TotP_ppm(0 : MAX_LAYER) As Double, ...
%    IC_P_ActIno_ppm(0 : MAX_LAYER) As Double, IC_P_SloIno_ppm(0 : MAX_LAYER) As Double, ...
%    IC_P_Labile_ppm(0 : MAX_LAYER) As Double
%ADK Dim N_Labile_ppm(0 : MAX_LAYER) As Double, TotP_ppm(0 : MAX_LAYER) As Double, ...
%    P_ActIno_ppm(0 : MAX_LAYER) As Double, P_SloIno_ppm(0 : MAX_LAYER) As Double, ...
%    P_Labile_ppm(0 : MAX_LAYER) As Double, TotP_kg(0 : MAX_LAYER) As Double     %AG
%ADK Dim Load_SW As Boolean, Load_Ninorg As Boolean
%ADK Dim Load_TotP As Boolean, Load_P_ActIno As Boolean, Load_P_SloIno As Boolean, ...
%    Load_P_Labile As Boolean  %AG

% Variables for mass balance error checking, i.e., for correcting total mass yesterday:
% This is supplementary to the initialization in RESET_SOIL_BIO:

%ADK Dim Tot_Act_Inorg_kg  As Double % Total Active inorganic in profile [kg ha-1]
%ADK Dim Tot_Slo_Inorg_kg  As Double % Total Slow inorganic in profile [kg ha-1]
%ADK Dim Tot_Labile_kg     As Double % Total Labile [includes Solution] in profile [kg ha-1]
%ADK Dim Tot_Solution_kg   As Double % Total Solution [included in Labile] in profile [kg ha-1]

RsTemp.MoveFirst
IC_NLayr = 0
IC_ZLayr(0) = 0
Load_SW = true
Load_Ninorg = true  %AG
Load_TotP = true  %AG
Load_P_ActIno = true  %AG
Load_P_SloIno = true  %AG
Load_P_Labile = true  %AG

while (RsTemp.EOF ~= true)
    
    % if there is no layer depth information skip the record, otherwise print message:
    
    if (~IsNull(RsTemp('DLayrI')))
        IC_NLayr = IC_NLayr + 1
        IC_ZLayr(IC_NLayr) = RsTemp('DLayrI')
        
        if (IsNull(RsTemp('SWInit')))
            Load_SW = false
        else
            IC_SW(IC_NLayr) = Min(RsTemp('SWInit'), cSoil.SAT(IC_NLayr))
            if (IC_SW(IC_NLayr) < 0)
                Load_SW = false
            end
        end
        
        if (IsNull(RsTemp('INinorg')))
            Load_Ninorg = false
        else
            IC_Ninorg_ppm(IC_NLayr) = RsTemp('INinorg')
            if (IC_Ninorg_ppm(IC_NLayr) < 0)
                Load_Ninorg = false
            end
        end
        
        if (IsNull(RsTemp('TotP')))               %AG
            Load_TotP = false                            %AG
        else                                           %AG
            IC_TotP_ppm(IC_NLayr) = RsTemp('TotP')           %AG
            if (IC_TotP_ppm(IC_NLayr) < 0)              %AG
                Load_TotP = false                          %AG
            end                                       %AG
        end                                         %AG
        
        if (IsNull(RsTemp('P_ActIno')))               %AG
            Load_P_ActIno = false                            %AG
        else                                               %AG
            IC_P_ActIno_ppm(IC_NLayr) = RsTemp('P_ActIno')       %AG
            if (IC_P_ActIno_ppm(IC_NLayr) < 0)              %AG
                Load_P_ActIno = false                          %AG
            end                                           %AG
        end                                             %AG
        
        if (IsNull(RsTemp('P_SloIno')))               %AG
            Load_P_SloIno = false                            %AG
        else                                               %AG
            IC_P_SloIno_ppm(IC_NLayr) = RsTemp('P_SloIno')       %AG
            if (IC_P_SloIno_ppm(IC_NLayr) < 0)              %AG
                Load_P_SloIno = false                          %AG
            end                                           %AG
        end                                             %AG
        
        if (IsNull(RsTemp('P_Labile')))               %AG
            Load_P_Labile = false                            %AG
        else                                               %AG
            IC_P_Labile_ppm(IC_NLayr) = RsTemp('P_Labile')       %AG
            if (IC_P_Labile_ppm(IC_NLayr) < 0)              %AG
                Load_P_Labile = false                          %AG
            end                                           %AG
        end                                             %AG
        
    else
        [DUMMY] = PRINTERR(WARNING, 'Initial conditions depth missing for record ' & CStr(IC_NLayr + 1))
    end
    RsTemp.MoveNext
end

if (IC_NLayr > 0)
    if (Load_SW)
        [DUMMY] = LMATCH(IC_NLayr, IC_ZLayr(), IC_SW(), cSoil.nlayr, cSoil.Zlayr(), cSoil.SW(), true)
    end
    
    % Load the nitrate and ammonia into the correct layers and then convert
    % to kg/ha
    
    if (Load_Ninorg)                                                     %AG
        [DUMMY] = LMATCHD(IC_NLayr, IC_ZLayr(), IC_Ninorg_ppm(), cSoil.nlayr, cSoil.Zlayr(), N_Labile_ppm(), false) %AG
        for l = 1 : cSoil.nlayr                                                %AG
            n.TotMassYest_kg = n.TotMassYest_kg - n.Labile_kg(l) % subtract previous N
            n.Labile_kg(l) = N_Labile_ppm(l) * ppm2kg(l)         % calculate new N
            n.TotMassYest_kg = n.TotMassYest_kg + n.Labile_kg(l) % add new labile N
            n.Solution_kg(l) = n.Labile_kg(l) * n.Fraction_Labile_in_Sol
        end                                                                    %AG
    end                                                                    %AG
    
    % Load P into the correct layers and then convert to kg/ha (if not already) %AG
    
    if (Load_TotP)                                                       %AG
        [DUMMY] = LMATCHD(IC_NLayr, IC_ZLayr(), IC_TotP_ppm(), cSoil.nlayr, cSoil.Zlayr(), TotP_ppm(), false) %AG
        for l = 1 : cSoil.nlayr                                                %AG
            TotP_kg(l) = TotP_ppm(l) * ppm2kg(l)                                  %AG
        end                                                                    %AG
    end                                                                    %AG
    
    if (Load_P_ActIno)                                                   %AG
        [DUMMY] = LMATCHD(IC_NLayr, IC_ZLayr(), IC_P_ActIno_ppm(), cSoil.nlayr, cSoil.Zlayr(), P_ActIno_ppm(), false) %AG
        for l = 1 : cSoil.nlayr                                                %AG
            P.TotMassYest_kg = P.TotMassYest_kg - P.Act_Inorg_kg(l) % subtract previous P
            P.Act_Inorg_kg(l) = P_ActIno_ppm(l) * ppm2kg(l)         % calculate new P
            P.TotMassYest_kg = P.TotMassYest_kg + P.Act_Inorg_kg(l) % add new P
        end                                                                    %AG
    end                                                                    %AG
    
    if (Load_P_SloIno)                                                   %AG
        [DUMMY] = LMATCHD(IC_NLayr, IC_ZLayr(), IC_P_SloIno_ppm(), cSoil.nlayr, cSoil.Zlayr(), P_SloIno_ppm(), false) %AG
        for l = 1 : cSoil.nlayr                                                %AG
            P.TotMassYest_kg = P.TotMassYest_kg - P.Slo_Inorg_kg(l) % subtract previous P
            P.Slo_Inorg_kg(l) = P_SloIno_ppm(l) * ppm2kg(l)         % calculate new P
            P.TotMassYest_kg = P.TotMassYest_kg + P.Slo_Inorg_kg(l) % add new P
        end                                                                    %AG
    end                                                                    %AG
    
    if (Load_P_Labile)                                                   %AG
        [DUMMY] = LMATCHD(IC_NLayr, IC_ZLayr(), IC_P_Labile_ppm(), cSoil.nlayr, cSoil.Zlayr(), P_Labile_ppm(), false) %AG
        for l = 1 : cSoil.nlayr                                                %AG
            P.TotMassYest_kg = P.TotMassYest_kg - P.Labile_kg(l) % subtract previous P
            P.Labile_kg(l) = P_Labile_ppm(l) * ppm2kg(l)         % calculate new P
            P.TotMassYest_kg = P.TotMassYest_kg + P.Labile_kg(l) % add new P
        end                                                                    %AG
    end                                                                    %AG
    
end

end

function [] = SetInitialConditions()
% Private Sub SetInitialConditions(ByRef ConExp As ADODB.Connection, ...
%                                  ByRef RsTemp As ADODB.Recordset, ...
%                                  ByRef cSoil As SOIL_TYPE, ByRef C As Soil_Bio_Element_Type, ...
%                                  ByRef n As Soil_Bio_Element_Type, ...
%                                  ByRef P As Soil_Bio_Element_Type, ...
%                                  ByRef Dc As Soil_Bio_Delta_Element_Type, ...
%                                  ByRef DN As Soil_Bio_Delta_Element_Type, ...
%                                  ByRef DP As Soil_Bio_Delta_Element_Type, ...
%                                  ByRef csoil_bio As Soil_Bio_Type, ByVal ExpID As Double, ...
%                                  ByVal ISwNit As Boolean, ByRef sfdrn As Single, ...
%                                  ByRef newRootFOM_kg_C As Double, ...
%                                  ByRef newRootFOM_kg_N As Double, ...
%                                  ByRef newRootFOM_kg_P As Double)

% +-----------------------------------------------------------------------
% | Read in initial conditions from Experiment database
% +-----------------------------------------------------------------------
% | Calls: LoadInitialCond
% +-----------------------------------------------------------------------
% | Created by:
% | Modified by: aris gerakis, 26 nov 1999:  Conformed to soil biochemistry
% | module.
% | Modified by: aris gerakis, 07 jun 2001:  Added previous crop root residue.
% | Modified by: aris gerakis, 11 oct 2001:  Added previous tillage implement.
% +-----------------------------------------------------------------------

%   Dim gfile As String, PrCrop As String * 2, sql As String, TImpl As String
%   Dim depth As Single, fldd As Single, KnDnFrac As Single, ...
%       Root_Layer_Frac(0 : MAX_LAYER) As Single, TDep As Single, ...
%       WResR As Single, WResAG As Single, Wrn(0 : MAX_LAYER) As Single, Wsum As Single
%   Dim FOM_kg As Double, IncorporationPercent As Double, ...
%       IncorporationDepth As Single, Percent_C As Double, Percent_N As Double, ...
%       Percent_P As Double, Percent_Slow_C As Double, Percent_Inter_C As Double, ...
%       Percent_Slow_N As Double, Fraction_Vol_N As Double, newResFOM_kg_C As Double, ...
%       newResFOM_kg_N As Double, newResFOM_kg_P As Double
%   Dim k_Slo_FOM_X(-1 : MAX_LAYER) As Double, k_Int_FOM_X(-1 : MAX_LAYER) As Double, ...
%       k_Rap_FOM_X(-1 : MAX_LAYER) As Double
%   Dim MixFrac As Double
%   Dim Knock_Down_Fraction_Till As Single % knock-down fraction of tillage implement
%   Dim Decomp_Fac As Double
%   Dim l As Integer, Root_Front_Layer As Integer
%   Dim Rs As ADODB.Recordset

% Initial conditions independent of layer:

%ADK sql = "Select * from [Mgt_InitialCond]  WHERE [ExpID] = " + Str(ExpID)
%   RsTemp.Open sql, ConExp, adOpenDynamic, adLockOptimistic, adCmdText
if RsTemp.EOF ~= true
    PrCrop = GetString(RsTemp, 'PrCrop', WARNING)
    WResR = GetDouble(RsTemp, 'WResR', WARNING)
    WResAG = GetDouble(RsTemp, 'WResAG', WARNING)
    KnDnFrac = GetDouble(RsTemp, 'KnDnFrac', WARNING)
    TImpl = GetString(RsTemp, 'TImpl', WARNING)
    TDep = GetDouble(RsTemp, 'TDep', WARNING)
    fldd = GetSingle(RsTemp, 'Fldd', WARNING)
    sfdrn = GetSingle(RsTemp, 'Sfdrn', WARNING)
else
    % set to default values...
    PrCrop = ''
    WResR = -99
    WResAG = -99
    KnDnFrac = 1
    TImpl = '-'
    TDep = -99
    fldd = -99
    sfdrn = -99
end
RsTemp.Close

%********** R O O T S **********

if ISwNit && WResR > 0
    
    % Distribute root mass from previous crop.  This is from DSSAT SOILNI.FOR:
    
    Wsum = 0
    depth = 0
    
    for l = 1 : Min(cSoil.nlayr, 12)
        depth = depth + cSoil.dlayr(l)
        if cSoil.DepMax > 0
            Wrn(l) = exp(-3 * depth / cSoil.DepMax)
        else
            Wrn(l) = 0
        end
        Wsum = Wsum + Wrn(l)
    end
    
    for l = 1 : Min(cSoil.nlayr, 12)
        if Wsum > 0
            Root_Layer_Frac(l) = Wrn(l) / Wsum
        else
            Root_Layer_Frac(l) = 0
        end
    end
    
    % [DUMMY] = FOM_Placement to partition the nutrients into pools.  Root C, N, P
    % concentrations, % Slow C, % Intermediate C, and % Slow N have to be read
    % from the Crop_par table.  Volatile N is assumed zero for roots:
    
    gfile = MakeFullName(App.path, 'salus.gdb')
    
    if FileExists(gfile)
        
        %ADK sql = "Select * from [Crop_Par] WHERE [Code] = '" + PrCrop + "'"
        frmMDI.Dc1.ConnectionString = 'Provider=Microsoft.Jet.OLEDB.4.0;Persist ' ...
            + 'Security Info=false;Data Source=' + gfile
        frmMDI.Dc1.RecordSource = sql
        frmMDI.Dc1.Refresh
        Set Rs = frmMDI.Dc1.Recordset
        
        % Default root nutrient information:
        
        if ~Rs.EOF
            Percent_C = GetDouble(Rs, 'RootC', WARNING)
            Percent_N = GetDouble(Rs, 'RootN', WARNING)
            Percent_P = GetDouble(Rs, 'RootP', WARNING)
            Percent_Slow_C = GetDouble(Rs, 'RootSloC', WARNING)
            Percent_Inter_C = GetDouble(Rs, 'RootIntC', WARNING)
            Percent_Slow_N = GetDouble(Rs, 'RootSloN', WARNING)
        else
            [DUMMY] = PRINTERR(WARNING, 'Default root nutrient' + ...
                'information not found')
        end
        frmMDI.Dc1.Recordset.Close
    else
        MsgBox 'Global Parameters file not found: ' + gfile
    end
    
    FOM_kg = WResR
    IncorporationPercent = CDbl(100)
    Root_Depth = depth
    IncorporationDepth = Root_Depth
    Fraction_Vol_N = 0
    Root_Front_Layer = 1 % any positive value will do
    for l = -1 : cSoil.nlayr
        k_Rap_FOM_X(l) = 0
        k_Int_FOM_X(l) = 0
        k_Slo_FOM_X(l) = 0
    end
    [DUMMY] = FOM_Placement(FOM_kg, k_Slo_FOM_X(), k_Int_FOM_X(), ...
        k_Rap_FOM_X(), IncorporationPercent, ...
        Root_Front_Layer, Root_Depth, ...
        Root_Layer_Frac(), IncorporationDepth, ...
        Percent_C, Percent_N, Percent_P, ...
        Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, ...
        Fraction_Vol_N, C, n, P, csoil_bio, ...
        cSoil.dlayr(), cSoil.nlayr, ...
        newRootFOM_kg_C, newRootFOM_kg_N, ...
        newRootFOM_kg_P)
    
end % there is root residue from previous crop

%********** A B O V E  G R O U N D  R E S I D U E **********

% if previous crop was tilled, standing dead is determined by the knock-down
% fraction of the tillage implement in Till_Par.  That's why we set the local
% variable KnDnFrac to 0 at this point.  The residue is incorporated by invoking
% an implicit tillage event (till_soil_bio), without invoking till_soil_physical
% that disturbs the soil physical properties.  if previous crop was no-till,
% standing dead is determined by the knock-down fraction entered by the user in
% the general section of initial conditions.  That has been already read in at
% this point in the program so we leave as it is.

if ISwNit && WResAG > 0
    
    if Trim(TImpl) ~= '-'
        % case where previous crop residues were tilled in:
        KnDnFrac = 0
    end
    
    % Allocate aboveground residue mass from previous crop:
    
    for l = 1 : cSoil.nlayr
        Root_Layer_Frac(l) = 0
    end
    
    % [DUMMY] = FOM_Placement to partition the nutrients into pools.  Aboveground C, N, P
    % concentrations, % Slow C, % Intermediate C, and % Slow N have to be read
    % from the Crop_par table.  Volatile N is assumed zero:
    
    gfile = App.path + '\salus.gdb'
    
    if FileExists(gfile)
        
        %ADK sql = "Select * from [Crop_Par] WHERE [Code] = '" + PrCrop + "'"
        frmMDI.Dc1.ConnectionString = 'Provider=Microsoft.Jet.OLEDB.4.0;Persist ' ...
            + 'Security Info=False;Data Source=' + gfile
        frmMDI.Dc1.RecordSource = sql
        frmMDI.Dc1.Refresh
        Set Rs = frmMDI.Dc1.Recordset
        % Default vegetative nutrient information:
        if ~Rs.EOF
            Percent_C = GetDouble(Rs, 'VegC', WARNING)
            Percent_N = GetDouble(Rs, 'VegN', WARNING)
            Percent_P = GetDouble(Rs, 'VegP', WARNING)
            Percent_Slow_C = GetDouble(Rs, 'VegSloC', WARNING)
            Percent_Inter_C = GetDouble(Rs, 'VegIntC', WARNING)
            Percent_Slow_N = GetDouble(Rs, 'VegSloN', WARNING)
        else
            [DUMMY] = PRINTERR(WARNING, 'Default aboveground residue nutrient' + ...
                'information not found')
        end
        frmMDI.Dc1.Recordset.Close
    else
        MsgBox 'Global Parameters file not found: ' + gfile
    end
    
    Root_Depth = 0
    IncorporationPercent = CDbl(0)
    Fraction_Vol_N = 0
    Root_Front_Layer = -99 % any positive value will do
    for l = -1 : cSoil.nlayr
        k_Rap_FOM_X(l) = 0
        k_Int_FOM_X(l) = 0
        k_Slo_FOM_X(l) = 0
    end
    
    % Residue fallen on surface:
    
    if KnDnFrac > 0
        FOM_kg = WResAG * KnDnFrac
        IncorporationDepth = 0 % must be 0 for fallen residue
        [DUMMY] = FOM_Placement(FOM_kg, k_Slo_FOM_X(), k_Int_FOM_X(), ...
            k_Rap_FOM_X(), IncorporationPercent, ...
            Root_Front_Layer, Root_Depth, ...
            Root_Layer_Frac(), IncorporationDepth, ...
            Percent_C, Percent_N, Percent_P, ...
            Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, ...
            Fraction_Vol_N, C, n, P, csoil_bio, ...
            cSoil.dlayr(), cSoil.nlayr, ...
            newRootFOM_kg_C, newRootFOM_kg_N, ...
            newRootFOM_kg_P)
    end % fallen on surface
    
    % Standing dead residue:
    
    if KnDnFrac < 1
        if KnDnFrac < 0
            FOM_kg = WResAG
        else
            FOM_kg = WResAG * (1 - KnDnFrac)
        end
        IncorporationDepth = -99 % must be negative for standing dead
        [DUMMY] = FOM_Placement(FOM_kg, k_Slo_FOM_X(), k_Int_FOM_X(), ...
            k_Rap_FOM_X(), IncorporationPercent, ...
            Root_Front_Layer, Root_Depth, ...
            Root_Layer_Frac(), IncorporationDepth, ...
            Percent_C, Percent_N, Percent_P, ...
            Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, ...
            Fraction_Vol_N, C, n, P, csoil_bio, ...
            cSoil.dlayr(), cSoil.nlayr, ...
            newRootFOM_kg_C, newRootFOM_kg_N, ...
            newRootFOM_kg_P)
    end % standing dead
    
    % Case where previous crop residues were tilled in:
    
    if Trim(TImpl) ~= '-'
        
        % Get information about implement from the tillage parameter table
        
        gfile = App.path + '\salus.gdb'
        
        if FileExists(gfile)
            
            %ADK sql = "Select * FROM [Till_par] WHERE [Code] = '" + Trim(TImpl) + "'"
            frmMDI.Dc1.ConnectionString = 'Provider=Microsoft.Jet.OLEDB.4.0;Persist ' ...
                + 'Security Info=False;Data Source=' + gfile
            frmMDI.Dc1.RecordSource = sql
            frmMDI.Dc1.Refresh
            Set Rs = frmMDI.Dc1.Recordset
            
            if ~Rs.EOF
                Rs.MoveFirst
                MixFrac = GetSingle(Rs, 'MixFrac', WARNING)
                Knock_Down_Fraction_Till = GetSingle(Rs, 'KnDnFrac', WARNING)
                Decomp_Fac = GetDouble(Rs, 'DecomFac', WARNING)
            else
                MixFrac = 0
                Knock_Down_Fraction_Till = 0
                for layer = 1 : cSoil.nlayr
                    csoil_bio.Settled_TillDecompFac(layer) = 0
                end
            end
            frmMDI.Dc1.Recordset.Close
        else
            MsgBox 'Global Parameters file not found: ' + gfile
        end
        
        [DUMMY] = till_soil_bio(cSoil, C, n, P, Dc, DN, DP, TDep, ...
            Knock_Down_Fraction_Till, MixFrac, Decomp_Fac, ...
            csoil_bio.TillDecompFac, ...
            csoil_bio.Settled_TillDecompFac, ...
            csoil_bio.Tilled_TillDecompFac)
        
    end % previous crop residues were tilled in:
    
end % there is aboveground residue from previous crop

%********** T I L E S **********

%  if there is tile drainage find the layer that the drain is in
if fldd <= 0
    cSoil.TDLNO = -99
else
    % Find layer number for tile:
    for l = 1 : cSoil.nlayr
        if cSoil.Zlayr(l) >= fldd && cSoil.alyr(l) < fldd
            cSoil.TDLNO = l
        end
    end
end


% Initial conditions by layer:

%ADK sql = "Select * from [Mgt_InitialCond_Lay]  WHERE [ExpID] = " + Str(ExpID) + ...
%" ORDER BY [DLayrI] ASC"
%ADK RsTemp.Open sql, ConExp, adOpenDynamic, adLockOptimistic, adCmdText
if RsTemp.EOF ~= true
    [DUMMY] = LoadInitialCond(RsTemp, cSoil, n, P, csoil_bio.ppm2kg())  %AG
end

RsTemp.Close

end

function [] = SetRotationComponents()
%Public Sub SetRotationComponents(ConExp As ADODB.Connection, RsComp As ADODB.Recordset, ExpID As Double, FatalError As Boolean)

%ADK sql = "Select * from [Rotation_Components] WHERE [ExpID] = " + Str(ExpID) + ...
%ADK " ORDER BY [OrderNum] ASC"

%ADK RsComp.Open sql, ConExp, adOpenDynamic, adLockOptimistic, adCmdText

if ADO_RecordCount(RsComp) == 0
    msg = 'No rotation components are defined for this experiment.'
    [DUMMY] = PRINTERR(FATAL, msg)
    FatalError = true
else
    RsComp.MoveFirst
end



end