function [Killed,Cum_slow_dev_days] = environmental_extremes(cropmod,Cum_slow_dev_days,dTT,Killed,...
    Tmin,rel_TT,rel_TT_at_Senesc,gPhase)
%* module Environmental_Extremes
%+-----------------------------------------------------------------------
%| Stop crop model if any environmental extreme such as frost-kill, long-
%| term cold, drought, or nutrient stress, etc.
%+-----------------------------------------------------------------------
%| Created by:  aris gerakis - 15 apr 2001
%| Translated to MATLAB by: Anthony Kendall - 16 oct 2009
%+-----------------------------------------------------------------------
%
%ADK Notes:
% - Disabled warnings from this module, not necessary but were translated
%   to ILHM format for re-enabling later

%Pull necessary values from data_controller
%ADK: add this below if warnings are re-enabled
%[CurDate] = data_controller();

%**** CONSTANTS
%ADK: Move this elsewhere
% Cumulative slow development days to stop crop model:
Cum_slow_dev_days_to_stop_crop = 20;

%Frost kill
switch upper(cropmod)
    case 'S' % if simple crop
        %warningType = 'Simple';
        
        % Stop simple crop model if frost after start of senescence (or max. LAI):
        indKill = (rel_TT > rel_TT_at_Senesc) & (Tmin <= -10);
        if any(indKill)
            Killed(indKill) = true;
            %warning_LHM([warningType,' crop model stopped on ', CurDate.DateStr , ' due to frost.']);
        end
        
    case 'C' % if complex crop
        %warningType = 'Complex';
        
        % Stop complex crop model if frost during grain filling:
        indKill = (gPhase == 4) & (Tmin <= -10);
        if any(indKill)
            Killed(indKill) = true;
            %warning_LHM([warningType,' crop model stopped on ', CurDate.DateStr , ' due to frost.']);
        end
end

%Slow development
% Check accumulation of an arbitrary number of slow development days (dTT < 0.1):
indSlow = (dTT <= 0.1);
indNotSlow = ~indSlow;

%Reset for crops that are developing adequately
if any(indNotSlow)
    Cum_slow_dev_days(indNotSlow) = 0;
end

%Flow slowly developing crops
if any(indSlow)
    Cum_slow_dev_days(indSlow) = Cum_slow_dev_days(indSlow) + 1;
    indKill = (Cum_slow_dev_days >= Cum_slow_dev_days_to_stop_crop);
    if any(indKill)
        Killed(indKill) = true;
        %warning_LHM([warningType,' crop model stopped on ', CurDate.DateStr , ' due to slowed development.']);
    end
end

end % Env_Extremes