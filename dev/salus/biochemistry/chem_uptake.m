%----------------------------------------------------------------------------------------------
%	Brad Garrod --  edit into chem_uptake as a Matlab function (1/23/2012)
%	
%	Unsure of some of the formal implications of the functions from VB to Matlab,
%	therefore, all changes will be document and nothing will be removed, just commented out.
%	Expect a lot of block comments as well as possible function application errors.
%	Any line that has been changed will be followed by a commented date.
%----------------------------------------------------------------------------------------------

% Commented out up until  the function chem_uptake actually begins since these are not needed.
% BG - 1/23/2012

% Attribute VB_Name = "NutrientUptake"
% % Nutrient uptake for crop models

% Option Explicit


% %*** CONSTANTS

% % Minimum allowable concentration in plant as fraction of optimum concentration:

% Public Const Min_as_fraction_of_opt As Double = 0.6;

% %*** TYPES

% Public Type Plant_Nutrient_Type
% Symbol                      As String * 2 % Chemical symbol
% MnMlt                       As Double % Multiplier for minimum concentration [unitless]  [Range: 0.4-1]
% % Used only by the simple crop model:
% Opt_Plant_Fraction(1 To 3)  As Double % Optimum whole plant nutrient concentration as fraction
% Opt_Grain_Fraction          As Double % Optimum grain nutrient concentration at maturity as fraction
% % Used only by the complex crop model:
% MxVg                        As Double % Maximum concentration in vegetative parts as fraction
% MxKr                        As Double % Maximum concentration in grain as fraction
% RlVg                        As Double % Relative concentration in vegetative parts
% RlKr                        As Double % Relative concentration in grain
% RlVg_XY_3() As XY_ELEMENT_TYPE        % Curve for max. tops concentration multiplier for phase 3
% RlKr_XY_3() As XY_ELEMENT_TYPE        % Curve for max. grain concentration multiplier for phase 3
% RlVg_XY_4() As XY_ELEMENT_TYPE        % Curve for max. tops concentration multiplier for phase 4
% RlKr_XY_4() As XY_ELEMENT_TYPE        % Curve for max. grain concentration multiplier for phase 4
% End Type % Plant_Nutrient_Type

% %*** SUBROUTINES

% Public Sub Init_Chem_Uptake(ByRef chem As Plant_Nutrient_Type, ByVal Symbol As String, ...
    % Optional ByVal MnMlt As Single)

% %* Init_Chem_Uptake : Initializes values in Plant_Nutrient_Type data object that are not
% %                     initialized by the setup crop routines.
% %  Created by:      aris gerakis - 19 apr 2001

% Dim i As Integer

% ReDim chem.RlVg_XY_3(1 To 1)
% ReDim chem.RlKr_XY_3(1 To 1)
% ReDim chem.RlVg_XY_4(1 To 1)
% ReDim chem.RlKr_XY_4(1 To 1)

% Call Init_XY(chem.RlVg_XY_3())
% Call Init_XY(chem.RlKr_XY_3())
% Call Init_XY(chem.RlVg_XY_4())
% Call Init_XY(chem.RlKr_XY_4())

% if chem.MnMlt <= 0
    % chem.MnMlt = MnMlt
% end
% chem.RlVg = 0
% chem.RlKr = 0
% chem.Symbol = Symbol

% end %Init_Chem_Uptake

function [] = chem_uptake(chem, cropmod, DAE, topWt, rootWt, grainWt, Fraction_Labile_inSol,...
	Live_kg, ch_labile_kg, ch_supply_kg, ch_demand_kg, ch_delta_uptake_layer_kg, todays_stress,...
	dlayr, nlayr, Root_Layer_Frac, rwu, zlyr) %1/23/2012

% Public Declaration not needed in Matlab
% BG - 1/23/2012
% Public Sub Chem_Uptake(ByRef chem As Plant_Nutrient_Type, ...
    % ByVal cropmod As String, ...
    % ByVal DAE As Integer, ...
    % ByVal topWt As Single, ...
    % ByVal rootWt As Single, ...
    % ByVal grainWt As Single, ...
    % ByVal Fraction_Labile_in_Sol As Double, ...
    % ByRef Live_kg As Double, ...
    % ByRef ch_labile_kg() As Double, ...
    % ByRef ch_supply_kg As Double, ...
    % ByRef ch_demand_kg As Double, ...
    % ByRef ch_delta_uptake_layer_kg() As Double, ...
    % ByRef todays_stress As Single, ...
    % ByRef dlayr() As Single, ...
    % ByVal nlayr As Integer, ByVal rel_TT As Single, ...
    % ByRef Root_Layer_Frac() As Single, ...
    % ByRef rwu() As Single, ByRef zlyr() As Single)

% +-----------------------------------------------------------------------
% | Nutrient uptake (N, P) from live plants.  Must be called AFTER the
% | simple crop module because it needs emergence status and maturity status.
% +-----------------------------------------------------------------------
% | Calls: tabexD
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 03 nov 1999
% | Modified by: aris gerakis, 11 jan 2000:  Removed harvest.
% | Modified by: aris gerakis, 18 apr 2001:  New stress function. Modified
% |              to work with both crop models.
% +-----------------------------------------------------------------------

% Dim layer As Integer, point As Integer
%
% Dim rel_TT_fixed_points(1 To 3) As Single % three fixed points in relative thermal time span
%
% Dim ch_live_kg As Double, ch_supply_layer_kg(0 To 20) As Double, ...
%     ch_delta_uptake_kg As Double, sum As Double, sum_weights_2 As Double, ...
%     upt_check_kg As Double, upt_corr_ppm As Double, weight_1(0 To 20) As Double, ...
%     weight_2(0 To 20) As Double, Wf As Double
% Dim ch_min_fraction  As Double % Minimum whole plant nutrient concentration as fraction
% Dim ch_opt_fraction  As Double % Optimum whole plant nutrient concentration as fraction
% Dim ch_pot_fraction  As Double % Potential whole plant nutrient concentration as fraction
%
% Dim err_str As String

% Initialize local variables:

% following for loop not needed 
% BG - 1/23/2012
% for layer = 0 : nlayr
    % ch_supply_layer_kg(layer) = 0
    % ch_delta_uptake_layer_kg(layer) = 0
    % weight_1(layer) = 0
    % weight_2(layer) = 0
% end

% Added following 4 lines to replace the previous for loop.
% Values from 1 to nlayr are replaced with 0
% BG - 1/23/2012
ch_supply_layer_kg(1:nlayer) = 0;
ch_delta_uptake_layer_kg(1:nlayr) = 0;
weight_1(1:nlayr) = 0;
weight_2(1:nlayr) = 0;

layer = 1;
ch_min_fraction = 0;
ch_opt_fraction = 0;
ch_pot_fraction = 0;
ch_demand_kg = 0;
ch_live_kg = 0;
ch_supply_kg = 0;
ch_delta_uptake_kg = 0;
sum = 0;
sum_weights_2 = 0;
rel_TT_fixed_points(1) = 0;
rel_TT_fixed_points(2) = 0.5;
rel_TT_fixed_points(3) = 1;
todays_stress = 1;
upt_check_kg = 0;
upt_corr_ppm = 0;
Wf = 0;

% Uptake:

% Total soil supply in kg ha-1:

for layer = 1 : nlayr
    
    % Water factor to reduce supply:
    
    Wf = min(1, 1 / 0.005 * rwu(layer) / dlayr(layer));
    
    % Select the min. of water factor, root density factor:
    weight_1(layer) = min(Root_Layer_Frac(layer), Wf);
    ch_supply_layer_kg(layer) = weight_1(layer) * ch_labile_kg(layer) * Fraction_Labile_in_Sol;
    ch_supply_kg = ch_supply_kg + ch_supply_layer_kg(layer);
    
end

% Figure optimum and minimum concentrations of nutrient in whole plant, incl. roots.
% The assumption is that roots have the same concentration as vegetative tops (for lack
% of better data). In the simple model, optimum concentrations are interpolated between
% three points in the life of the plant: emergence, halfway through development, and
% at maturity. In the complex model, optimum concentrations are interpolated between
% values read from the species phase table:

% if cropmod == 'S'
% strcmp is faster in Matlab
% BG - 1/23/2012
if strcmp(cropmod, 'S')
    ch_opt_fraction = max(0, interp1(rel_TT_fixed_points, chem.Opt_Plant_Fraction, ...
        rel_TT, 'linear', 'extrap'));
    ch_min_fraction = ch_opt_fraction * chem.MnMlt;
%elseif cropmod == 'C'
elseif strcmp(cropmod, 'C') && (topWt + rootWt > 0)
    % if topWt + rootWt > 0
	% added into elseif
	% BG - 1/23/2012
        % weighted average of vegetative and grain concentration:
        ch_opt_fraction = chem.MxVg * chem.RlVg * (topWt + rootWt - grainWt) / (topWt + rootWt) + ...
            chem.MxKr * chem.RlKr * grainWt / (topWt + rootWt);
        ch_min_fraction = ch_opt_fraction * chem.MnMlt;
    end
else
    % Call PRINTERR(WARNING, "Crop model not defined for nutrient uptake", "Chem_Uptake")
	% not a function in Matlab, replaced with function below
	% BG - 1/23/2012
	error('Crop model not defined for nutrient uptake')
end

% Potential nutrient concentration with the new biomass (fraction).
% The first day after emergence assume that live nutrient content eq. optimum (some
% nutrient comes from the seed):

if DAE > 1
    ch_live_kg = Live_kg; % comes from the model
else
    ch_live_kg = ch_opt_fraction * (topWt + rootWt) * gpm2_to_kgpha; % comes from seed
    Live_kg = ch_live_kg;
end

if topWt + rootWt > 0
    ch_pot_fraction = ch_live_kg / ((topWt + rootWt) * gpm2_to_kgpha);
end

% Calculate demand in kg ha-1.  Also, 0-1 stress factor due to
% nutrient deficiency:

if ch_pot_fraction < ch_opt_fraction && ch_opt_fraction > ch_min_fraction
    ch_demand_kg = (ch_opt_fraction - ch_pot_fraction) * (topWt + rootWt) * gpm2_to_kgpha;
    todays_stress = 1 - (max(0, ch_pot_fraction - ch_min_fraction) / ...
        (ch_opt_fraction - ch_min_fraction) - 1) ^ 4;
else
    ch_demand_kg = 0;
    todays_stress = 1;
end

% Actual uptake total, kg ha-1:

ch_delta_uptake_kg = min(ch_supply_kg, ch_demand_kg);

% Uptake per layer, kg ha-1:

for layer = 0 : nlayr
    if ch_supply_kg > 0
        ch_delta_uptake_layer_kg(layer) = min((ch_supply_layer_kg(layer) / ch_supply_kg) * ...
            ch_delta_uptake_kg, Min_kg_in_any_pool);
    end
    % Uptake correction if labile pool is overdrawn:
    %   if ch_labile_kg(layer) - ch_delta_uptake_layer_kg(layer) < Min_kg_in_any_pool
    %      ch_delta_uptake_layer_kg(layer) = max(0, ch_labile_kg(layer) - Min_kg_in_any_pool)
    %   end
end

end %Chem_Uptake
