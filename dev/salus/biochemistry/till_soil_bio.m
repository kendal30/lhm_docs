function [C,n,P,Dc,DN,DP,TillDecompFac,Tilled_TillDecompFac] = ...
    till_soil_bio(cSoil, C, n, P, Dc, DN, DP, TDep, Knock_Down_Fraction_Till,...
    Mixing_Fraction, TillDecompFac, Settled_TillDecompFac, Tilled_TillDecompFac)
%* TILL_SOIL_BIO : Changes the soil biophysical properties for a tillage event
%*
%  Created:  aris gerakis on 12 may 2000

%Get the maximum till layer
l = find(cSoil.Zlayr > TDep,1,'first');

% Soil biochemistry pools decomposition factor: - AG
[TillDecompFac] = Mix_LayersD(1, l, cSoil.dlayr, cSoil.Zlayr, TillDecompFac, Mixing_Fraction);
[Settled_TillDecompFac] = Mix_LayersD(1, l, cSoil.dlayr, cSoil.Zlayr, Settled_TillDecompFac, Mixing_Fraction);

% Amounts of standing dead to mulch due to knock-down by tillage:
Dc.Rapid_FOM_Stand_Dead_2_Mulch_kg = max(0, (C.Rapid_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
Dc.Inter_FOM_Stand_Dead_2_Mulch_kg = max(0, (C.Inter_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
Dc.Slow_FOM_Stand_Dead_2_Mulch_kg = max(0, (C.Slow_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);

DN.Rapid_FOM_Stand_Dead_2_Mulch_kg = max(0, (n.Rapid_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
DN.Inter_FOM_Stand_Dead_2_Mulch_kg = max(0, (n.Inter_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
DN.Slow_FOM_Stand_Dead_2_Mulch_kg = max(0, (n.Slow_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);

if ISwPho
  DP.Rapid_FOM_Stand_Dead_2_Mulch_kg = max(0, (P.Rapid_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
  DP.Inter_FOM_Stand_Dead_2_Mulch_kg = max(0, (P.Inter_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
  DP.Slow_FOM_Stand_Dead_2_Mulch_kg = max(0, (P.Slow_FOM_kg(-1) - Min_kg_in_any_pool) * ...
                                     Knock_Down_Fraction_Till);
end

% Adjust for standing dead knocked-down by tillage on the soil surface:
C.Rapid_FOM_kg(-1) = C.Rapid_FOM_kg(-1) - Dc.Rapid_FOM_Stand_Dead_2_Mulch_kg;
C.Inter_FOM_kg(-1) = C.Inter_FOM_kg(-1) - Dc.Inter_FOM_Stand_Dead_2_Mulch_kg;
C.Slow_FOM_kg(-1) = C.Slow_FOM_kg(-1) - Dc.Slow_FOM_Stand_Dead_2_Mulch_kg;

n.Rapid_FOM_kg(-1) = n.Rapid_FOM_kg(-1) - DN.Rapid_FOM_Stand_Dead_2_Mulch_kg;
n.Inter_FOM_kg(-1) = n.Inter_FOM_kg(-1) - DN.Inter_FOM_Stand_Dead_2_Mulch_kg;
n.Slow_FOM_kg(-1) = n.Slow_FOM_kg(-1)  - DN.Slow_FOM_Stand_Dead_2_Mulch_kg;

if ISwPho
  P.Rapid_FOM_kg(-1) = P.Rapid_FOM_kg(-1) - DP.Rapid_FOM_Stand_Dead_2_Mulch_kg;
  P.Inter_FOM_kg(-1) = P.Inter_FOM_kg(-1) - DP.Inter_FOM_Stand_Dead_2_Mulch_kg;
  P.Slow_FOM_kg(-1) = P.Slow_FOM_kg(-1) - DP.Slow_FOM_Stand_Dead_2_Mulch_kg;
end

C.Rapid_FOM_kg(0) = C.Rapid_FOM_kg(0) + Dc.Rapid_FOM_Stand_Dead_2_Mulch_kg;
C.Inter_FOM_kg(0) = C.Inter_FOM_kg(0) + Dc.Inter_FOM_Stand_Dead_2_Mulch_kg;
C.Slow_FOM_kg(0) = C.Slow_FOM_kg(0) + Dc.Slow_FOM_Stand_Dead_2_Mulch_kg;

n.Rapid_FOM_kg(0) = n.Rapid_FOM_kg(0) + DN.Rapid_FOM_Stand_Dead_2_Mulch_kg;
n.Inter_FOM_kg(0) = n.Inter_FOM_kg(0) + DN.Inter_FOM_Stand_Dead_2_Mulch_kg;
n.Slow_FOM_kg(0) = n.Slow_FOM_kg(0) + DN.Slow_FOM_Stand_Dead_2_Mulch_kg;

if ISwPho
  P.Rapid_FOM_kg(0) = P.Rapid_FOM_kg(0) + DP.Rapid_FOM_Stand_Dead_2_Mulch_kg;
  P.Inter_FOM_kg(0) = P.Inter_FOM_kg(0) + DP.Inter_FOM_Stand_Dead_2_Mulch_kg;
  P.Slow_FOM_kg(0) = P.Slow_FOM_kg(0) + DP.Slow_FOM_Stand_Dead_2_Mulch_kg;
end

% Homogenize the soil biochemistry variables, from mulch down to tillage layer:
[C.Fertilizer_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Fertilizer_kg, Mixing_Fraction);
[C.Act_Inorg_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Act_Inorg_kg, Mixing_Fraction);
[C.Slo_Inorg_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Slo_Inorg_kg, Mixing_Fraction);
[C.Act_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Act_Org_kg, Mixing_Fraction);
[C.Slo_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Slo_Org_kg, Mixing_Fraction);
[C.Res_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Res_Org_kg, Mixing_Fraction);
[C.Rapid_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Rapid_FOM_kg, Mixing_Fraction);
[C.Inter_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Inter_FOM_kg, Mixing_Fraction);
[C.Slow_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, C.Slow_FOM_kg, Mixing_Fraction);

[N.Fertilizer_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Fertilizer_kg, Mixing_Fraction);
[N.Labile_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Labile_kg, Mixing_Fraction);
[N.Solution_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Solution_kg, Mixing_Fraction);
[N.Act_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Act_Org_kg, Mixing_Fraction);
[N.Slo_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Slo_Org_kg, Mixing_Fraction);
[N.Res_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Res_Org_kg, Mixing_Fraction);
[N.Rapid_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Rapid_FOM_kg, Mixing_Fraction);
[N.Inter_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Inter_FOM_kg, Mixing_Fraction);
[N.Slow_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, n.Slow_FOM_kg, Mixing_Fraction);

if ISwPho
  [P.Fertilizer_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Fertilizer_kg, Mixing_Fraction);
  [P.Act_Inorg_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Act_Inorg_kg, Mixing_Fraction);
  [P.Slo_Inorg_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Slo_Inorg_kg, Mixing_Fraction);
  [P.Labile_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Labile_kg, Mixing_Fraction);
  [P.Solution_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Solution_kg, Mixing_Fraction);
  [P.Act_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Act_Org_kg, Mixing_Fraction);
  [P.Slo_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Slo_Org_kg, Mixing_Fraction);
  [P.Res_Org_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Res_Org_kg, Mixing_Fraction);
  [P.Rapid_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Rapid_FOM_kg, Mixing_Fraction);
  [P.Inter_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Inter_FOM_kg, Mixing_Fraction);
  [P.Slow_FOM_kg] = Mix_Layers_SB(0, l, cSoil.dlayr, cSoil.Zlayr, P.Slow_FOM_kg, Mixing_Fraction);
end

% Some or all N in surface-applied volatile pools will be added to below-ground N:
for layer = 1 : l
   n.Fertilizer_kg(layer) = n.Fertilizer_kg(layer) + n.Vol_Fert_kg * Mixing_Fraction * ...
                            cSoil.dlayr(layer) / cSoil.Zlayr(l);
   n.Rapid_FOM_kg(layer) = n.Rapid_FOM_kg(layer) + n.Vol_FOM_kg * Mixing_Fraction * ...
                           cSoil.dlayr(layer) / cSoil.Zlayr(l);
end

n.Vol_Fert_kg = (1 - Mixing_Fraction) * n.Vol_Fert_kg;
n.Vol_FOM_kg = (1 - Mixing_Fraction) * n.Vol_FOM_kg;

% Change the soil biochemistry pools decomposition factor based on tillage:  
depthFactor = max((1 ./ (-log(TDep)) * log(cSoil.Z(:,1:l)) + 1), 0);
Tilled_TillDecompFac(:,1:l) = 4 * Settled_TillDecompFac(:,1:l) * (1 + Decomp_Fac * depthFactor);

% Do not allow to drop below yesterday's homogenized value:     'AG
Tilled_TillDecompFac(:,1:l) = max(TillDecompFac(:,1:l), Tilled_TillDecompFac(:,1:l));
TillDecompFac(:,1:l) = Tilled_TillDecompFac(:,1:l);

end % Till_Soil_Bio



function [target_var] = Mix_LayersD(top_layer, bot_layer, dlayr, Zlayr, target_var, Mixing_Fraction)
% Private Sub Mix_LayersD(ByVal top_layer As Integer, ByVal bot_layer As Integer, ...
%                        ByRef dlayr() As Single, ByRef Zlayr() As Single, ...
%                        ByRef target_var() As Double, ByVal Mixing_Fraction As Single)

%* MIX_LAYERSD : subroutine to mix the values from top_layer to bot_layer for one variable
%  Modified:   aris gerakis on 11 jan 2000:  modified from equal_lay, activated the
%              mixing fraction.  Accepts double as input array.

%Calculate the total mass
total = cumsum(dlayr(:,top_layer:bot_layer) .* target_var,2);

%Now partition into other layers using a mixing fraction
target_var(:,top_layer:bot_layer) = total .* Mixing_Fraction ./ repmat(Zlayr(:,bot_layer),1,bot_layer - top_layer + 1) + ...
        (1 - Mixing_Fraction) .* target_var(:,top_layer:bot_layer);
end % Mix_Layers

function [target_var] = Mix_Layers_SB(top_layer, bot_layer, dlayr, Zlayr, target_var, Mixing_Fraction)
%* MIX_LAYERS : subroutine to mix the values from top_layer to bot_layer for one soil
%               biochemistry variable.
%  Modified:   aris gerakis on 11 jan 2000:  modified from equal_lay, activated the
%              mixing fraction.  Accept double as input array.  Average not mass but
%              mass/dlayr.

%Calculate the total mass
total = cumsum(dlayr(:,top_layer:bot_layer) .* target_var,2);

%Now partition into other layers using a mixing fraction
target_var(:,top_layer:bot_layer) = total .* Mixing_Fraction .* dlayr(:,bot_layer:top_layer) ./ ...
    repmat(Zlayr(:,bot_layer),1,bot_layer - top_layer + 1) + (1 - Mixing_Fraction) .* target_var(:,top_layer:bot_layer);


end % Mix_Layers_SB