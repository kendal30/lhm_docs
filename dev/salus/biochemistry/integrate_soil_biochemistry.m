function [Cum_C_CO2, Cum_C_In, Cum_C_Out, Cum_N_In, Cum_N_Out, Cum_P_In, Cum_P_Out, ...
    MULCHDW, MassError] = integrate_soil_biochemistry(csoil_bio, C, n, P, Dc, DN, DP,...
    newFOM_kg_C, newFOM_kg_N, newFOM_kg_P, newFert_kg_N, newFert_kg_P,...
    newRootFOM_kg_N, newRootFOM_kg_P,...
    Cum_C_CO2, Cum_C_In, Cum_N_In, Cum_N_Out, Cum_P_In, Cum_P_Out,...
    DAE, Harvested)
% +-----------------------------------------------------------------------
% | Update C, N, and P pools.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 15 oct 1999 (gia tin Irini)
% +-----------------------------------------------------------------------
%

for layer = -1 : nlayr
    %N:
    % Sum up all the changes in organic N.  This change is subtracted from Labile N
    % which acts as a buffer pool for N shortages and surpluses, except for standing
    % dead (layer -1) where Labile N is irrelevant:
    n.Fertilizer_kg(layer) = n.Fertilizer_kg(layer) - DN.Fertilizer_2_Labile_kg(layer);
    n.Labile_kg(layer) = n.Labile_kg(layer) - DN.Labile_2_Atmosphere_kg(layer) - ...
        DN.Labile_2_Other_Layr_kg(layer) - DN.Labile_2_Uptake_kg(layer) + ...
        DN.Fertilizer_2_Labile_kg(layer);
    
    if layer >= 0
        [n.Labile_kg] = AdjustDecomposition(Dc, csoil_bio, n.Labile_kg(), layer);
        %DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer)
        %n.Labile_kg(layer) = n.Labile_kg(layer) - DN_Organic
    else % standing dead
        % We need to check that DN_Organic is not positive, otherwise we have
        % to change the C:N:P or the transformation rates.  Same for DP_Organic:
        DN_Organic = -Dc.Rapid_FOM_2_CO2_kg(layer) * csoil_bio.N2C_Rapid_FOM(layer);
        if DN_Organic > 0.0001
            warning_LHM(['Delta organic N is positive on ',CurDate.DateStr, ': ', ...
                DN_Organic,' You need to change either C:N:P or transformation rates for standing dead']);
            MassError = true;
        end
        n.Labile_kg(layer) = n.Labile_kg(layer) - DN_Organic;
    end % standing dead
    
    if n.Labile_kg(layer) < -0.0001
        warning_LHM(['Labile N is negative on ',CurDate.DateStr, ': ' & ...
            n.Labile_kg(layer),' in layer ',layer, '.  Check residue C:N, or initial labile N']);
        MassError = true;
    end
    
    % Update state variables: Concentration of element per layer per pool [always as elemental]
    
    %C:
    C.Fertilizer_kg(layer) = C.Fertilizer_kg(layer) - Dc.Fertilizer_2_Act_Inorg_kg(layer);
    C.Act_Inorg_kg(layer) = C.Act_Inorg_kg(layer) + Dc.Fertilizer_2_Act_Inorg_kg(layer) + ...
        Dc.Slo_Inorg_2_Act_Inorg_kg(layer) - Dc.Act_Inorg_2_CO2_kg(layer);
    C.Slo_Inorg_kg(layer) = C.Slo_Inorg_kg(layer) - Dc.Slo_Inorg_2_Act_Inorg_kg(layer);
    C.Act_Org_kg(layer) = C.Act_Org_kg(layer) - Dc.Act_Org_2_Slo_Org_kg(layer) - ...
        Dc.Act_Org_2_CO2_kg(layer) + Dc.Inter_FOM_2_Act_Org_kg(layer) + ...
        Dc.Rapid_FOM_2_Act_Org_kg(layer);
    C.Slo_Org_kg(layer) = C.Slo_Org_kg(layer) - Dc.Slo_Org_2_Res_Org_kg(layer) - ...
        Dc.Slo_Org_2_CO2_kg(layer) + Dc.Act_Org_2_Slo_Org_kg(layer) + ...
        Dc.Slow_FOM_2_Slo_Org_kg(layer);
    C.Res_Org_kg(layer) = C.Res_Org_kg(layer) - Dc.Res_Org_2_CO2_kg(layer) + ...
        Dc.Slo_Org_2_Res_Org_kg(layer);
    C.Rapid_FOM_kg(layer) = C.Rapid_FOM_kg(layer) - Dc.Rapid_FOM_2_Act_Org_kg(layer) - ...
        Dc.Rapid_FOM_2_CO2_kg(layer);
    C.Inter_FOM_kg(layer) = C.Inter_FOM_kg(layer) - Dc.Inter_FOM_2_Act_Org_kg(layer) - ...
        Dc.Inter_FOM_2_CO2_kg(layer);
    C.Slow_FOM_kg(layer) = C.Slow_FOM_kg(layer) - Dc.Slow_FOM_2_Slo_Org_kg(layer) - ...
        Dc.Slow_FOM_2_CO2_kg(layer);
    
    
    % N state variables
    n.Solution_kg(layer) = n.Labile_kg(layer) * n.Fraction_Labile_in_Sol;
    n.Act_Org_kg(layer) = n.Act_Org_kg(layer) + (-Dc.Act_Org_2_Slo_Org_kg(layer) - ...
        Dc.Act_Org_2_CO2_kg(layer) + Dc.Inter_FOM_2_Act_Org_kg(layer) + ...
        Dc.Rapid_FOM_2_Act_Org_kg(layer)) * N2C_Act_Org;
    n.Slo_Org_kg(layer) = n.Slo_Org_kg(layer) + (-Dc.Slo_Org_2_Res_Org_kg(layer) - ...
        Dc.Slo_Org_2_CO2_kg(layer) + Dc.Act_Org_2_Slo_Org_kg(layer) + ...
        Dc.Slow_FOM_2_Slo_Org_kg(layer)) * N2C_Slo_Org;
    n.Res_Org_kg(layer) = n.Res_Org_kg(layer) + (Dc.Slo_Org_2_Res_Org_kg(layer) - ...
        Dc.Res_Org_2_CO2_kg(layer)) * N2C_Res_Org;
    n.Rapid_FOM_kg(layer) = n.Rapid_FOM_kg(layer) + (-Dc.Rapid_FOM_2_Act_Org_kg(layer) - ...
        Dc.Rapid_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Rapid_FOM(layer);
    n.Inter_FOM_kg(layer) = n.Inter_FOM_kg(layer) + (-Dc.Inter_FOM_2_Act_Org_kg(layer) - ...
        Dc.Inter_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Inter_FOM(layer);
    n.Slow_FOM_kg(layer) = n.Slow_FOM_kg(layer) + (-Dc.Slow_FOM_2_Slo_Org_kg(layer) - ...
        Dc.Slow_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Slow_FOM(layer);
    n.Uptake_kg(layer) = n.Uptake_kg(layer) + DN.Labile_2_Uptake_kg(layer);
    
    %P:
    % Sum up all the changes in organic P.  This change is subtracted from Labile P
    % which acts as a buffer pool for P shortages and surpluses, except for standing
    % dead (layer -1) where Labile P is irrelevant:
    
    if ISwPho
        if layer >= 0
            DP_Organic = (-Dc.Act_Org_2_Slo_Org_kg(layer) - Dc.Act_Org_2_CO2_kg(layer) + ...
                Dc.Inter_FOM_2_Act_Org_kg(layer) + Dc.Rapid_FOM_2_Act_Org_kg(layer)) * P2C_Act_Org + ...
                (-Dc.Slo_Org_2_Res_Org_kg(layer) - Dc.Slo_Org_2_CO2_kg(layer) + ...
                Dc.Act_Org_2_Slo_Org_kg(layer) + Dc.Slow_FOM_2_Slo_Org_kg(layer)) * P2C_Slo_Org + ...
                (Dc.Slo_Org_2_Res_Org_kg(layer) - Dc.Res_Org_2_CO2_kg(layer)) * P2C_Res_Org + ...
                (-Dc.Rapid_FOM_2_Act_Org_kg(layer) - Dc.Rapid_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Rapid_FOM(layer) + ...
                (-Dc.Inter_FOM_2_Act_Org_kg(layer) - Dc.Inter_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Inter_FOM(layer) + ...
                (-Dc.Slow_FOM_2_Slo_Org_kg(layer) - Dc.Slow_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Slow_FOM(layer);
        else % standing dead
            DP_Organic = (-Dc.Rapid_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Rapid_FOM(layer);
            if DP_Organic > 0.0001
                warning_LHM(['Delta organic P is positive on ',CurDate.DateStr, ': ', ...
                    DP_Organic,' You need to change either C:N:P or transformation rates for standing dead']);
                MassError = true;
            end
        end % standing dead
        
        P.Fertilizer_kg(layer) = P.Fertilizer_kg(layer) - DP.Fertilizer_2_Labile_kg(layer);
        P.Labile_kg(layer) = P.Labile_kg(layer) - DP.Labile_2_Uptake_kg(layer) + ...
            DP.Fertilizer_2_Labile_kg(layer) + DP.Act_Inorg_2_Labile_kg(layer) - ...
            DP_Organic;
        
        if P.Labile_kg(layer) < -0.0001
            warning_LHM(['Labile P is negative on ',CurDate.DateStr, ': ' & ...
                P.Labile_kg(layer),' in layer ',layer, '.  Check residue C:P, or initial labile P']);
            MassError = true;
        end
        
        P.Solution_kg(layer) = P.Labile_kg(layer) * P.Fraction_Labile_in_Sol;
        P.Act_Inorg_kg(layer) = P.Act_Inorg_kg(layer) - DP.Act_Inorg_2_Labile_kg(layer) - ...
            DP.Act_Inorg_2_Slo_Inorg_kg(layer);
        P.Slo_Inorg_kg(layer) = P.Slo_Inorg_kg(layer) + DP.Act_Inorg_2_Slo_Inorg_kg(layer);
        P.Act_Org_kg(layer) = P.Act_Org_kg(layer) + (-Dc.Act_Org_2_Slo_Org_kg(layer) - ...
            Dc.Act_Org_2_CO2_kg(layer) + Dc.Inter_FOM_2_Act_Org_kg(layer) + ...
            Dc.Rapid_FOM_2_Act_Org_kg(layer)) * P2C_Act_Org;
        P.Slo_Org_kg(layer) = P.Slo_Org_kg(layer) + (-Dc.Slo_Org_2_Res_Org_kg(layer) - ...
            Dc.Slo_Org_2_CO2_kg(layer) + Dc.Act_Org_2_Slo_Org_kg(layer) + ...
            Dc.Slow_FOM_2_Slo_Org_kg(layer)) * P2C_Slo_Org;
        P.Res_Org_kg(layer) = P.Res_Org_kg(layer) + (Dc.Slo_Org_2_Res_Org_kg(layer) - ...
            Dc.Res_Org_2_CO2_kg(layer)) * P2C_Res_Org;
        P.Rapid_FOM_kg(layer) = P.Rapid_FOM_kg(layer) + (-Dc.Rapid_FOM_2_Act_Org_kg(layer) - ...
            Dc.Rapid_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Rapid_FOM(layer);
        P.Inter_FOM_kg(layer) = P.Inter_FOM_kg(layer) + (-Dc.Inter_FOM_2_Act_Org_kg(layer) - ...
            Dc.Inter_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Inter_FOM(layer);
        P.Slow_FOM_kg(layer) = P.Slow_FOM_kg(layer) + (-Dc.Slow_FOM_2_Slo_Org_kg(layer) - ...
            Dc.Slow_FOM_2_CO2_kg(layer)) * csoil_bio.P2C_Slow_FOM(layer);
        P.Uptake_kg(layer) = P.Uptake_kg(layer) + DP.Labile_2_Uptake_kg(layer);
    end % end if ISwphos
end

% Adjust for mulch OM transferred mechanically to the ground by rain:

C.Act_Org_kg(0) = C.Act_Org_kg(0) - Dc.Act_Org_Mulch_2_Soil_kg;
C.Slo_Org_kg(0) = C.Slo_Org_kg(0) - Dc.Slo_Org_Mulch_2_Soil_kg;
C.Res_Org_kg(0) = C.Res_Org_kg(0) - Dc.Res_Org_Mulch_2_Soil_kg;
n.Act_Org_kg(0) = n.Act_Org_kg(0) - DN.Act_Org_Mulch_2_Soil_kg;
n.Slo_Org_kg(0) = n.Slo_Org_kg(0) - DN.Slo_Org_Mulch_2_Soil_kg;
n.Res_Org_kg(0) = n.Res_Org_kg(0) - DN.Res_Org_Mulch_2_Soil_kg;

if ISwPho
    P.Act_Org_kg(0) = P.Act_Org_kg(0) - DP.Act_Org_Mulch_2_Soil_kg;
    P.Slo_Org_kg(0) = P.Slo_Org_kg(0) - DP.Slo_Org_Mulch_2_Soil_kg;
    P.Res_Org_kg(0) = P.Res_Org_kg(0) - DP.Res_Org_Mulch_2_Soil_kg;
end

C.Act_Org_kg(1) = C.Act_Org_kg(1) + Dc.Act_Org_Mulch_2_Soil_kg;
C.Slo_Org_kg(1) = C.Slo_Org_kg(1) + Dc.Slo_Org_Mulch_2_Soil_kg;
C.Res_Org_kg(1) = C.Res_Org_kg(1) + Dc.Res_Org_Mulch_2_Soil_kg;
n.Act_Org_kg(1) = n.Act_Org_kg(1) + DN.Act_Org_Mulch_2_Soil_kg;
n.Slo_Org_kg(1) = n.Slo_Org_kg(1) + DN.Slo_Org_Mulch_2_Soil_kg;
n.Res_Org_kg(1) = n.Res_Org_kg(1) + DN.Res_Org_Mulch_2_Soil_kg;

if ISwPho
    P.Act_Org_kg(1) = P.Act_Org_kg(1) + DP.Act_Org_Mulch_2_Soil_kg;
    P.Slo_Org_kg(1) = P.Slo_Org_kg(1) + DP.Slo_Org_Mulch_2_Soil_kg;
    P.Res_Org_kg(1) = P.Res_Org_kg(1) + DP.Res_Org_Mulch_2_Soil_kg;
end

% Update N:C, P:C ratios for FOM pools:

for layer = -1 : nlayr
    if C.Slow_FOM_kg(layer) > 0.000000001
        csoil_bio.N2C_Slow_FOM(layer) = n.Slow_FOM_kg(layer) / C.Slow_FOM_kg(layer);
    end
    if C.Rapid_FOM_kg(layer) > 0.000000001
        csoil_bio.N2C_Rapid_FOM(layer) = n.Rapid_FOM_kg(layer) / C.Rapid_FOM_kg(layer);
        if ISwPho
            csoil_bio.P2C_Rapid_FOM(layer) = P.Rapid_FOM_kg(layer) / C.Rapid_FOM_kg(layer);
        end
    end
    csoil_bio.N2C_Inter_FOM(layer) = 0;
    if ISwPho
        csoil_bio.P2C_Slow_FOM(layer) = 0;
        csoil_bio.P2C_Inter_FOM(layer) = 0;
    end
end

% Update cumulative leaching out of bottom of profile:
n.Leaching_kg = n.Leaching_kg + DN.Labile_2_Leaching_kg(nlayr);

% Update N in volatile pools:
n.Vol_Fert_kg = n.Vol_Fert_kg - DN.Vol_Fert_2_Atm_kg;
n.Vol_FOM_kg = n.Vol_FOM_kg - DN.Vol_FOM_2_Atm_kg;

% Update live plant nutrients:
if DAE >= 1 && ~Harvested
    n.Live_kg = n.Live_kg + sumd(DN.Labile_2_Uptake_kg(), 1, nlayr) + ...
        DN.Fix_kg - newRootFOM_kg_N;
    if ISwPho
        P.Live_kg = P.Live_kg + sumd(DP.Labile_2_Uptake_kg(), 1, nlayr) - ...
            newRootFOM_kg_P;
    end
    
    % Update N fixed:
    n.Fix_kg = n.Fix_kg + DN.Fix_kg;
end

% Update mulch dry weight for the water balance, assuming that mulch is approximately 40% C:
MULCHDW = 100 / 40 * (C.Rapid_FOM_kg(0) + C.Inter_FOM_kg(0) + ...
    C.Slow_FOM_kg(0));

% Cumulative CO2, C, N, and P budget:
Cum_C_CO2 = Cum_C_CO2 + sumd(Dc.Act_Inorg_2_CO2_kg(), -1, nlayr) + ...
    sumd(Dc.Act_Org_2_CO2_kg(), -1, nlayr) + ...
    sumd(Dc.Slo_Org_2_CO2_kg(), -1, nlayr) + ...
    sumd(Dc.Res_Org_2_CO2_kg(), -1, nlayr) + ...
    sumd(Dc.Rapid_FOM_2_CO2_kg(), -1, nlayr) + ...
    sumd(Dc.Inter_FOM_2_CO2_kg(), -1, nlayr) + ...
    sumd(Dc.Slow_FOM_2_CO2_kg(), -1, nlayr);
Cum_C_In = Cum_C_In + newFOM_kg_C + newFert_kg_C;
Cum_C_Out = Cum_C_CO2;
Cum_N_In = Cum_N_In + newFOM_kg_N + newFert_kg_N + DN.Fix_kg;
Cum_N_Out = Cum_N_Out + DN.Vol_Fert_2_Atm_kg + DN.Vol_FOM_2_Atm_kg + ...
    DN.Labile_2_Leaching_kg(nlayr) + DN.Live_2_Harvest_kg;
Cum_P_In = Cum_P_In + newFOM_kg_P + newFert_kg_P;
Cum_P_Out = Cum_P_Out + DP.Live_2_Harvest_kg;

end % Integrate_Soil_Biochemistry

function [Labile_kg] = AdjustDecomposition(Dc, csoil_bio, Labile_kg, layer)
% Added by Iurii on 11/21/08

%     Dim DN_Organic As Double, ...
%     N_Deficit As Double, tmp_N_pool As Double, Decomp_Adjustment_Factor As Double

DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
if Labile_kg(layer) >= DN_Organic
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end

% Stop Res_Org decomposition
Dc.Res_Org_2_CO2_kg(layer) = 0;
DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
if Labile_kg(layer) > DN_Organic
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end

%Adjust Slow_Org decomposition
tmp_N_pool = (-Dc.Slo_Org_2_Res_Org_kg(layer) - Dc.Slo_Org_2_CO2_kg(layer)) * N2C_Slo_Org + ...
    Dc.Slo_Org_2_Res_Org_kg(layer) * N2C_Res_Org;
N_Deficit = DN_Organic - Labile_kg(layer);
if tmp_N_pool <= max(N_Deficit, 0.0001)
    Dc.Slo_Org_2_Res_Org_kg(layer) = 0;
    Dc.Slo_Org_2_CO2_kg(layer) = 0;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
else
    Decomp_Adjustment_Factor = (tmp_N_pool - N_Deficit) / tmp_N_pool;
    Dc.Slo_Org_2_Res_Org_kg(layer) = Dc.Slo_Org_2_Res_Org_kg(layer) * Decomp_Adjustment_Factor;
    Dc.Slo_Org_2_CO2_kg(layer) = Dc.Slo_Org_2_CO2_kg(layer) * Decomp_Adjustment_Factor;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end

%Adjust Slow_FOM
tmp_N_pool = (-Dc.Slow_FOM_2_Slo_Org_kg(layer) - Dc.Slow_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Slow_FOM(layer) + ...
    Dc.Slow_FOM_2_Slo_Org_kg(layer) * N2C_Slo_Org;
N_Deficit = DN_Organic - Labile_kg(layer);
if tmp_N_pool <= max(N_Deficit, 0.0001)
    Dc.Slow_FOM_2_Slo_Org_kg(layer) = 0;
    Dc.Slow_FOM_2_CO2_kg(layer) = 0;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
else
    Decomp_Adjustment_Factor = (tmp_N_pool - N_Deficit) / tmp_N_pool;
    Dc.Slow_FOM_2_Slo_Org_kg(layer) = Dc.Slow_FOM_2_Slo_Org_kg(layer) * Decomp_Adjustment_Factor;
    Dc.Slow_FOM_2_CO2_kg(layer) = Dc.Slow_FOM_2_CO2_kg(layer) * Decomp_Adjustment_Factor;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end

%Adjust Inter_FOM
tmp_N_pool = (-Dc.Inter_FOM_2_Act_Org_kg(layer) - Dc.Inter_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Inter_FOM(layer) + ...
    Dc.Inter_FOM_2_Act_Org_kg(layer) * N2C_Act_Org;
N_Deficit = DN_Organic - Labile_kg(layer);
if tmp_N_pool <= max(N_Deficit, 0.0001)
    Dc.Inter_FOM_2_Act_Org_kg(layer) = 0;
    Dc.Inter_FOM_2_CO2_kg(layer) = 0;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
else
    Decomp_Adjustment_Factor = (tmp_N_pool - N_Deficit) / tmp_N_pool;
    Dc.Inter_FOM_2_Act_Org_kg(layer) = Dc.Inter_FOM_2_Act_Org_kg(layer) * Decomp_Adjustment_Factor;
    Dc.Inter_FOM_2_CO2_kg(layer) = Dc.Inter_FOM_2_CO2_kg(layer) * Decomp_Adjustment_Factor;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end

%Adjust Act_Org
tmp_N_pool = (-Dc.Act_Org_2_Slo_Org_kg(layer) - Dc.Act_Org_2_CO2_kg(layer)) * N2C_Act_Org + ...
    Dc.Act_Org_2_Slo_Org_kg(layer) * N2C_Slo_Org;
N_Deficit = DN_Organic - Labile_kg(layer);
if tmp_N_pool <= max(N_Deficit, 0.0001)
    Dc.Act_Org_2_Slo_Org_kg(layer) = 0;
    Dc.Act_Org_2_CO2_kg(layer) = 0;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
else
    Decomp_Adjustment_Factor = (tmp_N_pool - N_Deficit) / tmp_N_pool;
    Dc.Act_Org_2_Slo_Org_kg(layer) = Dc.Act_Org_2_Slo_Org_kg(layer) * Decomp_Adjustment_Factor;
    Dc.Act_Org_2_CO2_kg(layer) = Dc.Act_Org_2_CO2_kg(layer) * Decomp_Adjustment_Factor;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end

%Adjust Rapid_FOM
tmp_N_pool = (-Dc.Rapid_FOM_2_Act_Org_kg(layer) - Dc.Rapid_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Rapid_FOM(layer) + ...
    Dc.Rapid_FOM_2_Act_Org_kg(layer) * N2C_Act_Org;
N_Deficit = DN_Organic - Labile_kg(layer);
if tmp_N_pool <= max(N_Deficit, 0.0001)
    Dc.Rapid_FOM_2_Act_Org_kg(layer) = 0;
    Dc.Rapid_FOM_2_CO2_kg(layer) = 0;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer); %#ok<NASGU>
else
    Decomp_Adjustment_Factor = (tmp_N_pool - N_Deficit) / tmp_N_pool;
    Dc.Rapid_FOM_2_Act_Org_kg(layer) = Dc.Rapid_FOM_2_Act_Org_kg(layer) * Decomp_Adjustment_Factor;
    Dc.Rapid_FOM_2_CO2_kg(layer) = Dc.Rapid_FOM_2_CO2_kg(layer) * Decomp_Adjustment_Factor;
    DN_Organic = Get_DN_Organic(Dc, csoil_bio, layer);
    Labile_kg(layer) = Labile_kg(layer) - DN_Organic;
    return
end
end

function [DN_Organic] = Get_DN_Organic(Dc,csoil_bio,layer)
DN_Organic = (-Dc.Act_Org_2_Slo_Org_kg(layer)  - Dc.Act_Org_2_CO2_kg(layer) + ...
    Dc.Inter_FOM_2_Act_Org_kg(layer) + Dc.Rapid_FOM_2_Act_Org_kg(layer)) * N2C_Act_Org + ...
    (-Dc.Slo_Org_2_Res_Org_kg(layer) - Dc.Slo_Org_2_CO2_kg(layer) + ...
    Dc.Act_Org_2_Slo_Org_kg(layer) + Dc.Slow_FOM_2_Slo_Org_kg(layer)) * N2C_Slo_Org + ...
    (Dc.Slo_Org_2_Res_Org_kg(layer) - Dc.Res_Org_2_CO2_kg(layer)) * N2C_Res_Org + ...
    (-Dc.Rapid_FOM_2_Act_Org_kg(layer) - Dc.Rapid_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Rapid_FOM(layer) + ...
    (-Dc.Inter_FOM_2_Act_Org_kg(layer) - Dc.Inter_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Inter_FOM(layer) + ...
    (-Dc.Slow_FOM_2_Slo_Org_kg(layer) - Dc.Slow_FOM_2_CO2_kg(layer)) * csoil_bio.N2C_Slow_FOM(layer);
end