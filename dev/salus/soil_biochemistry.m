function [] = soil_biochemistry(C,...
    cSoil)
% function [] =Soil_Biochemistry(ByRef C As Soil_Bio_Element_Type, ByRef n As Soil_Bio_Element_Type, ...
%                               ByRef P As Soil_Bio_Element_Type, ByRef Dc As Soil_Bio_Delta_Element_Type, ...
%                               ByRef DN As Soil_Bio_Delta_Element_Type, ByRef DP As Soil_Bio_Delta_Element_Type, ...
%                               ByRef cSoil As SOIL_TYPE, ByRef csoil_bio As Soil_Bio_Type, ...
%                               ByRef Nitro As Plant_Nutrient_Type, ByRef Phos As Plant_Nutrient_Type, ...
%                               ByVal cropmod As String, ByVal DAE As Integer, ByVal dBiomass As Single, ...
%                               ByRef Root_Layer_Frac() As Single, ...
%                               ByRef Live_Root_2_FOM_Frac As Single, ByRef NFac As Single, ...
%                               ByRef PFac As Single, ByVal Nfixing As String, ...
%                               ByVal Lag_Req_4_NFix_Is_Met As Boolean, ByVal rel_TT As Single, ...
%                               ByVal topWt As Single, ByVal rootWt As Single, ...
%                               ByRef Sen_rootWt As Single, ByVal grainWt As Single, ...
%                               ByVal Root_Part_Coeff As Single, ByVal Root_Depth As Single, ...
%                               ByVal Root_Front_Layer As Integer, ByVal RootC As Single, ...
%                               ByVal RootSloC As Single, ByVal RootIntC As Single, ...
%                               ByVal RootSloN As Single, ByVal C_to_Nod_Tot_kg As Double, ...
%                               ByVal Resp_Param_to_Fix_N As Double, ByVal Harvested As Boolean, ...
%                               ByVal Ambient_Temperature As Single, ...
%                               ByVal Rain As Single, ByRef newRootFOM_kg_C As Double, ...
%                               ByRef newRootFOM_kg_N As Double, ByRef
%                               newRootFOM_kg_P As Double)

% +-----------------------------------------------------------------------
% | Soil Biochemistry science functions and subroutines
% +-----------------------------------------------------------------------
% | Routines:  Chem_Dif, Chem_Par, Integrate_Soil_Biochemistry,
% | Soil_Biochemistry, Soil_Biochemistry_MassBalance
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 14 oct 1999
% | Modified by: aris gerakis, 22 nov 1999:
% | 1.  Added certain constants.
% | 2.  Updated variable definitions.  Declared all real variables double
% |     precision to eliminate mass balance errors.
% | Modified by:  aris gerakis - 04 apr 2000: Removed inoculation pools
% | Modified by:  B.D.Baer - 11 sep 2006: added N_N2 and N_NO2
% +-----------------------------------------------------------------------

%**** CONSTANTS
%ADK: Move these elsewhere

% Convert % to mg kg-1 (ppm):
percent2ppm = 10000;
ppm2percent = 1 / 10000;

% C/N, C/P ratios.  According to Eldor Paul these can remain fixed (30 sep 1999):
C2N_Act_Org = 8;
C2N_Slo_Org = 11;
C2N_Res_Org = 9;

C2P_Act_Org = 80; % JTR '40 % Samira Daroub 03 Dec 1999
C2P_Slo_Org = 107; % Samira Daroub 03 Dec 1999
C2P_Res_Org = 107; % Samira Daroub 03 Dec 1999

% Inverse of C/N, C/P ratios:
N2C_Act_Org = C2N_Act_Org ^ -1;
N2C_Slo_Org = C2N_Slo_Org ^ -1;
N2C_Res_Org = C2N_Res_Org ^ -1;

P2C_Act_Org = C2P_Act_Org ^ -1;
P2C_Slo_Org = C2P_Slo_Org ^ -1;
P2C_Res_Org = C2P_Res_Org ^ -1;

% Fractions to initialize Resistant C and Active Inorganic C from Total C and
% Total Inorganic C, respectively.  According to Ann-Marie Ezanno these can
% remain fixed (3 dec 1999):
Fraction_Res_Org_in_Tot_Org_C = 0.5;
Fraction_Act_Inorg_in_Tot_Inorg_C = 0.05;

% Decomposition rates for FOM pools are expressed as the slowest rate,
% k_Slo_FOM(), a user input, times one of these multipliers.
% These multipliers can be hardwired according to JTR (3 dec 1999):
Rap_FOM_Above_k_Modifier = 18.2;
Rap_FOM_Below_k_Modifier = 20;
Int_FOM_Above_k_Modifier = 1.7;
Int_FOM_Below_k_Modifier = 8;

% Decomposition rates for organic C pools are expressed as the fastest rate,
% k_Act_Org (a user input), times one of these multipliers.
% These multipliers can be hardwired according to JTR (3 dec 1999):
Slo_Org_k_Modifier = 0.015;
Res_Org_k_Modifier = 0.0045;

% Min. OC in Active C as fraction of Resistant C - JTR (17 Dec 1999):
Min_Act_Org_Fraction = 0.04;

% Minimum quantity in any pool, to avoid negative Labile N (kg ha-1):
Min_kg_in_any_pool = 5;


% +-----------------------------------------------------------------------
% | Master soil biochemistry routine.  Must be called AFTER the
% | crop module because it needs emergence status and maturity status.
% +-----------------------------------------------------------------------
% | Calls: Chem_Par, Env_Modifiers
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 16 oct 1999
% +-----------------------------------------------------------------------

% Get values for all the environmental modifiers (minus clay factor which is only
% calculated once):

[cSoil.NsuffFac, cSoil.PsuffFac, cSoil.rainFac, cSoil.saturationFac, ...
    cSoil.CFac, cSoil.tempFac, cSoil.waterFac] = ...
    Env_Modifiers(Ambient_Temperature, cSoil.AD, C.Act_Org_kg, C.Rapid_FOM_kg, ...
    n.Labile_kg, cSoil.nlayr, P.Labile_kg, Rain, cSoil.OC(), cSoil.SAT(), cSoil.ST(), cSoil.SW());

% Partition chemicals into compartments.  Only rate calculation is done here:

[] = Chem_Par(C, n, P, Dc, DN, DP, Nitro, Phos, csoil_bio, cropmod, ...
    DAE, C_to_Nod_Tot_kg, Lag_Req_4_NFix_Is_Met, ...
    Resp_Param_to_Fix_N, Live_Root_2_FOM_Frac, ...
    rel_TT, dBiomass, Root_Layer_Frac(), topWt, rootWt, ...
    Sen_rootWt, grainWt, Root_Part_Coeff, Root_Depth, ...
    Root_Front_Layer, RootC, RootSloC, RootIntC, RootSloN, Harvested, ...
    NFac, PFac, Nfixing, cSoil.dlayr(), cSoil.DUL(), ...
    cSoil.FlowD(), cSoil.FlowU(), cSoil.nlayr, ...
    cSoil.ROOTWATUP(), cSoil.SAT(), cSoil.ST(), cSoil.SW(), ...
    cSoil.Zlayr(), newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P);

end %Soil_Biochemistry


function [] = Chem_Par()
% Private Sub Chem_Par(ByRef C As Soil_Bio_Element_Type, ByRef n As Soil_Bio_Element_Type, ...
%                       ByRef P As Soil_Bio_Element_Type, ByRef Dc As Soil_Bio_Delta_Element_Type, ...
%                       ByRef DN As Soil_Bio_Delta_Element_Type, ByRef DP As Soil_Bio_Delta_Element_Type, ...
%                       ByRef Nitro As Plant_Nutrient_Type, ByRef Phos As Plant_Nutrient_Type, ...
%                       ByRef csoil_bio As Soil_Bio_Type, ByVal cropmod As String, ...
%                       ByVal DAE As Integer, ...
%                       ByVal C_to_Nod_Tot_kg As Double, ByVal Lag_Req_4_NFix_Is_Met As Boolean, ...
%                       ByVal Resp_Param_to_Fix_N As Double, ByVal Live_Root_2_FOM_Frac As Single, ...
%                       ByVal rel_TT As Single, ByVal dBiomass As Single, ...
%                       ByRef Root_Layer_Frac() As Single, ByVal topWt As Single, ...
%                       ByVal rootWt As Single, ByRef Sen_rootWt As Single, ...
%                       ByVal grainWt As Single, ByVal Root_Part_Coeff As Single, ...
%                       ByVal Root_Depth As Single, ByVal Root_Front_Layer As Integer, ...
%                       ByVal RootC As Single, ByVal RootSloC As Single, ...
%                       ByVal RootIntC As Single, ByVal RootSloN As Single, ...
%                       ByVal Harvested As Boolean, ...
%                       ByRef NFac As Single, ByRef PFac As Single, ...
%                       ByVal Nfixing As String, ByRef dlayr() As Single, ...
%                       ByRef DUL() As Single, ByRef FlowD() As Single, ...
%                       ByRef FlowU() As Single, ByVal nlayr As Integer, ...
%                       ByRef rwu() As Single, ...
%                       ByRef SAT() As Single, ByRef ST() As Single, ByRef SW() As Single, ...
%                       ByRef zlyr() As Single, ByRef newRootFOM_kg_C As Double, ...
%                       ByRef newRootFOM_kg_N As Double, ByRef newRootFOM_kg_P As Double)


% +-----------------------------------------------------------------------
% | Partition C, N, P into compartments.  Only rate calculation is done
% | here.
% +-----------------------------------------------------------------------
% | Calls:  chem_dif
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 14 oct 1999
% +-----------------------------------------------------------------------

% Dim layer As Integer
% Dim C_Cost_for_Nod_Tot_kg As Double, ch_demand_kg As Double, ...
%     ch_supply_kg As Double
% Dim FOM_kg As Double, IncorporationPercent As Double, IncorporationDepth As Single, ...
%     Percent_C As Double, Percent_N As Double, Percent_P As Double, ...
%     Percent_Slow_C As Double, Percent_Inter_C As Double, ...
%     Percent_Slow_N As Double, Fraction_Vol_N As Double, ...
%     newResFOM_kg_C As Double, newResFOM_kg_N As Double, ...
%     newResFOM_kg_P As Double
% Dim k_Rap_FOM_X(-1 : MAX_LAYER) As Double, ...
%     k_Int_FOM_X(-1 : MAX_LAYER) As Double, ...
%     k_Slo_FOM_X(-1 : MAX_LAYER) As Double

for layer = -1 : nlayr % above and below ground
    
    % Delta C that leaves to another pool.  Immobilization or mineralization of C & N of
    % FOM happens only in the FOM pool containing hemicellulose/cellulose. The
    % FOM pool containing lignin is assumed to be controled by the k only. The soluble
    % pool is considered to have no N limitation - Ann-Marie Ezanno, 21 Nov 1999.
    % Therefore NsuffFac and PsuffFac are used only to multiply Intermediate FOM - AG:
    
    % The problem is, labile soil N often becomes negative. This is a result of
    % decomposing organic materials that are applied on or into the soil that are poor in N.
    % Because the C:N ratios of the OM pools are fixed (approximately 10:1), any fresh
    % OM that is poorer in N must "steal" N from the labile N pool in order to decompose.
    % Even when labile N is near zero, fresh OM continues to decompose and deplete soil
    % N, and inevitably causes labile N to drop below zero. The N sufficiency factor
    % controls only the turnover rates of the intermediate pool. - AG
    
    Dc.Fertilizer_2_Act_Inorg_kg(layer) = Chem_Dif(C.Fertilizer_kg(layer), C.k_Fertilizer(layer), ...
        1, csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer));
    
    Dc.Slo_Inorg_2_Act_Inorg_kg(layer) = Chem_Dif(C.Slo_Inorg_kg(layer), C.k_Slo_Inorg, ...
        1, csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer));
    
    Dc.Act_Org_2_Slo_Org_kg(layer) = Chem_Dif(C.Act_Org_kg(layer) - Min_Act_Org_Fraction * ...
        C.Res_Org_kg(layer), C.k_Act_Org, ...
        csoil_bio.clayFac(layer), csoil_bio.waterFac(layer), 1, 1,...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer)) * ...
        (1 - csoil_bio.f_Act_Org_2_CO2);
    
    Dc.Slo_Org_2_Res_Org_kg(layer) = Chem_Dif(C.Slo_Org_kg(layer), C.k_Slo_Org, ...
        csoil_bio.clayFac(layer), csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer)) * ...
        (1 - csoil_bio.f_Slo_Org_2_CO2);
    
    if layer >= 1 % below ground
        Dc.Rapid_FOM_2_Act_Org_kg(layer) = Chem_Dif(C.Rapid_FOM_kg(layer), C.k_Rap_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * ...
            (1 - csoil_bio.f_Rapid_FOM_2_CO2); % * (1 + (csoil_bio.TillDecompFac(layer) - 1) / 1))
        
        Dc.Inter_FOM_2_Act_Org_kg(layer) = Chem_Dif(C.Inter_FOM_kg(layer), C.k_Int_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * ...
            (1 - csoil_bio.f_Inter_FOM_2_CO2); % * (1 + (csoil_bio.TillDecompFac(layer) - 1) / 1))
        
        Dc.Slow_FOM_2_Slo_Org_kg(layer) = Chem_Dif(C.Slow_FOM_kg(layer), C.k_Slo_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * ...
            (1 - csoil_bio.f_Slow_FOM_2_CO2); % * (1 + (csoil_bio.TillDecompFac(layer) - 1) / 1)) % (1 - csoil_bio.f_Slow_FOM_2_CO2) changed by Iurri and Bruno on Aug 20, 2008 in Rome to increase CO2 loss and less C to slow Org C when tillage'
    
    elseif layer == 0 % surface/mulch
        Dc.Rapid_FOM_2_Act_Org_kg(layer) = Chem_Dif(C.Rapid_FOM_kg(layer), C.k_Rap_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ....
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * ...
            (1 - csoil_bio.f_Rapid_FOM_2_CO2);
        
        Dc.Inter_FOM_2_Act_Org_kg(layer) = Chem_Dif(C.Inter_FOM_kg(layer), C.k_Int_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * ...
            (1 - csoil_bio.f_Inter_FOM_2_CO2);
        
        Dc.Slow_FOM_2_Slo_Org_kg(layer) = Chem_Dif(C.Slow_FOM_kg(layer), C.k_Slo_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * ...
            (1 - csoil_bio.f_Slow_FOM_2_CO2);
    
    elseif layer == -1 % standing dead
        % Only the rapid part of standing dead FOM decomposes, to CO2:
        Dc.Rapid_FOM_2_Act_Org_kg(layer) = 0;
        Dc.Inter_FOM_2_Act_Org_kg(layer) = 0;
        Dc.Slow_FOM_2_Slo_Org_kg(layer) = 0;
    end
    
    % Delta C that goes to CO2:
    Dc.Act_Inorg_2_CO2_kg(layer) = Chem_Dif(C.Act_Inorg_kg(layer), C.k_Act_Inorg, ...
        1, csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer)) * ...
        csoil_bio.f_Act_Inorg_2_CO2;
    
    Dc.Act_Org_2_CO2_kg(layer) = Chem_Dif(C.Act_Org_kg(layer) - Min_Act_Org_Fraction * ...
        C.Res_Org_kg(layer), C.k_Act_Org, ...
        csoil_bio.clayFac(layer), csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer)) * ...
        csoil_bio.f_Act_Org_2_CO2;
    
    Dc.Slo_Org_2_CO2_kg(layer) = Chem_Dif(C.Slo_Org_kg(layer), C.k_Slo_Org, ...
        csoil_bio.clayFac(layer), csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer)) * ...
        csoil_bio.f_Slo_Org_2_CO2;
    
    % The only loss from Resistant Organic is to CO2:
    Dc.Res_Org_2_CO2_kg(layer) = Chem_Dif(C.Res_Org_kg(layer), C.k_Res_Org, ...
        csoil_bio.clayFac(layer), csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer));
    
    if layer >= 1 % below ground
        Dc.Rapid_FOM_2_CO2_kg(layer) = Chem_Dif(C.Rapid_FOM_kg(layer), C.k_Rap_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * csoil_bio.f_Rapid_FOM_2_CO2;
        
        Dc.Inter_FOM_2_CO2_kg(layer) = Chem_Dif(C.Inter_FOM_kg(layer), C.k_Int_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * csoil_bio.f_Inter_FOM_2_CO2;
        
        Dc.Slow_FOM_2_CO2_kg(layer) = Chem_Dif(C.Slow_FOM_kg(layer), C.k_Slo_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * csoil_bio.f_Slow_FOM_2_CO2;
        
    elseif layer == 0 % surface/mulch
        Dc.Rapid_FOM_2_CO2_kg(layer) = Chem_Dif(C.Rapid_FOM_kg(layer), C.k_Rap_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * csoil_bio.f_Rapid_FOM_2_CO2;
        
        Dc.Inter_FOM_2_CO2_kg(layer) = Chem_Dif(C.Inter_FOM_kg(layer), C.k_Int_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * csoil_bio.f_Inter_FOM_2_CO2;
        
        Dc.Slow_FOM_2_CO2_kg(layer) = Chem_Dif(C.Slow_FOM_kg(layer), C.k_Slo_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, csoil_bio.NsuffFac(layer), ...
            csoil_bio.PsuffFac(layer), csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer)) * csoil_bio.f_Slow_FOM_2_CO2;
        
    elseif layer == -1 % standing dead
        % Only the rapid part of standing dead FOM decomposes, to CO2:
        Dc.Rapid_FOM_2_CO2_kg(layer) = Chem_Dif(C.Rapid_FOM_kg(layer), C.k_Rap_FOM(layer), ...
            1, csoil_bio.waterFac(layer), 1, 1, ...
            1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer));
        
        Dc.Inter_FOM_2_CO2_kg(layer) = 0;
        Dc.Slow_FOM_2_CO2_kg(layer) = 0;
    end
    
    % Integration of C at the end of the day in Integrate_Soil_Biochemistry
    
    % Delta N that leaves pool.  if there was DC that also left that pool,
    % then compute DN as a function of DC and N:C.  However, if there is
    % no equivalent DC, such as from N fertilizer to labile, apply logic similar
    % to computation of DC:
    
    DN.Fertilizer_2_Labile_kg(layer) = Chem_Dif(n.Fertilizer_kg(layer), n.k_Fertilizer(layer), ...
        1, csoil_bio.waterFac(layer), 1, 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer));
    
    DN.Labile_2_Atmosphere_kg(layer) = Chem_Dif(n.Labile_kg(layer) - Min_kg_in_any_pool, n.k_Labile, ...
        1, csoil_bio.saturationFac(layer), csoil_bio.CFac(layer), 1, ...
        1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
        csoil_bio.TillDecompFac(layer));
    
    % Delta P that leaves pool.  if there was DC that also left that pool,
    % then compute DP as a function of DC and P:C.  However, if there is
    % no equivalent DC, such as from fertilizer to labile, apply logic similar
    % to computation of DC:
    
    if ISwPho
        DP.Fertilizer_2_Labile_kg(layer) = Chem_Dif(P.Fertilizer_kg(layer), P.k_Fertilizer(layer), ...
            1, csoil_bio.waterFac(layer), 1, 1, ...
            1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer));
        
        DP.Act_Inorg_2_Labile_kg(layer) = Chem_Dif(P.Act_Inorg_kg(layer), P.k_Act_Inorg_2_Labile, ...
            1, csoil_bio.waterFac(layer), 1, 1, ...
            1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer));
        
        DP.Act_Inorg_2_Slo_Inorg_kg(layer) = Chem_Dif(P.Act_Inorg_kg(layer), ...
            (P.k_Act_Inorg - P.k_Act_Inorg_2_Labile), ...
            1, csoil_bio.waterFac(layer), 1, 1, ...
            1, csoil_bio.tempFac(layer), csoil_bio.depthFac(layer), ...
            csoil_bio.TillDecompFac(layer));
        
        % Integration of P at the end of the day in Integrate_Soil_Biochemistry
    end % end if ISwpho
    
end

% Amounts of mulch to topsoil.  Only rain factor is relevant because the energy for
% the transfer is provided by rain:
Dc.Act_Org_Mulch_2_Soil_kg = Chem_Dif(C.Act_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
    1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);

Dc.Slo_Org_Mulch_2_Soil_kg = Chem_Dif(C.Slo_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
    1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);

Dc.Res_Org_Mulch_2_Soil_kg = Chem_Dif(C.Res_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
    1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);

DN.Act_Org_Mulch_2_Soil_kg = Chem_Dif(n.Act_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
    1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);

DN.Slo_Org_Mulch_2_Soil_kg = Chem_Dif(n.Slo_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
    1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);

DN.Res_Org_Mulch_2_Soil_kg = Chem_Dif(n.Res_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
    1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);

if ISwPho
    DP.Act_Org_Mulch_2_Soil_kg = Chem_Dif(P.Act_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
        1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);
    
    DP.Slo_Org_Mulch_2_Soil_kg = Chem_Dif(P.Slo_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
        1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);
    
    DP.Res_Org_Mulch_2_Soil_kg = Chem_Dif(P.Res_Org_kg(0), csoil_bio.k_Mulch_2_Soil, ...
        1, csoil_bio.rainFac, 1, 1, 1, 1, 1, 1);
end

% N in volatile pool that volatilizes, so far modified only by temperature:
DN.Vol_Fert_2_Atm_kg = Chem_Dif(n.Vol_Fert_kg, n.k_Fert_Vol, ...
    1, 1, 1, 1, 1, csoil_bio.tempFac(0), 1, 1);

DN.Vol_FOM_2_Atm_kg = Chem_Dif(n.Vol_FOM_kg, n.k_FOM_Vol, ...
    1, 1, 1, 1, 1, csoil_bio.tempFac(0), 1, 1);

% Inorganic N movement between layers and leaching:
[] = Nflux(dlayr(), DUL(), FlowD(), FlowU(), csoil_bio.kg2ppm(), nlayr, ...
    DN.Labile_2_Leaching_kg(), DN.Labile_2_Other_Layr_kg(), SAT(), n.Labile_kg(), ...
    ST(), SW());

% Plant uptake, and C that moves to the roots for respiration, exhudation, etc.:
if DAE >= 1 && ~Harvested
    
    %N uptake:
    if DAE == 1 %this block corrects for N mass balance errors
        n.TotMassYest_kg = n.TotMassYest_kg - n.Live_kg;
        n.Live_kg = 0;
    end
    [] = Chem_Uptake(Nitro, cropmod, DAE, topWt, rootWt, grainWt, ...
        n.Fraction_Labile_in_Sol, n.Live_kg, ...
        n.Labile_kg(), ch_supply_kg, ch_demand_kg, ...
        DN.Labile_2_Uptake_kg(), NFac, dlayr(), nlayr, ...
        rel_TT, Root_Layer_Frac(), rwu(), zlyr());
    
    % Fix N if symbiotic switch is on and if thermal time exceeds the lag
    % phase for N-fixation.  Nodules are not grown explicitely:
    if (upper(Nfixing) == 'Y') && Lag_Req_4_NFix_Is_Met
        % N fixing supplements N needs as in CROPGRO:
        DN.Fix_kg = max(0, ch_demand_kg - sumd(DN.Labile_2_Uptake_kg(), 1, nlayr));
        % Assimilate C consumed for N-fixation, as in CROPGRO.  The  respiration
        % parameter is higher than in CROPGRO to reflect that we do not
        % calculate protein:
        C_Cost_for_Nod_Tot_kg = Resp_Param_to_Fix_N * DN.Fix_kg;
        % No stress:
        NFac = 1;
        % Assimilate C reserved for nodules is a fixed fraction of assimilate C
        % that goes to roots.  if this C is inadequate, scale back N fixation
        % and N stress factor (this adjustment not in CROPGRO):
        if C_Cost_for_Nod_Tot_kg > C_to_Nod_Tot_kg;
            DN.Fix_kg = DN.Fix_kg * C_to_Nod_Tot_kg / C_Cost_for_Nod_Tot_kg;
            C_Cost_for_Nod_Tot_kg = C_to_Nod_Tot_kg;
            % Stress function by JTR that has worked well for P:
            NFac = 1 - (min(1, (ch_supply_kg + DN.Fix_kg) / ...
                max(0.000001, ch_demand_kg)) - 1) ^ 4;
        end
        % Zero local variables:
        ch_demand_kg = 0;
        ch_supply_kg = 0;
    else % if no N-fixing
        DN.Fix_kg = 0;
    end
    
    if ISwPho
        %P uptake:
        [] = Chem_Uptake(Phos, cropmod, DAE, topWt, rootWt, grainWt, ...
            P.Fraction_Labile_in_Sol, P.Live_kg, ...
            P.Labile_kg(), ch_supply_kg, ch_demand_kg, ...
            DP.Labile_2_Uptake_kg(), PFac, dlayr(), nlayr, ...
            rel_TT, Root_Layer_Frac(), rwu(), zlyr());
    end
    
    % A fraction of assimilate C goes to roots (Root Partition Coefficient).  Of that,
    % a fraction goes to live root growth, and the rest to the soluble pool of FOM
    % (about 0.1 = 1 - Frac_of_New_Root_Biomass_4_Root_Growth) that has a high turnover rate.
    % This way we don't have to deal with respiration directly. It is essential that we
    % send into the soluble pool not just C but N and P, too, otherwise we run into
    % immobilization problems pretty fast:  'AG
    
    FOM_kg = dBiomass * Root_Part_Coeff * (1 - ...
        Frac_of_New_Root_Biomass_4_Root_Growth) * gpm2_to_kgpha;
    IncorporationPercent = 100;
    IncorporationDepth = Root_Depth;
    Percent_C = RootC;
    Percent_Slow_C = RootSloC;
    Percent_Inter_C = RootIntC;
    Percent_Slow_N = RootSloN;
    Fraction_Vol_N = 0;
    if ((topWt + rootWt) > 0)
        Percent_N = n.Live_kg / gpm2_to_kgpha * (topWt + rootWt) * 100;
        if ISwPho
            Percent_P = P.Live_kg / gpm2_to_kgpha * (topWt + rootWt) * 100;
        end
    end
    for layer = -1 : nlayr
        k_Rap_FOM_X(layer) = 0;
        k_Int_FOM_X(layer) = 0;
        k_Slo_FOM_X(layer) = 0;
    end
    [C, n, P, csoil_bio, newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P, MassError] = ...
        FOM_Placement(FOM_kg, k_Slo_FOM_X, k_Int_FOM_X, k_Rap_FOM_X, IncorporationPercent,...
        Root_Front_Layer, Root_Depth, Root_Layer_Frac, IncorporationDepth,...
        Percent_C, Percent_N, Percent_P, Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, Fraction_Vol_N,...
        C, n, P, csoil_bio, dlayr, nlayr, newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P);
    % Root senescence.  In the simple crop model, everyday a fraction of the
    % existing live root C goes to FOM (time-variable:  starts at 0.001 and
    % goes non-linearly to 0.1.  It's close to nothing during the first 1/2
    % of the plant life). In the complex crop model, things are simpler:
    % there are only two values, depending on development stage:
    
    FOM_kg = rootWt * gpm2_to_kgpha * Live_Root_2_FOM_Frac;
    IncorporationPercent = 100;
    IncorporationDepth = Root_Depth;
    Percent_C = RootC;
    Percent_Slow_C = RootSloC;
    Percent_Inter_C = RootIntC;
    Percent_Slow_N = RootSloN;
    Fraction_Vol_N = 0;
    if (topWt + rootWt) > 0
        Percent_N = n.Live_kg / gpm2_to_kgpha * (topWt + rootWt) * 100;
        if ISwPho
            Percent_P = P.Live_kg / gpm2_to_kgpha * (topWt + rootWt) * 100;
        end
    end
    for layer = -1 : nlayr
        k_Rap_FOM_X(layer) = 0;
        k_Int_FOM_X(layer) = 0;
        k_Slo_FOM_X(layer) = 0;
    end
    [C, n, P, csoil_bio, newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P, MassError] = ...
        FOM_Placement(FOM_kg, k_Slo_FOM_X, k_Int_FOM_X, k_Rap_FOM_X, IncorporationPercent,...
        Root_Front_Layer, Root_Depth, Root_Layer_Frac, IncorporationDepth,...
        Percent_C, Percent_N, Percent_P, Percent_Slow_C, Percent_Inter_C, Percent_Slow_N, Fraction_Vol_N,...
        C, n, P, csoil_bio, dlayr, nlayr, newRootFOM_kg_C, newRootFOM_kg_N, newRootFOM_kg_P);
    % It is critical to save the senesced root mass in order to subtract it from rootWt,
    % otherwise next day rootWt will be larger than actual, therefore senescence will be
    % larger and so on, which creates a positive feedback loop:
    Sen_rootWt = FOM_kg * kgpha_to_gpm2;
else
    
    for layer = 1 : nlayr
        DN.Labile_2_Uptake_kg(layer) = 0;
        if ISwPho
            DP.Labile_2_Uptake_kg(layer) = 0;
        end
    end
    
end %growing and not harvested today

end %Chem_Par


function [chemDiff] = Chem_Dif(Pool,Constant,clayFac,waterFac,CFac,NsuffFac,PsuffFac,tempFac,depthFac,TillDecompFac)
% +-----------------------------------------------------------------------
% | Returns the delta C, N, P for each pool.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 15 oct 1999
% +-----------------------------------------------------------------------

% Made tillage decomposition a mandatory multiplier because it may exceed 1:
if Pool > Min_kg_in_any_pool
    if waterFac <= 1 % most of the time
        chemDiff = Pool .* min(1, Constant .* tempFac .* clayFac .* TillDecompFac .* ...
            min(cat(3,waterFac,NsuffFac, PsuffFac, CFac, depthFac),[],3));
    else % for standing dead and mulch waterFac (or rainFac) can be > 1 acc. to JTR
        chemDiff = Pool .* min(1, Constant .* tempFac .* clayFac .* TillDecompFac .* waterFac .* ...
            min(cat(3,NsuffFac, PsuffFac, CFac, depthFac),[],3));
    end
else
    chemDiff = 0;
end

end % Chem_Dif

function [NsuffFac, PsuffFac, rainFac, saturationFac, CFac, tempFac, waterFac] = ...
    Env_Modifiers(Ambient_Temperature, AD, C_Act_Org_kg, C_Rapid_FOM_kg, ...
    N_Labile_kg, nlayr, P_Labile_kg, Rain, OC, SAT, ST, SW)
% Private Sub Env_Modifiers(ByVal Ambient_Temperature As Single, ...
%                           ByRef AD() As Single, ByRef BD() As Single, ...
%                           ByRef C_Act_Org_kg() As Double, ByRef dlayr() As Single, ...
%                           ByRef FlowD() As Single, ByRef FlowU() As Single, ...
%                           ByRef C_Rapid_FOM_kg() As Double, ByRef N_Labile_kg() As Double, ...
%                           ByVal nlayr As Integer, ByRef NsuffFac() As Double, ...
%                           ByRef P_Labile_kg() As Double, ByRef PsuffFac() As Double, ...
%                           ByVal Rain As Single, ByRef rainFac As Double, ...
%                           ByRef saturationFac() As Double, ByRef CFac() As Double, ...
%                           ByRef OC() As Single, ByRef SAT() As Single, ...
%                           ByRef ST() As Single, ...
%                           ByRef SW() As Single, ByRef SWCN() As Single, ...
%                           ByRef tempFac() As Double, ByRef waterFac() As Double)

% +-----------------------------------------------------------------------
% | Environmental modifiers of soil C transformation rates.  Clay factor
% | has been calculated once in Setup_Soil_Biochemistry.
% +-----------------------------------------------------------------------
% | Calls:
% +-----------------------------------------------------------------------
% | Created by: aris gerakis, 11 dec 1999
% | Modified by: aris gerakis, 27 jul 2001:  Did nutrient sufficiency diff-
% |              erently for the aboveground layers.
% |              bruno and valentina, 17 may 2003: The rate modifier coeff
% |              NsuffFac(layer) has been set to 0 when Min N/Act C < 0.001
% +-----------------------------------------------------------------------

% Declare constants:
Threshold_ratio_N = 0.01;  % Threshold mineral N:Active C ratio for beginning of reduction of decomposition - JTR
Threshold_ratio_P = 0.003; % Threshold mineral P:Active C ratio for beginning of reduction of decomposition - JTR
Crit_Wat_Fill_Por = 0.63;  % Critical N water filled porosity for denitrification:

% Declare local variables:
RainDay = (Rain > 0);

for layer = -1 : nlayr
    
    if layer >= 1  % for buried stuff
        
        % C factor for denitrification:
        
        CFac(layer) = 1 - 1 / (1 + (OC(layer) / 0.25) ^ 3);
        
        % Saturation factor for denitrification:
        
        saturationFac(layer) = 1 / (1 + ((SW(layer) - SAT(layer)) / -0.02) ^ 3);
        
        % Water factor:
        
        if SW(layer) < 0.4 % dry side (JTR - 18 Nov 1999)
            waterFac(layer) = min(1, max(0, 1 - 0.98 / (1 + ((SW(layer) - 0.07) / ...
                0.025) ^ 3)));
        else % wet side (JTR - 01 Jan 2000)
            waterFac(layer) = 1 - saturationFac(layer);
        end
        
    elseif layer == 0 % for mulch
        
        if RainDay
            waterFac(layer) = 1;
        else
            waterFac(layer) = min(0.8, max(0, (SW(1) - AD(1) - 0.002) * 16.66));
        end
        
    elseif layer == -1 % for standing dead
        
        if RainDay
            waterFac(layer) = 2;
        else
            waterFac(layer) = 0.1;
        end
    end
    
    % N & P sufficiency factors:
    
    if layer >= 1 % below ground
        
        % N sufficiency factor (JTR - 14 Dec 1999; JTR, Bruno, Vale - 29 Nov 2002):
        
        if N_Labile_kg(layer) > Min_kg_in_any_pool && C_Act_Org_kg(layer) > 0
            if N_Labile_kg(layer) / C_Act_Org_kg(layer) > Threshold_ratio_N
                NsuffFac(layer) = 1;
            elseif N_Labile_kg(layer) / C_Act_Org_kg(layer) < 0.001
                NsuffFac(layer) = 0;
            else
                NsuffFac(layer) = max(0, 1 / Threshold_ratio_N * N_Labile_kg(layer) / ...
                    C_Act_Org_kg(layer));
            end
        else % if no labile N then we can't decompose (AG - 07 Apr 2001)
            NsuffFac(layer) = 0;
        end
        
        % P sufficiency factor (JTR - 14 Dec 1999):
        
        if ISwPho
            if P_Labile_kg(layer) > Min_kg_in_any_pool && C_Act_Org_kg(layer) > 0
                if P_Labile_kg(layer) / C_Act_Org_kg(layer) > Threshold_ratio_P
                    PsuffFac(layer) = 1;
                else
                    PsuffFac(layer) = max(0, 1 / Threshold_ratio_P * P_Labile_kg(layer) / ...
                        C_Act_Org_kg(layer));
                end
            else % if no labile P then we can't decompose (AG - 07 Apr 2001)
                PsuffFac(layer) = 0;
            end
        end
        
    else % above ground
        
        % N sufficiency factor (26 jul 2001):
        
        if N_Labile_kg(layer) >= Min_kg_in_any_pool && C_Rapid_FOM_kg(layer) > 0
            if N_Labile_kg(layer) / C_Rapid_FOM_kg(layer) > Threshold_ratio_N
                NsuffFac(layer) = 1;
            elseif N_Labile_kg(layer) / C_Rapid_FOM_kg(layer) < 0.001
                NsuffFac(layer) = 0;
            else
                NsuffFac(layer) = max(0, 1 / Threshold_ratio_N * N_Labile_kg(layer) / ...
                    C_Rapid_FOM_kg(layer));
            end
        else % if no labile N then we can't decompose (AG - 07 Apr 2001)
            NsuffFac(layer) = 0;
        end
        
        % P sufficiency factor (26 jul 2001):
        
        if ISwPho
            if P_Labile_kg(layer) >= Min_kg_in_any_pool && C_Rapid_FOM_kg(layer) > 0
                if P_Labile_kg(layer) / C_Rapid_FOM_kg(layer) > Threshold_ratio_P
                    PsuffFac(layer) = 1;
                else
                    PsuffFac(layer) = max(0, 1 / Threshold_ratio_P * P_Labile_kg(layer) / ...
                        C_Rapid_FOM_kg(layer));
                end
            else % if no labile P then we can't decompose (AG - 07 Apr 2001)
                PsuffFac(layer) = 0;
            end
        end
        
    end
    
    % Temperature factor (JTR - 14 Dec 1999):
    
    if layer >= 1 % for buried stuff
        Temperature = ST(layer);
    else % for aboveground stuff
        Temperature = Ambient_Temperature;
    end
    
    if Temperature < 30
        tempFac(layer) = 0.177 * exp(0.069 * Temperature);
    else
        tempFac(layer) = 1.403 - 0.002 * (Temperature - 30) ^ 2;
    end
    
end

% Rain factor for mechanical transfer of standing dead and mulch to the ground
% (JTR - 18 Nov 1999):
rainTest = (Rain > 0.5); %cm
rainFac = populate_array(zerosGrid, 5, 1, rainTest, ~rainTest);

end %Env_Modifiers