%This script is an irrigation model to be applied first to the Jordan River
%part of the EBCW model. It serves as a first attempt at an irrigation
%model, which works by modifying precipitation rather than scheduling or
%soil-dependent irrigation. The rest of the logic necessary for the
%irrigation model I wrote down in an Evernote scanned PDF.
%
%This has a basic problem right now, in that for cells without complete
%irrigation coverage, it adds a lower rate of irrigation. I need to create
%subdomains of irrigated and non-irrigated ag. But that is a task for
%another day!
%
%One thing that I did is to put all of the logic for creating the WEL file
%into the specify_modflow, initialize_modflow, and wel_write files. Also, I
%created the specification for the well pumping schedules in the LAND file.

%--------------------------------------------------------------------------
%Specify inputs
%--------------------------------------------------------------------------
%Irrigated areas grid - same resolution as the model
gridIrrigPath = '\\Hydrostat\f\Users\sherry\Data\GIS\Derived\Boardman-Charlevoix_Watershed\Model_Layers\ILHM\irrigarea';

%Precipitation grid
gridPrecipClip = 'D:\Users\Anthony\Data\GIS_Data\Boardman-Charlevoix_Watershed\Precipitation\bdry_nexrad';

%Irrigation wells dbf - must have field x, y, field ID (to match grid),
dbfWells = '\\Hydrostat\f\Users\sherry\Data\GIS\Derived\Boardman-Charlevoix_Watershed\Irrigation\center_pivots_2005_georef_points_wells.dbf';
fieldWellsX = 'GEOREFX';
fieldWellsY = 'GEOREFY';
fieldWellsFieldID = 'Field_ID';
fieldWellsCap = 'Cap_gpm';
fieldWellsScreenTop = 'Screen_Top'; %elevation, in model datum and units
fieldWellsScreenBot = 'Screen_Bot'; 
capConvFac = unit_conversions(unit_conversions(1,'gpm','cms'),'ps','pday');

%Specify fixed parameters
paramAvg = unit_conversions(12,'in','m'); %Annual Irrigation Average
paramStart = 150; %start of irrigation time period, day of the year
paramEnd = 225; %end of period
paramRate = unit_conversions(unit_conversions(2,'in','m'),'pday','phr'); %apply at 2 in/day, want hourly rate
paramPivotDays = 7; %days to make a complete rotation, even though I apply all of the water to the cell in a single day, it takes 7 days to pump out

%Specify CLIM file input and output
climInput = '\\Hydrostat\f\Users\sherry\Modeling\ILHM\Input\CLIM_EBCW-2000-2009_1_20101202.h5';
climOutput = '\\Hydrostat\f\Users\sherry\Modeling\ILHM\Input\CLIM_EBCW-2000-2009_2-irrig_20111105.h5';

%Specify LAND file to add well schedule to
landInput = '\\Hydrostat\f\Users\sherry\Modeling\ILHM\Input\LAND_EBCW_2-irrig_20111105.h5';

%Specify MODFLOW parameters
groundwaterGrid = '\\hydrostat\F\Users\sherry\Data\GIS\Derived\Boardman-Charlevoix_Watershed\Model_Layers\ILHM_MODFLOW\ibound_lay1';
groundwaterSPLen = 7; %nominal, last will be shorter
outWelFilename = 'testIrrigWell.wel';
outDir = 'D:\Users\Anthony\Modeling\Working_Directory\EBCW';

%--------------------------------------------------------------------------
%Read inputs
%--------------------------------------------------------------------------
%Read rainfall
rainIn = h5datareadilhm(climInput,'/rain');

%Read irrigation grid
[gridIrrigArea,headerIrrig] = import_grid(gridIrrigPath);
[gridPrecip,headerPrecip] = import_grid(gridPrecipClip);
gridIrrigAreaPrecip = cell_statistics(headerPrecip,headerIrrig,gridIrrigArea,'sum');

%Read wells
[wellsTable,wellsFields] = dbfread(dbfWells);

%Read the groundwater model grid
[groundwaterHeader] = import_grid(groundwaterGrid,'header');

%--------------------------------------------------------------------------
%Analyze precipitation and temporally distribute 
%--------------------------------------------------------------------------
%Average across model cells
avgRain = nanmean(rainIn.data,2);

%Create full-period hourly dataset
hourlyRain = zeros(size(rainIn.index,1),1);
hourlyRain(rainIn.events) = avgRain;

%Calculate daily averages
[modelDays,modelDailyRain] = time_series_aggregate(rainIn.index(:,1),hourlyRain,'days','sum');

%Calculate two-week forward and backward-looking averages
smoothedRain = smoothn(modelDailyRain,15);

%Analyze groups of years to determine driest periods
[yearStart,~,~,~,~,~] = datevec(rainIn.index(1,1));
[yearEnd,~,~,~,~,~] = datevec(rainIn.index(end,1));

%Calculate the total number of irrigation days in the model
numDays = (yearEnd - yearStart + 1) * paramAvg / paramRate / (24*60); %rate is hourly, need daily

%Now, go through all days and distribute them to the driest periods
testIrrig = (date2doy(modelDays) >= paramStart) & (date2doy(modelDays) <= paramEnd);
irrigDays = modelDays(testIrrig);
irrigEvents = zeros(numDays,1);
for m = 1:numDays
    %Pick the lowest value
    driestIrrigInd = find(smoothedRain(testIrrig) == min(smoothedRain(testIrrig)),1,'first');
    
    %Identify the day, get the index in the overall rain array
    irrigEvents(m) = irrigDays(driestIrrigInd);
    driestInd = find(irrigEvents(m) == modelDays);
    
    %Add rain to that day, then resmooth
    modelDailyRain(driestInd) = modelDailyRain(driestInd) + paramRate * (24*60); %daily rate
    smoothedRain = smoothn(modelDailyRain,15);
end

%--------------------------------------------------------------------------
%Map back to hourly grid values, write modified rainfall to the CLIM file
%--------------------------------------------------------------------------
%Now, map these back to hourly values
daysRound = floor(rainIn.index(:,1));
irrigRain = zeros(size(rainIn.index,1),size(rainIn.data,2));
irrigRain(rainIn.events,:) = rainIn.data;
for m = 1:numDays
    thisInd = (daysRound == irrigEvents(m));
    irrigRain(thisInd,:) = irrigRain(thisInd,:) + repmat(paramRate.*(gridIrrigAreaPrecip(:)/headerIrrig.cellsize^2)',[sum(thisInd),1]);
end

%Trim out null rows and renumber index
noRain = all(irrigRain==0,2);
irrigRain(noRain,:) = [];
index = rainIn.index;
index(noRain,2) = -1;
index(~noRain,2) = (1:size(irrigRain,1));

%Write output
import_write_dataset(climOutput,'/rain',irrigRain,rainIn.attribs,index,rainIn.stationsXY,rainIn.lookup,true)

%--------------------------------------------------------------------------
%Create the irrigation schedule and write to the LAND file
%--------------------------------------------------------------------------
%Create the well list
wellList = [(1:size(wellsTable,1))',cell2mat(wellsTable(:,strcmp(fieldWellsFieldID,wellsFields))),...
    cell2mat(wellsTable(:,strcmp(fieldWellsX,wellsFields))),cell2mat(wellsTable(:,strcmp(fieldWellsY,wellsFields))),...
    cell2mat(wellsTable(:,strcmp(fieldWellsScreenTop,wellsFields))),cell2mat(wellsTable(:,strcmp(fieldWellsScreenBot,wellsFields)))];

%Create the pumping schedule, all wells identical here
%Check to make sure none of the irrigation events are within the pivot time
%This should not happen provided that the smoothing time is longer than the
%pivot time!
irrigEvents = sort(irrigEvents);
assert(all(diff(irrigEvents)>paramPivotDays),'Increase smoothing time to not overlap irrigation events');

%Create the well schedule, assume pumping at specified capacity
wellSchedule = zeros(length(irrigEvents)*paramPivotDays,size(wellsTable,1));
pumpDays = zeros(length(irrigEvents)*paramPivotDays,1);
for m = 1:numDays
    wellSchedule((m-1)*paramPivotDays+1:m*paramPivotDays,:) = repmat(cell2mat(wellsTable(:,strcmp(fieldWellsCap,wellsFields)))'*capConvFac,paramPivotDays,1);
    pumpDays((m-1)*paramPivotDays+1:m*paramPivotDays) = (irrigEvents(m):irrigEvents(m)+paramPivotDays-1)';
end

%Create the well index
wellIndex = [(floor(rainIn.index(1)):floor(rainIn.index(end,1)))',(floor(rainIn.index(1)):floor(rainIn.index(end,1)))'+1-1/86400];
indPumpDays = ismember_dates(wellIndex(:,1),pumpDays,3);
wellIndex = [wellIndex,zeros(size(wellIndex,1),1)-1];
wellIndex(indPumpDays,3) = (1:sum(indPumpDays));

%Write to the LAND file (this is just for a template and for future models, I don't actually use
%it as such right now)
h5groupcreate(landInput,'/schedules');
h5groupcreate(landInput,'/schedules/well');
h5datawrite(landInput,'/schedules/well/index',wellIndex);
h5datawrite(landInput,'/schedules/well/list',wellList);
h5datawrite(landInput,'/schedules/well/schedule',wellSchedule,struct('units','m^3/day','class','double'));

%--------------------------------------------------------------------------
%Write the WEL file -- this logic is all in initialize_modflow too
%--------------------------------------------------------------------------
%Match model x and y and screen to layer, row, and column
%First, match x and y to row, and col
%Set up the model grid arrays
modelX = groundwaterHeader.left + (0:groundwaterHeader.cols)'*groundwaterHeader.cellsizeX;
modelY = groundwaterHeader.top - (0:groundwaterHeader.rows)'*groundwaterHeader.cellsizeY;

%Determine the i,j for all of the input points
wellIJK = zeros(size(wellList,1),3); %(row,col,lay)
for m = 1:size(wellList,1);
    wellIJK(m,2) = find((wellList(m,3) >= modelX(1:end-1)) & (wellList(m,3) < modelX(2:end)),1,'first');
    wellIJK(m,1) = find((wellList(m,4) <= modelY(1:end-1)) & (wellList(m,4) > modelY(2:end)),1,'first');
end

%Determine stress periods
numDays = wellIndex(end,1) - wellIndex(1) + 1;
nper = ceil(numDays/groundwaterSPLen);

test=rem(nper/groundwaterSPLen,numDays);
if test==0 %all stress periods are identical length
    perlen = repmat(groundwaterSPLen,1,nper);
else %the last stress period is shorter
    perlen = [repmat(groundwaterSPLen,1,nper-1),...
        (numDays-(nper-1)*groundwaterSPLen)];
end

nstp = perlen; %assumes daily timesteps, be careful!
stressTable = zeros(sum(nstp), 4); 
index = 0;
for h=1:nper
    for i=1:nstp(h)
        index = index + 1;
        stressTable(index,1) = h;
        stressTable(index,2) = i;
        if index == 1
            stressTable(index,3) = wellIndex(1);
        else
            stressTable(index,3) = stressTable(index-1,4);
        end
        stressTable(index,4) = stressTable(index,3) + perlen(h) / nstp(h);
    end
end

structure.nper = nper;
structure.perlen = perlen;
wellScheduleSP = zeros(structure.nper,size(wellSchedule,2));
for m = 1:structure.nper
    stressStart = stressTable(sum(structure.perlen(1:m))-structure.perlen(m)+1,3);
    stressEnd = stressTable(sum(structure.perlen(1:m)),4);
    thisInd = (wellIndex(:,1) >= stressStart) & (wellIndex(:,2) <= stressEnd);
    sumInd = unique(wellIndex(thisInd,3));
    sumInd(sumInd==-1) = [];
    if ~isempty(sumInd)
        wellScheduleSP(m,:) = sum(wellSchedule(sumInd,:),1)/structure.perlen(m); %average pump rate
    end
end

%Put all wells in layer 1 (only 1 layer model!)
wellIJK(:,3) = 1;

%Assemble inputs for wel file writing
welInputs.IJK = wellIJK;
welInputs.Q = -wellScheduleSP;
%Count active number of wells
welInputs.ITMP = sum(wellScheduleSP>0,2);
welInputs.MXACTW = max(welInputs.ITMP);
welInputs.IWELCB = 0;
package.inputs = welInputs;
package.name = outWelFilename;
%Now, write to the wel file
wel_write(outDir,package,structure,[],[]);