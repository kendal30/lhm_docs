#This script merges SSURGO polygon datasets into a single shapefile and
#composites tabular data from multiple SSURGO counties into a single dataset
#Currently, it's formulated for MI
#Notes: generates errors with the sdv* tables, but these aren't critical, I don't believe

import os, arcgisscripting


#-------------------------------------------------------------------------------
#Specify inputs and outputs
#-------------------------------------------------------------------------------
#Specify the base directory containing the SSURGO directories (structure is base -> county -> spatial)
stateName = 'New_York'
stateAbbr = 'ny'
startDir = '001'

baseDir = 'X:\\Downloaded\\SSURGO_Soils\\' + stateName
outDir = 'X:\\Downloaded\\SSURGO_Soils'

#Specify a list of county numbers to import
allCounties = True #ignore the next line if this is set to true
countyDirNames = ['']

#Specify a directory containing the list of tables to composite
tableListDir = baseDir + '\\soil_' + stateAbbr + startDir + '\\tabular'
excludeTables = ['version.txt']
allComposite = False #composites all tables, this should only be used for smaller areas, ignores next line if true
compositeTables = ['legend.txt','mapunit.txt','comp.txt','chtexgrp.txt','chtextur.txt','chorizon.txt']; #this will allow files to be created, but all will be empty except those in this list
tableExtension = '.txt'

#Specify the dataset to match the projection to
projMatch = baseDir + '\\soil_' + stateAbbr + startDir + '\\spatial\\soilmu_a_' + stateAbbr + startDir + '.shp'

#Specify the output directory and filename
outFileGDB = stateName.lower() + '_ssurgo.gdb'
outFeatureName = 'ssurgo_polys'

#Shouldn't have to change these
outCompDir = stateName + 'ssurgo_tables'
#Specify the base shapefile name
baseName = 'soilmu_a'

#-------------------------------------------------------------------------------
#Create the Geoprocessing Object
#-------------------------------------------------------------------------------
#Create the geoprocessing object
gp = arcgisscripting.create(9.3)

#Set the output coordinate system
gp.OutputCoordinateSystem = projMatch

#-------------------------------------------------------------------------------
#Prepare directory and table list
#-------------------------------------------------------------------------------
#Get a list of the directories to search in
dirs = [];
[dirs.append(os.path.join(baseDir,dir)) for dir in os.listdir(baseDir) if os.path.isdir(os.path.join(baseDir,dir))]
if not allCounties:
    [dirs.remove(dir) for dir in dirs if not os.path.split(dir)[-1] in countyDirNames]

#Fetch the list of tables
tables = [];
[tables.append(files) for files in os.listdir(tableListDir) if \
    files.endswith(tableExtension) and not (files in excludeTables)]

#-------------------------------------------------------------------------------
#Composite the Maps
#-------------------------------------------------------------------------------
#Create the value table holding the list of shapefiles to merge
valueTable = gp.createobject("ValueTable")

#Loop through the directories, adding entries to the value table
for dir in dirs:
    #Set the geoprocessing workspace to the current spatial directory
    gp.workspace = os.path.join(dir,'spatial')

    #List the featureclasses in the workspace that match the baseName
    fcs = gp.listfeatureclasses(baseName + '*', "Polygon")

    #Add the featureclass to the value table
    for fc in fcs:
        valueTable.AddRow(os.path.join(gp.workspace,fc))

#Create the output file geodatabase
gp.CreateFileGDB_management(outDir,outFileGDB)

#Run the merge tool
gp.Merge(valueTable, os.path.join(outDir,outFileGDB) + '/' + outFeatureName)

#Clean up
del valueTable

#-------------------------------------------------------------------------------
#Composite the Tables
#-------------------------------------------------------------------------------
##outDir2 = os.path.join(outDir,outCompDir)
##
##if not os.path.exists(outDir2):
##    os.mkdir(outDir2)
##
###Loop through each table
##for table in tables:
##    #First, create the output table
##    outTable = open(os.path.join(outDir2,table),'w')
##
##    if (table in compositeTables) or allComposite:
##        #Now, loop through all of the directories
##        for dir in dirs:
##            try:
##                #Read in the relevant table
##                inDir = os.path.join(dir,'tabular')
##                inTable = open(os.path.join(inDir,table),'U')
##
##                #Write the content out to the outTable
##                outTable.write(inTable.read())
##
##                #Close the inTable
##                inTable.close()
##            except:
##                pass
##
##    #Close the outTable
##    outTable.close()
##
###Finally, copy the version.txt file from the tableListDir to the outDir
##os.system("copy %s %s" % (os.path.join(tableListDir,'version.txt'), os.path.join(outDir2,'version.txt')))

#Finish clean up
del gp
