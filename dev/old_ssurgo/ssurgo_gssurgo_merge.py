# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 14:42:20 2017

@author: Kendal30
"""

import os, zipfile, glob, shutil, arcpy
#-------------------------------------------------------------------------------
#Specify inputs and outputs
#-------------------------------------------------------------------------------
#Inputs
inBaseDir = 'S:\\Data\\GIS_Data\\Downloaded\\SSURGO_Soils\\Original_Updated_8_28_2017'
inBaseZipName = 'soils_GSSURGO'
inSubZipSoilName = 'gSSURGO_g'
inSubZipValName = 'valu'

#Outputs
outDir = 'F:\\Users\\Anthony\\Processing\\Nationwide_SSURGO\\gSSURGO'
outDBname = 'gSSURGO_nationwide.gdb'
outRasterBaseName = 'MapunitRaster'
outRasterSuffix = '_10m'
outCoarseSuffix = '_30m'

#Specify output coarse cellsize
outCoarseSize = 30

#Temporary
tempOutDir = 'temp'

#Get list of states to process
stateList = glob.glob(inBaseDir+'/*/')

#Set up the environment
arcpy.env.overwriteOutput = 1
arcpy.env.compression = 'LZ77'
arcpy.env.pyramid = 'NONE'
#%% ----------------------------------------------------------------------------
#Do the processing
#-------------------------------------------------------------------------------
restartIndex = 0 #set to 0 if starting from scratch, make sure to remove the temp directory if restarting
for state in stateList:
    thisIndex = stateList.index(state)
    if thisIndex >= restartIndex:
        ##Unzip master zip and sub-zips to a temporary location
        thisOutDir = os.path.join(outDir,tempOutDir)
        os.mkdir(thisOutDir)
        
        #Get the name of the first zip file and extract
        thisZipFile = glob.glob(state+inBaseZipName+'*')
        if len(thisZipFile)>0:
            thisZipObj = zipfile.ZipFile(thisZipFile[0])
            thisZipObj.extractall(thisOutDir)
            thisZipObj.close()
        
        #Process the soils GeoDB
        thisSubOutDir = os.path.join(thisOutDir,'soils')
        thisSubZipSoilFile = glob.glob(thisSubOutDir+'/'+inSubZipSoilName+'*')
        if len(thisSubZipSoilFile)>0:
            thisSubZipSoilObj = zipfile.ZipFile(thisSubZipSoilFile[0])
            thisSubZipSoilObj.extractall(thisSubOutDir)
            thisSubZipSoilObj.close()
            
            thisDBSoil = glob.glob(thisSubOutDir+'/gSSURGO*.gdb')
            
            if thisIndex == 0 and len(stateList)>1:
                #Copy 1st to a permanent location
                outDBSoilTemp = os.path.join(outDir,os.path.split(thisDBSoil[0])[1])
                shutil.move(thisDBSoil[0],outDBSoilTemp)
                
                #Rename the state DB and mapunit raster
                outDBSoil = os.path.join(outDir,outDBname)
                arcpy.Rename_management(outDBSoilTemp,outDBSoil)
                arcpy.env.workspace = outDBSoil
                listRasters = arcpy.ListRasters()
                arcpy.Rename_management(listRasters[0],outRasterBaseName+outRasterSuffix)
            else:
                ##For others, get a list of all tables, rasters, and feature classes
                listFeatures = arcpy.ListFeatureClasses()
                listTables = arcpy.ListTables()
                thisRaster = outRasterBaseName + '_' + \
                    thisDBSoil[0][thisDBSoil[0].find('gSSURGO_')+8:thisDBSoil[0].find('.gdb')]+\
                    outRasterSuffix
                
                arcpy.Append_management(thisDBSoil[0]+'/'+thisRaster,outDBSoil+'/'+outRasterBaseName+outRasterSuffix,'NO_TEST')
                for feature in listFeatures:
                    arcpy.Append_management(thisDBSoil[0]+'/'+feature,outDBSoil+'/'+feature,'NO_TEST')
                for table in listTables:
                    arcpy.Append_management(thisDBSoil[0]+'/'+table,outDBSoil+'/'+table,'NO_TEST')
         
        #Process the value added tables GeoDB           
        thisSubZipValFile = glob.glob(thisSubOutDir+'/'+inSubZipValName+'*')
        if len(thisSubZipValFile)>0:
            thisSubZipValObj = zipfile.ZipFile(thisSubZipValFile[0])
            thisSubZipValObj.extractall(thisSubOutDir)
            thisSubZipValObj.close()
            
            thisDBVal = glob.glob(thisSubOutDir+'/valu*.gdb')
            if thisIndex == 0:
                #Copy 1st to a permanent location
                outDBVal = os.path.join(outDir,os.path.split(thisDBVal[0])[1])
                shutil.move(thisDBVal[0],outDBVal)
            else:
                arcpy.Append_management(thisDBVal[0]+'/valu1',outDBVal+'/valu1')
        
        #Delete the temporary output dir
        shutil.rmtree(thisOutDir)
    else:
        pass

#Build the pyramids
arcpy.BuildPyramids_management(outDBSoil+'/'+outRasterBaseName+outRasterSuffix)

#Create the 30m version of mapunit raster
arcpy.env.workspace = outDBSoil
arcpy.Resample_management(outRasterBaseName+outRasterSuffix,outRasterBaseName+outCoarseSuffix,\
                          outCoarseSize,'NEAREST')
arcpy.BuildPyramids_management(outDBSoil+'/'+outRasterBaseName+outCoarseSuffix)

#Compact the geodatabase
arcpy.Compact_management(outDBSoil)