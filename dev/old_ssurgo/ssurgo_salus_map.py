#This script creates SALUS SDB files using SSURGO data, along with secondary sources
#for key hydraulic properties

#Specify input and output paths
inFeature = '' #for extraction and masking
outDir = ''



#Import modules
import pyodbc, arcpy, os, numpy
#-------------------------------------------------------------------------------
#Advanced/Standardized Inputs
#-------------------------------------------------------------------------------
#If needing to restart: States already completed, or ones to omit, use two/three letter names of the states
statesDone = ['IL']
statesOmit = ['']



#Specify maskLayer
maskLayer = inFeature


#Specify the soils directory, within this, there should be a DB named Spatial_Data.gdb,
#and a directory named Tabular containing the tabular DBs named with their two-letter
#USPS codes
soilsDir = 'S:\\Data\\GIS_Data\\Downloaded\\SSURGO_Soils'
spatialDB = os.path.join(soilsDir,'Spatial_Data.gdb')
spatialStates = spatialDB + '/' + 'TIGER_States'

#Specify the output cellsize,must match the units of the mask layer, not the model
#resolution, just used to accurately map polygons to raster
outCellsize = 25


#Assign temporary names created when exporting
tempStates = 'tempstates'

#-------------------------------------------------------------------------------
#Prepare the environment settings
#-------------------------------------------------------------------------------
#Check out a spatial analyst extension license
arcpy.CheckOutExtension("Spatial")

#Set the a few environment settings
arcpy.env.outputCoordinateSystem = maskLayer
arcpy.env.cellSize = outCellsize
arcpy.env.overwriteOutput = 1
arcpy.env.compression = 'LZ77'
arcpy.env.pyramid = 'NONE'
arcpy.env.workspace = outDir
arcpy.env.scratchWorkspace = outDir


#-------------------------------------------------------------------------------
#Determine which states will be required
#-------------------------------------------------------------------------------
#Intersect the overlay of states with the project mask
arcpy.Intersect_analysis([spatialStates,maskLayer],tempStates)

#Bring in these features and make a list of unique states
rows = arcpy.SearchCursor(arcpy.env.workspace+'/'+tempStates)
states = []
for row in rows:
    if not row.SSURGO_NAME in states:
        states.append(row.SSURGO_NAME)

#Remove already completed states, and omitted states
states = [state for state in states if state not in statesDone]
states = [state for state in states if state not in statesOmit]

#Clean up
arcpy.Delete_management(tempStates)