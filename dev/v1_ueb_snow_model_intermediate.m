function state = v1_ueb_snow_model(state,structure,params)

    % Get necessary inputs
    uebParams = params.ueb;
    [k,z,zo,hff,depthHeatExch] = deal_struct(uebParams,...
        'k','z','zo','hff','de');
    [densityMineral,densityWater,densityAir,cpMineral,cpWater,porosity,soilSatCap] = deal_struct(params,...
        'density_mineral','density_water','density_air','heat_cap_mineral',...
        'heat_cap_water','soil_porosity','soil_sat_cap');
    [gapFrac,snowFall,rainFall,...
        airTemp,windspeed,airVapPress,qRadShort,qRadLong,...
        pressure,qCondGround,snowWaterEquiv,snowHeatStore,surfAge,soilWater,...
        newSnowDens,snowThick,snowIceFrac] = deal_struct(state,...
        'canopy_gap_fraction','total_throughfall_snow','total_throughfall_rain',...
        'temp_air','windspeed','vap_pressure_air','rad_short_upland_net','rad_long_in',...
        'pressure','q_cond_ground','water_snow_upland','snow_heat_storage_upland','snow_surf_age_upland',...
        'water_soil','snow_new_density','snow_thickness_upland','snow_frozen_fraction_upland');
    [zerosGrid,indUpland,soilThick,timestepLength] = deal_struct(structure,...
        'zeros_grid','index_upland','thickness_soil','timestep_length');
    
    
    % Determine if the snow model needs to run or not
    indSnow = ((snowWaterEquiv > 0) | (snowFall > 0)) & indUpland;
    
    if any(isnan(snowThick))
            disp('debug me');
    end
        
    if any(indSnow(:))
        % Initialize output variables
        [cump,cume,cummr,heatCapGround,densGround] = deal(zerosGrid);
        
        %  Forest cover fraction(0-1)
        fc = 1 - gapFrac;
        qCondGround(indSnow) = unit_conversions(unit_conversions(qCondGround(indSnow),'J','kJ'),'ps','phr');
        qRadShort(indSnow) = unit_conversions(unit_conversions(qRadShort(indSnow),'J','kJ'),'ps','phr');
        
        % Longwave is currently not affected by forest cover, affect it here
        qRadLong(indSnow) = unit_conversions(unit_conversions(qRadLong(indSnow),'J','kJ'),'ps','phr') .* gapFrac(indSnow);
        
        dt = timestepLength/3600;  % timestep is in seconds, this model wants hours
        
        % Convert precipitation totals into hourly rate
        snowRate = snowFall/dt;
        rainRate = rainFall/dt;
        
        %   initialize variables for mass balance
        prevSwe = snowWaterEquiv;
    
        % Get the deepest layer of interaction with the soil
        maxLay = sum(depthHeatExch>cumsum(soilThick,2),2) + 1;
        maxLay = mode(maxLay); % this is a simplification, but probably not a terrible one
        
        % Update ground density and heat capacity, need kJ
        weightMineral = 1 - porosity(indSnow,1:maxLay);
        weightWater = soilWater(indSnow,1:maxLay) ./ soilSatCap(indSnow,1:maxLay) .* porosity(indSnow,1:maxLay);
        weightAir = 1 - weightMineral - weightWater;
        
        % Calculate layer-specific terms
        heatCapLayers = weighted_mean('arithmetic',cpMineral,cpWater,...
            weightMineral.*densityMineral,weightWater.*densityWater);
        densLayers = weighted_mean('arithmetic',densityMineral,densityWater,densityAir,...
            weightMineral,weightWater,weightAir);
        
        % Average across layers
        totalSoilThick = repmat(sum(soilThick(indSnow,1:maxLay),2),[1,maxLay]);
        heatCapGround(indSnow) = sum(heatCapLayers .* soilThick(indSnow,1:maxLay) ./ totalSoilThick,2);
        densGround(indSnow) = sum(densLayers .* soilThick(indSnow,1:maxLay) ./ totalSoilThick,2);
        
        % Convert to kJ
        heatCapGround = unit_conversions(heatCapGround,'J','kJ'); % kJ/kg/C
        
        %   Calculate constants
        %    These need only be calculated once.
        %    The model is more efficient for large nt since it saves on these
        %    calc's
        cd = k.*k.*hff./(log(z./zo).^2).*(1-0.8.*fc(indSnow));
        % factor in turbulent fluxes
        %    The FC correction is applied here to hit sensible and latent heat fluxes
        %    and evaporation rate in one place and maintain consistency in the surface
        %     temperature and stability calculations. It is consistent with wind speed
        %     reduction by a factor of 1-0.8 FC which is the most reasonable physical
        %     justification for this approach.
        %     I recognize that this is not a very good way to parameterize vegetation.
        %     This will be an area of future improvements(I hope).
        %    FC is also used below to adjust radiation inputs.
        %rrhoi = densIce./densWater;
        %rrho = densSnowpack./densWater;
        %rid = 1./rrho-1./rrhoi;
        
        %   Calculate neutral mass transfer coefficient
        rkn = cd.*windspeed(indSnow);
        
        %     FC corrections are also in the following subroutines.
        %      QFM where outgoing longwave radiation is calculated.
        %      SNOTMP where outgoing longwave radiation is used in the surface
        %      temperature equilibrium approx.
        
        %   Call predictor corrector subroutine to do all the work
        [snowHeatStore(indSnow),snowWaterEquiv(indSnow),snowIceFrac(indSnow),snowThick(indSnow),...
            evapRate,drainRate,qSens,qEvap,qMelt,qStor,qnet,tsurf] = predicorr(...
            dt,snowHeatStore(indSnow),snowWaterEquiv(indSnow),snowIceFrac(indSnow),...
            airTemp(indSnow),rainRate(indSnow),snowRate(indSnow),windspeed(indSnow),...
            airVapPress(indSnow),qRadShort(indSnow),qRadLong(indSnow),rkn,...
            fc(indSnow),pressure(indSnow),qCondGround(indSnow),heatCapGround(indSnow),...
            densGround(indSnow),snowThick(indSnow),newSnowDens(indSnow),uebParams); % #ok<ASGLU>
        
        %    accumulate for mass balance
        cump(indSnow) = (snowFall(indSnow)+rainFall(indSnow));
        cume(indSnow) = evapRate*dt;
        cummr(indSnow) = drainRate*dt;
        % tave = tavg(snowHeatStore,snowWaterEquiv,densWater,heatCapSnow,to,densGround,depthHeatExch,heatCapGround,heatFusion); % #ok<NASGU>
        
        % Calculate the new snow surface age
        surfAge(indSnow) = snow_surface_age(surfAge(indSnow),cump(indSnow),tsurf,timestepLength);
        
        % If most of the pack has melted reset
        test = (snowWaterEquiv < 0.00005);
        if any(test) % reset if snowWaterEquiv less than 0.05 mm
            cummr(test) = cummr(test) + snowWaterEquiv(test);
            snowWaterEquiv(test) = 0;
            snowHeatStore(test) = 0;
            snowThick(test) = 0;
            snowIceFrac(test) = 0;
            surfAge(test) = 0;
        end
        
        % Test error
        if assert_LHM()
            %         plot(state.timestep,mean(snowWaterEquiv(indSnow)),'o');
            %         plot(state.timestep,mean(cump(indSnow)),'rs');
            %         drawnow
            errmbal = prevSwe+cump-cummr-cume-snowWaterEquiv; % #ok<NASGU>
            %disp(max(abs(errmbal)))
            if any(isnan(snowWaterEquiv) | isinf(snowWaterEquiv))
                disp('debug this')
            end
            if any(isnan(snowThick) | isinf(snowThick))
                disp('debug this')
            end
            % if any(abs(snowHeatStore)>1e8)
            %     disp('Issues with snowHeatStore array')
            % end
            assert_LHM(~any(isnan(snowWaterEquiv)),'There are NaNs in the snowpack water');
        end
        
        % Update the data controller
        state = update_struct(state,'snow_heat_storage_upland',snowHeatStore,'water_snow_upland',...
            snowWaterEquiv,'snow_melt_upland',cummr,'evap_snow_upland',cume,'snow_surf_age_upland',surfAge,...
            'snow_frozen_fraction_upland',snowIceFrac,'snow_thickness_upland',snowThick);
    else
        state = update_struct(state,'snow_melt_upland',zerosGrid,'evap_snow_upland',zerosGrid);
    end
    
    end % subroutine snow
    
    % ********************** PREDICORR() **************************
    %     Predictor-corrector scheme to update the state variables,
    %     snowHeatStorage and snowWaterEquiv for each time step
    function [snowHeatStore,snowWaterEquiv,snowIceFrac,snowThick,evapRate,drainRate,...
        qSens,qEvap,qMelt,qStor,qNet,tSurf] = predicorr(dt,snowHeatStore,snowWaterEquiv,snowIceFrac,...
        airTemp,rainRate,snowRate,windspeed,airVapPress,qRadShort,qli,rkn,fc,pressure,...
        qCondGround,heatCapGround,densGround,snowThick,newSnowDens,uebParams)
    
    % wtol = 0.0025;
    % utol = 2000;
    
    %  Parameters
    [densWater] = deal_struct(uebParams,'rhow');
    
    prevSWE = snowWaterEquiv;
    
    % Update frozen fraction, and swe
    snowIce0 = snowWaterEquiv .* snowIceFrac;
    snowLiquid0 = snowWaterEquiv - snowIce0;
    
    snowIce = snowIce0 + snowRate * dt;
    snowLiquid = snowLiquid0 + rainRate * dt;
    
    snowWaterEquiv = snowIce + snowLiquid;
    snowIceFrac = snowIce ./ snowWaterEquiv;
    
    %  Update snow thickness
    test = (snowRate > 0);
    snowThick(test) = snowThick(test) + (snowRate(test)*dt).*(densWater./newSnowDens(test));
    
    % Calculate initial estimate of mass fluxes
    [snowWaterEquiv0,snowHeatStore0,snowStore0,drainRate0,evapRate0,snowIceFrac0,...
        snowThick0,qMelt0,qEvap0,qSens0,qNet0,tSurf0] = qfm(...
        snowHeatStore,snowWaterEquiv,snowIceFrac,airTemp,rainRate,snowRate,windspeed,airVapPress,...
        qRadShort,qli,rkn,fc,pressure,qCondGround,dt,heatCapGround,densGround,...
        snowThick,uebParams);
    
    
    % Update
    [snowWaterEquiv1,snowHeatStore1,snowStore1,drainRate1,evapRate1,snowIceFrac1,...
        snowThick1,qMelt1,qEvap1,qSens1,qNet1,tSurf1] = qfm(...
        snowHeatStore0,snowWaterEquiv0,snowIceFrac0,airTemp,rainRate,snowRate,windspeed,airVapPress,...
        qRadShort,qli,rkn,fc,pressure,qCondGround,dt,heatCapGround,densGround,...
        snowThick0,uebParams);
    
    % Average
    snowWaterEquiv = (snowWaterEquiv + snowWaterEquiv1)/2;
    snowHeatStore = (snowHeatStore0 + snowHeatStore1)/2;
    snowIceFrac = (snowIceFrac + snowIceFrac1)/2;
    snowThick = (snowThick + snowThick1)/2;
    evapRate = (evapRate0 + evapRate1)/2;
    %snowStore = (snowStore0 + snowStore1)/2;
    qSens = (qSens0 + qSens1)/2;
    qEvap = (qEvap0 + qEvap1)/2;
    qMelt = (qMelt0 + qMelt1)/2;
    qStor = (qMelt0 + qMelt1)/2;
    qNet = (qNet0 + qNet1)/2;
    tSurf = (tSurf0 + tSurf1)/2;
    
    % Correct rates
    drainRate = rainRate + snowRate + (prevSWE - snowWaterEquiv)/dt - evapRate;
    drainRate(drainRate<0) = 0;
    evapRate = rainRate + snowRate + (prevSWE - snowWaterEquiv)/dt - drainRate;
    % 
    % % %      PREDICTOR
    % % %w1 = snowWaterEquiv + dt.*storRate;
    % % % test = (w1<0);
    % % % if any(test)
    % % %     w1(test) = 0;
    % % %     [storRate(test),drainRate(test),evapRate(test),qMelt(test),qEvap(test),qStor(test)] = ...
    % % %         prehelp(w1(test),snowWaterEquiv(test),dt,storRate(test),1,...
    % % %         snowRate(test),rainRate(test),meltRate(test),evapRate(test),densWater,heatFusion,qStor(test),...
    % % %         qMelt(test),qEvap(test),hneu);
    % % % end
    % % ub1 = snowHeatStore + dt.*qStor;
    % % 
    % % %snowWaterEquiv = w1;
    % % snowHeatStore = ub1;
    % 
    % % % Save first estimates
    % % snowIceFrac1 = snowIceFrac;
    % % snowThick1 = snowThick;
    % % q1 = qStor;
    % % ablation1 = storRate;
    % % %   save values so that they can be averaged for output
    % % qh1 = qSens;
    % % qe1 = qEvap;
    % % e1 = evapRate;
    % % mr1 = drainRate;
    % % qm1 = qMelt;
    % % tsurf1 = tsurf;
    % % qnet1 = qnet;
    % % [storRate,qStor,qMelt,meltRate,drainRate,snowIceFrac,snowThick,qEvap,evapRate,tsurf,qSens,qnet] = qfm(...
    % %     ub1,w1,snowIceFrac,airTemp,rainRate,snowRate,windspeed,airVapPress,...
    % %     qRadShort,qli,rkn,fc,pressure,qCondGround,dt,heatCapGround,densGround,...
    % %     snowThick,uebParams);
    % % 
    % % %      CORRECTOR
    % % w2 = snowWaterEquiv + dt./2.0.*(ablation1 + storRate);
    % % if any(w2<0)
    % %     disp('debug');
    % % end
    % % test = (w2<0);
    % % if any(test)
    % %     w2(test) = 0;
    % %     snowThick(test) = 0;
    % %     snowIceFrac(test) = 0;
    % %     [storRate(test),drainRate(test),evapRate(test),qMelt(test),qEvap(test),qStor(test)] = ...
    % %         prehelp(w2(test),snowWaterEquiv(test),dt,storRate(test),ablation1(test),...
    % %         snowRate(test),rainRate(test),meltRate(test),evapRate(test),densWater,heatFusion,qStor(test),...
    % %         qMelt(test),qEvap(test),hneu);
    % % end
    % % ub2 = snowHeatStore + dt./2.*(q1 + qStor);
    % % 
    % % snowWaterEquiv = w2;
    % % snowHeatStore = ub2;
    % % %  average values from two time steps for output.  This is done for drainRate
    % % %  and evapRate to ensure mass balance and the others for better physical
    % % %  comparisons
    % % snowThick = (snowThick + snowThick1)/2;
    % % snowIceFrac = (snowIceFrac + snowIceFrac1)/2;
    % % qSens = (qSens+qh1)/2;
    % % qEvap = (qEvap+qe1)/2;
    % % evapRate = (evapRate+e1)/2;
    % % drainRate = (drainRate+mr1)/2;
    % % qMelt = (qMelt+qm1)/2;
    % % tsurf = (tsurf+tsurf1)/2;
    % % qnet = (qnet+qnet1)/2;
    % % qStor = (qStor+q1)/2;
    end % subroutine predicorr
    % 
    % *********************** QFM() ********************************
    %     Calculates Energy and Mass Flux at any instant
    function [snowWaterEquiv,snowHeatStore,snowStore,drainRate,evapRate,snowIceFrac,...
        snowThick,qMelt,qEvap,qSens,qNet,tSurf] = qfm(snowHeatStore,snowWaterEquiv,snowIceFrac,...
        airTemp,rainRate,snowRate,windspeed,airVapPress,qRadShort,qli,rkn,fc,pressure,qCondGround,dt,...
        heatCapGround,densGround,snowThick,uebParams)
    
    % Parameters
    [t0,tk,emissSnow,sbc,heatFusion,hneu,heatCapWater,heatCapIce,heatCapAir,ra,densWater,liquidCap,...
        depthHeatExch,rs] = deal_struct(uebParams,...
        'to','tk','es','sbc','hf','hneu','cw','cs','cp','ra',...
        'rhow','lc','de','rs');
    
    zerosGrid = zeros(size(airTemp));
    t0 = zerosGrid + t0; % need to distribute this scalar parameter
    
    prevSWE = snowWaterEquiv;
    
    % First, calculate the melting
    snowIce = snowWaterEquiv .* snowIceFrac;
    snowLiquid = snowWaterEquiv - snowIce;
    [snowMelt,liquidFreeze,snowLiquid,snowIce,snowThick] = fmelt(snowHeatStore,densWater,snowIce,...
        heatFusion,snowLiquid,snowThick); %
    meltRate = snowMelt / dt;
    freezeRate = liquidFreeze / dt;
    
    % Update liquid holding capacity
    liquidCapThick = liquidCap .* snowThick;
    
    % Now calculate final snow liquid and melt rate
    snowDrain = max(snowLiquid-liquidCapThick,0);
    snowLiquid = snowLiquid - snowDrain;
    drainRate = snowDrain / dt;
    
    % Update snowWaterEquiv 
    snowWaterEquiv = snowIce + snowLiquid;
    test = (snowWaterEquiv > 0);
    snowIceFrac(test) = snowIce(test) ./ snowWaterEquiv(test);
    snowIceFrac(~test) = 0;
    
    % Update heat capacity of the snow
    weightIce = snowIceFrac;
    weightWater = 1 - weightIce;
    heatCapSnow = weighted_mean('arithmetic',heatCapIce,heatCapWater,weightIce,weightWater);
    
    % Update the density of the snowpack
    densSnowpack = zerosGrid;
    densSnowpack(test) = snowWaterEquiv(test) ./ snowThick(test) .* densWater;
    densSnowpack(~test) = densWater; % this will work for all cells melted this timestep
    
    % Initialize other fluxes at 0
    [tave,qPrecip,tSurf,qle,qlnet,qSens,qEvap,evapRate,qCondSurf] = deal(zerosGrid);
    
    if any(test)
        % Calculate average snowpack temperature, only run for cells with snowWaterEquiv > 0
        tave(test) = tavg(snowHeatStore(test),snowWaterEquiv(test),densWater,heatCapSnow(test),t0(test),...
            densGround(test),depthHeatExch,heatCapGround(test),heatFusion);
        
        % Calculate advected heat from precipication
        qPrecip(test) = qpf(rainRate(test),airTemp(test),t0(test),snowRate(test),densWater,heatFusion,heatCapWater,heatCapSnow(test));
        
        % Calculate the surface temperature
        [tSurf(test),qCondSurf(test)] = srftmp(qRadShort(test),qli(test),qPrecip(test),airVapPress(test),...
            airTemp(test),tave(test),pressure(test),ra,heatCapAir,densSnowpack(test),...
            rkn(test),hneu,emissSnow,sbc,heatCapSnow(test),rs,snowWaterEquiv(test),...
            windspeed(test),fc(test),uebParams);
        
        % Calculate outgoing longwave radiation and net longwave
        qle(test) = (1-fc(test)) .* emissSnow .* sbc .* (tSurf(test) + tk).^4;
        qlnet(test) = qli(test) - qle(test);
        
        % Calculate the turbulent fluxes (sensible and latent)
        [qSens(test),qEvap(test),evapRate(test)] = turbflux(airTemp(test),tSurf(test),...
            rkn(test),windspeed(test),...
            pressure(test),ra,heatCapAir,hneu,airVapPress(test),densWater,uebParams);
        
        % Account for evaporation in snow water equivalent and drainRate
        test1 = evapRate ~= 0;
        if any(test1)
            % Test for excess evap
            evapTotal = evapRate(test1)*dt;
            evapSnowpack = evapRate*dt;
            evapSnowpack(test1) = min(snowWaterEquiv(test1),evapTotal);
            evapRemain = evapTotal - evapSnowpack(test1);
            evapDrain = min(evapRemain, drainRate(test1)*dt);
            fracUnsatisfied = (evapTotal - evapSnowpack(test1) - evapDrain)./evapTotal;
            qShift = qEvap(test1).*fracUnsatisfied;
            qEvap(test1) = qEvap(test1) - qShift;
            qSens(test1) = qSens(test1) + qShift;
            
            % Update snow thickness, subtract at same density
            test2 = evapSnowpack > 0;
            snowThick(test2) = snowThick(test2) .* (1 - evapSnowpack(test2)./snowWaterEquiv(test2));
            test3 = evapSnowpack < 0;
            snowThick(test3) = snowThick(test3);
            
            % Update change in water mass in the snowpack, drain rate
            snowWaterEquiv(test1) = snowWaterEquiv(test1) - evapSnowpack(test1);
            drainRate(test1) = drainRate(test1) - evapDrain/dt;
            evapRate(test1) = (evapSnowpack(test1) + evapDrain)/dt;
        end
    end
    
    % Update water storage
    snowStore = prevSWE - snowWaterEquiv;
    
    % Correct thickness, use mean of other cells
    test = (snowWaterEquiv > 0) & (snowThick == 0);
    test1 = (snowWaterEquiv > 0) & (snowThick > 0);
    snowThick(test) = mean(snowThick(test1));
    
    % Calculate net radiation
    qNet = qRadShort + qlnet;
    
    % Calculate the heat flux due to melting and freezing of water in the snowpack
    qMelt = meltRate.*densWater .* (heatFusion + (tave-t0).*heatCapWater) - ...
        freezeRate.*densWater .* (heatFusion + (t0-tave)*heatCapIce); % QM in kj/m2/hr.  
    
    % Original from UEB
    %qStor = qNet + qPrecip + qCondGround + qSens + qEvap - qMelt;
    qStor = qCondSurf + qPrecip + qCondGround + qSens + qEvap - qMelt;
    
    % Update snow heat store
    snowHeatStore = snowHeatStore + dt.*qStor;
    
    end 
    
    
    function [tsnow] = tavg(snowHeatStore,snowWaterEquiv,densWater,heatCapSnow,t0,...
        densGround,depthHeatExch,heatCapGround,heatFusion)
    %     Calculates the average temperature of snow and interacting
    %     soil layer
    
    snhc = densWater.*snowWaterEquiv.*heatCapSnow; % Snow heat capacity, in units of kJ/m2
    shc  = densGround.*depthHeatExch.*heatCapGround; % Soil heat capacity, kJ/m2
    chc  = snhc+shc; % Combined heat capacity, kJ/m2
    
    % When total heat storage is positive, but just run everywhere
    al = snowHeatStore./(densWater.*heatFusion); % units are m
    tsnow = (snowWaterEquiv>al).*t0 + ...
        (snowWaterEquiv<= al).*(snowHeatStore-snowWaterEquiv*densWater*heatFusion)./chc;
    
    % When total heat storage is less than zero
    test = (snowHeatStore<= 0);
    tsnow(test) = snowHeatStore(test)./chc(test);
    
    end % function  tavg
    
    
    function [ts,qCond] = srftmp(qRadShort,qli,qPrecip,airVapPress,airTemp,tave,pressure,ra,heatCapAir,...
        densSnowpack,rkn,hneu,emissSnow,sbc,heatCapSnow,rs,snowWaterEquiv,windspeed,fc,uebParams)
    %   Computes the surface temperature of snow
    %   This version written on 4/23/97 by Charlie Luce solves directly the
    %   energy balance equation using Newtons method - avoiding the linearizations
    %   used previously.  The derivative is evaluated numerically over the range
    %   ts t0 fff*ts  where fff = 0.999
    
    % fff = 0.999;
    perturb = 0.001;
    tol = 0.01;
    
    %     t0 ensure all temperatures in kelvin
    tak = unit_conversions(airTemp,'C','K');
    tavek = unit_conversions(tave,'C','K');
    
    testNoSnow = (snowWaterEquiv<= 0);
    testPrecip = (qPrecip > 0);
    
    qPrecip(testNoSnow & testPrecip) = 0;
    %      ignore the effect of precip advected
    %      energy on the calculation of surface temperature when there is no snow.
    %      Without this ridiculously high temperatures can result as the model
    %      tries to balance outgoing radiation with precip advected energy.
    
    ts = tak; % first approximation
    maxChange = 2; % maximum tempearture change per iteration
    minChange = -2;
    er = 1.0;
    niter = 0;
    lower = min(tavek,tak); % The interface temperature should almost never be colder than the colder of the air or the surface
    while (sum(er>tol)>0) && (niter<40)
        tslast = ts;
        [f1,qCond1] = surfeb(ts,rkn,windspeed,tak,qPrecip,ra,heatCapAir,hneu,pressure,airVapPress,...
            densSnowpack,heatCapSnow,rs,tavek,qRadShort,qli,fc,emissSnow,sbc,uebParams);
        [f2,qCond2] = surfeb(ts+perturb,rkn,windspeed,tak,qPrecip,ra,heatCapAir,hneu,pressure,airVapPress,...
            densSnowpack,heatCapSnow,rs,tavek,qRadShort,qli,fc,emissSnow,sbc,uebParams);
        test = (f2~= f1);
        ts(test) = ts(test) - min(max((perturb * f1(test)) ./(f2(test) - f1(test)),minChange),maxChange);
        ts(~test) = tslast(~test);
        % disp([max(ts),min(ts)])
        test = ts < lower;
        ts(test) = lower(test);
        er = mean(abs(ts - tslast));
        niter = niter+1;
    end
    qCond = (qCond1 + qCond2)/2;
    ts = unit_conversions(ts,'K','C');
    ts(testNoSnow & ts>0) = 0;
    
    if niter>=40
        warning_LHM('Srftemp niter warning UEB');
    end
    end % function srftmp
    
    % *********************************************************************
    % SURFEB
    function [surfebResult,qCond] = surfeb(tsk,rkn,windspeed,tak,qPrecip,ra,heatCapAir,...
        hneu,pressure,airVapPress,dens,heatCapSnow,rs,tavek,qRadShort,qli,fc,emissSnow,sbc,uebParams)
    %      function to evaluate the surface energy balance for use in solving for
    %      surface temperature
    %      DGT and C Luce 4/23/97
    tk = uebParams.tk;
    rkin = rkinst(rkn,windspeed,tak,tsk,uebParams);
    [qSens,qEvap] = turbflux(tak-tk,tsk-tk,rkin,windspeed,pressure,ra,heatCapAir,...
        hneu,airVapPress,dens,uebParams);
    qCond = dens.*heatCapSnow.*rs.*(tsk-tavek);
    qle = (1 - fc) .* emissSnow .* sbc .* tsk.^4;
    surfebResult = qPrecip + qSens + qEvap - qCond + qRadShort + qli - qle;
    
    end % function surfeb
    
    % *********************************************************************
    % RKINST
    function [rkinstResult] = rkinst(rkn,windspeed,airTemp,ts,uebParams)
    %     function to calculate no neutral turbulent transfer coefficient using the
    %     richardson number correction.
    % airTemp must be in K
    
    [z,g,fstab] = deal_struct(uebParams,'z','g','fstab');
    rkinstResult = zeros(size(airTemp));
    test = (windspeed>0); % If now wind, no sensible or latent heat fluxes.
    rich = g*(airTemp(test)-ts(test))*z./(windspeed(test).^2.*airTemp(test));
    rkinstResult(test) = (rich>0).*rkn(test)./(1+10.*rich)+(rich<= 0).*rkn(test).*(1-10.*rich);
    %  Linear damping of stability correction through parameter fstab
    rkinstResult = rkn+fstab.*(rkinstResult-rkn);
    end % function rkinst
    
    
    % ************************** QPF() ***************************
    %     Calculates the heat advected to the snowpack due to rainRate
    
    function [qpfResult] = qpf(pressure,airTemp,t0,snowRate,densWater,heatFusion,heatCapWater,heatCapSnow)
    
    train = t0;
    tsnow = airTemp;
    
    test = (airTemp > t0);
    train(test) = airTemp(test);
    tsnow(test) = t0(test);
    
    qpfResult = pressure*densWater.*(heatFusion+heatCapWater.*(train-t0))+...
        snowRate*densWater.*heatCapSnow.*(tsnow-t0);
    
    end % function qpf
    
    % *********************** TURBFLUX() ***************************
    %     Calculates the turbulent heat fluxes(sensible and latent
    %     heat fluxes) and condensation/sublimation.
    function [qSens,qEvap,evapRate] = turbflux(airTemp,ts,rkn,windspeed,pressure,ra,...
        heatCapAir,hneu,airVapPress,densWater,uebParams)
    tak = unit_conversions(airTemp,'C','K');
    tsk = unit_conversions(ts,'C','K');
    rkin = rkinst(rkn,windspeed,tak,tsk,uebParams);
    rhoa = pressure./(ra.*(tak));%     RHOA in kg/m3
    qSens = rhoa.*(airTemp-ts).*heatCapAir.*rkin;
    satVapPress = sat_vap_pressure(ts);
    qEvap = 0.622.*hneu./(ra.*(tak)).*rkin.*(airVapPress-satVapPress);
    if nargout ==  3
        evapRate = -qEvap./(densWater.*hneu);%     evapRate in  m/hr
    end
    
    end
    
    
    % *********************** FMELT() ***************************
    %     Calculates the melt rate and melt outflow
    function [snowMelt,liquidFreeze,snowLiquid,snowIce,snowThick] = fmelt(snowHeatStore,densWater,snowIce,...
        heatFusion,snowLiquid,snowThick)
    % function [fmeltResult] = fmelt(snowHeatStore,densWater,snowWaterEquiv,heatFusion,liquidCap,rid,ks,rainRate)
    
    
    [snowMelt,newMelt,liquidFreeze] = deal(zeros(size(snowHeatStore)));
    
    % Calculate available melt
    test1 = (snowHeatStore > 0) & (snowIce > 0);
    if any(test1)
        newMelt(test1) = min(snowHeatStore(test1) ./ (densWater * heatFusion),snowIce(test1));
        snowLiquid(test1) = snowLiquid(test1) + newMelt(test1);
        origSnowIce = snowIce;
        snowIce(test1) = snowIce(test1) - newMelt(test1);
        snowThick(test1) = snowThick(test1) .* (1 - newMelt(test1)./origSnowIce(test1));
        snowThick(snowThick<=0) = 0; %should only be numerical error that can cause this
    end
    % if any(isnan(snowThick))
    %     disp('debug me');
    % end
    % if any((snowIce > 0) & (snowThick==0))
    %     disp('debug me');
    % end
    
    % Calculate freezing of snowLiquid
    test2 = (snowHeatStore < 0) & (snowLiquid > 0);
    if any(test2)
        liquidFreeze(test2) = min(-snowHeatStore(test2)./(densWater * heatFusion),snowLiquid(test2));
        snowLiquid(test2) = snowLiquid(test2) - liquidFreeze(test2);
        snowIce(test2) = snowIce(test2) + liquidFreeze(test2);
    end
    
    end % function fmelt
    
    
    % function [drainRate,evapRate,qMelt,qEvap,qStor] = prehelp(w1,snowWaterEquiv,dt,...
    %     ablation1,fac,snowRate,rainRate,evapRate,densWater,heatFusion,qStor,qMelt,qEvap,hneu)
    % %      Routine to correct energy and mass fluxes when
    % %      numerical overshoots dictate that snowWaterEquiv was changed in
    % %      the calling routine - either because snowWaterEquiv went negative
    % %      or due to the liquid fraction being held constant.
    % 
    % %   The next statement calculates the value of storRate given
    % %    the snowWaterEquiv and w1 values
    % storRate = (w1-snowWaterEquiv)/dt.*fac-ablation1;
    % %   The next statement calculates the changed meltRate and evapRate due to the new storRate.
    % %    storRate was = rainRate+snowRate-meltRate-evapRate
    % %    Here the difference is absorbed in meltRate first and then evapRate if meltRate < 0.
    % 
    % drainRate = max(0,(snowRate+rainRate-storRate-evapRate));
    % evapRate = snowRate+rainRate-storRate-drainRate;
    % %    Because melt rate changes the advected energy also changes.  Here
    % %     advected and melt energy are separated,
    % qother = qStor+qMelt-qEvap;
    % %     then the part due to melt recalculated
    % qMelt = drainRate*densWater.*heatFusion;
    % %     then the part due to evaporation recalculated
    % qEvap = -evapRate*densWater.*hneu;
    % %     Energy recombined
    % qStor = qother-qMelt+qEvap;
    % end % subroutine prehelp
    