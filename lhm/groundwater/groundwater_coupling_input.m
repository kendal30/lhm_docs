function state = groundwater_coupling_input(state,structure,params,m)
%GROUNDWATER_COUPLING_INPUT read in MODFLOW calculated head and cbc flux
%   output = groundwater_coupling_input(input)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> groundwater_coupling_input
%
%   See also:

% Author: Anthony Kendall, Tianfang Xu
% Contact: anthony [dot] kendall [at] msu [dot] edu
% Created: 2014-08-14
% Modified: 2017-06-30 onlineCoupling
% Copyright 2014 Michigan State University.

%Get needed inputs
[onlineCoupling,timestep,dcell,zerosGrid] = deal_struct(structure,...
    'groundwater_coupling_online','timestep_length','cell_resolution','zeros_grid');

%Specify here for now, can change later to fetch from structure or
%groundwater
timestepGW = 86400; %this is in seconds

if onlineCoupling > 0
    % read head at the end of the day before yesterday
    load .\groundwater\initial_head.txt
    % map to LHM array
    prevWtElev = initial_head(:);
    prevWtElev(isnan(prevWtElev)) = [];
    %Update water_table_depth in both uplands and wetlands
    topography = deal_struct(structure,'topography');
%     elevUpland = topography.elevation_mean;
%     elevWetland = topography.elevation_min;
    elevUpland = topography.elevation_up;
    elevWetland = topography.elevation_wet;

    prevWTDepthUpland = elevUpland - prevWtElev;
    prevWTDepthWetland = elevWetland - prevWtElev;

    % read head at the end of yesterday
    % head lower than this value will be set to nan, either inactive cell
    % or dry cell. Assuming that MODFLOW grid is aligned with LHM, daily
    % simulation & stress period, hourly time step
    opt.nan = -999;  
    opt.nrow = structure.num_row;
    opt.ncol = structure.num_col;
    opt.nlayer = 1;  % to be put in config.xls
    opt.m = m; % write instruction file only once
    fname = 'daily.head';  
    wtElev_grid = read_modflow_head(fname,1,1:opt.ncol,1:opt.nrow,1,opt);
    % prepare initial head for next run (after LHM loop today)
    save .\groundwater\initial_head.txt wtElev_grid -ascii -tabs

    % map to LHM array
    wtElev = wtElev_grid(:);
    idNan = isnan(wtElev);
    wtElev(idNan) = [];
    %Update water_table_depth in both uplands and wetlands
    wtDepthUpland = elevUpland - wtElev;
    wtDepthWetland = elevWetland - wtElev;
    
%     prevWTDepthWetland = wtDepthWetland;
%     prevWTDepthUpland = wtDepthUpland;

    % Clear soil moisture for saturated layers
    if (m == 1)
        [waterSoil] = deal_struct(state,'water_soil');
        [soilThick] = deal_struct(structure,'thickness_soil');
        upSatFrac = gw_lay_sat(soilThick,prevWTDepthUpland);
        ind = upSatFrac ==1;
%         waterSoilRem = zeros(size(waterSoil));
%         waterSoilRem(ind) = waterSoil(ind);
%         rechargeWTUpland = rechargeWTUpland + sum(waterSoilRem,2);
        waterSoil(ind) = 0;
        state = update_struct(state,'water_table_depth_wetland',prevWTDepthWetland,...
            'water_table_depth_upland',prevWTDepthUpland,'water_soil',waterSoil);
        % zero for the first day
        dischargeWetland = zerosGrid;
    else
        state = update_struct(state,'water_table_depth_wetland',prevWTDepthWetland,...
            'water_table_depth_upland',prevWTDepthUpland);
        % groundwater discharge to wetland (baseflow)
        fname = 'daily.cbc'; 
%         dischargeWetland = function_read_drain_fluxes;
        dischargeWetland = read_modflow_drain_flux(fname,1,1:opt.ncol,1:opt.nrow,1,opt);
        dischargeWetland = - dischargeWetland(:); % modflow drain flux is always negative (gw out)
        dischargeWetland(idNan) = [];
    end
else
    [prevWTDepthWetland,prevWTDepthUpland,timestep] = deal_struct(state,'water_table_depth_wetland','water_table_depth_upland','outer_timestep');

    %Update the water table depth in wetlands
    [wtDepthWetland,wtDepthUpland] = water_table_depth(state,structure,params);
    
    %Discharge to wetlands is 0
    dischargeWetland = zerosGrid;
end


%Update the rate of change of water table depth
wtDepthChangeWetland = (wtDepthWetland - prevWTDepthWetland)/timestepGW;
wtDepthChangeUpland = (wtDepthUpland - prevWTDepthUpland)/timestepGW;

% distribute drain flux to every LHM time step, i.e., hour
dischargeWetland = dischargeWetland / timestepGW * timestep / dcell^2;

%Update the state variables structure
state = update_struct(state,'water_table_change_upland',wtDepthChangeUpland,'water_table_change_wetland',wtDepthChangeWetland,...
    'groundwater_discharge_wetland',dischargeWetland);
% TXU 12/23/2016
% No need to store this variable. What is needed is another
% groundwater_discharge_wetland calculated in update_water_table because
% that needs to be removed from groundwater
% state = update_struct(state,'water_table_change_upland',wtDepthChangeUpland,'water_table_change_wetland',wtDepthChangeWetland);


%% update other water table related parameters
%     %Load the starting heads grid
%     stateGrids = load_grid_params(paths,'state','STAT');
% 
%     %Calculate total unsaturated thickness array
%     totalUnsatThickness = max(0,structure.topography.elevation_mean - stateGrids.starting_heads);
%     meanThick = nanmean(totalUnsatThickness(:));
%     totalUnsatThickness(isnan(totalUnsatThickness)) = meanThick;
% 
%     %Calculate root zone thickness array
%     structure.root_zone_thickness = max(structure.min_soil_thickness,min(structure.max_soil_thickness,totalUnsatThickness));
% 
%     %Now for unsaturated zone thickness
%     structure.unsat_thickness = max(0,totalUnsatThickness-structure.root_zone_thickness);
% 


end

function [wtDepthWetland,wtDepthUpland] = water_table_depth(state,structure,params)
%Calculates water table depth, currently only for wetlands, via an
%empirical function (sinusoidal)
%   water_table_depth(date)
%
%   Note: water table depth is positive downwards
%
%   Descriptions of Input Variables:
%   date:   matlab datenum of the current model day
%
%   Descriptions of Output Variables:
%   wtDepthWetland: positive downwards
%   wtDepthUpland: currently just zero

date = deal_struct(state,'date');
[wtAmp,disconnThresh] = deal_struct(params,'wetland_water_table_amp','wetland_disconn_thresh');
[indShallowWetland,indDisconn,indConnected,wetBottom,zerosGrid] = deal_struct(structure,...
    'index_shallow_wetland','index_disconnected_wetlands','index_connected_wetlands','wetland_bottom_depth','zeros_grid');

%Calculate the fraction of the year
[doy,fracYear] = date2doy(date); %#ok<ASGLU>

%Calculate water table depth with a simple sinusoidal function, max is
%~April 1, min is ~Oct 1. This is defined as positive downward from the
%bottom of the wetland
[wtDepthWetland,wtDepthUpland] = deal(zerosGrid);
ind = indDisconn & indShallowWetland; %only disconnected, shallow wetlands
wtDepthWetland(ind) = disconnThresh * 1.1; %to keep these disconnected
ind = indConnected & indShallowWetland; %deep wetlands, assume water table is at the surface
wtDepthWetland(ind) = -wetBottom(ind) - wtAmp * sin(2 * pi() * fracYear);
end

function [upSatFrac,wtLayer] = gw_lay_sat(soilThick,wtDepthUpland)
% find which soil layer groundwater table is in, and calculate saturation
% fraction of all layers

% determine which soil layer the current gw level is in
numLay = size(soilThick,2);
depthSoil = cumsum(soilThick,2);

wtDepthRepmat = repmat(wtDepthUpland,[1,numLay]);
indWet = depthSoil > wtDepthRepmat;

wtLayer = (numLay + 1) - sum(indWet,2);
wtLayer(wtDepthUpland<0) = 0;

% Compute the thickness of each layer saturated
satThick = zeros(size(soilThick));
satThick(indWet) = min(soilThick(indWet), depthSoil(indWet) - wtDepthRepmat(indWet));

% Finally, calculate the saturated fraction
upSatFrac = satThick ./ soilThick;
end
