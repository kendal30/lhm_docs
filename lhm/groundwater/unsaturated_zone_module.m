function state = unsaturated_zone_module(state,structure,groundwater)
    % load data from the LHM data structures
    [paths,xllcornerSurf,yllcornerSurf,cellsizeSurf,numColSurf,numRowSurf,trimStruct,...
        throughflowFlowtimes,watershedsRelate,watershedKey,...
        unsatThick,rootThick,indExternal,timestepLength,zerosGrid] = ...
        deal_struct(structure,...
        'paths','left','bot','cell_resolution','num_col','num_row','trim_struct',...
        'runoff_subsurface_flowtimes','runoff_watershed_relate','runoff_watershed_key',...
        'unsat_thickness','thickness_soil','index_externally_drained','timestep_length','zeros_grid');
    [package,structureGnd,params,constants,mult] = deal_struct(groundwater,...
        'package','structure','params','constants','mult');
    [watershedThroughflow] = deal_struct(state,'watershed_throughflow');
    
    % Delay table, this was derived empirically from a 1-D richard's equation
    % model, first row is vkSat, second is the delay parameter
    % Eventually move to a parameter file (original delay was 2.47)
    delayTable = [20,1.62;10,1.81;5,2.47;5/2,3.13;5/3,4.18];
    % delayParam = 2.47;
    
    % force a maximum stress period delay
    maxDelay = 25; % for this model, this is 1/2 year
    
    % Set the number of days to prime the unsaturated zone
    primeDays = 365;
    
    % Update the watershedsRelate array for subsurface throughflow routing
    % overwriting the surface flowtimes with the subsurface flowtimes
    watershedsRelate(:,3) = watershedsRelate(:,4);
    
    %--------------------------------------------------------------------------
    % Load some grids
    %--------------------------------------------------------------------------
    gridLoad = load_grid_params(paths.input.static,'groundwater');
    
    % Handle different means of specifying vertical conductivity
    specVK = false;
    if isfield(gridLoad,'vk')
        grids.vk = gridLoad.vk;
        constVK = false;
        specVK = true;
    elseif isfield(constants,'vk')
        constVK = true;
    else
        [constHK,constVANI] = deal(true);
        if isfield(gridLoad,'hk')
            grids.hk = gridLoad.hk;
            constHK = false;
        end
        if isfield(gridLoad,'vani')
            grids.vani = gridLoad.vani;
            constVANI = false;
        end
    end
    
    % Get the first layer ibound
    ibound = gridLoad.ibound(:,:,1);
    clear gridLoad
    
    %--------------------------------------------------------------------------
    % set up calculated model-specific arrays and values
    %--------------------------------------------------------------------------
    % Zeros grids
    zerosGridGnd = zeros([structureGnd.num_row,structureGnd.num_col,structureGnd.num_lay]);
    zerosGridLay = zerosGridGnd(:,:,1);
    
    spStart = structureGnd.start_date + cumsum(structureGnd.perlen) - structureGnd.perlen;
    spStart(end+1) = spStart(end) + structureGnd.perlen(end);
    if strcmpi(structureGnd.simulation_type,'RAMP') % The first steady-state stress period is a virtual one
        spStart = spStart - structureGnd.perlen(1);
    end
    xArray = structureGnd.xllcorner + (0:structureGnd.cellsize:structureGnd.cellsize*(structureGnd.num_col-1)) + structureGnd.cellsize/2;
    yArray = fliplr(structureGnd.yllcorner + (0:structureGnd.cellsize:structureGnd.cellsize*(structureGnd.num_row-1)))' + structureGnd.cellsize/2;
    xArraySurf = xllcornerSurf + (0:cellsizeSurf:cellsizeSurf*(numColSurf-1)) + cellsizeSurf / 2;
    yArraySurf = fliplr(yllcornerSurf + (0:cellsizeSurf:cellsizeSurf*(numRowSurf-1)))' + cellsizeSurf / 2;
    
    % Create a lookup table for the coarse-fine mapping, of form fineData =
    % coarseData(lookup)
    coarseIndeces = reshape((1:numColSurf*numRowSurf),numRowSurf,numColSurf);
    gndwaterLookup = uint32(interp2(xArraySurf,yArraySurf,coarseIndeces,xArray,yArray,'nearest'));
    gndwaterMask = (gndwaterLookup == 0);
    gndwaterLookup(gndwaterMask) = 1;
    
    % Create appropriate masks, one for where the groundwater model is outside
    % of the surface water domain, the other where the surface domain is larger
    % than the groundwater model
    maskGndwater = (ibound == 0);
    maskSurface = structure.trim_struct.indices(gndwaterLookup);
    
    %--------------------------------------------------------------------------
    % Create vk array, in groundwater resolution
    %--------------------------------------------------------------------------
    % Handle different cases for vk
    if ~specVK
        % Get HK
        if isfield(params,'hk')
            hk = zerosGridGnd;
            % Build the hk grid
            for m = 1:length(params.hk.code)
                thisInd = (grids.hk(:,:,1)==params.hk.code(m));
                hk(thisInd) = params.hk.val(m);
            end
        elseif constHK
            % HK value is constant
            hk = constants.hk + zerosGridGnd;
        else
            % HK grid is given directly
            hk = grids.hk(:,:,1);
        end
        
        % Get VANI
        if isfield(params,'vani')
            vani = zerosGridGnd;
            % Build the vani grid
            for m = 1:length(params.vani.code)
                thisInd = (grids.vani(:,:,1)==params.vani.code(m));
                vani(thisInd) = params.vani.val(m);
            end
        elseif constVANI
            % VANI value is constant
            vani = constants.vani + zerosGridGnd;
        else
            % VANI grid is given directly
            vani = grids.vani(:,:,1);
        end
        
        % Use a multiplier for each array
        if isfield(mult,'hk')
            hk = hk .* mult.hk.curr(1);
        end
        if isfield(mult,'vani')
            vani = vani .* mult.vani.curr(1);
        end
        
        % calculate vk
        vk = hk ./ vani;
        vk(isnan(vk)) = 0;
    else
        % Get VK
        if isfield(params,'vk')
            vk = zerosGridGnd;
            % Build the vk grid
            for m = 1:length(params.vk.code)
                thisInd = (grids.vk(:,:,1)==params.vk.code(m));
                vk(thisInd) = params.vk.val(m);
            end
        elseif constVK
            % VK value is constant
            vk = constants.vk + zerosGridGnd;
        else
            % VK grid is given directly
            vk = grids.vk(:,:,1);
        end
    
        % Use a multiplier
        if isfield(mult,'vk')
            vk = vk .* mult.vk.curr(1);
        end
    end
    
    % Define the recharge capacity, and interpolate the recharge capacity to LHM resolution
    rechCap = interp2(xArray,yArray,vk,xArraySurf,yArraySurf);
    rechCap = trim_domain(trimStruct,rechCap);
    
    % Clean up
    clear grids
    
    %--------------------------------------------------------------------------
    % Read in the lookup table and index from the percolation file
    %--------------------------------------------------------------------------
    % First, for deep percolation
    percIndex = h5dataread(paths.output.perc,'/deep_percolation/index');
    percLookup = h5dataread(paths.output.perc,'/deep_percolation/lookup');
    
    % Just in case there are 0s in the lookup table, convert to 1
    percLookup(percLookup==0) = 1;
    
    % Determine the size of the percolation array
    percSize = h5varget(paths.output.perc,'/deep_percolation/data',false);
    
    %--------------------------------------------------------------------------
    % read in the delay grid and create the delay masks stack, if needed
    %--------------------------------------------------------------------------
    if any(strcmpi(structureGnd.simulation_type,{'RAMP','TR'}))
        % Convert vk to delayTable
        delayParam = zerosGridLay;
        delayParam(:) = interp1(delayTable(:,1),delayTable(:,2),vk(:),'linear','extrap');
    
        % Convert to LHM resolution
        delayParam = interp2(xArray,yArray,delayParam,xArraySurf,yArraySurf);
    
        % Account for thickness of root zone soil
        unsatThick = unsatThick - sum(rootThick,2);
        unsatThick(unsatThick < 0) = 0;
    
        % interpolate the unsat. thickness grid to MODFLOW grid
        unsatThick = extend_domain(trimStruct,unsatThick); % extend unsaturated thickness to ILHM model from trimmed
    
        % convert unsat. thickness to delay
        delayGrid = unsatThick .* delayParam;
    
        % next, convert the delay grid into model stress periods, and clean it up
        spLength = structureGnd.perlen(1); % this is appropriate only for fixed-length stress periods
        delayGrid = floor(((delayGrid+spLength/2)/spLength));
        delayGrid = limit_vals([0,maxDelay],delayGrid);
        delayGrid(isnan(delayGrid)) = 0;
    
        % Calculate the number of unique delay values
        numDelay = maxDelay + 1;
    
        % finally, create the indeces grid to select from the perc stack
        delayInd = int32((delayGrid)*(numRowSurf*numColSurf) + reshape((1:numRowSurf*numColSurf),numRowSurf,numColSurf));
    
        % Create the blank perc stack
        percStack = zeros(numRowSurf,numColSurf,numDelay,'single');
    end
    
    %--------------------------------------------------------------------------
    % Prepare the SS recharge, if needed
    %--------------------------------------------------------------------------
    % Open the binary recharge file
    fid = fopen([paths.output.groundwater,filesep,package.rch_bin.name],'w');
    
    % Determine which stress period to start with for accumulating recharge
    if any(strcmpi(structureGnd.simulation_type,{'SS','TR'}))
        startLoop = 1;
    elseif strcmpi(structureGnd.simulation_type,'RAMP')
        startLoop = 2;
    end
    
    if any(strcmpi(structureGnd.simulation_type,{'SS','RAMP'}))
        % Initialize the stress period recharge array
        spRecharge = extend_domain(trimStruct,zerosGrid);
    
        h = waitbar(0,sprintf('Writing MODFLOW %s recharge',structureGnd.simulation_type));
        for m = startLoop:structureGnd.nper
            % Determine the length of the inner loop, to prevent reading in
            % the entire PERC file at once, read in 7 days at once
            numInner = ceil((structureGnd.perlen(m) - startLoop + 1)/7);
            
            for n = 1:numInner
                waitbar((m-1+n/numInner)/structureGnd.nper,h);
    
                % Set the inner start and end
                if numInner == 1
                    innerStart = spStart(m);
                    innerEnd = spStart(m+1) - 1e-5;
                else
                    innerStart = spStart(m) + (n-1)*7;
                    innerEnd = spStart(m) + n*7 - 1e-5;
                end
                
                % Read in the data
                % Determine the start and end rows to read in from the HDF files for
                % this stress period
                rowIndPerc = bracket_index(percIndex(:,1),innerStart,innerEnd);
    
                % Read in this stress period's deep percolation and transpiration
                % arrays, and sum
                innerPerc = h5dataread(paths.output.perc,'/deep_percolation/data',true,...
                    [rowIndPerc(1)-1,0],[diff(rowIndPerc)+1,percSize(2)],[1,1]);
    
                % In case there was no percolation or transpiration written out, though
                % this shouldn't occur
                if islogical(innerPerc),innerPerc = zerosGrid';end
    
                % For the transient part of the model run, we are going to remove excess throughflow
                % on a daily basis, to preserve the temporal structure for stream routing
                throughflow = max(innerPerc - repmat(rechCap',[size(innerPerc,1),1]),0); %defined at LHM resolution
                innerPerc = innerPerc - throughflow;
    
                % Sum across rows, results in meters/stress period
                innerPerc = sum(innerPerc,1);
    
                % Convert to grid dimensions
                innerPerc = innerPerc(percLookup);
    
                % Remove any NaNs
                innerPerc(isnan(innerPerc)) = 0;
                
                % Sum percolation grids for total recharge
                spRecharge = spRecharge + innerPerc; 
            end
        end
        if ishandle(h); close(h); end
    
        % Convert total over stress period to m/d
        spRecharge = spRecharge  / sum(structureGnd.perlen(startLoop:end));  % to convert to m/d
    
        % Interpolate to groundwater model resolution
        spRechargeInterp = spRecharge(gndwaterLookup);
    
        % Mask out the domain mismatch areas
        spRechargeInterp(maskSurface) = 0;
        spRechargeInterp(maskGndwater) = 0;
    
        % Write out data to binary recharge file
        write_binary_array(fid,1,1,structureGnd.perlen(1),structureGnd.perlen(1),...
            'RechargeBinary00',structureGnd.num_col,structureGnd.num_row,1,spRechargeInterp);
    end
    
    %--------------------------------------------------------------------------
    % Process transient recharge, if any
    %--------------------------------------------------------------------------
    if any(strcmpi(structureGnd.simulation_type,{'RAMP','TR'}))
        % Calculate the groundwater model transient timestep length in days, assumes that all model
        % transient stress periods have identical length timesteps
        if strcmpi(structureGnd.timeunits,'days')
            timestepLength = structureGnd.perlen(startLoop)/structureGnd.nstp(startLoop);
        end
    
        h = waitbar(0,'Writing MODFLOW transient recharge...');
        % Prime the unsaturated zone by running the first year(s) without writing anything to the MODFLOW recharge
        % binary file
        for m = startLoop:structureGnd.nper
            % Break out of this loop if we have gone past the length of priming
            if spStart(m) > primeDays
                break
            end
    
            % Read in the data
            % Determine the start and end rows to read in from the HDF files for
            % this stress period
            rowIndPerc = bracket_index(percIndex(:,1),spStart(m),spStart(m+1)-1e-5);
    
            % Read in this stress period's deep percolation
            % arrays, and sum
            spPerc = h5dataread(paths.output.perc,'/deep_percolation/data',true,...
                [rowIndPerc(1)-1,0],[diff(rowIndPerc)+1,percSize(2)],[1,1]);
    
            % In case there was no percolation or transpiration written out, though
            % this shouldn't occur
            if islogical(spPerc),spPerc = zerosGrid';end
                
            % For the transient part of the model run, we are going to remove excess throughflow
            % on a daily basis, to preserve the temporal structure for stream routing
            throughflow = max(spPerc - rechCap,0); %defined at LHM resolution
            spPerc = spPerc - throughflow;
    
            % Sum across rows, results in meters/stress period
            spPerc = sum(spPerc,1);
    
            % Convert to grid dimensions
            spPerc = spPerc(percLookup);
    
            % Remove any NaNs
            spPerc(isnan(spPerc)) = 0;
    
            % Update the percolation grid stack
            % shift the percolation grid stack up 1
            percStack(:,:,2:end) = percStack(:,:,1:end-1);
    
            % Add the current stress period percolation to the bottom of the stack
            percStack(:,:,1) = spPerc;
        end
    
    
        % Now, with the primed percStack, go ahead and start writing out recharge
        for m = startLoop:structureGnd.nper
            waitbar(m/structureGnd.nper,h);
    
            % Read in the data
            % Determine the start and end rows to read in from the HDF files for
            % this stress period
            rowIndPerc = bracket_index(percIndex(:,1),spStart(m),spStart(m+1)-1e-5);
    
            % Read in this stress period's deep percolation
            % arrays, and sum
            spPerc = h5dataread(paths.output.perc,'/deep_percolation/data',true,...
                [rowIndPerc(1)-1,0],[diff(rowIndPerc)+1,percSize(2)],[1,1]);
    
            % In case there was no percolation or transpiration written out, though
            % this shouldn't occur
            if islogical(spPerc),spPerc = zerosGrid';end
    
            % For the transient part of the model run, we are going to remove excess throughflow
            % on a daily basis, to preserve the temporal structure for stream routing
            % throughflow will get routed for this stress period (on a daily basis), down below
            throughflow = max(spPerc - repmat(rechCap',[size(spPerc,1),1]),0); %defined at LHM resolution
            spPerc = spPerc - throughflow;
    
            % Sum across rows, results in meters/stress period
            spPerc = sum(spPerc,1);
    
            % Convert to LHM grid dimensions
            spPerc = spPerc(percLookup);
    
            % Remove any NaNs
            spPerc(isnan(spPerc)) = 0;
    
            % Update the percolation grid stack
            % shift the percolation grid stack up 1
            percStack(:,:,2:end) = percStack(:,:,1:end-1);
    
            % Add the current stress period percolation to the bottom of the stack
            percStack(:,:,1) = spPerc;
    
            % Calculate spRecharge from the percStack, convert units, resample
            % Select current stress period recharge values from the perc stack
            spRecharge = percStack(delayInd) / structureGnd.perlen(m);  % to convert to m/d
    
            % Interpolate to groundwater model resolution
            spRechargeInterp = spRecharge(gndwaterLookup);
    
            % Mask out the domain mismatch areas
            spRechargeInterp(maskSurface) = 0;
            spRechargeInterp(maskGndwater) = 0;
    
            % Write out data to binary recharge file
            write_binary_array(fid,1,m,structureGnd.perlen(m),spStart(m)-spStart(1)+structureGnd.perlen(m),...
                'RechargeBinary00',structureGnd.num_col,structureGnd.num_row,1,spRechargeInterp);
    
            % Route throughflow through the stream network, daily
            % This uses the reduced domain (i.e. only active LHM surface cells)
            for n = 1:structureGnd.perlen(m)
                % Trim to the surface model domain
                thisThroughflow = trim_domain(trimStruct,throughflow(n,:))';
    
                % Get the externally-drained portion of this timestep's throughflow
                thisThroughExt = thisThroughflow .* indExternal;
    
                % Get the current model day
                thisDay = sum(structureGnd.perlen(1:m-1)) + n;
    
                % Run the runoff model accumulation function for this day
                [watershedThroughflow] = runoff_model(watershedThroughflow,thisThroughExt,thisDay,...
                    throughflowFlowtimes,watershedsRelate,watershedKey,timestepLength,cellsizeSurf);
            end
        end
        if ishandle(h); close(h); end
    end
    fclose(fid);
    
    % Update the state array
    state = update_struct(state,'watershed_throughflow',watershedThroughflow);
    end
    
    
    function write_binary_array(fid,kstp,kper,pertim,totim,text,ncol,nrow,nlay,recharge)
    fwrite(fid,kstp,'int'); % KSTP
    fwrite(fid,kper,'int'); % KPER
    fwrite(fid,pertim,'float32'); % PERTIM
    fwrite(fid,totim,'float32'); % TOTIM
    fwrite(fid,text,'char'); % TEXT
    fwrite(fid,ncol,'int'); % NCOL
    fwrite(fid,nrow,'int'); % NROW
    fwrite(fid,nlay,'int'); % NLAY
    fwrite(fid,recharge','float32'); % the transpose is because MATLAB writes data columnwise
    end