function streamflow_composite(state,structure)
%This function composites the baseflow, throughflow, and surface flow components of
%streamflow. It also subtracts out post-evaporative demand

%Get the LHM values
[paths,dates,watershedKey,innerHours] = deal_struct(structure,'paths','dates',...
    'runoff_watershed_key','inner_timestep_hours');
[watershedRunoff,watershedThroughflow,watershedBaseflow,watershedEvapDemand] = deal_struct(state,...
    'watershed_runoff','watershed_throughflow','watershed_baseflow','watershed_evap_demand');

% Load the observations, and prepare our output table
tableObs = h5obstableread(paths.input.obs,'station/streamflow');
tableOut = tableObs(:,{'time_start','grid_id','loc_id','loc_name','value'});
tableOut.grid_id = categorical(tableOut.grid_id);
tableOut.time_start = uint32(tableOut.time_start);

% Prepare a structured array so the processing can be done in a loop
processStruct = struct();
processStruct.variables = {watershedRunoff,watershedThroughflow,watershedBaseflow,watershedEvapDemand};
processStruct.hourly = {true,false,false,true};
processStruct.column = {'runoff','throughflow','baseflow','evap_demand'};

%--------------------------------------------------------------------------
% Process watershedRunoff and watershedEvapDemand
%--------------------------------------------------------------------------
% Convenience variables
numSheds = length(watershedRunoff);
numDates = length(dates);

for m = 1:length(processStruct.variables)
    thisVar = processStruct.variables{m};
    
    % Build our table of daily values
    thisTable = table('Size',[numDates,numSheds+1],'VariableTypes',["uint32",repmat("double",[1,numSheds])],...
        'VariableNames',["time_start",string([watershedKey{:,1}])]);

    % Assign the dates column
    thisTable.time_start = uint32(dates');

    % Loop through the watershedRunoff and assign columns, resample daily
    for n = 1:numSheds
        if processStruct.hourly{m}
            thisHours = dates(1) + (0:length(thisVar{n})-1)*innerHours/24;
            [~,thisDaily] = time_series_aggregate(thisHours,thisVar{n},'days','mean');
        else
            thisDaily = thisVar{n};
        end
        thisTable.(string(watershedKey{n,1}))(:) = thisDaily(1:numDates);
    end

    % Stack the table
    thisTable = stack(thisTable,2:numSheds+1,'NewDataVariableName',...
        processStruct.column{m},'IndexVariableName','grid_id');

    % Join to the observations table
    tableOut = innerjoin(tableOut,thisTable,'Keys',{'grid_id','time_start'});
end

% Calculate the simulated field from the routed quantities
tableOut.simulated = tableOut.runoff + tableOut.throughflow + tableOut.baseflow - tableOut.evap_demand;
tableOut.simulated(tableOut.simulated < 0) = 0;

%--------------------------------------------------------------------------
% Save the output
%--------------------------------------------------------------------------
gauge_flow = tableOut;

if structure.groundwater_enabled
    save(paths.output.simobs,'gauge_flow','-append');
else
    save(paths.output.simobs,'gauge_flow');

    % Add date string column in output file
    gauge_flow.datetime = datestr(gauge_flow.time_start);

    % Create a .csv version
    [outDir,outFilename,~] = fileparts(paths.output.simobs);
    writetable(gauge_flow,[outDir,filesep,outFilename,'.csv']);
end
end