function state = groundwater_flow_routing(state,structure,params,groundwater)
% This code reads in MODFLOW drains discharge and routes it watersheds
% It produces a new state variable, watershed_baseflow
% NOTE: For this to function properly, MODFLOW timesteps must be daily for now

% load data from the LHM data structures
[paths,xllcornerLHM,yllcornerLHM,cellsizeLHM,numColLHM,numRowLHM,...
    baseflowFlowtimes,watershedsRelate,watershedKey,...
    upFrac,wetFrac,strFrac,...
    trimStruct,zerosGrid] = deal_struct(structure,...
    'paths','left','bot','cell_resolution','num_col','num_row',...
    'runoff_flowtimes','runoff_watershed_relate','runoff_watershed_key',...
    'fraction_upland','fraction_wetland','fraction_stream',...
    'trim_struct','zeros_grid');
[watershedBaseflow] = deal_struct(state,'watershed_baseflow');
[package,structureGnd] = deal_struct(groundwater,'package','structure');
[recessionConst] = deal_struct(params,'wetland_recession');

% Define timestepLength, currently hard-coded
timestepLength = 86400; % seconds

%--------------------------------------------------------------------------
% Set up MODFLOW-surface model lookup arrays
%--------------------------------------------------------------------------
%Set up the dimension arrays for cell statistics
dimLHM = struct('top',yllcornerLHM + cellsizeLHM * numRowLHM,'left',xllcornerLHM,...
    'cellsizeX',cellsizeLHM,'cellsizeY',cellsizeLHM,'rows',numRowLHM,'cols',numColLHM);
dimMODFLOW = struct('top',structureGnd.yllcorner + structureGnd.cellsize*structureGnd.num_row,'left',structureGnd.xllcorner,...
    'cellsizeX',structureGnd.cellsize,'cellsizeY',structureGnd.cellsize,'rows',structureGnd.num_row,'cols',structureGnd.num_col);
aggFactor = dimLHM.cellsizeX / dimMODFLOW.cellsizeX;

% Test to see if grid cellsizes are integer multiples
gridIntMult = (rem(aggFactor,1)<1e-8);

% Determine if the grid extent of the MODFLOW array is significantly smaller
% than LHM
extentDiff = ((dimMODFLOW.top - dimLHM.top) < -dimMODFLOW.cellsizeY) || ...
    ((dimMODFLOW.left - dimLHM.left) < -dimMODFLOW.cellsizeY);

% Rescale to LHM resolution only if necessary
testRescale = ~gridIntMult || extentDiff;

% If the grids aren't integer multiples in cellsize, get the precalcAgg array
% for upscaling with cell_statistics
if testRescale
    [~,~,precalcAgg] = cell_statistics(dimLHM,dimMODFLOW,zeros(dimMODFLOW.rows,dimMODFLOW.cols),'mean');
end

%--------------------------------------------------------------------------
% Open the CCF file for reading, build lookup information
%--------------------------------------------------------------------------
fid = fopen([paths.output.groundwater,filesep,package.ccf.name],'r');

% Read the header information, building a file position for each drains
% timestep
% TODO: For now, this fails if anything other than drains are written out
% using the COMPACT option
numSteps = sum(structureGnd.nstp);
tableIndex = array2table(zeros(numSteps,6),'VariableName',{'kstp','kper','delt','pertim','totim','loc'});
for m = 1:numSteps
    % Get the timestep and stress period of this output
    tableIndex.kstp(m) = fread(fid,1,'int'); % KSTP
    tableIndex.kper(m)= fread(fid,1,'int'); % KPER

    % Make sure this is a drains type
    text = strtrim(char(fread(fid,16,'char')'));
    assert(strcmpi(text,'drains'),'Outputs other than drains are currently not supported');

    % Read in ncol, nrow, nlay
    ncol = fread(fid,1,'int');
    nrow = fread(fid,1,'int');
    nlay = fread(fid,1,'int');

    if m == 1
        % Assert that dimensions match
        assert(ncol==structureGnd.num_col,'Groundwater model dimensions do not match LHM specs');
        assert(nrow==structureGnd.num_row,'Groundwater model dimensions do not match LHM specs');
    end
        
    if nlay < 1 %read second header row
        % Get the drains header
        imeth = fread(fid,1,'int');
        assert(imeth==5,'Must be reading a compact budget file');
        tableIndex.delt(m) = fread(fid,1,'single');
        tableIndex.pertim(m) = fread(fid,1,'single');
        tableIndex.totim(m) = fread(fid,1,'single');
        naux = fread(fid,1,'int') - 1;
        assert(naux==0,'Auxiliary parameter read not supported');
        tableIndex.nlist(m) = fread(fid,1,'int');

        % Get the data location
        tableIndex.loc(m) = ftell(fid); % location of the beginning of the list output

        % Get the cell numbers into lay, row, and col
        if m == 1 % || tableIndex.nlist(m)~=tableIndex.nlist(m-1), currently doesn't support changing number of drains
            % Read in the cellNums and skip 4 bytes of flow data for each
            % entry
            cellNum = fread(fid,tableIndex.nlist(m),'int',4);

            % Calculate the indices of the drain cells
            lay = ceil(cellNum/(ncol*nrow)); % no drains in lay2 for now
            row = ceil((cellNum-(lay-1)*ncol*nrow)/ncol);
            col = cellNum-(lay-1)*ncol*nrow-(row-1)*ncol;
            indDrn = sub2ind([nrow,ncol],row,col);

            % Get the drain groups for summing
            [drnGroups,indGroups] = findgroups(indDrn);
        else
            % Skip the drains data
            fseek(fid,tableIndex.nlist(m)*(4+4),'cof'); %4 bytes for the cellNum, 4 for the single-precision flow value
        end
    else
        error('Must be reading a compact budget file');
    end
end

%--------------------------------------------------------------------------
% Create the runoff recession constant array
%--------------------------------------------------------------------------
% This is necessary because MODFLOW discharges water to wetlands, which then
% have some storage capacity before that water really becomes surface runoff 
% available for routing.

% First, LHM recession constant is hour^-1, convert to day^-1
% This slightly overestimates the daily recession rate, but not by that
% much (i.e. compounding negative interest)
recessionDaily = unit_conversions(recessionConst,'phr','pday');

% Now, calculate an effective recession constant, streams and upland run
% off same day. This is in the LHM surface grid dimensions
recession = (upFrac + strFrac + recessionDaily * wetFrac);

%--------------------------------------------------------------------------
% Create the output grids for the monthly sums, initialize other arrays
%--------------------------------------------------------------------------
scaleFactor = 1e6;
outClass = 'int32';
classFunc = str2func(outClass);

% Test to see if this was a RAMP simulation, if so, skip the first SS stress
% period
if strcmpi(structureGnd.simulation_type,'RAMP')
    startStep = structureGnd.nstp(1) + 1;
else
    startStep = 1;
end

% Define the output months table
startDatevec = datevec(structureGnd.stress_step_table(startStep,3));
endDatevec = datevec(structureGnd.stress_step_table(end,4));
numYears = endDatevec(1) - startDatevec(1) + 1;
monthlyDates = datenum([reshape(repmat((startDatevec(1):endDatevec(1)),12,1),[],1),...
    repmat((1:12)',numYears,1),ones(numYears*12,1)]); % #ok<NASGU>
% if last year incomplete, handle more carefully
if ~all(endDatevec(2:3)==[12,31])
    monthlyDates(end-11:end) = []; % erase the last year
    numLastMonths = endDatevec(2);
    monthlyDates(end+1:end+numLastMonths) = datenum([repmat(endDatevec(1),numLastMonths,1),...
        (1:numLastMonths)',ones(numLastMonths,1)]);
end

% Initialize these for the output monthly grid sums
prevMonth = 1;
monthInd = 0;

% Initialize the arrays needed below
[dischargeGrid] = deal(zeros(nrow,ncol)); %at MODFLOW resolution
[storage,baseflowSum] = deal(zerosGrid); %at surface resolution
baseflowMonthly = zeros(numYears*12,length(zerosGrid),outClass); %at surface resolution

%--------------------------------------------------------------------------
% For each stress period, read in the appropriate grids, sum, write out evap
%--------------------------------------------------------------------------
h = waitbar(0,'Reading and routing groundwater discharge...');
for m = startStep:numSteps % this assumes that model is daily timesteps
    waitbar(m/(numSteps),h);

    %----------------------------------------------------------------------
    % Read in the groundwater discharge data, update storage, calculate runoff
    %----------------------------------------------------------------------
    % Read in the groundwater discharge for this timestep, units are m^3/d
    fseek(fid,tableIndex.loc(m),'bof');
    fseek(fid,4,'cof'); %skip the cellNum
    discharge = -fread(fid,tableIndex.nlist(m),'float32',4); %skip 4 bytes for each entry

    % Find flow values < 0, shouldn't happen, but numerical error perhaps
    discharge(discharge<0) = 0;

    % Sum across drains in the cell
    dischargeGroup = splitapply(@sum,discharge,drnGroups);

    % Convert to a grid at MODFLOW resolution
    dischargeGrid(indGroups) = dischargeGroup; 

    %----------------------------------------------------------------------
    % Rescale discharge to surface resolution, convert to model domain
    %----------------------------------------------------------------------
    % Upscale daily baseflow to surface model resolution, output is m^3/day
    if testRescale
        if gridIntMult && ~extentDiff
            % calculations, assumes grid dimensions are integer multiples
            discargeUpscale = aggregate(dischargeGrid,aggFactor,'sum',dimLHM,dimMODFLOW);
        else
            discargeUpscale = cell_statistics(dimLHM,dimMODFLOW,dischargeGrid,'sum','weight',precalcAgg);
        end
    else
        discargeUpscale = dischargeGrid;
    end
    
    % Trim baseflowUpscale to the model domain
    [dischargeTrim] = trim_domain(trimStruct,discargeUpscale);

    %----------------------------------------------------------------------
    % Add upscaled discharge to storage, calculate baseflow
    %----------------------------------------------------------------------
    % Add all discharge to storage temporarily
    % units are m^3
    storage = storage + dischargeTrim * timestepLength/86400;

    % Calculate runoff from baseflow for this day, m^3/day
    baseflow = storage .* recession;

    % Remove baseflow from storage
    storage = max(storage - baseflow,0);

    % Update summed baseflow, at the surface model resolution
    baseflowSum = baseflowSum + baseflow;

    % Get the current model day
    thisDay = m - startStep + 1;

    % Run the runoff model accumulation function for this day
    % Note: route flow from surface internally-drained areas as well
    % because we set drain elevations equal to the filled DEM elevations
    % for these locations, output is m^3/s
    [watershedBaseflow] = runoff_model(watershedBaseflow,baseflow,thisDay,...
        baseflowFlowtimes,watershedsRelate,watershedKey,timestepLength,1); %pass 1 for cellSize, because baseflowExt in m^3/day already

    % Update the monthly grids
    % Check to see if this is the same month as the previous one
    thisDate = datevec(structureGnd.stress_step_table(m,3));
    if thisDate(2) ~= prevMonth
        % Increment the month index
        monthInd = monthInd + 1;
        prevMonth = thisDate(2);

        % Want output in m^3/month/m^2 cell area, not m^3/month
        baseflowOut = baseflowSum / structure.cell_resolution^2;% 

        % Add current grid to monthly sum grids
        baseflowMonthly(monthInd,:) = baseflowMonthly(monthInd,:) + classFunc(baseflowOut' * scaleFactor);

        % Reset the baseflowSum grid
        baseflowSum = zerosGrid;
    end

    % Update the state array
    state = update_struct(state,'watershed_baseflow',watershedBaseflow);

end
if ishandle(h); close(h); end
fclose(fid);