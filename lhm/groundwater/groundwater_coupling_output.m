function buffers = groundwater_coupling_output(state,structure,buffers,varargin)
% GROUNDWATER_COUPLING_OUTPUT  Accumulates and writes out grids daily for the groundwater model
%   groundwater_coupling_output(varargin)
% 
%   Descriptions of Input Variables:
%   Input arguments are 'save', or none.  If 'save' is used, the current buffers
%   will be saved out and reset, regardless of whether the end of the buffer
%   has been reached.  If there is no input, then the current buffers will
%   be loaded from the data_controller and have the state variables added to it.
% 
%   Descriptions of Output Variables:
%   nont, all inputs come from data controller
% 
%   Example(s):
% 
% 
%   See also:

% Author: Anthony Kendall, Tianfang Xu
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-04-02
% Modified: 2017-06-30
% Copyright 2008 Michigan State University.

% Determine if this is an offline or online coupling
[onlineCoupling,groundwaterEnabled] = deal_struct(structure,'groundwater_coupling_online','groundwater_enabled');

if nargin==3

    if onlineCoupling > 0
            [deepPercUpland,deepPercWetland,wetlandFrac,...
                evapPostWetland,transpiration,uplandFrac,streamFrac,...
                evapPostUpland, transpUpland,...
                dischargeUpland,rechargeWTUpland,rechargeWTWetland] = deal_struct(state,...
                'deep_percolation_upland','deep_percolation_wetland','fraction_wetland',...
                'evap_post_demand','transp_wetland_phreatic','fraction_upland','fraction_stream',...
                'evap_upland_post_demand','transp_upland_phreatic',...
                'groundwater_discharge_upland','recharge_WT_change_upland','recharge_WT_change_wetland');
            [paths,numInnerSteps,indExternal] = deal_struct(structure,...
                'paths','num_inner_timesteps','index_externally_drained');
            [outBuff] = deal_struct(buffers,'groundwater');

            % Get internally drained cells
            indInternal = ~indExternal;

            % Calculate the wetland fraction to include stream fraction, currently
            wetlandFrac = wetlandFrac + streamFrac;

            % separate out positive and negative components of
            % recharge_WT_change_wetland, both are lumped in because
            % groundwater_discharge_wetland means drain flux
            dischargeWetland = zeros(size(dischargeUpland));
            ind_negative_wetland = rechargeWTWetland < 0;
            dischargeWetland(ind_negative_wetland) = - rechargeWTWetland(ind_negative_wetland);
            rechargeWTWetland(ind_negative_wetland) = 0;

            % similarly, separate positive and negative components of
            % recharge_WT_change_upland
            ind_negative_upland = rechargeWTUpland < 0;
            dischargeUpland(ind_negative_upland) = dischargeUpland(ind_negative_upland) ...
                -rechargeWTUpland(ind_negative_upland);
            rechargeWTUpland(ind_negative_upland) = 0;

            % Calculate total deep percolation
            deepPerc = (deepPercUpland + rechargeWTUpland) .* uplandFrac + ...
                (deepPercWetland + rechargeWTWetland) .* wetlandFrac;

            % Store this day's percolation from upland and wetland
            % separately
            deepPerc_up = deepPercUpland + rechargeWTUpland;
            deepPerc_wet = deepPercWetland + rechargeWTWetland;
            if state.inner_timestep == 1  % startover at the beginning of every day
                outBuff.perc_up = zeros(size(deepPerc,1),1);
                outBuff.perc_wet = zeros(size(deepPerc,1),1);
            end
            outBuff.perc_up = outBuff.perc_up + deepPerc_up;
            outBuff.perc_wet = outBuff.perc_wet + deepPerc_wet;

            % Rescale upland & wetland transpiration
            % 05/26/2017 T.XU. add upland ET from groundwater
            evaporation = evapPostWetland.* wetlandFrac + evapPostUpland .* uplandFrac;
            transpiration = transpiration .* wetlandFrac + transpUpland .* uplandFrac;

            % Only use the internally-drained portion of evaporation
            evaporation = evaporation .* indInternal;

            % Sum the two ET terms
            et = evaporation + transpiration;

            % Calculate discharge (only comprised of upland discharge)
            % 01/18/2017 T.XU. add upland & wetland discharge due to WT
            % decline (leaving water behind)
            discharge = dischargeUpland .* uplandFrac + dischargeWetland .* wetlandFrac;

            % Add upland discharge due to MODFLOW flooding to buffers
            if state.inner_timestep == 1  % startover at the beginning of every day
                outBuff.discharge = zeros(size(discharge,1),1);
            end
            outBuff.discharge = outBuff.discharge + discharge;

            % Update each of the stored variables, this will flush them if end of buffer is reached
            outBuff.transpiration = update_buffer(outBuff.transpiration,et,...
                paths.output.transp,'transpiration',numInnerSteps);
            outBuff.deep_percolation = update_buffer(outBuff.deep_percolation,deepPerc,...
                paths.output.perc,'deep_percolation',numInnerSteps);

            % Update the buffers controller
            buffers = update_struct(buffers,'groundwater',outBuff);
        elseif groundwaterEnabled
            [deepPercUpland,deepPercWetland,wetlandFrac,...
                transpiration,evapPostWetland,uplandFrac,streamFrac,...
                rechargeWTUpland,rechargeWTWetland] = deal_struct(state,...
                'deep_percolation_upland','deep_percolation_wetland','fraction_wetland',...
                'transp_wetland_phreatic','evap_post_demand','fraction_upland','fraction_stream',...
                'recharge_WT_change_upland','recharge_WT_change_wetland');
            [paths,numInnerSteps,indExternal] = deal_struct(structure,...
                'paths','num_inner_timesteps','index_externally_drained');
            [outBuff] = deal_struct(buffers,'groundwater');

            % Get internally drained cells
            indInternal = ~indExternal;

            % Calculate the wetland fraction to include stream fraction, currently
            wetlandFrac = wetlandFrac + streamFrac;

            % Calculate total deep percolation
            deepPerc = (deepPercUpland + rechargeWTUpland) .* uplandFrac + ...
                (deepPercWetland + rechargeWTWetland) .* wetlandFrac;

            % Rescale wetland transpiration and evaporation
            evaporation = evapPostWetland.* wetlandFrac;
            transpiration = transpiration .* wetlandFrac;

            % Only use the internally-drained portion of post-demand evaporation, the rest will
            % routed through the stream network
            evaporation = evaporation .* indInternal;

            % Sum the two ET terms
            et = evaporation + transpiration;

            % Update each of the stored variables, this will flush them if end of buffer is reached
            outBuff.transpiration = update_buffer(outBuff.transpiration,et,...
                paths.output.transp,'transpiration',numInnerSteps);
            outBuff.deep_percolation = update_buffer(outBuff.deep_percolation,deepPerc,...
                paths.output.perc,'deep_percolation',numInnerSteps);

            % Update the buffers controller
            buffers = update_struct(buffers,'groundwater',outBuff);
    end

    elseif ischar(varargin{1}) 
        mode = varargin{1};
        switch lower(mode)
            case 'save'
                if groundwaterEnabled
                    [paths] = deal_struct(structure,'paths');
                    [outBuff] = deal_struct(buffers,'groundwater');

                    % Write out each of the buffers to their separate files
                    outBuff.transpiration = write_buffer(outBuff.transpiration,...
                        paths.output.transp,'transpiration');
                    outBuff.deep_percolation = write_buffer(outBuff.deep_percolation,...
                        paths.output.perc,'deep_percolation');

                    % Save the buffers to the data controller
                    buffers = update_struct(buffers,'groundwater',outBuff);
                end
            otherwise
                error_LHM('Invalid input argument(s)');
        end
    else
        error_LHM('Invalid input argument(s)');
end
end

% --------------------------------------------------------------------------
% Internal Functions
% --------------------------------------------------------------------------
function outBuff = write_buffer(outBuff,outFile,outName)
if ~(outBuff.thisDay==0) % otherwise, the buffer is already empty
    % Write the dataset
    h5datawrite(outFile,['/',outName,'/data'],outBuff.data(:,1:outBuff.thisDay)',outBuff.attributes,true,...
        [outBuff.lastRow,0],[outBuff.thisDay,size(outBuff.data,1)],[1,1]);

    % Reset the counters, and update the lastRow counter
    outBuff.lastRow = outBuff.lastRow + outBuff.thisDay;
    outBuff.thisDay = 0;
    outBuff.thisHour = 0;

    % Empty the buffer
    outBuff.data = zeros(size(outBuff.data));
end
end


function outBuff = update_buffer(outBuff,newData,outFile,outName,numInnerSteps)
% Increment the thisDay counter if thisHour==0
if (outBuff.thisHour == 0)
    outBuff.thisDay = outBuff.thisDay + 1;
end
% Increment the thisHour counter
outBuff.thisHour = outBuff.thisHour + 1;

% Then, add the data
outBuff.data(:,outBuff.thisDay) = outBuff.data(:,outBuff.thisDay) + newData;

% check to see if this hour reached numInnerSteps, if so, increment the thisDay and roll over lastHour
if outBuff.thisHour == numInnerSteps
    % Check to see if its time to flush the buffer, and do so if necessary
    if outBuff.thisDay == outBuff.maxDays
        outBuff = write_buffer(outBuff,outFile,outName);
    else
        outBuff.thisHour = 0;
    end
end
end
