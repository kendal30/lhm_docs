function [buffers] = groundwater_controller(state,structure,groundwater,buffers,params)
% for offline coupling do nothing except at the end of simulation
% for online coupling call MODFLOW per stress period (currently assuming
% daily)
% Rewritten, 07/17/2017, TXU

[onlineCoupling] = deal_struct(structure,'groundwater_coupling_online');

if onlineCoupling == 0 && isempty(buffers)
    disp('Running the unsaturated zone module');
    %First, run the unsaturated groundwater code
    unsaturated_zone_module(structure,groundwater);
    %Then, write the binary post-surface evaporation demand
else
    buffer_long = buffers.groundwater.long;
    % Update the buffer_long array
    buffer_long.perc_up(:,1:end-1) = buffer_long.perc_up(:,2:end);
    buffer_long.perc_up(:,end) = buffers.groundwater.perc_up;

    buffer_long.perc_wet(:,1:end-1) = buffer_long.perc_wet(:,2:end);
    buffer_long.perc_wet(:,end) = buffers.groundwater.perc_wet;

    buffer_long.depth_front_up(:,1:end-1) = buffer_long.depth_front_up(:,2:end);
    buffer_long.depth_front_up(:,end) = zeros(size(buffer_long.depth_front_up,1),1);

    buffer_long.depth_front_wet(:,1:end-1) = buffer_long.depth_front_wet(:,2:end);
    buffer_long.depth_front_wet(:,end) = zeros(size(buffer_long.depth_front_wet,1),1);

    % Run the unsaturated groundwater code
    [buffer_long] = unsaturated_zone_module_online(state,structure,groundwater, buffers.groundwater, buffer_long);
    buffers.groundwater.long = buffer_long;
end

if (onlineCoupling == 0 && isempty(buffers)) || onlineCoupling == 1
%Run the groundwater model
currDir = pwd;
cd(structure.paths.output.groundwater);
% 01/18/2017 mute MODFLOW output
system([groundwater.structure.modflow_exec ' ' groundwater.package.nam.name,' >nul']);
cd(currDir);
end

if onlineCoupling == 0 && isempty(buffers)
%Post-process the result and remove evaporation
disp('Running the post-processing evaporation routine, and calculating simulated vs. observed')
groundwater_flow_post_evap(structure,params,groundwater)

%Read in simulated heads and save to OUTS file
obs_heads_read(structure,groundwater)

%Calculate total streamflow and save to OUTS file
streamflow_composite(state,structure)
end
end