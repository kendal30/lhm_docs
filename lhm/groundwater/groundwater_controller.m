function groundwater_controller(state,structure,params,groundwater)

if structure.groundwater_enabled
    %First, run the unsaturated groundwater code
    disp('Running the unsaturated zone module, calculating recharge')
    disp('routing and basin-aggregating shallow subsurface flows (throughflow)')
    state = unsaturated_zone_module(state,structure,groundwater);
    
    %Then, write the binary post-surface transpiration demand
    evt_binary_write(structure,groundwater);

    %Run the groundwater model
    currDir = pwd;
    cd(structure.paths.output.groundwater);
    system([groundwater.structure.modflow_exec ' ' groundwater.package.nam.name]);
    cd(currDir);

    %Read in simulated heads and save to OUTS file
    obs_heads_read(structure,groundwater);

    disp('Routing and basin-aggregating groundwater discharge to streams (baseflow)')
    state = groundwater_flow_routing(state,structure,params,groundwater);

    %Composite streamflows, and remove evaporative demand
    disp('Composite streamflow components, and remove post-surface storage evaporative demand')
    streamflow_composite(state,structure)
    
    %Post-process the result and remove evaporation
    %disp('Running the post-processing evaporation routine, and calculating simulated vs. observed')
    %groundwater_flow_post_evap(structure,params,groundwater)
    

end

end
