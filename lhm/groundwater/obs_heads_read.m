function varargout = obs_heads_read(structure,groundwater)
% This function reads MODFLOW OBS process heads and stores them in the OUTS
% file
[paths] = deal_struct(structure,'paths');
[structureGnd,package] = deal_struct(groundwater,'structure','package');

%--------------------------------------------------------------------------
% Import the heads obs output file and the LHM observations
%--------------------------------------------------------------------------
% Read in the observation table, columns in the obsContents array are sim,
% obs, loc_id
obsOpts = delimitedTextImportOptions('VariableNames',{'sim','obs','loc_id'},'VariableTypes',{'double','double','char'},...
    'DataLines',2,'Delimiter','  ','LeadingDelimitersRule','ignore');
obsContents = readtable([paths.output.groundwater,filesep,structureGnd.model_name,package.hob_out.extension],obsOpts);

% Load the LHM observations
obsData = struct();
obsData.head = h5obstableread(paths.input.obs,'point/head');

%--------------------------------------------------------------------------
% Process simulated vs. observed
%--------------------------------------------------------------------------
% Rename 'value' field to 'obs'
obsData.head.obs = obsData.head.value;
obsData.head.value = []; 

% Drop obs from the obsContents
obsContents.obs = [];

% Merge with the obsContents
obsData.head = outerjoin(obsData.head,obsContents,'Keys','loc_id','Type','left','MergeKeys',true);

%--------------------------------------------------------------------------
% Save to the OUTS file
%--------------------------------------------------------------------------
if nargout == 0
    if exist(paths.output.simobs,'file')
        save(paths.output.simobs,'-struct','obsData','-append');
    else
        save(paths.output.simobs,'-struct','obsData');
    end
else
    varargout = {obsData};
end

end
