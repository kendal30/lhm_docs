function [buffer_long] = unsaturated_zone_module_online(state,structure,groundwater,buffersGW,buffer_long)

%load data from the ILHM data controller
[paths,xllcornerILHM,yllcornerILHM,cellsizeILHM,numColILHM,numRowILHM] = ...
    deal_struct(structure,...
    'paths','left','bot','cell_resolution','num_col','num_row','trim_struct','thickness_soil');

%[date, timestep] = deal_struct(state,'date','outer_timestep');
[discharge] = deal_struct(buffersGW,'discharge');

[package,structure,params] = deal_struct(groundwater,...
    'package','structure','params');
%--------------------------------------------------------------------------
%Load zone_hk and zone_vani grids
%--------------------------------------------------------------------------
gridLoad = load_grid_params(paths.input.static,'groundwater','STAT');
grids.zone_hk = gridLoad.groundwater.hk.data;
grids.zone_vani = gridLoad.groundwater.vani.data;
clear gridLoad

% for debugging
%load lookup

%Delay table, this was derived empirically from a 1-D richard's equation
%model, first row is vkSat, second is the delay parameter
%Eventually move to a parameter file (original delay was 2.47)
delayTable = [20,1.62;
              10,1.81;
              5,2.47;
              5/2,3.13;
              5/3,4.18;
              0.01 10];
% delayParam = 2.47;

%force a maximum stress period delay
maxDelay = size(buffer_long.perc_up,2);

%Force a maximum recharge rate in m/day
maxRecharge = 0.05;

%--------------------------------------------------------------------------
%set up calculated model-specific arrays and values
%--------------------------------------------------------------------------
spStart = structure.start_date + state.outer_timestep - 1 + cumsum(structure.perlen) - structure.perlen;
spStart(end+1) = spStart(end) + structure.perlen(end);
if strcmpi(structure.simulation_type,'RAMP') %The first steady-state stress period is a virtual one
    spStart = spStart - structure.perlen(1);
end
xArray = structure.xllcorner + (0:structure.cellsize:structure.cellsize*(structure.num_col-1)) + structure.cellsize/2;
yArray = fliplr(structure.yllcorner + (0:structure.cellsize:structure.cellsize*(structure.num_row-1)))' + structure.cellsize/2;
xArrayILHM = xllcornerILHM + (0:cellsizeILHM:cellsizeILHM*(numColILHM-1)) + cellsizeILHM / 2;
yArrayILHM = fliplr(yllcornerILHM + (0:cellsizeILHM:cellsizeILHM*(numRowILHM-1)))' + cellsizeILHM / 2;

%Create a lookup table for the coarse-fine mapping, of form fineData =
%coarseData(lookup)
coarseIndeces = reshape((1:numColILHM*numRowILHM),numRowILHM,numColILHM);
gndwaterLookup = uint32(interp2(xArrayILHM,yArrayILHM,coarseIndeces,xArray,yArray,'nearest'));
gndwaterMask = (gndwaterLookup == 0);
gndwaterLookup(gndwaterMask) = 1;

%--------------------------------------------------------------------------
%Create vk array, in groundwater resolution
%--------------------------------------------------------------------------
if isfield(params,'hk') && isfield(params,'vani')
    [hk,vani] = deal(zeros(size(grids.zone_hk(:,:,1))));
    %Build the hk and vani grids
    for m = 1:length(params.hk.code)
        thisInd = (grids.zone_hk(:,:,1)==params.hk.code(1));
        hk(thisInd) = params.hk.val(1);
    end
    for m = 1:length(params.vani.code)
        thisInd = (grids.zone_vani(:,:,1)==params.vani.code(1));
        vani(thisInd) = params.vani.val(1);
    end
else %HK and VANI grids are given directly
    hk = grids.zone_hk(:,:,1);
    vani = grids.zone_vani(:,:,1);
end
%calculate vk
vk = hk ./ vani;
delayParam = zeros(size(grids.zone_hk(:,:,1)));

%Now, convert to delayTable
delayParam(:) = interp1(delayTable(:,1),delayTable(:,2),vk(:),'linear','extrap');

%Convert to ILHM resolution
% delayParam = interp2(xArray,yArray,delayParam,xArrayILHM,yArrayILHM);

% Convert to compressed column
delayParam(lookup == 0) = nan;
delayParam = delayParam(:);
delayParam(isnan(delayParam)) = [];

clear grids
%--------------------------------------------------------------------------
%read in the delay grid and create the delay masks stack
%--------------------------------------------------------------------------
% Calculate unsaturation zone thickness
[uplandFrac,wetlandFrac,wtDepthUpland,wtDepthWetland] = deal_struct(state,'fraction_upland','fraction_wetland','water_table_depth_upland','water_table_depth_wetland');

%Account for thickness of root zone soil
% unsatThick = unsatThick - sum(rootThick,2);
% unsatThick(unsatThick < 0) = 0;

% Propagate the percolation front during this time period
spLength = structure.perlen(1); %this is appropriate only for fixed-length stress periods
buffer_long.depth_front_up = buffer_long.depth_front_up + spLength ./ delayParam ...
    * ones(1,maxDelay);
buffer_long.depth_front_wet = buffer_long.depth_front_wet + spLength ./ delayParam ...
    * ones(1,maxDelay);

% Check if infiltration front hits groundwater table
ind_recharge_up = buffer_long.depth_front_up > (wtDepthUpland * ones(1,maxDelay))...
    & ( wtDepthUpland * ones(1, maxDelay)) > 0;
ind_recharge_wet = buffer_long.depth_front_wet > (wtDepthWetland * ones(1,maxDelay))...
    & ( wtDepthWetland * ones(1, maxDelay)) > 0;

% If infiltration front hits water table, generates recharge
spPerc = zeros(size(buffer_long.depth_front_up,1),1);
for i_delay = 1:maxDelay
    spPerc(ind_recharge_up(:,i_delay)) = spPerc(ind_recharge_up(:,i_delay)) + ...
        buffer_long.perc_up(ind_recharge_up(:,i_delay)) .* uplandFrac(ind_recharge_up(:,i_delay));
    spPerc(ind_recharge_wet(:,i_delay)) = spPerc(ind_recharge_wet(:,i_delay)) + ...
        buffer_long.perc_up(ind_recharge_wet(:,i_delay)) .* wetlandFrac(ind_recharge_wet(:,i_delay));
end

% Wipe recharge-generating percolation to zero
buffer_long.perc_up(ind_recharge_up) = 0;
buffer_long.depth_front_up(ind_recharge_up) = 0;

buffer_long.perc_wet(ind_recharge_wet) = 0;
buffer_long.depth_front_wet(ind_recharge_wet) = 0;

% Take out discharge as negative recharge
spPerc = spPerc - discharge;

%Add in held recharge from the previous timestep
spPerc = spPerc + buffer_long.holdRecharge;

% Take out EVT as negative recharge
transpiration = buffersGW.transpiration.data(:,buffersGW.transpiration.thisDay+1);
evaporation = buffersGW.evaporation.data(:,buffersGW.evaporation.thisDay+1);
spPerc = spPerc - transpiration - evaporation;

% Convert recharge total amount in this stress period to recharge rates,
% although not necessary since it's daily stress period
spPerc = spPerc / structure.perlen(1);

%Calculate recharge above the maximum rate and hold it for the next
%stress period
buffer_long.holdRecharge = max(0,spPerc-maxRecharge);
spPerc = spPerc - buffer_long.holdRecharge;

%Convert to grid dimensions
lookup(lookup==0) = 1;
spPerc = spPerc(lookup);
spPerc(isnan(spPerc)) = 0;

%interpolate the percolation grid to MODFLOW grid
% spRechargeInterp = extend_domain(trimStruct,spPerc); %extend unsaturated thickness to ILHM model from trimmed

% Generate recharge matrix
spRechargeInterp = spPerc(gndwaterLookup);
%Open the binary recharge file
write_recharge([paths.output.groundwater,filesep,package.rch_bin.name],spRechargeInterp,1)

% fid2 = fopen([paths.output.groundwater,filesep,package.rch_bin.name],'w');
%Write out data to binary recharge file
% fwrite(fid2,1,'int'); %KSTP
% fwrite(fid2,1,'int'); %KPER
% fwrite(fid2,structure.perlen(1),'float32'); %PERTIM
% fwrite(fid2,spStart(1)-spStart(1)+structure.perlen(1),'float32'); %TOTIM
% fwrite(fid2,'RechargeBinary00','char'); %TEXT
% fwrite(fid2,structure.num_col,'int'); %NCOL
% fwrite(fid2,structure.num_row,'int'); %NROW
% fwrite(fid2,1,'int'); %NLAY
% fwrite(fid2,spRechargeInterp','float32'); %the transpose is because MATLAB writes data columnwise
% fclose(fid2);
