function evt_binary_write(structure,groundwater)
%load data from the ILHM data controller
[paths,xllcornerILHM,yllcornerILHM,cellsizeILHM,numColILHM,numRowILHM,...
    trimStruct,zerosGrid] = ...
    deal_struct(structure,...
    'paths','left','bot','cell_resolution','num_col','num_row',...
    'trim_struct','zeros_grid');
[package,structure] = deal_struct(groundwater,'package','structure');


%--------------------------------------------------------------------------
%set up calculated model-specific arrays and values
%--------------------------------------------------------------------------
spStart = structure.start_date + cumsum(structure.perlen) - structure.perlen;
spStart(end+1) = spStart(end) + structure.perlen(end);
if strcmpi(structure.simulation_type,'RAMP') %The first steady-state stress period is a virtual one
    spStart = spStart - structure.perlen(1);
end
xArray = structure.xllcorner + (0:structure.cellsize:structure.cellsize*(structure.num_col-1)) + structure.cellsize/2;
yArray = fliplr(structure.yllcorner + (0:structure.cellsize:structure.cellsize*(structure.num_row-1)))' + structure.cellsize/2;
xArrayILHM = xllcornerILHM + (0:cellsizeILHM:cellsizeILHM*(numColILHM-1)) + cellsizeILHM / 2;
yArrayILHM = fliplr(yllcornerILHM + (0:cellsizeILHM:cellsizeILHM*(numRowILHM-1)))' + cellsizeILHM / 2;

%Create a lookup table for the coarse-fine mapping, of form fineData =
%coarseData(lookup)
coarseIndeces = reshape((1:numColILHM*numRowILHM),numRowILHM,numColILHM);
gndwaterLookup = uint32(interp2(xArrayILHM,yArrayILHM,coarseIndeces,xArray,yArray,'nearest'));
gndwaterMask = (gndwaterLookup == 0);
gndwaterLookup(gndwaterMask) = 1;

%--------------------------------------------------------------------------
%Read in the lookup table and index from the transpiration demand file
%--------------------------------------------------------------------------
transpIndex = h5dataread(paths.output.transp,'/transpiration/index');
transpLookup = h5dataread(paths.output.transp,'/transpiration/lookup');

%Just in case there are 0s in the lookup table, convert to 1
transpLookup(transpLookup==0) = 1;

%Determine the size of the evaporation and transpiration arrays
transpSize = h5varget(paths.output.transp,'/transpiration/data',false);

%--------------------------------------------------------------------------
% Prepare the SS EVT demand, if needed
%--------------------------------------------------------------------------
%Open the binary recharge file
fid = fopen([paths.output.groundwater,filesep,package.evt_bin.name],'w');

% Determine which stress period to start with for accumulating recharge
if any(strcmpi(structure.simulation_type,{'SS','TR'}))
    startLoop = 1;
elseif strcmpi(structure.simulation_type,'RAMP')
    startLoop = 2;
end

if any(strcmpi(structure.simulation_type,{'SS','RAMP'}))
    %Initialize the stress period recharge array
    spET = extend_domain(trimStruct,zerosGrid);
    
    h = waitbar(0,sprintf('Writing MODFLOW %s ET',structureGnd.simulation_type));
    for m = startLoop:structure.nper %There are only nper-1 real stress periods to sum
        % Determine the length of the inner loop, to prevent reading in
        % the entire PERC file at once, read in 7 days at once
        numInner = ceil((structure.perlen(m) - startLoop + 1)/7);
        
        for n = 1:numInner
            waitbar((m-1+n/numInner)/structure.nper,h);

            % Set the inner start and end
            if numInner == 1
                innerStart = spStart(m);
                innerEnd = spStart(m+1) - 1e-5;
            else
                innerStart = spStart(m) + (n-1)*7;
                innerEnd = spStart(m) + n*7 - 1e-5;
            end
            
            %Read in the data
            %Determine the start and end rows to read in from the HDF file for
            %this stress period
            rowIndTransp = bracket_index(transpIndex(:,1),innerStart,innerEnd);
            
            %Read in this stress period's post-evaporation array
            innerTransp = h5dataread(paths.output.transp,'/transpiration/data',true,...
                [rowIndTransp(1)-1,0],[diff(rowIndTransp)+1,transpSize(2)],[1,1]);
            
            %In case there was no percolation or transpiration written out, though
            %this shouldn't occur
            if islogical(innerTransp),innerTransp = zerosGrid';end
            
            %Sum across rows
            innerTransp = sum(innerTransp,1);
            
            %Convert to grid dimensions
            innerTransp = innerTransp(transpLookup);
            
            %Limit evaporation to 0 (condensation not allowed to groundwater!)
            innerTransp(innerTransp<0) = 0; %this shouldn't happen
            
            %Sum transp grids for total ET
            spET = spET + innerTransp; 
        end
    end
    if ishandle(h); close(h); end

    %Remove any NaNs
    spET(isnan(spET)) = 0;

    % Convert total over stress period to m/d
    spET = spET  / sum(structure.perlen(startLoop:end));  % to convert to m/d

    %Interpolate to groundwater model resolution
    spETInterp = spET(gndwaterLookup);

    % Write out data to binary recharge file
    write_binary_array(fid,1,1,structure.perlen(1),structure.perlen(1),...
        'PostEvapBinary00',structure.num_col,structure.num_row,1,spETInterp)
end

%--------------------------------------------------------------------------
% Process transient ET, if any
%--------------------------------------------------------------------------
if any(strcmpi(structure.simulation_type,{'RAMP','TR'}))
    h=waitbar(0,'Writing MODFLOW transient ET');
    for m=startLoop:structure.nper
        waitbar(m/structure.nper,h);
        
        %Read in the data
        %Determine the start and end rows to read in from the HDF files for
        %this stress period
        rowIndTransp = bracket_index(transpIndex(:,1),spStart(m),spStart(m+1)-1e-5);
        
        %Read in this stress period's post-evaporation and transpiration
        spTransp = h5dataread(paths.output.transp,'/transpiration/data',true,...
            [rowIndTransp(1)-1,0],[diff(rowIndTransp)+1,transpSize(2)],[1,1]);
        
        %In case there was no percolation or transpiration written out, though
        %this shouldn't occur
        if islogical(spTransp),spTransp = zerosGrid';end
        
        %Sum across rows
        spTransp = sum(spTransp,1);
        
        %Limit evaporation to 0 (condensation not allowed to groundwater!)
        spTransp(spTransp<0) = 0; %this shouldn't happen
        
        %Convert to grid dimensions
        spTransp = spTransp(transpLookup);
        
        %Sum the grids and convert to model units)
        spET = spTransp / structure.perlen(m);  %to convert to m/d;
        
        %Remove any NaNs
        spET(isnan(spET)) = 0;
        
        %Interpolate to groundwater model resolution
        spETInterp = spET(gndwaterLookup);
        
        % Write out data to binary recharge file
        write_binary_array(fid,1,m,structure.perlen(m),spStart(m)-spStart(1)+structure.perlen(m),...
            'PostEvapBinary00',structure.num_col,structure.num_row,1,spETInterp)
    end
    if ishandle(h); close(h); end
end
fclose(fid);

end


function write_binary_array(fid,kstp,kper,pertim,totim,text,ncol,nrow,nlay,recharge)
fwrite(fid,kstp,'int'); % KSTP
fwrite(fid,kper,'int'); % KPER
fwrite(fid,pertim,'float32'); % PERTIM
fwrite(fid,totim,'float32'); % TOTIM
fwrite(fid,text,'char'); % TEXT
fwrite(fid,ncol,'int'); % NCOL
fwrite(fid,nrow,'int'); % NROW
fwrite(fid,nlay,'int'); % NLAY
fwrite(fid,recharge','float32'); % the transpose is because MATLAB writes data columnwise
end