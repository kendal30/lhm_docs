function state = update_water_table(state,structure,params) %#ok<INUSD>
%UPDATE_WATER_TABLE  Updates the water table during model hourly timesteps,
%calculates groundwater discharge and WT change recharge
%   state = update_water_table(state,structure,params)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> update_water_table
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] msu [dot] edu
% Created: 2015-03-04
% Modified: 
% Copyright 2015 Michigan State University.


[wtChangeUpland,wtChangeWetland,prevWTDepthUpland,prevWTDepthWetland,...
    waterWetland] = deal_struct(state,...
    'water_table_change_upland','water_table_change_wetland','water_table_depth_upland','water_table_depth_wetland',...
    'water_wetland');
[soilThetaAvail] = deal_struct(params,'soil_theta_avail');
[indShallowWetland,indConnected,timestep,zerosGrid] = deal_struct(structure,...
    'index_shallow_wetland','index_connected_wetlands','timestep_length','zeros_grid');

%Initialize arrays
[dischargeUpland] = deal(zerosGrid);

%Specify a maximum depth change for calculating recharge, handles first
%timestep issues
maxDepthChange = 0.01;

%Approximate soil theta available for all layers as that of the first layer
%only
soilThetaAvail = soilThetaAvail(:,1);

%Update water table--------------------------------------------------------
wtDepthUpland = timestep*wtChangeUpland + prevWTDepthUpland;
wtDepthWetland = timestep*wtChangeWetland + prevWTDepthWetland;

%Calculate groundwater discharge in upland cells---------------------------
%(wetlands already calculated on model coupling)
%For now, upland water table depth is 0, this will calculate as 0
indFloodUp = wtDepthUpland < 0;
dischargeUpland(indFloodUp) = min(0,-wtDepthUpland(indFloodUp)); %will be removed from groundwater model on next iteration
%TODO: dischargeUpland should be multiplied by specific yield because MODFLOW doesn't know about the land surface
wtDepthUpland(indFloodUp) = 0;

%Calculate groundwater recharge due to water table change------------------
%Initialize some arrays
[wetlandTheta,rechargeWTWetland,rechargeWTUpland] = deal(zerosGrid);

%Deep wetlands, assume water table is at the surface, only for connected
%wetlands
indWTRecharge = (indConnected & indShallowWetland);

%Calculate water table rise, depth reduces
wtDepthChangeWetland = wtDepthWetland - prevWTDepthWetland;
wtDepthChangeUpland = wtDepthUpland - prevWTDepthUpland; %#ok<NASGU>
indWTRise = (wtDepthChangeWetland < 0) & (prevWTDepthWetland > 0); %only for subsurface cells

%Calculate wetland theta using prior wt depth (volumetric water content)
ind = indWTRise & indWTRecharge;
wetlandTheta(ind) = min(waterWetland(ind) ./ prevWTDepthWetland(ind),soilThetaAvail(ind)); %Cannot be more than maximum theta of soil

%Now, calculate recharge from WT rise
rechargeWTWetland(ind) = min(waterWetland(ind),wetlandTheta(ind) .* min(-wtDepthChangeWetland(ind),maxDepthChange));

%Update the state structure
state = update_struct(state,'water_table_depth_upland',wtDepthUpland,'water_table_depth_wetland',wtDepthWetland,...
    'groundwater_discharge_upland',dischargeUpland,...
    'recharge_WT_change_wetland',rechargeWTWetland,'recharge_WT_change_upland',rechargeWTUpland);
end