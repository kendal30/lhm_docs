function q = read_modflow_drain_flux(fname,t_idx,x_idx,y_idx,z_idx,opt)
% read MODFLOW calculated DRAIN flux from .cbc output
% 06/30/2017, T. Xu
cd('groundwater');
%% call bin2asc.exe
system('del flux.out');
if opt.m == 2
    fp = fopen('read_flux.in','wt');
    fprintf(fp,'1\n%s\nflux.out\n3\n',fname);
    fclose(fp);
end
system('bin2asc < read_flux.in >nul');
%% read from output
system('copy flux.out flux1.out > nul');
q = zeros(length(y_idx),length(x_idx),length(t_idx),length(z_idx));
fp = fopen('flux1.out','r+');
count = 1;
for i = 1:max(t_idx)
    for j = 1:opt.nlayer
        fgetl(fp);
        tmp = fscanf(fp,'%f ');  
        if ~isempty(tmp)
            % storage, constant head, flux front, flux right, drain
            for kk = 1:3
                fgetl(fp);
                fscanf(fp,'%f ');
            end
            fgetl(fp);
            tmp = fscanf(fp,'%f ');
            if ~isempty(find(t_idx == i, 1)) && ~isempty(find(z_idx == j,1))
                tmp2 = reshape(tmp,[opt.ncol, opt.nrow])';
                q(:,:,count,find(z_idx == j,1)) = tmp2(y_idx,x_idx);
                if j == max(z_idx)
                    count = count + 1;
                end
            end
        else  % early termination due to failure of convergence
    %         h(:,:,:) = -999;
            break;
        end
    end
end
fclose(fp);
cd('..');
end