function h = read_modflow_head(fname,t_idx,x_idx,y_idx,z_idx,opt)
cd('groundwater');
%% call bin2asc.exe
system('del head.out');
if opt.m == 1
    fp = fopen('read_head.in','wt');
    fprintf(fp,'0\n%s\nhead.out\n3\n',fname);
    fclose(fp);
end
system('bin2asc < read_head.in >nul');
%% read from output
system('copy head.out head1.out > nul');
h = zeros(length(y_idx),length(x_idx),length(t_idx),length(z_idx));
fp = fopen('head1.out','r+');
count = 1;
for i = 1:max(t_idx)
    for j = 1:opt.nlayer
        fgetl(fp);
        tmp = fscanf(fp,'%f ');
        tmp(tmp == opt.nan) = NaN;
        if ~isempty(tmp)
            if ~isempty(find(t_idx == i, 1)) && ~isempty(find(z_idx == j,1))
                tmp2 = reshape(tmp,[opt.ncol, opt.nrow])';
                h(:,:,count,find(z_idx == j,1)) = tmp2(y_idx,x_idx);
                if j == max(z_idx)
                    count = count + 1;
                end
            end
        else  % early termination due to failure of convergence
    %         h(:,:,:) = -999;
            break;
        end
    end
end
fclose(fp);
cd('..');

% 
