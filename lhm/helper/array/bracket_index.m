function indBracket = bracket_index(inVals,valStart,valEnd,inclusion)
%BRACKET_INDEX  Returns the indeces of an input values vector falling within a min and maximum boundary
%   indBracket = bracket_index(inVals,valStart,valEnd)
%
%   This function returns the indeces of the values vector inVals that
%   indicate the first value greater than or equal to valStart, and the
%   last less than or equal to valEnd
%
%   Descriptions of Input Variables:
%   inVals: a sorted vector of MATLAB datenumbers
%   valStart: a MATLAB datenumber, or leave empty
%   valEnd: a MATLAB datenumber, or leave empty (or unspecified)
%   inclusion: a logical array of dimension 2x1, indicating whether the
%       bracket values should be included, or excluded (i.e. <= vs. <),
%       this is optional
%
%   Descriptions of Output Variables:
%   indBracket: a 1x2 array containing the first and last indeces within
%       the bracket values.  If one of the bounds was not specified, the
%       corresponding index value will be 0;
%
%   Example(s):
%   >> [indBracket] = bracket_index(timeCount,startTime,endTime);
%   >> intervalTime = timeCount(indBracket(1):indBracket(2));
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-13
% Copyright 2008 Michigan State University.

%Check that the inVals array is sorted, throw an error otherwise
assert(issorted(inVals),'The input values vector must be sorted');

%Check to see if a fourth argument was supplied
if nargin < 4
    inclusion = true([1,2]);
end

%Determine the first index of inVals with a value greater than or equal to
%valStart
if ~isempty(valStart)
    if inclusion(1)
        indStart = find(inVals >= valStart, 1, 'first');
    else
        indStart = find(inVals > valStart, 1, 'first');
    end
else
    indStart = 1;
end

%Determine the last index of inVals with a value less than or equal to
%valEnd
if nargin < 3
    valEnd = [];
end
if ~isempty(valEnd)
    if inclusion(2)
        indEnd = find(inVals <= valEnd, 1, 'last');
    else
        indEnd = find(inVals < valEnd, 1, 'last');
    end
else
    indEnd = length(inVals);
end

%Four possibilities
if isempty(indStart) && ~isempty(indEnd) %in this case, all values are greater than the inVals
    indBracket = [];
elseif ~isempty(indStart) && isempty(indEnd) %in this case, all values are less than the inVals
    indBracket = [];
elseif indStart > indEnd %In this case, there is a gap in inVals greater than the distance between the start and end vals
    indBracket = [];
else
    indBracket = [indStart,indEnd];
end
