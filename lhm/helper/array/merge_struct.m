function struct1 = merge_struct(struct1,struct2,prefix,overwrite)
%MERGE_STRUCT  Merges structured arrays
%   struct1 = merge_struct(struct1,struct2,prefix,overwrite)
%
%   Merges structured arrays by appending the fields of the second
%   onto the first. The user can specify how to handle existing fieldnames
%
%   Descriptions of Input Variables:
%   struct1: structured array
%   struct2: structured array
%   prefix: optional, a character array that will be added to the beginning
%       of the fieldnames from struct2, default is ''
%   overwrite: option, a boolean flag to specify whether existing fields
%       in struct1 will be overwritten by the struct2
%
%   Descriptions of Output Variables:
%   struct1: the expanded struct1, with struct2 merged with it
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Last Updated: 2016-05-19
% Copyright 2008 Michigan State University.


assert(isstruct(struct1) && isstruct(struct2),'First two inputs must be structs');
fields = fieldnames(struct2);
origFields = fieldnames(struct1);

if nargin < 4
    overwrite = false;
    if nargin < 3
        prefix = '';
    end
end
assert(ischar(prefix),'Prefix must be a character array');

newFields = cell(size(fields));
for m = 1:length(fields)
    newFields{m} = [prefix,fields{m}];
end

assert(overwrite || ~any(ismember(origFields,newFields)),'Fieldnames cannot be identical unless overwrite = true')

for m = 1:length(fields)
    struct1.(newFields{m}) = struct2.(fields{m});
end
end