function h5varputcell(hfile,varname,data)
% This function writes a cell array to a variable length string array type
% in the HDF5 file.
%
% H5VARPUTCELL(hfile,varname,data) writes an entire dataset to the variable given
% by VARNAME.
%% Create a new file using the default properties.
%
file = H5F.open (hfile,'H5F_ACC_RDWR','H5P_DEFAULT');

%
% Create file and memory datatypes. 
%
filetype = H5T.copy ('H5T_C_S1');
H5T.set_size (filetype,'H5T_VARIABLE');
memtype = H5T.copy ('H5T_FORTRAN_S1');
H5T.set_size (memtype, 'H5T_VARIABLE');

%
% Create dataspace.  Setting maximum size to [] sets the maximum
% size to be the current size.
%
space = H5S.create_simple (ndims(data),size(data), []);

%
% Create the dataset and write the variable-length string data to
% it.
%
dset = H5D.create (file, varname , filetype, space, 'H5P_DEFAULT');
H5D.write (dset, memtype, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data');

%
% Close and release resources.
%
H5D.close (dset);
H5S.close (space);
H5T.close (filetype);
H5T.close (memtype);
H5F.close (file);




end