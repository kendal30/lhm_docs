function [data,attribs] = h5dataread(hfile,hvar,reformat,start,count,stride)
%H5DATAREAD  Reads HD5 data using the ILHM data/attribute standards
%   [data,units] = h5dataread(hfile,hvar,reformat,start,count,stride)
%
%   This is a high-level wrapper for the H5VARGET function that reads in a
%   dataset, reads its attributes, and then outputs a properly-formatted,
%   rescaled, double-precision array.
%
%   Descriptions of Input Variables:
%   hfile:  the hdf5 file to read inputs from
%   hvar:   the complete address of the input dataset in the HDF5 file
%   reformat:   a logical true/false value that, if true will execute the
%           attribute-specified scale, offset, and transform inverse
%           operations, and output a double-precision array.  If false, the
%           output array will have the same format as was stored in the HDF
%           file.  If this value is missing or empty, a TRUE value will be
%           assumed.
%   start:  an optional array indicating the index to start reading from.
%           Note, HDF5 indeces are 0-based, whereas MATLAB's are 1-based.
%           The array must be of length equal to the dataset dimension.
%   count:  an optional array specifying the number of elements to read in
%           each dimension.
%   stride: an optional scalar specifying the inter-element distance to
%           read in each direction.
%
%   Descriptions of Output Variables:
%   data:   array specified in varargin, or logical false if array does not
%           exist
%   units:  a character array containing the physical units of the dataset
%           as specified in the attributes, or logical false if array does not
%           exist
%
%   Example(s):
%   none
%
%   See also: h5varget h5datawrite

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-01
% Copyright 2008 Michigan State University.

%Fill in default values if not specified
switch nargin
    case 2
        [start,count,stride] = deal([]);
        reformat = true;
    case 3
        [start,count,stride] = deal([]);        
    case 4
        error('If START is specified, COUNT must be specified as well');
    case 5
        stride = [];
end
if isempty(reformat),reformat = true;end
assert(islogical(reformat),'Input "reformat" must be a logical value, see HELP');

%First, try to read in the dataset
try
    data = h5varget(hfile,hvar,start,count,stride);
catch
    try
        data = h5vargetcell(hfile,hvar);
    catch
        data = false;
    end
end

if islogical(data)
    attribs = false;
    return
end
if ischar(data)
    attribs.class = class(data);
elseif iscell(data)
    attribs.class = class(data);
else
    %Next, try to fetch each of the attributes, get the default one if the
    %attribute is not specified explicitly
    attribList = {'units','scale','offset','nan'}; %'transform'};
    attribs = struct();
    for m = 1:length(attribList)
        tryAttrib = h5attget(hfile,[hvar,'/',attribList{m}]);
        if ~islogical(tryAttrib)
            attribs.(attribList{m}) = tryAttrib;
        end
    end
    
    %Add the class of the data to the attributes
    attribs.class = class(data);
    
    if reformat
        data = double(data);
        
        %Convert attribute-specified NaN values to NaNs
        if isfield(attribs,'nan')
            data(data==str2double(attribs.nan)) = NaN;
        end
        
        data = h5scale_offset_transform(data,attribs,false);
    end
end
end

function [attrib] = try_attrib(hfile,hvar,attribName) %#ok<DEFNU>
try
    attrib = h5attget(hfile,[hvar,'/',attribName]);
catch
    attrib = false;
end
end