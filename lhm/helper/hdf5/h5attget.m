function attval = h5attget(hfile,varname,attname)
% H5ATTGET:  retrieves an attribute from an HDF5 file.
%
% ATTVAL=H5ATTGET(HFILE,ATTPATH) retrieves an attribute with full path
% name ATTNAME from the HDF5 file HFILE.  
%
% ATTVAL=H5ATTGET(HFILE,DATASET,ATTNAME) retrieves an attribute named
% ATTNAME from the HDF5 file HFILE.  DATASET can be either a group
% or a traditional dataset.  Use '/' for the root group.
%
% [rx1] Character string attributes are changed into 'left-right' order, 
% i.e. [1xr].
% 
% Example:  
%     >> d = h5attget ( 'example.h5', '/g1/g1.1/dset1.1.1', 'att1' );
%
% Revision History:
% ADK 2018-04-04: Removed the use of deprecated H5ML functions

%Check arguments and validate inputs
narginchk(2,3);
nargoutchk(1,1);

if nargin == 2
    slashes = strfind ( varname, '/' );
    if isempty(slashes)
        error ( 'MATLAB:H5ATTGET:badAttributePath', ...
                'Could not parse the given attribute path, ''%s''', varname );
    elseif slashes == 1
        % case of "/attname" is different than "/path/to/attname"
        attname = varname(2:end);
        varname = varname(1);
    else
        attname = varname(slashes(end)+1:end);
        varname = varname(1:slashes(end)-1);
    end
end
parent_is_dataset = true;

% Just use the defaults for now
flags = 'H5F_ACC_RDONLY';
plist_id = 'H5P_DEFAULT';

file_id = H5F.open(hfile,flags,plist_id);

if strcmp(varname,'/')
    parent_is_dataset = false;
    parent_id = H5G.open(file_id,varname);
else
    try
        parent_id=H5D.open(file_id,varname);
    catch
        parent_is_dataset = false;
        parent_id = H5G.open(file_id,varname);
    end
    if parent_id == -1
        parent_is_dataset = false;
        parent_id = H5G.open(file_id,varname);
    end
end

try
    %Commented out to try and speed up the function
    att_id = H5A.open_name(parent_id,attname);
    attval = H5A.read(att_id,'H5ML_DEFAULT');
    H5A.close(att_id);
catch
    attval = false;
end

% Post process things.
switch class(attval)
    case 'char'
        % If the characters are a column vector, change it into a row vector.
        [~,n] = size(attval);
        if ( n == 1) 
            attval = attval';
        end
end

if parent_is_dataset
    H5D.close(parent_id);
else
    H5G.close(parent_id);
end
H5F.close(file_id);
