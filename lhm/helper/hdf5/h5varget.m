function data = h5varget ( varargin )
% H5VARGET:  reads data from an HDF5 file.
%
% DATA = H5VARGET(HFILE,HVAR) retrieves all of the data from the variable HVAR
% in the file HFILE.
%
% DATA = H5VARGET(HFILE,HVAR,START,COUNT) reads a contiguous subset of data
% from the variable HVAR in the file HFILE.  The contiguous subset is a
% hyperslab defined by the index vectors START and COUNT, which are zero-based.
%
% DATA = H5VARGET(HFILE,HVAR,START,COUNT,STRIDE) reads a strided subset of data
% from the variable HVAR in the file HFILE.  The strided subset will begin at
% the index START, have a length extent of COUNT along each dimension, but
% have an inter-element distance given by STRIDE.  When not explicitly
% specified, STRIDE is implicitly a vector of ones, i.e. contiguous data.
%
% Example:
%   data = h5varget ( 'example.h5', '/g2/dset2.1' );
%
% Revision History:
% ADK 2018-04-04: Removed the use of deprecated H5ML functions
%
%   Copyright 2007 The MathWorks, Inc.
%   $Revision: 1.2 $  $Date: 2008/08/01 20:30:41 $

%Check arguments and validate inputs
narginchk(2,5);
nargoutchk(1,1);
[hfile,varname,offset,count,stride,readflag] = parse_h5_varget_options ( varargin{:} );

% Just use the defaults for opening the file
flags = 'H5F_ACC_RDONLY';
plist_id = 'H5P_DEFAULT';

%Open the file and the dataset
file_id = H5F.open(hfile,flags,plist_id);
dataset_id = H5D.open(file_id,varname);

%In case the dataset doesn't exist
if isempty(dataset_id)
    data = false;
    H5F.close(file_id);
    return
elseif dataset_id==-1
    data = false;
    H5F.close(file_id);
    return
end

%First, make sure we have the right number of requested dimensions
dimension_id = create_dataspace_id(dataset_id,[],[],[]);
[rank,~,~] = H5S.get_simple_extent_dims(dimension_id);
H5S.close(dimension_id);

if ~isempty(offset);offset = offset(1:rank);end
if ~isempty(count);count = count(1:rank);end
if ~isempty(stride);stride = stride(1:rank);end

%Get the filespace and dataspace IDs
filespace_id = create_filespace_id(dataset_id,offset,count,stride);
dataspace_id = create_dataspace_id(dataset_id,offset,count,stride);

%Get the array size with bounds
[numdims,dims,maxdims] = H5S.get_simple_extent_dims(dataspace_id); %#ok<ASGLU>

%Return the right number of dims
dims = dims(1:numdims);

if readflag
    data = H5D.read(dataset_id, 'H5ML_DEFAULT', dataspace_id, filespace_id, plist_id);
    
    %Reshape the output to match the array stored on disk
    if ischar(data)
        data = data';
    elseif iscell(data)
        data = data';
    else
        data = reshape(permute(data,(length(dims):-1:1)),dims);
    end
else
    data = dims';
end

%Clean up
H5S.close(dataspace_id);
H5D.close(dataset_id);
H5F.close(file_id);
end


%===============================================================================
% Create the dataspace that corresponds to the given selection.
function dataspace_id = create_dataspace_id(dataset_id,offset,count,stride)

% Create the appropriate output dataspace.
if isempty(offset) && isempty(count) && isempty(stride)
    dataspace_id = H5D.get_space(dataset_id); %'H5S_ALL';
else
    dataspace_id = H5S.create_simple(length(offset),count,count);
end
end



% Create the filespace that corresponds to the given selection.
function filespace = create_filespace_id(dataset_id,offset,count,stride)

% Create the appropriate mem dataspace
if isempty(offset) && isempty(count) && isempty(stride)
    filespace = 'H5S_ALL';
else
    % define the memory hyperslab
    filespace = H5D.get_space(dataset_id);
    H5S.select_hyperslab(filespace, 'H5S_SELECT_SET', ...
        offset, stride, count, ...
        ones(1,length(offset)));
end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [hfile,varname,start,count,stride,readflag] = parse_h5_varget_options ( varargin )
%
% Have to be able to check the following signatures.

% DATA = H5_VARGET(HFILE,VARIABLE)
% DATA = H5_VARGET(HFILE,VARIABLE,START,COUNT)
% DATA = H5_VARGET(HFILE,VARIABLE,START,COUNT,STRIDE)

% First argument should be the filename.
assert(ischar(varargin{2}),'MATLAB:H5TOOLS:badInput', 'File argument must be character')
hfile = varargin{1};

% 2nd argument should be the variable name.
assert(ischar(varargin{2}),'MATLAB:H5TOOLS:badInput', 'Variable name argument must be character')
varname = varargin{2};


readflag = true; %this defaults to true
switch nargin
    case 2
        start = [];
        stride = [];
        count = [];
    case 3
            assert(islogical(varargin{3}),'MATLAB:H5TOOLS:badInput','3rd input argument must be a logical flag')
            readflag = varargin{3};
            start = [];
            stride = [];
            count = [];          
    case 4
        %
        % Given the start, stride, and count.
        assert(isnumeric(varargin{3}),'MATLAB:H5TOOLS:badInput', 'Start argument must be numeric')
        start = varargin{3};
        
        assert(isnumeric(varargin{4}),'MATLAB:H5TOOLS:badInput', 'Count argument must be numeric')
        count = varargin{4};
        
        stride = [];
    case 5
        %
        % Given the start, stride, and count.
        assert(isnumeric(varargin{3}),'MATLAB:H5TOOLS:badInput', 'Start argument must be numeric')
        start = varargin{3};
        
        assert(isnumeric(varargin{4}),'MATLAB:H5TOOLS:badInput', 'Count argument must be numeric')
        count = varargin{4};
        
        assert(isnumeric(varargin{5}),'MATLAB:H5TOOLS:badInput', 'Stride argument must be numeric')
        stride = varargin{5};
    otherwise
        error('MATLAB:H5TOOLS:badInput', 'Bad number of input arguments.')
        
end

%Make sure this is true
count = double(count);
start = double(start);
stride = double(stride);
end


