function existFlag = h5groupcreate(h5file,groupName)
%h5groupcreate(h5file,groupName)
%groupName: the full path to the group to be created
%The function will return a true/false flag indicating if the group already
%exists.
if exist(h5file,'file')
    file_id = H5F.open(h5file,'H5F_ACC_RDWR','H5P_DEFAULT');
else
    file_id = H5F.create(h5file,'H5F_ACC_TRUNC','H5P_DEFAULT','H5P_DEFAULT');
end

%Create the group 
try
    group_id = H5G.open(file_id,groupName);
    existFlag = true;
catch
    group_id = H5G.create(file_id,groupName,0);
    existFlag = false;
end

%Clean up
H5G.close(group_id);
H5F.close(file_id);