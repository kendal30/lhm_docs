function u = tridiagonal_solver(a,b,c,r)
%currently, this solver works only for 1-D problems, including arrays of
%1-D problems.  if solving arrays of 1-D problems, layers go in the row
%index, and individual problems in the column index

numLayers=size(a,2);

%Initialize arrays
[u,gam] = deal(zeros(size(a)));

%first_layer
beta = b(:,1);
u(:,1) = r(:,1) ./ beta;

%second through last layers
for m=2:numLayers
    gam(:,m) = c(:,m-1) ./ beta;
    beta = b(:,m) - a(:,m) .* gam(:,m);
    u(:,m) = (r(:,m) - a(:,m) .* u(:,m-1)) ./ beta;
end

%now, go backwards through the layers
for m = numLayers-1:-1:1
    u(:,m) = u(:,m) - gam(:,m+1) .* u(:,m+1);
end

