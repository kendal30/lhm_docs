function [output] = weighted_mean(typeMean,varargin)
%WEIGHTED_MEAN  Calculates weighted means of input value and weight vectors.
%   [output] = weighted_mean(typeMean,varargin)
%
%   This function calculates three types of weighted means: arithmetic,
%   geometric, and harmonic.  It differes from the built-in MATLAB function
%   'mean' in that the individual values are weighted differently than 1.
%   Also, this function can take the mean across a number of input arrays
%   of arbitrary dimensions.
%
%   If there are N (mxn) size values arrays are input along with N (mxn)
%   size weight arrays.  The output will be of size (mxn) with each element
%   representing the weighted mean of all the N
%   cells in that position across the input arrays.  Another way to think
%   of this is that the input arrays are turned into a single array of size
%   (mxnxN) and the weighted mean is taken across the third dimension.
%   This is, in fact, what the code does.  The arrays can be of completely
%   aribtrary dimension.
%
%   It is not necessary that the sum of the weights equal one, as the
%   formula used here performs the normalization.  The weights must,
%   however be nonnegative and at least one must be greater than 0.
%
%   Note, for the geometric mean, values must be > 0.
%
%   There is one additional functional mode, if there is only a single
%   input vector and weights vector, then the code will take the
%   weighted_mean across the highest non-singleton dimension.
%
%   Descriptions of Input Variables:
%   typeMean: a string with values 'geometric','arithmetic', or 'harmonic'
%       specifying the type of geometric mean to be calculated.
%   varargin: contains vectors of values and weights to calculate means.
%       varargin={val1, val2, ..., valN, weight1, weight2,
%           ...,weightN}
%           All must have identical dimension or the code will result in an
%           error.
%
%   Descriptions of Output Variables:
%   output:
%        an array with size equal to the input value and weight
%           arrays
%
%   Example(s):
%   >> arrayMean =
%   weighted_mean('geometric',[1,2,3],[4,5,6],[0.2,0.3,0.2],[0.2,0.1,0.1]);
%   %the output is a vector of size (1x3)
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

numArrays = (nargin - 1) / 2;
fhandle = str2func(typeMean);

if numArrays > 1
    [numerator,denominator] = deal(0);
    for m = 1:numArrays
        [numTemp,denTemp] = fhandle(varargin{m},varargin{numArrays+m});
        numerator = numerator + numTemp;
        denominator = denominator + denTemp;
    end
    output = numerator ./ denominator;
else
    maxDim = ndims(varargin{1});
    [numTemp,denTemp] = fhandle(varargin{1},varargin{2});
    output = sum(numTemp,maxDim)./ sum(denTemp,maxDim);
end

if strcmpi(typeMean,'geometric')
    output = exp(output);
end

end

function [num,den] = arithmetic(vals,weights) %#ok<DEFNU>
num = weights .* vals;
den = weights;
end

function [num,den] = harmonic(vals,weights) %#ok<DEFNU>
num = weights;
den = weights ./ vals;
end

function [num,den] = geometric(vals,weights) %#ok<DEFNU>
num = weights .* log(vals);
den = weights;
end