function [output] = aggregate(fineGrid,factor,statType,coarseDim,fineDim)
% [output] = aggregate(fineGrid,factor,method)
% or
% [output] = aggregate(fineGrid,factor,statType,coarseDim,fineDim)
%
% This function has two modes:
% 1) Aggregate a fine grid with an integer factor, producing an output
%    with the same top/left as the fine grid, but a different bot/right to 
%    correspond to an integer aggregation
% 2) Aggregate a fine grid to match the extent of a coarse grid, with a specified 
%    integer aggregation factor. Top/left will match the coarse grid, and
%    bot/right will be close, corresponding to an integer aggregation of the
%    fine grid.
%
% Inputs:
% - fineGrid: 	A 2-D array to be aggregated
% - factor: 	An integer aggregation factor
% - statType: 	A string specifying the name of the statistic to use for aggregation
% - coarseDim: 	A structured array of grid dimensions, coarse grid (used for mode 2)
% - fineDim:	A structured array of grid dimensions, fine grid (used for mode 2)
%
% For the coarseDim and fineDim arrays, fields are: top, left, cellsizeX, cellsizeY, rows, cols
%
% Currently supported statTypes are: 'median','mean','sum','min',and 'max' and their 'nan' equivalents

if nargin == 5
	% For convenience
	fineEdges = struct('rows',fineDim.top - (0:1:fineDim.rows)*fineDim.cellsizeY, ...
        'cols',fineDim.left + (0:1:fineDim.cols)*fineDim.cellsizeX);

	%First, check to see how the grids align, add a small delta to the coarse
	%grid because of input precision in the grid dimensions
	delta = 1e-3; %m
	firstFineY = find(coarseDim.top - coarseDim.cellsizeY/2 - delta <= fineEdges.rows,1,'last');
	firstFineX = find(coarseDim.left + coarseDim.cellsizeX/2 + delta >= fineEdges.cols,1,'last');

	% Fill in the right and bottom
	coarseDim.right = coarseDim.left + coarseDim.cellsizeX * coarseDim.cols;
	coarseDim.bot = coarseDim.top - coarseDim.cellsizeY * coarseDim.rows;
	fineDim.right = fineDim.left + fineDim.cellsizeX * fineDim.cols;
	fineDim.bot = fineDim.top - fineDim.cellsizeY * fineDim.rows;

	% Identify the last fineGrid cells to include
	lastFineY = find(coarseDim.bot + coarseDim.cellsizeY/2 + delta >= fineEdges.rows,1,'first');
	lastFineX = find(coarseDim.right - coarseDim.cellsizeX/2 - delta <= fineEdges.cols,1,'first');

	% Slice from the fine grid to match the new output
	fineGrid = fineGrid(firstFineY:lastFineY,firstFineX:lastFineX);
elseif nargin == 3
    fineDim = struct();
end

% Determine the outSize and lastInd of the fineGrid
fineSize = size(fineGrid);
fineDim.rows = fineSize(1);
fineDim.cols = fineSize(2);
outSize = [floor(fineDim.rows/factor),floor(fineDim.cols/factor)];
lastInd = outSize * factor;

% Now, split the grid into a 3-D grid with the appropriate dimensions
aggArray = zeros(outSize(1),outSize(2),factor);
for m=1:factor
    for n=1:factor
        index=(m-1)*factor+n;
        aggArray(:,:,index)=fineGrid((m:factor:lastInd(1)),...
            (n:factor:lastInd(2)));
    end
end

% Finally, aggregate the data according to the specified method
statFunc = str2func(statType);
if strcmpi(statType,{'mean','median','sum','nanmedian','nanmean','nansum'})
    output = statFunc(aggArray,3);
elseif strcmpi(statType,{'min','max','nanmin','nanmax'})
    output = statFunc(aggArray,[],3);
else
    output = statFunc(aggArray,3);
end
end