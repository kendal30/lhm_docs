function [cellStats,mask,precalc] = cell_statistics(coarseDim,fineDim,fineGrid,statType,fracHandling,precalc)
%CELL_STATISTICS Calculates cell statistics of a 2-D array within a coarser grid
% [cellStats,mask,precalc] = cell_statistics(coarseDim,fineDim,fineGrid,statType,fracHandling)
% OR
% [cellStats,mask,precalc] = cell_statistics(coarseDim,fineDim,fineGrid,statType,fracHandling,precalc)
%
%   This function is intended to calculate statistics on a 2D array within
%   the cells of a coarser-resolution grid.
%
%   It is capable of calculating statistics using any MATLAB function, in
%   addition there are several functions defined herein: 'majority',
%   'minority', 'count', 'range', 'variety', 'lookup'.  'majority' returns
%   the majority value within each coarse cell, and so for 'minority'.
%   'count' returns the number of fine grid cells that are non-Nan within
%   each coarse cell.  'range' returns the difference between max and min
%   values. 'variety' returns the number of unique values within each
%   coarse cell.
%
%   Another function, 'partition', splits the fine grid into separate
%   statistics grids based on the unique values of the fine grid.  For
%   instance, if the fine grid contains the values '4', '5', and '6', then
%   partition will return a set of coarse grids with the fraction of each
%   coarse grid covered by each of those fine grid values. Be careful with
%   'partition', as it creates a cell array of dimensions:
%   [size(coarse_grid,1),size(coarse_grid,2),length(unique(fineGrid))]
%   obviously this can create problems if there are a large number of unique
%   values of fineGrid.
%
%   'lookup' returns a lookup array for mapping the coarse grid onto the
%   fine grid. It's used like this: fineGrid(mask) = coarseGrid(lookup);
%
%   To use any other function, for instance 'mean', 'median', 'min', 'max',
%   'std', 'sum', those functions must be on the MATLAB search path.  As
%   long as those functions behave normally (that is hard-to-define, but
%   basically the statistic is just a straightforward application of the
%   called function on the fine grid cells within the coarser grid cells),
%   then no modification of this code is needed.
%
%   This code has several options to handle fine grid cells that are only
%   partially within a given coarse grid cell.  They are discussed in the
%   next section.  Only a few functions make sense with the use of 'weight'
%   as an option, and they are 'sum', 'mean', 'minority', and 'majority'.
%   'partition' is supported as well.
%
%   Each additional function that 'weight' is desired for must be added
%   manually as weighting is handled differently for all of these
%   functions.  Any explicitly unsupported function attempting to use
%   'weight' will result in behavior as if 'any' were the fracHandling
%   specification.
%
%   Note: 'lookup' is not a valid request if precalc mode is used.
%
%   There is another special mode, if 'fast_mean' or 'fast_sum' are the
%   statTypes, accumarray is used for significant speed increases.
%   Note that this will include NaNs in calculations.  Use 'mean' if this
%   is not desired. This will also use the 'majority' option for weighting.
%
%   Descriptions of Input Variables:
%   coarseDim:  is a specific structured array with the following format:
%               coarseDim = struct('top',[scalar],'left',[scalar],
%               'cellsizeX',[scalar],'cellsizeY',[scalar],'rows',[scalar],
%               'cols',[scalar]).  'top' and 'left' refer to the cell edge
%               locations
%   fineDim:    is a structured array the same as coarseDim, except it has a
%               field 'nan' that is required for integer grids.  This field
%               defines what the noData value or NaN value is of that grid.
%   fineGrid:   a 2-D array, this is the statistics grid.  This function is
%               currently limited to 2147483647 elements.  This could
%               easily be increased for a 64-bit machine
%   statType:   is a cell array of characters with the following options.
%               As many options can be specified as desired
%   fracHandling:   defines how to fine resolution cells that are partially
%               within the coarse cells.  This is a cell array of the same
%               dimensions as statType.  Options are 'any','majority','weight',or
%               'none'. If this is unspecified, then the default value will be
%               'majority'.
%               - 'any' effectively applies no weighting, treating
%                   all cells equally.
%               - 'majority' will only include cells that are greater than
%                   50% within each coarse cell.
%               - 'weight' will weight the statistics according to the cell
%                   fractions.
%               - 'none' will exclude all cells that are only partly within
%                   the cell from analysis.
%   precalc:    This optional structured array allows for all of the grid-location portions of this
%               script to be calculated just once, and re-used.  This is
%               helpful for looped applications.
%
%
%   note:   if just the dimension grids are input, then the output will be
%           the lookup and mask grids only
%
%   Descriptions of Output Variables:
%   cellStats: a structured array with fields governed by the 'statType' input
%               array. If there is only a single statType requested, then the
%               returned output will not be a structured array, but rather
%               a 2-D numerical array.
%   mask:       logical mask for the fine grid to map coarse values
%   precalc:    a structured array with fields
%
%   Example(s):
%
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-18
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Validate the inputs
%--------------------------------------------------------------------------
error(nargchk(2,6,nargin,'struct'));
error(nargoutchk(1,3,nargout,'struct'));
numArg = nargin;
if numArg == 2
    statType = validate_inputs(coarseDim,fineDim);
else
    assert(numArg >= 4,'If more than two arguments are supplied, at least 4 must be included, see HELP');
        if numArg == 4
            fracHandling = 'majority';
        end
    if numArg < 6 %Limited validating on precalc
        [statType,fracHandling] = validate_inputs(coarseDim,fineDim,fineGrid,statType,fracHandling);
    else
        if ischar(statType),statType = {statType};end
        if ischar(fracHandling),fracHandling = {fracHandling};end
    end
    if ~isfield(fineDim,'nan')
        if isinteger(fineGrid)
            nanVal = intmax(class(fineGrid));
        else
            nanVal = NaN;
        end
    else
        nanVal = fineDim.nan;
    end
    if numArg > 5
        assert(isstruct(precalc),'The precalc array appears not to have been produced by this function on a previous call');
    end
end
numStat = length(statType);
%Check to see whether the output will be a flat array, or a structure
flatOut = (numStat == 1) && ~strcmpi(statType{1},{'partition'});

%Check to see if fast mode can be used for each type
fastMode = false(size(statType));
if numArg > 2
    for m = 1:numStat
        if any(strcmpi(statType{m},{'fast_mean','fast_sum'})),fastMode(m) = true;end
    end
end

if nargin < 6 %This is the standard mode
    %--------------------------------------------------------------------------
    %First, create the lookup and mask arrays
    %--------------------------------------------------------------------------
    %Identify the bounding box of the coarse grid
    coarseLeftEdge = coarseDim.left;
    coarseRightEdge = coarseDim.left + coarseDim.cellsizeX * coarseDim.cols;
    coarseTopEdge = coarseDim.top;
    coarseBotEdge = coarseDim.top - coarseDim.cellsizeY * coarseDim.rows;
    
    %Trim the fine resolution grid using this bounding box, define this as if a
    %cell node lies within the bounding box
    %First, build an array with cell node locations
    fineX = fineDim.left + fineDim.cellsizeX * ((0:fineDim.cols-1) + 1/2);  %cell nodes, runs from left to right
    fineY = fineDim.top - fineDim.cellsizeY * ((0:fineDim.rows-1) - 1/2);  %cell nodes, runs from top to bottom
    
    %Second, determine if these nodes are within the bounding box
    fineInCoarseX = (fineX > coarseLeftEdge) & (fineX < coarseRightEdge);
    fineInCoarseY = (fineY > coarseBotEdge) & (fineY < coarseTopEdge);
    
    %Third, build a mask using repmat
    mask = repmat(fineInCoarseX,fineDim.rows,1) & repmat(fineInCoarseY',1,fineDim.cols);
    testMask = any(~mask(:));
    %Throw an error if there are areas of the fine grid outside the coarse
    %grid, and the mask has not been requested as an output
    if testMask && (nargout==1)
        error('Some part of the fine grid lies outside the coarse grid, the mask must be returned');
    end
    
    %Now, trim the fine grid dimensions to match the masked domain
    if testMask
        fineDim.cols = sum(fineInCoarseX);
        fineDim.rows = sum(fineInCoarseY);
        fineX = fineX(fineInCoarseX);
        fineY = fineY(fineInCoarseY);
        fineDim.left = fineX(1) - fineDim.cellsizeX/2;
        fineDim.top = fineY(1) + fineDim.cellsizeY/2;
        
        testX = find(fineInCoarseX);
        testY = find(fineInCoarseY);
    end
    
    %calculate coarse->fine cellsize ratios and grid offsets
    cellRatioX = fineDim.cellsizeX / coarseDim.cellsizeX;
    cellRatioY = fineDim.cellsizeY / coarseDim.cellsizeY;
    gridOffsetX = (fineDim.left - coarseDim.left) / coarseDim.cellsizeX;
    gridOffsetY = (coarseDim.top - fineDim.top) / coarseDim.cellsizeY;
    
    %next, calculate the index of the coarse cell in which each fine cell lies
    coarseIndFineX = 1 + fix((0:fineDim.cols-1) * cellRatioX + gridOffsetX);
    coarseIndFineY = 1 + fix((0:fineDim.rows-1) * cellRatioY + gridOffsetY);
    coarseIndActive = [coarseIndFineX(1),coarseIndFineX(end),coarseIndFineY(1),coarseIndFineY(end)];
    
    %Finally, calculate the coarse-fine lookup table
    testLookup = strcmpi(statType,'lookup');
    if any(testLookup)
        cellStats.lookup = sub2ind([coarseDim.rows,coarseDim.cols],...
            repmat(int32(coarseIndFineY'),1,fineDim.cols),...
            repmat(int32(coarseIndFineX),fineDim.rows,1));
        if flatOut %abort right here if no other outputs are desired
            tempStats = cellStats;
            clear cellStats
            cellStats = tempStats.lookup;
            return
        else %remove 'lookup' as it's obviously already been calculated
            statType(testLookup) = [];
            fracHandling(testLookup) = [];
        end
    end
    
    if any(fastMode)
        fastInd = sub2ind([coarseDim.rows,coarseDim.cols],repmat(coarseIndFineY',1,fineDim.cols),...
            repmat(coarseIndFineX,fineDim.rows,1));
        [fastIndUnique,fastIndNum] = count_unique(fastInd);
    end
    
    if ~all(fastMode)
        %--------------------------------------------------------------------------
        %Calculate the fractional cell coverage weighting
        %--------------------------------------------------------------------------
        %first, calculate the fine cell in which each coarse grid edge lies
        coarseEdgeFineX = 1 + ((0:coarseDim.cols) - gridOffsetX) / cellRatioX;
        coarseEdgeFineY = 1 + ((0:coarseDim.rows) - gridOffsetY) / cellRatioY;
        
        %Now, calculate intermediate quantities from this
        edgeCellCoarseX = floor(coarseEdgeFineX);
        edgeCellCoarseY = floor(coarseEdgeFineY);
        fracCellCoarseX = rem(coarseEdgeFineX,1);
        fracCellCoarseY = rem(coarseEdgeFineY,1);
        
        %Determine the edge cells, replace with NaNs if values are out of range
        xMin = edgeCellCoarseX(1:end-1);
        xMax = edgeCellCoarseX(2:end);
        yMin = edgeCellCoarseY(1:end-1);
        yMax = edgeCellCoarseY(2:end);
        
        %then, calculate the fraction of the previous and next edge cell, negative
        %values are present, as are values greater than 1, but these match the NaNs
        %in the edgeCell* arrays, so that's okay
        xMinFrac = 1 - fracCellCoarseX(1:end-1);
        xMaxFrac = fracCellCoarseX(2:end);
        yMinFrac = 1 - fracCellCoarseY(1:end-1);
        yMaxFrac = fracCellCoarseY(2:end);
        
        %Trim the values to the proper range, this can result in inproper values
        %for coarse grid cells entirely outside of the fine grid, but this is OK as
        %those are not used later on
        test = (xMin < 1);
        xMin(test) = 1;
        xMinFrac(test) = 1;
        
        test = (xMax < 1);
        xMax(test) = 1;
        xMaxFrac(test) = 1;
        
        test = (xMin > fineDim.cols);
        xMin(test) = fineDim.cols;
        xMinFrac(test) = 1;
        
        test = (xMax > fineDim.cols);
        xMax(test) = fineDim.cols;
        xMaxFrac(test) = 1;
        
        test = (yMin < 1);
        yMin(test) = 1;
        yMinFrac(test) = 1;
        
        test = (yMax < 1);
        yMax(test) = 1;
        yMaxFrac(test) = 1;
        
        test = (yMin > fineDim.rows);
        yMin(test) = fineDim.rows;
        yMinFrac(test) = 1;
        
        test = (yMax > fineDim.rows);
        yMax(test) = fineDim.rows;
        yMaxFrac(test) = 1;
    end
    
    %Create a blank template grid
    coarseBlank = zeros(coarseDim.rows,coarseDim.cols);
    if isinteger(fineGrid)
        coarseBlank = coarseBlank + nanVal;
    else
        coarseBlank = coarseBlank*NaN;
    end
    
else %Precalc mode
    %Used for all modes
    mask = precalc.mask;
    testMask = precalc.testMask;
    coarseBlank = precalc.coarseBlank;
    fastMode = precalc.fastMode;
    
    if testMask
        testX = precalc.testX;
        testY = precalc.testY;
    end
    if any(fastMode)
        fastInd = precalc.fastInd;
        fastIndUnique = precalc.fastIndUnique;
        fastIndNum = precalc.fastIndNum;
    end
    if ~all(fastMode)
        coarseDim = precalc.coarseDim;
        fineDim = precalc.fineDim;
        fineInCoarseX = precalc.fineInCoarseX;
        fineInCoarseY = precalc.fineInCoarseY;
        coarseIndActive = precalc.coarseIndActive;
        mask = precalc.mask;
        nanVal = precalc.nanVal;
        xMin = precalc.xMin;
        xMax = precalc.xMax;
        yMin = precalc.yMin;
        yMax = precalc.yMax;
        xMinFrac = precalc.xMinFrac;
        xMaxFrac = precalc.xMaxFrac;
        yMinFrac = precalc.yMinFrac;
        yMaxFrac = precalc.yMaxFrac;
    end
end

%--------------------------------------------------------------------------
%If requested, create the precalc structured array
%--------------------------------------------------------------------------
if nargout == 3
    precalc = struct('mask',mask,'testMask',testMask,'coarseBlank',coarseBlank);
    if testMask
        precalc.testX = testX;
        precalc.testY = testY;
    end
    if ~all(fastMode)
        precalcAdd = struct('coarseDim',coarseDim,'xMin',xMin,'xMax',xMax,'yMin',yMin,...
            'yMax',yMax,'xMinFrac',xMinFrac,'xMaxFrac',xMaxFrac,'yMinFrac',yMinFrac,...
            'yMaxFrac',yMaxFrac,'nanVal',nanVal,'coarseIndActive',coarseIndActive,...
            'fineInCoarseX',fineInCoarseX,'fineInCoarseY',fineInCoarseY,...
            'fineDim',fineDim,'fastMode',false);
        precalc = merge_struct(precalc,precalcAdd);
    end
    if any(fastMode)
        precalc.fastMode = fastMode;
        precalc.fastInd = fastInd;
        precalc.fastIndUnique = fastIndUnique;
        precalc.fastIndNum = fastIndNum;
    end
end

%Now, trim the fine grid to match the masked domain
if testMask
    fineGrid = fineGrid(testY(1):testY(end),testX(1):testX(end));
end

%--------------------------------------------------------------------------
%calculate this now because I may not have enough memory to do it later
%--------------------------------------------------------------------------
for h = 1:numStat
    switch statType{h}
        case 'partition'
            uniqueFine = count_unique(fineGrid(~isbad(fineGrid,nanVal)));
    end
end

%--------------------------------------------------------------------------
%Loop through the zonal stat types
%--------------------------------------------------------------------------

%Initialize the output struct, if it's not already defined
if ~exist('cellStats','var');cellStats = struct;end
for h = 1:numStat
    if fastMode(h)
        [accumStat] = accumarray(fastInd(:),fineGrid(:));
        tempStat = coarseBlank;
        if strcmpi(statType{h},'fast_mean')
            tempStat(fastIndUnique) = accumStat(fastIndUnique)./fastIndNum;
        end
        cellStats.(statType{h}) = tempStat;
    else
        [xMinTemp,xMaxTemp,yMinTemp,yMaxTemp] = frac_handling(xMin,xMax,yMin,yMax,...
            xMinFrac,xMaxFrac,yMinFrac,yMaxFrac,fracHandling{h});
        switch statType{h}
            case 'partition'
                %initialize the partition array
                partArray = cell(length(uniqueFine),1);
                for o = 1:length(partArray)
                    loopStat = single(coarseBlank);
                    fineTemp = (fineGrid == uniqueFine(o));
                    weightTest = strcmpi(fracHandling{h},{'weight'});
                    for m = coarseIndActive(3):coarseIndActive(4)
                        for n = coarseIndActive(1):coarseIndActive(2)
                            tempData = fineTemp(yMinTemp(m):yMaxTemp(m),xMinTemp(n):xMaxTemp(n));
                            test = ~isnan(tempData);
                            %Build the weight array if necessary
                            if weightTest
                                tempWeight = weight_array(tempData,[xMinFrac(n),xMaxFrac(n),yMinFrac(m),yMaxFrac(m)]);
                            else
                                tempWeight = ones(size(tempData));
                            end
                            tempWeight = tempWeight(test);
                            %Wait to trim tempData because this will change its
                            %size
                            tempData = tempData(test);
                            if ~isempty(tempData)
                                loopStat(m,n) = sum(tempWeight(tempData)) / sum(tempWeight);
                            end
                        end
                    end
                    partArray{o} = loopStat;
                end
                %write the output and clear the variables used here
                cellStats.partition.index = uniqueFine;
                cellStats.partition.array = partArray;
                clear uniqueFine partArray tempStat
            otherwise  %All other cases behave normally
                tempStat = coarseBlank;
                tempFunc = str2func(statType{h});
                fineTemp = double(fineGrid);
                weightTest = strcmpi(fracHandling{h},{'weight'});
                for m = coarseIndActive(3):coarseIndActive(4)
                    for n = coarseIndActive(1):coarseIndActive(2)
                        tempData = fineTemp(yMinTemp(m):yMaxTemp(m),xMinTemp(n):xMaxTemp(n));
                        test = ~isbad(tempData,nanVal);
                        %Build the weight array if necessary
                        if weightTest
                            tempWeight = weight_array(tempData,[xMinFrac(n),xMaxFrac(n),yMinFrac(m),yMaxFrac(m)]);
                            tempWeight = tempWeight(test);
                        end
                        %Wait to trim tempData because this will change its
                        %size
                        tempData = tempData(test);
                        if ~isempty(tempData)
                            %Weight the numerator
                            if any(strcmpi(statType{h},{'mean','sum'})) && weightTest
                                tempData = tempData .* tempWeight;
                            end
                            %This function call is defined by the handle above
                            if any(strcmpi(statType{h},{'minority','majority'})) && weightTest
                                %Weighting is performed in the function call
                                tempStat(m,n) = tempFunc(tempData,tempWeight);
                            else
                                tempStat(m,n) = tempFunc(tempData);
                            end
                            %Weight the denominator
                            if strcmpi(statType{h},{'mean'}) && weightTest
                                tempStat(m,n) = tempStat(m,n) ./ sum(tempWeight) .* numel(tempData);
                            end
                        end
                    end
                end
                cellStats.(statType{h}) = tempStat;
        end
        clear tempStat tempData tempWeight fineTemp
    end
end
%flatten the structured array if only one output is used
if flatOut
    tempStats = cellStats;
    clear cellStats
    cellStats = tempStats.(statType{1});
end
end

%--------------------------------------------------------------------------
%Some internal helper functions
%--------------------------------------------------------------------------
function [output]=isbad(input,nanVal)
if isinteger(input)
    output=(input==nanVal);
else
    output=(isnan(input) | isinf(input));
end

end

function weight = weight_array(data,frac)
weight = ones(size(data));
weight(:,1) = weight(:,1) .* frac(1);
weight(:,end) = weight(:,end) .* frac(2);
weight(1,:) = weight(1,:) .* frac(3);
weight(end,:) = weight(end,:) .* frac(4);
end

function [xMin,xMax,yMin,yMax] = frac_handling(xMin,xMax,yMin,yMax,...
    xMinFrac,xMaxFrac,yMinFrac,yMaxFrac,fracHandling)
switch lower(fracHandling)
    case 'none'
        threshold = 1;
    case 'majority'
        threshold = 0.5;
    otherwise
        return %weight and any don't require altering the bounds
end
test = (xMinFrac < threshold);
xMin(test) = xMin(test) + 1; %increase by 1 to not include boundary cell
test = (xMaxFrac < threshold);
xMax(test) = xMax(test) - 1; %similarly, decrease by 1
test = (yMinFrac < threshold);
yMin(test) = yMin(test) + 1;
test = (yMaxFrac < threshold);
yMax(test) = yMax(test) -1;
end

function [statType,fracHandling] = validate_inputs(coarseDim,fineDim,fineGrid,statType,fracHandling)
assert(isstruct(coarseDim) && isstruct(fineDim),'Fine and coarse dimensions must be structured arrays')
requiredFields = {'top','left','cellsizeX','cellsizeY','rows','cols'};
assert(all(ismember(requiredFields,fields(coarseDim))),...
    'Required fields of dimension arrays are: "top", "left", "cellsizeX", "cellsizeY", "rows", "cols"');
assert(all(ismember(requiredFields,fields(fineDim))),...
    'Required fields of dimension arrays are: "top", "left", "cellsizeX", "cellsizeY", "rows", "cols"');
if nargin > 2
    if isinteger(fineGrid)
        assert(any(strcmpi('nan',fields(fineDim))),'Integer grids must have a field "nan" in their dimension array');
    end
    assert(ischar(statType) || iscell(statType),'"statType" array must be a character array or a cell array of characters');
    if ischar(statType)
        statType = {statType};
    end
    assert(ischar(fracHandling) || iscell(fracHandling),'"fracHandling" array must be a character array or a cell array of characters');
    if ischar(fracHandling)
        fracHandling = {fracHandling};
    end
    if (length(fracHandling) == 1) && (length(statType) > 1)
        fracHandling = repmat(fracHandling,length(statType),1);
    end
    assert(length(fracHandling) == length(statType),'fracHandling must be specified for each statType');
    for m = 1:length(fracHandling)
        if isempty(fracHandling{m})
            fracHandling{m} = 'majority';
        else
            assert(any(strcmpi(fracHandling{m},{'any','majority','weight','none'})),...
                'An unrecognized option for fracHandling was specified');
        end
    end
else
    statType = {'lookup'};
end
end

%--------------------------------------------------------------------------
%User-defined statistics functions
%--------------------------------------------------------------------------
function [val] = range(data) %#ok<DEFNU>
val = max(data) - min(data);
end

function [val] = variety(data) %#ok<DEFNU>
val = length(count_unique(data));
end

function [val] = minority(data,weight) %#ok<DEFNU>
[uniques,numDups] = count_unique(data); %#ok<ASGLU>
if nargin == 2
    totalWeight = sum(weight);
    for m = 1:length(uniques)
        numDups(m) = numDups(m) * sum(weight(ismember(data,uniques(m)))) / totalWeight;
    end
end
val = uniques(find(numDups == min(numDups),1,'first'));
end

function [val] = majority(data,weight) %#ok<DEFNU>
[uniques,numDups] = count_unique(data); %#ok<ASGLU>
if nargin == 2
    totalWeight = sum(weight);
    for m = 1:length(uniques)
        numDups(m) = numDups(m) * sum(weight(ismember(data,uniques(m)))) / totalWeight;
    end
end
val = uniques(find(numDups == max(numDups),1,'first'));
end

function [val] = count(data) %#ok<DEFNU>
val = numel(data);
end