function [temp,deltaT] = temp_change_post_phase(temp,qNet,heatCap,density,thick,timestep)
%TEMP_CHANGE_POST_PHASE  Calculates temperature change after phase change.
%   [temp] = temp_change_post_phase(temp,qNet,heatCap,density,thick,timestep)
%
%   Descriptions of Input Variables:
%   temp: temperature of the material (C)
%   qNet: net energy remaining for temperature change (W/m^2)
%   heatCap: heat capacity of the material (J/kg/K)
%   density: density of the material (kg/m^3)
%   thick: thickness of the material (m)
%   timestep: length of the model timestep (s)
%
%   Descriptions of Output Variables:
%   temp: temperature of the material after warming/cooling
%   deltaT: if desired, the temperature change can be output as well
%   
%   Example(s):
%   none
%
%   See also: temp_change_pre_phase phase_change

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-01
% Copyright 2008 Michigan State University.

thermalInertia = heatCap .* density .* thick; %units are J/m2/K
deltaT = qNet * timestep ./ thermalInertia; 
temp = temp + deltaT;