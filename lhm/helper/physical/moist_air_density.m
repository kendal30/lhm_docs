function [densAir] = moist_air_density(params,temperature,airVapPress,airPress)
%MOIST_AIR_DENSITY  Calculates the density of moist air.
%   [densAir] = moist_air_density(temperature,airVapPress,airPress)
%
%   Descriptions of Input Variables:
%   temperature: air temperature, (C)
%   airVapPress: air vapor pressure, (Pa)
%   airPress: air pressure, (Pa)
%
%   Descriptions of Output Variables:
%   densAir: moist air density (kg/m^3)
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

[gasConstDryAir,gasConstWaterVap] = deal_struct(params,'gas_const_dry_air','gas_const_water_vap');

kelvTemp = unit_conversions(temperature,'C','K');
densAir = ((airPress-airVapPress)./(gasConstDryAir * kelvTemp)) + (airVapPress./(gasConstWaterVap * kelvTemp));
