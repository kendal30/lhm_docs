function [waterTemp,rhow] = convective_mixing(waterTemp,rhow,delZ)
%CONVECTIVE_MIXING  Convectively mix deep lakes
%   [waterTemp,rhow] = convective_mixing(waterTemp,delZ)
%
%   Descriptions of Input Variables:
%   waterTemp: temperature of the water, C
%   delZ: layer thicknesses, m
%
%   Descriptions of Output Variables:
%   waterTemp: mixed water temperature, C
%   rhow: density of the water, kg/m^3
%
%   Example(s):
%   >> convective_mixing
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-30
% Copyright 2008 Michigan State University.

numLayers = size(waterTemp,2);

%Now, calculate mixing for instabilities generated by bottom warming
[waterTemp,rhow] = mixing_up(waterTemp,rhow,delZ,numLayers);

%Now, calculate mixing for instabilities generated by surface cooling
[waterTemp,rhow] = mixing_down(waterTemp,rhow,delZ,numLayers);

%Error checking
if assert_LHM
    assert_LHM(all(reshape(rhow(:,1:end-1) <= rhow(:,2:end),[],1)),'Density instabilities still present after mixing')
end

end

function [waterTemp,rhow] = mixing_down(waterTemp,rhow,delZ,numLayers)
ind = (rhow(:,1) > rhow(:,2));
for m = 2:numLayers
    waterTemp(ind,1:m) = repmat(sum(waterTemp(ind,1:m) .* delZ(ind,1:m),2) ./ sum(delZ(ind,1:m),2),[1,m]);
    rhow(ind,1:m) = water_density(waterTemp(ind,1:m));
    if m ~=numLayers
        ind = (rhow(:,m) > rhow(:,m+1));
    end
end
end

function [waterTemp,rhow] = mixing_up(waterTemp,rhow,delZ,numLayers)
ind = (rhow(:,end-1) > rhow(:,end));
for m = numLayers - 1: -1: 1
    waterTemp(ind,m:end) = repmat(sum(waterTemp(ind,m:end) .* delZ(ind,m:end),2) ./ sum(delZ(ind,m:end),2),[1,numLayers-m+1]);
    rhow(ind,m:end) = water_density(waterTemp(ind,m:end));
    if m~=1
        ind = (rhow(:,m-1) > rhow(:,m));
    end
end
end