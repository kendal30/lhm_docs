function [varargout] = van_genuchten_model(theta,thetaSat,thetaResid,alpha,N,kSat,k0,L,soilTemp,refTemp,beta)
%VAN_GENUCHTEN_MODEL  Calculate unsaturated conductivity and suction head (with gradients) using Van Genuchten.
%   [varargout] = van_genuchten_model(theta,thetaSat,thetaResid,alpha,N,kSat,soilTemp,refTemp,beta)
%
%   Descriptions of Input Variables:
%   theta:  current volumeetric moisture (m^3/m^3)
%   thetaSat:   volumetric moisture at saturation (m^3/m^3)
%   thetaResid: residual volumetric saturation (m^3/m^3)
%   alpha:  van Genucthen alpha (1/m)
%   N:      van Genucthen N (unitless)
%   kSat:   saturated conductivity of the soil (m/s)
%   k0:     value of conductivity used in the Maulem-van Genuchten equation
%           for unsaturated conductivity
%   L:      exponent on thetaPrime(see below) used in the Maulem-van
%           Genucthen equation
%   soilTemp:   (optional) actual soil temperature (C)
%   refTemp:    (optional) soil temperature at which corrective parameters
%               were measured (C)
%   beta:   (optional) empirical correction factor used to adjust for soil
%       temperature, (see Bachmann et al. 2002).  Beta can be distributed.
%
%   Descriptions of Output Variables:
%   varargout: 1) unsaturated hydraulic conductivity K, 2)d(K)/d(Theta), and 
%       3) Psi, 4) d(Psi)/d(Theta), or the gradient of conductivity and then 
%       matric potential with soil moisture, respectively.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

smallNum = 1e-8;
delTheta = 1e-14;

%Assign default values of k0 and L
if nargin < 7
    k0 = kSat/10; %from Schaap and Leij 2000
    L = -1; %from Schaap and Leij 2000
end

%precalculate these to save on later computation
thetaPrime = (theta - thetaResid) ./(thetaSat - thetaResid);
thetaPrime(thetaPrime <= 0) = smallNum; %to avoid singularities
thetaPrime(thetaPrime >= (1 - smallNum)) = 1 - smallNum; %to avoid singularities
recipN = 1 ./ N;
M = 1 - recipN;
recipM = (1 ./ M);

%Calculate unsaturated hydraulic conductivity
theta_1 = thetaPrime.^(recipM);
varargout{1} = (kSat.*(thetaPrime) + k0.*(1-thetaPrime)) .* thetaPrime.^(L) .* (1 - (1 - theta_1).^(M)).^(2); %Modified form, must converge to kSat at saturation
if (nargout > 1)
    delThetaPrime = thetaPrime + delTheta;
    theta2 = delThetaPrime.^(recipM);
    delKTheta = k0 .* delThetaPrime.^(L) .* (1 - (1 - theta2).^(M)).^(2);
    varargout{2} = (delKTheta - varargout{1}) / delTheta;
    if (nargout > 2)
        recipAlpha = 1 ./ alpha;
        varargout{3} = -recipAlpha .* (1 ./theta_1 - 1).^(recipN);
        delPsi = -recipAlpha .* (1 ./theta2 - 1).^(recipN);
        varargout{4} = (delPsi - varargout{3}) / delTheta;
        if (nargin > 8) %assume that del_psi_del_theta is unaffected by temperature
            refTemp = unit_conversions(refTemp,'C','K');
            varargout{3} = varargout{3} .* (beta + soilTemp) ./ (beta + refTemp); %From Bachman et al. 2002
        end
    end
end