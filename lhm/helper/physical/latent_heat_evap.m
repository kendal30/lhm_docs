function [latentHeatEvap]=latent_heat_evap(tempWater,satVapPress,gradientVapPress)
%LATENT_HEAT_EVAP  Calculates the latent heat absorbed upon evaporation.
%   latentHeatEvap = heat_heat_evap(tempWater,satVapPress,gradientVapPress)
%
%   This function calculates the latent heat of evaporation using the
%   clausius-clapeyron equation.  If tempWater is the only input, then the
%   function will call sat_vap_pressure to get the saturated vapor pressure
%   and the gradient of vapor pressure.
%
%   Descriptions of Input Variables:
%   tempWater: temperature of the water to be evaporated or sublimated (C)
%   satVapPress: the saturated vapor pressure at the input temperature (Pa)
%   gradientVapPress: the gradient of saturated vapor pressure at the input
%       temperature (Pa/K)
%
%   Descriptions of Output Variables:
%   latentHeatEvap: latent heat absorbed upon evaporation (J/kg)
%
%   Example(s):
%   none
%
%   See also: sat_vap_pressure

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

gasConst = 461.495;

if nargin < 3
    [satVapPress,gradientVapPress] = sat_vap_pressure(tempWater);
end

%Using the Clausius-Clapeyron equation, works for both water and ice
latentHeatEvap = gradientVapPress ./ satVapPress .* gasConst .* unit_conversions(tempWater,'C','K').^2;
