function [qNet,frozenFrac] = phase_change(qNet,frozenFrac,totalWater,heatFusion,densityIce,timestepLength,zerosGrid)
%PHASE_CHANGE  Calculates full/partial phase change from inputs.
%   [qNet,frozenFrac] =
%   phase_change(qNet,frozenFrac,totalWater,heatFusion,densityIce,timestepLength,zerosGrid)
%
%   Descriptions of Input Variables:
%   qNet: balance of heat fluxes remaining after temp_change_pre_phase,
%       units are (W/m^2).  Two combinations of qNet and frozenFrac only should
%       be used for this function.  They are: ((frozenFrac > 0) & (qNet > 0))
%       OR ((frozenFrac < 1) & (qNet < 0)).  In other words some fraction of
%       the litter/soil/snow must be frozen, and excess heat is available for
%       melt, OR, the litter/soil/snow must be incompletely frozen, and energy
%       loss is still required to balance the heat fluxes that will result in
%       more freezing.
%   frozenFrac: fraction of the litter/soil/snow that is frozen
%   totalWater: total water available (m)
%   heatFusion: latent heat of fusion (J/kg)
%   densityIce: density of ice (kg/m^3)
%   timestepLength: length of the model timestep (s)
%   zerosGrid: empty grid with the same size as qNet and frozenFrac
%
%   Descriptions of Output Variables:
%   qNet: remaining heat flux after phase change (W/m^2)
%   frozenFrac: output frozen fraction
%
%   Example(s):
%   >> ind = ((litterFrozenFrac>0) & (qNet>0)) | ((litterFrozenFrac<1) &
%   (qNet<0)); %melting or freezing conditions
%   >> [qNet(ind),litterFrozenFrac(ind)] = phase_change(qNet(ind),litterFrozenFrac(ind),...
%           litterWater(ind),heatFusion,densityIce(ind),timestepLength,zerosGrid(ind));
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

%Calculate indices and signs
phaseSign = sign(qNet);
indFrozen = (phaseSign > 0);
indLiquid = (phaseSign < 0);
%Calculate the fraction of  water in either the liquid or frozen states
phaseFraction = populate_array(zerosGrid,frozenFrac,(1 - frozenFrac),...
    indFrozen,indLiquid);
%Calculate the quantity of water in each state
phaseWater = totalWater .* phaseFraction; 
%energy required to change phase completely
reqdEnergy = phaseSign .* phaseWater .* heatFusion * densityIce; 
%fraction of phase change based on available energy
fracPhaseChange = min(qNet * timestepLength ./ reqdEnergy , 1);
%change in water in each phase
phaseWater = phaseWater .* (1 - fracPhaseChange);
%resultant change in the phase fraction
phaseFraction = phaseWater ./ totalWater;
%Calculate the frozen fraction, essentially this is the reverse of
%populate_array
frozenFrac(indFrozen) = phaseFraction(indFrozen);
frozenFrac(indLiquid) = 1 - phaseFraction(indLiquid);
%Calculate the net energy remaining
qNet = qNet - reqdEnergy .* fracPhaseChange / timestepLength; 
end