function pet = penman_monteith(params,vapPressure,temp,netflux,pressure,rc,ra)
%PENMAN_MONTEITH  Penman-Monteith Potential ET function
%   output = penman_monteith(input)
%
%   Descriptions of Input Variables:
%   vapPressure: air vapor pressure (Pa)
%   temp:       air temp (C)
%   netflux:    net energy flux (W/m^2)
%   pressure:   air pressure (Pa)
%   rc:         canopy/ground resistance (s/m)
%   ra:         aerodynamic resistance (s/m)
%
%   Descriptions of Output Variables:
%   pet:        potential evaporation/transpiration (m/s)
%
%   Example(s):
%   >> penman_monteith
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-13
% Copyright 2008 Michigan State University.

%all inputs are in standard SI units, except temp is in degrees C. relative
%humidity is in % rather than fraction

%stored value of heat_cap_air based is relatively correct (1000 @ 0C and 1010 @ 100C).
%technically, this should be the specific heat of moist air, but the
%difference is very slight, i.e. less than 1%
[cp,densWater] = deal_struct(params,'heat_cap_air','density_water');
%calculate these
[vapSat,delta] = sat_vap_pressure(temp); %saturated vapor pressure in Pa, input in degrees C
rho = moist_air_density(params,temp,vapPressure,pressure); %calculate the density of air as a function of these variables in kg/m^3, inputs in deg. C, Pa, and Pa
lambda = latent_heat_evap(temp,vapSat,delta); %latent heat of vaporization J/kg
psychro_const = 1.61 .* cp .* pressure ./ (lambda); %psychrometric constant Pa/K
nr = (delta .* (netflux)) + rho .* cp .* ((vapSat - vapPressure) ./ ra); %numerator, units are kg^2*K^-1*m*s^-5
dn = lambda .* (delta + psychro_const .* (1 + (rc ./ ra))); %denominator, units are kg*m*s^-4*K^-1
pet = nr ./ dn / densWater; %to convert units of kg*m^-2*s^-1 into units of m/s, divide by the density of water (1000 kg/m^3)
end