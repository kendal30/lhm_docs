function [qPrecip] = precip_heat_flux(params,precip,precipTemp,substrateTemp,phaseFlag)
%PRECIP_HEAT_FLUX  Calculates heat advected by precipitation.
%   qPrecip = precip_heat_flux(massFlux,precipTemp,substrateTemp)
%
%   This function calculates the heat flux associated with precipitation
%   falling onto a substrate.  This works in three steps: 1) It cools/warms
%   the precipitation to the freezing point.  2) It changes the phase of
%   the precipitation, if necessary. 3) It cools/warms the precipitation
%   from the freezing point to the substrate temperature.  If, however,
%   both the substrate and precip are in the same phase everywhere, then
%   the calculation is made in one step.
%
%   Note: Fluxes of heat into the substrate are defined as positive.
%
%   Descriptions of Input Variables:
%   precip: flux of precipitation into the control volume, (m/time).
%       Output energy flux will be in same time units.
%   precipTemp: temperature of the incoming precipitation, (C)
%   substrateTemp: temperature of the substrate onto which the
%       precipitation falls, (C)
%   phaseFlag: will manually force phase change to be inactive if set to
%       false
%
%   Descriptions of Output Variables:
%   qPrecip: heat flux into the substrate, in units of heat
%       capacity and latent heat of fusion fetched from data_controller.  
%       Should be (J/kg/K) and (J/kg).  ALso, time rate is governed by the
%       input precip flux.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

if nargin < 5
    phaseFlag = true;
end

[heatCapIce,heatCapWater,rhoWater,heatFusion] = deal_struct(params,...
    'heat_cap_ice','heat_cap_water','density_water','latent_heat_fusion');

[heatCap,heatPhase] = deal(zeros(size(precip)));
massFlux = precip.* rhoWater;
frozenPrecip = (precipTemp < 0);
frozenSubstrate = (substrateTemp < 0);
test = xor(frozenPrecip,frozenSubstrate);
if any(test) && phaseFlag %Phase change will be necessary, follow this approach
    %Calculate the amount of heat released or absorbed by the precipitation as
    %it cools or warms to freezing
    heatCap = populate_array(heatCap,heatCapIce,heatCapWater,frozenPrecip,~frozenPrecip);
    heatPrecip = heatCap .* (precipTemp - 0);

    %Calculate the amount of heat released or absorbed by the precipitation as
    %it changes phase to match the substrate phase
    heatPhase(test) = heatFusion;
    heatPhase(frozenPrecip) = -heatPhase(frozenPrecip);

    %Calculate the amount of heat released or absorbed by the precipitation as
    %it cools or warms to the substrate temperature from freezing
    heatCap = populate_array(heatCap,heatCapIce,heatCapWater,frozenSubstrate,~frozenSubstrate);
    heatSubstrate =  heatCap .* substrateTemp;
    
    %Now, calculate qPrecip as the sum of all three components
    qPrecip = massFlux .* (heatPrecip + heatPhase + heatSubstrate);
    
else %Phase change will not be necessary, follow this approach
    %Calculate the amount of heat released or absorbed by the precipitation
    %as it cools or warms to the substrate temperature
    heatCap = populate_array(heatCap,heatCapIce,heatCapWater,frozenPrecip,~frozenPrecip); %pick the appropriate heat capacity
    qPrecip = massFlux .* heatCap .* (precipTemp - substrateTemp);
end


