function [qNet,temp] = temp_change_pre_phase(qNet,temp,cp,density,thick,timestep)
%TEMP_CHANGE_PRE_PHASE  Calculates temperature change prior to any phase change.
%   [qNet,temp] = temp_change_pre_phase(qNet,temp,cp,density,thick,timestep)
%
%   Calculates the temperature change as the litter/soil/snow temperature moves toward
%   freezing/melting
%
%   Descriptions of Input Variables:
%   qNet: balance of heat fluxes prior to temperature change, here only
%       two combinations of qNet and temp should be used: ((qNet > 0) & (temp <
%       0)) OR ((qNet < 0) & (temp > 0)).  These are the cases when the
%       temperature is less than freezing, but will warm, and when the
%       temperature is greater than freezing, but will cool.  Units are
%       w/m^2
%   temp: temperature of the litter/soil/snow (C)
%   cp: heat capacity (J/kg/K)
%   density: composite density of the litter/soil/snow (kg/m^3)
%   thick: thickness of the litter/soil/snow (m)
%   timestep: length of the model timestep (s)
%
%   Descriptions of Output Variables:
%   qNet: remaining energy after temperature change (W/m^2)
%   temp: temperature after temperature change (C)
%
%   Example(s):
%   >> ind = ((litterTemp>0) & (qNet<0)) | ((litterTemp<0) & (qNet>0));
%   >> [qNet(ind),litterTemp(ind)] = temp_change_pre_phase(qNet(ind),litterTemp(ind),...
%           cpLitter(ind),densityComposite(ind),litterThick(ind),timestepLength);
%
%   See also: litter_temperature_module snowpack_model phase_change

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

thermalInertia = cp .* density .* thick; %units are J/m2/K
reqdEnergy = -temp.* thermalInertia / timestep; %determine energy loss rate needed to freeze the litter
fracTempChange = min(qNet ./ reqdEnergy,1); %calculate fraction of reqd energy available
temp = temp .* (1 - fracTempChange);  %change temp as fraction of reqd energy available
qNet = qNet - reqdEnergy .* fracTempChange;
end