function [output] = root_depth_model(type,beta,fracOrDepths)
%ROOT_DEPTH_MODEL  Outputs the cumulative root mass at depth, or the inverse.
%   output = root_depth_model(type,beta,fracOrDepths)
%
%   This model comes from Gale and Grigal (1987), see jackson et al 1996 for
%   the reference.  Equation is Y = 1-beta^d, where y = cumulative root mass 
%   fraction, beta is a fitted parameter, d is the depth in cm.
%   
%   Note: the input/output depths must be in meters, the code uses cm
%   internally and will do the conversion itself.
%   
%   Descriptions of Input Variables:
%   type: 'forward', or 'inverse'.  If 'forward', the output will be the
%       fraction of root mass at a depth (in meters), or the fraction within a layer.
%       If 'inverse', the output will be the depth at which a given root
%       mass fraction occurs.
%   beta: landuse-dependent exponent governing exponential root mass
%       behavior
%   fracOrDepths: either a fraction (0-1) array for which a depth array
%       will be output, this is the inverse case; or, an array of depths,
%       which if 1D will output the root mass fraction, or if 2D will
%       output the root mass fraction in each 1D layer.
%
%   Descriptions of Output Variables:
%   output: see above for the 'forward' or 'inverse' case, as it differs.
%       If fractional, values will be from 0-1, and depths will be in meters.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

switch lower(type)
    case 'inverse'
        output = unit_conversions(log(1-fracOrDepths) ./ log(beta),'cm','m');
    case 'forward'
        cumFrac = 1 - beta.^(unit_conversions(fracOrDepths,'m','cm')); 
        output = [cumFrac(:,1),diff(cumFrac,[],2)];
end
        