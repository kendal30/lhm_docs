function [age] = snow_surface_age(age,newSnow,surfTemp,timestepLength)
%SNOW_SURFACE_AGE  Updates the dimensionless age of the snow surface
%   [age] = snow_surface_age(age,newSnow,surfTemp,timestepLength)
%
%   Calculates the dimensionless age of the snow surface.  This is from the
%   UEB snow model by Tarboton and Luce, 1996.  It is used for albedo
%   calculations.
%
%   Descriptions of Input Variables:
%   age: dimensionless age of the snow surface at the previous timestep
%   newSnow: depth of new snow in (m)
%   surfTemp: temperature of the snow surface, the interface temperature
%       (C)
%   timestepLength: length of the modeled timestep (s)
%
%   Descriptions of Output Variables:
%   age: updated dimensionless surface age
%
%   Example(s):
%   >> snow_surface_age
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-02
% Copyright 2008 Michigan State University.

surfTemp = unit_conversions(surfTemp,'C','K');
r1 = exp(5e3 * (1/273.15 - 1./surfTemp));
r2 = r1.^10;
r2(r2>1) = 1;
r3 = 0.3;
tau0 = 1e6; %seconds
age = max((age + timestepLength/tau0 * (r1 + r2 + r3)) .* (1 - 100.*newSnow),0);