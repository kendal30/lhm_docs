function eddyCoeff = eddy_diffusion_coefficient(params,rhow,z,windspeed,latitude)
%EDDY_DIFFUSION_COEFFICIENT  Calculates the eddy diffusion coefficient.
%   eddyCoeff = eddy_diffusion_coefficient(rhow,z,windspeed,latitude)
%
%   Calculates the eddy diffusion coefficient as in Hostetler and Bartlein
%   (1990).
%
%   Descriptions of Input Variables:
%   rhow: density of water, kg/m^3
%   z: lake layer depths, m
%   windspeed: wind speed, m/s
%   latitude: latitude, degrees
%
%   Descriptions of Output Variables:
%   eddyCoeff: eddy diffusion coefficient, 
%
%   Example(s):
%   none
%
%   See also: wetland_temperature_module

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-30
% Copyright 2008 Michigan State University.

[gravConst,vonKarman,neutPrandtl] = deal_struct(params,...
    'grav_constant','von_karman','wetland_neutral_prandtl');

z = cumsum(z,2);
numLayers = size(z,2);

diffRho = diff(rhow,[],2); %take the difference down the layers
diffZ = -diff(z,[],2);
tempDelRho = diffRho ./ diffZ;
delRhoZ = zeros(size(rhow));
%tempDelRho is size(rhow,2)-1, also, want to average between layer differences
delRhoZ(:,1) = tempDelRho(:,1);
delRhoZ(:,end) = tempDelRho(:,end);
delRhoZ(:,2:end-1) = (tempDelRho(:,1:end-1) + tempDelRho(:,2:end)) / 2;
%delRhoZ must be negative going downward
if assert_LHM()
    assert(all(delRhoZ(:)<=0),'Layer instabilities are present that must be taken care of')
end
fricVel = repmat(max(1e-7, 1.2e-3*windspeed),1,numLayers);
ekmanParam = repmat(6.6 * sin(unit_conversions(latitude,'deg','rad')).^(1/2) .* windspeed.^(-1.84),1,numLayers);
bruntVaisalaFreq2 = -gravConst ./ rhow .* delRhoZ;
richardson = (1/20) .* (-1 + sqrt(max(1 + 40 .* bruntVaisalaFreq2 .* vonKarman.^2 .* ...
    z.^2 ./ (fricVel.^2 .* exp(-2 .* ekmanParam .*z )),0)));
eddyCoeff = (vonKarman .* fricVel .* z ./ neutPrandtl) .* exp(-ekmanParam .* z) ./ (1 + 37*richardson.^2);