function qCond = conductive_heat_flux(temp1,temp2,conductance,thickness)
%CONDUCTIVE_HEAT_FLUX  Calculates the conductive heat flux. 
%   qCond = conductive_heat_flux(temp1,temp2,conductance,thickness)
%
%   This function calculates the heat conducted from material 1  to
%   material 2 using the specified conductance and interacting material
%   thickness. 
%
%   Descriptions of Input Variables:
%   temp1: Temperature of material 1, units are defined as outward from
%       material 1. Temperature is (C) or (K), but must be consistent with 
%       temp2.
%   temp2: Temperature of material 2, units are defined as inward to
%       material 2. Temperature is (C) or (K), but must be consistent with
%       temp1.
%   conductance: Thermal conductance of the material interface.  This is
%       typically calculated as the harmonic weighted mean of the respective
%       conductances.  Units are (W/m/K).
%   thickness: Total conductive thickness of the two materials.  Units are
%       (m).
%
%   Descriptions of Output Variables:
%   qCond: Conductive heat flux, units are W/m^2.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-02
% Copyright 2008 Michigan State University.

qCond = conductance .* (temp1 - temp2) ./ thickness;