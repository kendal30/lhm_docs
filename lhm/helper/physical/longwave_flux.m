function qLong = longwave_flux(surfTemp,emissivity)
%LONGWAVE_FLUX  Calculates the flux of longwave radiation from a surface.
%   qLong = longwave_flux(surfTemp,emissivity)
%
%   Descriptions of Input Variables:
%   surfTemp: surface temperature in degrees C
%   emissivity: emissivity of the surface
%
%   Descriptions of Output Variables:
%   qLong: longwave flux in J/m^2/s (W/m^2)
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-07
% Copyright 2008 Michigan State University.

%Declare the lookup table as a persistent variable

%The lookup table is faster
% qLong = 5.6704e-8 * emissivity .* unit_conversions(surfTemp,'C','K').^4;

persistent lookup

if isstruct(lookup)
    valsIndexTemp = uint32((surfTemp - lookup.minVal.temperature) * lookup.mult.temperature) + 1;
    valsIndexEmissivity = uint32((emissivity - lookup.minVal.emissivity)  * lookup.mult.emissivity) + 1;
    valsIndex = sub2ind([lookup.size.temperature,lookup.size.emissivity],valsIndexTemp,valsIndexEmissivity);
    %Lookup the water density
    try
        qLong = lookup.values.qLong(valsIndex);
    catch %#ok<CTCH>
        valsIndex = limit_vals([1,length(lookup.values.qLong)],valsIndex);
        qLong = lookup.values.qLong(valsIndex);
    end
else
    %Create the lookup table
    lookup = lookup_table_creation();
    
    %Run this function again
    [qLong] = longwave_flux(surfTemp,emissivity);
end

end


function [table] = lookup_table_creation()
%Set the temperature range for the lookup table
rangeSpacingTemp = 0.01;
temperature = (-90:rangeSpacingTemp:90);

%Set the emissivity range for the lookup table
rangeSpacingEmissivity = 0.002;
emissivity = (0 : rangeSpacingEmissivity : 1);

%Build the lookup table
table.minVal.temperature = temperature(1);
table.mult.temperature = 1 / rangeSpacingTemp;
table.size.temperature = length(temperature);
table.minVal.emissivity = emissivity(1);
table.mult.emissivity = 1 / rangeSpacingEmissivity;
table.size.emissivity = length(emissivity);

%Convert to K, which is what all these equations work in
temperature = unit_conversions(temperature,'C','K');

%Define the stefan boltzman constant
stefanBoltzmann = 5.6704e-8;

%Build the arrays
emissivity = repmat(emissivity,table.size.temperature,1);
temperature = repmat(temperature',1,table.size.emissivity);

%Finish building the lookup table
table.values.qLong = stefanBoltzmann * emissivity .* temperature.^4;
end