function [qEvap,evap,surfVapPress,qEvap2,evap2,qEvap3,evap3]=latent_flux(params,...
    surfTemp,surfRelHum,airVapPress,airPress,densMoistAir,timestepLength,...
    resistAero,resistCanopy,resistCanopy2,resistCanopy3)
%LATENT_FLUX  Calculates latent heat fluxes.
%   [qEvap,evap,surfVapPress] = latent_flux(evap,surfTemp,surfRelHum,airVapPress,airPress,resistAero,resistCanopy)
%
%   Fluxes are defined as positive upward.
%
%   Descriptions of Input Variables:
%   surfTemp: surface temperature (C)
%   surfRelHum: surface relative humidity (fraction), not percent
%   airVapPress: air vapor pressure (Pa)
%   airPress: air pressure (Pa)
%   densMoistAir: moist air density (kg/m^3)
%   timestepLength: length of the model timestep in (s), to ouptut
%       evaporation
%   resistAero: aerodynamic resistance (s/m)
%   resistCanopy: canopy or surface resistance (s/m)
%   resistCanopy2: an optional specification that will calculate a separate
%       qEvap, useful for calculating PE and PT canopy separately
%   resistCanopy3: an optional specification that will calculate a separate
%       qEvap, useful for calculating PE and PT canopy separately
%
%   Descriptions of Output Variables:
%   qEvap: latent heat flux (W/m^2)
%   evap: evaporation rate (m/m^2/timestep)
%   qEvap2: latent heat flux of the second evaporation/transpiration
%       component, only used if two canopy resistances are given
%   evap2: evaporation rate of the second component
%
%   Example(s):
%   none
%
%   See also: latent_heat_evap sat_vap_pressure sens_flux

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

[densWater,mwWater,mwAir] = deal_struct(params,...
    'density_water','mol_weight_water','mol_weight_dry_air');

%calculate the surface vapor pressure conditions
[surfSatVapPress,gradVapPress] = sat_vap_pressure(surfTemp);

%Calculate the vapor pressure at the surface
surfVapPress = surfSatVapPress .* surfRelHum;

%Calculate the latent heat of evaporation, either vaporization or
%sublimation
latentHeatVap = latent_heat_evap(surfTemp,surfSatVapPress,gradVapPress);

%Calculate the evaporation rate in kg/m^2/s
specHumSurf = 1./(1 + (airPress ./ surfVapPress - 1) * mwAir / mwWater);
specHumAir = 1./(1 + (airPress ./ airVapPress - 1) * mwAir / mwWater);
precalc = densMoistAir .* (specHumSurf - specHumAir);
evap = precalc ./ (resistAero + resistCanopy);

%Calculate the latent heat flux
qEvap = latentHeatVap .* evap;

%Output evap rate in m/m^2/timestep
evap = evap * timestepLength / densWater;

%For additional canopy resistances
if nargin > 9
    evap2 = precalc ./ (resistAero + resistCanopy2);
    qEvap2 = latentHeatVap .* evap2;
    evap2 = evap2 * timestepLength / densWater;
    if nargin > 10
        evap3 = precalc ./ (resistAero + resistCanopy3);
        qEvap3 = latentHeatVap .* evap3;
        evap3 = evap3 * timestepLength / densWater;
    end
end