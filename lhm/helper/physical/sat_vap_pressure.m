function [satVapPress,gradient] = sat_vap_pressure(temperature,equation)
%SAT_VAP_PRESSURE  Calculates the saturated vapor pressure over water or ice.
%   [satVapPress,gradient] = sat_vap_pressure(temperature,equation)
%
%   This function calculates the saturated vapor pressure over water or ice
%   using three different equations: the Goff-Gratch equation, the WMO
%   equation (which is closely-related to Goff-Gratch, and the Buck
%   equation which is a simpler polynomial description.  See
%   http://cires.colorado.edu/~voemel/vp.html
%
%   Descriptions of Input Variables:
%   temperature: air temperature, (C)
%   equation: name of the equation 'goff','wmo', or 'buck'.  'buck' is the
%       default value, as it is the simplest and fastest.
%
%   Descriptions of Output Variables:
%   satVapPress: saturated vapor pressure, (Pa)
%   gradient: gradient of the saturated vapor pressure (Pa/K)
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

%Declare the lookup table as a persistent variable
persistent lookup

%if equation type is not specified, use default value
if (nargin < 2)
    equation = 'goff'; %Even though this is more intensive, the lookup table capability means it's no faster for repeated calls
end

%If the input data are a vector, check to see if the output will have to be
%transposed, as by default the lookup table produces row vectors
transp = false;
if isvector(temperature)
    if size(temperature,2) == 1,transp = true;end
end

%Check to see if a lookup table has already been defined for this equation
if isfield(lookup,equation)
    valsIndex = uint32((temperature - lookup.(equation).minVal) * lookup.(equation).mult) + 1;
    %Lookup the saturated vapor pressure
    try
        satVapPress = lookup.(equation).values.satVapPress(valsIndex);
    catch %#ok<CTCH>
        valsIndex = limit_vals([1,length(lookup.(equation).values.satVapPress)],valsIndex);
        satVapPress = lookup.(equation).values.satVapPress(valsIndex);
    end
    if transp,satVapPress = satVapPress';end
    if nargout == 2
        gradient = lookup.(equation).values.gradient(valsIndex);
        if transp,gradient = gradient';end
    end
else
    %for the specified equation, get the function handle
    switch equation
        case 'goff'
            fhandle = @goff_gratch;
        case 'wmo'
            fhandle = @wmo;
        case 'buck'
            fhandle = @buck;
    end

    %Create the lookup table
    lookup.(equation) = lookup_table_creation(fhandle);
    
    %Run this function again
    if nargout == 1
        [satVapPress] = sat_vap_pressure(temperature,equation);
    else
        [satVapPress,gradient] = sat_vap_pressure(temperature,equation);
    end
end

end

function [table] = lookup_table_creation(fhandle)
%Set the temperature range for the lookup table
rangeSpacing = 0.001;
temperature = (-75:rangeSpacing:99);

%Build the lookup table
table.minVal = temperature(1);
table.maxVal = temperature(end);
table.mult = 1 / rangeSpacing;

%Convert to K, which is what all these equations work in
temperature = unit_conversions(temperature,'C','K');

%use the function handle to calculate satVapPress and the gradient
[satVapPress,gradient] = deal(zeros(size(temperature)));

%Initialize the gradient variables
delta = 1e-5;
deltaTemp = temperature + delta;
deltaPress = satVapPress;

%Liquid water
ind = (temperature >= 273.15);
satVapPress(ind) = fhandle(temperature(ind),'water');
deltaPress(ind) = fhandle(deltaTemp(ind),'water');
gradient(ind) = (deltaPress(ind) - satVapPress(ind)) / delta;
    
%Frozen Water
ind = (~ind);
satVapPress(ind) = fhandle(temperature(ind),'ice');
deltaPress(ind) = fhandle(deltaTemp(ind),'ice');
gradient(ind) = (deltaPress(ind) - satVapPress(ind)) / delta;

%Build the lookup table
table.values.satVapPress = satVapPress;
table.values.gradient = gradient;
end

function [e_out] = goff_gratch(T,phase)
%takes input temperature as Kelvin, outputs saturated vapor pressure in Pa
switch phase
    case 'water'
        a = -7.90298;
        b = 5.02808;
        c = -1.3816 * 10^(-7);
        d = 11.344;
        f = 8.1328* 10^(-3);
        h = -3.49149;
        est = 1013.246; %saturation vapor pressure at steam-point temperature, in hPa (10^2 Pa)
        Ts = 373.16; %steam-point temperature, K
        Tk = Ts./T;
        Z = a*(Tk-1)+b*log10(Tk)+c*(10.^(d*(1-1./Tk))-1)+f*(10.^(h*(Tk-1))-1)+log10(est);
        e_out = unit_conversions(10.^Z,'hPa','Pa'); %saturated vapor pressure in Pa
    case 'ice'
        a = -9.09718;
        b = -3.56654;
        c = 0.876793;
        T0 = 273.16; %ice point temperature
        ei0 = 6.1071; %saturation vapor pressure at ice-point temperature, in hPa (10^2 Pa)
        Tk = T0./T;
        Z = a*(Tk-1)+b*log10(Tk)+c*(1-1./Tk)+log10(ei0);
        e_out = unit_conversions(10.^Z,'hPa','Pa');
end
end

function [e_out] = wmo(T,phase)
%takes input temperature as Kelvin, outputs saturated vapor pressure in Pa
switch phase
    case 'water'
        a = 10.79574;
        b = -5.02800;
        c = 1.50475 * 10^(-4);
        d = -8.2969;
        f = 0.42873 * 10^(-3);
        h = 4.76955;
        k = 0.78614;
        T0 = 273.16; %ice-point temperature, K
        Tk = T0./T;
        Z = a*(1-Tk)+b*log10(1./Tk)+c*(1-10.^(d*(1./Tk-1)))+f*(10.^(h*(1-Tk))-1)+k;
        e_out = unit_conversions(10.^Z,'hPa','Pa'); %saturated vapor pressure in Pa
    case 'ice' %note, using Goff-Gratch as the WMO recommendation is very similar
        a = -9.09718;
        b = -3.56654;
        c = 0.876793;
        T0 = 273.16; %ice point temperature
        ei0 = 6.1071; %saturation vapor pressure at ice-point temperature, in hPa (10^2 Pa)
        Tk = T0./T;
        Z = a*(Tk-1)+b*log10(Tk)+c*(1-1./Tk)+log10(ei0);
        e_out = unit_conversions(10.^Z,'hPa','Pa');
end
end

function [e_out] = buck(T,phase)
%takes input temperature as Kelvin, outputs saturated vapor pressure in Pa
T = unit_conversions(T,'K','C');
switch phase
    case 'water'
        a = 6.1121;
        b = 18.678;
        c = 234.5;
        d = 257.14;
        Z = a*exp((b-T/c).*T./(d+T));
        e_out = unit_conversions(Z,'hPa','Pa');
    case 'ice'
        a = 6.115;
        b = 23.036;
        c = 333.7;
        d = 279.82;
        Z = a*exp((b-T/c).*T./(d+T));
        e_out = unit_conversions(Z,'hPa','Pa');
end
end