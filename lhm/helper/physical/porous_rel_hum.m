function relHum = porous_rel_hum(params,temperature,psi)
%POROUS_REL_HUM  Calculates the relative humidity of porous media.
%   relHum = porous_rel_hum(temperature,psi)
%
%   This particular description of porous relative humidity comes from the
%   Unsat-H 3.0 manual, and it also appears in Ogee and Brunet [2002]
%
%   Descriptions of Input Variables:
%   temperature: local temperature of the porous medium (C)
%   psi: suction head (negative pressure head) in m
%
%   Descriptions of Output Variables:
%   relHum: fractional relative humidity
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[gravConst,gasConstWaterVap] = deal_struct(params,'grav_constant','gas_const_water_vap');

temperature = unit_conversions(temperature,'C','K');

%assumes phase equilibrium between liquid and vapor
relHum = exp(psi * gravConst ./ (gasConstWaterVap  .* temperature));