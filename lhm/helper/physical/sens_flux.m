function [qSens] = sens_flux(params,surfTemp,airTemp,densMoistAir,resistAero)
%SENS_FLUX  Calculates sensible heat flux.
%   [qSens] = sens_flux(surfTemp,airTemp,densMoistAir,resistAero)
%
%   Fluxes are defined as positive upward.
%
%   Descriptions of Input Variables:
%   surfTemp: surface temperature (C)
%   airTemp: air temperature (C)
%   densMoistAir: moist air density (kg/m^3)
%   resistAero: aerodynamic resistance (s/m)
%
%   Descriptions of Output Variables:
%   qSens: sensible heat flux (W/m^2)
%
%   Example(s):
%   none
%
%   See also: latent_heat_evap sat_vap_pressure latent_flux

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

[heatCapAir] = deal_struct(params,'heat_cap_air');

%Calculate the sensible heat flux
qSens = -densMoistAir .* heatCapAir ./ resistAero .* (airTemp - surfTemp);