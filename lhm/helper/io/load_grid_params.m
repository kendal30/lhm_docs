function [output,header] = load_grid_params(filePath,subset)
%--------------------------------------------------------------------------
%Gridded parameter loading function
%--------------------------------------------------------------------------

%Check to see if this is an older .mat format, or the newer .h5
[~,~,ext] = fileparts(filePath);
output = struct();
%Load the data subset
if strcmpi(ext,'.mat')
    data = load(filePath,subset);
    data = data.(subset);

    %Now loop through and save the output grids
    gridNames = fieldnames(data);
    for m = 1:length(gridNames)
        output.(gridNames{m}) = data.(gridNames{m}).data;
        if m == 1
            header = data.(gridNames{m}).header(1); %save out the header information
        end
    end
else
    infoStruct = h5info(filePath,['/',subset]);

    %Loop through the groups, these are individual datasets to load in
    for m = 1:length(infoStruct.Groups)
        gridName = fliplr(strtok(fliplr(infoStruct.Groups(m).Name),'/'));
        output.(gridName) = h5dataread(filePath,[infoStruct.Groups(m).Name,'/',...
            infoStruct.Groups(m).Datasets(1).Name]);
    end

    %Get the header
    header = struct();
    if ~isempty(m)
        if m > 0
            for m = 1:length(infoStruct.Groups(1).Groups)
                groupName = fliplr(strtok(fliplr(infoStruct.Groups(1).Groups(m).Name),'/'));
                if strcmpi(groupName,'header')==1
                    for n = 1:length(infoStruct.Groups(1).Groups(m).Datasets)
                        headerName = infoStruct.Groups(1).Groups(m).Datasets(n).Name;
                        header.(headerName) = h5dataread(filePath,[infoStruct.Groups(1).Groups(m).Name,'/',...
                            infoStruct.Groups(1).Groups(m).Datasets(n).Name]);
                    end
                end
            end
        end
    end
end

end
