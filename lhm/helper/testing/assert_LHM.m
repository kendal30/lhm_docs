function testMode = assert_LHM(varargin)
%ASSERT_LHM  LHM version of assert that can be deactivated.
%   assert_LHM(expression,varargin)
%
%   This function is intended to override the built-in MATLAB assert
%   function with a single purpose: it can be deactivated across the entire
%   suite when not needed.  The intent here is that assert functions can be
%   built in to the LHM code for development purposes.  
%
%   assert_LHM('on') or assert_LHM('off')
%
%   The function is intended to be used as follows:
%   Descriptions of Input Variables: (optional, no input will return test
%   mode)
%   expression: experession that returns a single logical true/false value
%   varargin: options are:
%       errmsg - error mesage to be printed
%       errmsg, value1, value2,... - errmsg with sprintf-style characters
%           to be replaced by value1, value2, ...
%       msgID, errmsg, value1, value2,... - same as above, with an optional
%           error message ID printed as well.  See "help error" for syntax.
%
%   Descriptions of Output Variables:
%
%   Example(s):
%   >> assert_LHM(test > 0, 'Test must be positive');
%   >> testMode = assert_LHM; %testMode will be a logical value
%
%   See also: error_LHM warning_LHM assert error initialize_params

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.


persistent assertSwitch

if isempty(assertSwitch)
    assertSwitch = true;
end

if nargin==0
    testMode = assertSwitch;
    return
end

if ischar(varargin{1})
    switch varargin{1}
        case 'off'
            assertSwitch = false;
        case 'on'
            assertSwitch = true;
        otherwise
            error('Only character arguments to assert_LHM are "on" or "off"');
    end
else
    if assertSwitch
        assert(varargin{:});
    end
end

end %assert_LHM
