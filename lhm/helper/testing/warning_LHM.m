function testMode = warning_LHM(varargin)
%WARNING_LHM  LHM version of warning that can be deactivated.
%   warning_LHM(varargin)
%
%   This function is intended to override the built-in MATLAB warning
%   function with a two purposes: 1) it can be deactivated across the entire
%   suite when not needed, and 2) that it warns with a test that, if failed
%   will produce a warning.
%   
%   The intent here is that warning functions can be
%   built in to the LHM code for development purposes.  But, for performance
%   reasons, the this can be called to turn it on or off
%
%   warning_LHM('on') or warning_LHM('off')
%
%   Descriptions of Input Variables:(optional, no input will return test
%   mode)
%   varargin: options are:
%       
%       message - warning mesage to be printed
%       message, A, B,... - warning with sprintf-style characters
%           to be replaced by A, B, ...
%       msgID, message, A, B,... - same as above, with an optional
%           error message ID printed as well.  See "help warning" for
%           syntax.
%
%   Descriptions of Output Variables:
%
%   Example(s):
%   >> warning_LHM('Value must be positive');
%   >> testMode = warning_LHM; %testMode will be a logical value
%
%   See also: assert error initialize_params
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-19
% Copyright 2008 Michigan State University.

persistent warningSwitch

if isempty(warningSwitch)
    warningSwitch = true;
end

if nargin==0
    testMode = warningSwitch;
    return
end
if ischar(varargin{1})
    switch varargin{1}
        case 'off'
            warningSwitch = false;
        case 'on'
            warningSwitch = true;
        otherwise
            if warningSwitch
                warning(varargin{:});
            end
    end
else
    if warningSwitch
        if ~varargin{1} %this is the test, must pass
            if ischar(varargin{2})
                warning(varargin{2});
            else
                error('Second argument to warning_LHM must be a string')
            end
        end
    end
end


end %warning_LHM