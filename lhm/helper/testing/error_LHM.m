function testMode = error_LHM(varargin)
%ERROR_LHM  LHM version of error that can be deactivated.
%   error_LHM(varargin)
%
%   This function is intended to override the built-in MATLAB error
%   function with a two purposes: 1) it can be deactivated across the entire
%   suite when not needed, and 2) that it warns with a test that, if failed
%   will produce a warning.
%   
%   The intent here is that warning functions can be
%   built in to the LHM code for development purposes.  But, for performance
%   reasons, the this can be called to turn it on or off
%
%   error_LHM('on') or error_LHM('off')
%
%   Descriptions of Input Variables: (optional, no input will return test
%   mode)
%   varargin: options are:
%       errmsg - error mesage to be printed
%       errmsg, value1, value2,... - errmsg with sprintf-style characters
%           to be replaced by value1, value2, ...
%       msgID, errmsg, value1, value2,... - same as above, with an optional
%           error message ID printed as well.  See "help error" for syntax.
%
%   Descriptions of Output Variables:
%
%   Example(s):
%   >> error_LHM('Value must be positive');
%   >> testMode = error_LHM; %testMode will be a logical value
%
%   See also: assert_LHM warning_LHM error initialize_params

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

persistent errorSwitch

if isempty(errorSwitch)
    errorSwitch = true;
end

if nargin==0
    testMode = errorSwitch;
    return
end

if ischar(varargin{1})
    switch varargin{1}
        case 'off'
            errorSwitch = false;
        case 'on'
            errorSwitch = true;
        otherwise
            if errorSwitch
                error(varargin{:});
            end
    end
else
    if errorSwitch
        if ~varargin{1} %this is the test, must pass
            if ischar(varargin{2})
                error(varargin{2});
            else
                error('Second argument to error_LHM must be a string')
            end
        end
    end
end


end %error_LHM