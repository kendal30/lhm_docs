function updateStruct = update_struct(updateStruct,varargin)
%UPDATE_STRUCT  Compactly updates select fields of a structured array
%   outputStruct = update_struct(inputStruct,varargin)
%
%   Note, this function does not have any error checks for performance
%   purposes.
%
%   Descriptions of Input Variables:
%   updateStruct: The input structured array to update select fields
%   varargin: List of character field names alternating with contents of
%               field to update, i.e. 'field1',field1Val,'field2',field2Val
%
%   Descriptions of Output Variables:
%   updateStruct: Updated structured array
%
%   Example(s):
%   >> update_struct
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2011-0901
% Copyright 2011 Michigan State University.

numFields = length(varargin)/2;
for m = 1:numFields
    updateStruct.(varargin{m*2-1}) = varargin{m*2};
end
