function buffers = save_checkpoint(state,structure,params,buffers,groundwater,modules,outPaths,crash_state)
%Flush the output buffers
buffers = groundwater_coupling_output(state,structure,buffers,'save');
buffers = output_controller(state,structure,buffers,'save');

%Get the output filenames
dumpFile = outPaths.dump;
outFile = outPaths.output;

%Copy the previous dumpFile to a backup
[dumpdir,dumpFilename,dumpFileext] = fileparts(dumpFile);
if exist(dumpFile,'file')
    lastFile = [dumpdir,filesep,'last_',dumpFilename,dumpFileext];
    if exist(lastFile,'file')
        delete(lastFile);
    end
    copyfile(dumpFile,lastFile);
    delete(dumpFile);
end

%Dump the data controllers to file
state = update_struct(state,'crash_save',crash_state); %indicate whether the save resulted from a crash
save(dumpFile,'state','structure','params','buffers','groundwater','modules','-v7.3');

%Save a copy of the OUTF file for intermediate reading
[outdir,outFilename,outFileext] = fileparts(outFile);
lastFile = [outdir,filesep,'last_',outFilename,outFileext];
if exist(lastFile,'file')
    delete(lastFile);
end
copyfile(outFile,lastFile);

%Turn the diary off and on to force a write to disk
diary off
diary on