function buffers = output_controller(state,structure,buffers,varargin)
% This function serves to accumulate model state variables in a useful way
% while greatly reducing the quantity of information that must be stored.
% 
% There are several possible modes:
%   - normal mode, with three input variables: state, structure, and buffers
%   - other modes, where the first input is a string:
%       - 'save': saves the output buffer to disk, but not the temporal aggregation data
%       - 'reload': updates the function handles to overload MATLAB functions on a restart
%       - 'timestamp':  writes the model run time to the output file

% Load the initialization structure from the buffer
paths = deal_struct(structure,'paths');
outBuffer = deal_struct(buffers,'output');

if nargin==3
    % Load a few variables from the data controller
    [wetlandFrac,uplandFrac,streamFrac] = deal_struct(state,...
        'fraction_wetland','fraction_upland','fraction_stream');
    
    % For now, add the stream and wetland fractions
    wetlandFrac = wetlandFrac + streamFrac;
    
    % Run the buffer update function
    outBuffer = update_buffer(state,outBuffer,paths,wetlandFrac,uplandFrac,streamFrac);
    
elseif ischar(varargin{1})
    mode=varargin{1};
    switch lower(mode)
        case 'timestamp'
            % Write the model run time to the output file
            h5varput(paths.output.output,'/info/run_date',datestr(varargin{2},'yyyy-mm-dd HH:MM:SS'));
        case 'reload'
            % Update the function handles
            outBuffer = update_function_handles(outBuffer);
        case 'save'
            % This will flush buffers to disk, but not the values stored
            % within an interval (for grid values, only)
            
            % Save the buffers to disk
            outBuffer = save_buffer(outBuffer,paths);
        otherwise
            error('Invalid input argument(s)');
    end
else
    error('Invalid input argument(s)');
end

% Update the buffer data controller
buffers = update_struct(buffers,'output',outBuffer);

end

%--------------------------------------------------------------------------
% Overloaded calculation functions
%--------------------------------------------------------------------------
% Mean is here to match the call structure of max, min, and std.
function y = mean(x,dummy,dim) % #ok<INUSL,DEFNU>
    y = sum(x,dim)/size(x,dim);
end

% This function simply outputs the value of the input, for no spatial/temporal aggregation
function y = value(x)
    y = x;
end

%--------------------------------------------------------------------------
% Update function handles to overload functions internally
%--------------------------------------------------------------------------
function outBuffer = update_function_handles(outBuffer)
% Loop through the output types
outTypes = fieldnames(outBuffer);
for m = 1:length(outTypes)
    % Now list the output dataset groups
    outSets = fieldnames(outBuffer.(outTypes{m}));
    for n = 1:length(outSets)
        datasets = outBuffer.(outTypes{m}).(outSets{n}).internal.outputs;
        stats = outBuffer.(outTypes{m}).(outSets{n}).internal.stats;
        
        % Loop through the datasets updating the function handles
        for o = 1:length(datasets)
            dataset = outBuffer.(outTypes{m}).(outSets{n}).(datasets{o});
            for p = 1:length(stats)
                func = char(dataset.(stats{p}).fhandle);
                dataset.(stats{p}).fhandle = str2func(func);
            end
            % Update the dataset in the outBuffer array
            outBuffer.(outTypes{m}).(outSets{n}).(datasets{o}) = dataset;
        end
    end
end
end

%--------------------------------------------------------------------------
% Write buffer function
%--------------------------------------------------------------------------
function buffer = write_buffer(tempGroup,buffer,attributes,buffSize,currRowHDF5,outFile)
% Write out the dataset
h5datawrite(outFile,tempGroup,buffer,attributes,false,...
    [currRowHDF5-1,zeros(1,length(buffSize)-1)],buffSize,ones(size(buffSize)));
% Reset the buffer
buffer(:) = 0;
end

%--------------------------------------------------------------------------
% Save buffer function
%--------------------------------------------------------------------------
function outBuffer = save_buffer(outBuffer,paths)
% Loop through the output types
outTypes = fieldnames(outBuffer);
for m = 1:length(outTypes)
    % Now list the output dataset groups
    outSets = fieldnames(outBuffer.(outTypes{m}));
    for n = 1:length(outSets)
        internal = outBuffer.(outTypes{m}).(outSets{n}).internal;
        datasets = internal.outputs;
        stats = internal.stats;
        
        % Determine the current buffer row
        currBufferRow = internal.lastBufferRow;
        
        % Check to see if this set needs to be run at all, continue to next
        % set if not
        if ~(currBufferRow == 0)
            % Reset the buffer counters
            internal.lastBufferRow = 0;
            
            % Update the HDF5 row counters
            currRowHDF5 = internal.lastRowHDF5 + 1;
            internal.lastRowHDF5 = internal.lastRowHDF5 + currBufferRow;
            
            % Update the buffer
            outBuffer.(outTypes{m}).(outSets{n}).internal = internal;
            
            % Loop through the datasets calculating the new values
            for o = 1:length(datasets)
                dataset = outBuffer.(outTypes{m}).(outSets{n}).(datasets{o});
                for p = 1:length(stats)
                    buffer = dataset.(stats{p}).buffer;
                    % Flush to disk
                    buffer(1:currBufferRow,:,:) = write_buffer(dataset.(stats{p}).hdf5loc,...
                        buffer(1:currBufferRow,:,:),dataset.(stats{p}).attributes,...
                        [currBufferRow,dataset.(stats{p}).buffSize(2:end)],currRowHDF5,paths.output.output);
                    % Update the buffer in the dataset array
                    dataset.(stats{p}).buffer = buffer;
                end
                % Update the dataset in the outBuffer array
                outBuffer.(outTypes{m}).(outSets{n}).(datasets{o}) = dataset;
            end
        end
    end
end
end

%--------------------------------------------------------------------------
% Update buffer function - Main data processing function
%--------------------------------------------------------------------------
% Loop through the output types
function outBuffer = update_buffer(state,outBuffer,paths,wetlandFrac,uplandFrac,streamFrac)
outTypes = fieldnames(outBuffer);
for m = 1:length(outTypes)
    % Now list the output dataset groups
    outSets = fieldnames(outBuffer.(outTypes{m}));
    for n = 1:length(outSets)
        internal = outBuffer.(outTypes{m}).(outSets{n}).internal;
        datasets = internal.outputs;
        stats = internal.stats;
        
        % Determine if this timestep is to be written
        if internal.outputTimestep(state.timestep) 

            % Check to see whether the data will be flushed to the buffer after calculation
            % Handle temporally-aggregated and non-aggregated data separately
            switch outTypes{m}
                case {'grid','station'}
                % Increment counter
                intervalStep = internal.lastIntervalStep + 1;
                % Determine the current interval position, flag for flush to buffer
                if intervalStep == internal.intervalLength(internal.currInterval)
                    % Set the flag to flush data to the buffer
                    flushData = true;
                    % Reset counters
                    internal.lastIntervalStep = 0;     
                else
                    flushData = false;
                    % Increment the interval step
                    internal.lastIntervalStep = intervalStep;
                end
                % Set the number of buffer rows calculated this timestep to 1
                numBufferRows = 1;

            case {'zone'}
                % Set the flag to flush data to the buffer
                flushData = true;
                %Set the number of buffer rows calculated this timestep to 1
                numBufferRows = 1;

            case {'point'}
                % Set the flag to flush data to the buffer
                flushData = true;
                % Determine numBufferRows as the number of points to be output this interval
                numBufferRows = sum(internal.pointsTimestep == state.timestep);
            end
        
            % Handle cases where data is to be flushed to buffer
            if flushData
                % Calculate currBufferRow
                lastBufferRow = internal.lastBufferRow; %only used for points
                currBufferRow = internal.lastBufferRow + numBufferRows;

                % Test to see if buffer will be flushed to disk after data is calculated
                testFullBuffer = (currBufferRow == internal.maxBufferRows);
                testLastInterval = (internal.currInterval == length(internal.intervalLength));
                
                if ~testLastInterval %note, points and zones will always flush to buffer
                    % Increment current interval for the next time
                    internal.currInterval = internal.currInterval + 1;
                end
                
                % Check to see if the next timestep will be output, if not flag hiatus
                if internal.outputTimestep(state.timestep) % assure that the current timestep is being written
                    % check next timestep
                    testOutputHiatus = ~internal.outputTimestep(state.timestep + 1);
                else
                    testOutputHiatus = true;
                end
                

                % If any of the flush conditions are met, set flushBuffer flag
                flushBuffer = false;
                if  testFullBuffer || testLastInterval || testOutputHiatus
                    % Set the flag to flush buffer to disk
                    flushBuffer = true;
                    % Determine the HDF5 row to start
                    currRowHDF5 = internal.lastRowHDF5 + 1;
                    % Update the lastRowHDF5 variable
                    internal.lastRowHDF5 = internal.lastRowHDF5 + currBufferRow;
                    % Reset buffer row counter
                    internal.lastBufferRow = 0;
                else
                    % Update the incremented buffer row
                    internal.lastBufferRow = currBufferRow;
                end
            end

            % Update the internal structure in the buffer
            outBuffer.(outTypes{m}).(outSets{n}).internal = internal;
            
            % Test to see if the requested output variables are present in
            % the state array
            testFields = ismember(datasets,fieldnames(state));
            datasets = datasets(testFields);

            % Load the required datasets
            data = deal_struct(state,datasets{:});
            if ~isstruct(data) % only happens if there's just one variable in an output set
                origData = data;
                data = struct();
                data.(datasets{1}) = origData;
            end
            
            % For zones, determine which zones to calculate, empty zones will be
            % ignored
            if strcmpi(outTypes{m},'zone')
                activeZones = find(any(internal.lookup > 0,1));
            end
            
            % Loop through the datasets calculating the new values
            prevNumLayers = 0;
            for o = 1:length(datasets)
                % Determine the number of layers in the ouput dataset, will be
                % used to determine if a new domain fraction grid must be
                % composited
                numLayers = size(data.(datasets{o}),2);
                
                % Extract the current dataset buffer from the overall buffer,
                % just for convenience
                dataset = outBuffer.(outTypes{m}).(outSets{n}).(datasets{o});
                
                % Recalcluate new upland and wetland fractions, if necessary
                % This is an expensive step
                if internal.rescaleFlag(o) && (prevNumLayers ~= numLayers)
                    prevNumLayers = numLayers;
                    loopWetlandFrac = repmat(wetlandFrac,1,numLayers);
                    loopUplandFrac = repmat(uplandFrac,1,numLayers);
                    loopStreamFrac = repmat(streamFrac,1,numLayers);
                end
                
                % Run differently, depending on which type
                switch outTypes{m}
                    case 'grid'
                        % Use only the required data using the lookup table
                        loopData = data.(datasets{o})(internal.lookup,:);
                        % Rescale if necessary
                        if internal.rescaleFlag(o)
                            if internal.domainFlag(o) == 2 % wetlands
                                loopData = loopData .* loopWetlandFrac(internal.lookup,:);
                            elseif internal.domainFlag(o) == 1 % uplands
                                loopData = loopData .* loopUplandFrac(internal.lookup,:);
                            elseif internal.domainFlag(o) == 0 % streams
                                loopData = loopData .* loopStreamFrac(internal.lookup,:);
                            end
                        end
                        % For each statistic, calculate the value
                        for p = 1:length(stats)
                            if intervalStep == 1 % For the first interval step, initialize with data
                                dataset.(stats{p}).data = loopData;
                            else % Not first interval step, calculate running grid values
                                dataset.(stats{p}).data = dataset.(stats{p}).fhandle(loopData,dataset.(stats{p}).data);
                            end
                        end
                    case 'station'
                        % Use only the required data using the lookup table
                        loopData = data.(datasets{o})(internal.lookup,:);
                        % Rescale if necessary
                        if internal.rescaleFlag(o)
                            if internal.domainFlag(o) == 2 % wetlands
                                loopData = loopData .* loopWetlandFrac(internal.lookup,:);
                            elseif internal.domainFlag(o) == 1 % uplands
                                loopData = loopData .* loopUplandFrac(internal.lookup,:);
                            elseif internal.domainFlag(o) == 0 % streams
                                loopData = loopData .* loopStreamFrac(internal.lookup,:);
                            end
                        end
                        % For each statistic, calculate the value
                        for p = 1:length(stats)
                            if intervalStep == 1 % For the first interval step, initialize with data
                                dataset.(stats{p}).data = loopData;
                            else % Not first interval step, calculate running grid values
                                dataset.(stats{p}).data = dataset.(stats{p}).fhandle(loopData,dataset.(stats{p}).data);
                            end
                        end
                    case 'zone'
                        for p = activeZones
                            % Use the lookup table to sub-select data
                            loopData = data.(datasets{o})(internal.lookup(:,p),:);
                            % Rescale if necessary
                            if internal.rescaleFlag(o)
                                if internal.domainFlag(o) == 2 % wetlands
                                    loopData = loopData .* loopWetlandFrac(internal.lookup(:,p),:);
                                elseif internal.domainFlag(o) == 1 % uplands
                                    loopData = loopData .* loopUplandFrac(internal.lookup(:,p),:);
                                elseif internal.domainFlag(o) == 0 % streams
                                    loopData = loopData .* loopStreamFrac(internal.lookup(:,p),:);
                                end
                            end
                            % For each statistic, calculate the value
                            for q = 1:length(stats)
                                dataset.(stats{q}).data(p,:) = dataset.(stats{q}).fhandle(loopData,[],1);
                            end
                        end
                    case 'point'
                        % Use only the required data using the lookup table
                        loopData = data.(datasets{o})(internal.lookup,:);
                        % Rescale if necessary
                        if internal.rescaleFlag(o)
                            if internal.domainFlag(o) == 2 % wetlands
                                loopData = loopData .* loopWetlandFrac(internal.lookup,:);
                            elseif internal.domainFlag(o) == 1 % uplands
                                loopData = loopData .* loopUplandFrac(internal.lookup,:);
                            elseif internal.domainFlag(o) == 0 % streams
                                loopData = loopData .* loopStreamFrac(internal.lookup,:);
                            end
                        end
                        % For each statistic, calculate the value
                        for p = 1:length(stats)
                            if intervalStep == 1 % For the first interval step, initialize with data
                                dataset.(stats{p}).data = loopData;
                            else % Not first interval step, calculate running grid values
                                dataset.(stats{p}).data = dataset.(stats{p}).fhandle(loopData,dataset.(stats{p}).data);
                            end
                        end
                end
                
                % Now, flush data to buffer, and buffer to disk if necessary
                for p = 1:length(stats)
                    % Flush to buffer, if necessary
                    if flushData
                        thisBuffer = dataset.(stats{p}).buffer;

                        % Add data to the buffer, but first reshape it, and then
                        % scale, offset, and transform it
                        switch outTypes{m}
                            case {'grid','station','zone'}
                                thisBuffer(currBufferRow,:,:) = h5scale_offset_transform(...
                                    reshape(dataset.(stats{p}).data,[1,dataset.(stats{p}).buffSize(2:end)]),...
                                    dataset.(stats{p}).attributes,'forward');
                            case 'point'
                                % Points are different because they can write to multiple rows at the same time
                                thisBuffer(lastBufferRow+1:currBufferRow,:,:) = h5scale_offset_transform(...
                                    reshape(dataset.(stats{p}).data,[currBufferRow,dataset.(stats{p}).buffSize(2:end)]),...
                                    dataset.(stats{p}).attributes,'forward');
                                error('Point data not yet tested')
                        end

                        % Flush to disk, if necessary
                        if flushBuffer
                            thisBuffer(1:currBufferRow,:,:) = write_buffer(dataset.(stats{p}).hdf5loc,...
                                thisBuffer(1:currBufferRow,:,:),dataset.(stats{p}).attributes,...
                                [currBufferRow,dataset.(stats{p}).buffSize(2:end)],currRowHDF5,paths.output.output);
                        end
                        % Update the buffer in the dataset array
                        dataset.(stats{p}).buffer = thisBuffer;
                    end
                end
                % Update the dataset in the outBuffer array
                outBuffer.(outTypes{m}).(outSets{n}).(datasets{o}) = dataset;
            end
        end
    end
end
end