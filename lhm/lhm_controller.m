function lhm_controller(runType,initFile,inputPath)
% lhm_controller  Runs LHM, controls model sequence
%   lhm_controller(inputFile,respawnFlag,inputPath)
% 
%   Descriptions of Input Variables:
%   runType:        A string indicating: 'start', 'respawn', or 'join'
%   initFile:       Complete path to the initialization/respawn file
%   inputPath:      If not a respawn, path to input files
% 
%   Example(s):
%   >> lhm_controller
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

% Parse inputs
assert(ischar(runType),'First argument must be the run type');
if nargin >= 2
    switch lower(runType)
        case 'respawn'
            respawnFlag = true;
            joinFlag = false;
        case 'start'
            respawnFlag = false;
            joinFlag = false;
            assert(nargin == 3, 'User must specify the path to the input files on start');
            assert(ischar(initFile), 'Initialization file must be a string');
            assert(ischar(inputPath), 'Input path name must be a string');
        case 'join'
            respawnFlag = true;
            joinFlag = true;
        otherwise
            error('Unrecognized run type, see HELP');
    end
else
    error('At least two inputs are required, see HELP');
end

% Determine whether this is a model split, and get the split num
[initPath,initFilename,~] = fileparts(initFile);
if strcmp(initFilename(1:5),'split')
    splitFlag = true;
    splitNum = str2num(fliplr(strtok(fliplr(initPath),'_'))); %#ok<ST2NM>
    groundwater = false; % for now, no groundwater run in the split
else
    splitFlag = false;
    splitNum = 1;
end

%--------------------------------------------------------------------------
% Load initialization variables, add paths, declare module function handles
%--------------------------------------------------------------------------
% Print to screen, prepare desktop environment
clc
disp('Loading initialization/restart data')
more off
diary off

% Load the initialization file
addpath(pwd); % adds the source path, need to add this here for function handles
cd ..
basePath = pwd;
cd(initPath) % go up to directory containing the init file
load(initFile); %#ok<LOAD>

% Handle paths and generate run time and matlab version
if ~respawnFlag
    % Initialize paths
    paths = initialize_run_paths(inputPath,initPath,basePath,structure.paths); %#ok<*NODEF> 
    structure.paths = paths;

    % Generate the official run time and matlab version
    runTime = now;
    structure.model_run_time = runTime;
    structure.matlab_version = version;

    % Add split information
    structure.split_flag = splitFlag;
    structure.split_num = splitNum;
    
    % Write the start file
    write_status(paths,'start');
else
    paths = structure.paths;
end

%--------------------------------------------------------------------------
% First-time/Restart operations
%--------------------------------------------------------------------------
% Reload function handles in the output controller to handle overloads
buffers = output_controller(state,structure,buffers,'reload');

% Turn on the diary to record screen output
diary(paths.output.diary);

% Set up the test functions
if ~structure.test_mode
    assert_LHM('off');
    warning_LHM('off');
    error_LHM('off');
end

% Handle splits
if structure.num_splits > 1 && ~joinFlag && ~splitFlag
    save_checkpoint(state,structure,params,buffers,groundwater,modules,paths.output,false);
    [modules] = split_controller(structure,modules,false); % this will invoke the splits and then exit
end
    
if ~respawnFlag % First-time run
    % Write the run timestamp to the output file
    buffers = output_controller(state,structure,buffers,'timestamp',runTime);

    % save a checkpoint here in order to be able to respawn
    save_checkpoint(state,structure,params,buffers,groundwater,modules,paths.output,false);
elseif joinFlag % this is the base model being called after split finished
    [modules] = split_controller(structure,modules,joinFlag);
else % This is a restart
    % Backup the current initfile
    [initdir,initfilename,initfileext] = fileparts(initFile);
    respawnFile = [initdir,filesep,'respawn_',initfilename,initfileext];
    if exist(respawnFile,'file')>0
        delete(respawnFile);
    end
    copyfile(initFile,respawnFile);
end

%--------------------------------------------------------------------------
% Model execution
%--------------------------------------------------------------------------
% Get the module functions
[mod_surface_controller,mod_groundwater_controller] = deal_struct(modules,...
    'mod_surface_controller','mod_groundwater_controller');

% Run the surface controller, will be skipped in join mode
[state,structure,params,groundwater] = mod_surface_controller(state,structure,...
    params,groundwater,buffers,modules);

% Run the groundwater controller
mod_groundwater_controller(state,structure,params,groundwater);

%--------------------------------------------------------------------------
% Finish up
%--------------------------------------------------------------------------
% Model is finished
[runName,groupName] = deal_struct(structure,'run_name','group_name');
if splitFlag
    fprintf('Split %d of LHM run %s of group %s is finished\n',splitNum,runName,groupName);
else
    fprintf('LHM run %s of group %s is finished\n',runName,groupName);
end

% Write the complete file
write_status(paths,'complete');

% Turn the diary off
diary off

% Run the lhm_controller in join mode, if split
if splitFlag && ~joinFlag
    run_join(paths);
elseif splitFlag
    % Will only reach this after done joining
    save_checkpoint(state,structure,params,buffers,groundwater,modules,paths.output,false);
end

end % End main


%--------------------------------------------------------------------------
% Internal Functions
%--------------------------------------------------------------------------
function write_status(paths,status)
%status must be either 'start' or 'complete'
fid = fopen(paths.output.(status),'wt');
fprintf(fid,'%s',datestr(now));
fclose(fid);
end


function [paths] = initialize_run_paths(inputPath,outputPath,basePath,paths)
% Build the input file names
fileList = fieldnames(paths.input);
for m = 1:length(fileList)
    paths.input.(fileList{m}) = [inputPath,filesep,paths.input.(fileList{m})];
end

% Build the output file names
fileList = fieldnames(paths.output);
for m = 1:length(fileList)
    paths.output.(fileList{m}) = [outputPath,filesep,paths.output.(fileList{m})];
end

% Add the groundwater and source paths
paths.source = [basePath,filesep,paths.subdirs.source];
paths.output.groundwater = [basePath,filesep,paths.subdirs.groundwater];

% Add the base paths
paths.input.dir = inputPath;
paths.output.dir = outputPath;
end


function run_join(paths)
disp('Running the join operation');

% Get the DUMP file of the base run
baseInit = dir('../INIT*.mat');

% Now, run lhm_controller in join mode
cd(paths.source) % always run lhm_controller from the source directory
lhm_controller('join',[baseInit.folder,filesep,baseInit.name]);
end