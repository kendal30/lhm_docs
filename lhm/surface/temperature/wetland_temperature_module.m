function state = wetland_temperature_module(state,structure,params)
%WETLAND_TEMPERATURE_MODULE  Calculates shallow and deep wetland temperatures
%   wetland_temperature_module()
%
%   This function calculates shallow and deep lake temperatures, along with
%   heat conduction to lake bed sediments.  Currently, it neglects the heat
%   flux from groundwater.
%
%   Descriptions of Input Variables:  
%   All inputs are fetched from the data controller
%
%   Descriptions of Output Variables:
%   All outputs are written to the data controller
%
%   Example(s):
%   none
%
%   See also: 

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-06-31
% Copyright 2008 Michigan State University.

[radTransIce,radShort,qCondInterf,...
    qCondIce,densityWetland,waterTemp,iceMelt,...
    bedTemp,interfTemp,qPhaseOpen,...
    windspeed,totalPrecip,wetlandFrac,streamFrac,streamVelocity] = ...
    deal_struct(state,...
    'rad_transmit_ice_pack','rad_short_wetland_net','q_cond_interf_wetland',...
    'q_cond_ice_pack','wetland_water_density','temp_wetland','ice_pack_melt',...
    'temp_bed_sediment','temp_interf_wetland','q_phase_wetland',...
    'windspeed','total_throughfall','fraction_wetland','fraction_stream','stream_velocity');
[cpWater,heatFusion,molDiffWater,lightAttenCoeff,...
    interfShortFrac,densityIce,sensCoeff,condBed]=...
    deal_struct(params,...
    'heat_cap_water','latent_heat_fusion','diff_coeff_water','wetland_light_atten_coeff',...
    'wetland_light_interf_absorb','density_ice','stream_sensible_coeff','bed_sediment_therm_cond');
[zerosGrid,zerosLakeGrid,indShallow,indDeep,...
    layerDepth,layerThick,timestepLength,indWater,...
    indIce,indOpen,latitude,bedThick,indStream,numLayers] =...
    deal_struct(structure,...
    'zeros_grid','zeros_grid_wetland','index_shallow_wetland','index_deep_wetland',...
    'wetland_depth','thickness_wetland','timestep_length','index_wetland',...
    'index_ice_pack','index_open_water','latitude','thickness_bed_sediment',...
    'index_stream','num_layers_wetland');

%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[eddyCoeff,radAbsorb,freezingWater,outLight,sumDiffusionCoeff,bulkCoeff] = deal(zerosLakeGrid);
outLight(:,end+1) = zerosGrid;  %add another layer
[qNetShallow,frozenFrac,newIce,advectTemp,advectRate,botEnergy,qAdvect,...
    surfEnergy,radTransmit,radNet]  = deal(zerosGrid);

%--------------------------------------------------------------------------
%Calculate the total diffusion coefficient
%--------------------------------------------------------------------------
%Calculate the eddy diffusion coefficient
ind = (indOpen) & (indDeep);
if any(ind)
    eddyCoeff(ind,:) = eddy_diffusion_coefficient(params,densityWetland(ind,:),layerDepth(ind,:),...
        windspeed(ind),latitude(ind));
end

%Calculate the bulk energy transfer coefficient for streams from Patterson and Hamblin, 
%currently these are all shallow so only 1 layer, 
%The effective bulk coefficient depends on the stream fraction, so take the weighted mean.  
%Bulk coefficient in non-streams is zero, in partial
%stream/wetland cells is a mix of 0 and the full bulk transfer coefficient
ind = (indStream);
bulkStream = sensCoeff * layerDepth(ind,1) .* streamVelocity(ind);
bulkCoeff(ind,1) = weighted_mean('arithmetic',bulkStream,0,streamFrac(ind),wetlandFrac(ind));

%Calculate the total diffusion coefficient
sumDiffusionCoeff(indWater,:) = molDiffWater ./ densityWetland(indWater,:) + ...
    eddyCoeff(indWater,:) + bulkCoeff(indWater,:);

%--------------------------------------------------------------------------
%Calculate Energy Fluxes
%--------------------------------------------------------------------------
%Calculate absorbtion of shortwave radiation by each layer
radShort(indOpen) = radShort(indOpen) .* (1 - interfShortFrac); %subtract the part already absorbed by the interface
radNet = populate_array(radNet,radShort,radTransIce,indOpen,indIce);
%Use exponential decay, Beer's Law, see Hostetler and Bartlein
%Deep Lakes
outLight(:,1) = 1; %by definition all light enters the top of layer 1
outLight(indDeep,2:end) = exp(-lightAttenCoeff .* (layerDepth(indDeep,:) + layerThick(indDeep,:)/2));
radAbsorb(indDeep,:) = (outLight(indDeep,1:end-1) - outLight(indDeep,2:end)) .* repmat(radNet(indDeep),[1,numLayers]);
%Shallow Lakes
outLight(indShallow,2) = exp(-lightAttenCoeff .* (layerDepth(indShallow,1) + layerThick(indShallow,1)/2));
radAbsorb(indShallow,1) = (outLight(indShallow,1) - outLight(indShallow,2)) .* radNet(indShallow);

%Calculate the radiation transmitted to the sediments
radTransmit = populate_array(radTransmit,outLight(:,end),outLight(:,2),...
    indDeep,indShallow) .* radNet; 

%Calculate the rate of advected energy
advectTemp = populate_array(advectTemp,0,interfTemp,indIce,indOpen);
advectRate = populate_array(advectRate,iceMelt,totalPrecip,indIce,indOpen)/timestepLength;
qAdvect(indWater) = precip_heat_flux(params,advectRate(indWater),advectTemp(indWater),waterTemp(indWater,1));

%Calculate the net surface energy flux
surfEnergy(indIce) = -qCondIce(indIce) - qAdvect(indIce); %qCondIce is positive into the lake
surfEnergy(indOpen) = -qCondInterf(indOpen) - qAdvect(indOpen) + ...
    qPhaseOpen(indOpen); %qCondInterf is positive into the lake
%surfEnergy is positive outward

%Calculate conduction of energy into the lake bed sediments
%Calculate the thermal diffusion coefficient (thermal conductance)
thermDiffCoeff = sumDiffusionCoeff .* densityWetland / cpWater;

%Deep water cells (different only in that shallow water only is a single
%layer
condLakeBed = weighted_mean('harmonic',thermDiffCoeff(indDeep,end),condBed(indDeep),...
    layerThick(indDeep,end),bedThick(indDeep));
condThick = layerThick(indDeep,end) + bedThick(indDeep);
botEnergy(indDeep) = conductive_heat_flux(waterTemp(indDeep,end),bedTemp(indDeep),...
    condLakeBed,condThick); %positive out of the lake

%Shallow water cells
condLakeBed = weighted_mean('harmonic',thermDiffCoeff(indShallow,1),condBed(indShallow),...
    layerThick(indShallow,1),bedThick(indShallow));
condThick = layerThick(indShallow,1) + bedThick(indShallow);
botEnergy(indShallow) = conductive_heat_flux(waterTemp(indShallow,1),bedTemp(indShallow),...
    condLakeBed,condThick); %positive out of the lake

%--------------------------------------------------------------------------
%Calculate the deep lakes temperature
%--------------------------------------------------------------------------
ind = indDeep;
%calculate necessary constants
cvWater = cpWater .* densityWetland(ind,:);

%Calculate the lake temperature of deep lakes
waterTemp(ind,:) = eddy_diffusion_model(waterTemp(ind,:),surfEnergy(ind),botEnergy(ind),...
    radAbsorb(ind,:),layerThick(ind,:),sumDiffusionCoeff(ind,:),cvWater,timestepLength);

%check to see if the water is frozen, if so, create ice layer
test = waterTemp < 0;
testFlat = any(test,2);
if any(testFlat)
    freezingWater(test) = cpWater .* layerThick(test) .* -waterTemp(test) ./ heatFusion;
    waterTemp(test) = 0;
    newIce(testFlat) = sum(freezingWater(testFlat,:),2);
end

%Convectively mix the lakes, and update density
densityWetland(ind,:) = water_density(waterTemp(ind,:));
[waterTemp(ind,:),densityWetland(ind,:)] = convective_mixing(waterTemp(ind,:),densityWetland(ind,:),layerThick(ind,:));

%--------------------------------------------------------------------------
%Calculate the shallow lake and wetland temperature
%--------------------------------------------------------------------------
ind = indShallow;

%Calculate the net energy flux
qNetShallow(ind) = radAbsorb(ind,1) + surfEnergy(ind) - botEnergy(ind);

testCool = (qNetShallow < 0);
testWarm = (qNetShallow > 0);
%Calculate temperature change prior to any freezing if wetland is cooling
test = testCool & (waterTemp(:,1) > 0);
if any(test)
    [qNetShallow(test),waterTemp(test,1)] = temp_change_pre_phase(qNetShallow(test),waterTemp(test,1),...
        cpWater,densityWetland(test,1),layerThick(test,1),timestepLength);
end

%Calculate partial freezing if necessary
test = (qNetShallow < 0);
if any(test)
   [qNetShallow(test),frozenFrac(test)] = phase_change(qNetShallow(test),zerosGrid(test),...
       layerThick(test,1),heatFusion,densityIce,timestepLength,zerosGrid(test));
   newIce(test) = frozenFrac(test) .* layerThick(test,1);
end

%Calculate warming of wetlands
test = testWarm;
if any(test)
    waterTemp(test,1) = temp_change_post_phase(waterTemp(test,1),qNetShallow(test),...
        cpWater,densityWetland(test,1),layerThick(test,1),timestepLength);
end

%Error Check
if assert_LHM
    assert_LHM(all(abs(qNetShallow)<1e-10),'Energy remains after temperature and phase change in wetlands')
end

%Update water density
densityWetland(ind,1) = water_density(waterTemp(ind,1)); 

%--------------------------------------------------------------------------
%calculate energy storage in the lake and in new ice formation (in W/m^2)
%--------------------------------------------------------------------------
qStorage = radNet - radTransmit + surfEnergy - botEnergy;

%--------------------------------------------------------------------------
%Update stored quantities
%--------------------------------------------------------------------------
state = update_struct(state,'therm_cond_wetland',thermDiffCoeff,...
    'rad_transmit_wetland',radTransmit,'wetland_water_density',densityWetland,'ice_pack_bot_accrete_lake',newIce,...
    'temp_wetland',waterTemp,'energy_wetland',qStorage,'q_cond_lake_bed',botEnergy);
%q_cond_lake_bed is positive out of the lake and into the bed sediments
end
