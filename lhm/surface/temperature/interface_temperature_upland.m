function state = interface_temperature_upland(state,structure,params)
%INTERFACE_TEMPERATURE_UPLAND  Computes the temperature of the air-ground interface.
%   interface_temperature_upland()
%
%   Descriptions of Input Variables:
%   none, all fetched from the data controller
%
%   Descriptions of Output Variables:
%   none, all written to the data controller
%
%   Example(s):
%   >> interface_temperature_upland()
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-27
% Copyright 2008 Michigan State University.

[snowThick,condSnow,condLitter,condSoil,...
    litterPsi,soilPsi,waterLitter,waterSoil,...
    airTemp,snowTemp,litterTemp,soilTemp,rainTemp,...
    airPress,airVapPress,qRadShort,...
    qLongDown,interfTemp,resistAeroHeatVapor,...
    dripRain,throughRain,canopyTemp,snowWater,waterDepression,...
    densMoistAir] = ...
    deal_struct(state,...
    'snow_thickness_upland','therm_cond_snow_upland','therm_cond_litter','therm_cond_soil',...
    'matric_potential_litter','matric_potential_soil','water_litter','water_soil',...
    'temp_micro_upland','temp_snow_upland','temp_litter','temp_soil','temp_rain',...
    'pressure','vap_pressure_micro_upland','rad_short_upland_net',...
    'rad_long_canopy_down','temp_interf_upland','resist_aero_ground_heat_vapor_upland',...
    'drip_rain','throughfall_rain','temp_canopy','water_snow_upland','water_depression',...
    'dens_moist_air');
[litterThick,snowEmissivity,litterEmissivity,...
    litterRough,soilRough,litterFrac,snowInterfShortFrac,...
    soilSatCap,litterSatCap,residWater,litterResidWater] = deal_struct(params,...
    'litter_thickness','emissivity_snow','emissivity_litter',...
    'litter_rough_height','soil_rough_height','litter_fraction','snow_light_interf_absorb',...
    'soil_sat_cap','litter_sat_cap','soil_water_resid','litter_resid_water');
[zerosGrid,soilThick,timestepLength,tolEnergy,...
    indSnow,indUpland,iterTol,iterMax,iterPerturb,...
    indLitter,indSoil] = ...
    deal_struct(structure,...
    'zeros_grid','thickness_soil','timestep_length','energy_balance_tolerance',...
    'index_snow_upland','index_upland','iteration_error_tolerance','iteration_max_iterations','iteration_perturbation',...
    'index_bare_litter','index_bare_soil');

%--------------------------------------------------------------------------
% Initialize arrays
%--------------------------------------------------------------------------
[subsCond,subsThick,subsTemp,interfRelHum,interfEmissivity,availSoilWater,...
    litterRelHum,soilRelHum,soilSatFrac,litterSatFrac,soilTheta,...
    qBalance,qLongUp,qCond,qSens,qPrecip,qEvapDep,qEvapSnow,qEvapLitter,qEvapSoil,...
    interfVapPress,evapSnow,evapLitter,evapSoil,evapDep,qPhase,soilEmissivity,...
    availLitterWater,qLongReflect,qStatic] = deal(zerosGrid);

%--------------------------------------------------------------------------
% Determine properties
%--------------------------------------------------------------------------
%Calculate the relative humidity of the substrate
litterRelHum(indLitter) = porous_rel_hum(params,airTemp(indLitter),litterPsi(indLitter));
soilRelHum(indSoil) = porous_rel_hum(params,airTemp(indSoil),soilPsi(indSoil,1));

%Calculate the soil and litter saturation fraction, theta
soilSatFrac(indSoil) = waterSoil(indSoil,1) ./ soilSatCap(indSoil,1);
litterSatFrac(indLitter) = waterLitter(indLitter) ./ litterSatCap(indLitter);
soilTheta(indUpland) = waterSoil(indUpland,1) ./ soilThick(indUpland,1);
availSoilWater(indUpland) = waterSoil(indUpland,1) - residWater(indUpland,1);
availLitterWater(indLitter) = max(waterLitter(indLitter),litterResidWater(indLitter));

%Calculate the soil emissivity as a function of soil moisture, this is from
%Saito et al 2006
soilEmissivity(indSoil) = 0.9 + 0.08 * soilSatFrac(indSoil);

%Account for partial litter coverage
soilFrac = 1 - litterFrac;
condLitter = weighted_mean('arithmetic',condLitter,condSoil(:,1),litterFrac,soilFrac);
litterThick = weighted_mean('arithmetic',litterThick,soilThick(:,1),litterFrac,soilFrac);
litterTemp = weighted_mean('arithmetic',litterTemp,soilTemp(:,1),litterFrac,soilFrac);
litterRelHum = weighted_mean('arithmetic',litterRelHum,soilRelHum,litterFrac,soilFrac);
litterEmissivity = weighted_mean('arithmetic',litterEmissivity+zerosGrid,soilEmissivity,litterFrac,soilFrac);
litterRough = weighted_mean('arithmetic',litterRough+zerosGrid,soilRough+zerosGrid,litterFrac,soilFrac);

%Thermal Conductance of the substrate
subsCond = populate_array(subsCond,condSnow,condLitter,condSoil(:,1),...
    indSnow,indLitter,indSoil);

%Thickness of the substrate
subsThick = populate_array(subsThick,snowThick,litterThick,soilThick(:,1),...
    indSnow,indLitter,indSoil);

%Temperature of the substrate
subsTemp = populate_array(subsTemp,snowTemp,litterTemp,soilTemp(:,1),...
    indSnow,indLitter,indSoil);

%Relative humidity at the interface
%assume that snow surface relative humidity is 1
interfRelHum = populate_array(interfRelHum,zerosGrid+1,litterRelHum,soilRelHum,...
    indSnow,indLitter,indSoil);

%Emissivity of the interface
interfEmissivity = populate_array(interfEmissivity,snowEmissivity,litterEmissivity,soilEmissivity,...
    indSnow,indLitter,indSoil);

%Convert units on precip and dripthrough inputs
dripRain = dripRain / timestepLength;
throughRain = throughRain / timestepLength;

%For shortwave radiation in snow, use only the fraction absorbed by the
%surface, the rest is absorbed at depth
qRadShort(indSnow) = qRadShort(indSnow) .* snowInterfShortFrac;

%Longwave radiation is not all absorbed
qLongReflect(indUpland) = qLongReflect(indUpland) .* (1 - interfEmissivity(indUpland));

%Calculate the ground resistance to vapor transport
[resistSoilLitter,resistLitter] = soil_resistance(structure,params,soilTheta,litterSatFrac,indLitter,indUpland,densMoistAir);

%--------------------------------------------------------------------------
%                   Iteration Loop for Interface Temperature
%--------------------------------------------------------------------------
%Calculate the static portion of the energy balance
qStatic(indUpland) = qRadShort(indUpland) + qLongDown(indUpland) - qLongReflect(indUpland);

%The first step is to check for instances where snow melting occurs, to do this,
%set the surface temperature everywhere to 0, and observe in the surface
%energy balance is positive (indicating the temperature would tend to
%increase above 0).  If so, then set qPhase=energy imbalance.
if any(indSnow)
    ind = indSnow;
    qPhase(ind) = energy_balance(zerosGrid(ind),...
        canopyTemp(ind),subsTemp(ind),rainTemp(ind),airTemp(ind),dripRain(ind),throughRain(ind),...
        interfRelHum(ind),airVapPress(ind),airPress(ind),densMoistAir(ind),...
        resistAeroHeatVapor(ind),resistSoilLitter(ind),resistLitter(ind),...
        interfEmissivity(ind),subsCond(ind),subsThick(ind),availLitterWater(ind),snowWater(ind),...
        availSoilWater(ind),waterDepression(ind),zerosGrid(ind),qStatic(ind),...
        indSnow(ind),indSoil(ind),indLitter(ind),timestepLength,zerosGrid(ind),litterFrac(ind));
    indPhase = (qPhase > 0);
    indNoPhase = ~indPhase;
    interfTemp(indPhase) = 0; %set interface temperature at 0 for cells with melting
    qPhase(indNoPhase) = 0; %if energy imbalance was negative, no melting occurred
else
    indNoPhase = logical(zerosGrid + 1);
end

%Only loop over cells that did not melt
ind = (indUpland) & (indNoPhase); %the size of this index should match the domain size of the model

if any(ind)
    %Newton-Raphson iteration for interface temperature
    error = 1;
    niter = 0;
    while (error>iterTol) && (niter<iterMax)
        tempLast = interfTemp(ind);
        niter = niter + 1;
        f1 = energy_balance(interfTemp(ind),...
            canopyTemp(ind),subsTemp(ind),rainTemp(ind),airTemp(ind),dripRain(ind),throughRain(ind),...
            interfRelHum(ind),airVapPress(ind),airPress(ind),densMoistAir(ind),...
            resistAeroHeatVapor(ind),resistSoilLitter(ind),resistLitter(ind),...
            interfEmissivity(ind),subsCond(ind),subsThick(ind),availLitterWater(ind),snowWater(ind),...
            availSoilWater(ind),waterDepression(ind),qPhase(ind),qStatic(ind),...
            indSnow(ind),indSoil(ind),indLitter(ind),timestepLength,zerosGrid(ind),litterFrac(ind));
        f2 = energy_balance(interfTemp(ind) + iterPerturb,...
            canopyTemp(ind),subsTemp(ind),rainTemp(ind),airTemp(ind),dripRain(ind),throughRain(ind),...
            interfRelHum(ind),airVapPress(ind),airPress(ind),densMoistAir(ind),...
            resistAeroHeatVapor(ind),resistSoilLitter(ind),resistLitter(ind),...
            interfEmissivity(ind),subsCond(ind),subsThick(ind),availLitterWater(ind),snowWater(ind),...
            availSoilWater(ind),waterDepression(ind),qPhase(ind),qStatic(ind),...
            indSnow(ind),indSoil(ind),indLitter(ind),timestepLength,zerosGrid(ind),litterFrac(ind));
        interfTemp(ind) = tempLast - (iterPerturb .* f1)./(f2 - f1);
        error = mean(abs(interfTemp(ind) - tempLast));
    end
end

%Now, fetch all of the calculated values with the final interfTemp
ind = indUpland;
[qBalance(ind),qLongUp(ind),qCond(ind),qPrecip(ind),qSens(ind),qEvapSnow(ind),...
    qEvapLitter(ind),qEvapSoil(ind),qEvapDep(ind),evapSnow(ind),evapLitter(ind),...
    evapSoil(ind),evapDep(ind),interfVapPress(ind)] = energy_balance(interfTemp(ind),...
    canopyTemp(ind),subsTemp(ind),rainTemp(ind),airTemp(ind),dripRain(ind),throughRain(ind),...
    interfRelHum(ind),airVapPress(ind),airPress(ind),densMoistAir(ind),...
    resistAeroHeatVapor(ind),resistSoilLitter(ind),resistLitter(ind),...
    interfEmissivity(ind),subsCond(ind),subsThick(ind),availLitterWater(ind),snowWater(ind),...
    availSoilWater(ind),waterDepression(ind),qPhase(ind),qStatic(ind),...
    indSnow(ind),indSoil(ind),indLitter(ind),timestepLength,zerosGrid(ind),litterFrac(ind));

%Check the energy balance
if assert_LHM()
    assert_LHM(mean(abs(qBalance))<tolEnergy,'After iteration, the upland interface energy balance did not meet the convergence criterion');
end

%--------------------------------------------------------------------------
%                   Finish and Update Stored Quantities
%--------------------------------------------------------------------------
%Remove depression evaporation from depression water storage
waterDepression(ind) = waterDepression(ind) - evapDep(ind);

%Calculate total upwelling longwave radiation
qLongUp(ind) = qLongUp(ind) + qLongReflect(ind);

%Update stored quantities
state = update_struct(state,'rough_height_litter',litterRough,'vap_pressure_interf_upland',interfVapPress,...
    'temp_interf_upland',interfTemp,'evap_soil',evapSoil,'evap_litter',evapLitter,...
    'evap_snow_upland',evapSnow,'q_cond_interf_upland',qCond,...
    'water_depresion',waterDepression,'rad_long_upland_up',qLongUp,'q_precip_interf_upland',qPrecip,...
    'evap_depression',evapDep,'q_phase_upland',qPhase,'sensible_interf_upland',qSens,...
    'latent_depression',qEvapDep,'latent_soil',qEvapSoil,'latent_snow_upland',qEvapSnow,'latent_litter',qEvapLitter);
end

%--------------------------------------------------------------------------
%                           Energy Balance Function
%--------------------------------------------------------------------------
%This is nested to avoid having to pass all of the inputs
function [qBalance,qLongUp,qCond,qPrecip,qSens,qEvapSnow,qEvapLitter,qEvapSoil,...
    qEvapDep,evapSnow,evapLitter,evapSoil,evapDep,interfVapPress] = energy_balance(interfTemp,...
    canopyTemp,subsTemp,rainTemp,airTemp,dripRain,throughRain,...
    interfRelHum,airVapPress,airPress,densMoistAir,resistAeroHeatVapor,...
    resistSoilLitter,resistLitter,interfEmissivity,subsCond,subsThick,...
    waterLitter,snowWater,availSoilWater,waterDepression,...
    qMelt,qStatic,indSnow,indSoil,indLitter,timestepLength,zerosGrid,litterFrac)

%Calculate the precipitation heat flux, snow is not included in this
%because we don't assume that it will all melt, it is passed to the
%snowpack model
qPrecip = precip_heat_flux(params,dripRain,canopyTemp,interfTemp) + precip_heat_flux(throughRain,rainTemp,interfTemp);

%Calculate upwelling longwave radiation
qLongUp = longwave_flux(interfTemp,interfEmissivity);

%Calculate heat conduction to the substrate
qCond = conductive_heat_flux(interfTemp,subsTemp,subsCond,subsThick);

%Calculate latent and sensible heat fluxes
qSens = sens_flux(params,interfTemp,airTemp,densMoistAir,resistAeroHeatVapor);

[qEvap,peInterf,interfVapPress,qEvapSoil,evapSoil,qEvapLitter,evapLitter] = latent_flux(params,...
    interfTemp,interfRelHum,airVapPress,airPress,densMoistAir,timestepLength,...
    resistAeroHeatVapor,zerosGrid,resistSoilLitter,resistLitter); %assume no ground resistance for snow, also for sensible heat flux

%Reduce evapLitter and qEvapLitter to match the litter fraction in upland
%cells
qEvapLitter = qEvapLitter .* litterFrac;
evapLitter = evapLitter .* litterFrac;

%Actual soil evaporation cannot be negative, dew formation can occur, but
%it occurs at the interface and becomes depression storage
[evapSoil,qEvapSoil] = limit_vals([0,Inf],evapSoil,qEvapSoil);

%Call actual_ET, it partitions moisture and energy fluxes
%between snow, litter, depressions, and soil, and limits fluxes based on
%available moisture in each domain
[qEvapSnow,qEvapLitter,qEvapSoil,qEvapDep,evapSnow,evapLitter,evapSoil,evapDep] = actual_ET(...
    peInterf,qEvap,evapLitter,qEvapLitter,evapSoil,qEvapSoil,...
    snowWater,availSoilWater,waterLitter,waterDepression,...
    indSnow,indSoil,indLitter,zerosGrid);

%Calculate the final balance, soil and litter evaporation are not included
%in the interface energy balance
qBalance = qStatic - qLongUp - qCond + qPrecip - qMelt - qEvapDep - qEvapSnow - qSens - qEvapLitter - qEvapSoil;
end
    
%--------------------------------------------------------------------------
%Calculate partitioning of energy and mass fluxes into upland domains, also
%incorporates limitations due to moisture availability
%--------------------------------------------------------------------------
function [qEvapSnowLimit,qEvapLitterLimit,qEvapSoilLimit,qEvapDepLimit,...
    evapSnowLimit,evapLitterLimit,evapSoilLimit,evapDepLimit] = actual_ET(...
    peTotal,qEvapTotal,evapLitter,qEvapLitter,evapSoil,qEvapSoil,...
    snowWater,availSoilWater,availLitterWater,waterDepression,indSnow,indSoil,indLitter,zerosGrid)

%Intialize output and internal arrays
[qEvapSnowLimit,qEvapLitterLimit,qEvapSoilLimit,qEvapDepLimit,...
    evapSnowLimit,evapLitterLimit,evapSoilLimit,evapDepLimit] = deal(zerosGrid);

%Snow-covered ground
if any(indSnow)
    evapSnowLimit(indSnow) = min(peTotal(indSnow),snowWater(indSnow)); %can be negative
    qEvapSnowLimit(indSnow) = abs(evapSnowLimit(indSnow) ./ peTotal(indSnow)) .* qEvapTotal(indSnow);
    qEvapSnowLimit(isnan(qEvapSnowLimit)) = 0;
end

%Bare litter
if any(indLitter)
    evapLitterLimit(indLitter) = min(evapLitter(indLitter),availLitterWater(indLitter)); %can be negative
    qEvapLitterLimit(indLitter) = abs(evapLitterLimit(indLitter) ./ evapLitter(indLitter)) .* qEvapLitter(indLitter);
    qEvapLitterLimit(isnan(qEvapLitterLimit)) = 0;
    %Remove these fluxes from the potential remaining for soil
    peTotal(indLitter) = peTotal(indLitter) - evapLitter(indLitter); %remaining pe for soil
    qEvapTotal(indLitter) = qEvapTotal(indLitter) - qEvapLitterLimit(indLitter);
end   

%Depressions, for bare soil only
if any(indSoil)
    evapDepLimit(indSoil) = min(waterDepression(indSoil),peTotal(indSoil)); %can be negative
    qEvapDepLimit(indSoil) = abs(evapDepLimit(indSoil) ./ peTotal(indSoil)) .* qEvapTotal(indSoil);
    qEvapDepLimit(isnan(qEvapDepLimit)) = 0;
    %Remove these fluxes from the potential remaining for soil
    peTotal(indSoil) = peTotal(indSoil) - evapDepLimit(indSoil); %remaining pe for soil
    qEvapTotal(indSoil) = qEvapTotal(indSoil) - qEvapDepLimit(indSoil);
end

%Bare soil, bare-litter covered soil
indBoth = (indLitter | indSoil); %soil underlies litter as well
if any(indBoth) %this would only fail if the model is completely snow covered
    %Already, evapSoil and qEvapSoil are constrained to be positive
    % bare soil can have negative evap, but depressions would already have
    % stored this water
    
    %Calculate the reduction in potential evap and latent flux due to
    %litter and depressions
    peSoil = min(peTotal(indSoil),evapSoil(indSoil)); %total soil evaporation cannot exceed the remaining potential
    qPeSoil = min(qEvapTotal(indSoil),qEvapSoil(indSoil)); 
    
    %Calculate the limited evaporation and latent heat flux
    evapSoilLimit(indSoil) = min(peSoil,availSoilWater(indSoil));
    qEvapSoilLimit(indSoil) = abs(evapSoilLimit(indSoil) ./ peSoil) .* qPeSoil;
    qEvapSoilLimit(isnan(qEvapSoilLimit)) = 0;
end
end