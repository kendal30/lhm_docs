function state = canopy_temperature_module(state,structure,params)
%CANOPY_TEMPERATURE_MODULE  Calculates canopy temperature and PE.
%   canopy_temperature_module()
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[interceptRain,interceptSnow,dripRain,dripSnow,radShort,...
    qLongDown,lai,waterCanopy,canopyCap,canopyHeight,...
    windspeed,airTemp,canopyTemp,densMoistAir,qLongUpland,qLongWetland,...
    canopyGapFrac,airVapPress,airPress,rainTemp,snowTemp,...
    wetlandFrac,uplandFrac,streamFrac,waterSoil] = ...
    deal_struct(state,...
    'intercept_rain','intercept_snow','drip_rain','drip_snow','rad_short_canopy_net',...
    'rad_long_in','lai','water_canopy','canopy_capacity','canopy_height',...
    'windspeed','temp_air','temp_canopy','dens_moist_air','rad_long_upland_up','rad_long_wetland_up',...
    'canopy_gap_fraction','vap_pressure_air','pressure','temp_rain','temp_snowfall',...
    'fraction_wetland','fraction_upland','fraction_stream','water_soil');
[cvLeaf,dLeaf,soilResidWater] = ...
    deal_struct(params,'turb_trans_leaf','canopy_leaf_dimension','soil_water_resid');
[timestepLength,zerosGrid,iterTol,iterPerturb,...
    iterMax,numLayers,indWetCanopy,tolEnergy,...
    indUpland,soilZerosGrid] = ...
    deal_struct(structure,...
    'timestep_length','zeros_grid','iteration_error_tolerance','iteration_perturbation',...
    'iteration_max_iterations','num_layers_soil','index_wet_canopy','energy_balance_tolerance',...
    'index_upland','zeros_grid_soil');

%Update wetland frac to include stream frac, currently
wetlandFrac = wetlandFrac + streamFrac;

%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[qBalance,qLongCanopy,qPrecip,qSens,qEvap,evap,leafVapPress,qTranspUpland,...
    transpUpland,qTranspWetland,transpWetland] = deal(zerosGrid);
[transpUplandLay] = deal(soilZerosGrid);

%--------------------------------------------------------------------------
%                   Prepare Inputs for Iteration Loop
%--------------------------------------------------------------------------
%Need to add together the interception and dripthrough components because
%they both affect the temperature, convert to units of /s
rain = (interceptRain + dripRain) / timestepLength;
snow = (interceptSnow + dripSnow) / timestepLength;

%Re-calculate relative humidity for the leaf relative humidity
relHum = airVapPress ./ sat_vap_pressure(airTemp);
leafRelHum = relHum; %assume the leaf relative humidity same as air relative humidity if no water stored on leaves
if any(indWetCanopy)
    leafRelHum(indWetCanopy) = 1; %assume wet surface everywhere if any water storage
end

%Calculate emissivity as simply the closed fraction of the canopy
emissivity = (1 - canopyGapFrac); %from CLM, p. 43

%--------------------------------------------------------------------------
%Calculate incident longwave radiation from the surface and atmosphere
%--------------------------------------------------------------------------
qLongSurf = uplandFrac .* qLongUpland + wetlandFrac .* qLongWetland;
qLongSurfTransmit = qLongSurf .* canopyGapFrac;
qLongDownTransmit = qLongDown .* canopyGapFrac;
qLongIntercept = qLongSurf + qLongDown - qLongSurfTransmit - qLongDownTransmit;

%--------------------------------------------------------------------------
%Calculate aerodynamic,leaf boundary layer resistances, and stomatal conductances
%--------------------------------------------------------------------------
%Guess the starting temperature as the average of the previous canopy temp
%and the current airTemp
canopyTemp = (canopyTemp + airTemp) / 2;

%Calculate aerodynamic resistances
[resistAeroMomentum,resistAeroHeatVapor] = ...
    aerodynamic_resistance(structure,params,'unstable',windspeed,canopyHeight,airTemp,canopyTemp);

%Calculate the leaf boundary layer resistance
windspeedCanopy = windspeed .* (1./(resistAeroMomentum .* windspeed)).^(1/2);
resistLeafBL = 1 ./ cvLeaf .* (dLeaf ./ windspeedCanopy).^(1/2);

%Calculate canopy resistances
[condStomatalUpland,condStomatalWetland,indActive] = stomatal_conductance(state,structure,params);


%--------------------------------------------------------------------------
%Calculate Stomatal Resistance and Transpiration and Latent Fluxes
%Only if transpiration is active (from stomatal_conductance)
%--------------------------------------------------------------------------
if ~islogical(condStomatalUpland)
    ind = indActive;
    %The resistance at the canopy is the parallel sum of the soil resistances,
    %or the inverse of the conductance sum
    resistCanopyTranspUpland = 1 ./ sum(condStomatalUpland(ind,:) .* repmat(lai(ind),[1,numLayers]), 2);

    %The wetland resistance is for a single-layer only
    resistCanopyTranspWetland = 1 ./ (condStomatalWetland(ind) .* lai(ind));

    %Now, to figure out which layer the water is extracted from, calculate the
    %transpiration fraction
    transpFracUpland = condStomatalUpland(ind,:) ./ repmat(sum(condStomatalUpland(ind,:),2),[1,numLayers]);

    %Error checking
    if assert_LHM
        assert_LHM(~any(isnan(resistCanopyTranspUpland(:))),'Canopy resistance for upland transpiration contains NaN');
        assert_LHM(~any(isnan(resistCanopyTranspWetland(:))),'Canopy resistance for wetland transpiration contains NaN');
    end
    
    %Calculate transpiration and latent fluxes
    [qTranspUpland(ind),transpUpland(ind),leafVapPress(ind),qTranspWetland(ind),transpWetland(ind)] = latent_flux(params,...
        canopyTemp(ind),leafRelHum(ind),airVapPress(ind),airPress(ind),densMoistAir(ind),timestepLength,...
        resistAeroHeatVapor(ind),resistCanopyTranspUpland+resistLeafBL(ind),resistCanopyTranspWetland+resistLeafBL(ind));

    %Transpiration can not be negative, nor can qTransp
    [qTranspUpland(ind),transpUpland(ind),qTranspWetland(ind),transpWetland(ind)] =...
        limit_vals([0,Inf],...
        qTranspUpland(ind),transpUpland(ind),qTranspWetland(ind),transpWetland(ind));
    
    %Rescale each component as the fraction of each cell occupied by
    %each type
    qTranspWetland(ind) = qTranspWetland(ind) .* wetlandFrac(ind);
    qTranspUpland(ind) = qTranspUpland(ind) .* uplandFrac(ind);
    
    %Split the canopy transpiration up into the soil layers, based on the
    %previously-calculated transpiration fraction
    transpUplandLay(ind,:) = repmat(transpUpland(ind),[1,numLayers]) .* transpFracUpland;

    %Calculate transpiration moisture limitation, or pt->T
    [transpUplandLay,qTranspUpland] = actual_T(transpUplandLay,qTranspUpland,...
        waterSoil,soilResidWater,indUpland,indActive,zerosGrid,soilZerosGrid);
end

%--------------------------------------------------------------------------
%                   Iteration Loop for Canopy Temperature
%--------------------------------------------------------------------------
%Calculate the static portion of the energy balance
staticBalance = radShort + qLongIntercept - qTranspUpland - qTranspWetland;

%Only run these for upland cells, otherwise there is no canopy by
%definition, also, canopyGapFraction must be less than 1
ind = indUpland & (canopyGapFrac < 0.99);

%Newton-Raphson iteration for canopy temperature
error = 1;
niter = 0;
while (error>iterTol) && (niter<iterMax)
    tempLast = canopyTemp(ind);
    niter = niter + 1;
    f1 = energy_balance(canopyTemp(ind),...
        rainTemp(ind),snowTemp(ind),airTemp(ind),rain(ind),snow(ind),emissivity(ind),...
        densMoistAir(ind),resistAeroHeatVapor(ind),resistLeafBL(ind),leafRelHum(ind),...
        airVapPress(ind),airPress(ind),waterCanopy(ind),canopyCap(ind),indWetCanopy(ind),...
        timestepLength,zerosGrid(ind),staticBalance(ind));
    f2 = energy_balance(canopyTemp(ind) + iterPerturb,...
        rainTemp(ind),snowTemp(ind),airTemp(ind),rain(ind),snow(ind),emissivity(ind),...
        densMoistAir(ind),resistAeroHeatVapor(ind),resistLeafBL(ind),leafRelHum(ind),...
        airVapPress(ind),airPress(ind),waterCanopy(ind),canopyCap(ind),indWetCanopy(ind),...
        timestepLength,zerosGrid(ind),staticBalance(ind));
    canopyTemp(ind) = tempLast - (iterPerturb .* f1)./(f2 - f1);
    error = mean(abs(canopyTemp(ind) - tempLast));
end

%Now, fetch all of the calculated values with the final canopyTemp
[qBalance(ind),qLongCanopy(ind),qPrecip(ind),qSens(ind),qEvap(ind),evap(ind)] = energy_balance(canopyTemp(ind),...
    rainTemp(ind),snowTemp(ind),airTemp(ind),rain(ind),snow(ind),emissivity(ind),...
    densMoistAir(ind),resistAeroHeatVapor(ind),resistLeafBL(ind),leafRelHum(ind),...
    airVapPress(ind),airPress(ind),waterCanopy(ind),canopyCap(ind),indWetCanopy(ind),...
    timestepLength,zerosGrid(ind),staticBalance(ind));

%Check the energy balance
if assert_LHM()
    assert_LHM(mean(abs(qBalance(ind)))<tolEnergy,'After iteration, the canopy energy balance did not meet the convergence criterion');
end

%--------------------------------------------------------------------------
%                   Finish and Update Stored Quantities
%--------------------------------------------------------------------------
%Calculate some final longwave flux terms
qLongCanopyUp = qLongCanopy + qLongSurfTransmit;
qLongCanopyDown = qLongCanopy + qLongDownTransmit;

%Update the water stored in the canopy
waterCanopy(indWetCanopy) = waterCanopy(indWetCanopy) - evap(indWetCanopy);

%Save the data to data controller
state = update_struct(state,'water_canopy',waterCanopy,'evap_canopy',evap,...
    'transp_upland',transpUplandLay,'transp_wetland',transpWetland,'temp_canopy',canopyTemp,...
    'rad_long_canopy_down',qLongCanopyDown,'rad_long_canopy_up',qLongCanopyUp,...
    'latent_canopy_evap',qEvap,'latent_canopy_transp_upland',qTranspUpland,...
    'latent_canopy_transp_wetland',qTranspWetland,'sensible_canopy',qSens,...
    'q_precip_canopy',qPrecip,'resist_aero_canopy_momentum',resistAeroMomentum,'resist_leaf_boundary_layer',resistLeafBL,...
    'resist_aero_canopy_heat_vapor',resistAeroHeatVapor,'vap_pressure_leaf',leafVapPress);

end

%--------------------------------------------------------------------------
%Energy balance function
%--------------------------------------------------------------------------
function [qBalance,qLongCanopy,qPrecip,qSens,qEvap,evap] = energy_balance(canopyTemp,...
    rainTemp,snowTemp,airTemp,rain,snow,emissivity,...
    densMoistAir,resistAero,resistLeafBL,leafRelHum,...
    airVapPress,airPress,waterCanopy,canopyCap,indWetCanopy,...
    timestepLength,zerosGrid,staticBalance)

%Calculate the precip heat flux
qPrecip = precip_heat_flux(params,rain,rainTemp,canopyTemp,false) + precip_heat_flux(snow,snowTemp,canopyTemp,false);

%Calculate the longwave flux
qLongCanopy = longwave_flux(canopyTemp,emissivity); %This will be both up and down

%Calculate the evaporative and latent heat fluxes
[qSens] = sens_flux(params,canopyTemp,airTemp,densMoistAir,resistAero);
[qEvap,evap] = latent_flux(params,canopyTemp,leafRelHum,airVapPress,airPress,densMoistAir,timestepLength,...
    resistAero,resistLeafBL);

%Calculate moisture limitation in E
[evap,qEvap] = actual_E(evap,qEvap,waterCanopy,canopyCap,indWetCanopy,zerosGrid);

%Calculate the final balance
qBalance = staticBalance - 2*qLongCanopy + qPrecip - qEvap - qSens;
end

%--------------------------------------------------------------------------
%Evaporation and transpiration moisture limitation functions
%--------------------------------------------------------------------------
%Limit Evap
function [evapLimit,qEvapLimit] = actual_E(evap,qEvap,waterCanopy,canopyCap,indWetCanopy,zerosGrid)
[evapLimit,qEvapLimit] = deal(zerosGrid);
%Calculate evaporation moisture limitation
ind = indWetCanopy;
if any(ind)
    %This from Manfreda et al? or Chen 2005?
    evapLimit(ind) = min((waterCanopy(ind)./canopyCap(ind)).^(2/3) .* evap(ind), waterCanopy(ind)); %can be negative
    qEvapLimit(ind) = evapLimit(ind) ./ evap(ind) .* qEvap(ind);
    qEvapLimit(isnan(qEvapLimit)) = 0;
end
end

%Limit transp
function [transpLimit,qTranspLimit] = actual_T(transpUpland,qTranspUpland,...
    waterSoil,soilResidWater,indUpland,indActive,zerosGrid,soilZerosGrid)
%Initialize outputs
qTranspLimit = zerosGrid;
transpLimit = soilZerosGrid;

%Calculate transpiration moisture limitation
ind = indUpland & indActive;
if any(ind)
    transpLimit(ind,:) = min(transpUpland(ind,:),waterSoil(ind,:) - soilResidWater(ind,:)); %already constrained to be positive
    qTranspLimit(ind) = sum(transpLimit(ind,:) ./ transpUpland(ind,:), 2) .* qTranspUpland(ind);
    qTranspLimit(isnan(qTranspLimit)) = 0;
end
end