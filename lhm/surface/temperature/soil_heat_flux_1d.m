function [soilTemp,deltaT,qWaterFlux] = soil_heat_flux_1d(soilTemp,condSoil,soilThick,...
    advectFluxPot,cpSoilSpecific,qSurf,qDeep,timestepLength,innerTimesteps,numLayers,soilZerosGrid)
%SOIL_HEAT_FLUX_1D  One-line description here, please.
%   output = soil_heat_flux_1d(input)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> soil_heat_flux_1d
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-21
% Copyright 2008 Michigan State University.

%Allow for multiple inner timesteps
timestepLength = timestepLength / innerTimesteps;

%Initialize arrays
[qCondMinus,qCondPlus,qWaterFlux,m1,m2] = deal(soilZerosGrid);

%Here, 'minus' refers to decreasing index, i.e. condMinus refers to the
%conductivity between the ith and the (i-1)th layer
condMinus = weighted_mean('harmonic',condSoil(:,1:end-1),condSoil(:,2:end),...
    soilThick(:,1:end-1),soilThick(:,2:end));
thickMinus = weighted_mean('arithmetic',soilThick(:,1:end-1),soilThick(:,2:end),...
    1,1);

m1(:,2:end) = condMinus ./ thickMinus;
m2(:,1:end-1) = m1(:,2:end);
m3 = timestepLength ./ (soilThick .* cpSoilSpecific);

%Advect flux potential is defined as positive upward
%qCondMinus and qPlus are defined as positive upward also
%qWaterFlux is positive into the layer
tempMinus = soilTemp(:,2:end) - soilTemp(:,1:end-1);
qCondMinus(:,2:end) = m1(:,2:end) .* tempMinus;
qCondPlus(:,1:end-1) = m2(:,1:end-1) .* tempMinus;

%Calculate the flux of heat from interlayer soil water fluxes.  Here, only
%fluxes into the layer are used, so since advectFluxPot is positive upward,
%the fluxes into the layer from above use only the negative advectFluxPot
%from the overlying cells, and the fluxes into the layer from below use
%only the positive advectFluxPot from the underlying cells
qWaterFlux(:,1) = max(advectFluxPot(:,2),0) .* tempMinus(:,2); 
qWaterFlux(:,2:end-1) = (max(advectFluxPot(:,3:end),0) .* tempMinus(:,2:end) + ...
    min(advectFluxPot(:,1:end-2),0)) .* tempMinus(:,1:end-1);
qWaterFlux(:,end) = min(advectFluxPot(:,end-1),0) .* tempMinus(:,end);
    
alpha = 1/2;
prevSoilTemp = soilTemp;
soilTemp = unit_conversions(soilTemp,'C','K');%need to work in absolute temperature
for m=1:innerTimesteps
    [r,a,b,c] = deal(soilZerosGrid);
    %For the first layer
    ind = 1;
    b(:,ind) = 1 + (1-alpha) * m3(:,ind) .* m2(:,ind);
    c(:,ind) = -(1-alpha) * m3(:,ind) .* m2(:,ind);
    r(:,ind) = soilTemp(:,ind) - qSurf .* m3(:,ind) + ...
        alpha .* m3(:,ind) .* (qCondPlus(:,ind) + qWaterFlux(:,ind));
    %For interior layers
    ind = (2:numLayers-1);
    a(:,ind) = -(1-alpha) * m3(:,ind) .* m1(:,ind);
    b(:,ind) = 1 + (1-alpha) * m3(:,ind) .* (m1(:,ind) + m2(:,ind));
    c(:,ind) = -(1-alpha) * m3(:,ind) .* m2(:,ind);
    r(:,ind) = soilTemp(:,ind) + alpha .* m3(:,ind) .* (qCondPlus(:,ind) - ...
        qCondMinus(:,ind) + qWaterFlux(:,ind));
    %For the bottom layer
    ind = numLayers;
    a(:,ind) = -(1-alpha) * m3(:,ind) .* m1(:,ind);
    b(:,ind) = 1 + (1-alpha) * m3(:,ind) .* m1(:,ind);
    r(:,ind) = soilTemp(:,ind) - qDeep .* m3(:,ind) + alpha .*m3(:,ind) .* (-qCondMinus(:,ind) + ...
        qWaterFlux(:,ind));

    %solve system of equations
    soilTemp = tridiagonal_solver(a,b,c,r);
end
%Calculate the soilTemp in celsius, and deltaT
soilTemp = unit_conversions(soilTemp,'K','C');
deltaT = prevSoilTemp - soilTemp;