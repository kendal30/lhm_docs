function state = interface_temperature_wetland(state,structure,params)
%INTERFACE_TEMPERATURE_WETLAND  Computes the temperature of the air-ground interface.
%   interface_temperature_wetland()
%
%   Descriptions of Input Variables:
%   none, all fetched from the data controller
%
%   Descriptions of Output Variables:
%   none, all written to the data controller
%
%   Example(s):
%   >> interface_temperature_wetland()
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-27
% Copyright 2008 Michigan State University.

[snowThick,condSnow,dripSnow,throughSnow,...
    airTemp,snowTemp,iceTemp,rainTemp,...
    snowfallTemp,airPress,airVapPress,qRadShort,qLongDown,...
    interfTemp,resistAeroHeatVapor,waterTemp...
    dripRain,throughRain,canopyTemp,snowWater,iceWater,condWetland,...
    iceThick,densMoistAir] = ...
    deal_struct(state,...
    'snow_thickness_wetland','therm_cond_snow_wetland','drip_snow','throughfall_snow',...
    'temp_micro_wetland','temp_snow_wetland','temp_ice_pack','temp_rain',...
    'temp_snowfall','pressure','vap_pressure_micro_wetland','rad_short_wetland_net','rad_long_canopy_down',...
    'temp_interf_wetland','resist_aero_ground_heat_vapor_wetland','temp_wetland',...
    'drip_rain','throughfall_rain','temp_canopy','water_snow_wetland','water_ice_pack','therm_cond_wetland',...
    'ice_pack_thickness','dens_moist_air');
[snowEmissivity,iceEmissivity,waterEmissivity,condIce,...
    snowInterfShortFrac,iceInterfShortFrac,waterInterfShortFrac] = ...
    deal_struct(params,...
    'emissivity_snow','emissivity_ice','emissivity_water','therm_cond_ice',...
    'snow_light_interf_absorb','ice_light_interf_absorb','wetland_light_interf_absorb');
[zerosGrid,waterThick,timestepLength,tolEnergy,indSnow,indIce,indWater,indIceWet,...
    iterTol,iterMax,iterPerturb,...
    indOpen] = ...
    deal_struct(structure,...
    'zeros_grid','thickness_wetland','timestep_length','energy_balance_tolerance',...
    'index_snow_wetland','index_ice_pack','index_wetland','index_ice_pack_ponding',...
    'iteration_error_tolerance','iteration_max_iterations','iteration_perturbation',...
    'index_open_water');

%--------------------------------------------------------------------------
% Initialize arrays
%--------------------------------------------------------------------------
[subsCond,subsThick,subsTemp,interfEmissivity,qBalance,qLongUp,...
    qCond,qSens,qPrecip,qEvapSnow,qEvapIce,qEvapOpen,interfVapPress,...
    evapSnow,evapIce,evapOpen,qPhase,qLongReflect,qStatic] = deal(zerosGrid);

%--------------------------------------------------------------------------
% Determine properties
%--------------------------------------------------------------------------
%Thermal Conductance of the substrate
subsCond = populate_array(subsCond,condSnow,condIce,condWetland,indSnow,indIce,indOpen);

%Thickness of the substrate
subsThick = populate_array(subsThick,snowThick,iceThick,waterThick(:,1),indSnow,indIce,indOpen);

%Temperature of the substrate
subsTemp = populate_array(subsTemp,snowTemp,iceTemp,waterTemp(:,1),indSnow,indIce,indOpen);

%Relative humidity at the interface
%assume that relative humidity is 1 of all wetland surfaces
interfRelHum = zerosGrid + 1; 

%Emissivity of the interface
interfEmissivity = populate_array(interfEmissivity,snowEmissivity,iceEmissivity,waterEmissivity,...
    indSnow,indIce,indOpen);

%Convert units on precip and dripthrough inputs
dripRain = dripRain / timestepLength;
throughRain = throughRain / timestepLength;
dripSnow = dripSnow / timestepLength;
throughSnow = throughSnow / timestepLength;

%For shortwave radiation use only the fraction absorbed by the
%surface, the rest is absorbed at depth
qRadShort(indSnow) = qRadShort(indSnow) .* snowInterfShortFrac;
qRadShort(indIce) = qRadShort(indIce) .* iceInterfShortFrac;
qRadShort(indOpen) = qRadShort(indOpen) .* waterInterfShortFrac;

%Longwave radiation is not all absorbed
qLongReflect(indWater) = qLongDown(indWater) .* (1 - interfEmissivity(indWater));

%--------------------------------------------------------------------------
%                   Iteration Loop for Interface Temperature
%--------------------------------------------------------------------------
%Calculate the static portion of the energy balance
qStatic(indWater) = qRadShort(indWater) + qLongDown(indWater) - qLongReflect(indWater);

%The first step is to check for instances where snow or ice melting occurs, or freezing of water occurs
%that is ponded on ice or in open water.
%To do this,set the surface temperature everywhere to 0, and observe in the surface
%energy balance is positive (indicating the temperature would tend to
%increase above 0).  If so, then set qPhase=energy imbalance.
%For cells with open water or ponded on ice, qPhase = energy imbalance.  
ind = indWater;
qPhase(ind) = energy_balance(zerosGrid(ind),...
        canopyTemp(ind),subsTemp(ind),rainTemp(ind),snowfallTemp(ind),airTemp(ind),...
        dripRain(ind),throughRain(ind),dripSnow(ind),throughSnow(ind),interfRelHum(ind),...
        airVapPress(ind),airPress(ind),densMoistAir(ind),resistAeroHeatVapor(ind),...
        interfEmissivity(ind),subsCond(ind),subsThick(ind),snowWater(ind),iceWater(ind),...
        indOpen(ind),indSnow(ind),indIce(ind),zerosGrid(ind),qStatic(ind),timestepLength,zerosGrid(ind));
indPhase = ((qPhase > 0) & (indSnow | (indIce & ~indIceWet))) | ((qPhase < 0) & (indIceWet | indOpen));
indNoPhase = ~indPhase;
interfTemp(indPhase) = 0; %set interface temperature at 0 for cells with melting
qPhase(indNoPhase) = 0; %no phase change occurred

%Only loop over cells that did not melt
ind = (indWater) & (indNoPhase); %the size of this index should match the domain size of the model

if any(ind)
    %Newton-Raphson iteration for interface temperature
    error = 1;
    niter = 0;
    while (error>iterTol) && (niter<iterMax)
        tempLast = interfTemp(ind);
        niter = niter + 1;
        f1 = energy_balance(interfTemp(ind),...
            canopyTemp(ind),subsTemp(ind),rainTemp(ind),snowfallTemp(ind),airTemp(ind),...
            dripRain(ind),throughRain(ind),dripSnow(ind),throughSnow(ind),interfRelHum(ind),...
            airVapPress(ind),airPress(ind),densMoistAir(ind),resistAeroHeatVapor(ind),...
            interfEmissivity(ind),subsCond(ind),subsThick(ind),snowWater(ind),iceWater(ind),...
            indOpen(ind),indSnow(ind),indIce(ind),qPhase(ind),qStatic(ind),timestepLength,zerosGrid(ind));
        f2 = energy_balance(interfTemp(ind) + iterPerturb,...
            canopyTemp(ind),subsTemp(ind),rainTemp(ind),snowfallTemp(ind),airTemp(ind),...
            dripRain(ind),throughRain(ind),dripSnow(ind),throughSnow(ind),interfRelHum(ind),...
            airVapPress(ind),airPress(ind),densMoistAir(ind),resistAeroHeatVapor(ind),...
            interfEmissivity(ind),subsCond(ind),subsThick(ind),snowWater(ind),iceWater(ind),...
            indOpen(ind),indSnow(ind),indIce(ind),qPhase(ind),qStatic(ind),timestepLength,zerosGrid(ind));
        interfTemp(ind) = tempLast - (iterPerturb .* f1)./(f2 - f1);
        error = mean(abs(interfTemp(ind) - tempLast));
    end
end

%Now, fetch all of the calculated values with the final interfTemp
ind = indWater;
[qBalance(ind),qLongUp(ind),qCond(ind),qPrecip(ind),qSens(ind),qEvapSnow(ind),...
    qEvapIce(ind),qEvapOpen(ind),evapSnow(ind),evapIce(ind),evapOpen(ind),...
    interfVapPress(ind)] = energy_balance(interfTemp(ind),...
    canopyTemp(ind),subsTemp(ind),rainTemp(ind),snowfallTemp(ind),airTemp(ind),...
    dripRain(ind),throughRain(ind),dripSnow(ind),throughSnow(ind),interfRelHum(ind),...
    airVapPress(ind),airPress(ind),densMoistAir(ind),resistAeroHeatVapor(ind),...
    interfEmissivity(ind),subsCond(ind),subsThick(ind),snowWater(ind),iceWater(ind),...
    indOpen(ind),indSnow(ind),indIce(ind),qPhase(ind),qStatic(ind),timestepLength,zerosGrid(ind));

%Check the energy balance
if assert_LHM()
    assert_LHM(all(abs(qBalance)<tolEnergy),'After iteration, the wetland interface energy balance did not meet the convergence criterion');
end

%--------------------------------------------------------------------------
%                   Finish and Update Stored Quantities
%--------------------------------------------------------------------------
%Calculate total upwelling longwave radiation
qLongUp(indWater) = qLongUp(indWater) + qLongReflect(indWater);

%Update stored quantities
state = update_struct(state,'vap_pressure_interf_wetland',interfVapPress,...
    'temp_interf_wetland',interfTemp,'evap_snow_wetland',evapSnow,'evap_ice_pack',evapIce,...
    'evap_wetland',evapOpen,'q_cond_interf_wetland',qCond,...
    'rad_long_wetland_up',qLongUp,'q_precip_interf_wetland',qPrecip,...
    'q_phase_wetland',qPhase,'sensible_interf_wetland',qSens,...
    'latent_snow_wetland',qEvapSnow,'latent_ice_pack',qEvapIce,'latent_wetland',qEvapOpen);
end

%--------------------------------------------------------------------------
%                           Energy Balance Function
%--------------------------------------------------------------------------
function [qBalance,qLongUp,qCond,qPrecip,qSens,qEvapSnow,qEvapIce,qEvapOpen,...
    evapSnow,evapIce,evapOpen,interfVapPress] = energy_balance(interfTemp,...
    canopyTemp,subsTemp,rainTemp,snowfallTemp,airTemp,...
    dripRain,throughRain,dripSnow,throughSnow,interfRelHum,airVapPress,airPress,...
    densMoistAir,resistAeroHeatVapor,interfEmissivity,subsCond,subsThick,...
    snowWater,iceWater,indOpen,indSnow,indIce,qMelt,qStatic,timestepLength,zerosGrid)

%Calculate the precipitation heat flux, snow is not included in this
%because we don't assume that it will all melt, it is passed to the
%snowpack model
qPrecip = precip_heat_flux(params,dripRain,canopyTemp,interfTemp) + precip_heat_flux(params,throughRain,rainTemp,interfTemp);
%For open cells, include solid precipitation as well
qPrecip(indOpen) = qPrecip(indOpen) + precip_heat_flux(params,dripSnow(indOpen),canopyTemp(indOpen),interfTemp(indOpen)) +...
    precip_heat_flux(params,throughSnow(indOpen),snowfallTemp(indOpen),interfTemp(indOpen));

%Calculate upwelling longwave radiation
qLongUp = longwave_flux(interfTemp,interfEmissivity);

%Calculate heat conduction to the substrate
qCond = conductive_heat_flux(interfTemp,subsTemp,subsCond,subsThick);

%Calculate latent and sensible heat fluxes
qSens = sens_flux(params,interfTemp,airTemp,densMoistAir,resistAeroHeatVapor);

[qEvap,peInterf,interfVapPress] = latent_flux(params,interfTemp,interfRelHum,...
    airVapPress,airPress,densMoistAir,timestepLength,resistAeroHeatVapor,zerosGrid); %No ground resistance


%Calculate evaporation and transpiration moisture limitation, or pe/pt -> actual
[qEvapSnow,qEvapIce,qEvapOpen,evapSnow,evapIce,evapOpen] = actual_ET(...
    peInterf,qEvap,snowWater,iceWater,indSnow,indIce,indOpen,zerosGrid);

%Calculate the final balance
qBalance = qStatic - qLongUp - qCond + qPrecip - qMelt - qEvapSnow - qEvapIce - qEvapOpen - qSens;
end
    
%--------------------------------------------------------------------------
%            Limit Evaporation based on Moisture Availability 
%--------------------------------------------------------------------------
function [qEvapSnow,qEvapIce,qEvapOpen,evapSnow,evapIce,evapOpen] = actual_ET(...
    peInterf,qEvap,snowWater,iceWater,indSnow,indIce,indOpen,zerosGrid)
%Intialize output and internal arrays
[evapSnow,evapIce,evapOpen,qEvapSnow,qEvapIce,qEvapOpen] = deal(zerosGrid);
%For snow-covered wetlands
if any(indSnow)
    evapSnow(indSnow) = min(peInterf(indSnow),snowWater(indSnow)); %can be negative
    qEvapSnow(indSnow) = abs(evapSnow(indSnow) ./ peInterf(indSnow)) .* qEvap(indSnow);
    qEvapSnow(isnan(qEvapSnow)) = 0;
end
%For bare ice covered wetlands
if any(indIce)
    evapIce(indIce) = min(peInterf(indIce),iceWater(indIce)); %can be negative
    qEvapIce = abs(evapIce(indIce) ./ peInterf(indIce)) .* qEvap(indIce);
    qEvapIce(isnan(qEvapIce)) = 0;
end
%For open wetlands, no limitations on evaporation here
if any(indOpen)
    evapOpen(indOpen) = peInterf(indOpen); %can be negative
    qEvapOpen(indOpen) = qEvap(indOpen);
end
end