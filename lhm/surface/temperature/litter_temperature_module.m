function  state = litter_temperature_module(state,structure,params)
%LITTER_TEMPERATURE_MODULE  Calculates litter temperature components.
%   litter_temperature_module()
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[snowMelt,interfTemp,litterPrevTemp,qCondInterf,qCondSnow,...
    litterFrozenFrac,condSoil,soilTemp,...
    waterLitter,meltTemp,qRadSnow,totalPrecip] = deal_struct(state,...
    'snow_melt_upland','temp_interf_upland','temp_litter','q_cond_interf_upland','q_cond_snow_upland',...
    'litter_frozen_fraction','therm_cond_soil','temp_soil',...
    'water_litter','temp_snow_melt_upland','rad_transmit_snow_upland','total_throughfall');
[litterThick,condOrganic,condWater,...
    condIce,condAir,litterPorosity,cpOrganic,...
    cpWater,cpIce,densityLitter,densityWater,densityIce,...
    litterCoverFrac,heatFusion,maxDeltaT,litterSatCap,psiDry,psiB] = deal_struct(params,...
    'litter_thickness','therm_cond_organic','therm_cond_water',...
    'therm_cond_ice','therm_cond_air','litter_porosity','heat_cap_organic',...
    'heat_cap_water','heat_cap_ice','litter_density','density_water','density_ice',...
    'litter_fraction','latent_heat_fusion','litter_max_delta_T','litter_sat_cap',...
    'litter_dry_potential','litter_potential_exponent');
[zerosGrid,timestepLength,soilThick,...
    indLitter,indSnow,indBare] = ...
    deal_struct(structure,...
    'zeros_grid','timestep_length','thickness_soil',...
    'index_litter','index_snow_upland','index_bare_litter');

%--------------------------------------------------------------------------
%Initialize Arrays
%--------------------------------------------------------------------------
[qAdvect,qCondUp,condLitter,qCondDown,qNet,cpLitter,densityComposite,qStorage,...
    advectTemp,advectRate,deltaT] = deal(zerosGrid);
litterTemp = litterPrevTemp;

%--------------------------------------------------------------------------
% Rescale storage according to litter fraction
%--------------------------------------------------------------------------
waterLitter(indLitter) = waterLitter(indLitter) ./ litterCoverFrac(indLitter); %units are m/litter_area

%--------------------------------------------------------------------------
%Calculate qAdvect and qCondUp
%--------------------------------------------------------------------------
%Calculate advected energy infiltrating from snowmelt or the interface
ind = (totalPrecip > 0) | (snowMelt > 0);
if any(ind)
    advectRate(ind) = populate_array(advectRate(ind),totalPrecip,snowMelt,...
        indBare(ind),indSnow(ind)) / timestepLength; %convert units on hydrologic fluxes
    advectTemp(ind) = populate_array(advectTemp(ind),interfTemp(ind),meltTemp(ind),...
        indBare(ind),indSnow(ind));
    qAdvect(ind) = precip_heat_flux(params,advectRate,advectTemp(ind),litterTemp(ind)); %positive inward
end

%Calculate net conductive flux upward
qCondUp = populate_array(qCondUp,-qCondInterf,-qCondSnow,indBare,indSnow);

%--------------------------------------------------------------------------
%Calculate the original thermal conductance, heat capacity and density
%--------------------------------------------------------------------------
ind = indLitter;
litterPorosity = litterPorosity + zerosGrid;
waterFrac = waterLitter(ind) ./ litterSatCap(ind) .* litterPorosity(ind);

%Calculate the thermal conductivity of the litter
%First, calculate the effective thickness of each of the four litter
%components
weightOrganic = 1 - litterPorosity(ind); %for matrix
weightWater = (1 - litterFrozenFrac(ind)) .* waterFrac; %water
weightIce = litterFrozenFrac(ind) .* waterFrac; %ice
weightAir = 1 - weightOrganic - weightWater - weightIce; %air

condLitter(ind) = weighted_mean('geometric',condOrganic,condWater,condIce,condAir,...
    weightOrganic,weightWater,weightIce,weightAir);
cpLitter(ind) = weighted_mean('arithmetic',cpOrganic,cpWater,cpIce,...
    weightOrganic*densityLitter,weightWater*densityWater,weightIce*densityIce);
densityComposite(ind) = weighted_mean('arithmetic',densityLitter,densityWater,densityIce,...
    weightOrganic,weightWater,weightIce);    
    
%--------------------------------------------------------------------------
%Calculate qCondDown
%--------------------------------------------------------------------------
%Calculate the combined thermal conductivity of the litter-soil interface
condLitterSoil = weighted_mean('harmonic',condLitter(ind),condSoil(ind,1),...
    litterThick(ind),soilThick(ind,1));
condThick = litterThick(ind) + soilThick(ind,1);
qCondDown(ind) = conductive_heat_flux(litterPrevTemp(ind),soilTemp(ind,1),...
    condLitterSoil,condThick); %positive out of the litter

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%Calculate the new litter layer temperature
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%First, calculate the energy budget of the litter layer, units are W/m2
%--------------------------------------------------------------------------
ind = indLitter;
qNet(ind) = qRadSnow(ind) - qCondUp(ind) - qCondDown(ind) + qAdvect(ind);
qStorage(ind) = qNet(ind);

%--------------------------------------------------------------------------
%First, calculate delta T prior to any phase change, moving toward T = 0;
%--------------------------------------------------------------------------
%Calculate the indices of non-frozen litter with negative temperature
%change or frozen litter with positive temperature change.  i.e., those
%cells moving toward phase change
ind = ((litterTemp>0) & (qNet<0)) | ((litterTemp<0) & (qNet>0)); 
if any(ind)
    [qNet(ind),litterTemp(ind)] = temp_change_pre_phase(qNet(ind),litterTemp(ind),...
        cpLitter(ind),densityComposite(ind),litterThick(ind),timestepLength);
end

%--------------------------------------------------------------------------
%Handle partial phase change prior to any remaining temperature change
%--------------------------------------------------------------------------
%Calculate the indices of at least partially frozen litter that is warming, and
%therefore melting, or the indices of incompletely frozen litter that is
%colling, and therefore freezing
ind = ((litterFrozenFrac>0) & (qNet>0)) | ((litterFrozenFrac<1) & (qNet<0)) & (waterLitter > 0); %melting or freezing conditions
if any(ind)
    [qNet(ind),litterFrozenFrac(ind)] = phase_change(qNet(ind),litterFrozenFrac(ind),...
        waterLitter(ind),heatFusion,densityIce,timestepLength,zerosGrid(ind));
    %Now calculate updated multiphase heat capacity and density
    weightWater = waterLitter(indLitter) .* (1 - litterFrozenFrac(indLitter)); %water
    weightIce = waterLitter(indLitter) .* litterFrozenFrac(indLitter); %ice
    cpLitter(indLitter) = weighted_mean('arithmetic',cpOrganic,cpWater,cpIce,...
        weightOrganic*densityLitter,weightWater*densityWater,weightIce*densityIce);
    densityComposite(indLitter) = weighted_mean('arithmetic',densityLitter,densityWater,densityIce,...
        weightOrganic,weightWater,weightIce);
end

%--------------------------------------------------------------------------
%Finally, calculate remaining temperature change
%--------------------------------------------------------------------------
ind = indLitter;
[litterTemp(ind),deltaT(ind)] = temp_change_post_phase(litterTemp(ind),qNet(ind),cpLitter(ind),...
    densityComposite(ind),litterThick(ind),timestepLength);

%Check to see if deltaT exceeds maxDeltaT
ratio = maxDeltaT ./ abs(deltaT);
ind = (ratio < 1) & indLitter;
if any(ind)
    signDelta = sign(deltaT(ind));
    litterTemp(ind) = litterTemp(ind) - deltaT(ind) + signDelta .* maxDeltaT;
    excessEnergy = qNet(ind) .* (1 - ratio(ind));
    qCondDown(ind) = qCondDown(ind) + excessEnergy; %assign remaining energy to qCondDown
    %qCondDown is positive out of the litter, and into the soil, thus the
    %sign is the same as qNet
    qStorage(ind) = qStorage(ind) - excessEnergy;
end

%Error Check
if assert_LHM
    assert_LHM(all(~isnan(litterTemp)),'An NaN appeared in the litter temperature');
end

%--------------------------------------------------------------------------
%Calculate litter suction potential for use in the interface_temperature
%module
%--------------------------------------------------------------------------
psi = psiDry .* (densityWater .* waterLitter ./ densityComposite).^(-psiB);

%--------------------------------------------------------------------------
% Rescale storage according to litter fraction
%--------------------------------------------------------------------------
qStorage(indLitter) = qStorage(indLitter) .* litterCoverFrac(indLitter);

%--------------------------------------------------------------------------
%Update the state controller
%--------------------------------------------------------------------------
state = update_struct(state,'therm_cond_litter',condLitter,'matric_potential_litter',psi,...
    'temp_litter',litterTemp,'energy_litter',qStorage,'q_cond_litter',qCondDown,...
    'litter_frozen_fraction',litterFrozenFrac);
%The litter->soil heat flux is defined as positive out of the litter
%Note, the units of energy_litter are W/cell area, and the units of
%q_cond_litter_soil are W/litter area
end
