function state = bed_sediment_temperature_model(state,structure,params)
%BED_SEDIMENT_TEMPERATURE_MODEL  Calculate the temperature of lake bed sediments
%   bed_sediments_temperature_model()
%
%   Descriptions of Input Variables:
%   Fetches all inputs from the data controller
%
%   Descriptions of Output Variables:
%   Writes all outputs to the data controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-01
% Copyright 2008 Michigan State University.

[qCondLake,bedTemp,radShort] = deal_struct(state,...
    'q_cond_lake_bed','temp_bed_sediment','rad_transmit_wetland');
[condBed,cpBed,densBed,deepTemp] = deal_struct(params,...
    'bed_sediment_therm_cond','bed_sediment_heat_cap','bed_sediment_wet_density','soil_deep_temp');
[zerosGrid,timestepLength,ind,bedThick] = deal_struct(structure,...
    'zeros_grid','timestep_length','index_wetland','thickness_bed_sediment');
    
%--------------------------------------------------------------------------
%Initialize arrays used herein
%--------------------------------------------------------------------------
[qCondDeep,qNet] = deal(zerosGrid);

%--------------------------------------------------------------------------
%Calculate qCondDeep, positive downward
%--------------------------------------------------------------------------
qCondDeep(ind) = -condBed(ind) .* (deepTemp - bedTemp(ind)) ./ ...
    bedThick(ind);

%--------------------------------------------------------------------------
%Calculate energy balance
%--------------------------------------------------------------------------
qNet(ind) = radShort(ind) + qCondLake(ind) - qCondDeep(ind);

%--------------------------------------------------------------------------
%Calculate bed sediment temperature
%--------------------------------------------------------------------------
bedTemp(ind) = temp_change_post_phase(bedTemp(ind),qNet(ind),cpBed(ind),densBed(ind),...
    bedThick(ind),timestepLength);

%--------------------------------------------------------------------------
%Update the state controller
%--------------------------------------------------------------------------
state = update_struct(state,'q_cond_deep_wetland',qCondDeep,...
    'temp_bed_sediment',bedTemp,'energy_bed_sediment',qNet);