function state = soil_temperature_module(state,structure,params)
%SOIL_TEMPERTATURE_MODULE  Soil temperature and heat flux module
%   soil_temperature_module()
%
%   Descriptions of Input Variables:
%   all inputs are taken from the data controller
%
%   Descriptions of Output Variables:
%   all outputs are written to the data controller
%
%   Example(s):
%   none
%
%   See also: upland_controller

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-21
% Copyright 2008 Michigan State University.

[qCondInterf,qCondSnow,qCondLitter,frozenFrac,soilTemp,...
    litterTemp,interfTemp,meltTemp,infiltration,soilWater,...
    soilWaterFlux,qRadSnow] = ...
    deal_struct(state,...
    'q_cond_interf_upland','q_cond_snow_upland','q_cond_litter','soil_frozen_fraction','temp_soil',...
    'temp_litter','temp_interf_upland','temp_snow_melt_upland','soil_infiltration','water_soil',...
    'soil_water_flux','rad_transmit_snow_upland');
[litterFrac,condMineral,condWater,condIce,...
    condAir,cpMineral,cpWater,cpIce,densityMineral,...
    densityWater,densityIce,deepTemp,porosity,heatFusion,...
    soilSatCap] = ...
    deal_struct(params,...
    'litter_fraction','therm_cond_mineral','therm_cond_water','therm_cond_ice',...
    'therm_cond_air','heat_cap_mineral','heat_cap_water','heat_cap_ice','density_mineral',...
    'density_water','density_ice','soil_deep_temp','soil_porosity','latent_heat_fusion',...
    'soil_sat_cap');
[zerosGrid,timestepLength,soilZerosGrid,soilThick,...
    innerTimesteps,tolEnergy,indUpland,...
    indLitterBare,indLitter,indSoilBare,indSoilSnow,...
    indSoil,numLayers] = ...
    deal_struct(structure,...
    'zeros_grid','timestep_length','zeros_grid_soil','thickness_soil',...
    'soil_temp_inner_timesteps','energy_balance_tolerance','index_upland',...
    'index_bare_litter','index_litter','index_bare_soil','index_snow_soil',...
    'index_soil','num_layers_soil');

%--------------------------------------------------------------------------
%Initialize arrays used herein
%--------------------------------------------------------------------------
[qAdvect,qCondUp,overTemp,qCondDeep,qNetFirst] = deal(zerosGrid);
[condSoil,cpSoil,densComposite,cpSoilSpecific,qStorage,advectFluxPot] = deal(soilZerosGrid);

%--------------------------------------------------------------------------
%Calculate the advected energy into the first layer
%--------------------------------------------------------------------------
%Calculate the advected heat flux, positive into first soil layer
indInfil = (infiltration > 0);
if any(indInfil)
    %Calculate the temperature of the overburden
    %Account for fractional litter coverage
    %Calculate for cells where no snow lies on top of the litter
    ind = indLitterBare;
    overTemp(ind) = weighted_mean('arithmetic',litterTemp(ind),interfTemp(ind),...
        litterFrac(ind),(1-litterFrac(ind)));
    %Calculate for cells where snow lies on top of the litter
    ind = ~indLitterBare;
    overTemp(ind) = weighted_mean('arithmetic',litterTemp(ind),meltTemp(ind),...
        litterFrac(ind),(1-litterFrac(ind)));
    %Account for either snow cover or full exposure (i.e., coupling with
    %the interface
    ind = indSoilBare;
    overTemp(ind) = populate_array(overTemp(ind),interfTemp(ind),meltTemp(ind),...
        indSoilBare(ind),indSoilSnow(ind));
    %Finally, calculate the advected energy
    ind = indInfil;
    rateInfil = infiltration(ind) / timestepLength;
    qAdvect(ind) = precip_heat_flux(params,rateInfil,overTemp(ind),soilTemp(ind,1));
end

%--------------------------------------------------------------------------
%Calculate net conductive flux upward
%--------------------------------------------------------------------------
%Account for partial litter coverage
qCondLitter(indLitter) = (qCondInterf(indLitter) + qCondSnow(indLitter)) .* (1 - litterFrac(indLitter)) + ...
    qCondLitter(indLitter) .* litterFrac(indLitter); %positive into soil as originally defined
qCondUp = populate_array(qCondUp,-qCondInterf,-qCondSnow,-qCondLitter,indSoilBare,indSoilSnow,indLitter);
%qCondUp is positive away from the soil

%--------------------------------------------------------------------------
%Calculate the net energy flux into the first layer, positive outward
%--------------------------------------------------------------------------
ind = indUpland;
qNetFirst(ind) = -qAdvect(ind) - qCondUp(ind) + qRadSnow(ind) .* (1 - litterFrac(ind));

%--------------------------------------------------------------------------
%Define multiphase thermal conductivity, heat capacity, and density
%--------------------------------------------------------------------------
ind = indSoil;
waterFrac = soilWater(ind) ./ soilSatCap(ind) .* porosity(ind);

weightMineral = 1 - porosity(ind);
weightWater = (1 - frozenFrac(ind)) .* waterFrac;
weightIce = frozenFrac(ind) .* waterFrac;
weightAir = 1 - weightMineral - weightWater - weightIce;

condSoil(ind) = weighted_mean('geometric',condMineral,condWater,condIce,condAir,...
    weightMineral,weightWater,weightIce,weightAir);
cpSoil(ind) = weighted_mean('arithmetic',cpMineral,cpWater,cpIce,...
    weightMineral.*densityMineral,weightWater.*densityWater,weightIce.*densityIce);
densComposite(ind) = weighted_mean('arithmetic',densityMineral,densityWater,densityIce,...
    weightMineral,weightWater,weightIce); 
cpSoilSpecific(ind) = cpSoil(ind) .* densComposite(ind);  %units are J/m^3/K

%--------------------------------------------------------------------------
%Calculate net conductive flux downward from final soil layer, positive
%downward
%--------------------------------------------------------------------------
ind = indUpland;
qCondDeep(ind) = conductive_heat_flux(soilTemp(ind,end),deepTemp,...
    condSoil(ind,end),soilThick(ind,end));

%--------------------------------------------------------------------------
%Define the advective flux potential in units of W/m^2/K
%--------------------------------------------------------------------------
%defined as positive upward, soil water flux is positive downward
advectFluxPot(ind,:) = -soilWaterFlux(ind,:) * cpWater * densityWater / timestepLength;

%--------------------------------------------------------------------------
%Run soil_heat_flux_1d, calculate SoilTemp and deltaT (used for energy
%balance calcs)
%--------------------------------------------------------------------------
[soilTemp(indSoil),deltaT,advectFlux] = soil_heat_flux_1d(soilTemp(ind,:),condSoil(ind,:),soilThick(ind,:),...
    advectFluxPot(ind,:),cpSoilSpecific(ind,:),qNetFirst(ind),qCondDeep(ind),...
    timestepLength,innerTimesteps,numLayers,soilZerosGrid(ind,:));
qStorage(ind,:) = deltaT .* cpSoilSpecific(ind,:) .* soilThick(ind,:) / timestepLength + advectFlux/2;

%--------------------------------------------------------------------------
%Handle phase change and subsequent warming/cooling
%--------------------------------------------------------------------------
ind = ((frozenFrac>0) & (soilTemp>0)) | ((frozenFrac<1) & (soilTemp<0));
if any(ind(:))
    thermalInertia = cpSoilSpecific(ind) .* soilThick(ind);
    %Calculate the amount of energy stored in the soil by warming/cooling prior to
    %phase change
    phaseChangeEnergy = soilTemp(ind) .* thermalInertia / timestepLength; %Units are W/m^2
    %Now, calculate phase change, and the amount of energy left to
    %warm/cool the soil
    [qWarm,frozenFrac(ind)] = phase_change(phaseChangeEnergy,frozenFrac(ind),soilWater(ind),...
        heatFusion,densityIce,timestepLength,soilZerosGrid(ind));
    %Warm or cool the soil according to the remaining energy
    soilTemp(ind) = qWarm * timestepLength ./ thermalInertia;
end

%--------------------------------------------------------------------------
%Error checks
%--------------------------------------------------------------------------
if assert_LHM
    balance = sum(qStorage,2) - qNetFirst - qCondDeep;
    assert_LHM(all(abs(balance(:)) < tolEnergy),'Soil temperature module energy balance error exceeds tolerance');
    assert_LHM(all(~isnan(soilTemp(:))),'An NaN has appeared in the soil temperature array');
end

%--------------------------------------------------------------------------
%Update the state in the data_controller
%--------------------------------------------------------------------------
state = update_struct(state,'temp_soil',soilTemp,'soil_frozen_fraction',frozenFrac,...
    'therm_cond_soil',condSoil,'energy_soil',qStorage,'q_cond_deep_upland',qCondDeep);
end