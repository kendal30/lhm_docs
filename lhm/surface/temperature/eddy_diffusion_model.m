function waterTemp = eddy_diffusion_model(waterTemp,surfEnergy,botEnergy,phi,...
    delZ,sumDiffusion,cvWater,timestepLength)
%EDDY_DIFFUSION_MODEL  Calculates lake temperature using an eddy diffusion model
%   waterTemp = eddy_diffusion_model(waterTemp,surfEnergy,botEnergy,phi,...
%       delZ,sumDiffusion,cvWater,timestepLength)
%
%   This formulation, in terms of solving the eddy-diffusion 1-D heat
%   transport equations, comes from the LSM documentation.  The equations
%   are written in Crank-Nicholson form and solved via a tri-diagonal
%   solver.
%
%   Descriptions of Input Variables:
%   waterTemp: temperature of the water, MxN array, where M is the number
%       of modeled cells in each layer (or a subset thereof), and N is the
%       number of vertical layers, units are degrees C
%   surfEnergy: the surface energy balance, where positive values indicate
%       a flux of energy into the surface layer.  Excludes shortwave radiation.
%       Array is is Mx1.  Units are W/m^2
%   botEnergy: the bottom energy balance, positive values indicate a flux
%       of energy into the bottom lake layer.  Array size is Mx1. Units are W/m^2
%   phi: flux of shortwave radiation absorbed by each layer, units
%       are W/m^2, array size is MxN
%   delZ: thickness of each lake layer, array size is MxN.
%   sumDiffusion: the sum of the thermal and eddy diffusion coefficients,
%       array is is MXN, units are m^2/s
%   cvWater: constant pressure, volumetric heat capacity of water, array is
%       is MxN, units are m^2/s (is a function of water density)
%   timestepLength: timestep length in seconds, scalar value
%
%   Descriptions of Output Variables:
%   waterTemp: temperature of the water after 1 timestep, same size and
%       units as input waterTemp
%
%   Example(s):
%   none
%
%   See also: wetland_temperature_module

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-30
% Copyright 2008 Michigan State University.

%--------------------Write Crank-Nicholson form of equations---------------
%set up tridiagonal system of equations via Crank-Nicholson method
[m1,m2,a,b,c,r] = deal(zeros(size(waterTemp)));
numLayers = size(waterTemp,2);

%for shorthand notation, we define m1, m2, m3 as in LSM documentation
m1(:,2:end) = delZ(:,1:end-1) ./ sumDiffusion(:,1:end-1) + ...
    delZ(:,2:end) ./ sumDiffusion(:,2:end);
m2(:,1:end-1) = m1(:,2:end);
m3 = timestepLength ./ delZ;
precalc1 = m3 ./ m1;
precalc2 = m3 ./ m2;

%first layer
ind = 1;
b(:,ind) = 1 + precalc2(:,ind);
c(:,ind) = -precalc2(:,ind);
r(:,ind) = waterTemp(:,ind) + (surfEnergy + phi(:,ind)) .* m3(:,ind) ./ cvWater(:,ind) - ...
    (waterTemp(:,ind) - waterTemp(:,ind+1)) .* precalc2(:,ind);

%middle layers
ind = (2:numLayers-1);
a(:,ind) = -precalc1(:,ind);
b(:,ind) = 1 + precalc1(:,ind) + precalc2(:,ind);
c(:,ind) = -precalc2(:,ind);
r(:,ind) = waterTemp(:,ind) + phi(:,ind).* m3(:,ind) ./ cvWater(:,ind) + ...
    (waterTemp(:,ind-1) - waterTemp(:,ind)) .* precalc1(:,ind) - ...
    (waterTemp(:,ind) - waterTemp(:,ind+1)) .* precalc2(:,ind);

%bottom layer
ind = numLayers;
a(:,ind) = -precalc1(:,ind);
b(:,ind) = 1 + precalc1(:,ind);
r(:,ind) = waterTemp(:,ind) + (botEnergy + phi(:,end)) .* m3(:,ind) ./ cvWater(:,ind) + ...
    (waterTemp(:,ind-1) - waterTemp(:,ind)) .* precalc1(:,ind);

%solve for water temperature with tridiagonal solver
waterTemp=tridiagonal_solver(a,b,c,r);