function [state,structure] = wetland_controller(state,structure,params,modules)
%WETLAND_CONTROLLER  Controls wetland temperature and moisture codes.
%   wetland_controller()
%
%   Descriptions of Input Variables:
%   none, all inputs come from data controller
%
%   Descriptions of Output Variables:
%   nont, all inputs come from data controller
%
%   Example(s):
%   
%
%   See also: upland_controller canopy_controller 

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-04-02
% Copyright 2008 Michigan State University.

%Save water storage as previous timestep water storage
state = update_struct(state,'prev_water_wetland',state.water_wetland,...
    'prev_water_snow_wetland',state.water_snow_wetland,'prev_ice_pack_precip_store',state.ice_pack_precip_store,'prev_water_ice_pack_ponding',state.water_ice_pack_ponding);

[water_wetland,temp_interface_wetland,snow_wetland,ice_wetland,temp_wetland,temp_bed_sediment] = deal_struct(modules,...
    'water_wetland','temp_interface_wetland','snow_wetland','ice_wetland','temp_wetland','temp_bed_sediment');

%Run the interface temperature code
state = temp_interface_wetland(state,structure,params);

%Run the snowpack module
state = snow_wetland(state,structure,params);

%Run the ice module
[state,structure] = ice_wetland(state,structure,params);

%Run the wetland temperature module
state = temp_wetland(state,structure,params);

%Run the bed sediments temperature module
state = temp_bed_sediment(state,structure,params);

%Run the wetland storage module
[state,structure] = water_wetland(state,structure,params);

end
