function state = v1_soil_moisture_module(state,structure,params)
% modified 04/12/2017 by T. Xu.
[totalRain,snowMelt,depressionWater,evaporation,...
    waterSoil,transpiration,frozenImperm,waterSnow,...
    totalSnow,dischargeUpland,wtDepth,...
    evapDepth,irrigWater] = deal_struct(state,...
    'total_throughfall_rain','snow_melt_upland','water_depression','evap_soil',...
    'water_soil','transp_upland','impermeable_frozen','water_snow_upland',...
    'total_throughfall_snow','groundwater_discharge_upland','water_table_depth_upland',...
    'soil_evap_depth','irrigation_water_subsurface');
[kSat,porosity,thetaResid,vanGenAlpha,vanGenN,...
    satCap,fieldCap,infilCap,maulemK0,maulemL,...
    evapDepthResetThresh] = deal_struct(params,...
    'soil_ksat','soil_porosity','soil_theta_r','soil_alpha','soil_N',...
    'soil_sat_cap','soil_field_cap','soil_infil_cap','soil_k0','soil_L',...
    'soil_evap_depth_reset_thresh');
[indBare,indSnow,indUpland,indWetland,indSoil,zerosGrid,...
    impervFrac,timestepLength,soilThick,zerosGridSoil,onlineCoupling,...
    irrigActive] = ...
    deal_struct(structure,...
    'index_bare_soil','index_snow_upland','index_upland','index_wetland','index_soil','zeros_grid',...
    'fraction_impervious','timestep_length','thickness_soil','zeros_grid_soil','groundwater_coupling_online',...
    'irrigation_enabled');
if irrigActive
    [irrigStruct] = deal_struct(structure,'irrigation');
end
if onlineCoupling == 1
    [upSatFrac,rechargeWTUpland] = deal_struct(state,'soil_wt_fraction','recharge_WT_change_upland');
    % Update unsaturated soil thickness
    unsatThick = soilThick .* (1 - upSatFrac);
    fieldCap = fieldCap .* ( 1 - upSatFrac );
    satCap = satCap .* (1- upSatFrac);
end
%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[availableWater,infiltration,precipExcess,deepPerc,exfiltration,...
    urbExcess,urbUpRunon,urbWetRunon,urbConcentrate,kAir] = deal(zerosGrid);
[drainage,kTheta,dkdTheta,psi,dpsidTheta] = deal(zerosGridSoil);

%--------------------------------------------------------------------------
%Calculate available moisture for infiltration
%--------------------------------------------------------------------------
%For bare soil either exposed or snow-covered
%Update the indSnow index here
indSnow = indSnow | (snowMelt > 0) | (waterSnow > 0) | (totalSnow > 0);
indBare = ~indSnow; %indBare, commented out because v1_soil_moisture_module doesn't use litter
availableWater = populate_array(availableWater,totalRain,snowMelt,indBare,indSnow);
%Add in depression storage and groundwater discharge to uplands
availableWater(indUpland) = availableWater(indUpland) + depressionWater(indUpland) + dischargeUpland(indUpland);

%--------------------------------------------------------------------------
% Add in subsurface irrigation, if any
%--------------------------------------------------------------------------
if any(irrigWater)
    % Apply to our pre-calculated soil layer
    irrigWaterSoil = repmat(irrigwater,[1,size(zerosGridSoil,2)]);
    waterSoil(irrigStruct.soilIrrigLay) = waterSoil(irrigStruct.soilIrrigLay) + irrigWaterSoil(irrigStruct.soilIrrigLay);
end


%--------------------------------------------------------------------------
%Calculate infiltration and precip excess
%--------------------------------------------------------------------------
ind = (availableWater > 0);
if any(ind(:))
    %Calculate urban runoff, urbBal is the amount of available water that
    %is now part of the urban acccounting, effectively urbBal =
    %availableWater - urbInfil - urbExcess - urbWetRunon
    indPervious = (impervFrac.pervious > 0);
    [urbExcess(ind),urbUpRunon(ind),urbWetRunon(ind)] = impervious_hydrology_module(...
        availableWater(ind),impervFrac.upland(ind),impervFrac.wetland(ind),impervFrac.runoff(ind),...
        indWetland(ind),indPervious(ind));

    %The depth of water available for infiltration in the non-urban part of
    %each cell is now the original depth plus the urban upland runon
    perviousWater = availableWater(ind) .* impervFrac.pervious(ind);
    urbConcentrate(ind) = perviousWater + urbUpRunon(ind);

    %Check to see quickly if any water is greater than the infiltration
    %capacity (steady state) at this point, if so, use Green-Ampt to
    %calculate infiltration rate for this timestep
    indGA = urbConcentrate > (infilCap*timestepLength);
    if any(indGA)
        %Calculate psi in the first soil layer
        theta = waterSoil(indGA,1) ./ soilThick(indGA,1);
        [~,~,psiGA] = van_genuchten_model(theta,porosity(indGA,1),thetaResid(indGA,1),...
            vanGenAlpha(indGA,1),vanGenN(indGA,1),kSat(indGA,1),...
            maulemK0(indGA,1),maulemL(indGA,1));

        %Now, calculate total time-step infiltration according to Green
        %Ampt
        gaInfilCap = abs(psiGA) .* kSat(indGA,1) .* (porosity(indGA,1) - theta) ./ ...
            (urbConcentrate(indGA) - infilCap(indGA)*timestepLength);

        %Update infilCap
        infilCap(indGA) = max(infilCap(indGA),gaInfilCap); %while it shouldn't be possible to have this be less, I will check here anyway
    end
    %Calculate infiltration in these areas, first determine the soil-freezing modified infiltration capacity
    modInfilCap = infilCap(ind) * timestepLength  .* (1 - frozenImperm(ind)) .* impervFrac.pervious(ind);

    %Now, calculate infiltration, which only occurs in permeable areas
    infiltration(ind) = min(urbConcentrate(ind),modInfilCap);

    % 4/12/2017 added by T. XU
    % for flooded cell set to zero
    if onlineCoupling == 1
        ind_sat = upSatFrac(:,1) == 1;
        infiltration(ind_sat) = 0;
    end

    %Add infiltration to the first layer
    waterSoil(ind,1) = waterSoil(ind,1) + infiltration(ind);

    % 4/12/2017 added by T. Xu
    % when exceeding saturation, send infiltration back
    if onlineCoupling == 1
        ind_sat = waterSoil(:,1) > satCap(:,1);
        satExcess = waterSoil(ind_sat,1) - satCap(ind_sat,1);
        waterSoil(ind_sat) = satCap(ind_sat,1);
        infiltration(ind_sat) = infiltration(ind_sat) - satExcess;
    end

    %Now, calculate the total precip. excess
    precipExcess(ind) = urbConcentrate(ind) - infiltration(ind) + urbExcess(ind);
end

%--------------------------------------------------------------------------
%Remove evaporation and transpiration, update soil evaporative depth and 
%reset if needed
%--------------------------------------------------------------------------
%Identify the layer in which evaporation is occuring
[indEvap,soilLayerTops] = find_soil_layer(evapDepth,soilThick);

%Remove evporation
waterSoil(indEvap(indUpland)) = waterSoil(indEvap(indUpland)) - evaporation(indUpland);

%Update depth to evaporative layer
soilTheta(indUpland,:) = waterSoil(indUpland,:) ./ soilThick(indUpland,:);
evapDepth(indUpland) = soil_evaporation_depth(evapDepth(indUpland),evaporation(indUpland),...
    soilTheta(indUpland),thetaResid(indEvap(indUpland)),soilThick(indEvap(indUpland)),...
    soilLayerTops(indEvap(indUpland)));

%Limit it to the deepest soil layer
maxThick = sum(soilThick,2); %should just calculate this once - MOVE TO INITIALIZE STRUCTURE
indTooDeep = (evapDepth >= maxThick) & indUpland;
evapDepth(indTooDeep) = maxThick(indTooDeep)-0.01;
    
%Reset soil evaporative depth, if infiltration exceeds a threshold
%Reset it here because otherwise evaporation may exceed available first
%layer water (which is checked in v1_ET_upland
indReset = indUpland & (infiltration > evapDepthResetThresh);
evapDepth(indReset) = 0;

%Remove transpiration
waterSoil(indSoil) = waterSoil(indSoil) - transpiration(indSoil);

%--------------------------------------------------------------------------
%Calculate subsurface moisture redistribution (no lateral drainage)
%--------------------------------------------------------------------------
% Define a minimum water threshold to test for
minTheta = thetaResid(indSoil) .* 1.01;
minWater = zerosGridSoil;
minWater(indSoil) = minTheta .* soilThick(indSoil);
maxWater = satCap;

indFree = ind; %use free-drainage for cells that had infiltration
indRich = indUpland & ~ind; %use the richards equation for other cells
if onlineCoupling == 1
    % added 4/12/2017, T. XU
    %Calculate unsaturated conductivity
    theta = waterSoil(indUpland,:) ./ unsatThick(indUpland,:);
    [kTheta] = van_genuchten_model(theta,porosity(indUpland,:),thetaResid(indUpland,:),...
        vanGenAlpha(indUpland,:),vanGenN(indUpland,:),kSat(indUpland,:),...
        maulemK0(indUpland,:),maulemL(indUpland,:));
    %Calculate the redistribution of moisture using the 1-D bucket free
    %drainage model
    [waterSoil(indUpland,:),deepPerc(indUpland),exfiltration(indUpland),...
       drainage(indUpland,:),rechargeWTUpland] = ...
                couple_free_drainage_model(waterSoil(indUpland,:),...
                satCap(indUpland,:),fieldCap(indUpland,:),kSat(indUpland,:),kTheta,...
                unsatThick(indUpland,:),zerosGridSoil(indUpland,:),timestepLength,upSatFrac(indUpland,:),rechargeWTUpland(indUpland));
else
    %Calculate unsaturated conductivity
    theta = waterSoil(indUpland,:) ./ soilThick(indUpland,:);
    [kTheta(indUpland,:),dkdTheta(indUpland,:),psi(indUpland,:),dpsidTheta(indUpland,:)] = ...
        van_genuchten_model(theta,porosity(indUpland,:),thetaResid(indUpland,:),...
        vanGenAlpha(indUpland,:),vanGenN(indUpland,:),kSat(indUpland,:),...
        maulemK0(indUpland,:),maulemL(indUpland,:));

    %Calculate the redistribution of moisture using the 1-D bucket free
    %drainage model
    [waterSoil(indFree,:),deepPerc(indFree),exfiltration(indFree),drainage(indFree,:)] = ...
        free_drainage_model(waterSoil(indFree,:),...
        satCap(indFree,:),kSat(indFree,:),kTheta(indFree,:),...
        soilThick(indFree,:),zerosGridSoil(indFree,:),timestepLength);

    %Calculate the redistribution of moisture using full unsaturated hydraulic gradients
    [waterSoil(indRich,:),deepPerc(indRich),drainage(indRich,:)] = richards_equation_model(...
        waterSoil(indRich,:),zerosGridSoil(indRich,:),...
        zerosGrid(indRich,:),zerosGrid(indRich),...
        kTheta(indRich,:),dkdTheta(indRich,:),psi(indRich,:),dpsidTheta(indRich,:),...
        soilThick(indRich,:),minWater(indRich,:),maxWater(indRich,:),zerosGridSoil(indRich,:),timestepLength);
end

% Remove any soil water over saturation, add to exfiltration
excessWater = max(0,waterSoil(indUpland,:) - satCap(indUpland,:));
if any(excessWater(:)>0)
    exfiltration(indUpland,:) = exfiltration(indUpland,:) + sum(excessWater,2);
    waterSoil(indUpland,:) = waterSoil(indUpland,:) - excessWater;
end

%Calculate the air conductivity, used next timestep for calculating air
%exchange depth, for now just assume the first layer controls this
thetaAir = porosity(indUpland,1) - theta(:,1);
[kAir(indUpland)] = van_genuchten_model(thetaAir,porosity(indUpland,1),thetaResid(indUpland,1),...
    vanGenAlpha(indUpland,1),vanGenN(indUpland,1),kSat(indUpland,1),maulemK0(indUpland,1),maulemL(indUpland,1));

%Add exfiltration to precip excess
precipExcess(indUpland) = precipExcess(indUpland) + exfiltration(indUpland);

%--------------------------------------------------------------------------
%Some error testing
%--------------------------------------------------------------------------
if any(deepPerc(:) < 0)
    disp('Check Deep Percolation Array');
end
if any(waterSoil(:) < 0)
    disp('Check soil water, went negative');
end
if any(waterSoil(:) > (satCap(:)*1.1))
    disp('Check soil water, above sat cap');
end
if any(precipExcess(:)>100)
    disp('Check precip excess, upland');
end

%--------------------------------------------------------------------------
%Update the state controller
%--------------------------------------------------------------------------
state = update_struct(state,'soil_infiltration',infiltration,'precip_excess_upland',precipExcess,...
    'deep_percolation_upland',deepPerc,'soil_water_flux',drainage,'water_soil',waterSoil,...
    'precip_excess_urban',urbExcess,'runon_upland_wetland',urbWetRunon,'soil_evap_depth',evapDepth,...
    'soil_unsat_conductivity',kTheta,'soil_air_conductivity',kAir);
if onlineCoupling == 1
    state = update_struct(state,'recharge_WT_change_upland',rechargeWTUpland);
end
end

function [evapInd,soilLayerTops] = find_soil_layer(evapDepth,soilThick)
soilLayerTops = [zeros(size(soilThick,1),1),cumsum(soilThick(:,1:end-1),2)];
evapDepthRepmat = repmat(evapDepth,[1,size(soilThick,2)]);
[evapLayer,evapRow] = find((evapDepthRepmat >= soilLayerTops &  evapDepthRepmat < (soilLayerTops + soilThick))');
evapInd = sub2ind(size(soilThick),evapRow,evapLayer);
end

function depthEvap = soil_evaporation_depth(depthEvap,evap,theta,thetaResid,thickness,layTop)
%Calculate thickness of layer already evaporated
thickEvap = depthEvap - layTop;

%Calculate thetaNonEvap
thetaNonEvap = (thickness .* theta - ...
    thickEvap .* thetaResid)./(thickness - thickEvap);
testInf = isinf(thetaNonEvap);
thetaNonEvap(testInf) = thetaResid(testInf);

%Update evaporative depth
depthEvap = max(0,depthEvap + evap ./ (thetaNonEvap - thetaResid));
testInf = isinf(depthEvap);
depthEvap(testInf) = layTop(testInf)+thickness(testInf);
end

