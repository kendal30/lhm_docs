function state = v1_update_albedo(state,structure,params)
%UPDATE_ALBEDO  Updates all shortwave albedos in the model.
%   update_albedo()
%
%   This module calculates all diffuse and beam albedos (no VIS/NIR
%   partitioning currently) for vegetation, litter, soil, wetlands (with
%   ice cover possibly), and snow.  It outputs only the beam and diffuse
%   components of the canopy, uplands, and wetlands.  Using the litter
%   fraction and snow thickness grids, it calculates the net albedo for
%   all three types.
%
%   The output of this module is primarily only used by the shortwave
%   radiation module which calculates net radiation for canopy, upland, and
%   wetlands.
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also: shortwave_radiation_module

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[lai,coszen,waterSoil,snowWaterUpland,uplandSnowSurfAge] = deal_struct(state,...
    'lai','sun_zenith_cos','water_soil','water_snow_upland','snow_surf_age_upland');
[albedoBrown,albedoGreen,albedoLakeBeam,albedoLakeDiff,albedoSoilDry,...
    soilSatCap,albedoNewSnowVis,albedoNewSnowNIR,snowGndAlbedoExtDep,...
    albedoLakeIceDry] = deal_struct(params,...
    'albedo_brown','albedo_green','albedo_water_beam','albedo_water_diffuse','albedo_soil_dry',...
    'soil_sat_cap','albedo_snow_vis','albedo_snow_nir','snow_gnd_albedo_ext_depth',...
    'albedo_lake_ice_dry');
[zerosGrid,snowCoveredUpland,iceCoveredWetland,indUpland,indWetland] = ...
    deal_struct(structure,...
    'zeros_grid','index_snow_upland','index_ice_pack','index_upland','index_wetland');

%--------------------------------------------------------------------------
%Initialize Arrays
%--------------------------------------------------------------------------
[albedoDiffUpland,albedoBeamUpland,albedoCanopyDiff,soilAlbedoDiff,...
    albedoBeamWetland,albedoDiffWetland] = deal(zerosGrid);

%--------------------------------------------------------------------------
%                             Vegetation Albedo 
%--------------------------------------------------------------------------
%query the green and brown albedo values from the params controller

%calculate the vegetation albedo as a linear interpolation between the
%green and brown values.
indComplete = (lai > 3);
indPartial = ~indComplete;
%Linearly interpolate between green and brown albedo 
albedoCanopyDiff(indComplete) = albedoGreen(indComplete);
albedoCanopyDiff(indPartial) = (lai(indPartial) / 3 .* (albedoGreen(indPartial) - ...
    albedoBrown(indPartial)) + albedoBrown(indPartial));
albedoCanopyBeam = albedoCanopyDiff; %assume these are equal for now

%--------------------------------------------------------------------------
%                             Soil Albedo 
%--------------------------------------------------------------------------
soilFracSat = waterSoil(indUpland,1) ./ soilSatCap(indUpland,1);
%Wet soil albedos are 1/2 of dry soil albedos, from CLM
soilAlbedoDiff(indUpland) = albedoSoilDry(indUpland) .* (1 - soilFracSat) + albedoSoilDry(indUpland) / 2 .* soilFracSat;
soilAlbedoBeam = soilAlbedoDiff; %assume equal for now

%--------------------------------------------------------------------------
%                       Net Upland Albedo
%--------------------------------------------------------------------------
albedoDiffUpland(indUpland) = soilAlbedoDiff(indUpland);
albedoBeamUpland(indUpland) = soilAlbedoBeam(indUpland);

%--------------------------------------------------------------------------
%                             Wetland Albedo 
%--------------------------------------------------------------------------
%For this step, start with beam and diffuse wetland albedos are equal
albedoBeamWetland(indWetland) = albedoLakeDiff;
albedoDiffWetland(indWetland) = albedoLakeDiff;

%Modify wetland albedo for ice cover
ind = (iceCoveredWetland);
if any(ind)
    albedoLakeIce(ind) = albedoLakeIceDry;
    albedoBeamWetland(ind) = albedoLakeIce(ind); %assume equal to diffuse, for now
    albedoDiffWetland(ind) = albedoLakeIce(ind);
end

%Modify wetland beam albedo for the zenith angle
ind = (~iceCoveredWetland) & (coszen > 1e-3) & (indWetland);
if any(ind)
    albedoBeamWetland(ind) = albedoLakeBeam ./ (coszen(ind).^(1.7) + 0.15); %from LSM
end

%--------------------------------------------------------------------------
%                             Upland Snow Albedo 
%--------------------------------------------------------------------------
ind = snowCoveredUpland;
if any(ind)
    [snowAlbedoDiff,snowAlbedoBeam] = deal(zerosGrid);
      
    %Calculate the new snow surface albedo
    [snowAlbedoDiff(ind),snowAlbedoBeam(ind)] = bats_albedo(uplandSnowSurfAge(ind),coszen(ind),albedoNewSnowVis,albedoNewSnowNIR);
    
    %Calculate snow thickness, assume 0.2 density relative to water
    snowThickUpland = snowWaterUpland ./ 0.2; 
    
    ind1 = (snowThickUpland < snowGndAlbedoExtDep);
    if any(ind1)
        %Calculate the net snowpack albedo that takes into account exponential
        %light extinction with depth
        albedoDiffUpland(ind1) = shallow_snow_albedo(snowAlbedoDiff(ind1),...
            albedoDiffUpland(ind1),snowThickUpland(ind1),snowGndAlbedoExtDep);
        albedoBeamUpland(ind1) = shallow_snow_albedo(snowAlbedoBeam(ind1),...
            albedoBeamUpland(ind1),snowThickUpland(ind1),snowGndAlbedoExtDep);
    end
    ind1 = ~ind1 & (indUpland);
    if any(ind1)
        albedoDiffUpland(ind1) = snowAlbedoDiff(ind1);
        albedoBeamUpland(ind1) = snowAlbedoBeam(ind1);
    end
end

%--------------------------------------------------------------------------
%update the values stored in the data controller
%--------------------------------------------------------------------------
state = update_struct(state,'albedo_canopy_diff',albedoCanopyDiff,'albedo_canopy_beam',albedoCanopyBeam,...
    'albedo_upland_diff',albedoDiffUpland,'albedo_upland_beam',albedoBeamUpland,...
    'albedo_wetland_diff',albedoDiffWetland,'albedo_wetland_beam',albedoBeamWetland);

end

%--------------------------------------------------------------------------
%                             Additional Functions 
%--------------------------------------------------------------------------

function [albedo] = shallow_snow_albedo(albedoSnow,albedoGround,snowThick,lightExtinctDepth)
%this is from the UEB model as well
r = (1 - snowThick/lightExtinctDepth) .* exp(-snowThick / (2*lightExtinctDepth));
albedo = albedoSnow .* (1 - r) + albedoGround .* r;
end


function [diffuse,beam] = bats_albedo(age,coszen,av0,anir0)
%This is the BATS albedo model from the UEB snow model
%Specify BATS constants
b=2;
cs=0.2;
cn=0.5;

ind=(coszen>0.5);
fzen=zeros(size(age));
fzen(ind)=1/b.*((b+1)./(1+2*b*coszen(ind))-1);

fage=age./(1+age);

avd=(1-cs*fage)*av0;
avis=avd+0.4*fzen.*(1-avd);
anird=(1-cn*fage)*anir0;
anir=anird+0.4*fzen.*(1-anird);

diffuse = weighted_mean('arithmetic',avd,anird,0.45,0.55);
beam = weighted_mean('arithmetic',avis,anir,0.45,0.55);
end

