function state = v1_couple_ET_upland(state,structure,params)

[radNet,airVapPress,airTemp,pressure,windspeed,...
    waterDepression,waterSoil,depthSoilEvap,...
    wtDepthUpland,upSatFrac,...
    Air] = deal_struct(state,...
    'rad_short_upland_net','vap_pressure_air','temp_air','pressure','windspeed',...
    'water_depression','water_soil','depth_soil_evap',...
    'water_table_depth_upland','soil_wt_fraction',...
    'soil_air_conductivity');
[soilPorosity,diffCoeffVapor,gndRghHeight,soilThetaResid] = deal_struct(params,...
    'soil_porosity','diff_coeff_vapor','soil_rough_height','soil_theta_r');
[zerosGrid,timestepLength,snowCovered,soilThick,indUpland...
    airExchangeStruct] = deal_struct(structure,...
    'zeros_grid','timestep_length','index_snow_upland','thickness_soil','index_upland',...
    'soil_air_exchange_structure');

% Calculate unsaturated and saturated thickness of each layer
satThick = soilThick .* upSatFrac;
unsatThick = soilThick - satThick;

depthSoil = sum(soilThick,2);
%--------------------------------------------------------------------------
%Initialize variables
%--------------------------------------------------------------------------
[peDepression,peSoil,evapDep,evapSoil,evapPost,depthAirExchange] = deal(zerosGrid);

% wipe GW depth in flooded cell to zero, but don't update the state
% variable
wtDepthUpland = max(wtDepthUpland,0);

%--------------------------------------------------------------------------
%Calculate potential and actual depression evaporation
%--------------------------------------------------------------------------
%Calculate the aerodynamic resistance, shared for depression evaporation
%and soil evaporation
ra = aerodynamic_resistance(structure,params,'neutral',windspeed,gndRghHeight);

%Check to see if there is water in any depressions
ind = (waterDepression > 0);
if any(ind(:))
    %Canopy resistance is zero for depressions
    rc = zerosGrid(ind);
    
    %Calculate the peDepression
    peDepression(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rc,ra(ind));
    
    %Multiply rate /s by timestep length to get model units
    peDepression(ind) = peDepression(ind) * timestepLength;
    
    %Calculate water limitation
    evapDep(ind) = min(waterDepression(ind),peDepression(ind));
    
    %Remove depression evaporation from depression water storage
    waterDepression(ind) = waterDepression(ind) - evapDep(ind);
    
    %Calculate fraction of potential evaporation
    evapFrac = evapDep(ind) ./ peDepression(ind);
    evapFrac(isnan(evapFrac)) = 0; %divide by 0 potential here
    
    %Reduce net radiation by fraction of potential evaporation that
    %occurred
    radNet(ind) = radNet(ind) .* (1 - evapFrac);
end

%--------------------------------------------------------------------------
%Calculate potential and actual bare soil evaporation
%--------------------------------------------------------------------------
%Only run this if previous time step not completely snow covered
ind = ~snowCovered & indUpland;
if any(ind(:))
    %Choudhury and Monteith 1988 note that soil evaporation resistance is 0
    %down to a certain depth to which atmospheric pressure variations drive
    %water vapor exchange with the surface
%     depthEvapEff = depthSoilEvap;
%     indNoResist = (depthSoilEvap<=depthEvapAirExchange);
%     depthEvapEff(indNoResist) = 0;
     depthEvapEff = max(0,depthSoilEvap - depthAirExchange);
   
    % for close to surface groundwater, set depthEvap to GW depth
    indShallow = ind & depthSoilEvap > wtDepthUpland;
    depthEvapEff(indShallow) = wtDepthUpland(indShallow);
    
    %Ground resistance for soil
    rs = (2 .* depthEvapEff(ind)) ./...
        (soilPorosity(ind,1) .* diffCoeffVapor); %assumes tortuosity of 2, from Choudhury and Monteith 1988
    
    %Calculate potential soil evaporation
    peSoil(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rs,ra(ind));
    
    %Multiply rate /s by timestep length to get model units
    peSoil(ind) = peSoil(ind) * timestepLength;
    
    indEvap = find_soil_layer(depthSoilEvap,soilThick);
    soilTheta = waterSoil(indEvap) ./ unsatThick(indEvap);
    availWater = max(0,(soilTheta - soilThetaResid(indEvap)) .* unsatThick(indEvap));

    % for groundwater deeper than evapDepth
    %Calculate actual soil evporation by limiting soil evaporation to
    %available water in that layer
    ind_tmp = ind & wtDepthUpland > depthSoilEvap;
    evapSoil(ind_tmp) = min(peSoil(ind_tmp),availWater(ind_tmp));

    % Update depth to evaporative layer for deep groundwater cells
    depthSoilEvap(ind_tmp) = soil_evaporation_depth(depthSoilEvap(ind_tmp),evapSoil(ind_tmp),...
        soilTheta(ind_tmp),soilThetaResid(indEvap(ind_tmp)),unsatThick(indEvap(ind_tmp)));
    
    % for deep groundwater, neglecting post demand evaporation
%     ind_tmp = ind & upSatFrac(:,end) > 0 & wtDepthUpland >= depthSoilEvap; 
%     peRem = zerosGrid;
%     peRem(ind_tmp) = peSoil(ind_tmp) - evapSoil(ind_tmp);
    % linear interpolation
%     evapPost(ind_tmp) = peRem(ind_tmp) .* (depthSoil(ind_tmp) - wtDepthUpland(ind_tmp)) ...
%         ./ (depthSoil(ind_tmp) - depthSoilEvap(ind_tmp));
    
    % close to surface gw cells
    % Calculate soil evaporation 
    ind_tmp = ind & depthSoilEvap >= wtDepthUpland;
    % rechargeWTUpland(ind_tmp) = rechargeWTUpland(ind_tmp) - peSoil(ind_tmp);
    evapPost(ind_tmp) = peSoil(ind_tmp);

    % Update depth to evaporative layer 
    depthSoilEvap(ind_tmp) = wtDepthUpland(ind_tmp);
    
    %ADK - 2017/04/26, commenting this out, because it seems like that is a
    %double limitation on soil evaporation
    %evapSoil(ind) =
    %v1_soil_evap_model(peSoil(ind),soilTheta(:,1),ind,structure,params);
       
   
    %Calculate actual soil evaporation using the Ritchie-type evaporation
    %model
%     soilTheta = waterSoil(ind,1) ./ soilThick(ind,1);
%     evapSoil(ind) = v2_soil_evap_model(peSoil(ind),soilTheta,ind,structure,params);
end
%--------------------------------------------------------------------------
%Update the model state variables
%--------------------------------------------------------------------------
state = update_struct(state,'evap_depression',evapDep,'water_depression',waterDepression,...
    'evap_soil',evapSoil,'depth_soil_evap',depthSoilEvap,...
    'evap_upland_post_demand', evapPost,'soil_air_exchange_depth',depthAirExchange);
end

function depthEvap = soil_evaporation_depth(depthEvap,evap,theta,thetaResid,thickness)
%Calculate thetaNonEvap
thetaNonEvap = (thickness .* theta - ...
    depthEvap .* thetaResid)./(thickness - depthEvap);

%Update evaporative depth
depthEvap = max(0,depthEvap + evap ./ (thetaNonEvap - thetaResid));
end

function [evapInd,soilLayerTops] = find_soil_layer(evapDepth,soilThick)
soilLayerTops = [zeros(size(soilThick,1),1),cumsum(soilThick(:,1:end-1),2)];
evapDepthRepmat = repmat(evapDepth,[1,size(soilThick,2)]);
[evapLayer,evapRow] = find((evapDepthRepmat >= soilLayerTops &  evapDepthRepmat < (soilLayerTops + soilThick))');
evapInd = sub2ind(size(soilThick),evapRow,evapLayer);
end

% function evap = v2_soil_evap_model(pe,soilTheta,indTrim,structure,params)
% 
% % calculate evaporation from soil
% % when groundwater level is within the top layer of soil, split evaporation
% % into soil source and gw source. gw source will be implemented as negative
% % recharge in MODFLOW
% 
% % use the Ritchie method. See Anthony's v2_ET_upland.m
% 
% % 04/18/2017, Tianfang Xu
% 
% %Fetch properties from the data_controller
% [soilResidTheta,soilPorosity,soilKsat,soilBubPress,soilPoreSize] = ...
%     deal_struct(params,'soil_theta_r','soil_porosity','soil_ksat','soil_bub_press','soil_pore_size');
% [soilThick,timestepLength] = deal_struct(structure,'thickness_soil','timestep_length');
% 
% %Calculate the evap limit, currently assumed to be the residual
% %water content of the soil
% availableWater = (soilTheta - soilResidTheta(indTrim,1)) .* soilThick(indTrim,1);
% 
% %calculate evap from peLimited
% desorptivity = (8 * soilPorosity(indTrim,1) .* soilKsat(indTrim,1) .* soilBubPress(indTrim,1) ./ ...
%     (3 * (1 + 3 * soilPoreSize(indTrim,1)) .* (1 + 4 * soilPoreSize(indTrim,1)))).^(1/2) .* ...
%     (soilTheta).^((soilPoreSize(indTrim,1) / 2) + 2); %in units of m s^(-1/2)
% exfiltrationDepth = desorptivity .* timestepLength.^(1/2); %time steps must be in seconds, units are m/timestep
% evap = min(min(pe, exfiltrationDepth),availableWater); %because PE is in m/timestep
% end