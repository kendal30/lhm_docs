function state = v1_couple_ET_canopy(state,structure,params)

[radNet,airVapPress,airTemp,pressure,lai,windspeed,...
    canopyHeight,waterCanopy,canopyCap,waterSoil,...
    upSatFrac] = deal_struct(state,...
    'rad_short_canopy_net','vap_pressure_air','temp_air','pressure','lai','windspeed',...
    'canopy_height','water_canopy','canopy_capacity','water_soil',...
    'soil_wt_fraction');
[stomatalCond,dormantTemp,soilWiltPoint,porosity,plantAvailWater,...
    kSat, thetaResid, vanGenAlpha, vanGenN, maulemK0, maulemL...  % this line for vanGen k-unsat
    rootFrac] = deal_struct(params,...
    'stomatal_conductance_max','stomatal_temp_dormant','soil_wilting_point','soil_porosity','soil_plant_avail_water',...
    'soil_ksat','soil_theta_r','soil_alpha','soil_N', 'soil_k0','soil_L',...
    'root_mass_fraction');
[zerosGrid,timestepLength,soilZerosGrid,numLayers,indWetland,...
    soilThick] = deal_struct(structure,...
    'zeros_grid','timestep_length','zeros_grid_soil','num_layers_soil','index_wetland',...
    'thickness_soil');

% Get unitless soil properties
soilWiltPoint = soilWiltPoint ./ soilThick;

% Calculate unsaturated and saturated thickness of each layer
satThick = soilThick .* upSatFrac;
unsatThick = soilThick - satThick;

% Water stored in saturated thickness
waterSatAvail = satThick .* (porosity - soilWiltPoint);

%--------------------------------------------------------------------------
%Initialize variables
%--------------------------------------------------------------------------
[ptCanopy,evapCanopy,transpUpPhreatic] = deal(zerosGrid);
[transpiration] = deal(soilZerosGrid);

%--------------------------------------------------------------------------
%Calculate potential and actual transpiration
%--------------------------------------------------------------------------
%Calculate the aerodynamic resistance, shared for transpiration and
%evaporation
ra = aerodynamic_resistance(structure,params,'neutral',windspeed,canopyHeight);

%--------------------------------------------------------------------------
%Calculate potential and actual canopy evaporation
%--------------------------------------------------------------------------
%Canopy resistance is 0, aerodynamic resistance is still present
rc = zerosGrid;
peCanopy = penman_monteith(params,airVapPress,airTemp,radNet,pressure,rc,ra);

%Multiply rate /s by timestep length to get model units
peCanopy = peCanopy * timestepLength;

%Determine if any water is or should stored in the canopy to calculate potential evaporation
ind = (waterCanopy > 0) | (peCanopy < 0);
if any(ind(:))
    %Scale potential evaporation to actual evporation
    ind1 = (peCanopy < 0); %for cases with negative evap (dew or frost)
    evapCanopy(ind1) = min(waterCanopy(ind1) - canopyCap(ind1), peCanopy(ind1));  %this is a min because of the negative peCanopy
    evapCanopy(waterCanopy > canopyCap) = 0; %this will result in a little less dew in some cases
    ind2 = (peCanopy > 0) & (waterCanopy > 0); %for positive evap from a wet canopy
    evapCanopy(ind2) = min((waterCanopy(ind2)./canopyCap(ind2)).^(2/3) .* peCanopy(ind2) , waterCanopy(ind2)); %This from Manfreda et al? or Chen 2005?

    %Remove or add this water from the canopy water
    waterCanopy(ind) = max(waterCanopy(ind) - evapCanopy(ind) , 0);

    %Calculate fraction of potential evaporation
    evapFrac = evapCanopy(ind) ./ peCanopy(ind);
    evapFrac(isnan(evapFrac)) = 0; %divide by 0 potential here

    %Reduce net radiation by fraction of potential evaporation that
    %occurred
    radNet(ind) = radNet(ind) .* (1 - evapFrac);
end

%Check to make sure that the air temperature exceeds the dormant
%temperature for transpiration to occur
ind = (airTemp >= dormantTemp);
if any(ind(:))
    %Calculate canopy resistance
    rc = 1 ./ (stomatalCond(ind) .* lai(ind)); %for typical LU and LAI structure
    rc(isnan(rc)) = 0;

    %Calculate potential canopy transpiration
    ptCanopy(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rc,ra(ind));

    %Multiply rate /s by timestep length to get model units
    ptCanopy(ind) = ptCanopy(ind) * timestepLength;

    %Get the multi-layer index and multi-layer ptCanopy
    indSoil = repmat(ind,[1,numLayers]);
    ptCanopySoil = repmat(ptCanopy,[1,numLayers]);

    %Calculate water-limited transpiration, function of root mass and saturation thickness in each layer
    theta = waterSoil ./ unsatThick;
    % when unsatThick==0, waterSoil should be zero
    theta(isnan(theta)) = porosity(isnan(theta));

    fracSat = (waterSoil - soilWiltPoint .* unsatThick) ./ plantAvailWater;

    id_flooded = upSatFrac(:,1) == 1;
    id_shallow = upSatFrac(:,numLayers) > 0 & ~id_flooded;
    id_deep = upSatFrac(:,numLayers) == 0;

    % First handle deep groundwater upland cells
    id_tmp = indSoil & repmat(id_deep,[1,numLayers]);
    transpiration(id_tmp) = (theta(id_tmp) > soilWiltPoint(id_tmp)) .*...
        min(1,4/3 * fracSat(id_tmp)) .* ptCanopySoil(id_tmp) .* rootFrac(id_tmp);

    % Calculate unsaturated conductivity
    [kUnsat] = van_genuchten_model(theta,porosity,thetaResid,...
    vanGenAlpha,vanGenN,kSat,...
    maulemK0,maulemL);

    % Decide the proportion of dividing PT into saturated and unsaturated
    ptFracUnsat = unsatThick .* kUnsat ./ (unsatThick .* kUnsat + satThick .* kSat);
    ptUnsat = ptCanopySoil .* ptFracUnsat;
    ptSat = ptCanopySoil - ptUnsat;

    % T from unsaturated zone
    id_tmp = indSoil & repmat(id_shallow,[1,numLayers]);
    transpiration(id_tmp) = (theta(id_tmp) > soilWiltPoint(id_tmp)) .*...
        min(1,4/3 * fracSat(id_tmp)) .* ptUnsat(id_tmp) .* rootFrac(id_tmp);
%     transpiration(id_tmp) = min(waterUnsatAvail(id_tmp), transpiration(id_tmp));

    tRemUnsat = soilZerosGrid;
    tRemUnsat(id_tmp) = ptUnsat(id_tmp) .* rootFrac(id_tmp) - transpiration(id_tmp);

    % T from saturated zone (phreatic)
    tmp = soilZerosGrid;
    tmp(id_tmp) = min(waterSatAvail(id_tmp), ptSat(id_tmp) .* rootFrac(id_tmp) + tRemUnsat(id_tmp));
    tmp(~ind) = 0;
%     rechargeWTUpland = rechargeWTUpland - sum(tmp,2);
    transpUpPhreatic = transpUpPhreatic + sum(tmp,2);

    % Handle flooded cell
    tmp = zerosGrid;
    tmp(id_flooded) = min(sum(waterSatAvail(id_flooded,:),2),ptCanopy(id_flooded));
    tmp(~ind) = 0;
%     rechargeWTUpland(id_flooded) = rechargeWTUpland(id_flooded) - tmp(id_flooded);
    transpUpPhreatic = transpUpPhreatic + tmp;

    %Calculate canopy transpiration
    ptCanopy(~indWetland) = 0;
end

%--------------------------------------------------------------------------
%Update the model state variables
%--------------------------------------------------------------------------
state = update_struct(state,'evap_canopy',evapCanopy,'water_canopy',waterCanopy,...
    'transp_upland',transpiration,'transp_upland_phreatic', transpUpPhreatic,...
    'transp_wetland',ptCanopy);
end
