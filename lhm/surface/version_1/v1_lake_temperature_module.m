function state = v1_lake_temperature_module(state,structure,params)

[densityWetland,windspeed,radNet,airTemp,...
    totalPrecip,waterTemp,longwaveDown,qEvap,qSens,...
    rain,iceThickness,iceWater,evap,...
    icePrecipWater,canopyGapFrac] = ...
    deal_struct(state,...
    'wetland_water_density','windspeed','rad_short_wetland_net','temp_air',...
    'total_throughfall','temp_wetland','rad_long_in','latent_wetland','sensible_wetland',...
    'total_throughfall_rain','ice_pack_thickness','water_ice_pack','evap_ice_pack',...
    'ice_pack_precip_store','canopy_gap_fraction');
[molDiffWater,emissivityIce,emissivityWater,lambdaFusion,densIce,...
    cpWater,densWater,meltThreshold,lightAttenCoeff] =...
    deal_struct(params,...
    'diff_coeff_water','emissivity_ice','emissivity_water','latent_heat_fusion','density_ice',...
    'heat_cap_water','density_water','ice_melt_threshold','wetland_light_atten_coeff');
[zerosLakeGrid,indDeep,indOpen,layerDepth,...
    latitude,layerThick,indIce,timestepLength,zerosGrid,numLayers] = ...
    deal_struct(structure,...
    'zeros_grid_wetland','index_deep_wetland','index_open_water','wetland_depth',...
    'latitude','thickness_wetland','index_ice_pack','timestep_length','zeros_grid','num_layers_wetland');

%Modify index locally to avoid modifying update coverage
indOpen = indOpen & indDeep;
openTest = any(indOpen);
iceTest = any(indIce);

%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[eddyCoeff,sumDiffusionCoeff,outLight,radAbsorb,qStorage,freezingWater] = deal(zerosLakeGrid);
outLight(:,end+1) = zerosGrid;  %add another layer
[surfTemp,emissivity,longwaveUp,qAdvect,iceMelt,iceBotAccrete,precipStoreLoss,precipFrac,qAdvectIce] = deal(zerosGrid);

%--------------------------------------------------------------------------
%Convectively mix if needed
%--------------------------------------------------------------------------
%Convectively mix the lakes, and update density, should only be needed if variable
%is not properly initialized
densityWetland(indDeep,:) = water_density(waterTemp(indDeep,:));
[waterTemp(indDeep,:),densityWetland(indDeep,:)] = convective_mixing(waterTemp(indDeep,:),...
    densityWetland(indDeep,:),layerThick(indDeep,:));

%--------------------------------------------------------------------------
%Longwave radiative fluxes
%--------------------------------------------------------------------------
iceSurfTemp = min(airTemp,0); %Assume ice surface is at 0 degrees or airTemp, whichever is cooler
surfTemp = populate_array(surfTemp,iceSurfTemp,waterTemp(:,1),indIce,indOpen);
emissivity = populate_array(emissivity,emissivityWater,emissivityIce,indIce,indOpen);
%Because the canopy temperature is not explicitly calculated, assume that
%the radiative exchange between the water and the canopy is equal, only the
%sky-water exchange is important
longwaveUp(indDeep) = longwave_flux(surfTemp(indDeep),emissivity(indDeep)) .* canopyGapFrac(indDeep);
longwaveDown(indDeep) = emissivity(indDeep) .* longwaveDown(indDeep) .* canopyGapFrac(indDeep);

%--------------------------------------------------------------------------
%Ice Thickness Model
%--------------------------------------------------------------------------
if iceTest
    ind = indIce;
    %Calculate the rate of advected energy
    qAdvectIce(ind) = precip_heat_flux(params,rain(ind)/timestepLength,airTemp(ind),zerosGrid(ind)-0.01);

    %Calculate the fraction of the ice thickness that is stored
    %precipitation
    precipFrac(ind) = icePrecipWater(ind) ./ iceWater(ind);

    %Calculate new meteorological water, removing (or adding) evaporation
    newWater = totalPrecip(ind) - evap(ind);

    %Add meteorological water store to ice pack
    icePrecipWater(ind) = icePrecipWater(ind) + newWater;

    %Add new meteorological water to the ice pack
    iceWater(ind) = iceWater(ind) + newWater;
    iceThickness(ind) = iceWater(ind) / (densIce / densWater);

    %Determine if pack has completely melted here due to evaporation
    test = (iceWater < 0) & (indDeep);
    iceWater(test) = 0;
    iceThickness(test) = 0;
    icePrecipWater(test) = 0;

    %recalculate for rest of the ice module
    indIce = (iceWater > 0);
    ind = indDeep & indIce;

    %Carry on for non-melted cells
    %Melt or Thicken the ice as necessary
    iceEnergy = -longwaveUp(ind) + longwaveDown(ind) + radNet(ind) + qAdvectIce(ind) - qSens(ind) - qEvap(ind);
    delWater = -iceEnergy * timestepLength / (lambdaFusion * densWater); %Positive values are increase in ice thickness

    %Ice melt will be defined as positive melting
    iceMelt(ind) = -delWater; %is the negative part of delWater
    iceMelt(iceMelt < 0) = 0;

    %Ice bot accrete is the positive part of delWater
    iceBotAccrete(ind) = delWater;
    iceBotAccrete(iceBotAccrete < 0) = 0;

    %Add delWater to the icePack
    iceWater(ind) = iceWater(ind) + delWater;

    %Check to see if melting exceeds iceWater
    testMelt = (iceWater < 0) & (indDeep);

    %Reduce iceMelt where overage has occurred
    iceMelt(testMelt) = iceMelt(testMelt) + iceWater(testMelt);

    %Set ice thickness to 0 there, zero out everything
    iceWater(testMelt) = 0;
    precipStoreLoss(testMelt) = icePrecipWater(testMelt);
    icePrecipWater(testMelt) = 0;

    %Finally, determine if all of the ice is melted, if ice pack < 0.1
    %mm
    testThreshold = (iceWater > 0) & (iceWater < meltThreshold) & (indDeep);
    iceMelt(testThreshold) = iceMelt(testThreshold) + iceWater(testThreshold);
    iceWater(testThreshold) = 0;
    precipStoreLoss(testThreshold) = icePrecipWater(testThreshold);
    icePrecipWater(testThreshold) = 0;

    %Now, update icePrecipWater, melting from the meteorological storage is
    %proportional to total ice thickness
    testWater = (iceWater > 0) & (iceMelt > 0);
    precipStoreLoss(testWater) = min(icePrecipWater(testWater),iceMelt(testWater) .* precipFrac(testWater));
    icePrecipWater(testWater) = icePrecipWater(testWater) - precipStoreLoss(testWater);

    %Calculate final iceThickness
    iceThickness(ind) = iceWater(ind) / (densIce / densWater);
end

%Recalculate indOpen
indOpen = indDeep & ~indIce;

%--------------------------------------------------------------------------
%Only for non-ice covered portions of the lake
%--------------------------------------------------------------------------
if openTest
    ind = indOpen;
    %--------------------------------------------------------------------------
    %Calculate Lake Radiation and Advected Energy
    %--------------------------------------------------------------------------
    %Calculate absorbtion of shortwave radiation by each layer
    %Use exponential decay, Beer's Law, see Hostetler and Bartlein
    %Deep Lakes
    outLight(:,1) = 1; %by definition all light enters the top of layer 1
    outLight(ind,2:end) = exp(-lightAttenCoeff .* (layerDepth(ind,:) + layerThick(ind,:)/2));

    %Calculate absorbed radiation in each layer, and then send the remaining
    %radiation to the bottom layer
    radAbsorb(ind,:) = (outLight(ind,1:end-1) - outLight(ind,2:end)) .* repmat(radNet(ind),[1,numLayers]);
    radBot = max(0,radNet(ind) - sum(radAbsorb(ind,:),2));

    %Calculate the rate of advected energy
    qAdvect(indOpen) = precip_heat_flux(params,totalPrecip(indOpen)/timestepLength,airTemp(indOpen),waterTemp(indOpen,1));

    %Calculate the net surface energy flux
    surfEnergy = longwaveDown(ind) - longwaveUp(ind) - qEvap(ind) - qSens(ind) + qAdvect(ind);
    %surfEnergy is positive outward

    %----------------------------------------------------------------------
    %Calculate the total diffusion coefficient
    %----------------------------------------------------------------------
    eddyCoeff(ind,:) = eddy_diffusion_coefficient(params,densityWetland(ind,:),...
        layerDepth(ind,:),windspeed(ind),latitude(ind));
    sumDiffusionCoeff(ind,:) = molDiffWater ./ densityWetland(ind,:) + eddyCoeff(ind,:);

    %----------------------------------------------------------------------
    %Calculate the lake temperature of deep lakes
    %----------------------------------------------------------------------
    %calculate necessary constants
    cvWater = cpWater .* densityWetland(ind,:);

    prevWaterTemp = waterTemp;
    waterTemp(ind,:) = eddy_diffusion_model(waterTemp(ind,:),surfEnergy,radBot,...
        radAbsorb(ind,:),layerThick(ind,:),sumDiffusionCoeff(ind,:),cvWater,timestepLength);


    %check to see if the water is frozen, if so, create ice layer
    test = waterTemp < 0;
    testFlat = any(test,2);
    if any(testFlat)
        freezingTemp = waterTemp;
        freezingTemp(~test) = 0;
        freezingWater(testFlat,:) = cpWater .* layerThick(testFlat,:) .* -freezingTemp(testFlat,:) ./ lambdaFusion;
        waterTemp(test) = 0;
        newIce = sum(freezingWater(testFlat,:),2);
        iceBotAccrete(testFlat) = iceBotAccrete(testFlat) + newIce;
        iceWater(testFlat) = iceWater(testFlat) + newIce;
        iceThickness(testFlat) = iceWater(testFlat) / (densIce / densWater);
    end

    %Add in a check for water temperatures above 30 degrees, if so then
    %force cooling--this will lose some energy if it comes into play!
    waterTemp(ind,:) = limit_vals([0,30],waterTemp(ind,:));

    %Convectively mix the lakes, and update density
    densityWetland(ind,:) = water_density(waterTemp(ind,:));
    [waterTemp(ind,:),densityWetland(ind,:)] = convective_mixing(waterTemp(ind,:),densityWetland(ind,:),layerThick(ind,:));

    if any(waterTemp(ind,:)>30)
        disp('Check water temperatures, greater than max temperature');
    end

    %calculate energy storage in the lake (in W/m^2)
    qStorage(ind,:) = (waterTemp(ind,:) - prevWaterTemp(ind,:)).*cpWater .*densityWetland(ind,:).*layerThick(ind,:)/timestepLength;
end

%--------------------------------------------------------------------------
%Update the data controller
%--------------------------------------------------------------------------
state = update_struct(state,'temp_wetland',waterTemp,'wetland_water_density',densityWetland,...
    'ice_pack_thickness',iceThickness,'ice_pack_melt',iceMelt,'energy_wetland',qStorage,...
    'water_ice_pack',iceWater,'ice_pack_bot_accrete_lake',iceBotAccrete,...
    'ice_pack_precip_store',icePrecipWater,'ice_pack_precip_release',precipStoreLoss);
end
