function state = v1_ET_canopy(state,structure,params)

[radNet,airVapPress,airTemp,pressure,lai,windspeed,...
    canopyHeight,waterCanopy,canopyCap,waterSoil] = deal_struct(state,...
    'rad_short_canopy_net','vap_pressure_air','temp_air','pressure','lai','windspeed',...
    'canopy_height','water_canopy','canopy_capacity','water_soil');
[stomatalCond,dormantTemp,soilWiltPoint,soilSatCap,plantAvailWater,...
    rootFrac] = deal_struct(params,...
    'stomatal_conductance_max','stomatal_temp_dormant','soil_wilting_point','soil_sat_cap','soil_plant_avail_water',...
    'root_mass_fraction');
[zerosGrid,timestepLength,soilZerosGrid,numLayers,indWetland] = deal_struct(structure,...
    'zeros_grid','timestep_length','zeros_grid_soil','num_layers_soil','index_wetland');

%--------------------------------------------------------------------------
%Initialize variables
%--------------------------------------------------------------------------
[ptCanopy,evapCanopy] = deal(zerosGrid);
[transpiration] = deal(soilZerosGrid);

%--------------------------------------------------------------------------
%Calculate potential and actual transpiration
%--------------------------------------------------------------------------
%Calculate the aerodynamic resistance, shared for transpiration and
%evaporation
ra = aerodynamic_resistance(structure,params,'neutral',windspeed,canopyHeight);

%--------------------------------------------------------------------------
%Calculate potential and actual canopy evaporation
%--------------------------------------------------------------------------
%Canopy resistance is 0, aerodynamic resistance is still present
rc = zerosGrid;
peCanopy = penman_monteith(params,airVapPress,airTemp,radNet,pressure,rc,ra);

%Multiply rate /s by timestep length to get model units
peCanopy = peCanopy * timestepLength;

%Determine if any water is or should stored in the canopy to calculate potential evaporation   
ind = (waterCanopy > 0) | (peCanopy < 0);
if any(ind(:))    
    %Scale potential evaporation to actual evporation
    ind1 = (peCanopy < 0); %for cases with negative evap (dew or frost)
    evapCanopy(ind1) = peCanopy(ind1);
    %ADK 2/12/2019: commented this out, should allow buildup of dew, will
    %drip off later
    %evapCanopy(ind1) = min(waterCanopy(ind1) - canopyCap(ind1), peCanopy(ind1));  %this is a min because of the negative peCanopy
    %evapCanopy((waterCanopy > canopyCap) & ind1) = 0; %this will result in a little less dew in some cases
    
    ind2 = (peCanopy > 0) & (waterCanopy > 0); %for positive evap from a wet canopy
    evapCanopy(ind2) = min((waterCanopy(ind2)./canopyCap(ind2)).^(2/3) .* peCanopy(ind2) , waterCanopy(ind2)); %This from Manfreda et al? or Chen 2005?
    
    %Remove or add this water from the canopy water
    waterCanopy(ind) = max(waterCanopy(ind) - evapCanopy(ind) , 0);
    
    %Calculate fraction of potential evaporation
    evapFrac = evapCanopy(ind) ./ peCanopy(ind);
    evapFrac(isnan(evapFrac)) = 0; %divide by 0 potential here
    
    %Reduce net radiation by fraction of potential evaporation that
    %occurred
    radNet(ind) = radNet(ind) .* (1 - evapFrac);
end

%Check to make sure that the air temperature exceeds the dormant
%temperature for transpiration to occur
ind = (airTemp >= dormantTemp);
if any(ind(:))
    %Calculate canopy resistance
    rc = 1 ./ (stomatalCond(ind) .* lai(ind)); %for typical LU and LAI structure
    rc(isnan(rc)) = 0;
    
    %Calculate potential canopy transpiration
    ptCanopy(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rc,ra(ind));
    
    %Limit ptCanopy to 0
    ptCanopy(ind) = max(ptCanopy(ind),0);
    
    %Multiply rate /s by timestep length to get model units
    ptCanopy(ind) = ptCanopy(ind) * timestepLength;
    
    %Get the multi-layer index and multi-layer ptCanopy
    indSoil = repmat(ind,[1,numLayers]);
    ptCanopySoil = repmat(ptCanopy,[1,numLayers]);
    
    %Calculate water-limited transpiration, function of root mass in each
    %layer
    availWater = max(0,waterSoil(indSoil) - soilWiltPoint(indSoil));
    fracSat = availWater ./ plantAvailWater(indSoil);
    transpiration(indSoil) = min(availWater,...
        (waterSoil(indSoil) > soilWiltPoint(indSoil)) .*...
        min(1,4/3 * fracSat) .* ptCanopySoil(indSoil) .* rootFrac(indSoil));
    
    %Calculate canopy transpiration
    ptCanopy(~indWetland) = 0;
end
%--------------------------------------------------------------------------
%Update the model state variables
%--------------------------------------------------------------------------
state = update_struct(state,'evap_canopy',evapCanopy,'water_canopy',waterCanopy,...
    'transp_upland',transpiration,'transp_wetland',ptCanopy);
end
