function state = v1_ueb_snow_model(state,structure,params)

    % Get necessary inputs
    [depthHeatExch] = deal_struct(params.ueb,'de');
    [densityMineral,densityWater,densityAir,cpMineral,cpWater,porosity,soilSatCap,k,z0] = deal_struct(params,...
        'density_mineral','density_water','density_air','heat_cap_mineral',...
        'heat_cap_water','soil_porosity','soil_sat_cap','von_karman','snow_rough_height');
    [gapFrac,snowFall,rainFall,...
        airTemp,windspeed,airVapPress,qRadShort,qRadLong,...
        pressure,qCondGround,snowWaterEquiv,snowHeatStore,surfAge,soilWater,...
        newSnowDens,snowThick,snowIceFrac] = deal_struct(state,...
        'canopy_gap_fraction','total_throughfall_snow','total_throughfall_rain',...
        'temp_air','windspeed','vap_pressure_air','rad_short_upland_net','rad_long_in',...
        'pressure','q_cond_ground','water_snow_upland','snow_heat_storage_upland','snow_surf_age_upland',...
        'water_soil','snow_new_density','snow_thickness_upland','snow_frozen_fraction_upland');
    [zerosGrid,indUpland,soilThick,timestepLength,z] = deal_struct(structure,...
        'zeros_grid','index_upland','thickness_soil','timestep_length','wind_meas_height');
    
    % for convenience, add wind_mean_height to params for rkinst function
    params.wind_meas_height = structure.wind_meas_height;

    % Determine if the snow model needs to run or not
    indSnow = ((snowWaterEquiv > 0) | (snowFall > 0)) & indUpland;
    
    % if any(isnan(snowThick))
    %         disp('debug me');
    % end
        
    if any(indSnow(:))
        % Initialize output variables
        [cump,cume,cummr,heatCapGround,densGround] = deal(zerosGrid);
        
        %  Forest cover fraction(0-1)
        fracCanopy = 1 - gapFrac;
        qCondGround(indSnow) = unit_conversions(unit_conversions(qCondGround(indSnow),'J','kJ'),'ps','phr');
        qRadShort(indSnow) = unit_conversions(unit_conversions(qRadShort(indSnow),'J','kJ'),'ps','phr');
        
        % Longwave is currently not affected by forest cover, affect it here
        qRadLong(indSnow) = unit_conversions(unit_conversions(qRadLong(indSnow),'J','kJ'),'ps','phr') .* gapFrac(indSnow);
        
        dt = unit_conversions(timestepLength,'s','hr');  % timestep is in seconds, this model wants hours
        
        % Convert precipitation totals into hourly rate
        snowRate = snowFall/dt;
        rainRate = rainFall/dt;
        
        %   initialize variables for mass balance
        prevSwe = snowWaterEquiv;
    
        % Get the deepest layer of interaction with the soil
        maxLay = sum(depthHeatExch>cumsum(soilThick,2),2) + 1;
        maxLay = mode(maxLay); % this is a simplification, but probably not a terrible one
        
        % Update ground density and heat capacity, need kJ
        weightMineral = 1 - porosity(indSnow,1:maxLay);
        weightWater = soilWater(indSnow,1:maxLay) ./ soilSatCap(indSnow,1:maxLay) .* porosity(indSnow,1:maxLay);
        weightAir = 1 - weightMineral - weightWater;
        
        % Calculate layer-specific terms
        heatCapLayers = weighted_mean('arithmetic',cpMineral,cpWater,...
            weightMineral.*densityMineral,weightWater.*densityWater);
        densLayers = weighted_mean('arithmetic',densityMineral,densityWater,densityAir,...
            weightMineral,weightWater,weightAir);
        
        % Average across layers
        totalSoilThick = repmat(sum(soilThick(indSnow,1:maxLay),2),[1,maxLay]);
        heatCapGround(indSnow) = sum(heatCapLayers .* soilThick(indSnow,1:maxLay) ./ totalSoilThick,2);
        densGround(indSnow) = sum(densLayers .* soilThick(indSnow,1:maxLay) ./ totalSoilThick,2);
        
        % Convert to kJ
        heatCapGround = unit_conversions(heatCapGround,'J','kJ'); % kJ/kg/C
        
        %   Calculate constants
        %    These need only be calculated once.
        %    The model is more efficient for large nt since it saves on these
        %    calc's
        cd = k^2./(log(z./z0).^2).*(1-0.8.*fracCanopy(indSnow));
        cd = unit_conversions(cd,'ps','phr');

        % factor in turbulent fluxes
        %    The fracCanopy correction is applied here to hit sensible and latent heat fluxes
        %    and evaporation rate in one place and maintain consistency in the surface
        %     temperature and stability calculations. It is consistent with wind speed
        %     reduction by a factor of 1-0.8 fracCanopy which is the most reasonable physical
        %     justification for this approach.
        %     I recognize that this is not a very good way to parameterize vegetation.
        %     This will be an area of future improvements(I hope).
        %    fracCanopy is also used below to adjust radiation inputs.
        %rrhoi = densIce./densWater;
        %rrho = densSnowpack./densWater;
        %rid = 1./rrho-1./rrhoi;
        
        %   Calculate neutral mass transfer coefficient
        neutMassCoeff = cd.*windspeed(indSnow);
        
        %     fracCanopy corrections are also in the following subroutines.
        %      energy_mass_fluxes where outgoing longwave radiation is calculated.
        %      SNOTMP where outgoing longwave radiation is used in the surface
        %      temperature equilibrium approx.
        
        %   Call predictor corrector subroutine to do all the work
        [snowHeatStore(indSnow),snowWaterEquiv(indSnow),snowIceFrac(indSnow),snowThick(indSnow),...
            evapRate,drainRate,tsurf] = predicorr(...
            dt,snowHeatStore(indSnow),snowWaterEquiv(indSnow),snowIceFrac(indSnow),...
            airTemp(indSnow),rainRate(indSnow),snowRate(indSnow),windspeed(indSnow),...
            airVapPress(indSnow),qRadShort(indSnow),qRadLong(indSnow),neutMassCoeff,...
            fracCanopy(indSnow),pressure(indSnow),qCondGround(indSnow),heatCapGround(indSnow),...
            densGround(indSnow),snowThick(indSnow),newSnowDens(indSnow),params); % #ok<ASGLU>
        
        %    accumulate for mass balance
        cump(indSnow) = (snowFall(indSnow)+rainFall(indSnow));
        cume(indSnow) = evapRate*dt;
        cummr(indSnow) = drainRate*dt;
        % tSnowpack = snowpack_temp(snowHeatStore,snowWaterEquiv,densWater,heatCapSnow,to,densGround,depthHeatExch,heatCapGround,heatFusion); % #ok<NASGU>
        
        % Calculate the new snow surface age
        surfAge(indSnow) = snow_surface_age(surfAge(indSnow),cump(indSnow),tsurf,timestepLength);
        
        % If most of the pack has melted reset
        test = (snowWaterEquiv < 0.00005);
        if any(test) % reset if snowWaterEquiv less than 0.05 mm
            cummr(test) = cummr(test) + snowWaterEquiv(test);
            snowWaterEquiv(test) = 0;
            snowHeatStore(test) = 0;
            snowThick(test) = 0;
            snowIceFrac(test) = 0;
            surfAge(test) = 0;
        end
        
        % Test error
        if assert_LHM()
            %         plot(state.timestep,mean(snowWaterEquiv(indSnow)),'o');
            %         plot(state.timestep,mean(cump(indSnow)),'rs');
            %         drawnow
            errmbal = prevSwe+cump-cummr-cume-snowWaterEquiv; % #ok<NASGU>
            %disp(max(abs(errmbal)))
            % if any(isnan(snowWaterEquiv) | isinf(snowWaterEquiv))
            %     disp('debug this')
            % end
            % if any(isnan(snowThick) | isinf(snowThick))
            %     disp('debug this')
            % end
            % if any(abs(snowHeatStore)>1e8)
            %     disp('Issues with snowHeatStore array')
            % end
            assert_LHM(~any(isnan(snowWaterEquiv)),'There are NaNs in the snowpack water');
        end
        
        % Update the data controller
        state = update_struct(state,'snow_heat_storage_upland',snowHeatStore,'water_snow_upland',...
            snowWaterEquiv,'snow_melt_upland',cummr,'evap_snow_upland',cume,'snow_surf_age_upland',surfAge,...
            'snow_frozen_fraction_upland',snowIceFrac,'snow_thickness_upland',snowThick);
    else
        state = update_struct(state,'snow_melt_upland',zerosGrid,'evap_snow_upland',zerosGrid);
    end
    
    end % subroutine snow
    
    % ********************** PREDICORR() **************************
    %     Predictor-corrector scheme to update the state variables,
    %     snowHeatStorage and snowWaterEquiv for each time step
    function [snowHeatStore,snowWaterEquiv,snowIceFrac,snowThick,evapRate,drainRate,tSurf] = ...
        predicorr(dt,snowHeatStore,snowWaterEquiv,snowIceFrac,...
        airTemp,rainRate,snowRate,windspeed,airVapPress,qRadShort,qLongIn,neutMassCoeff,fracCanopy,pressure,...
        qCondGround,heatCapGround,densGround,snowThick,newSnowDens,params)
    
    % wtol = 0.0025;
    % utol = 2000;
    
    %  Parameters
    [densWater] = deal_struct(params,'density_water');
    
    % Save this for imbalance calculation
    prevSWE = snowWaterEquiv;
    
    % Update frozen fraction, and swe
    snowIce0 = snowWaterEquiv .* snowIceFrac;
    snowLiquid0 = snowWaterEquiv - snowIce0;
    
    snowIce = snowIce0 + snowRate * dt;
    snowLiquid = snowLiquid0 + rainRate * dt;
    
    snowWaterEquiv = snowIce + snowLiquid;
    snowIceFrac = snowIce ./ snowWaterEquiv;
    
    %  Update snow thickness
    test = (snowRate > 0);
    snowThick(test) = snowThick(test) + (snowRate(test)*dt).*(densWater./newSnowDens(test));
    
    % Calculate initial estimate of mass fluxes
    [snowWaterEquiv0,snowHeatStore0,drainRate0,evapRate0,snowIceFrac0,...
        snowThick0,tSurf0] = energy_mass_fluxes(...
        snowHeatStore,snowWaterEquiv,snowIceFrac,airTemp,rainRate,snowRate,windspeed,airVapPress,...
        qRadShort,qLongIn,neutMassCoeff,fracCanopy,pressure,qCondGround,dt,heatCapGround,densGround,...
        snowThick,params);
    
    % Don't use corrector
    snowWaterEquiv = snowWaterEquiv0;
    snowHeatStore = snowHeatStore0;
    drainRate = drainRate0;
    evapRate = evapRate0;
    snowIceFrac = snowIceFrac0;
    snowThick = snowThick0;
    tSurf = tSurf0;
    
    % testBalance = (snowWaterEquiv - prevSWE)/dt - (snowRate + rainRate - evapRate - drainRate);
    % if any(abs(testBalance)>1e-10)
    %     disp('imbalance, line 206 v1_UEB');
    % end
    
    % % Update
    % [snowWaterEquiv1,snowHeatStore1,snowStore1,drainRate1,evapRate1,snowIceFrac1,...
    %     snowThick1,tSurf1] = energy_mass_fluxes(...
    %     snowHeatStore0,snowWaterEquiv0,snowIceFrac0,airTemp,rainRate,snowRate,windspeed,airVapPress,...
    %     qRadShort,qLongIn,neutMassCoeff,fracCanopy,pressure,qCondGround,dt,heatCapGround,densGround,...
    %     snowThick0,params);
    % 
    % % Average
    % snowWaterEquiv = (snowWaterEquiv + snowWaterEquiv1)/2;
    % snowHeatStore = (snowHeatStore0 + snowHeatStore1)/2;
    % snowIceFrac = (snowIceFrac + snowIceFrac1)/2;
    % snowThick = (snowThick + snowThick1)/2;
    % evapRate = (evapRate0 + evapRate1)/2;
    % %snowStore = (snowStore0 + snowStore1)/2;
    % %qSens = (qSens0 + qSens1)/2;
    % %qEvap = (qEvap0 + qEvap1)/2;
    % %qMelt = (qMelt0 + qMelt1)/2;
    % %qStor = (qMelt0 + qMelt1)/2;
    % %qNet = (qNet0 + qNet1)/2;
    % tSurf = (tSurf0 + tSurf1)/2;
    % 
    % % Correct rates
    % drainRate = rainRate + snowRate + (prevSWE - snowWaterEquiv)/dt - evapRate;
    % drainRate(drainRate<0) = 0;
    % evapRate = rainRate + snowRate + (prevSWE - snowWaterEquiv)/dt - drainRate;
    end % subroutine predicorr
    
    
    % 
    % *********************** energy_mass_fluxes() ********************************
    %     Calculates Energy and Mass Flux at any instant
    function [snowWaterEquiv,snowHeatStore,drainRate,evapRate,snowIceFrac,...
        snowThick,tSurf] = energy_mass_fluxes(snowHeatStore,snowWaterEquiv,snowIceFrac,...
        airTemp,rainRate,snowRate,windspeed,airVapPress,qRadShort,qLongIn,neutMassCoeff,fracCanopy,pressure,qCondGround,dt,...
        heatCapGround,densGround,snowThick,params)
    
    % Parameters
    [depthHeatExch] = deal_struct(params.ueb,'de');
    [qRadInterfFrac,condIce,condAir,condWater,heatCapWater,heatCapIce,densWater,...
        heatFusion,liquidCap] = deal_struct(params,...
        'snow_light_interf_absorb','therm_cond_ice','therm_cond_air','therm_cond_water',...
        'heat_cap_water','heat_cap_ice','density_water','latent_heat_fusion','snow_max_retention');
    
    heatFusion = unit_conversions(heatFusion,'J','kJ');
    heatCapWater = unit_conversions(heatCapWater,'J','kJ');
    heatCapIce = unit_conversions(heatCapIce,'J','kJ');
    
    zerosGrid = zeros(size(airTemp));
    prevSWE = snowWaterEquiv; %save for error checking below
    
    % First, calculate the melting
    snowIce = snowWaterEquiv .* snowIceFrac;
    snowLiquid = snowWaterEquiv - snowIce;
    [snowMelt,liquidFreeze,snowLiquid,snowIce,snowThick] = fmelt(snowHeatStore,densWater,snowIce,...
        heatFusion,snowLiquid,snowThick); %
    
    meltRate = snowMelt / dt;
    freezeRate = liquidFreeze / dt;
    
    % Update liquid holding capacity
    liquidCapThick = liquidCap .* snowThick;
    
    % Now calculate final snow liquid and melt rate
    snowDrain = max(snowLiquid-liquidCapThick,0);
    snowLiquid = snowLiquid - snowDrain;
    drainRate = snowDrain / dt;
    
    % Update snowWaterEquiv 
    snowWaterEquiv = snowIce + snowLiquid;
    test = (snowWaterEquiv > 0);
    snowIceFrac(test) = snowIce(test) ./ snowWaterEquiv(test);
    snowIceFrac(~test) = 0;
    
    % Update the density of the snowpack
    %densSnowpack = zerosGrid;
    %densSnowpack(test) = snowWaterEquiv(test) ./ snowThick(test) .* densWater;
    %densSnowpack(~test) = densWater; % this will work for all cells melted this timestep
    
    % Initialize other fluxes at 0
    [tSnowpack,meltRateSurf,qAdvSurf,tSurf,evapRate,evapFrost,qCondSurf,qEvap,qRadSurf,...
        qRadPack,qEvapUnsatisfied,evapDrain,evapIce,evapLiquid,heatCapSnow,thermCondSnow] = deal(zerosGrid);
    
    if any(test)
        % Update heat capacity of the snow
        weightIce = snowIceFrac(test);
        weightWater = 1 - weightIce;
        heatCapSnow(test) = weighted_mean('arithmetic',heatCapIce,heatCapWater,weightIce,weightWater);
    
        % Calculate thermal conductance of the snowpack
        weightIce = snowIce(test);
        weightWater = snowLiquid(test);
        weightAir = snowThick(test) - snowIce(test) - snowLiquid(test);
        thermCondSnow(test) = weighted_mean('geometric',condIce,condWater,condAir,...
            weightIce,weightWater,weightAir);
        thermCondSnow(isnan(thermCondSnow)) = 0;
    
        % Calculate average snowpack temperature, only run for cells with snowWaterEquiv > 0
        tSnowpack(test) = snowpack_temp(snowHeatStore(test),snowWaterEquiv(test),densWater,heatCapSnow(test),...
            densGround(test),depthHeatExch,heatCapGround(test),heatFusion);
        
        % Split shortwave radiation into that absorbed by the interface,
        % specified as a parameter fraction, and that absorbed by the pack
        qRadSurf(test) = qRadShort(test) .* qRadInterfFrac;
        qRadPack(test) = qRadShort(test) - qRadSurf(test);
       
        % Calculate the surface temperature, turbulent fluxes, conduction
        % of heat to the snowpack, and advection of heat to the snowpack
        [tSurf(test),qCondSurf(test),qEvap(test),evapRate(test),meltRateSurf(test),surfBalance] = surface_temp(...
            snowWaterEquiv(test),windspeed(test),...
            airVapPress(test),airTemp(test),pressure(test),rainRate(test),snowRate(test),...
            neutMassCoeff(test),qRadSurf(test),qLongIn(test),tSnowpack(test),...
            thermCondSnow(test),fracCanopy(test),params);
    
        % First, melt ice at the surface
        test0 = (meltRateSurf) > 0 & (snowIce > 0);
        meltIce = min(meltRateSurf(test0)*dt,snowIce(test0));
        snowThick(test0) = snowThick(test0) .* (1 - meltIce./snowIce(test0)); %update thickness
        snowIce(test0) = snowIce(test0) - meltIce;
        snowLiquid(test0) = snowLiquid(test0) + meltIce;
       
        % Add unsatisfied energy demand to evap
        meltUnsatis = meltRateSurf(test0) - meltIce/dt;
        qEvap(test0) = qEvap(test0) .* (meltUnsatis + evapRate(test0))./evapRate(test0);
        qEvap(isnan(qEvap)) = 0;
        evapRate(test0) = evapRate(test0) + meltUnsatis;
        
        % Then, take evaporation from snowLiquid
        evapTotal = evapRate*dt;
        test1 = (evapTotal > 0) & (snowLiquid > 0);
        evapLiquid(test1) = min(snowLiquid(test1),evapTotal(test1));
        snowLiquid(test1) = snowLiquid(test1) - evapLiquid(test1);
        evapRemain = evapTotal;
        evapRemain(test1) = evapRemain(test1) - evapLiquid(test1);
        
        % Add accumulation to ice
        test2 = (evapRemain < 0);
        evapFrost(test2) = -evapRemain(test2);
        snowIce(test2) = snowIce(test2) + evapFrost(test2);
        evapRemain(test2) = 0;
        snowThick(test2) = snowThick(test2) + evapFrost(test2); %update thickness
        
        % Remove sublimation from ice
        test3 = (evapRemain > 0) & (snowIce > 0);
        evapIce(test3) = min(evapRemain(test3),snowIce(test3));
        snowThick(test3) = snowThick(test3) .* (1 - evapIce(test3)./snowIce(test3)); %update thickness
        snowIce(test3) = snowIce(test3) - evapIce(test3);
        evapRemain(test3) = evapRemain(test3) - evapIce(test3);
        
        % Finally, remove evaporation from drainage
        test4 = (evapRemain > 0) & (snowDrain > 0);
        evapDrain(test4) = min(evapRemain(test4),snowDrain(test4));
        snowDrain(test4) = snowDrain(test4) - evapDrain(test4);
        evapRemain(test4) = evapRemain(test4) - evapDrain(test4);
        drainRate(test4) = snowDrain(test4)/dt;
        
        % Update snow water equivalent
        snowWaterEquiv(test) = snowIce(test) + snowLiquid(test);
        test5 = (snowWaterEquiv > 0);
        snowIceFrac(test5) = snowIce(test5) ./ snowWaterEquiv(test5);
        
        % Get final evap rate
        evapRate = (evapDrain + evapIce + evapLiquid - evapFrost)/dt; 
        
        % Check for unsatisfied evap demand, shift to sensible heat flux
        test6 = (evapRemain > 0);
        fracUnsatisfied = evapRemain(test6)./evapTotal(test6);
        qShift = qEvap(test6).*fracUnsatisfied;
        qEvap(test6) = qEvap(test6) - qShift;
        qEvapUnsatisfied(test6) = qShift; % most sensible flux taken from surface balance
        % only the shfited portion here will be taken from the pack (to
        % maintain overall energy balance)
            
        % Calculate advected heat
        qAdvSurf(test) = precip_heat_flux(meltRateSurf(test),0,tSurf(test),tSnowpack(test),...
            densWater,heatFusion,heatCapWater,heatCapIce);
    end
    
    % Correct thickness, use mean of other cells
    test = (snowWaterEquiv > 0) & (snowThick == 0);
    test1 = (snowWaterEquiv > 0) & (snowThick > 0);
    if any(test1) && any(test)
        snowThick(test) = mean(snowThick(test1));
    elseif any(test) %this happens when all of the snow ice has melted to liquid
        drainRate(test) = drainRate(test) + snowWaterEquiv(test)/dt;
        snowIceFrac(test) = 0;
        snowWaterEquiv(test) = 0;
    end
    
    % testBalance = (prevSWE - snowWaterEquiv)/dt - (evapRate + drainRate);
    % if any(abs(testBalance)>1e-10)
    %     disp('imbalance, line 399 v1_UEB');
    % end
    
    % Recalculate the snow ice fraction
    test = (snowIceFrac > 0) & (snowWaterEquiv == 0);
    snowIceFrac(test) = 0;
    
    % Calculate the heat flux due to melting and freezing of water in the
    % snowpack, positive inward
    qMelt = -meltRate.*densWater .* (heatFusion - (tSnowpack<0).*tSnowpack.*heatCapIce)+ ...
        freezeRate.*densWater .* (heatFusion - tSnowpack*heatCapIce); % QM in kj/m2/hr.  
    
    % Calculate the change in energy storage rate, all positive inward
    qCondSurf = -qCondSurf; % originally positive toward the surface interface
    qStor = qRadPack + qCondSurf + qAdvSurf + qCondGround + qMelt + qEvapUnsatisfied;
    
    % Update snow heat store
    snowHeatStore = snowHeatStore + dt.*qStor;
    
    end 
    
    %--------------------------------------------------------------------------
    % snowpack_temp: Calculate the surface temperature iteratively
    %--------------------------------------------------------------------------
    function [tSnowpack] = snowpack_temp(snowHeatStore,snowWaterEquiv,densWater,heatCapSnow,...
        densGround,depthHeatExch,heatCapGround,heatFusion)
    %     Calculates the average temperature of snow and interacting
    %     soil layer
    
    snhc = densWater.*snowWaterEquiv.*heatCapSnow; % Snow heat capacity, in units of kJ/m2
    shc  = densGround.*depthHeatExch.*heatCapGround; % Soil heat capacity, kJ/m2
    chc  = snhc+shc; % Combined heat capacity, kJ/m2
    
    % When total heat storage is positive, but just run everywhere
    al = snowHeatStore./(densWater.*heatFusion); % units are m
    tSnowpack = (snowWaterEquiv<= al).*(snowHeatStore-snowWaterEquiv*densWater*heatFusion)./chc;
    
    % When total heat storage is less than zero
    test = (snowHeatStore<= 0);
    tSnowpack(test) = snowHeatStore(test)./chc(test);
    end
    
    %--------------------------------------------------------------------------
    % surface_temp: Calculate the surface temperature iteratively
    %--------------------------------------------------------------------------
    function [tSurf,qCondSurf,qEvap,evapRate,meltRateSurf,surfBalance] = surface_temp(snowWaterEquiv,windspeed,airVapPress,airTemp,...
        pressure,rainRate,snowRate,neutMassCoeff,qRadSurf,qLongIn,...
        tSnowpack,thermCondSnow,fracCanopy,params)
    %   Computes the surface temperature of snow
    %   This version written on 4/23/97 by Charlie Luce solves directly the
    %   energy balance equation using Newtons method - avoiding the linearizations
    %   used previously.  The derivative is evaluated numerically over the range
    %   tSurf t0 fff*tSurf  where fff = 0.999
    
    % Initialize constants for the iteration convergence
    % fff = 0.999;
    perturb = 0.001;
    tol = 2; %kJ/m^2
    damp = 0.5;
    
    [densWater,heatFusion,heatCapWater,heatCapIce] = deal_struct(params,...
        'density_water','latent_heat_fusion','heat_cap_water','heat_cap_ice');
    heatFusion = unit_conversions(heatFusion,'J','kJ');
    heatCapWater = unit_conversions(heatCapWater,'J','kJ');
    heatCapIce = unit_conversions(heatCapIce,'J','kJ');
    
    % Initialize loop variables
    tSurf = airTemp; % first approximation
    maxChange = 3; % maximum tempearture change per iteration
    minChange = -3;
    er = tol * 10;
    niter = 0;
    testNoSnow = (snowWaterEquiv<= 0);
    %lowerLimit = min(tSnowpack,airTemp); % The interface temperature should almost never be colder than the colder of the air or the snowpack
    meltRateSurf = zeros(size(airTemp)); % Initialize melt rate at 0
    
    % Iterate to converge
    while (sum(er>tol)>0) && (niter<40)
        tslast = tSurf;
        
        % Calculate advected heat from precipication
        qPrecip = precip_heat_flux(rainRate,snowRate,airTemp,tSurf,densWater,heatFusion,heatCapWater,heatCapIce);
        qPrecip(testNoSnow) = 0; %ignore the effect of precip advected 
        % energy on the calculation of surface temperature when there is no snow.
        % Without this ridiculously high temperatures can result as the model
        % tries to balance outgoing radiation with precip advected energy.
     
        % Calculate the surface energy balance using this
        [f1,melt1,qCond1,qEvap1,evapRate1] = surface_balance(tSurf,airTemp,tSnowpack,meltRateSurf,neutMassCoeff,windspeed,pressure,airVapPress,...
            qPrecip,qRadSurf,qLongIn,thermCondSnow,fracCanopy,params);
        perturbSign = perturb * sign(f1);
        [f2,melt2,qCond2,qEvap2,evapRate2] = surface_balance(tSurf+perturbSign,airTemp,tSnowpack,meltRateSurf,neutMassCoeff,windspeed,pressure,airVapPress,...
            qPrecip,qRadSurf,qLongIn,thermCondSnow,fracCanopy,params);
        
        % Use the surface energy imbalance to update the surface temperature
        test = (f2~= f1);
        tSurf(test) = tSurf(test) - min(max((perturbSign(test) .* f1(test)) ./(f2(test) - f1(test)),minChange),maxChange)*damp;
        tSurf(~test) = tslast(~test);
        % disp([max(tSurf),min(tSurf)])
        
        % Constrain to the lower temperature - ADK, removed for now, because hourly it can indeed be colder than the air or snowpack
        %test = tSurf < lowerLimit;
        %tSurf(test) = lowerLimit(test);
        
        % Update the error function
        %er = mean(abs(tSurf - tslast));
        er = abs(f1);
        
        % Update the melt rate
        meltRateSurf = melt1;
        
        % Increment the iteration counter
        niter = niter+1;
    end
    qCondSurf = (qCond1 + qCond2)/2;
    qEvap = (qEvap1 + qEvap2)/2;
    evapRate = (evapRate1 + evapRate2)/2;
    surfBalance = (f1 + f2)/2;
    
    if niter>=40
        warning_LHM('Srftemp niter warning UEB');
    end
    end % function surface_temp
    
    %--------------------------------------------------------------------------
    % surface_balance: Calculate the surface energy balance
    %--------------------------------------------------------------------------
    function [surfebResult,meltRateSurf,qCondSurf,qEvap,evapRate] = surface_balance(...
            tSurf,airTemp,tSnowpack,meltRateSurf,...
            neutMassCoeff,windspeed,pressure,airVapPress,...
            qPrecip,qRadSurf,qLongIn,thermCondSnow,fracCanopy,params)
    %      function to evaluate the surface energy balance for use in solving for
    %      surface temperature
    %      DGT and C Luce 4/23/97
    
    % Specify a parameter here -- move to parameters
    interfCondThick = 0.02; %m
    
    % Get the constants
    [emissSnow,sbc,densWater,heatCapAir,heatCapWater,heatFusion] = deal_struct(params,...
        'emissivity_snow','stefan_boltzmann','density_water','heat_cap_air','heat_cap_water','latent_heat_fusion');
    
    % Get the right units for UEB, kJ, hr^-1
    thermCondSnow = unit_conversions(unit_conversions(thermCondSnow,'J','kJ'),'ps','phr'); %W/m/K to kJ/hr/m/K
    sbc = unit_conversions(unit_conversions(sbc,'J','kJ'),'ps','phr');
    heatCapAir = unit_conversions(heatCapAir,'J','kJ');
    heatFusion = unit_conversions(heatFusion,'J','kJ');
    heatCapWater = unit_conversions(heatCapWater,'J','kJ');
    
    % Calculate the turbulent fluxes from the interface
    tak = unit_conversions(airTemp,'C','K');
    tsk = unit_conversions(tSurf,'C','K');
    rkin = rkinst(neutMassCoeff,windspeed,tak,tsk,params);
    [qSens,qEvap,evapRate] = turbulent_fluxes(airTemp,tSurf,rkin,windspeed,pressure,...
        heatCapAir,airVapPress,densWater,params);
    
    % Calculate conduction of heat to the snowpack, defined as positive inward
    qCondSurf = thermCondSnow .* (tSnowpack - tSurf) / interfCondThick;
    
    % Calculate longwave radiation output from the surface
    qLongOut = (1 - fracCanopy) .* emissSnow .* sbc .* tsk.^4;
    
    % Calculate melting/freezing at the surface
    % positive meltRate is a negative qMelt
    qMeltSurf = -meltRateSurf.*densWater .* (heatFusion + (tSurf > 0).*tSurf.*heatCapWater);
       
    % Calculate the surface energy imbalance, will converge toward 0, qEvap
    % will come from the snowpack
    surfebResult = (qRadSurf + qLongIn - qLongOut) + qPrecip + qMeltSurf + qSens + qEvap + qCondSurf;
    
    % Update surface melting
    testMelt = (surfebResult > 0) & (tSurf > 0); %only for positive energy storage, and if tSurf > freezing
    % if f1 is negative, qMeltSurf will absorb difference
    meltRateDiff = surfebResult(testMelt) / (densWater * heatFusion);
    meltRateSurf(testMelt) = meltRateSurf(testMelt) + meltRateDiff;
    surfebResult(testMelt) = 0;
    
    % Refreeze some melt
    testFreeze = (surfebResult < 0) & (meltRateSurf > 0); %absorb some negative energy balance by re-freezing surf melting 
    freezeDemand = -surfebResult(testFreeze) / (densWater * heatFusion);
    freezeRateDiff = min(freezeDemand, meltRateSurf(testFreeze));
    meltRateSurf(testFreeze) = meltRateSurf(testFreeze) - freezeRateDiff;
    freezeFrac = (freezeRateDiff./freezeDemand);
    surfebResult(testFreeze) = surfebResult(testFreeze).*freezeFrac;
    
    % Update surfebResult
    end % function surface_balance
    
    
    % ************************** precip_heat_flux() ***************************
    %     Calculates the heat advected to the surface due to rainRate, positive
    %     inward
    
    function [qpfResult] = precip_heat_flux(rainRate,snowRate,airTemp,tSurf,densWater,heatFusion,heatCapWater,heatCapIce)
    
    % Initialize as the air temp or 0, whichever makes more sense
    tRain = max(airTemp,0);
    tSnow = min(airTemp,0);
    
    % Calculate advected heat
    qpfResult = rainRate*densWater.*(heatFusion+heatCapWater.*(tRain-tSurf))+...
        snowRate*densWater.*heatCapIce.*(tSnow-tSurf);
    
    end % function precip_heat_flux
    
    % *********************** turbulent_fluxes() ***************************
    %     Calculates the turbulent heat fluxes(sensible and latent
    %     heat fluxes) and condensation/sublimation.
    % Defined as positive inwards
    function [qSens,qEvap,evapRate] = turbulent_fluxes(airTemp,tSurf,neutMassCoeff,windspeed,pressure,...
        heatCapAir,airVapPress,densWater,params)
    
    % Get parameters used here
    [ra,hneu] = deal_struct(params,'gas_const_dry_air','latent_heat_sublimation'); 
    
    % Calculate rkin
    tak = unit_conversions(airTemp,'C','K');
    tsk = unit_conversions(tSurf,'C','K');
    rkin = rkinst(neutMassCoeff,windspeed,tak,tsk,params);
    
    % Calculate air density
    rhoa = pressure./(ra.*tak);%     RHOA in kg/m3
    
    hneu = unit_conversions(hneu,'J','kJ');
    
    % Calculate sensible heat flux
    qSens = rhoa.*(airTemp-tSurf).*heatCapAir.*rkin;
    satVapPress = sat_vap_pressure(tSurf);
    
    % Calculate latent heat flux
    qEvap = 0.622.*hneu./(ra.*tak).*rkin.*(airVapPress-satVapPress);
    
    % Calculate the rate of evaporation, positive outwards
    if nargout ==  3
        evapRate = -qEvap./(densWater.*hneu);%     evapRate in  m/hr
    end
    
    end
    
    % *********************************************************************
    % RKINST
    function [rkinstResult] = rkinst(neutMassCoeff,windspeed,airTemp,tSurf,params)
    %     function to calculate no neutral turbulent transfer coefficient using the
    %     richardson number correction.
    % airTemp must be in K
    
    [z,g] = deal_struct(params,'wind_meas_height','grav_constant');
    [fstab] = deal_struct(params.ueb,'fstab');
    rkinstResult = zeros(size(airTemp));
    test = (windspeed>0); % If now wind, no sensible or latent heat fluxes.
    rich = g*(airTemp(test)-tSurf(test))*z./(windspeed(test).^2.*airTemp(test));
    rkinstResult(test) = (rich>0).*neutMassCoeff(test)./(1+10.*rich)+(rich<= 0).*neutMassCoeff(test).*(1-10.*rich);
    %  Linear damping of stability correction through parameter fstab
    rkinstResult = neutMassCoeff+fstab.*(rkinstResult-neutMassCoeff);
    end % function rkinst
    
    
    % *********************** FMELT() ***************************
    %     Calculates the melt rate and melt outflow
    function [snowMelt,liquidFreeze,snowLiquid,snowIce,snowThick] = fmelt(snowHeatStore,densWater,snowIce,...
        heatFusion,snowLiquid,snowThick)
    % function [fmeltResult] = fmelt(snowHeatStore,densWater,snowWaterEquiv,heatFusion,liquidCap,rid,ks,rainRate)
    
    
    [snowMelt,newMelt,liquidFreeze] = deal(zeros(size(snowHeatStore)));
    
    % Calculate available melt
    test1 = (snowHeatStore > 0) & (snowIce > 0);
    if any(test1)
        newMelt(test1) = min(snowHeatStore(test1) ./ (densWater * heatFusion),snowIce(test1));
        snowLiquid(test1) = snowLiquid(test1) + newMelt(test1);
        origSnowIce = snowIce;
        snowIce(test1) = snowIce(test1) - newMelt(test1);
        snowThick(test1) = snowThick(test1) .* (1 - newMelt(test1)./origSnowIce(test1));
        snowThick(snowThick<=0) = 0; %should only be numerical error that can cause this
    end
    % if any(isnan(snowThick))
    %     disp('debug me');
    % end
    % if any((snowIce > 0) & (snowThick==0))
    %     disp('debug me');
    % end
    
    % Calculate freezing of snowLiquid
    test2 = (snowHeatStore < 0) & (snowLiquid > 0);
    if any(test2)
        liquidFreeze(test2) = min(-snowHeatStore(test2)./(densWater * heatFusion),snowLiquid(test2));
        snowLiquid(test2) = snowLiquid(test2) - liquidFreeze(test2);
        snowIce(test2) = snowIce(test2) + liquidFreeze(test2);
    end
    
    end % function fmelt
    
    