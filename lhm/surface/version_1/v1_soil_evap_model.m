function evap = v1_soil_evap_model(pe,soilTheta,indTrim,structure,params)
%SOIL_EVAP_MODEL  Calculates soil evap from PE and soil moisture.
%   evap = soil_evap_model(soilWater)
%
%   Calculates soil evap using a variant of the Ritchie model.  There is an
%   energy-limited phase, during which evap is controlled by the
%   amount of energy available.  Then, there is a moisture-limited phase,
%   during which evap is controlled by the ability of the moisture
%   to diffuse up to the evap surface.
%
%   Descriptions of Input Variables:
%   pe: potential soil evaporation
%   soilTheta: volumetric soil moisture (m^3/m^3)
%   indTrim: indices to trim the variables fetched from the data_controller
%
%   Descriptions of Output Variables:
%   evap: soil evaporation in m/timestep
%
%   Example(s):
%   none
%
%   See also: soil_moisture_model

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

%Fetch properties from the data_controller
[soilResidTheta,soilPorosity,soilKsat,soilBubPress,soilPoreSize] = ...
    deal_struct(params,'soil_theta_r','soil_porosity','soil_ksat','soil_bub_press','soil_pore_size');
[soilThick,timestepLength] = deal_struct(structure,'thickness_soil','timestep_length');

%Calculate the evap limit, currently assumed to be the residual
%water content of the soil
availableWater = (soilTheta - soilResidTheta(indTrim,1)) .* soilThick(indTrim,1);

%calculate evap from peLimited
desorptivity = (8 * soilPorosity(indTrim,1) .* soilKsat(indTrim,1) .* soilBubPress(indTrim,1) ./ ...
    (3 * (1 + 3 * soilPoreSize(indTrim,1)) .* (1 + 4 * soilPoreSize(indTrim,1)))).^(1/2) .* ...
    (soilTheta).^((soilPoreSize(indTrim,1) / 2) + 2); %in units of m s^(-1/2)
exfiltrationDepth = desorptivity .* timestepLength.^(1/2); %time steps must be in seconds, units are m/timestep
evap = min(min(pe, exfiltrationDepth),availableWater); %because PE is in m/timestep

