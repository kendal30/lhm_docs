function state = v1_ET_upland(state,structure,params)

[radNet,airVapPress,airTemp,pressure,windspeed,...
    waterDepression,waterSoil,depthSoilEvap,pressureRange,kAir] = ...
    deal_struct(state,...
    'rad_short_upland_net','vap_pressure_air','temp_air','pressure','windspeed',...
    'water_depression','water_soil','soil_evap_depth','pressure_range','soil_air_conductivity');
[soilPorosity,diffCoeffVapor,gndRghHeight,soilThetaResid] = deal_struct(params,...
    'soil_porosity','diff_coeff_vapor','soil_rough_height','soil_theta_r');
[zerosGrid,zerosGridSoil,timestepLength,snowCovered,soilThick,indUpland,...
    airExchangeStruct] = deal_struct(structure,...
    'zeros_grid','zeros_grid_soil','timestep_length','index_snow_upland','thickness_soil','index_upland',...
    'soil_air_exchange_structure');

%--------------------------------------------------------------------------
%Initialize variables
%--------------------------------------------------------------------------
[peDepression,peSoil,evapDep,evapSoil,depthAirExchange] = deal(zerosGrid);
[soilTheta] = deal(zerosGridSoil);

%--------------------------------------------------------------------------
%Calculate potential and actual depression evaporation
%--------------------------------------------------------------------------
%Calculate the aerodynamic resistance, shared for depression evaporation
%and soil evaporation
ra = aerodynamic_resistance(structure,params,'neutral',windspeed,gndRghHeight);

%Check to see if there is water in any depressions
ind = (waterDepression > 0);
if any(ind(:))
    %Canopy resistance is zero for depressions
    rc = zerosGrid(ind);
    
    %Calculate the peDepression
    peDepression(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rc,ra(ind));
    
    %Multiply rate /s by timestep length to get model units
    peDepression(ind) = peDepression(ind) * timestepLength;
    
    %Calculate water limitation
    evapDep(ind) = min(waterDepression(ind),peDepression(ind));
    
    %Remove depression evaporation from depression water storage
    waterDepression(ind) = waterDepression(ind) - evapDep(ind);
    
    %Calculate fraction of potential evaporation
    evapFrac = evapDep(ind) ./ peDepression(ind);
    evapFrac(isnan(evapFrac)) = 0; %divide by 0 potential here
    
    %Reduce net radiation by fraction of potential evaporation that
    %occurred
    radNet(ind) = radNet(ind) .* (1 - evapFrac);
end

%--------------------------------------------------------------------------
%Calculate potential and actual bare soil evaporation
%--------------------------------------------------------------------------
%Only run this if previous time step not completely snow covered
ind = ~snowCovered & indUpland;
if any(ind(:))
    %Calculate the air exchange depth
    soilTheta(ind,:) = waterSoil(ind,:) ./ soilThick(ind,:);
    
    %ADK - Commenting these out for now until I resolve how to handle
    %resistance without calculating soil temperature
    %airTheta = soilPorosity(ind,1) - soilTheta(ind,1);
    %[depthAirExchange(ind)] =
    %lookup_air_exchange_depth(airTemp(ind),kAir(ind),airTheta,pressureRange(ind),airExchangeStruct); 
    
    %Choudhury and Monteith 1988 note that soil evaporation resistance is 0
    %down to a certain depth to which atmospheric pressure variations drive
    %water vapor exchange with the surface
    depthEvapEff = max(0,depthSoilEvap - depthAirExchange);
    
    %Ground resistance for soil
    rs = (2 .* depthEvapEff(ind)) ./...
        (soilPorosity(ind,1) .* diffCoeffVapor); %assumes tortuosity of 2, from Choudhury and Monteith 1988
    
    %Calculate potential soil evaporation
    peSoil(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rs,ra(ind));
    
    %Multiply rate /s by timestep length to get model units
    peSoil(ind) = peSoil(ind) * timestepLength;
    
    %Calculate actual soil evporation by limiting soil evaporation to
    %available water in that layer
    indEvap = find_soil_layer(depthSoilEvap,soilThick);
    availWater = max(0,(soilTheta(indEvap) - soilThetaResid(indEvap)) .* soilThick(indEvap));
    evapSoil(ind) = min(peSoil(ind),availWater(ind));

    %ADK - 2017/04/26, commenting this out, because it seems like that is a
    %double limitation on soil evaporation
    %evapSoil(ind) =
    %v1_soil_evap_model(peSoil(ind),soilTheta(:,1),ind,structure,params);
    
    
end
%--------------------------------------------------------------------------
%Update the model state variables
%--------------------------------------------------------------------------
state = update_struct(state,'evap_depression',evapDep,'water_depression',waterDepression,...
    'evap_soil',evapSoil,'soil_air_exchange_depth',depthAirExchange);
end


function [evapInd,soilLayerTops] = find_soil_layer(evapDepth,soilThick)
soilLayerTops = [zeros(size(soilThick,1),1),cumsum(soilThick(:,1:end-1),2)];
evapDepthRepmat = repmat(evapDepth,[1,size(soilThick,2)]);
[evapLayer,evapRow] = find((evapDepthRepmat >= soilLayerTops &  evapDepthRepmat < (soilLayerTops + soilThick))');
evapInd = sub2ind(size(soilThick),evapRow,evapLayer);
end

function [depthExchange] = lookup_air_exchange_depth(airTemp,kAir,airTheta,pressureRange,airExchangeStruct)
%Log transform kAir
kAir = log10(kAir);

%Constrain to lookup values to the ranges in the lookup table
airTemp = limit_vals(airExchangeStruct.Tlimits,airTemp);
kAir = limit_vals(airExchangeStruct.Klimits,kAir);
airTheta = limit_vals(airExchangeStruct.Thetalimits,airTheta);
pressureRange = limit_vals(airExchangeStruct.P0limits,pressureRange);

%Now, do the ndlookup
[depthExchange] = interpn(airExchangeStruct.Kaxis,airExchangeStruct.Taxis,...
    airExchangeStruct.P0axis,airExchangeStruct.Thetaaxis,airExchangeStruct.exchDepth,...
    kAir,airTemp,pressureRange,airTheta);
end

