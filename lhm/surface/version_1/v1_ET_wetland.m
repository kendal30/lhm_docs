function state = v1_ET_wetland(state,structure,params)

[radNet,airVapPress,airTemp,pressure,windspeed,moistAirDens,...
    waterTemp,icePrecipWater,wetlandWater,wtDepth] = ...
    deal_struct(state,...
    'rad_short_wetland_net','vap_pressure_air','temp_air','pressure','windspeed','dens_moist_air',...
    'temp_wetland','ice_pack_precip_store','water_wetland','water_table_depth_wetland');
[wetRghHeight,latentEfficiency,...
    soilPorosity,diffCoeffVapor,gndRghHeight,soilResidTheta] = ...
    deal_struct(params,...
    'wetland_roughness_height','wetland_latent_efficiency',...
    'soil_porosity','diff_coeff_vapor','soil_rough_height','soil_theta_r');
[zerosGrid,timestepLength,indWater,indFlood,...
    indSnow,indOpen,indDeep,soilThick] = deal_struct(structure,...
    'zeros_grid','timestep_length','index_wetland','index_wetland_flooded',...
    'index_snow_upland','index_open_water','index_deep_wetland','thickness_soil');

%--------------------------------------------------------------------------
%Initialize variables
%--------------------------------------------------------------------------
[qEvap,qSens,evap,resistAero,evapIce] = deal(zerosGrid);
indShallow = ~indDeep & indWater;

%--------------------------------------------------------------------------
%Calculate shallow wetland evaporation
%--------------------------------------------------------------------------
%Calculate the aerodynamic resistance, shared for shallow and deep wetlands
roughHeight = populate_array(zerosGrid,wetRghHeight,gndRghHeight,indShallow,indFlood);

%Calculate aerodynamic resistance
resistAero(indWater) = aerodynamic_resistance(structure,params,'neutral',windspeed(indWater),roughHeight(indWater));

% %Turn off shallow wetland evaporation if there are any snow-covered upland cells
% ind = ~indSnow & indShallow & indFlood;
% if any(ind)
%     %Canopy resistance is zero for shallow wetlands
%     rc = zerosGrid(ind);
%     
%     %Calculate the potential evaporation
%     evap(ind) = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rc,resistAero(ind));
%     
%     %Reduce this by latent efficiency parameter
%     evap(ind) = evap(ind) .* latentEfficiency;
%     
%     %Multiply rate /s by timestep length to get model units
%     evap(ind) = evap(ind) * timestepLength;
% end

%For non-flooded wetland cells, if snow-covered upland, no evap in wetland cells
ind = ~indSnow & indShallow & ~indFlood;

if any(ind)
    waterDepth = wtDepth(ind) - wetlandWater(ind)./soilPorosity(ind,1);
    waterDepth(waterDepth<0) = 0; %shouldn't happen often, because then they would be flooded cells!
    
    %Evaporation rate here is equal to soil evaporation
    %Canopy resistance is actually ground resistance for soil
    rc = (2 .* waterDepth) ./...
        (soilPorosity(ind,1) .* diffCoeffVapor);
    
    %Calculate potential soil evaporation
    peSoil = penman_monteith(params,airVapPress(ind),airTemp(ind),radNet(ind),pressure(ind),rc,resistAero(ind));
    
    %Multiply rate /s by timestep length to get model units
    peSoil = peSoil * timestepLength;
    
    %Calculate actual soil evporation using the Ritchie-type evaporation
    %model
    %soilTheta = max((soilThick(ind,1) - waterDepth) ./ soilThick(ind,1), soilResidTheta(ind,1));
    %evap(ind) = v1_soil_evap_model(peSoil,soilTheta,ind,structure,params);
	evap(ind) = peSoil;
end

%--------------------------------------------------------------------------
%Calculate sensible and latent heat fluxes, and also lake evaporation
%for deep lakes only
%--------------------------------------------------------------------------
ind = indOpen & indWater & indFlood;

if any(ind)
    %Calculate waterTemp for shallow wetland cells, for now assume is the
    %average of deeper wetland cells, this will mute shallow wetland E
    %somewhat -- THIS IS INTENDED TO BE TEMPORARY
    waterTemp(indShallow,1) = mean(waterTemp(indDeep,1));
    
    %Calculate sensible and latent fluxes
    qSens(ind) = sens_flux(params,waterTemp(ind,1),airTemp(ind),moistAirDens(ind),resistAero(ind));
    [qEvap(ind),evap(ind)] = latent_flux(params,...
        waterTemp(ind,1),zerosGrid(ind)+1,airVapPress(ind),pressure(ind),...
        moistAirDens(ind),timestepLength,resistAero(ind),zerosGrid(ind));
    %no canopy resistance, and surface relative humidity is 1
end

%Limit evaporation to available water in the ice pack
ind = ~indOpen & indWater & indFlood;

if any(ind)
    %Assume ice temperature is -0.01 degrees or air temperature, whichever is cooler
    iceTemp = min(airTemp(ind),-0.01);
    qSens(ind) = sens_flux(params,iceTemp,airTemp(ind),moistAirDens(ind),resistAero(ind));
    [qEvap(ind),evap(ind)] = latent_flux(params,...
        zerosGrid(ind)-3,zerosGrid(ind)+1,airVapPress(ind),pressure(ind),...
        moistAirDens(ind),timestepLength,resistAero(ind),zerosGrid(ind));
    evapIce(ind) = min(evap(ind),icePrecipWater(ind));
    evap(ind) = 0;
end

%--------------------------------------------------------------------------
%Update the model state variables
%--------------------------------------------------------------------------
state = update_struct(state,'evap_wetland',evap,'latent_wetland',qEvap,...
    'sensible_wetland',qSens,'evap_ice_pack',evapIce);
end