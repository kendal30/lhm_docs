function state = v1_soil_temperature_module(state,structure,params)

[soilTempShallow,soilTempDeep,soilWater] = deal_struct(state,...
    'temp_soil_shallow','temp_soil_deep','water_soil');
[soilFreezeTemp,frozenImpermeability,...
    porosity,condMineral,condWater,condAir] = deal_struct(params,...
    'soil_freeze_temp','soil_frozen_impermeability',...
    'soil_porosity','therm_cond_mineral','therm_cond_water','therm_cond_air');
[landUse,indUpland,snowCovered,soilThick,...
    measSep,zerosGrid] = deal_struct(structure,...
    'land_use','index_upland','index_snow_upland','thickness_soil',...
    'soil_temp_meas_sep','zeros_grid');

%Initialize arrays
[qCond,impermeable] = deal(zerosGrid);

%Determine which cells are capable of freezing, currently Urban, Ag, and
%Barren
freezable = ((landUse.majority == 1) | (landUse.majority == 2) | (landUse.majority == 7)) & indUpland;

%Calculate the frozen soil impermeability
ind = (soilTempShallow < soilFreezeTemp) & indUpland & freezable;
if any(ind(:))
    impermeable(ind) = frozenImpermeability;
end

%Calculate the ground heat flux if there are any snowcovered cells
%calculate soil heat flux
ind = snowCovered;
if any(ind(:))
    %First, calculate the weights of each phase
    weightMineral = (1 - porosity(ind)) .* soilThick(ind);
    weightWater = soilWater(ind);
    weightAir = porosity(ind) .* soilThick(ind);

    %Then, calculate the thermal conductance
    condSoil = weighted_mean('geometric',condMineral,condWater,condAir,...
        weightMineral,weightWater,weightAir);

    %Finally, calculate the heat flux over the temperature gradient
    qCond(ind) = condSoil .* (soilTempDeep(ind) - soilTempShallow(ind)) / measSep;
end

%update the state controller
state = update_struct(state,'impermeable_frozen',impermeable,'q_cond_ground',qCond);
end
