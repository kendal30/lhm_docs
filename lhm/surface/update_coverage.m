function structure = update_coverage(state,structure,params)
%UPDATE_COVERAGE Updates snow and ice coverage maps.
%   update_coverage()
%
%   The coverage updates are done after all modules are run because all
%   modules subsequent to the snow and ice modules assume that the coverage
%   state represents that at the beginning of the timestep.  This is the
%   simplest way to update the coverage at the end of the timestep.
%
%   Descriptions of Input Variables:
%   None, takes all inputs from the data controller.
%
%   Descriptions of Output Variables:
%   None, writes all outputs to the data controller.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-01
% Copyright 2008 Michigan State University.

[snowWaterUpland,snowWaterWetland,waterIce,wetlandFraction,...
    canopyWater,streamFraction,uplandFraction,wtDepthWetland] = deal_struct(state,...
    'water_snow_upland','water_snow_wetland','water_ice_pack','fraction_wetland',...
    'water_canopy','fraction_stream','fraction_upland','water_table_depth_wetland');
[litterFraction] = deal_struct(params,'litter_fraction');
[numSoilLayers,gwDisconnThresh,onlineCoupling,...
    indGWDisconn,indGWConn] = deal_struct(structure,'num_layers_soil','wetland_disconn_thresh','groundwater_coupling_online',...
    'index_disconnected_wetlands','index_connected_wetlands');

%Other indexes that are calculated just once:
%index_internally_drained
%index_externally_drained

%Update the wetland fraction, stream fraction, and upland fraction
%For now, just return the same variables
% wetlandFraction = wetlandFraction;
% streamFraction = streamFraction;
% uplandFraction = uplandFraction;

indSnowUpland = snowWaterUpland > 0;
noSnowUpland = ~indSnowUpland;

indSnowWetland = snowWaterWetland > 0;
noSnowWetland = ~indSnowWetland;

indIceWetland = waterIce > 0;
noIceWetland = ~indIceWetland;


indUpland = uplandFraction > 0; %should just calculate once for now

indStream = streamFraction > 0; %should just calculate once for now

%Streams are included in wetlands for the purposes of all energy balance calculations
indWetland = (wetlandFraction > 0) | indStream; %should just calculate once for now

indLitter = (litterFraction > 0) & (indUpland); %should just calculate once for now
noLitter = ~indLitter;

indSoil = repmat(indUpland,1,numSoilLayers); %should just calculate once for now

indNoLitter = (noLitter) & (indUpland); %should just calculate once for now

%Update wetland connectivity
if onlineCoupling %only if coupling is online, otherwise read in indices will be output again
    indGWDisconn = (wtDepthWetland > gwDisconnThresh) & indWetland;
    indGWConn = ~indGWDisconn & indWetland;
end


indWetCanopy = canopyWater > 0;

indBareLitter = (indUpland) & (indLitter) & (noSnowUpland);

indBareSoil = (indUpland) & (noSnowUpland) & (noLitter);
indSnowSoil = (indUpland) & (indSnowUpland) & (noLitter);

indOpenWater = (indWetland) & (noSnowWetland) & (noIceWetland);

indBareIce = (indWetland) & (noSnowWetland) & (indIceWetland);

%For now, this will be updated in the wetland_hydrology_module
%indFlooded = (indWetland) & (wetlandWater >= 0);

structure = update_struct(structure,'index_snow_upland',indSnowUpland,...
    'index_snow_wetland',indSnowWetland,'index_ice_pack',indIceWetland,...
    'index_upland',indUpland,'index_wetland',indWetland,'index_stream',indStream,...
    'index_wet_canopy',indWetCanopy,...
    'index_bare_litter',indBareLitter,'index_bare_soil',indBareSoil,...
    'index_open_water',indOpenWater,'index_bare_ice',indBareIce,'index_litter',indLitter,...
    'index_soil',indSoil,'index_snow_soil',indSnowSoil,'index_no_litter',indNoLitter,...
    'index_disconnected_wetlands',indGWDisconn,'index_connected_wetlands',indGWConn,...
    'fraction_wetland',wetlandFraction,'fraction_upland',uplandFraction,...
    'fraction_stream',streamFraction);  %'index_wetland_flooded',indFlooded,...