function state = canopy_controller(state,structure,params,modules)
%CANOPY_CONTROLLER  Controls the execution of the canopy models.
%   canopy_controller()
%
%   Descriptions of Input Variables:
%   none, all inputs are fetched from the data controller
%
%   Descriptions of Output Variables:
%   none, all outputs are written to the data controller
%
%   Example(s):
%   none
%
%   See also: upland_controller wetland_controller

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-04-01
% Copyright 2008 Michigan State University.

%Save water storage as previous timestep water storage
state = update_struct(state,'prev_water_canopy',state.water_canopy);
%Get function handles
[rad_canopy,temp_canopy,water_canopy,temp_micro,irrigation] = deal_struct(modules,...
    'rad_canopy','temp_canopy','water_canopy','temp_micro','irrigation');

%Run the canopy radiation module
state = rad_canopy(state,structure,params);

%Run the irrigation module
state = irrigation(state,structure,params);

%Run the canopy hydrology module, this uses previous timestep's pe_canopy
state = water_canopy(state,structure,params);

%Run the canopy temperature module, also calculates canopy evap and
%transpiration, and updates canopy water and soil water
state = temp_canopy(state,structure,params);

%Run the canopy microclimate module, solves for below-canopy vapor pressure
%and air temperature
state = temp_micro(state,structure,params);