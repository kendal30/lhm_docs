function [state,structure,params,groundwater] = surface_controller(state,structure,...
    params,groundwater,buffers,modules)
% This is the LHM surface-process controller

% Get the variables and modules used internally
[m,n,timestep] = deal_struct(state,...
    'outer_timestep','inner_timestep','timestep');
[dates,respawnInterval,runName,groupName,numInnerSteps,splitFlag,splitNum,paths] = ...
    deal_struct(structure,...
    'dates','respawn_interval','run_name','group_name','num_inner_timesteps',...
    'split_flag','split_num','paths');
[rad_albedo,balance_water,balance_energy,input_climate,runoff_module] = deal_struct(modules,...
    'rad_albedo','balance_water','balance_energy','input_climate','water_runoff');

% Determine where to start the model run
[mStart,nStart] = start_position(m,n,numInnerSteps);

% Determine the number of output loop iterations to run
numDates = length(dates);

if mStart < numDates % Otherwise, jump to the groundwater model
    %save the model start time for respawning purposes
    startDate = datevec(dates(mStart));

    % Initialize this as all zero to force display of current date by
    % disp_progress
    currDatevec = zeros(1,6);

    % Print to the screen
    if splitFlag
        fprintf('Running split %d of LHM run %s of group %s\n',splitNum,runName,groupName);
    else
        fprintf('Running LHM run %s of group %s\n',runName,groupName);
    end

	% Capture the start time
	startTime = now;
	startDay = dates(mStart);

    % Update the coverages indices to begin
    structure = update_coverage(state,structure,params);

    for m = mStart:numDates % Now, run the models
        % Update the data controller with the outer timestep position
        state = update_struct(state,'outer_timestep',m);
        currDay = dates(m);

        % Update the year and month of the simulation on screen and update
        % currDatevec
        [currDatevec] = disp_progress(currDay,currDatevec,startTime,startDay);

        % Run the canopy evolution module
        [state,structure,params,buffers] = canopy_evolution_module(state,structure,params,buffers,currDay);

        % Update the surface albedo
        state = rad_albedo(state,structure,params);

        % Get the groundwater coupling inputs, if online coupling the model
        % pauses here to wait for the groundwater timestep to complete
        state = groundwater_coupling_input(state,structure,params);

        % Run the hourly codes
        for n = nStart:numInnerSteps
            % Increment the timestep counter, and calculate the current time
            timestep = timestep + 1;
            currTime = currDay + (n-1)/numInnerSteps;
            % Update the data controller appropriately
            state = update_struct(state,'date',currTime,'timestep',timestep,...
                'inner_timestep',n);

            % Update the stored values of the hourly climate parameters
            [state,buffers] = input_climate(state,structure,params,buffers,currTime);

            % Update the water table, calc WT recharge and discharge
            state = update_water_table(state,structure,params);

            % Update the coverage indices
            structure = update_coverage(state,structure,params);

            % Run the canopy controller,upland, and wetlands controllers
            state = canopy_controller(state,structure,params,modules);
            state = upland_controller(state,structure,params,modules);
            [state,structure] = wetland_controller(state,structure,params,modules);

            % Accumulate deep percolation, wetland transpiration, and post-
            % evaporative demand
            buffers = groundwater_coupling_output(state,structure,buffers); 

            % Run the runoff module
            state = runoff_module(state,structure,params);

            % Calculate the water and energy balances
            state = balance_water(state,structure);
            state = balance_energy(state,structure);

            % Update the output arrays
            buffers = output_controller(state,structure,buffers);
        end

        % Check to see if saving out model state grids is necessary, and do so
        % currently this is done yearly and at the end of the simulation
        buffers = dump_respawn(state,structure,params,buffers,groundwater,modules,...
            currDatevec,startDate,respawnInterval,m,numDates);

        % Reset the inner loop start position
        nStart = 1;
    end
end

% Clean up after the final run
finish_clean_up(paths);

end


%--------------------------------------------------------------------------
% Internal Functions
%--------------------------------------------------------------------------
function [mStart,nStart] = start_position(m,n,numInnerSteps)
if n == numInnerSteps % this is the respawn case
    nStart = 1;
    mStart = m + 1;
elseif n == 0 && m == 0 % this is the initial startup case
    nStart = 1;
    mStart = 1;
else % this is the crash reload case
    nStart = n + 1;
    mStart = m;
end
end


function [currDatevec] = disp_progress(currDay,lastDatevec,startTime,startDay)
currDatevec = datevec(currDay);
if currDatevec(2)~=lastDatevec(2)
    fprintf('Year: %s, Month: %s, ',datestr(currDay,'yyyy'),datestr(currDay,'mm'));
    fprintf('Rate: % g real hours/model year\n',((now-startTime)*24)/((currDay-startDay)/365.25));
end
end


function buffers = dump_respawn(state,structure,params,buffers,groundwater,modules,...
    currDatevec,startDate,respawnInterval,m,numDates)
if (sum(currDatevec(2:3)==[12,31])==2) || (m==numDates)
    %save the output data
    buffers = save_checkpoint(state,structure,params,buffers,groundwater,modules,...
        structure.paths.output,false);

    % Check to see if it's time to respawn
    if ((currDatevec(1)-startDate(1))==(respawnInterval-1)) && (m~=numDates)
        cd(structure.paths.source); % Change to the source code directory
        disp('Respawning');
        diary off
        initiate_str = ['!matlab -nosplash -minimize -r "lhm_controller(''respawn'',''',...
            structure.paths.output.dump,''')"&'];
        eval(initiate_str);
        quit
    end
end
end


function finish_clean_up(paths)
% Remove the last_ files, and the respawn_
[dumpdir,dumpFilename,dumpFileext] = fileparts(paths.output.dump);
[outdir,outFilename,outFileext] = fileparts(paths.output.output);
try
    delete([dumpdir,filesep,'last_',dumpFilename,dumpFileext]);
    delete([dumpdir,filesep,'respawn_',dumpFilename,dumpFileext]);
    delete([outdir,filesep,'last_',outFilename,outFileext]);
catch % #ok<CTCH>
    warning('Unable to delete all last_* and respawn_* files, clean up manually') % #ok<WNTAG>
end

% repack the HDF5 output files, (this can take quite a long time)
% disp('Repacking model output files');
% % repack_outfiles(paths);
% % Repack the percolation file
% h5repack(paths.output.perc,false);
% % Repack the transpiration file
% h5repack(paths.output.transp,false);
% % Repack the evap file
% h5repack(paths.output.evap,false);
% % Repack the output file
% h5repack(paths.output.output,false);
end