function state = water_balance(state,structure)
%WATER_BALANCE  Calculates the water balance
%   water_balance()
%
%   This function calculates the hourly water balance.
%   The cell-by-cell water balance is saved to the data_controller
%
%   Descriptions of Input Variables:  
%   All inputs are taken to the data controller.
%
%   Descriptions of Output Variables:
%   All inputs are written to the data controller
%
%   Example(s):
%   none
%
%   See also: energy_balance

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

[waterCanopy,waterLitter,waterDepression,waterSnowUpland,waterSoil,...
    waterWetland,waterSnowWetland,waterIcePrecip,... %Pass hydrologic storage values for this and previous timestep
    prevWaterCanopy,prevWaterLitter,prevWaterDepress,prevWaterSnowUpland,prevWaterSoil,...
    prevWaterWetland,prevWaterSnowWetland,prevWaterIcePrecip,...
    evapCanopy,evapSnowUpland,evapSnowWetland,evapDepression,...
    evapSoil,evapLitter,evapWetland,evapIce,... %Pass fluxes that enter or leave the system
    transpUpland,transpWetland,precipExcessUpland,precipExcessWetland,...
    transpWetPhreatic,evapPostDemand,...
    deepPercUpland,deepPercWetland,precip,...
    gwDischUpland,gwDischWetland,...
    rechargeWTUpland,rechargeWTWetland,irrigation,...
    prevBalance,wetlandFrac,uplandFrac,streamFrac] = ...
    deal_struct(state,...
    'water_canopy','water_litter','water_depression','water_snow_upland','water_soil',...
    'water_wetland','water_snow_wetland','ice_pack_precip_store',...
    'prev_water_canopy','prev_water_litter','prev_water_depression','prev_water_snow_upland','prev_water_soil',...
    'prev_water_wetland','prev_water_snow_wetland','prev_ice_pack_precip_store',...
    'evap_canopy','evap_snow_upland','evap_snow_wetland','evap_depression',...
    'evap_soil','evap_litter','evap_wetland','evap_ice_pack',...
    'transp_upland','transp_wetland','precip_excess_upland','precip_excess_wetland',...
    'transp_wetland_phreatic','evap_post_demand',...
    'deep_percolation_upland','deep_percolation_wetland','precip',...
    'groundwater_discharge_wetland','groundwater_discharge_upland',...
    'recharge_WT_change_wetland','recharge_WT_change_upland','irrigation_water_applied',...
    'water_balance','fraction_wetland','fraction_upland','fraction_stream');
imbalance = precip + irrigation - evapCanopy - waterCanopy + prevWaterCanopy -... %want to move irrigation to upland canopy only
    (uplandFrac) .* (deepPercUpland + precipExcessUpland + ...%Upland Components
    sum(transpUpland,2) + evapLitter + evapDepression + evapSnowUpland + evapSoil - ...
    gwDischUpland + rechargeWTUpland + ... %gw discharge adds water to system, recharge takes it out
    sum(waterSoil,2) - sum(prevWaterSoil,2) + waterDepression - prevWaterDepress + ...
    waterSnowUpland - prevWaterSnowUpland + waterLitter - prevWaterLitter) - ...
    (wetlandFrac + streamFrac) .* (deepPercWetland + precipExcessWetland + ... %Wetland Components
    transpWetland - transpWetPhreatic + evapWetland - evapPostDemand + ...
    evapSnowWetland + evapIce - ...
    gwDischWetland + rechargeWTWetland + ... %gw discharge adds water to the system, recharge takes it out
    waterWetland - prevWaterWetland + ...
    waterSnowWetland - prevWaterSnowWetland + waterIcePrecip - prevWaterIcePrecip);

if any(abs(imbalance)>1e-8)
    fprintf('Check water balance, max imbalance of: %g (m/hr)\n',max(abs(imbalance)));
end
if assert_LHM()
    assert_LHM(~any(isnan(imbalance(:))),'The water balance contains NaNs, investigate');
end

newBalance = prevBalance + imbalance; %to track cumulative cell-by-cell water balance
state = update_struct(state,'water_balance',newBalance); %update water balance
end