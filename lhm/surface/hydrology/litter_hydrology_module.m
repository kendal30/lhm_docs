function  state = litter_hydrology_module(state,structure,params)
%LITTER_HYDROLOGY_MODULE  Calculates litter hydrology components.
%   litter_hydrology_module()
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[waterLitter,evapLitter,litterFrozenFrac,...
    snowMelt,totalPrecip,waterSnow] = deal_struct(state,...
    'water_litter','evap_litter','litter_frozen_fraction','snow_melt_upland','total_throughfall','water_snow_upland');
[litterSatCap,litterFieldCap,litterCoverFrac] = deal_struct(params,...
    'litter_sat_cap','litter_field_cap','litter_fraction');
[zerosGrid,indLitter,indBare] = deal_struct(structure,...
    'zeros_grid','index_litter','index_bare_litter');

%--------------------------------------------------------------------------
% First, remove evaporation from litter water before rescaling to litter
% fraction as evapLitter has already been scaled
%--------------------------------------------------------------------------
%Remove evaporation/add dew/frost to/from litter layer
%Assumption: Ignore the difference between liquid and frozen water for this
%calculation because PE is so low during frozen times
ind = indBare;
if any(ind)
    waterLitter(ind) = waterLitter(ind) - evapLitter(ind); %evap litter calculated in interface temperature
end

%--------------------------------------------------------------------------
% Rescale storage according to litter fraction
%--------------------------------------------------------------------------
waterLitter(indLitter) = waterLitter(indLitter) ./ litterCoverFrac(indLitter);

%--------------------------------------------------------------------------
% Initialize internal arrays
%--------------------------------------------------------------------------
[waterFrozen,litterDrip,availableWater] = deal(zerosGrid);
prevWaterLitter = waterLitter;
waterLiquid = waterLitter; %initialize by assuming that there is no frozen fraction of the litter
litterLiquidCap = litterFieldCap; %this is the maximum value of the liquid holding capacity, freezing can reduce it

%--------------------------------------------------------------------------
% Calculate the water balance
%--------------------------------------------------------------------------
%Partition litter into frozen and liquid components, determine liquid
%holding capacity
ind = (waterLitter > 0) & (litterFrozenFrac > 0);
if any(ind)
    waterFrozen(ind) = waterLitter(ind) .* litterFrozenFrac(ind);
    waterLiquid(ind) = waterLitter(ind) - waterFrozen(ind);
    %Determine litter capacity taking into account the frozen part
    litterLiquidCap(ind) = min(litterFieldCap(ind),litterSatCap(ind) - waterFrozen(ind));
end

%Add input water to the liquid water portion
indSnow = (snowMelt > 0) | (waterSnow > 0);
indBare = (indBare) & ~(indSnow);
availableWater = populate_array(availableWater,totalPrecip,snowMelt,indBare,indSnow);
waterLiquid(indLitter) = waterLiquid(indLitter) + availableWater(indLitter);


%Remove water in excess of the liquid holding capacity
ind = (waterLiquid > litterLiquidCap) & indLitter;
if any(ind)
    litterDrip(ind) = waterLiquid(ind) - litterLiquidCap(ind);
    waterLiquid(ind) = waterLiquid(ind) - litterDrip(ind);
end

%Update the total water stored in the litter
waterLitter = waterFrozen + waterLiquid;

%Update the litter frozen fraction
ind = (waterLitter > 0) & (litterFrozenFrac > 0);
if any(ind)
    litterFrozenFrac(ind) = waterFrozen(ind) ./ waterLitter(ind);
end

%--------------------------------------------------------------------------
% Error checks
%--------------------------------------------------------------------------
if assert_LHM
    assert_LHM(all(litterLiquidCap(ind)>0),'Litter liquid capacity must be greater than 0, check this');
    balance = prevWaterLitter - waterLitter + snowMelt + totalPrecip - litterDrip;
    assert_LHM(all(abs(balance < 1e-10)),'Litter water balance was not achieved');
end

%--------------------------------------------------------------------------
% Rescale storage according to litter fraction
%--------------------------------------------------------------------------
waterLitter(indLitter) = waterLitter(indLitter) .* litterCoverFrac(indLitter);

%--------------------------------------------------------------------------
%Update quantities and save to data controller
%--------------------------------------------------------------------------
state = update_struct(state,'drip_litter',litterDrip,'water_litter',waterLitter,...
    'litter_frozen_fraction',litterFrozenFrac);
%Note: drip_litter is m/(unit litter area) which is unit_litter_area =
%cell_area .* litterCovFrac, water_litter = m/(cell area)
end