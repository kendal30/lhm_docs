function state = depression_storage_module(state,structure,params)
%DEPRESSION_STORAGE_MODULE  Updates depression storage
%   depression_storage_module()
%
%   Descriptions of Input Variables:
%   all inputs taken from the data_controller
%
%   Descriptions of Output Variables:
%   all outputs written to the data_controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

[precipExcess,urbanExcess] = deal_struct(state,'precip_excess_upland','precip_excess_urban');
[depCap] = deal_struct(params,'soil_dep_cap');
[indSink,zerosGrid] = deal_struct(structure,'runoff_internal_sink','zeros_grid');

ind = (precipExcess > 0);
depressionWater = zerosGrid;
if any(ind)
    % Route urban excess around depressions
    precipExcess(ind) = precipExcess(ind) - urbanExcess(ind);
    
    % Retain water in depressions
    depressionWater(ind) = min(precipExcess(ind),depCap(ind));

    % If this is a sink, retain all water
    depressionWater(indSink) = precipExcess(indSink);
    
    % Precip excess
    precipExcess(ind) = precipExcess(ind) - depressionWater(ind) + urbanExcess(ind);
end

state = update_struct(state,'precip_excess_upland',precipExcess,'water_depression',depressionWater);