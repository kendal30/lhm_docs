function state = soil_moisture_module(state,structure,params)
%SOIL_MOISTURE_MODULE  Soil moisture flux module
%   soil_moisture_module()
%
%   Calculates soil moisture fluxes, including infiltration, deep
%   percolation, and soil moisture.
%
%   Descriptions of Input Variables:
%   all inputs are taken from the data controller
%
%   Descriptions of Output Variables:
%   all outputs are written to the data controller
%
%   Example(s):
%   none
%
%   See also: upland_controller

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-21
% Copyright 2008 Michigan State University.
[soilWater,soilTemp,frozenFrac,...
    dripLitter,snowMelt,totalPrecip,depressionWater,transpiration,evaporation,...
    kTheta,waterSnow,psi,...
    dischargeUpland,wtDepth] = deal_struct(state,...
    'water_soil','temp_soil','soil_frozen_fraction',...
    'drip_litter','snow_melt_upland','total_throughfall','water_depression','transp_upland','evap_soil',...
    'soil_unsat_conductivity','water_snow_upland','matric_potential_soil',...
    'groundwater_discharge_upland','water_table_depth_upland');
[vanGenAlpha,vanGenN,porosity,thetaResid,kSat,fieldCap,...
    satCap,litterFrac,soilRefTemp,tempDependBeta,...
    iceImpedance,minKTheta,minPsi,maulemL,maulemK0,infilCap] = ...
    deal_struct(params,...
    'soil_alpha','soil_N','soil_porosity','soil_theta_r','soil_ksat','soil_field_cap',...
    'soil_sat_cap','litter_fraction','soil_van_gen_ref_temp','soil_temp_dep_beta',...
    'soil_ice_impedance','soil_min_ktheta','soil_min_psi','soil_L','soil_k0','soil_infil_cap');
[soilZerosGrid,timestepLength,zerosGrid,indUpland,indWetland,...
    indSoil,indLitter,soilThick,impervFrac] = deal_struct(structure,...
    'zeros_grid_soil','timestep_length','zeros_grid','index_upland','index_wetland',...
    'index_soil','index_litter','thickness_soil','fraction_impervious');

%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[availableWater,infiltration,precipExcess,deepPerc,exfiltration,...
    urbExcess,urbUpRunon,urbWetRunon] = deal(zerosGrid);
[qDown,qIn,dTheta,dkdTheta,dpsidTheta] = deal(soilZerosGrid);
prevSoilWater = soilWater;

%--------------------------------------------------------------------------
%Calculate available moisture for infiltration
%--------------------------------------------------------------------------
%First, determine available liquid moisture in non-litter covered areas
indSnow = (snowMelt > 0) | (waterSnow > 0);
availableWater = populate_array(availableWater,totalPrecip,snowMelt,(indUpland & ~indSnow),indSnow);
%For litter, account for partial coverage
availableWater(indLitter) = weighted_mean('arithmetic',dripLitter(indLitter),availableWater(indLitter),...
    litterFrac(indLitter),(1-litterFrac(indLitter)));
%Add in depression storage and groundwater discharge to uplands
availableWater(indUpland) = availableWater(indUpland) + depressionWater(indUpland) + dischargeUpland(indUpland);

%--------------------------------------------------------------------------
%Reduce kSat and infiltration capacity for soil freezing
%--------------------------------------------------------------------------
ind = (frozenFrac > 0) & (~indFlood);
if any(ind(:))
    kSat(ind) = kSat(ind) .* 10.^(-iceImpedance * frozenFrac(ind)); %use a impedance factor for K reduction (Hansson et al. 2004)
    infilCap(ind(:,1)) = infilCap(ind(:,1),1) .* 10.^(-iceImpedance * frozenFrac(ind(:,1)));
end

%--------------------------------------------------------------------------
%Calculate infiltration and precip excess
%--------------------------------------------------------------------------
ind = (availableWater > 0);
if any(ind)
    %Calculate urban runoff, urbBal is the amount of available water that
    %is now part of the urban acccounting, effectively urbBal =
    %availableWater - urbInfil - urbExcess - urbWetRunon
    indPervious = (impervFrac.pervious > 0);
    [urbExcess(ind),urbUpRunon(ind),urbWetRunon(ind)] = impervious_hydrology_module(...
        availableWater(ind),impervFrac.upland(ind),impervFrac.wetland(ind),impervFrac.runoff(ind),...
        indWetland(ind),indPervious(ind));
    
    %The depth of water available for infiltration in the non-urban part of
    %each cell is now the original depth plus the urban upland runon
    perviousWater = availableWater(ind) .* impervFrac.pervious(ind);
    urbConcentrate(ind) = perviousWater + urbUpRunon(ind);
    
    %Now, calculate infiltration, which only occurs in permeable areas
    infiltration(ind) = min(urbConcentrate(ind),infilCap(ind) * timestepLength .* impervFrac.pervious(ind));
    
    %Now, calculate the total precip. excess
    precipExcess(ind) = urbConcentrate(ind) - infiltration(ind) + urbExcess(ind);
    
    %Add infiltration to the first layer
    soilWater(ind,1) = soilWater(ind,1) + infiltration(ind);
end

%--------------------------------------------------------------------------
%Determine some minimum threshold values
%--------------------------------------------------------------------------
minTheta = thetaResid(indSoil) .* 1.01;
minWater = minTheta .* soilThick(indSoil);
prevTheta = max(soilWater(indSoil)./soilThick(indSoil),minTheta);

%--------------------------------------------------------------------------
%Calculate moisture redistribution
%--------------------------------------------------------------------------
if any(ind)
    %----------------------------------------------------------------------
    %Increase the stability of the Richard's Equation solution by pre-smoothing
    %the moisture field during infiltration events using a free drainage bucket
    %model
    %----------------------------------------------------------------------
    [soilWater(ind,:),deepPerc(ind),exfiltration(ind),qDown(ind,:)] = ...
        free_drainage_model(soilWater(ind,:),...
        satCap(ind,:),fieldCap(ind,:),kSat(ind,:),kTheta(ind,:),...
        soilThick(ind,:),soilZerosGrid(ind,:),timestepLength);
    
    %Add exfiltration to precipExcess
    precipExcess(ind) = precipExcess(ind) + exfiltration(ind);
    
    %----------------------------------------------------------------------
    %Save qDown for use by the soil temperature model
    %----------------------------------------------------------------------
    qDown(ind,end) = 0;
    %Don't want the last layer of freeDrainage because it's already been
    %assigned to deepPerc
    
	%TODO: This doesn't function as a pre-smoothing, it replaces the richards equation model, but
	%neglects transpiration and evpaoration for that time step, which will cause water balance problems.
else
    %----------------------------------------------------------------------
    %Calculate unsaturated hydraulic conductivity, matric potential, and
    %gradients
    %----------------------------------------------------------------------
    [kTheta(indSoil),dkdTheta(indSoil),psi(indSoil),dpsidTheta(indSoil)] = ...
        van_genuchten_model(prevTheta,porosity(indSoil),thetaResid(indSoil),...
        vanGenAlpha(indSoil),vanGenN(indSoil),kSat(indSoil),maulemK0(indSoil),maulemL(indSoil),...
        soilTemp(indSoil),soilRefTemp,tempDependBeta);

    %----------------------------------------------------------------------
    %Reduce Psi for soil freezing
    %----------------------------------------------------------------------
    ind = (frozenFrac > 0);
    if any(ind(:))
        psi(ind) = psi(ind) + -1e3-psi(ind) .* frozenFrac(ind); %linearly reduce psi as a function of the frozen fraction
    end

    %----------------------------------------------------------------------
    %Insert a manual constraint on psi and dpsidTheta to enhance stability
    %----------------------------------------------------------------------
    kTheta(kTheta < minKTheta) = minKTheta;
    testPsi = (psi < minPsi);
    psi(testPsi) = minPsi;
    dpsidTheta(testPsi) = 0;

    %----------------------------------------------------------------------
    %Solve the Richard's Equation for soil moisture fluxes
    %----------------------------------------------------------------------
    %work in m/s, be careful here, sensitive to timestep length
    %Here, use zeros for infiltration because the bucket model has already run
    [soilWater(indUpland,:),qIn(indUpland,:),dTheta(indUpland,:)] = richards_equation_model(...
        soilWater(indUpland,:),transpiration(indUpland,:)/timestepLength,...
        evaporation(indUpland)/timestepLength,zerosGrid(indUpland),...
        kTheta(indUpland,:),dkdTheta(indUpland,:),psi(indUpland,:),dpsidTheta(indUpland,:),...
        soilThick(indUpland,:),soilZerosGrid(indUpland,:),timestepLength);

    %----------------------------------------------------------------------
    %calculate subsurface freeDrainage, check to make sure freeDrainage isn't in excess
    %of residual moisture content
    %----------------------------------------------------------------------
    deepPerc(indUpland) = max(0,min((kTheta(indUpland,end) + dkdTheta(indUpland,end) .*...
        dTheta(indUpland,end)) * timestepLength, soilWater(indUpland,end) - minWater(indUpland,end)));

    %----------------------------------------------------------------------
    %Save qDown for use by the soil temperature model
    %----------------------------------------------------------------------
    qDown = -qIn * timestepLength; %qIn is defined as positive upward

end

%--------------------------------------------------------------------------
%Error Checks
%--------------------------------------------------------------------------
if assert_LHM
    balance = sum(prevSoilWater,2) + infiltration - sum(soilWater,2) -...
        evaporation - sum(transpiration,2) - deepPerc;
    assert_LHM(all(abs(balance(:) < 1e-10)),'The soil water balance has not been achieved');
    assert_LHM(all(soilWater(:) < satCap(:)),'Soil water exceeds saturation capacity');
    assert_LHM(all(soilWater(indSoil) > minWater),'Soil water is less than the minimum capacity');
    assert_LHM(all(~isnan(soilWater(:))),'An NaN appeared in the soil water');
end

%--------------------------------------------------------------------------
%Update the data controller
%--------------------------------------------------------------------------
state = update_struct(state,'matric_potential_soil',psi,'soil_unsat_conductivity',kTheta,...
    'soil_water_flux',qDown,'water_soil',soilWater,'deep_percolation_upland',deepPerc,...
    'precip_excess_upland',precipExcess,'soil_infiltration',infiltration,'precip_excess_urban',urbExcess,...
    'runon_upland_wetland',urbWetRunon);
end