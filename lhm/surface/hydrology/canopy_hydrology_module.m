function state = canopy_hydrology_module(state,structure,params)
%CANOPY_HYDROLOGY_MODULE  Calculates canopy hydrology components.
%   canopy_hydrology_module()
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

%load input parameters
[waterCanopy,gapFrac,rain,snow,precip,canopyCap,...
    irrig,irrigCanopyFrac,irrigDriftEvap,evapCanopy] = deal_struct(state,...
    'water_canopy','canopy_gap_fraction','rain','snow','precip','canopy_capacity',...
	'irrigation_water_applied','irrigation_canopy_partition','irrigation_drift_evap','evap_canopy');
[zerosGrid] = deal_struct(structure,'zeros_grid');

flagPrecip = any(rain > 0) || any(snow > 0) || any(irrig > 0) || any(evapCanopy < 0);
if flagPrecip
	%Calculate the fraction of precipitation that is rain, will use this below for calculating the rain/snow drip fraction
	rainFrac = rain ./ precip;
    rainFrac(isnan(rainFrac)) = 1;

	%Remove irrigation drift evaporation from applied irrigation prior to striking the canopy
    if any(irrig > 0)
        irrig = irrig - irrigDriftEvap;
        rainFrac = 1; %assume all rain if irrigating
    end

    %Add irrigation to rain and precip arrays, according to the partitioning defined in the irrigation module
    rain = rain + (irrig.*irrigCanopyFrac);
    precip = precip + (irrig.*irrigCanopyFrac);

    %Calculate the throughfall components, add in the irrigation that is applied below the leaves here
    throughRain = rain .* gapFrac;
    throughSnow = snow .* gapFrac;

	%Calculate the amount of precip intercepted, this is an intermediate
    %calculation
    intercepted = precip - (throughRain + throughSnow);
    %Calculate the net interception (interception - dripthrough), and
    %dripthrough
    [netStorage,dripthrough,waterCanopy] = interception_model(waterCanopy,intercepted,canopyCap,zerosGrid);

	%Partition drip and interception using same fractionation same as throughfall
    interceptRain = netStorage .* rainFrac;
    interceptSnow = netStorage - interceptRain;
    dripRain = dripthrough .* rainFrac;
    dripSnow = dripthrough - dripRain;

    %Calculate totalRain and totalSnow
    totalThroughRain = dripRain + throughRain + irrig.*(1-irrigCanopyFrac); %add in irrigation below the canopy
    totalThroughSnow = dripSnow + throughSnow;
    totalThrough = totalThroughRain + totalThroughSnow;
    
%     if any(isnan(totalThrough))
%         disp('There are NaNs in throughfall, check');
%     end

    %Update the data controller
    state = update_struct(state,'throughfall_rain',throughRain,'throughfall_snow',throughSnow,...
        'drip_rain',dripRain,'drip_snow',dripSnow,'intercept_rain',interceptRain,...
        'intercept_snow',interceptSnow,'water_canopy',waterCanopy,'canopy_capacity',canopyCap,...
        'total_throughfall_rain',totalThroughRain,'total_throughfall_snow',totalThroughSnow,'total_throughfall',totalThrough);
else
    state = update_struct(state,'throughfall_rain',zerosGrid,'throughfall_snow',zerosGrid,...
        'drip_rain',zerosGrid,'drip_snow',zerosGrid,'intercept_rain',zerosGrid,...
        'intercept_snow',zerosGrid,'water_canopy',waterCanopy,'canopy_capacity',canopyCap,...
        'total_throughfall_rain',zerosGrid,'total_throughfall_snow',zerosGrid,'total_throughfall',zerosGrid);
end



end

function [netStorage,dripthrough,waterCanopy] = interception_model(waterCanopy,intercepted,canopyCap,zerosGrid)
%This is a separate function to facilitate eventually separating the liquid
%and solid precipitation and storage behaviors

%First, calculate pre-drip from dew additions
preDrip = zerosGrid;
ind = (waterCanopy > canopyCap);
if any(ind)
    preDrip(ind) = waterCanopy(ind) - canopyCap(ind);
    waterCanopy(ind) = canopyCap(ind);
end

prevWaterCanopy = waterCanopy;
%Then, calculate new retention of moisture, currently this is the same for liquid
%and frozen precipitation, but that may change in the future
ind = (intercepted > 0);
if any(ind)
    availableWater = intercepted(ind) + waterCanopy(ind);
    waterCanopy(ind) = min(canopyCap(ind),availableWater);
end

%finally, calculate interception and dripthrough
netStorage = waterCanopy - prevWaterCanopy;
dripthrough = intercepted - netStorage + preDrip;
end
