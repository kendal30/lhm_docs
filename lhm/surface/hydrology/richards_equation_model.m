function [waterSoil,deepPerc,drainage] = richards_equation_model(waterSoil,transpiration,evaporation,infiltration,...
    kTheta,dkdTheta,psi,dpsidTheta,soilThickness,waterMin,waterMax,soilZerosGrid,timestepLength)
%RICHARDS_EQUATION_MODEL  Solves the Richard's Equation to redistribute soil moisture
%   [waterSoil,qIn,dTheta] = richards_equation_model(waterSoil,transpiration,evaporation,infiltration,...
%       kTheta,dkdTheta,psi,dpsidTheta,soilThickness,soilZerosGrid,timestepLength)
%
%   This formulation of the 1-D Richard's equation comes from CLM v. 3.0
%   documentation
%
%   Descriptions of Input Variables:
%   waterSoil: soil water content (m), multi-layer array
%   transpiration: transpiration rate (m/s), multi-layer array
%   evaporation: evaporation rate (m/s), single-layer array
%   infiltration: infiltration rate (m/s), single-layer array
%   kTheta: unsaturated hydraulic conductivity (m/s), multi-layer array
%   dkdTheta: gradient in unsaturated conductivity in volumetric soil
%       moisture, (m/s), multi-layer array
%   psi: soil matric potential (negative values), (m), multi-layer array
%   dpsidTheta: gradient in matric potential in volumetric soil moisture,
%       (m), multi-layer array
%   soilThickness: thickness of the soil layers, (m)
%   soilZerosGrid: a grid of zeros, multi-layer array
%   timestepLength: length of the model timestep (s)
%
%   Descriptions of Output Variables:
%   waterSoil: soil water content (m)
%   qIn: fluxes out of each layer into the layer above, (m/s)
%
%   Example(s):
%   none
%
%   See also: soil_moisture_module

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

numLayers = size(soilZerosGrid,2);

[thickMinus,kMinus,dkMinus,dhdzMinus,dpsiMinus,qDown,...
    dqMinusDthetaMinus,dqMinusDtheta,a,b,c,r,drainage] = deal(soilZerosGrid);

% Calculate the downward/upward flux in each layer
thickMinus(:,2:end) = weighted_mean('arithmetic',soilThickness(:,1:end-1),soilThickness(:,2:end),1,1);
thickFrac1 = soilThickness(:,1:end-1) ./ thickMinus(:,2:end);
thickFrac2 = 1 - thickFrac1;
kMinus(:,2:end) = weighted_mean('harmonic',kTheta(:,1:end-1),kTheta(:,2:end),thickFrac1,thickFrac2);
% dkMinus(:,2:end) = weighted_mean('harmonic',dkdTheta(:,1:end-1),dkdTheta(:,2:end),thickFrac1,thickFrac2);
dpsiMinus(:,2:end) = (psi(:,2:end) - psi(:,1:end-1)); %CHECK that psi values are negative
dzMinus = thickMinus; %defined as positive downward
dhdzMinus(:,2:end) = (dpsiMinus(:,2:end) ./ dzMinus(:,2:end) - 1);
%qDown(:,2:end) = -kMinus(:,2:end) ./ thickMinus(:,2:end) .* dhdzMinus(:,2:end); %ADK - seems like this is wrong, based on units
qDown(:,2:end) = -kMinus(:,2:end) .* dhdzMinus(:,2:end);

% Calculate interlayer fluxes, defined as positive downwards
layFlux = qDown(:,2:end) * timestepLength;

% [thickMinus,kMinus,dkMinus,psiMinus,dpsidzMinus,qIn,...
%     dqMinusDthetaMinus,dqMinusDtheta,a,b,c,r,drainage] = deal(soilZerosGrid);
% 
% % Calculate the downward/upward flux in each layer
% thickMinus(:,2:end) = weighted_mean('arithmetic',soilThickness(:,1:end-1),soilThickness(:,2:end),1,1);
% thickFrac1 = soilThickness(:,1:end-1) ./ thickMinus(:,2:end);
% thickFrac2 = 1 - thickFrac1;
% kMinus(:,2:end) = weighted_mean('harmonic',kTheta(:,1:end-1),kTheta(:,2:end),thickFrac1,thickFrac2);
% dkMinus(:,2:end) = weighted_mean('harmonic',dkdTheta(:,1:end-1),dkdTheta(:,2:end),thickFrac1,thickFrac2);
% psiMinus(:,2:end) = psi(:,2:end) - psi(:,1:end-1);
% dz_minus = -thickMinus;
% dpsidzMinus(:,2:end) = (psiMinus(:,2:end) + dz_minus(:,2:end)) ./ dz_minus(:,2:end);
% qIn(:,2:end) = -kMinus(:,2:end) .* dpsidzMinus(:,2:end);
% 
% % Calculate interlayer fluxes
% layFlux = -qIn(:,2:end) .* soilThickness(:,end) * timestepLength;


% Move water upward - interlayer fluxes
testUp = layFlux < 0;
drainUp = -(layFlux .* testUp); %flip sign, since drainage is positive downwards

drainUp = min(max(waterMax(:,1:end-1)-waterSoil(:,1:end-1),0),drainUp);
drainUp = min(max(waterSoil(:,2:end)-waterMin(:,2:end),0),drainUp);
waterSoil(:,1:end-1) = waterSoil(:,1:end-1) + drainUp;
waterSoil(:,2:end) = waterSoil(:,2:end) - drainUp;

% Move water downward - interlayer fluxes
testDown = layFlux > 0;
drainDown = layFlux .* testDown;

drainDown = min(max(waterSoil(:,1:end-1)-waterMin(:,1:end-1),0),drainDown);
drainDown = min(max(waterMax(:,2:end)-waterSoil(:,2:end),0),drainDown);
waterSoil(:,1:end-1) = waterSoil(:,1:end-1) - drainDown;
waterSoil(:,2:end) = waterSoil(:,2:end) + drainDown;

% Add interlayer fluxes to the drainage array
layFlux = -drainUp + drainDown;
drainage(:,1:end-1) = layFlux;

% Remove deep percolation
deepPerc = min(kTheta(:,end) * timestepLength, max(waterSoil(:,end)-waterMin(:,end),0));
if any(deepPerc<0)
    disp('Deep Percolation is Negative');
end
drainage(:,end) = deepPerc;
waterSoil(:,end) = waterSoil(:,end) - drainage(:,end);

% ADK - commented this out for now, doesn't seem to work right, thus this module isn't currently solving
% the Richard's equation, it's just using the full unsaturated matric potential to solve fluxes
% precalc1 = kMinus(:,2:end) ./ dz_minus(:,2:end);
% precalc2 = dkMinus(:,2:end) .* dpsidzMinus(:,2:end);
% dqMinusDthetaMinus(:,2:end) = precalc1 .* dpsidTheta(:,1:end-1) - precalc2;
% dqMinusDtheta(:,2:end) = -precalc1 .* dpsidTheta(:,2:end) - precalc2;
%
%
% %%set up tridiagonal system of equations
% %for the 1st layer
% ind = 1;
% b(:,ind) = dqMinusDthetaMinus(:,ind+1) - soilThickness(:,ind)/timestepLength;
% c(:,ind) = dqMinusDtheta(:,ind+1);
% r(:,ind) = transpiration(:,ind) + evaporation - infiltration - qIn(:,ind+1);
%
% %for interior layers
% ind = (2:numLayers-1);
% a(:,ind) = -dqMinusDthetaMinus(:,ind);
% b(:,ind) = dqMinusDthetaMinus(:,ind+1) - dqMinusDtheta(:,ind) - soilThickness(:,ind)/timestepLength;
% c(:,ind) = dqMinusDtheta(:,ind+1);
% r(:,ind) = transpiration(:,ind) + qIn(:,ind) - qIn(:,ind+1);
%
% %for the bottom layer
% ind = numLayers;
% a(:,ind) = -dqMinusDthetaMinus(:,ind);
% b(:,ind) = -dkdTheta(:,ind) - dqMinusDtheta(:,ind) - soilThickness(:,ind)/timestepLength;
% r(:,ind) = transpiration(:,ind) + qIn(:,ind) + kTheta(:,ind);
%
% %%now solve the tridiagonal system
% dTheta = tridiagonal_solver(a,b,c,r) * timestepLength;
%add this into the previous soil water array
%waterSoil = waterSoil + dTheta  .*  soilThickness;
