function [watershedRunoff] = runoff_model(watershedRunoff,precipExcess,indTimestep,...
    flowtimes,watershedRelate,watershedKey,timestepLength,cellResolution)
%this function routes water from grid cells to stream cells
%timestepLength must be in seconds
%output is average runoff in m^3/s
if any(precipExcess(:)>0)
    %Run the runoff routing function
    watershedRunoffTemp = runoff_routing(flowtimes,precipExcess,watershedKey);

    %Now, accumulate cell-by-cell runoff and times within each timestep at
    %the gauge, and add upstream gauges, this will start at the top of the
    %model domain (by elevation of gauge) and move downward, adding in the
    %already-aggregated flows from upstream gauges
    for i=1:length(watershedRunoffTemp)
        %Check to see if this watershed has any upstream sheds
        upstreamShedsInd = find(watershedRelate(:,2)==watershedRelate(i,1));

        %Loop through those upstream sheds, adding their runoff with a time
        %offset to this gauge
        for k=1:length(upstreamShedsInd)
            watershedRunoffTemp{i} = cat(2,cat(1,watershedRunoffTemp{i}(:,1),watershedRunoffTemp{upstreamShedsInd(k)}(:,1)+...
                watershedRelate(upstreamShedsInd(k),3)),cat(1,watershedRunoffTemp{i}(:,2),watershedRunoffTemp{upstreamShedsInd(k)}(:,2)));
        end

        %Calculate which timestep each cell's runoff arrives at the gauge
        tempRunoffTimesteps = ceil(watershedRunoffTemp{i}(:,1)/timestepLength);
        tempRunoffTimesteps(tempRunoffTimesteps==0) = 1;

        %Sum the runoff within each timestep
        tempRunoff = accumarray(tempRunoffTimesteps,watershedRunoffTemp{i}(:,2));

        %Determine the indices of the overall watershed runoff array
        indStart = indTimestep;
        indEnd = indTimestep + length(tempRunoff) - 1;

        %Lengthen watershed runoff if necessary
        test_length = length(watershedRunoff{i});
        if indEnd>test_length
            watershedRunoff{i} = [watershedRunoff{i};zeros((indEnd)-test_length,1)];
        end

        %Add this timestep's runoff the the overall array
        watershedRunoff{i}(indStart:indEnd) = watershedRunoff{i}(indStart:indEnd) + ...
            tempRunoff * cellResolution^2 / timestepLength;
    end
end
end

function [watershedRunoff] = runoff_routing(flowtimes,runoff,watershedKey)
%this subfunction takes a grid of flowtimes, runoff fluxes, and watersheds
%and outputs a cell array of runoff fluxes and time of entry into the
%stream (or time of arrival at a gauge site)
numSheds = size(watershedKey,1);
watershedRunoff = cell(length(numSheds),1);
for m=1:numSheds %this part of the operation can easily be done in parallel
    watershedRunoff{m} = [flowtimes(watershedKey{m,2}),runoff(watershedKey{m,2})];
end
end