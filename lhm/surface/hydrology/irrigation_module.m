function state = irrigation_module(state,structure,params)
% IRRIGATION_MODULE  Applies irrigation to the model
%   irrigation_module()
%
%   Calculates irrigation application to the canopy.
%
%   Descriptions of Input Variables:
%   all inputs are taken from the data controller
%
%   Descriptions of Output Variables:
%   all outputs are written to the data controller
%
%   Example(s):
%   none
%
%   See also: upland_controller

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2014-09-27
% Copyright 2014 Michigan State University.

if structure.irrigation_enabled
    [indActive,eventWater,annualTotal,airTemp,waterSoil,lai,lastIrrig,...
        accumET,eventSize,intensity,waitDays,...
        evapSoil,transpUpland,canopyHeight,...
        windspeed,infiltration,deepPerc,floodDepth,currTime] = deal_struct(state,...
        'irrigation_active','irrigation_event_water','irrigation_annual_water','temp_air','water_soil','lai','irrigation_last_apply',...
        'irrigation_accum_et','irrigation_event_size','irrigation_intensity','irrigation_wait_days',...
        'evap_soil','transp_upland','canopy_height',...
        'windspeed','soil_infiltration','deep_percolation_upland','water_depression','date');
    [irrigStruct,timestep,hoursInner,zerosGrid] = deal_struct(structure,...
        'irrigation','timestep_length','inner_timestep_hours','zeros_grid');
    [eventMax,eventMin,layerCheckMax,soilIrrigThresh,...
        tempDormant,dateStart,dateEnd,laiThresh,floodThresh,minWaitDays] = deal_struct(params,...
        'irrigation_event_max','irrigation_event_min','irrigation_check_layer_max','soil_irrigation_thresh',...
        'stomatal_temp_dormant','irrigation_start_date','irrigation_end_date','irrigation_lai_thresh',...
        'irrigation_flood_thresh','irrigation_min_wait_days');

    % Indeces used below
    indIrrig = irrigStruct.present;
    indDate = (date2doy(currTime) >= dateStart) & (date2doy(currTime) < dateEnd);
    indETSched = (irrigStruct.schedType == 1) & indIrrig; % ET and soil moisture scheduling
    indSMSched = (irrigStruct.schedType == 2) & indIrrig; % Soil moisture scheduling
    indFlood = (irrigStruct.flood); % flood irrigation

    % Reset the event water, event size, irrigActive, and lastIrrig if needed
    % This occurs if we have hit the eventSize, eventMax, the annual allocation,
    % or if we are no longer in the growing season
    indAnnualLim = annualTotal >= irrigStruct.limitation;

    if any(indActive)
        % Tests
        testEventLim = eventWater >= eventSize;
        indReset = indActive & (testEventLim | indAnnualLim | ~indDate);
        % Resets
        indActive(indReset) = 0;
        eventWater(indReset) = 0;
        eventSize(indReset) = 0;
        accumET(indReset) = 0;
        intensity(indReset) = 0;
        lastIrrig(indReset) = currTime-hoursInner/24; % Previous hour was last with irrigation
    end

    % Reset annual allowable irrigation if it's the first of the year
    currDatevec = datevec(currTime);
    if all(currDatevec(2:4)==[1,1,0])
        annualTotal = zerosGrid;
    end


    % Now, start the irrigation event calculations
    % Proceed if we are actually on a warm day in the growing season
    [irrigApply,irrigDriftEvap,irrigCanopyFrac] = deal(zerosGrid); % initialize here, whether we irrigate or not
    indTemp = airTemp > tempDormant;
    if any(indTemp & indDate)
        % Calculate the ET since the last irrigation event, subtract out infiltration, accumulate this
        indWaiting = indDate & ~indActive & indIrrig;
        accumET(indWaiting) = accumET(indWaiting) + sum(transpUpland(indWaiting),2) + ...
            evapSoil(indWaiting) - (infiltration(indWaiting) - deepPerc(indWaiting));

        %Irrigation will apply if: 1) the LAI exceeds the threshold, 2) we have
        %waited long enough since the last irrigation event, 3) if
        %the soil moisture is below the required threshold, 4) if the flood depth
        %falls below the specified threshold and 5) if the
        %accumulated ET has reached a minimum value, optionally

        % 1) Check the LAI threshold
        indLai = lai > laiThresh;

        % 2) Check the wait since last irrigation
        indWaitDone = (currTime - lastIrrig) > waitDays;

        % 3) Calculate the soil moisture application threshold
        indThreshSoil = sum(waterSoil(:,1:layerCheckMax),2) <= soilIrrigThresh;

        % 4) Check if the flooded depth falls below the specified threshold
        indFloodDepth = floodDepth < floodThresh;

        % 5) Optional, ET accumulation threshold
        indThreshET = accumET > eventMin;

        % Build the complete test index array
        indEvent = (~indAnnualLim & indTemp & indDate & indIrrig & indLai & indWaitDone) & ...
            ((indThreshSoil & indSMSched) | (indThreshSoil & indThreshET & indETSched)) | ...
            (indFloodDepth & indFlood) | indActive;
        indNewEvent = indEvent & ~indActive;

        if any(indEvent)
            % Determine the event size for new events
            % If the irrigation is ET-scheduled, constrained to be no greater
            % than event max
            ind = indNewEvent & indETSched;
            if any(ind)
                eventSize(ind) = min(accumET(ind),eventMax(ind));
            end

            % if it is soil moisture scheduled, use the provided fixed amount
            ind = indNewEvent & indSMSched;
            if any(ind)
                eventSize(ind) = eventMax(ind);
            end

            % Either way, reset the accumulated ET
            accumET(indNewEvent) = 0;

            % Calculate the irrigation application time, in days, same as wait days for a
            % future event
            waitDays(indNewEvent) = (irrigStruct.fieldSize(indNewEvent) .* eventSize(indNewEvent) ./ ...
                irrigStruct.systemYield(indNewEvent))/86400; % convert from seconds to days

            % Calculate intensity of irrigation, which will be in /timestep units
            % this calculation only works with center pivots, at the moment
            pivotVelocity = (irrigStruct.fieldSize(indNewEvent) * pi()).^0.5 ./ ...
                (waitDays(indNewEvent) * 86400/timestep); % For the velocity half-way down a pivot, in /timestep
            timeSprinkler = (irrigStruct.sprayWidth(indNewEvent) ./ pivotVelocity); % time each point is being watered, in timesteps
            timeSprinkler(timeSprinkler<1) = 1; % limit to 1 timestep, this will limit my ability to simulate runoff effects
            % but the finest routing timestep is hourly now anyway in LHM
            intensity(indNewEvent) = eventSize(indNewEvent) ./ timeSprinkler; % in m/timestep

            % Set wait days at a minimum value
            waitDays(indNewEvent) = max(waitDays(indNewEvent),minWaitDays); % minimum wait days

            % For both new and ongoing events
            % Check how much water is left in the annual allocation
            allocRemain = max(0,irrigStruct.limitation(indEvent)-annualTotal(indEvent));

            % Check how much water is left in the event
            eventRemain = max(0,eventSize(indEvent)-eventWater(indEvent));

            % Add water at the specified intensity, provided there is water remaining to apply
            irrigApply(indEvent) = min(min(allocRemain,eventRemain),intensity(indEvent));

            % Update the active irrigation array
            indActive = irrigApply > 0;

            % Calculate irrigation wind drift evaporation losses
            irrigDriftEvap(indActive) = calc_wind_drift(irrigApply(indActive),...
                windspeed(indActive),airTemp(indActive));

            % Now, scale this between sprinkler heights at 1.5 meters above the canopy and 0 (or sub-canopy)
            scaleHeight = (irrigStruct.height(indActive) - canopyHeight(indActive))/1.5;
            scaleHeight(scaleHeight<0) = 0;
            scaleHeight(scaleHeight>1) = 1;
            irrigDriftEvap(indActive) = irrigDriftEvap(indActive) .* scaleHeight;

            % For ET scheduling add this amount to the irrigApply in order to accurately compensate for total crop ET
            ind = indETSched & indActive;
            if any(ind)
                irrigApply(ind) = irrigApply(ind) + irrigDriftEvap(ind);
            end

            % Finally, update the annual and event totals
            eventWater = eventWater + irrigApply;
            annualTotal = annualTotal + irrigApply;
        end

        % Now, update the irrigation canopy/ground partition array
        if any(indActive)
            % Calculate irrigation canopy fraction for all cells
            irrigCanopyFrac = irrigStruct.height ./ canopyHeight;
            irrigCanopyFrac(irrigCanopyFrac>1) = 1;
            irrigCanopyFrac(isnan(irrigCanopyFrac)) = 1;
            irrigCanopyFrac(irrigStruct.subsurface) = 0;
        end
    end

    % Update state variables
    state = update_struct(state,'irrigation_water_applied',irrigApply,...
        'irrigation_event_water',eventWater,'irrigation_annual_water',annualTotal,...
        'irrigation_active',indActive,'irrigation_last_apply',lastIrrig,...
        'irrigation_canopy_partition',irrigCanopyFrac,'irrigation_accum_et',accumET,...
        'irrigation_event_size',eventSize,'irrigation_intensity',intensity,...
        'irrigation_wait_days',waitDays,'irrigation_drift_evap',irrigDriftEvap);

    end
end


function irrigDriftEvap = calc_wind_drift(irrigApply,windspeed,airTemp)
% Calculate wind-drift evaporative losses (WDEL)
% This will take the approach of an ensemble of statistical models

wdel = zeros(size(irrigApply,1),6);

% Sadeghi et al. 2015
wdel(:,1) = 100 - (0.9202 - 0.01555 * windspeed)*100; % solid-set, for a 1.5 m sprinkler height, 15 psi

% Playan et al. 2005
wdel(:,2) = -2.1 + 1.91 * windspeed + 0.231 * airTemp; % E18, for a moving lateral

% Faci et al. 2001 (see Playan 2005)
wdel(:,3) = -3.7 + 2.58 * windspeed + 0.47 * airTemp; % assuming a 5mm nozzle diameter, moving lateral I believe

% Dechmi et al. 2003 (see Playan 2005)
wdel(:,4) = 7.479 + 5.287 * windspeed; % not sure about the system on this one

% Playan et al. 2004 (see Playan 2005)
wdel(:,5) = 1.55 + 1.13 * windspeed; % not sure about the system on this one

% King et al. 2012
wdel(:,6) = (1.371 * windspeed - 3.023) + (0.2535 * windspeed - 0.3052); % solid set, 1.2m height, sum aerosolized and drift components


% Naively average statistical models to get total wind drift as a percent of what's applied, multiply by applied to get as m/timestep
meanWdel = median(wdel,2)/100;
meanWdel(meanWdel<0) = 0;
irrigDriftEvap = meanWdel.*irrigApply;

end
