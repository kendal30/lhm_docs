function [state,structure] = wetland_hydrology_module(state,structure,params)
%WETLAND_HYDROLOGY_MODULE  Calculates wetland hydrologic fluxes
%   wetland_hydrology_module()
%
%   Descriptions of Input Variables:
%   precip: precipitation in meters/timestep
%   wetlandWater: water stored in wetlands prior to the current timestep in
%       meters
%
%   Descriptions of Output Variables:
%   evap: evaporation in m/timestep
%   transp: transpiration in m/timestep
%   wetlandWater: water stored in wetlands after the current timestep, (m)
%   recharge: water recharged beneath wetlands (m/timestep)
%   precipExcess: runoff from wetlands (m/timestep)
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

%Fetch state variables
[evap,transp,wetlandWater,throughfall,iceMelt,...
    wetlandFrac,streamFrac,uplandFrac,wtDepth,upWetRunon,...
    gwDischarge,rechargeWTWetland] = deal_struct(state,...
    'evap_wetland','transp_wetland','water_wetland','total_throughfall','ice_pack_precip_release',...
    'fraction_wetland','fraction_stream','fraction_upland','water_table_depth_wetland','runon_upland_wetland',...
    'groundwater_discharge_wetland','recharge_WT_change_wetland');
%Fetch model parameters
[conductConn,conductDisconn,...
    recessionConst,runoffThreshold,soilThetaAvail,...
    infilCap] = ...
    deal_struct(params,...
    'wetland_cond_connected','wetland_cond_disconnected',...
    'wetland_recession','wetland_run_min_threshold','soil_theta_avail',...
    'soil_infil_cap');
[indGWDisconn,indGWConn,...
    indSWConn,zerosGrid,indWetland,indOpen,...
    indIce,timestepLength] = deal_struct(structure,...
    'index_disconnected_wetlands','index_connected_wetlands',...
    'index_externally_drained','zeros_grid','index_wetland','index_open_water',...
    'index_ice_pack','timestep_length');

% Module parameter
maxConcFactor = 100; %this corresponds to allowing upWetRunon to not be concentrated beyond 100x in wetlands (i.e.
% the wetland effectively cannot be less than 1% of the model cell. Runoff
% beyond this will become precip excess

%--------------------------------------------------------------------------
%Calculate internal indexes (evaluate if these should be included in
%update_coverage)
%--------------------------------------------------------------------------
indOpenConn = indGWConn & indOpen;
indOpenDisconn = indGWDisconn & indOpen;

%--------------------------------------------------------------------------
%Initialize Arrays
%--------------------------------------------------------------------------
[evapDemand,evapStorage,recharge,transpStorage,precipExcess,transpPhreatic,...
    effThreshold,transpActual,evapActual,relativeHead,rechargeHead,...
    storeUpWetRunon,excessUpWetRunon] = deal(zerosGrid);
totalFrac = wetlandFrac + streamFrac;
indFlood = false(size(zerosGrid));

%Really want water table height above bed surface, not depth to water
wtHeight = -wtDepth;
%Also, set wtHeight=0 for inGWDisconn cells
wtHeight(indGWDisconn) = 0; 

%Available storage in the subsurface, approximate as the first layer theta
%for all layers
soilThetaAvail = soilThetaAvail(:,1);

%Subtract out recharge to WT
wetlandWater(indWetland) = wetlandWater(indWetland) - rechargeWTWetland(indWetland); %update the wetland water

%Update the flooding status in the wetland, from changes to water table depth
[indFlood(indWetland),relativeHead(indWetland)] = relative_head(...
    wtHeight(indWetland),wetlandWater(indWetland),soilThetaAvail(indWetland),zerosGrid(indWetland));  

%--------------------------------------------------------------------------
%Add new sources of water to wetlandWater, calculate infiltration for dry
%wetlands
%--------------------------------------------------------------------------
newWater = populate_array(zerosGrid,throughfall,iceMelt,indOpen,indIce);

%Add condensation to the open cells
ind = indOpen & (evap < 0);
newWater(ind) = newWater(ind) - evap(ind);
evapActual(ind) = evap(ind);
evap(ind) = 0;

%Add runon from upland areas in cell, rescaling by the uplandFrac
%Add a limit of runon, based on the concentration factor
ind = (upWetRunon > 0) & indWetland; 
if any(ind)
    origUpWetRunon = upWetRunon(ind) .* uplandFrac(ind) ./ totalFrac(ind); %rescaled to wetland domain
    concFactor = min(uplandFrac(ind) ./ totalFrac(ind), maxConcFactor); %limited by maxConcFactor
    storeUpWetRunon(ind) = upWetRunon(ind) .* concFactor; %storage, limited
    excessUpWetRunon(ind) = origUpWetRunon - storeUpWetRunon(ind); 
    %newWater(ind) = newWater(ind) + storeUpWetRunon;
end

%Add input from groundwater discharged via drains
ind = (gwDischarge > 0);
if any(ind)
    newWater(ind) = newWater(ind) + gwDischarge(ind);
end

%For those without storage at the surface, calculate infiltration capacity
%and then precipitation excess
ind = (~indFlood);
if any(ind)
    newWater(ind) = newWater(ind) + storeUpWetRunon(ind); %add upWetRunon here for dry cells
    infiltration = min(newWater(ind),infilCap(ind) * timestepLength);
    precipExcess(ind) = newWater(ind) - infiltration + excessUpWetRunon(ind);
    wetlandWater(ind) = wetlandWater(ind) + infiltration;
end

%--------------------------------------------------------------------------
%Update storage and calculate Precip Excess Runoff in Flooded Cells
%--------------------------------------------------------------------------
ind = (indFlood);
if any(ind)
    % Add newWater and upWetRunon here
    wetlandWater(ind) = wetlandWater(ind) + newWater(ind) + storeUpWetRunon(ind);
    
    %Calculate an effective runoffThreshold, given the respective fractions of
    %each type, runoff threshold of streams is always 0
    effThreshold(ind) = runoffThreshold * wetlandFrac(ind) ./ totalFrac(ind);
    
    %Now, proceed only with cells whos water storage exceeds the effective
    %runoff threshold, this statement is nested because I want to not
    %include cells that had infiltration-calculated excess this timestep
    ind1 = ind;
    ind1(ind) = relativeHead(ind) > effThreshold(ind);
    
    if any(ind1)
        %Water falling on rivers is all directly removed, calculate an effective
        %recession constant
        effRecession = (streamFrac(ind1) + recessionConst * wetlandFrac(ind1)) ./ totalFrac(ind1);
        
        %Calculate the net precip excess, this will not include cells with
        %infiltration-calculated excess
        precipExcess(ind1) = min(wetlandWater(ind1), effRecession .* (relativeHead(ind1) - effThreshold(ind1)));
        
        %Remove this from storage
        wetlandWater(ind1) = wetlandWater(ind1) - precipExcess(ind1);
    end
    
    % Add excess upWetRunon here, to directly route out of the wetlands
    precipExcess(ind) = precipExcess(ind) + excessUpWetRunon(ind);
end

%Update the water level in the wetland, from addition of new water
[indFlood(indWetland),relativeHead(indWetland)] = relative_head(...
    wtHeight(indWetland),wetlandWater(indWetland),soilThetaAvail(indWetland),zerosGrid(indWetland));  

%--------------------------------------------------------------------------
%Calculate Transpiration and Evaporation, this goes to either surface or phreatic zone
%--------------------------------------------------------------------------
if any(transp > 0) %this is worth testing, since transpiration is 0 for many hours of the day
    %First, for disconnected wetlands
    transpStorage(indWetland) = max(0,min(wetlandWater(indWetland),transp(indWetland)));
    
    %Remove transpiration from storage
    wetlandWater(indWetland) = wetlandWater(indWetland) - transpStorage(indWetland);
    
    %Then, for connected wetlands, route excess demand to phreatic zone
    transpPhreatic(indGWConn) = transp(indGWConn) - transpStorage(indGWConn);
    
    %Now, update the actual transpiration array, accounting for moisture
    %limitation in disconnected wetlands
    transpActual = populate_array(transpActual,transp,transpStorage,...
        indGWConn,indGWDisconn);
end

if any(evap > 0) %this is worth testing, since evaporation is 0 when snow-covered or ice-covered
    %Calculate remaining water for evaporation
    ind = indOpen & (evap > 0);
    evapStorage(ind) = max(0,min(wetlandWater(ind), evap(ind)));
    
    %Remove this from storage
    wetlandWater(ind) = wetlandWater(ind) - evapStorage(ind);
    
    %Calculate remaining evaporative demand for gw connected wetlands
    ind = indOpenConn & (evap > 0);
    evapDemand(ind) = evap(ind) - evapStorage(ind);
    
    %Now, update the actual evaporation array
    ind2 = indOpenDisconn & (evap > 0);
    evapActual = populate_array(evapActual,evap,evapStorage,...
        ind,ind2);
end

%Update the water level in the wetland, from transpiration and evaporation
[indFlood(indWetland),relativeHead(indWetland)] = relative_head(...
    wtHeight(indWetland),wetlandWater(indWetland),soilThetaAvail(indWetland),zerosGrid(indWetland));  

%--------------------------------------------------------------------------
%Calculate Recharge
%--------------------------------------------------------------------------
%Allow in cells not connected to GW (indGWDisconn) or for internally drained
%cells
ind = (wetlandWater > 0) & (indGWDisconn | ~indSWConn);
if any(ind)
    %Calculate recharge head, must be positive, wtHeight is positive upward,
    rechargeHead(ind) = relativeHead(ind) - wtHeight(ind);
    
    %Populate the conductance array
    conductance = populate_array(zerosGrid,conductConn,conductDisconn,indGWConn,indGWDisconn);
    
    %Calculate recharge
    ind2 = ind & (rechargeHead > 0);
    recharge(ind2) = min(rechargeHead(ind2) .* conductance(ind2),wetlandWater(ind2));
    
    %Remove this from storage
    wetlandWater(ind2) = wetlandWater(ind2) - recharge(ind2);
end

%Update the water level in the wetland, from recharge
[indFlood(indWetland)] = relative_head(...
    wtHeight(indWetland),wetlandWater(indWetland),soilThetaAvail(indWetland),zerosGrid(indWetland));

if any(wetlandWater(indWetland)<0)
    disp('Wetland water storage went negative, check');
end
if any(wetlandWater(indWetland)>100)
    disp('Wetland water storage > 100 m, check');
end
if any(isnan(wetlandWater(indWetland)))
    disp('There NaNs in the wetland water storage, check');
end
if any(precipExcess(:)>100)
    disp('Check precip excess, wetland');
end    
%--------------------------------------------------------------------------
%Update the data controller
%--------------------------------------------------------------------------
state = update_struct(state,'transp_wetland_phreatic',transpPhreatic,...
    'evap_post_demand',evapDemand,'deep_percolation_wetland',recharge,...
    'water_wetland',wetlandWater,'precip_excess_wetland',precipExcess,...
    'transp_wetland',transpActual,'evap_wetland',evapActual);
structure = update_struct(structure,'index_wetland_flooded',indFlood);
end


function [indFlood,relativeHead] = relative_head(wtHeight,wetlandWater,soilThetaAvail,zerosGrid)
%Calculates water head relative to wetland bed surface
%First, calculate the available subsurface storage, to determine how much
%is left on the surface
availSubsurf = max(0,-wtHeight.*soilThetaAvail); %wtHeight is positive upwards, so this will be return 0 if wtHeight is above ground surface
remainSurf = wetlandWater - availSubsurf; %will be >0 if water is above the surface

%Determine indices for how to calculate head
indFlood = (remainSurf >= 0);

if nargout == 2
    %Now calculate heads, 3 cases
    relativeHead = zerosGrid;
    
    %Case 1 - WT below surface, water level in wetland above
    indCase1 = indFlood & (wtHeight < 0);
    relativeHead(indCase1) = remainSurf(indCase1);
    
    %Case 2 - WT below surface, water level in wetland below
    indCase2 = ~indFlood;
    relativeHead(indCase2) = wtHeight(indCase2) + wetlandWater(indCase2) ./ soilThetaAvail(indCase2);
    
    %Case 3 - WT above surface, water storage above surface (or 0)
    indCase3 = indFlood & (wtHeight >= 0);
    relativeHead(indCase3) = wtHeight(indCase3) + wetlandWater(indCase3);
end
end