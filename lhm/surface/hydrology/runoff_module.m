function state = runoff_module(state,structure,params)

    %Fetch variables common to both external runoff routing and internal runoff
    %routing
    [precipExcessUpland,precipExcessWetland,evapDemand,...
        wetlandFrac,uplandFrac,streamFrac,...
        waterWetland,waterDepression,timestep] = ...
        deal_struct(state,'precip_excess_upland','precip_excess_wetland','evap_post_demand',...
        'fraction_wetland','fraction_upland','fraction_stream',...
        'water_wetland','water_depression','timestep');
    [indExternal,wetComplex,indRunUp,runIntFrac,runIntBasin,zerosGrid] = ...
        deal_struct(structure,'index_externally_drained','wetland_complex',...
        'index_runoff_internal_upland','runoff_internal_frac','runoff_internal_basin','zeros_grid');
    
    if structure.runoff_enabled
        [watershedRunoff,watershedEvapDemand] = deal_struct(state,...
            'watershed_runoff','watershed_evap_demand');
        [flowtimes,relateWatersheds,watershedKey,cellResolution,timestepLength] = deal_struct(structure,...
            'runoff_flowtimes','runoff_watershed_relate','runoff_watershed_key',...
            'cell_resolution','timestep_length'); 
    end
%     % This code probably doesn't work, but I moved it here from groundwater_coupling_output
%     if onlineCoupling > 0
%         [evapUpland] = deal_struct(state,'evap_upland_post_demand');
%     end

    %Calculate some internally-used indices
    indInternal = ~indExternal;
    testInternal = any(indInternal);
    
    %Initialize some variables
    [runonUpland,runonWetland] = deal(zerosGrid);
    
    %Calculate the total precip excess in each cell from the three domains
    totalExcess = precipExcessUpland .* uplandFrac + precipExcessWetland .* (wetlandFrac + streamFrac);

    %Calculate post-evaporative demand for "routing", which will be removed later
    %evapDemand is set in wetland_hydrology_module and is always positive
    evapDemand = evapDemand .* wetlandFrac; %for online coupling: evapUpland .* uplandFrac

    % Runoff routing for external cells
    if any(totalExcess>0)
        %Calculate the external excess piece
        externalExcess = totalExcess .* indExternal;

        %Only run if there are gauges to observe flow, or not deactivtated by user
        if structure.runoff_enabled    
            %Run the runoff model accumulation function
            [watershedRunoff] = runoff_model(watershedRunoff,externalExcess,timestep,...
                flowtimes,relateWatersheds,watershedKey,timestepLength,cellResolution);
        end
    
        if testInternal
            %Now, run the internal drainage runon piece
            %Need just the internal excess amount
            internalExcess = totalExcess - externalExcess;
        else
            internalExcess = zerosGrid;            
        end
    else
        if testInternal
            internalExcess = zerosGrid;
        end
    end
    
    % Post-evaporative routing for external cells
    if any(evapDemand<0)
        %Only run if there are gauges to observe flow, or not deactivtated by user
        if structure.runoff_enabled    
            %Calculate the external demand piece
            externalDemand = evapDemand .* indExternal;
    
            %Run the runoff model accumulation function
            [watershedEvapDemand] = runoff_model(watershedEvapDemand,externalDemand,timestep,...
                flowtimes,relateWatersheds,watershedKey,timestepLength,cellResolution);
        end

        if testInternal
            %Need just the internal excess amount
            internalDemand = evapDemand .* indInternal;

            %Subtract from internalExcess
            internalExcess = internalExcess + internalDemand; %internal demand is negative
        end
    end

    % Now, route internal drainage to sinks
    % This routine will lose some water because of unsatisfied demand
    if testInternal
        %Sum excess by basin, convert to grid, scale by sink runon fraction
        sumExcess = accumarray(runIntBasin,internalExcess);
        gridExcess = sumExcess(runIntBasin);
        gridExcess = gridExcess .* runIntFrac;

        %Define wetland and upland runon, rescaling appropriately
        %First, route to sinks with only upland areas--no wetlands
        runonUpland(indRunUp) = gridExcess(indRunUp) ./ (uplandFrac(indRunUp));
        gridExcess(indRunUp) = 0;

        %runonWetland(indRunWet) = gridExcess(indRunWet) ./ (wetlandFrac(indRunWet) + streamFrac(indRunWet));

        %Distribute water evenly to all wetlands in this complex, proportional
        %to their fraction of the complex
        %These next lines could be done on initialization
        totalWetFrac = wetlandFrac + streamFrac;
        sumComplex = accumarray(wetComplex+1,totalWetFrac);
        fracComplex = totalWetFrac ./ sumComplex(wetComplex+1);

        %Get the total runon to each complex
        sumRunonComplex = accumarray(wetComplex+1,gridExcess);
        runonWetland = sumRunonComplex(wetComplex+1) .* fracComplex ./ totalWetFrac;
        runonWetland(isnan(runonWetland)) = 0;

        % Cheeck negative runon to not exceed storage
        indNegative = runonWetland < 0;
        runonWetland(indNegative) = max(-waterWetland(indNegative),runonWetland(indNegative));
        indNegative = runonUpland < 0;
        runonUpland(indNegative) = max(-waterDepression(indNegative),runonUpland(indNegative));

        %Now, add these to their respective storages
        waterWetland = waterWetland + runonWetland;
        waterDepression = waterDepression + runonUpland;
    end

    %Do some checks
    warning_LHM(runonUpland<=1,'Check internal runoff routing');
    warning_LHM(waterWetland<100,'Wetland water storage > 100m, check internal runoff routing');
    
    %Update the state variable for these quantities
    state = update_struct(state,'runon_internal_wetland',runonWetland,'runon_internal_upland',runonUpland,...
        'water_wetland',waterWetland,'water_depression',waterDepression);
    if structure.runoff_enabled
        %Update the data controller
        state = update_struct(state,'watershed_runoff',watershedRunoff,'watershed_evap_demand',watershedEvapDemand,...
            'runoff_external',externalExcess,'runoff_internal',internalExcess);
    end

    end