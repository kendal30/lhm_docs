function [soilWater,deepPerc,exfiltration,drainage] = free_drainage_model(...
    soilWater,satCap,kSat,kTheta,soilThick,zerosGrid,timestepLength)
%FREE_DRAINAGE_MODEL  Calculate free drainage in a bucket model.
%   output = free_drainage_model(input)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> free_drainage_model
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

%Calculate inter-layer drainage conductivity
kDrain = zerosGrid;
kDrain(:,1:end-1) = weighted_mean('harmonic',kTheta(:,1:end-1),kTheta(:,2:end),...
    soilThick(:,1:end-1),soilThick(:,2:end));
kDrain(:,end) = kTheta(:,end);

%Calculate free drainage
%drainage = min(kDrain * timestepLength, max(soilWater - fieldCap,0));
drainage = min(kDrain * timestepLength, soilWater);

%Remove free drainage from current layer
soilWater = soilWater - drainage;

%Add free drainage to lower levels
soilWater(:,2:end) = soilWater(:,2:end) + drainage(:,1:end-1);

%Check for excess saturation, if present, go through grid pushing water
%into deeper layers, as long as the infiltration does not exceed saturated
%conductivity in that layer
ind = any(soilWater > satCap,2);
exfiltration = zerosGrid(:,1);
if any(ind)
    excessDrainage = zerosGrid;
    %Redistribute the water, and calculate over-saturation exfiltration
    [excessDrainage(ind,:),exfiltration(ind),soilWater(ind,:)] = sat_excess_redistrib(...
        soilWater(ind,:),satCap(ind,:),kSat(ind,:),soilThick(ind,:),...
        drainage(ind,:),exfiltration(ind),zerosGrid(ind,:),timestepLength);
    drainage = drainage + excessDrainage;
end

%Drainage from the lowest layer becomes deep percolation
deepPerc = drainage(:,end);
end

function [excessDrainage,exfiltration,soilWater] = sat_excess_redistrib(...
    soilWater,satCap,kSat,soilThick,drainage,exfiltration,zerosGrid,timestepLength)
%Note: ind is 'true' if any cell in each column exceeds saturation

%Calculate inter-layer maximum drainage (under saturation), this could
%be calculated just once
maxDrain = zerosGrid;
maxDrain(:,1:end-1) = weighted_mean('harmonic',kSat(:,1:end-1),kSat(:,2:end),...
    soilThick(:,1:end-1),soilThick(:,2:end));
maxDrain(:,end) = kSat(:,end);

%This should be refined to use harmonic mean of inter-layer conductivities
%calculate available excess drainage capacity
remainCapacity = max(0,maxDrain*timestepLength - drainage);

%Loop through the soil layers from the top down
excessDrainage = zerosGrid;
numLay = size(soilWater,2);
for m = 1:numLay
    excessSat = max(0,soilWater(:,m) - satCap(:,m));
    ind = (excessSat > 0);

    %If any saturation excess exists
    if any(ind)
        %Drain at the capacity, limited by field capacity water
        excessDrainage(ind,m) = min(excessSat(ind),remainCapacity(ind,m));

        %Push any remaining excess out as exfiltration (this is currently
        %not quite right, because it should be able to move up layers)
        thisExfilt = max(0,excessSat(ind) - excessDrainage(ind,m));
        exfiltration(ind) = exfiltration(ind) + thisExfilt;

        %Now, update soil water
        soilWater(ind,m) = soilWater(ind,m) - excessDrainage(ind,m) - thisExfilt;

        %Add excess drainage to the layer below
        if m < numLay
            soilWater(ind,m+1) = soilWater(ind,m+1) + excessDrainage(ind,m);
        end
    end
end

% %loop through the soil layers from the bottom up, pushing excess water
% %upward
% excessdrainage = zerosgrid;
% lowercap = zerosgrid(:,1);
% numlay = size(soilwater,2);
% for m = numlay:-1:1
%     %check to see if this layer is saturated in excess
%     excesssat = max(0,soilwater(:,m) - satcap(:,m));
%     indsat = ind & (excesssat > 0);
%
%     %if saturation excess has occurred
%     exfiltration = zerosgrid(:,1);
%     if any(indsat)
%         %determine field capacity excess
%         excessfield = max(0,soilwater(indsat,m) - fieldcap(indsat,m));
%
%         %drain at the capacity, limited by field capacity water, unless
%         %exfiltration from below has already occurred or the lower layer is
%         %still saturated
%         if m < numlay
%             excessdrainage(indsat,m) = min(min(excessfield,remainCapacity(indsat,m)),lowercap(indsat));
%         else
%             excessdrainage(indsat,m) = min(excessfield,remainCapacity(indsat,m));
%         end
%
%         %push any remaining excess upward
%         exfiltration(indsat) = excesssat(indsat) - excessdrainage(indsat,m);
%
%         %now, update soil water
%         soilwater(indsat,m) = soilwater(indsat,m) - excessdrainage(indsat,m) - exfiltration(indsat);
%
%         %add exfiltration to the layer above (lower layer index),
%         if m > 1
%             soilwater(indsat,m-1) = soilwater(indsat,m-1) + exfiltration(indsat);
%         end
%     end
%
%     %capacity remaining for cells draining downward from above
%     if m > 1
%         lowercap(ind) = max(0,soilwater(ind,m) - satcap(ind,m));
%         lowercap(exfiltration > 0) = 0; %if exfiltration occurs, there is no capacity in this cell
%     end
% end
%
% %finally, add drainage to soilwater
% soilwater(ind,2:end) = soilwater(ind,2:end) + excessdrainage(ind,1:end-1);
end
