function [runoff,upRunon,wetRunon] = impervious_hydrology_module(availableWater,...
    impervUplandFrac,impervWetlandFrac,impervRunoffFrac,indWetland,indPervious)
%URBAN_RUNOFF  Calculates runoff from urban areas
%   [upRunon,wetRunon,runoff] = urban_runoff(availableWater,impervUplandFrac,
%       impervWetlandFrac,impervRunoffFrac,indWetland,indPervious)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> urban_runoff
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-11-12
% Copyright 2009 Michigan State University.

%Calculate the partitioning
runoff = availableWater .* impervRunoffFrac;
upRunon = availableWater .* impervUplandFrac;
wetRunon = availableWater .* impervWetlandFrac;

%Check a couple of things
test = ~indWetland;
upRunon(test) = upRunon(test) + wetRunon(test);
wetRunon(test) = 0;

test = ~indPervious;
runoff(test) = runoff(test) + upRunon(test);
upRunon(test) = 0;