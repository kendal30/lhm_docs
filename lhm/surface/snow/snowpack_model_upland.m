function state = snowpack_model_upland(state,structure,params)
%SNOWPACK_MODEL_UPLAND  Calculates snowpack accumulation, melting, and energy balance.
%   snowpack_model_upland()
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

[condLitter,condSoil,litterTemp,soilTemp,totalSnow] = deal_struct(state,...
    'therm_cond_litter','therm_cond_soil','temp_litter','temp_soil','total_throughfall_snow');
[litterFrac,litterThick] = deal_struct(params,'litter_fraction','litter_thickness');
[zerosGrid,indUpland,indSnow,indLitter,indSoil,...
    soilThick] = deal_struct(structure,...
    'zeros_grid','index_upland','index_snow_upland','index_litter','index_no_litter',...
    'thickness_soil');
    
%--------------------------------------------------------------------------
%Update to account for newly-fallen snow
%--------------------------------------------------------------------------
indSnow = (indSnow | (totalSnow > 0)) & (indUpland);


%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[subsCond,subsThick,subsTemp] = deal(zerosGrid);

if any(indSnow)
    %--------------------------------------------------------------------------
    %Calculate the substrate conductivity, this differs for upland and wetland
    %--------------------------------------------------------------------------
    %Take into account partial litter coverage
    ind = indLitter;
    soilFrac = 1 - litterFrac;
    condLitter(ind) = weighted_mean('arithmetic',condlitter(ind),condSoil(ind,1),...
        litterFrac(ind),soilFrac(ind));
    litterThick(ind) = weighted_mean('arithmetic',litterThick(ind),soilThick(ind,1),...
        litterFrac(ind),soilFrac(ind));
    litterTemp(ind) = weighted_mean('arithmetic',litterTemp(ind),soilTemp(ind,1),...
        litterFrac(ind),soilFrac(ind));
    
    %Calculate the substrate conductivity and thickness according to whether
    %the underlying cell is litter covered or bare soil
    subsCond = populate_array(subsCond,condLitter,condSoil(:,1),...
        indLitter,indSoil);
    subsThick = populate_array(subsThick,litterThick,soilThick(:,1),...
        indLitter,indSoil);
    subsTemp = populate_array(subsTemp,litterTemp,soilTemp(:,1),...
        indLitter,indSoil);
end

%--------------------------------------------------------------------------
%Run the snowpack model common core
%--------------------------------------------------------------------------
[surfAge,snowMelt,snowTemp,snowThick,snowWater,condSnow,snowDensity,qCondDown,...
    snowIceFrac,meltTemp,qStorage,qRadTransmit,qRadTransmitVis,qRadTransmitNir] = ...
    snowpack_model_core('upland',indSnow,subsCond,subsThick,subsTemp);

%--------------------------------------------------------------------------
%update the state variables
%--------------------------------------------------------------------------
state = update_struct(state,'snow_surf_age_upland',surfAge,'snow_melt_upland',snowMelt,...
    'snow_temp_upland',snowTemp,'snow_thickness_upland',snowThick,'water_snow_upland',snowWater,...
    'therm_cond_snow_upland',condSnow,'snow_density_upland',snowDensity,...
    'q_cond_snow_upland',qCondDown,'snow_frozen_fraction_upland',snowIceFrac,...
    'temp_snow_melt_upland',meltTemp,'energy_snow_upland',qStorage,'rad_transmit_snow_upland',qRadTransmit,...
    'rad_transmit_snow_vis_upland',qRadTransmitVis,'rad_transmit_snow_nir_upland',qRadTransmitNir); 
end