function state = snowpack_model_wetland(state,structure,params)
%SNOWPACK_MODEL_WETLAND  Calculates snowpack accumulation, melting, and energy balance.
%   snowpack_model_wetland()
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

[iceThick,iceTemp,totalSnow] = deal_struct(state,...
    'ice_pack_thickness','temp_ice_pack','total_throughfall_snow','therm_cond_soil');
[condIce] = deal_struct(params,'therm_cond_ice');
[indWater,indSnow,indIce,zerosGrid] = deal_struct(structure,...
    'index_wetland','index_snow_wetland','index_ice_pack','zeros_grid');

%--------------------------------------------------------------------------
%Update to account for newly-fallen snow
%--------------------------------------------------------------------------
indSnow = (indSnow | (totalSnow > 0)) & (indWater) & (indIce);

[subsCond,subsThick,subsTemp] = deal(zerosGrid);
if any(indSnow)
    %--------------------------------------------------------------------------
    %Calculate the substrate conductivity, this differs for upland and wetland
    %--------------------------------------------------------------------------
    %Calculate the conductive heat flux down to the ice pack    
    ind = indIce;
    subsCond(ind) = condIce;
    subsThick(ind) = iceThick(ind);
    subsTemp(ind) = iceTemp(ind);
end

%--------------------------------------------------------------------------
%Run the snowpack model common core
%--------------------------------------------------------------------------
[surfAge,snowMelt,snowTemp,snowThick,snowWater,condSnow,snowDensity,qCondDown,...
    snowIceFrac,meltTemp,qStorage,qRadTransmit,qRadTransmitVis,qRadTransmitNir] = ...
    snowpack_model_core('wetland',indSnow,subsCond,subsThick,subsTemp);
    
%--------------------------------------------------------------------------
%update the state variables
%--------------------------------------------------------------------------
state = update_struct(state,'snow_surf_age_wetland',surfAge,'snow_melt_wetland',snowMelt,...
    'snow_temp_wetland',snowTemp,'snow_thickness_wetland',snowThick,'water_snow_wetland',snowWater,...
    'therm_cond_snow_wetland',condSnow,'snow_density_wetland',snowDensity,...
    'q_cond_snow_wetland',qCondDown,'snow_frozen_fraction_wetland',snowIceFrac,...
    'temp_snow_melt_wetland',meltTemp,'energy_snow_wetland',qStorage,'rad_transmit_snow_wetland',qRadTransmit,...
    'rad_transmit_snow_vis_wetland',qRadTransmitVis,'rad_transmit_snow_nir_wetland',qRadTransmitNir); 
end