function [state,structure] = ice_model_wetland(state,structure,params)
%ICE_MODEL_WETLAND  Ice pack model.
%   ice_model_wetland()
%
%   This model is based heavily on Patterson and Hamblin, 1988
%
%   Descriptions of Input Variables:
%   none, all inputs taken from the data controller
%
%   Descriptions of Output Variables:
%   none, all outputs written to the data controller
%
%   Example(s):
%   non
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-21
% Copyright 2008 Michigan State University.

[zerosGrid,indIce] = deal_struct(structure,'zeros_grid','index_ice_pack');
%--------------------------------------------------------------------------
%Break if there are no cells with ice
%--------------------------------------------------------------------------
if ~any(indIce) %If there are no cells with ice, then break
    state = update_struct(state,'q_cond_ice_pack',zerosGrid,...
        'ice_pack_melt',zerosGrid,'water_ice_pack_ponding',zerosGrid,'temp_ice_pack',zerosGrid,...
        'ice_pack_thickness',zerosGrid,'water_ice_pack',zerosGrid,'water_snow_wetland',zerosGrid,...
        'snow_thickness_wetland',zerosGrid,'energy_ice_pack',zerosGrid,'rad_transmit_ice_pack',zerosGrid,...
        'ice_pack_precip_store',zerosGrid,'ice_pack_precip_release',zerosGrid);
    structure = update_struct(structure,'index_ice_pack_ponding',logical(zerosGrid));
    return
end

%--------------------------------------------------------------------------
%Load arrays
%--------------------------------------------------------------------------
[iceWater,totalRain,snowMelt,qPhaseTop,...
    pondingWater,iceTemp,interfTemp,snowMeltTemp,...
    accreteLake,radShort,radTransmitSnowVis,radTransmitSnowNir,...
    iceThick,qCondInterf,qCondSnow,sublimation,...
    snowWater,snowThick,condLake,iceWaterPrecip] = deal_struct(state,...
    'water_ice_pack','total_throughfall_rain','snow_melt_wetland','q_phase_wetland',...
    'water_ice_pack_ponding','temp_ice_pack','temp_interf_wetland','temp_snow_melt_wetland',...
    'ice_pack_bot_accrete_lake','rad_short_wetland_net','rad_transmit_snow_vis_wetland','rad_transmit_snow_nir_wetland',...
    'ice_pack_thickness','q_cond_interf_wetland','q_cond_snow_wetland','evap_ice_pack',...
    'water_snow_wetland','snow_thickness_wetland','therm_cond_wetland','ice_pack_precip_store');
[heatFusion,densityIce,densityWater,interfShortFrac,lightAttenVis,lightAttenNir,...
    condIce,cpIce,cpWater,iceMeltThreshold] = deal_struct(params,...
    'latent_heat_fusion','density_ice','density_water',...
    'ice_light_interf_absorb','ice_light_atten_coeff_vis','ice_light_atten_coeff_nir',...
    'therm_cond_ice','heat_cap_ice','heat_cap_water','ice_melt_threshold');
[timestepLength,lakeLayerThick,indSnow,...
    indPonding,indBare,indIce] = deal_struct(structure,...
    'timestep_length','thickness_wetland','index_snow_wetland',...
    'index_ice_pack_ponding','index_bare_ice','index_ice_pack');
    
%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[qCondPond,pondTemp,accreteTop,radNetVis,radNetNir,qNet,qCondUp,...
    qRadTransmitNir,qRadTransmitVis,qRadTransmit,qCondBotIce,qCondBotLake,...
    newMelt,surfMelt,maxMass,precipStoreLoss] = deal(zerosGrid);

%--------------------------------------------------------------------------
%Calculate storage of meteorological waters in the ice pack
%--------------------------------------------------------------------------
%Add incoming rain and snow melt to ice pack precip storage, subtract
%sublimation
iceWaterPrecip(indIce) = iceWaterPrecip(indIce) + totalRain(indIce) + snowMelt(indIce) - sublimation(indIce);

%--------------------------------------------------------------------------
%Calculate ice melting at the top interface, liquid precip accumulation,
%and ponding depth
%--------------------------------------------------------------------------
ind = (qPhaseTop > 0) & (indBare);
if any(ind)
    [qPhaseTop(ind),iceFrac] = phase_change(qPhaseTop(ind),zerosGrid(ind)+1,iceWater(ind),heatFusion,...
        densityIce,timestepLength,zerosGrid(ind));
    surfMelt(ind) = iceWater(ind) .* (1 - iceFrac);
end
%Note, qPhaseTop may still be greater than 0, in which case the ice
%has completely melted and the heat will go into warming the melt

%Add surfMelt, totalRain, and snowMelt to ponding water
pondingWater(indIce) = surfMelt(indIce) + totalRain(indIce) + snowMelt(indIce) + pondingWater(indIce);

%Calculate the net temperature of the ponded water, to conserve energy from
%snow melt
pondTemp(indIce) = snowMelt(indIce) .* snowMeltTemp(indIce) ./ pondingWater(indIce);

%--------------------------------------------------------------------------
%Calculate ponded water freezing at the top interface
%--------------------------------------------------------------------------
%First, calculate energy required to reduce pondTemp to 0
ind = (pondTemp > 0);
if any(ind)
    reqdEnergy = pondingWater(ind) * densityWater .* cpWater .* pondTemp(ind);
    qCondPond(ind) = reqdEnergy / timestepLength; %Positive out of the ice
end

%Now, calculate freezing due to qPhaseTop
ind = (qPhaseTop < 0) & (indPonding);
if any(ind)
    [qPhaseTop(ind),iceFrac] = phase_change(qPhaseTop(ind),zerosGrid(ind),pondingWater(ind),...
        heatFusion,densityIce,timestepLength,zerosGrid(ind));
    pondFreezing = iceFrac .* pondingWater(ind);
    pondingWater(ind) = pondingWater(ind) - pondFreezing;
    accreteTop(ind) = accreteTop(ind) + pondFreezing;
end
%Note: qPhaseTop may still be less than 0, in which case the ponded
%water has completely frozen and the rest of the heat will go into
%cooling the pack

%Finally, calculate freezing due to conduction to the ice
ind = (pondingWater > 0);
if any(ind)
    qCondTemp = conductive_heat_flux(iceTemp(ind),zerosGrid(ind),condIce,iceThick(ind));
    [qCondRemain,iceFrac] = phase_change(qCondTemp,zerosGrid(ind),pondingWater(ind),...
        heatFusion,densityIce,timestepLength,zerosGrid(ind));
    pondFreezing = iceFrac .* pondingWater(ind);
    pondingWater(ind) = pondingWater(ind) - pondFreezing;
    accreteTop(ind) = accreteTop(ind) + pondFreezing;
    %Subtract the difference between the total conductive potential and the
    %remainder to the existing qCondPond array, qCondPond is positive into
    %the ponds, and out of the ice
    qCondPond(ind) = qCondPond(ind) - (qCondTemp - qCondRemain);
end

%--------------------------------------------------------------------------
%Accrete water at the ice bottom, and calculate advection from this
%--------------------------------------------------------------------------
%First, add any ice that formed in the previous lake temperature timestep
accreteBot = accreteLake;

%Calculate heat fluxes at the bottom interface, as well as
%accretion/ablation from the imbalance
ind = indIce;

%Calculate the rate of conduction within the ice, the bottom interface is
%always at the freezing temperature of the water, assumed here to be 0
%degrees.  This will be positive away from the ice (it will always be a
%negative number, or 0).
qCondBotIce(ind) = conductive_heat_flux(iceTemp(ind),zerosGrid(ind),...
    condIce,iceThick(ind));

%Calculate the rate of conduction within the water, this will be positive
%away from the interface and into the water (it will then always be a
%negative number, as the interface is always colder than the water)
qCondBotLake(ind) = conductive_heat_flux(zerosGrid(ind),waterTemp(ind,1),...
    condLake,lakeLayerThick(ind,1));

%Calculate ice accretion/melting as the difference of the previous two
%conductive fluxes, see Patterson and Hamblin 1988.  This maintains an
%energy balance of the ice bottom interface.
qPhaseBot(ind) = qCondBotIce(ind) - qCondBotLake(ind); %If qPhaseBot is negative,
%then water needs to accrete onto the ice pack in order to maintain the
%energy balance, and vice versa

accreteBot(ind) = accreteBot(ind) - qPhaseBot(ind) .* timestepLength ./ ...
    (densityWater .* heatFusion);

%Addition of ice to the bottom at 0 degrees may advect heat into the ice
%pack, calculate this.  This wasn't taken into account at the interface
%because the interface temperature at the bottom is, by definition, the
%freezing temperature, so phase change is the only heat term in the
%balance.
qAdvectBot(ind) = precip_heat_flux(params,accreteBot/timestepLength,zerosGrid(ind) - 1e5, iceTemp(ind));

%Initialize iceMeltLake here, save it out as ice_pack_melt later
iceMeltLake = accreteBot;
iceMeltLake(iceMeltLake < 0) = 0; %this is a positive quantity only, used to advect heat to the lake

%--------------------------------------------------------------------------
%Update ice thickness and total water content
%--------------------------------------------------------------------------
iceWater(ind) = iceWater(ind) + accreteTop(ind) + accreteBot(ind) - sublimation(ind);
iceThick(ind) = iceWater(ind) / (densityIce / densityWater);

%--------------------------------------------------------------------------
%Calculate Radiative Energy Fluxes
%--------------------------------------------------------------------------
%Populate radShort array
radShort(indBare) = radShort(indBare) .* (1 - interfShortFrac); %subtract the part already absorbed by the interface
radNetVis = populate_array(radNetVis,radShort/2,radTransmitSnowVis,indBare,indSnow);
radNetNir = populate_array(radNetNir,radShort/2,radTransmitSnowNir,indBare,indSnow);

%Calculate absorbtion of shortwave radiation by the ice pack
%and radiation transmitted to the water beneath
qRadTransmitVis(ind) = radNetVis .* exp(-lightAttenVis .* iceThick(ind));
qRadTransmitNir(ind) = radNetNir .* exp(-lightAttenNir .* iceThick(ind));
qRadTransmit(ind) = qRadTransmitVis(ind) + qRadTransmitNir(ind);
qRadAbsorb(ind) = radNetVis(ind) + radNetNir(ind) - qRadTransmit(ind);

%--------------------------------------------------------------------------
%Calculate Conductive Flux Upward
%--------------------------------------------------------------------------
%Each of qCondInterf and qCondSnow are initially positive into the ice
%pack, so the sign needs to be reversed
qCondUp = -populate_array(qCondUp,qCondInterf,qCondSnow,indBare,indSnow);

%--------------------------------------------------------------------------
%Calculate Energy Balance
%--------------------------------------------------------------------------
qNet(ind) = qRadAbsorb(ind) - qCondUp(ind) - qCondBotIce(ind) - qCondPond(ind) + ...
    qAdvectBot(ind) + qPhaseTop(ind); %What about sublimation here, in non snow-covered areas?
qStorage(ind) = qNet(ind);  %will subtract qMelt (from totally melted ice pack) later

%--------------------------------------------------------------------------
%Calculate temperature change prior to any phase change, moving towards T=0
%--------------------------------------------------------------------------
ind = (iceTemp < 0) & (qNet > 0);
if any(ind)
    [qNet(ind),iceTemp(ind)] = temp_change_pre_phase(qNet(ind),iceTemp(ind),...
        cpIce,densityIce,iceThick(ind),timestepLength);
end

%--------------------------------------------------------------------------
%Calculate phase change, all melt occurs at the top
%--------------------------------------------------------------------------
ind = (qNet > 0);
if any(ind)
    [qNet(ind),iceFrac] = phase_change(qNet(ind),zerosGrid(ind)+1,iceWater(ind),heatFusion,...
        densityIce,timestepLength,zerosGrid(ind));
    newMelt(ind) = (1-iceFrac) .* iceWater(ind);
    pondingWater(ind) = pondingWater(ind) + newMelt;
    iceThick(ind) = iceThick(ind) - newMelt ./ (densityIce / densityWater);
end
%Dont subtract from iceWater yet, because the next step will account for
%warming the melt in the event of complete ice pack melt

%--------------------------------------------------------------------------
%Finally, calculate remaining temperature change
%--------------------------------------------------------------------------
ind = indIce;
iceTemp(ind) = temp_change_post_phase(iceTemp(ind),qNet(ind),cpIce,...
    densityIce,iceWater(ind),timestepLength);
iceTemp(iceTemp == 0) = -1e-5; %make sure that, if frozen, temperature is always very slightly less than 0
%so that precip heat flux will work properly

%Now, subtract the top melt from iceWater
iceWater = iceWater - newMelt;

%--------------------------------------------------------------------------
%Determine if snowpack mass overwhelms ice bouyancy, add to ice ponding
%--------------------------------------------------------------------------
%Calculate maximum ponded and snowpack mass, according to Patterson and
%Hamblin
maxMass(ind) = iceThick(ind) .* (densityWater - densityIce);

%Reduce perched mass by fraction in excess of bouyant capacity
perchedWater = snowWater(ind) + pondingWater(ind);
ind = (perchedWater > maxMass);
if any(ind)
    excess = perchedWater(ind) - maxMass(ind);
    
    %First, remove water from ponding
    pondLoss = min(pondingWater(ind),excess);
    pondingWater(ind) = pondingWater(ind) - pondLoss;
    snowLoss = excess - pondLoss;
    
    %Then, remove water from snowpack
    snowThick(ind) = snowThick(ind) .* (snowLoss ./ snowWater(ind));
    snowWater(ind) = snoWater(ind) - snowLoss;
    
    %Add loss terms to iceMeltLake
    iceMeltLake(ind) = iceMeltLake(ind) + pondLoss + snowLoss;
end
%This currently leaks energy because it assumes the snow pack is converted
%to water without a change in energy. The difference should be calculated and tracked.

%--------------------------------------------------------------------------
%Determine if ice pack is thinner than the melt threshold
%--------------------------------------------------------------------------
ind = (iceWater < iceMeltThreshold);
iceMeltLake(ind) = iceMeltLake(ind) + iceWater(ind) + pondingWater(ind);
pondingWater(ind) = 0;
iceWater(ind) = 0;
iceWaterPrecip(ind) = 0;
%Right now, this leaks energy, because I've assumed that the melt water and
%ponding water are at T=0.  In reality, they may be slightly warmer or
%colder, as mentioned above also.  I'm assuming that the error is small.
%But above, I calculate the temperature of the melt, so this would not be
%difficult to modify

%--------------------------------------------------------------------------
%Calculate change in meteorological water storage in the ice pack
%--------------------------------------------------------------------------    
ind = (iceWaterPrecip > 0);
precipStoreLoss(ind) = min(iceWaterPrecip(ind),iceMeltLake(ind));
iceWaterPrecip(ind) = iceWaterPrecip(ind) - precipStoreLoss(ind);

%--------------------------------------------------------------------------
%Calculate and write outputs
%--------------------------------------------------------------------------
icePonding = pondingWater > 0; %this needs to be written out for the interface temp. module

state = update_struct(state,'q_cond_ice_pack',qCondBotLake,...
    'ice_pack_melt',iceMeltLake,'water_ice_pack_ponding',pondingWater,'temp_ice_pack',iceTemp,...
    'ice_pack_thickness',iceThick,'water_ice_pack',iceWater,'water_snow_wetland',snowWater,...
    'snow_thickness_wetland',snowThick,'energy_ice_pack',qStorage,'rad_transmit_ice_pack',qRadTransmit,...
    'ice_pack_precip_store',iceWaterPrecip,'ice_pack_precip_release',precipStoreLoss);
structure = update_struct(structure,'index_ice_pack_ponding',icePonding);
