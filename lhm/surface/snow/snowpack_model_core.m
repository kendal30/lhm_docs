function [surfAge,snowMelt,snowTemp,snowThick,snowWater,condSnow,snowDensity,qCondDown,...
        snowIceFrac,meltTemp,qStorage,qRadTransmit,qRadTransmitVis,qRadTransmitNir] = ...
        snowpack_model_core(domain,indSnow,subsCond,subsThick,subsTemp,state,structure,params)
%SNOWPACK_MODEL_CORE  The core of the snowpack model, common to all areas.
%   [surfAge,snowMelt,snowTemp,snowThick,snowWater,condSnow,snowDensity,qCondDown,...
%        snowIceFrac,meltTemp,qStorage,qRadTransmit,qRadTransmitVis,qRadTransmitNir] = ...
%        snowpack_model_core(domain,indSnow,subsCond,subsThick,subsTemp)
%
%   Descriptions of Input Variables:
%   domain: either 'upland' or 'wetland', determines which state variables are read in
%   indSnow: the indeces that the snow model will run on
%   subsCond: thermal conductance of the substrate (W/m/K)
%   subsThick: thickness of the thermally-active portion of the substrate, (m)
%   subsTemp: temperature of the substrate (C)
%
%   Descriptions of Output Variables:
%   Too many to describe here
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-20
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Break if there are no cells with snow
%--------------------------------------------------------------------------
if ~any(indSnow) %If there are no cells with snow, then break
    %No other modules should rely on this, but just to make sure that
    %values are appropriately written for visualization purposes
    %CHECK THESE, MAY NEED TO HAVE SOME NON-ZERO VALUES
    [zerosGrid] = deal_struct(structure,'zeros_grid');
    [surfAge,snowMelt,snowTemp,snowThick,snowWater,condSnow,snowDensity,qCondDown,...
        snowIceFrac,meltTemp,qStorage,qRadTransmit,qRadTransmitVis,qRadTransmitNir] = ...
        deal(zerosGrid);
    return
end

%--------------------------------------------------------------------------
%Load variables from the data controller
%--------------------------------------------------------------------------
[totalRain,totalSnow,totalPrecip,densitySnowfall,snowfallTemp,...
    snowThick,interfTemp,sublimation,qCondInterf,...
    qMeltInterf,snowDensity,snowWater,snowIceFrac,...
    surfAge,qRadShort,snowTemp] = deal_struct(state,...
    'total_throughfall_rain','total_throughfall_snow','total_throughfall','snow_new_density','temp_snowfall',...
    ['snow_thickness_',domain],['temp_interf_',domain],['evap_snow_',domain],['q_cond_interf_',domain],...
    ['q_phase_',domain],['snow_density_',domain],['water_snow_',domain],['snow_frozen_fraction_',domain],...
    ['snow_surf_age_',domain],['rad_short_',domain,'_net'],['temp_snow_',domain]);
[densityWater,condIce,condWater,condAir,cpIce,cpWater,heatFusion,...
    lightAttenVis,lightAttenNir,interfShortFrac,...
    snowCapRet,snowMaxRet,snowMeltThreshold] = ...
    deal_struct(params,...
    'density_water','therm_cond_ice','therm_cond_water','therm_cond_air','heat_cap_ice','heat_cap_water','latent_heat_fusion',...
    'snow_light_atten_coeff_vis','snow_light_atten_coeff_nir','snow_light_interf_absorb',...
    'snow_capillary_retention','snow_max_retention','snow_melt_threshold');
[zerosGrid,timestepLength] = deal_struct(structure,'zeros_grid','timestep_length');

%--------------------------------------------------------------------------
%Determine additional indeces
%--------------------------------------------------------------------------
indPrecip = (totalPrecip > 0);

%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[snowMelt,snowLiquidFrac,qRadTransmit,qRadTransmitNir,qRadTransmitVis,qRadAbsorb,...
    qAdvect,qCondUp,qCondDown,qNet,qStorage,cpSnow,meltTemp,surfMelt] = deal(zerosGrid);
snowPrevTemp = snowTemp;
snowPrevWater = snowWater;

%--------------------------------------------------------------------------
%Remove/add submlimation/frost to the snowpack
%--------------------------------------------------------------------------
%Ignore the difference between solid/liquid here, just because the error is
%very small
ind = (sublimation ~= 0);
snowWater(ind) = snowWater(ind) - sublimation(ind);

%--------------------------------------------------------------------------
%Add precip to the snow pack, update its density, thickness, porosity, etc.
%--------------------------------------------------------------------------
%Update snowpack density, thickness, and total water content if any input
%precip
ind = (indPrecip) & (indSnow);
if any(ind) 
    %Update the snowpack thickness
    newSnowThick = totalSnow(ind) ./ (densitySnowfall(ind) ./ densityWater(ind));
    snowThick(ind) = snowThick(ind) + newSnowThick;
    %Update the total snow water content
    snowWater(ind) = snowWater(ind) + totalPrecip(ind);
    %Update the net snowpack density
    snowDensity(ind) = densityWater .* snowWater(ind) ./ snowThick(ind);
    %Update the frozen and liquid fractions
    newIceFrac = totalSnow(ind) ./ totalPrecip(ind);
    snowIceFrac(ind) = weighted_mean('arithmetic',snowIceFrac(ind),newIceFrac(ind),...
        snoWater(ind),totalPrecip(ind));
end

%--------------------------------------------------------------------------
%Remove water from already-calculated surface melting
%--------------------------------------------------------------------------
%No temperature change is necessary, because the energy balance already
%reflects the interface temperature being at 0
indMelt = (qMeltInterf > 0) & (indSnow);
if any(indMelt)
    ind = indMelt;
    %Calculate total amount of ice in the snowpack
    snowIce = snowIceFrac(ind) .* snowWater(ind);
    
    %Calculate the melting, note that qMeltInterf may still be positive in
    %the event of complete melting, if so, it will be added to the energy
    %balance later
    [qMeltInterf(ind),tempIceFrac] = phase_change(qMeltInterf(ind),zerosGrid(ind)+1,snowIce,...
        heatFusion,densityIce,timestepLength,zerosGrid(ind));
    
    %Calculate the amount of new water
    surfMelt(ind) = snowIce .* (1 - tempIceFrac);
    
    %Add this to the snowpack, and update the snowIceFrac
    snowIceFrac(ind) = snowIce .* tempIceFrac ./ snowWater(ind);
    
    %Calculate the new thickness, which is simply the old thickness times
    %tempIceFrac, along with the new density
    snowThick(ind) = snowThick(ind) .* tempIceFrac;
    snowDensity(ind) = densityWater .* snowWater(ind) ./ snowThick(ind);
end

%--------------------------------------------------------------------------
%Calculate the snow surface age
%--------------------------------------------------------------------------
%Assign indSnow to the index for much of the remaining calculations
ind = indSnow;
surfAge(ind) = snow_surface_age(surfAge(ind),newSnowThick(ind),interfTemp(ind),timestepLength);

%--------------------------------------------------------------------------
%Calculate the advected energy from precipitation and surface melting
%--------------------------------------------------------------------------
%From precipitation
rateRain = totalRain(ind) / timestepLength;
rateSnow = totalSnow(ind) / timestepLength;
qAdvect(ind) = precip_heat_flux(params,rateRain,interfTemp(ind),snowTemp(ind)) + ...
    precip_heat_flux(params,rateSnow,snowfallTemp(ind),snowTemp(ind)); %positive inward

%From surface melt
rateMelt = surfMelt(indMelt) / timestepLength;
qAdvect(indMelt) = precip_heat_flux(params,rateMelt,zerosGrid(ind),snowTemp(ind)); %positive inward

%--------------------------------------------------------------------------
%Calculate the conductive fluxes
%--------------------------------------------------------------------------
qCondUp(ind) = -qCondInterf(ind); %positive outward

%calculate the conductance of the snowpack
weightIce = snowWater(ind) .* snowIceFrac(ind);
weightWater = snowWater(ind) - weightIce;
weightAir = snowThick(ind) - weightIce - weightWater;
condSnow(ind)=weighted_mean('geometric',condIce,condWater,condAir,...
    weightIce,weightWater,weightAir);

%Calculate the combined thermal conductivity of the snow-substrate
%interface
condSnowSubs = weighted_mean('harmonic',condSnow(ind),subsCond,...
    snowThick(ind),subsThick);
condThick = subsThick + snowThick(ind);
%Calculate the conductive flux downward to the substrate
qCondDown(ind) = conductive_heat_flux(snowPrevTemp(ind),subsTemp,...
	condSnowSubs,condThick);

%--------------------------------------------------------------------------
%Calculate the attenuation and transmission of light by/through the snowpack
%--------------------------------------------------------------------------
qRadShort(ind) = qRadShort(ind) * (1 - interfShortFrac); %this fraction was already absorbed by the interface
%assume NIR and VIS the same for interface absorbtion
%partition energy half into NIR and VIS portion of spectrum
qRadTransmitVis(ind) = qRadShort(ind) / 2 .* exp(-lightAttenVis .* snowThick(ind));
qRadTransmitNir(ind) = qRadShort(ind) / 2 .* exp(-lightAttenNir .* snowThick(ind));
qRadTransmit(ind) = qRadTransmitVis(ind) + qRadTransmitNir(ind);
qRadAbsorb(ind) = qRadShort(ind) - qRadTransmit(ind);

%--------------------------------------------------------------------------
%First, calculate the energy budget of the snowpack, units are W/m2
%--------------------------------------------------------------------------
qNet(ind) = qRadAbsorb(ind) - qCondUp(ind) - qCondDown(ind) + qMeltInterf(ind) + qAdvect(ind);
qStorage(ind) = qNet(ind); %will remove qMelt from this later

%--------------------------------------------------------------------------
%Calculate the original heat capacity of the snowpack
%--------------------------------------------------------------------------
cpSnow(ind) = weighted_mean('arithmetic',cpIce,cpWater,...
    weightIce*densityIce,weightWater*densityWater);

%--------------------------------------------------------------------------
%Calculate delta T prior to any phase change, moving toward T = 0
%--------------------------------------------------------------------------
ind = (snowTemp < 0) & (qNet > 0);
if any(ind)
    [qNet(ind),snowTemp(ind)] = temp_change_pre_phase(qNet(ind),snowTemp(ind),...
        cpSnow(ind),snowDensity(ind),snowThick(ind),timestepLength);
end

%--------------------------------------------------------------------------
%Handle phase change prior to any remaining delta T
%--------------------------------------------------------------------------
ind = (qNet > 0);
if any(ind)
    [qNet(ind),snowIceFrac(ind)] = phase_change(qNet(ind),snowIceFrac(ind),snowWater(ind),heatFusion,...
        densityIce,timestepLength,zerosGrid(ind));
    %Now calculate updated multiphase heat capacity and density
    snowLiquidFrac(ind) = 1 - snowIceFrac(ind);
    weightIce(ind) = snowWater(ind) .* snowIceFrac(ind);
    weightWater(ind) = snowWater(ind) - weightIce(ind);
    cpSnow(ind) = weighted_mean('arithmetic',cpIce,cpWater,...
        weightIce(ind)*densityIce,weightWater(ind)*densityWater);
end

%--------------------------------------------------------------------------
%Finally, calculate remaining temperature change
%--------------------------------------------------------------------------
ind = indSnow;
snowTemp(ind) = temp_change_post_phase(snowTemp(ind),qNet(ind),cpSnow(ind),...
    snowDensity(ind),snowThick(ind),timestepLength);

%--------------------------------------------------------------------------
%Determine snow melt, pack thickness, update snowpack variables
%--------------------------------------------------------------------------
%Determine if entire snowpack has melted
ind = (snowLiquidFrac == 1);
snowMelt(ind) = snowWater(ind);
snowWater(ind) = 0;
snowLiquidFrac(ind) = 0;

%If not, check to see how much liquid will become snowmelt, based on liquid
%holding capacity and ksat
ind = (snowLiquidFrac > 0);
%Calculate the snow water holding capacity, from Jin et al. 1999
snowCap = water_holding_capacity(snowIceFrac(ind),snowDensity(ind),snowCapRet,snowMaxRet,zerosGrid(ind));
%Calculate snow melt
snowMelt(ind) = max(0,(snowLiquidFrac(ind) - snowCap) .* snowWater(ind));

%Update the ice fraction and total snow water content
newSnowWater = snowWater(ind) - snowMelt(ind);
snowIceFrac(ind) = snowIceFrac(ind) .* snowWater(ind) ./ newSnowWater;
snowWater(ind) = newSnowWater;

%Determine if snow melt will leave too little water left in snowpack
%Be careful here, this leaks energy right now
ind = (snowWater(ind) < snowMeltThreshold);
snowMelt(ind) = snowMelt(ind) + snowWater(ind);
snowWater(ind) = 0;

%Calculate snow melt temperature and qMelt
ind = (snowMelt > 0);
if any(ind)
    meltTemp(ind) = snowTemp(ind);
    qMelt(ind) = meltTemp(ind) ./ timestepLength .* cpWater .* densityWater .* snowMelt(ind);
    qStorage(ind) = qStorage(ind) - qMelt(ind);
end

%Update the snowpack thickness and density
ind = (snowWater > 0);
snowThick(ind) = snowThick(ind) - snowMelt(ind) ./ snowDensity(ind);
snowDensity(ind) = densityWater .* snowWater(ind) ./ snowThick(ind);

%Update the snowpack conductance to take into account pack melting
weightIce = snowWater(ind) .* snowIceFrac(ind);
weightWater = snowWater(ind) - weightIce;
weightAir = snowThick(ind) - weightIce - weightWater;
condSnow(ind)=weighted_mean('geometric',condIce,condWater,condAir,...
    weightIce,weightWater,weightAir);

%Reset the snow temperature, surface age, density, and ice fraction for
%melted cells
snowTemp(~ind) = 0;
surfAge(~ind) = 0;
snowDensity(~ind) = 0;
snowIceFrac(~ind) = 0;

%--------------------------------------------------------------------------
%Error checks
%--------------------------------------------------------------------------
if assert_LHM
    assert_LHM(all((snowMelt + snowWater) == (snowPrevWater + totalPrecip - sublimation)),...
        ['The ',domain,' snowpack water balance has not been achieved']);
    assert_LHM(all(~isnan(snowTemp)),['An NaN appeared in the ',domain,' snow temperature']);
end

end

function [snowCap] = water_holding_capacity(iceFrac,snowDens,capRet,maxRet,zerosGrid)
%This is specified in Jin et al 1999
refDens = 0.200; 
%Initialize the water holding capacity
snowCap = capRet + zerosGrid;
%Calculate the partial density of ice in the snow pack
icePartDens = iceFrac .* snowDens;
%If the ice partial density is less than the reference density, interpolate
%the value of the snow water holding capacity
ind = (icePartDens < refDens);
snowCap(ind) = snowCap(ind) + (maxRet - capRet) * (refDens - icePartDens(ind)) / refDens;
end