function state = upland_controller(state,structure,params,modules)
%UPLAND_CONTROLLER  Controls upland temperature and moisture codes.
%   upland_controller()
%
%   Descriptions of Input Variables:
%   none, all inputs come from data controller
%
%   Descriptions of Output Variables:
%   nont, all inputs come from data controller
%
%   Example(s):
%   
%
%   See also: wetland_controller canopy_controller 

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-04-02
% Copyright 2008 Michigan State University.

%Save water storage as previous timestep water storage;
state = update_struct(state,'prev_water_soil',state.water_soil,...
    'prev_water_snow_upland',state.water_snow_upland,'prev_water_litter',state.water_litter,...
    'prev_water_depression',state.water_depression);
[water_litter,temp_interface_upland,temp_litter,water_depression,water_soil,temp_soil,...
    snow,sediment_yield] = deal_struct(modules,...
    'water_litter','temp_interface_upland','temp_litter','water_depression',...
    'water_root_zone','temp_soil','snow_upland','sediment_yield');

%Run the interface temperature code
state = temp_interface_upland(state,structure,params);

%Run the snowpack model
state = snow(state,structure,params);

%Run the litter hydrology module
state = water_litter(state,structure,params);

%Run the litter temperature module
state = temp_litter(state,structure,params);

%Run the soil water code
state = water_soil(state,structure,params);

%Run the soil temperature code
state = temp_soil(state,structure,params);

%Run the depression storage code
state = water_depression(state,structure,params);

%Run the sediment yield module
state = sediment_yield(state,structure,params);

end
