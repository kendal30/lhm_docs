function varargout = aerodynamic_resistance(structure,params,type,varargin)
%AERODYNAMIC_RESISTANCE  Calculates aerodynamic resistance over a surface.
%   varargout = aerodynamic_resistance(structure,params,type,varargin)
%
%   This function calculates the aerodynamic resistance according to a
%   semi-empirical instability correction from Viney, 2001.  This can be
%   run just fine on a partial grid array as the only inputs it fetches
%   from the data controller are constants.
%
%   Descriptions of Input Variables:
%   type: string specifying 'neutral','unstable','precalc'.  'neutral'
%       indicates that no stability-correction will be used.  'unstable' and
%       'precalc' both use stability correction, but the difference is that
%       with 'unstable' the entire calculation will be made, in which case
%       varargin contains all four input variables listed below.  'precalc'
%       indicates that a previously-output structure of all of the non
%       temperature-dependent parts of the calculation will be used, and only
%       the final stability correction should be calculated.  This is done to
%       save time in iterative loops for the surface temperature.
%   varargin: contains either 2, 3 or 4 arguments
%       FOR 'neutral' or 'unstable'
%       - windspeed: windspeed in m/s
%       - canopyHeight: height of the canopy in m
%       OR, for 'precalc'
%       - precalc: structure previously output from this function when type
%           was specified as 'unstable'
%       PLUS , for 'unstable' and 'precalc'
%       - airTemp: air temperature at the measurement height in C
%       - surfTemp: surface temperature in C
%
%   Descriptions of Output Variables:
%   ram: aerodynamic resistance to momentum transport in s/m
%   rahw: aerodynamic resistance to heat and vapor transport in s/m
%   precalc: structured array containing four variables used in this
%       function for later time-savings
%
%   Example(s):
%   [raGuess,precalc] = aerodynamic_resistance('unstable',windspeed,canopyHeight,airTemp,surfTempGuess);
%   [raGuess] = aerodynamic_resistance('precalc',precalc,airTemp,surfTempGuess);
%   [raNeutral] = aerodynamic_resistance('neutral',windspeed,canopyHeight);
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-04-01
% Copyright 2008 Michigan State University.

[heightWindMeas] = deal_struct(structure,'wind_meas_height');
[windOffset,windAlpha,vonKarman,gravConst] = deal_struct(params,...
    'wind_offset','wind_alpha','von_karman','grav_constant');

type = lower(type);
switch type
    case 'neutral'
        if nargin == 5
            [windspeed,canopyHeight] = deal(varargin{:});
        else
            error_LHM('windspeed and canopy height must be input in ''neutral'' mode');
        end
    case 'unstable'
        if nargin == 7
            [windspeed,canopyHeight,airTemp,surfTemp] = deal(varargin{:});
        else
            error_LHM('All four inputs must be specified in ''unstable'' mode, see help');
        end
    case 'precalc'
        if nargin == 6 && isstruct(varargin{1})
            [precalc,airTemp,surfTemp] = deal(varargin{:});
            raNeutralMomentum = precalc.raNeutralMomentum;
            raNeutralHeatVapor = precalc.raNeutralHeatVapor;
            a = precalc.a;
            b = precalc.b;
            c = precalc.c;
        else
            error_LHM('Unrecognized combination of inputs for ''precalc'' mode, see help');
        end
end

if strcmp(type,'neutral') || strcmp(type,'unstable')
    %Adjust the wind measurement height to windOffset above the canopy
    %height
    heightWindAdjusted = max(heightWindMeas,windOffset * canopyHeight);
    zeroDisplacement = 2/3 * canopyHeight; %zero_plane displacement
    %Set minimum values of inputs to prevent errors
    windspeed(windspeed == 0) = 0.01;
    canopyHeight(canopyHeight == 0) = 0.01;
    %Adjust the windspeed according to the wind profile power law, assuming
    %neutral conditions for the alpha parameter
    windAdjusted = windspeed .* (heightWindAdjusted ./ heightWindMeas) .^ windAlpha;
    %Calculate the length for momentum and heat/vapor transport
    lengthMomentum = 0.123 * canopyHeight;
    lengthHeatVapor = 0.1 * lengthMomentum;
    %Calculate the reference height
    referenceHeight = heightWindAdjusted - zeroDisplacement;


    %Calculate the neutral aerodynamic resistance term
    %Derived from http://www.fao.org/docrep/X0490E/x0490e06.htm#penman%20monteith%20equation
    precalc1 = log(referenceHeight ./ lengthMomentum);
    precalc2 = (vonKarman^2 * windAdjusted);
    raNeutralMomentum = precalc1.^2 ./ precalc2;
    raNeutralHeatVapor = precalc1 .* log((referenceHeight)./lengthHeatVapor) ./ precalc2;

    varargout{1} = raNeutralMomentum;
    varargout{2} = raNeutralHeatVapor;
end

%Calculate the aerodynamic resistance using the Viney (1991) semi-empirical
%formulation

%From Viney, N.R. (1991), An empirical expression for aerodynamic
%resistance in the unstable boundary layer, Boundary-Layer Meteorology, 56,
%pp. 381-393.
if strcmp(type,'unstable')
    %Calculate the empirical instability correction terms
    a = 1.0591 - 0.0552 * log(1.72 + (4.03 - precalc1).^2);
    b = 1.9117 - 0.2237 * log(1.86 + (2.12 - precalc1).^2);
    c = 0.8437 - 0.1243 * log(3.49 + (2.79 - precalc1).^2);
    if (nargout ==  3)
        varargout{3} = struct('raNeutralMomentum',raNeutralMomentum,...
            'raNeutralHeatVapor',raNeutralHeatVapor,'a',a,'b',b,'c',c);
    end
end
if strcmp(type,'precalc') || strcmp(type,'unstable')
    %Calculate the bulk Richardson number
    airTemp = unit_conversions(airTemp,'C','K');
    surfTemp = unit_conversions(surfTemp,'C','K');
    richardsonBulk = (gravConst * (airTemp - surfTemp) .* ...
        (referenceHeight)) ./ (0.5 * (airTemp + surfTemp) .* windAdjusted.^2); %from Hydrology Handbook

    test = (richardsonBulk <= 0);
    precalc1 = (a(test) + b(test) .* (-richardsonBulk(test)).^c(test));

    %First, start with stable conditions
    ram = raNeutralMomentum;
    rahw = raNeutralHeatVapor;

    %Now, apply the instability correction
    ram(test) = ram(test) ./ precalc1;
    rahw(test) = rahw(test) ./ precalc1;

    varargout{1} = ram;
    varargout{2} = rahw;
end
end
