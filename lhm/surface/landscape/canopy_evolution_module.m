function [state,structure,params,buffers] = canopy_evolution_module(state,structure,params,buffers,date)
% CANOPY_EVOLUTION_MODULE  Controls canopy evolution and change
%   output = canopy_evolution_module(input)
%
%   Descriptions of Input Variables:
%   date: MATLAB serial datenumber of the current timestep
%
%   Descriptions of Output Variables:
%   none, all are written to the data controller
%
%   Example(s):
%   >> canopy_evolution_module
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-08
% Copyright 2008 Michigan State University.


% Call read LU to see if the current land use map is up to date, return a
% logical true false value to indicate if a new value was read in
[structure,buffers,readLU] = read_landuse(structure,buffers,date);
if structure.runoff_enabled
    [structure,buffers,readFlowtimes] = read_flowtimes(structure,buffers,date);
else
    readFlowtimes = false;
end
[structure,buffers,readImperv] = read_impervious(structure,buffers,date);
if structure.irrigation_enabled
    [structure,buffers,readIrrigTech] = read_irrigation(structure,buffers,date);
else
    [readIrrigTech] = deal(false);
end

% Update the irrigation present array
% This comes before LU in order to be used in setting depression capacity
if readIrrigTech
    [mapTable] = deal_struct(params,'map_table_irrig');
    [irrigation,trimStruct,soilThick,zerosGrid] = deal_struct(structure,'irrigation','trim_struct',...
      'thickness_soil','zeros_grid');
	[irrigSchedType,irrigYield,irrigFieldSize] = deal_struct(params,...
		'irrigation_scheduling_type','irrigation_system_yield','irrigation_field_size');

    % From the mapTable
	sprinklerHeights = [mapTable(:).APPHEIGHT]; % heights in meters for high impact spray, mesa, lpic, lesa, lepa, subsurface drip, flood, ditch/furrow
	sprayWidths = [mapTable(:).SPRAYWIDTH]; % spray pattern widths in meters
    sprayWidths = sprayWidths + 40; % this is to force the application rate down
    % to account for the fact that if there is runoff, it will often just
    % re-infiltrate elsewhere down that row

    % For now, this module does not allow a cell to have partial irrigation,
    % so we will use a clumping algorithm that preserves area, as well as
    % the spatial distribution of irriation, but reduces the number of
    % irrigated cells
    origWeight = sum(irrigation.weightsArray,2);
    clumpWeight = irrigation_clumping_algorithm(extend_domain(trimStruct,origWeight)); % needs to run on the actual grid
    irrigation.present = logical(trim_domain(trimStruct,clumpWeight)); % back to LHM internal array shape

    % Create a new normalized weights set, multiply by the presence array
    irrigation.weightsPresent = irrigation.weightsArray ./ repmat(origWeight,[1,length(irrigation.weightNames)]);
    irrigation.weightsPresent = irrigation.weightsPresent .* repmat(irrigation.present,[1,length(irrigation.weightNames)]);

    % Average sprinkler heights and spray widths
    irrigation.height = sum(repmat(sprinklerHeights,[size(zerosGrid,1),1]).*irrigation.weightsPresent,2);
    irrigation.sprayWidth = sum(repmat(sprayWidths,[size(zerosGrid,1),1]).*irrigation.weightsPresent,2);

    % Update for subsurface irrigation
    irrigation.subsurface = irrigation.height < 0;

    % Update for flood irrigation. Assume that if the cell is majority flood-irrigated
    % then it is completely flood
    irrigation.flood = irrigation.weightsPresent(:,8) > 0.5;
    irrigation.height(irrigation.flood) = 0; % update the height of application

    % Compute these for convenience
    numSoilLay = size(soilThick,2);
    indSubSoil = repmat(irrigation.subsurface,[1,numSoilLay]);

    % Compute the depth of irrigation relative to the depth of each soil layer bottom
    irrigHeightSoil = repmat(-irrigation.height,[1,numSoilLay]);
    soilLayBot = cumsum(soilThick,2);
    testIrrigAboveLayBot = irrigHeightSoil < soilLayBot;

    % Determine the first layer with a depth off irrigation less than the layer bottom
    irrigation.soilIrrigLay = [testIrrigAboveLayBot(:,1)>0,diff(testIrrigAboveLayBot,[],2)]; % returns a logical array with soil dimensions, and a 1 for the irrigation layer

    % If it's not subsurface irrigation, overwrite the value computed
    irrigation.soilIrrigLay(~indSubSoil) = false;

	% Create the schedule type array, will be read in eventually
	% For now, just assign a constant from params to the array
	irrigation.schedType = zerosGrid + irrigSchedType;

	% Create the system yield array, will be read in eventually
	% For now, just assign a constant from params to the array, convert from
	% m^3/hr to m^3/s
	irrigation.systemYield = zerosGrid + unit_conversions(irrigYield,'phr','ps');

	% Create the field size array, will be read in eventually
	% For now, just assign a constant from params to the array, convert to
	% from ha to m^2
	irrigation.fieldSize = zerosGrid + unit_conversions(irrigFieldSize,'ha','m2');


    % Update the structure
    state = update_struct(state,'irrigation_present',irrigation.present);
    structure = update_struct(structure,'irrigation',irrigation);
end

% If readLU is true, than a new landuse dataset was read in, so map over the
% params
if readLU
    [luMapTable,depCapMapTable,soilTexture] = deal_struct(params,'map_table_lu','map_table_dep_cap','soil_texture');
    [landUse,soilThick,topography,numLayers,zerosGrid] = ...
        deal_struct(structure,...
        'land_use','thickness_soil','topography','num_layers_soil','zeros_grid');
    slope = topography.slope;

    % Map the canopy properties if necessary, make sure this gets done on the
    % first call!
    [canopyParams] = lu_properties_map(landUse.weights,luMapTable,zerosGrid);
    if structure.irrigation_enabled
        [irrigation] = deal_struct(structure,'irrigation');
        depressionCap = dep_cap_map(landUse.weightsArray,soilTexture(:,1),slope,depCapMapTable,zerosGrid,irrigation);
    else
        depressionCap = dep_cap_map(landUse.weightsArray,soilTexture(:,1),slope,depCapMapTable,zerosGrid);
    end
    % Now, load some litter parameters and calculate only if litter
    % thicknesses are greater than 0
    if any(canopyParams.litter_thickness > 0)
        [fieldCapFrac,porosity,residWater] = deal_struct(params,...
            'litter_field_cap_frac','litter_porosity','litter_resid_water_frac');

        canopyParams.litter_field_cap = fieldCapFrac .* canopyParams.litter_thickness; % from Ogee and Brunet, 2002,
        canopyParams.litter_sat_cap = porosity * canopyParams.litter_thickness;
        canopyParams.litter_resid_water = residWater * canopyParams.litter_thickness;
    end

    % Calculate the root fraction in each layer
    soilLayerBottom = cumsum(soilThick,2);
    canopyParams.root_mass_fraction = root_depth_model('forward',...
        repmat(canopyParams.root_beta,1,numLayers),soilLayerBottom);
    % Normalize to 1
    canopyParams.root_mass_fraction = canopyParams.root_mass_fraction ./ ...
        repmat(sum(canopyParams.root_mass_fraction,2),1,numLayers);

    % Save the updated canopy properties
    % Stem area goes into the state variables
    state = update_struct(state,'stem_area',canopyParams.stem_area);
    canopyParams = rmfield(canopyParams,'stem_area');

    % The remainder are params variables
    canopyFields = fieldnames(canopyParams);
    for m = 1:length(canopyFields)
        params = update_struct(params,canopyFields{m},canopyParams.(canopyFields{m}));
    end
    params = update_struct(params,'soil_dep_cap',depressionCap);
end

% Update watershed routing structures
if readFlowtimes
    % Build the stream routing structure, updates structure directly
    [watershedRelate,watershedKey,watershedRunoff,watershedEvapDemand] = stream_routing_update(structure,state);

    % Save updated variables, really, runoff_watershed_key is only updated once, on the first model timestep
    structure = update_struct(structure,'runoff_watershed_key',watershedKey,...
        'runoff_watershed_relate',watershedRelate);

    % This is only ever updated if it hasn't been initialized
    state = update_struct(state,'watershed_runoff',watershedRunoff,'watershed_evap_demand',watershedEvapDemand);
end

% Update the imperviousness structures
if readImperv
    [impervFrac] = deal_struct(structure,'fraction_impervious');

    % Calculate total imperviousness and total perviousness
    impervFrac.total = impervFrac.upland + impervFrac.wetland + impervFrac.runoff;
    impervFrac.pervious = 1 - impervFrac.total;

    % Update data controller
    structure = update_struct(structure,'fraction_impervious',impervFrac);
end

% Check to see if the current LAI map is up to date, if not, read in the new
% one and interpolate between maps if necessary
[state,structure,buffers,readLAI] = read_lai(state,structure,buffers,date);

% If readLAI is true, then a new LAI dataset was read in, so run
% calculations on canopy height
if readLAI
    [lai,stemArea,laiIrrig,irrigPresent] = deal_struct(state,'lai','stem_area','lai_irrig','irrigation_present');
    [irrigActive] = deal_struct(structure,'irrigation_enabled');

    % ADK- 11/30/17: I commented this out because I am really not sure that
    % this should be done, I think that water cells are not somehow filled
    % uneccessarily
%     % Water land use class has, by definition, an LAI of 0 (wetlands do not)
%     % Need to correct LAI for this, because earlier in preparation we filled all
%     % Cells
%     lai = lai .* (1 - landUse.weights.water); % reduce in proportion to land use water fraction

    if irrigActive
        % Handle irrigated LAI, replace LAI with this
        lai(irrigPresent) = laiIrrig(irrigPresent);
    end

    % Make sure that there are no NaNs in the data -- this really shouldn't
    % happen, but it has cropped up for some reason 12/2/2017
    testNan = isnan(lai);
    lai(testNan) = mean(lai(~testNan));


    % Calculate the new canopy height
    [heightMin,heightMax,specCanopyCap,clumpingIndex] = ...
        deal_struct(params,...
        'canopy_height_min','canopy_height_max','specific_canopy_capacity','canopy_clumping_index');

    % assume that, at an LAI of 3, the canopy has reached its maximum height
    height = min(1,lai / 3) .* (heightMax - heightMin) + heightMin;

    % Calculate the new stem area
    % <--for now this is constant

    % Update the canopy capacity, make sure there are no zeros here (will be
    % divided by elsewhere)
    canopyCap = specCanopyCap .* (lai + stemArea);
    canopyCap(canopyCap==0) = 0.000001; % 0.001 mm of canopy capacity as a minimum

    % Calculate the new canopy gap fraction
    canopyGapFrac = exp(-0.5 .* clumpingIndex .* (lai + stemArea));

    % Update the data_controller
    state = update_struct(state,'canopy_height',height,'stem_area',stemArea,...
        'canopy_capacity',canopyCap,'canopy_gap_fraction',canopyGapFrac,'lai',lai);
end

% For calculations that must be updated in any case
if readLAI || readLU || readImperv
    if structure.sediment_yield_enabled
        % Update MUSLE Cover Factor
        coverFactor = calc_cover_factor(state, structure, params);
        state = update_struct(state, 'musle_C', coverFactor);
    end
end

end


% --------------------------------------------------------------------------
% Sub Functions
% --------------------------------------------------------------------------
function [canopyParams] = lu_properties_map(luWeights,luMapTable,zerosGrid)
% Initialize arrays
numClasses = length(luMapTable.ID);
blankParam = repmat(zerosGrid,1,numClasses);
canopyParams = struct();

% Loop through the map table
mapFields = fieldnames(luMapTable);
for m = 1:length(mapFields)
    % Omit the ID and name fields
    if any(strcmpi(mapFields{m},{'ID','name'}))
    elseif strcmpi(mapFields{m},{'litter_thickness'})
        tempParam = blankParam;
        % Don't include classes with zero litter thickness (interested in
        % litter thickness within the fraction that is covered)
        % Loop through the classes and build the multi-layer params array
        sumWeights = zeros(size(luWeights.(luMapTable.name{1})));
        for n = 1:numClasses
            if isfield(luWeights,luMapTable.name{n})
                tempParam(:,n) = luWeights.(luMapTable.name{n}) * luMapTable.(mapFields{m})(n);
                if luMapTable.(mapFields{m})(n) > 0
                    sumWeights = sumWeights + luWeights.(luMapTable.name{n});
                end
            end
        end
        % Now, normalize by the weights of classes with > 0 thicknesses
        canopyParams.(mapFields{m}) = sum(tempParam,2) ./ sumWeights;
    else
        tempParam = blankParam;
        % Loop through the classes and build the multi-layer params array
        for n = 1:numClasses
            if isfield(luWeights,luMapTable.name{n})
                tempParam(:,n) = luWeights.(luMapTable.name{n}) * luMapTable.(mapFields{m})(n);
            end
        end
        % Sum the params array, the LU weights add up to 1, so no
        % normalization of the weights is necessary
        canopyParams.(mapFields{m}) = sum(tempParam,2);
    end
end
end

function [depCap] = dep_cap_map(luWeights,soils,slope,mapTable,zerosGrid,irrigation)
% Get arrays of values
arrayTex = [mapTable(:).TEX_ID];
arrayLU = [mapTable(:).LU_ID];
arraySlopeMin = [mapTable(:).SLOPEMIN];
arrayIrrig = [mapTable(:).IRRIGTECH];

% First, do this without using the irrigation entries
indIrrig = arrayIrrig > 0;

% Determine the unique values of each of the classification columns
uniqueTex = count_unique(arrayTex(~indIrrig));
uniqueLU = count_unique(arrayLU(~indIrrig));
uniqueSlopeMin = count_unique(arraySlopeMin(~indIrrig));

% Omit water and wetland classes from this (depression capacity only used
% for uplands)
uniqueLU(uniqueLU==5 | uniqueLU==6 | uniqueLU==10) = [];

% Get the adjusted weight to omit water from depression capacity
adjustWeight = sum(luWeights(:,uniqueLU),2);

% Loop through each of unique classification values, and build find arrays
texFind = cell([1,length(uniqueTex)]);
for m = 1:length(uniqueTex)
   texFind{m} = (soils == uniqueTex(m));
end

% luFind = cell([1,length(uniqueLU)]);
% for m = 1:length(uniqueLU)
%     luFind{m} = (lu == uniqueLU(m));
% end

slopeFind = cell([1,length(uniqueSlopeMin)]);
for m = 1:length(uniqueSlopeMin)
    thisMax = mapTable(find(arraySlopeMin==uniqueSlopeMin(m),1,'first')).SLOPEMAX;
    slopeFind{m} = ((slope >= uniqueSlopeMin(m)) & (slope < thisMax));
end

% Now, loop through them in nested loops creating the depCap array
depCap = zerosGrid;
for m = 1:length(uniqueTex)
    thisTex = texFind{m};
    mapTex = (arrayTex==uniqueTex(m));
    for n = 1:length(uniqueLU)
        thisLU = uniqueLU(n);
        mapLU = (arrayLU==uniqueLU(n));
        for o = 1:length(uniqueSlopeMin)
            thisSlope = slopeFind{o};
            mapSlope = (arraySlopeMin==uniqueSlopeMin(o));
            mapVal = mapTable(mapTex & mapLU & mapSlope).DEPCAP;
            thisInd = thisSlope & thisTex;
            depCap(thisInd) = depCap(thisInd) + luWeights(thisInd,thisLU).*mapVal;
            % Newer, better formulation above that weights by LU, this below
            % is not needed any longer
%             indPres = mapTex & mapLU & mapSlope;
%             if any(indPres)
%             depCap(theseTex & theseLU & thisSlope) = mapVal;
%             end
        end
    end
end

% Adjust for the missing water classes
depCap = depCap ./ adjustWeight;
depCap(isnan(depCap)) = 0;

% Now, amend for irrigation types
if nargin == 6
    % Get the irrigation system types
    uniqueIrrig = count_unique(arrayIrrig(indIrrig));

    % Determine weight of irrigation systems to adjust
    adjustWeight = sum(irrigation.weightsPresent(:,uniqueIrrig),2);

    % Loop through, getting adjustment weights
    adjustDepCap = zerosGrid;
    for m = 1:length(uniqueIrrig)
        thisIrrig = uniqueIrrig(m);
        mapIrrig = (arrayIrrig==thisIrrig);
        for n = 1:length(uniqueSlopeMin)
            thisSlope = slopeFind{n};
            mapSlope = (arraySlopeMin==uniqueSlopeMin(n));
            mapVal = mapTable(mapIrrig & mapSlope).DEPCAP;
            adjustDepCap(thisSlope) = adjustDepCap(thisSlope) + irrigation.weightsPresent(thisSlope,thisIrrig).*mapVal;
        end
    end

    % Take the weighted sum of both
    depCap = depCap .* (1-adjustWeight) + adjustDepCap .* adjustWeight;
end
end

function coverFactor = calc_cover_factor(state, structure, params)
% Created: 9/12/2013 by Travis Dahl (dahlt@msu.edu)
% Last Modified: 11/30/2017 by Anthony Kendall

% Need to check cover factors for all land uses
[landUse,impervFrac,zerosGrid] = deal_struct(structure, 'land_use','fraction_impervious','zeros_grid');
[canopyGap, avgCanopyHeight] = deal_struct(state,'canopy_gap_fraction','canopy_height');
[mulchFrac,mapTable] = deal_struct(params,'litter_fraction','map_table_cov_fac');

% Allocate
[coverFactor,sumWeights] = deal(zerosGrid);

% Calculate percent canopy cover
canopyFrac = 1 - canopyGap;

luClasses = fieldnames(landUse.weights);
for m = 1:length(luClasses)
    thisLU = luClasses{m};
    incrementalWeight = landUse.weights.(thisLU); % by default
    switch thisLU
        case 'urban'
            % Rescale impervious fraction to urban area only
            urbImperv = max(1,impervFrac.total ./ incrementalWeight); % limit total imperviousness to 1
            urbImperv(incrementalWeight==0) = 0;
            urbImperv(urbImperv>1) = 1;
            urbPerv = 1 - urbImperv;
            % Now, calculate the incrementalFactor as the weighted average
            incrementalFactor = 0.001.*urbImperv + dynamic_cover_factor(canopyFrac,mulchFrac,avgCanopyHeight,mapTable).*urbPerv; % Assume minimal soil surface accessible to erosion
        case {'agriculture','grassland','shrubland'}
            incrementalFactor = dynamic_cover_factor(canopyFrac,mulchFrac,avgCanopyHeight,mapTable);
        case {'water','wetland','woody_wetland'}
            incrementalFactor = 0; % (may want to change based on "dryness" of cell in a later model iteration)
            incrementalWeight = 0; % should not be included in the calculation
        case 'barren'
            incrementalFactor = 0.45; % From Table 10 of Wischmeier (1978); No appreciable canopy/0% ground cover
        case {'coniferous','deciduous'}
            % Assume mature forest with 75% canopy cover and 90% area covered by 2" of duff
            % XXX Consider changing based on seasonal changes to canopy
            incrementalFactor = 0.001; % From Table 11 of Wischmeier (1978)
    end

    % Update the whole coverFactor array
    coverFactor = coverFactor + incrementalFactor .* incrementalWeight;
    sumWeights = sumWeights + incrementalWeight;

end

% Reweight to remove influence of water and wetland classes
coverFactor = coverFactor ./ sumWeights;
end


function [coverFactor] = dynamic_cover_factor(canopy,mulch,avgCanopyHeight,mapTable)
% The cover factors here are loaded into LHM via the config Excel file
% (lhm_config_v1.xls).
% The cover factors for 0.5 & 1m were derived from Wischmeier & Smith
% (1978), Figures 6 & 7, respectively.  0m data is assumed to be the same
% as the 0% canopy cover for 0.5m, regardless of canopy fraction.  Cover
% factors for 2m & 4m were derived by multiplying data from Figure 5
% (effect of fall height on C factor) by the corresponding line of the 0m
% data.  0% canopy cover is assumed to be the same across all canopy
% heights and is derived from the 1m data (Figure 6 in Wischmeier & Smith,
% 1978, as suggested in that document).

% Determine the unique values of each of the classification columns
uniqueMulch = count_unique([mapTable(:).MULCHFRAC]);
uniqueCanopy = count_unique([mapTable(:).CANFRAC]);
uniqueHeight = count_unique([mapTable(:).CANHEIGHT]);

% Reshape the table as a 3D array
factorC = [mapTable(:).COVFAC];
factorC = reshape(factorC,length(uniqueMulch),length(uniqueCanopy),length(uniqueHeight));

% Interpolate
coverFactor = interpn(uniqueMulch, uniqueCanopy, uniqueHeight, ...
    factorC, canopy, mulch, avgCanopyHeight);
end

function irrigClump = irrigation_clumping_algorithm(irrigWeights)
% Now, pass through the layer with an increasing size window, allocating
% irrigation weight to the heighest surrounding weight
numRows = size(irrigWeights,1);
numCols = size(irrigWeights,2);
tol = 1e-8; % tolerance for preservation of area
numThresh = 50; % number of cells to stop clumping algorithm

notComplete = true;
irrigClump = irrigWeights;
windowWidth = 1;
while notComplete
    windowWidth = windowWidth + 2; % increase by 2 cells
    halfWidth = (windowWidth-1)/2;

    for n = 1:numCols
        for m = 1:numRows
            % Determine if this cell can be skipped
            thisWeight = irrigClump(m,n);
            if (thisWeight>0) && (thisWeight<1)
                % Extract out the window
                subsLeft = max(n-halfWidth,1);
                subsRight = min(n+halfWidth,numCols);
                subsTop = max(m-halfWidth,1);
                subsBot = min(m+halfWidth,numRows);
                irrigClump(m,n) = 0; % set this for now, will reset later
                weightRemain = thisWeight;
                window = irrigClump(subsTop:subsBot,subsLeft:subsRight);
                windowSize = size(window);

                % Flatten, test for continue, then sort
                window = window(:);
                windowSum = sum(window);
                windowLength = length(window);

                % Mask out the center, and already full cells
                windowFull = window==1;
                window(windowFull) = 0;

                if any(window>thisWeight) % must be some irrig greater than current cell to continue
                    % Sort, retaining sort order
                    [windowSort,sortOrder] = sort(window,'descend');

                    % Distribute thisWeight to cells in descending order
                    weightRoom = 1 - windowSort;
                    for o = 1:windowLength
                        if weightRoom(o) < 1 % Must be some irrigation in this cell to begin with
                            weightMove = min(weightRoom(o),weightRemain);
                            windowSort(o) = windowSort(o) + weightMove;
                            weightRemain = weightRemain - weightMove;
                        else
                            break % we have reached the end of cells with any irrigation present
                        end
                        if weightRemain==0
                            break % stop when done
                        end
                    end

                    % Now, move these back to the window, and then to the
                    % weights array, add back in full cells
                    window(sortOrder) = windowSort;
                    window(windowFull) = 1;

                    assert(abs((sum(window)+weightRemain)-(windowSum+thisWeight))<tol)
                    window = reshape(window,windowSize);


                    % Finally, back to the irrigClump array
                    irrigClump(subsTop:subsBot,subsLeft:subsRight) = window;
                end

                % Set this back
                irrigClump(m,n) = weightRemain;

            end
        end
    end

    % Test for continuation
    numShort = sum(irrigClump(:)<(1-tol)&irrigClump(:)>0);
    notComplete = numShort > numThresh;
end

% Now set remaining to 0
testRound = irrigClump < 1;
irrigClump(testRound) = round(irrigClump(testRound));
end

function [watershedRelate,watershedKey,watershedRunoff,watershedEvapDemand] = stream_routing_update(structure,state)
[runoffGauges,dates,runoffFlowtimes,throughflowFlowtimes,downstreamWatershed,...
    runoffWatershed,topography,numInnerSteps,timestepLength] = ...
    deal_struct(structure,...
    'runoff_gauges','dates','runoff_flowtimes','runoff_subsurface_flowtimes','runoff_downstream_watershed',...
    'runoff_watershed','topography','num_inner_timesteps','timestep_length');
meanElev = topography.elevation_mean;

% Initialize
[watershedRelate,watershedKey] = deal(false);

% Determine the watershed/gauge numbers
if ~all(isnan(runoffGauges))
    gauges = count_unique(runoffGauges);
    gauges = gauges(gauges>0);
    numGauges = length(gauges);

    % Test to see if the watershed_runoff array is already created, if not, create it
    if ~isfield(state,'watershed_runoff')  || isempty(state.watershed_runoff)
        watershedRunoff = cell(numGauges,1);

        % Build the watershed_runoff and watershed_evap_demand arrays
        maxFlowtime = max(runoffFlowtimes(:));
        numTimestepsFlowtime = ceil(maxFlowtime / timestepLength);
        numTimestepsModel = length(dates) * numInnerSteps;
        for m = 1:numGauges
            state.watershed_runoff{m} = zeros(numTimestepsModel + numTimestepsFlowtime - 1,1);
        end
        % Copy watershedRunoff to watershedEvapDemand
        watershedEvapDemand = watershedRunoff;
    else
        watershedRunoff = state.watershed_runoff;
        watershedEvapDemand = state.watershed_evap_demand;
    end
    
    % Build the watershed relate table
    watershedRelate = zeros(length(gauges),4); % fields are gauge number (matches watershed), downstream gauge, flowtime to downstream gauge, and elevation (temporary)
    for m = 1:length(gauges)
        thisInd = runoffGauges==gauges(m);
        watershedRelate(m,1) = gauges(m);
        watershedRelate(m,2) = downstreamWatershed(thisInd); % this can be empty, if no DS watershed
        watershedRelate(m,3) = runoffFlowtimes(thisInd); % for surface runoff routing
        watershedRelate(m,4) = meanElev(thisInd);
        watershedRelate(m,5) = throughflowFlowtimes(thisInd); % for throughflow routing
    end

    % Check to make sure that no gauges runoff to themselves, set to NaN if
    % so
    test = (watershedRelate(:,1) == watershedRelate(:,2));
    watershedRelate(test,2) = NaN;

    % Sort by elevation, then remove the elevation field
    watershedRelate = sortrows(watershedRelate,4);
    watershedRelate(:,4) = [];

    % Build the watershed key, sorted in same order as watershedRelate
    % This could be moved to structure, as it never changes during the model run
    watershedKey = cell(length(gauges),2);
    for m = 1:length(gauges)
        watershedKey{m,1} = watershedRelate(m,1);
        watershedKey{m,2} = (runoffWatershed == watershedRelate(m,1));
    end
end

end
