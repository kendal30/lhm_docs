function state = shortwave_radiation_module(state,structure,params)
%SHORTWAVE_RADIATION_MODULE  Calculates canopy and ground shortwave radiation fluxes.
%   shortwave_radiation_module()
%
%   This module uses the radiation interception formulation Chen et al. 2005.
%   It intercepts beam and diffuse radiation separately.  Units of radiation
%   are J/timestep.  Currently, NIR and VIS radiation are treated as equal,
%   with albedos weighted according to the partitioning of shortwave radiation
%   into those respective wavebands.
%
%   All longwave radiation components are solved for in their respective
%   temperature modules.  This includes the canopy transmitted longwave
%   radiation which is added to the canopy-originated longwave radiation
%   (and vice-versa for the ground-originated longwave radiation).
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also: update_albedo

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[radBeam,radDiff,sunZenithCos,lai,canopyGapFrac,...
    albedoCanopyBeam,albedoCanopyDiff,albedoUplandBeam,albedoUplandDiff,...
    albedoWetlandBeam,albedoWetlandDiff,wetlandFrac,...
    uplandFrac,streamFrac] = ...
    deal_struct(state,...
    'rad_short_beam_in','rad_short_diff_in','sun_zenith_cos','lai','canopy_gap_fraction',...
    'albedo_canopy_beam','albedo_canopy_diff','albedo_upland_beam','albedo_upland_diff',...
    'albedo_wetland_beam','albedo_wetland_diff','fraction_wetland',...
    'fraction_upland','fraction_stream');
[zerosGrid] = deal_struct(structure,'zeros_grid');

%Check to see if there was any radiation during this timestep
flagRad = any(radBeam(:) > 0);
if flagRad
    %Update wetlandFrac to include streamFrac, currently
    wetlandFrac = wetlandFrac + streamFrac;
    
    %--------------------------------------------------------------------------
    %                         Canopy Beam Radiation
    %--------------------------------------------------------------------------
    %Calculate the beam radiation transmission factor
    coszen = max(1e-5,sunZenithCos);
    beamTransmitFac = exp(-canopyGapFrac ./ coszen);
    %Calculate the flux of beam radiation that passes through the canopy
    groundRadBeam = radBeam .* beamTransmitFac;
    %Calculate the reflected flux of beam radiation
    reflectRadBeam = (radBeam - groundRadBeam) .* albedoCanopyBeam;

    %--------------------------------------------------------------------------
    %                        Canopy Diffuse Radiation
    %--------------------------------------------------------------------------
    %Calculate the effective angle for diffuse radiation transmission (Chen et
    %al. 2005)
    cosEffDiffAngle = 0.537 + 0.025 * lai;
    %Now calculate the transmission fraction
    diffTransmitFac = exp(-canopyGapFrac ./ cosEffDiffAngle);
    %Finally, calculate the flux of diffuse radiation that passes through the
    %canopy
    groundRadDiff = radDiff .* diffTransmitFac;
    %Calculated the reflected flux of diffuse radiation
    reflectRadDiff = (radDiff - groundRadDiff) .* albedoCanopyDiff;

    %--------------------------------------------------------------------------
    %                 Upland- and Wetland- Reflected Radiation
    %--------------------------------------------------------------------------
    %Upland and Wetland radiation terms are treated as if the entire cell were
    %of one type or the other in each cell.  To be clearer, the portion of a
    %cell that is upland will receive radiation exactly the same as if that
    %cell were completely upland.  The total reflected radiation, however, will
    %depend on the fraction of the cell covered up upland.
    %Beam
    uplandReflectBeam = groundRadBeam .* albedoUplandBeam;
    wetlandReflectBeam = groundRadBeam .* albedoWetlandBeam;
    %Diffuse
    uplandReflectDiff = groundRadDiff .* albedoUplandDiff;
    wetlandReflectDiff = groundRadDiff .* albedoWetlandDiff;
    %Calculate transmitted fraction, assume that ground-reflected radiation
    %behaves diffusely rather than as beam radiation, need to scale
    %upland and wetland radiation to fraction of cell covered by each type
    groundReflect = (uplandReflectBeam + uplandReflectDiff) .* uplandFrac + ...
        (wetlandReflectBeam + wetlandReflectDiff) .* wetlandFrac;
    transmitReflect = groundReflect .* diffTransmitFac;

    %--------------------------------------------------------------------------
    %                       Net Fluxes Reflected and Absorbed
    %--------------------------------------------------------------------------
    %Sum the reflected Beam and Diffuse components, as reflected radiation will
    %behave like diffuse radiation from the canopy (i.e. non-specular
    %reflection)
    totalShortwave = radBeam + radDiff; %W/m^2_cell_average
    totalGround = groundRadDiff + groundRadBeam; %W/m^2_cell_average
    netReflect = reflectRadBeam + reflectRadDiff + transmitReflect; %W/m^2_cell_average
    netRadUpland = totalGround - uplandReflectDiff - uplandReflectBeam; %W/m^2_upland
    netRadWetland = totalGround - wetlandReflectDiff - wetlandReflectBeam; %W/m^2_wetland
    netRadCanopy = totalShortwave - netReflect - totalGround + groundReflect; %W/m^2_cell_average

    state = update_struct(state,'rad_short_surface_up',netReflect,'rad_short_upland_net',netRadUpland,...
        'rad_short_wetland_net',netRadWetland,'rad_short_canopy_net',netRadCanopy);
else
    state = update_struct(state,'rad_short_surface_up',zerosGrid,'rad_short_upland_net',zerosGrid,...
        'rad_short_wetland_net',zerosGrid,'rad_short_canopy_net',zerosGrid);
end