function [stomatalCondUpland,stomatalCondWetland,indActive] = stomatal_conductance(state,structure,params)
%STOMATAL_CONDUCTANCE  Calculates stomatal conductance under limiting conditions
%   output = stomatal_conductance(input)
%
%   After Jarvis, 1976 (see Chen et al 2005) canopy resistance is taken to
%   be:
%   rc = 1 / (LAI * sc), where sc is the stomatal conductance
%   sc = rs_max * F1(S) * F2(T) * F3(D) * F4(Phi) * F5(CO2), where S is the shortwave
%   flux, T is the air temperature, D is the vapor pressure deficit, and
%   Phi is the soil pressure head, or the negative of matric potential
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   stomatalCondUpland: upland stomatal conductance, one layer for each
%   soil layer
%   stomatalCondWetland: wetland stomatal conductance
%   indActive:     true/false value indicating if the canopy is dormant or
%                   not
%
%   Example(s):
%   >> stomatal_conductance()
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-09
% Copyright 2008 Michigan State University.

[radShortCanopy,airTemp] = deal_struct(state,...
    'rad_short_canopy_net','temp_air');
[rootFrac,maxStomatalCond,dormantTemp] = deal_struct(params,...
    'root_mass_fraction','stomatal_conductance_max','stomatal_temp_dormant');
[zerosGrid,zerosGridSoil,soilNumLayers,indUpland] = deal_struct(structure,...
    'zeros_grid','zeros_grid_soil','num_layers_soil','index_upland');

%Check to see if the entire area is colder than the dormant temperature
flagActive = any(airTemp(:) > dormantTemp) & any(radShortCanopy(:) > 0);
if flagActive
    %Initialize arrays
    ind = indUpland;
    stomatalCondWetland = zerosGrid;
    stomatalCondUpland = zerosGridSoil;
    
    %Calculate F1: Shortwave radiation effect on stomatal conductance
    F1 = shortwave_factor_chen(ind,radShortCanopy,params);
    
    %Calculate F2: Air temperature effect on stomatal conductance
    F2 = air_temperature_factor_chen(ind,airTemp,zerosGrid,params);

    %Calculate F3: Vapor pressure deficit effect on stomatal conductances
    F3 = vpd_factor_chen(ind,airTemp,zerosGrid,state,params);

    %Calculate F4: Plant moisture limitation, and soil resistance
    %This is multi-layer, as it's soil dependent, and varies in uplands and wetlands
    %F4Upland = soil_water_factor_lhomme(ind,state,structure,params) .* rootFrac(ind,:);
    F4Upland = soil_water_factor_chen(ind,state,params) .* rootFrac(ind,:);
    %uniform and 1 for wetlands
    F4Wetland = zerosGrid(ind) + 1; 
    
    %Calculate F5: CO2 effect on stomatal conductance
    %F5 = carbon_dioxide_factor_salus(ind,state,params,salus);
    
    %Calculate stomatal conductance
    precalc = maxStomatalCond(ind) .* F1 .* F2 .* F3;% .* F5;
    stomatalCondWetland(ind) = precalc .* F4Wetland;
    stomatalCondUpland(ind,:) = repmat(precalc,1,soilNumLayers).* F4Upland;
    
    %Check to see if any of the F1, F2, or F3 terms are 0
    indActive = (stomatalCondWetland > 0);
else
    stomatalCondWetland = false;
    stomatalCondUpland = false;
    indActive = logical(zerosGrid);
end
end

function [F1] = shortwave_factor_chen(ind,radShortCanopy,params)
%This calculation is from Chen et al 2005
[convertShortPhoto,coeffStomaPhoto] = deal_struct(params,...
    'stomatal_convert_shortwave_photo','stomatal_coeff_photo');
precalc = coeffStomaPhoto * convertShortPhoto * radShortCanopy(ind);
F1 = precalc ./ (1 + precalc);
end

function [F2] = air_temperature_factor_chen(ind,airTemp,zerosGrid,params)
%This calculation is from Chen et al 2005
[optimalTemp,maxTemp] = deal_struct(params,'stomatal_temp_optimal','stomatal_temp_max');

precalc = airTemp(ind) - optimalTemp;
ind1 = (precalc <= 0) & (airTemp(ind) > dormantTemp); %i.e. airTemp > optimalTemp
ind2 = (precalc > 0) & (airTemp(ind) <= maxTemp);
F2 = populate_array(zerosGrid(ind),log(airTemp(ind)) / log(optimalTemp),cos(pi()/2*precalc/(maxTemp - optimalTemp)),...
    ind1,ind2);
end

function [F3] = vpd_factor_chen(ind,airTemp,zerosGrid,state,params)
%This calculation is from Chen et al 2005
[airVapPress] = deal_struct(state,'vap_pressure_air');
[vapDeficitClose,vapDeficitOpen] = deal_struct(params,...
    'stomatal_vapor_deficit_close','stomatal_vapor_deficit_open');

vapPressDeficit = sat_vap_pressure(airTemp(ind)) - airVapPress(ind);
precalc = vapDeficitClose - vapPressDeficit;
ind1 = vapPressDeficit <= vapDeficitOpen;
ind2 = ~ind1 & (precalc > 0);
F3 = populate_array(zerosGrid(ind),1,precalc ./ (vapDeficitClose - vapDeficitOpen),...
    ind1,ind2);

end

function [F4] = soil_water_factor_lhomme(ind,state,structure,params) %#ok<DEFNU>
%This calculation is from Lhomme et al 1998
[psiSoil,latentUpland,soilCond] = deal_struct(state,...
    'matric_potential_soil','latent_canopy_transp_upland','soil_unsat_conductivity');
[leafPotClose,rootGeometryFac,resRootStem] = deal_struct(params,...
    'stomatal_leaf_potential_close','stomatal_root_geometry','stomatal_res_root_stem');
[soilThick] = deal_struct(structure,'thickness_soil');

resSoilRoot = rootGeometryFac * 0.4e-9 ./ (soilThick(ind,:) .* soilCond(ind,:));
resSoilPlant = resSoilRoot + resRootStem;
psiLeaf = psiSoil(ind,:) - resSoilPlant .* repmat(latentUpland(ind,:),[1,soilNumLayers]);
psiLeaf = max(psiLeaf,leafPotClose); %these are negative values
F4 = 1 - psiLeaf / leafPotClose; %multilayer
    
end

function [F4] = soil_water_factor_chen(ind,zerosGrid,state,params)
%Here's a more traditional linear piecewise model, using Chen et al 2005
%This recalculates quantities that may already be calculated elsewhere
[waterSoil] = deal_struct(state,'water_soil');
[soilWiltPt,soilFieldCap,soilSatCap] = deal_struct(params,'soil_wilting_point','soil_field_cap','soil_sat_cap');

ind1 = (waterSoil(ind,:) > soilWiltPt(ind,:)) & (waterSoil(ind,:) <= soilFieldCap(ind,:));
ind2 = (waterSoil(ind,:) > soilFieldCap(ind,:)) & (waterSoil(ind,:) <= soilSatCap(ind,:));

F4 = zerosGrid(ind,:);
F4(ind1) = (waterSoil(ind,:) - soilWiltPt(ind,:)) ./ (soilFieldCap(ind,:) - soilWiltPt(ind,:));
F4(ind2) = 1 - 0.5 * (waterSoil(ind,:) - soilFieldCap(ind,:)) ./ (soilSatCap(ind,:) - soilFieldCap(ind,:));
end

function [F5] = carbon_dioxide_factor_salus(ind,zerosGrid,state,params,salus)
%This comes from the SALUS function TRATIO
[airCO2] = deal_struct(state,'air_CO2');
[baselineCO2] = deal_struct(params,'stomatal_baseline_CO2');
[cCrop] = deal_struct(salus,'cCrop');

indC3 = strcmpi(cCrop.PhotoSynID(ind),'C3');
indC4 = strcmpi(cCrop.PhotoSynID(ind),'C4');

baselineRes = populate_array(zerosGrid(ind),(1 / (0.0328 - 0.0000549 * baselineCO2 + 0.0000000296 * baselineCO2 ^ 2)) + 10,...
    9.72 + 0.0757 * baselineCO2 + 10, indC4, indC3);
elevatedRes = populate_array(zerosGrid(ind),(1 / (0.0328 - 0.0000549 * airCO2 + 0.0000000296 * airCO2 ^ 2)) + 10,...
    9.72 + 0.0757 * airCO2 + 10, indC4, indC3);

%The factor is the inverse ratio of the airCO2 resistance and the baseline
%CO2 resistance
F5 = baselineRes ./ elevatedRes;
end