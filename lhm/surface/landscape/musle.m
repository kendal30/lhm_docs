function state = musle(state, structure, params)
% This function runs the Modified Universal Soil Loss Equation (MUSLE) as
% presented in Williams (1995).  See Williams (1975)for more information on
% the algorithm and its development.  This implementation is intended to be
% run on a daily basis, with hourly time steps used to calculate the peak
% runoff values.
% Created: 9/12/2013 by Travis Dahl (dahlt@msu.edu)
% Last Modified: 7/13/2015 by Anthony Kendall (kendal30@msu.edu)
%
% From Williams (1995):
%   MUSLE: SedOut = 1.586 * (Q * qPeak)^0.56 * A^0.12 * K * C * P * LS _
%                   * ROKF
%   SedOut: sediment yield in t/ha
%   Q: runoff volume in mm
%   qPeak: peak runoff rate in mm/hr
%   A: watershed area in ha
%
% qSurf should be the total runoff for the given day
% qPeak should be the highest single hour runoff as a single 3-d grid
% - area K, (C?), P, LS, CFRG are fixed and should probably be loaded as a
% single 3-D array
%
% Input Variables:
% K_P_LS_CFRG_sedArea -- Constant MUSLE factors multiplied together, along
%     with a conversion factor for area
% qPeak -- peak flow for day (m/timestep, estimated in this code)
% qSum -- total runoff for day (m/day)
% SWE -- snow water equivalent (m)
%
% Output Variable:
% sedOut -- sediment delivered to stream (kg/m^2)

% Read inputs
if structure.sediment_yield_enabled
    [K_P_LS_CFRG_sedArea] = deal_struct(params, 'musle_K_P_LS_CFRG_sedArea');
    [hour, qPeak, qSum, qSurf, SWE, coverFactor] = deal_struct(state, 'inner_timestep', ...
        'musle_qPeak', 'musle_qSum','precip_excess_upland', 'water_snow_upland','musle_C');
    [timestepLength,numInnerSteps,zerosGrid] = deal_struct(structure, ...
        'timestep_length','num_inner_timesteps','zeros_grid');
    
    %Update qPeak and qSum
    if (hour == 1)
        % Set initial qPeak candidate
        [qPeak,qSum] = deal(qSurf);
    else
        % Check for higher qPeak values
        qPeak = max(qPeak, qSurf);
        %Add to qSum
        qSum = qSum + qSurf;
    end
    
    % Check to see if it's the end of a day
    if (hour == numInnerSteps)
        %Conversion factors
        convFactPeak = 1000 / timestepLength * 3600; %converts from m/timestep to mm/hr
        convFactSum = 1000; %converts from summed m to mm
        
        % Calculate the sediment yield for the day
        sedOut = 1.586 .* (convFactPeak * convFactSum .* qSurf .* qPeak) .^0.56 .* coverFactor .* K_P_LS_CFRG_sedArea;
        
        % This code is the algorithm described in the SWAT 2009 Documentation
        % for limiting sediment production under snow cover.
        % Note: the SWAT equation uses SWE in mmH2O, so a conversion factor of
        % 1000 was included below
        sedOut = sedOut ./ exp(3000 * SWE / 25.4);
        
        % XXX Convert units to from metric tons/ha -> kg/m^2
        % sedOut = sedOut ./ 10000 % Convert hectares -> m^2
        % sedOut = sedOut .* 1000 % Convert metric tons -> kg
        sedOut = sedOut ./ 10;
    else
        %Not the end of the day, so no sediment yield yet
        sedOut = zerosGrid;
    end
    
    %Update the state variables
    state = update_struct(state, 'musle_qPeak', qPeak, 'musle_qSum', qSum, 'sediment_yield_upland', sedOut);
end
end
