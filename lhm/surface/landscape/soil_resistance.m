function [resistanceNet,rl] = soil_resistance(structure,params,soilTheta,litterSatFrac,indLitter,indSoil,airDens)
%SOIL_RESISTANCE  Calculates the soil resistance to vapor transport
%   resistance = soil_resistance(input)
%
%   Descriptions of Input Variables:
%   soilTheta:  volumetric soil moisture (m^3/m^3)
%   litterSatFrac: fraction of saturation of the litter
%       (m^3water/m^3porosity)
%   indLitter:  indeces where litter is present
%   indSoil:    indeces where soil is present
%   airDens:    moist air density (kg/m^3)
%
%   Descriptions of Output Variables:
%   resistanceNet: the net resistance considering the parallel summation of
%       resistances from the litter-soil system (covered by litterFrac area),
%       and the soil only (1 - litterFrac)
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-14
% Copyright 2008 Michigan State University.
[waterVapDiffCoeff,...
    litterThick,litterFrac,resDiffSurf,phase2Thresh,resCoeff] = ...
    deal_struct(params,...
    'diff_coeff_vapor','litter_thickness','litter_fraction',...
    'resistance_diffusion_surface','soil_evap_phase_2_thresh','soil_evap_resistance_coeff');
[zerosGrid] = deal_struct(structure,'zeros_grid');

[rl,rs] = deal(zerosGrid);

%Calculate the resistance to vapor transport in the litter, this is from
%Choudhury and Monteith and Moldrup et al. 2001
%This should use the actual density of air in the litter, but this seems
%like unecessary calculation, really
diffCoeffLitter = waterVapDiffCoeff * (1 - litterSatFrac(indLitter)).^(1.5) ./ airDens(indLitter);
rl(indLitter) = litterThick(indLitter) ./ diffCoeffLitter;

%Calculate soil resistance as an exponential function of saturation 
%This is from van de Griend and Owe 1994
phase2Theta = min(phase2Thresh,soilTheta(indSoil,1));
rs(indSoil) = resDiffSurf .* exp(resCoeff * (phase2Thresh - phase2Theta));

%Calculate the parallel resistance of the litter-covered and
%litter-uncovered area
resistanceNet = weighted_mean('harmonic',rl+rs,rs,litterFrac,(1-litterFrac));
resistanceNet(isnan(resistanceNet)) = 0; %to handle cells with no litter

%Limit rl based on the litter fraction
rl = rl .* litterFrac;