function state = canopy_microclimate(state,structure,params)
%CANOPY_MICROCLIMATE  Calculates below-canopy temperature and vapor pressure.
%   canopy_microclimate()
%
%   This uses formulations from the CLM documentation for intra-canopy
%   temperature and vapor pressure (interchangeable with specific humidity
%   at constant pressure).
%
%   Descriptions of Input Variables:
%   none, all inputs come from the data controller
%
%   Descriptions of Output Variables:
%   none, all outputs are sent to the data controller
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-13
% Copyright 2008 Michigan State University.

[ramCanopy,rahwCanopy,lai,...
    airTemp,canopyTemp,uplandInterfTemp,wetlandInterfTemp,...
    windspeed,canopyGapFrac,vapPressLeaf,vapPressAir,...
    vapPressUpland,vapPressWetland,...
    rb,stemArea,litterRough] = deal_struct(state,...
    'resist_aero_canopy_momentum','resist_aero_canopy_heat_vapor','lai',...
    'temp_air','temp_canopy','temp_interf_upland','temp_interf_wetland',...
    'windspeed','canopy_gap_fraction','vap_pressure_leaf','vap_pressure_air',...
    'vap_pressure_interf_upland','vap_pressure_interf_wetland',...
    'resist_leaf_boundary_layer','stem_area','rough_height_litter');
[csDense,vonKarman,windAlpha,nu,...
    snowRough,iceRough,soilRough] = deal_struct(params,...
    'turb_trans_dense_canopy','von_karman','wind_alpha','kinematic_viscosity_air',...
    'snow_rough_height','ice_rough_height','soil_rough_height');
[zerosGrid,indOpen,indSnowWetland,indIce,...
    indSnowUpland,indLitter,indSoil,indWetland,indUpland] = ...
    deal_struct(structure,...
    'zeros_grid','index_open_water','index_snow_wetland','index_ice_pack',...
    'index_snow_upland','index_litter','index_bare_soil','index_wetland','index_upland');

%--------------------------------------------------------------------------
%Initialize arrays
%--------------------------------------------------------------------------
[roughWetland,roughUpland,openRough,rahwGroundUpland,rahwGroundWetland] = deal(zerosGrid);
[microTempUpland,microTempWetland] = deal(airTemp);
[microVapPressUpland,microVapPressWetland] = deal(vapPressAir);

%--------------------------------------------------------------------------
%Calculate roughness heights
%--------------------------------------------------------------------------
%For wetlands
%Calculate roughness in open water as a function of wave height
openRough(indOpen) = wetland_surface_roughness(params,windspeed(indOpen),ramCanopy(indOpen));
roughWetland = populate_array(roughWetland,snowRough,iceRough,openRough,...
    indSnowWetland,indIce,indOpen);

%For uplandds
roughUpland = populate_array(roughUpland,snowRough,litterRough,soilRough,...
    indSnowUpland,indLitter,indSoil);

%--------------------------------------------------------------------------
%Calculate resistance terms
%--------------------------------------------------------------------------
%Ground aerodynamic resistances below canopy, upland, this is from CLM
%Calculate common terms
uav = windspeed .* (1./(ramCanopy .* windspeed)).^(1/2);
csPrecalc = csDense .* (1 - canopyGapFrac);

%For uplands
csBare = vonKarman / windAlpha .* (roughUpland(indUpland) .* uav(indUpland) / nu).^(-0.45);
cs = csBare .* canopyGapFrac(indUpland) + csPrecalc(indUpland);
rahwGroundUpland(indUpland) = 1 ./ (cs .* uav(indUpland));

%Now for wetlands
csBare = vonKarman / windAlpha .* (roughWetland(indWetland) .* uav(indWetland) / nu).^(-0.45);
cs = csBare .* canopyGapFrac(indWetland) + csPrecalc(indWetland);
rahwGroundWetland(indWetland) = 1 ./ (cs .* uav(indWetland));

%Bare-ground resistance, defined as canopy gap fraction > 95%
ind = canopyGapFrac > 0.95;
rahwGroundUpland(ind) = rahwCanopy(ind); %the canopy calculation is more appropriate
rahwGroundWetland(ind) = rahwCanopy(ind); %the canopy calculation is more appropriate

%--------------------------------------------------------------------------
%Calculate conductance terms
%--------------------------------------------------------------------------
%Defined over the entire domain
ca = 1 ./ rahwCanopy;
cv = (lai + stemArea)./ rb;
%Defined only over the domain of each
cgUpland = 1 ./ rahwGroundUpland(indUpland);
cgWetland = 1 ./ rahwGroundWetland(indWetland);

%--------------------------------------------------------------------------
%Below-canopy air temperature
%--------------------------------------------------------------------------
%Defined over the entire domain
precalc1 = ca .* airTemp + cv .* canopyTemp;
precalc2 = ca + cv;
%Defined only over the domain of each
precalc2up = precalc2(indUpland) + cgUpland;
precalc2wet = precalc2(indWetland) + cgWetland;

microTempUpland(indUpland) = (cgUpland .* uplandInterfTemp(indUpland) + precalc1(indUpland)) ./ precalc2up;
microTempWetland(indWetland) = (cgWetland .* wetlandInterfTemp(indWetland) + precalc1(indWetland)) ./ precalc2wet;

%--------------------------------------------------------------------------
%Below-canopy vapor pressure
%--------------------------------------------------------------------------
%Defined over the entire domain
precalc1 = ca .* vapPressAir + cv .* vapPressLeaf;

microVapPressUpland(indUpland) = (cgUpland .* vapPressUpland(indUpland) + precalc1(indUpland)) ./ precalc2up;
microVapPressWetland(indWetland) = (cgWetland .* vapPressWetland(indWetland) + precalc1(indWetland)) ./ precalc2wet;

%--------------------------------------------------------------------------
%Update the data controller
%--------------------------------------------------------------------------
state = update_struct(state,'vap_pressure_micro_upland',microVapPressUpland,...
    'vap_pressure_micro_wetland',microVapPressWetland,'temp_micro_upland',microTempUpland,...
    'temp_micro_wetland',microTempWetland,'resist_aero_ground_heat_vapor_upland',rahwGroundUpland,...
    'resist_aero_ground_heat_vapor_wetland',rahwGroundWetland);