function state = update_albedo(state,structure,params)
%UPDATE_ALBEDO  Updates all shortwave albedos in the model.
%   update_albedo()
%
%   This module calculates all diffuse and beam albedos (no VIS/NIR
%   partitioning currently) for vegetation, litter, soil, wetlands (with
%   ice cover possibly), and snow.  It outputs only the beam and diffuse
%   components of the canopy, uplands, and wetlands.  Using the litter
%   fraction and snow thickness grids, it calculates the net albedo for
%   all three types.
%
%   The output of this module is primarily only used by the shortwave
%   radiation module which calculates net radiation for canopy, upland, and
%   wetlands.
%
%   Descriptions of Input Variables:
%   takes all inputs from the data controller
%
%   Descriptions of Output Variables:
%   returns all outputs to the state controller
%
%   Example(s):
%   none
%
%   See also: shortwave_radiation_module

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-26
% Copyright 2008 Michigan State University.

[lai,canopyTemp,waterCanopy,canopyCap,coszen,waterSoil,...
    snowThickUpland,snowThickWetland,...
    uplandSnowSurfAge,wetlandSnowSurfAge] = deal_struct(state,...
    'lai','temp_canopy','water_canopy','canopy_capacity','sun_zenith_cos','water_soil',...
    'snow_thickness_upland','snow_thickness_wetland','snow_surf_age_upland','snow_surf_age_wetland');
[albedoBrown,albedoGreen,albedoLitter,albedoLakeBeam,albedoLakeDiff,albedoSoilDry,...
    soilSatCap,litterFrac,albedoNewSnowVis,albedoNewSnowNIR,snowGndAlbedoExtDep,...
    albedoLakeIceWet,albedoLakeIceDry] = deal_struct(params,...
    'albedo_brown','albedo_green','albedo_litter','albedo_water_beam','albedo_water_diffuse','albedo_soil_dry',...
    'soil_sat_cap','litter_fraction','albedo_snow_vis','albedo_snow_nir','snow_gnd_albedo_ext_depth',...
    'albedo_lake_ice_wet','albedo_lake_ice_dry');
[zerosGrid,snowCoveredUpland,snowCoveredWetland,iceCoveredWetland,indIceWet,...
    indUpland,indWetland,indFlooded] = deal_struct(structure,...
    'zeros_grid','index_snow_upland','index_snow_wetland','index_ice_pack','index_ice_pack_ponding',...
    'index_upland','index_wetland','index_wetland_flooded');

%--------------------------------------------------------------------------
%Initialize Arrays
%--------------------------------------------------------------------------
[albedoDiffUpland,albedoBeamUpland,albedoCanopyDiff,soilAlbedoDiff,...
    albedoBeamWetland,albedoDiffWetland] = deal(zerosGrid);

%--------------------------------------------------------------------------
%                             Vegetation Albedo 
%--------------------------------------------------------------------------
%query the green and brown albedo values from the params controller

%calculate the vegetation albedo as a linear interpolation between the
%green and brown values.
indComplete = (lai > 3);
indPartial = ~indComplete;
%Linearly interpolate between green and brown albedo 
albedoCanopyDiff(indComplete) = albedoGreen(indComplete);
albedoCanopyDiff(indPartial) = (lai(indPartial) / 3 .* (albedoGreen(indPartial) - ...
    albedoBrown(indPartial)) + albedoBrown(indPartial));
albedoCanopyBeam = albedoCanopyDiff; %assume these are equal for now

%Calculate the effect on albedo from snow on the canopy
test = (canopyTemp < 0) & (waterCanopy > 0);
if any(test)
    albedoNewSnow = (albedoNewSnowVis + albedoNewSnowNIR) / 2;
    snowCovFrac = waterCanopy(test) ./ canopyCap(test);
    %Assume that the snow lies on top of the leaves/stems, and therefore
    %affects beam radiation differently than diffuse radiation, this will
    %also have an effect on up-scattered radiation from beneath the canopy
    albedoCanopyBeam(test) = albedoCanopyBeam(test) .* (1 - snowCovFrac) + albedoNewSnow .* snowCovFrac;
    albedoCanopyDiff(test) = albedoCanopyDiff(test) .* (1 - snowCovFrac/2) + albedoNewSnow .* snowCovFrac/2;
end

%--------------------------------------------------------------------------
%                             Litter Albedo 
%--------------------------------------------------------------------------
litterAlbedoDiff = albedoLitter;  %for now, this will be constant, can be time-variant later
litterAlbedoBeam = albedoLitter; %assume the same for now

%--------------------------------------------------------------------------
%                             Soil Albedo 
%--------------------------------------------------------------------------
soilFracSat = waterSoil(indUpland,1) ./ soilSatCap(indUpland,1);
%Wet soil albedos are 1/2 of dry soil albedos, from CLM
soilAlbedoDiff(indUpland) = albedoSoilDry(indUpland) .* (1 - soilFracSat) + albedoSoilDry(indUpland) / 2 .* soilFracSat;
soilAlbedoBeam = soilAlbedoDiff; %assume equal for now

%--------------------------------------------------------------------------
%                       Net Upland Albedo
%--------------------------------------------------------------------------
litterFreeFrac = (1 - litterFrac(indUpland));
albedoDiffUpland(indUpland) = soilAlbedoDiff(indUpland) .* litterFreeFrac + litterAlbedoDiff .* litterFrac(indUpland);
albedoBeamUpland(indUpland) = soilAlbedoBeam(indUpland) .* litterFreeFrac + litterAlbedoBeam .* litterFrac(indUpland);

%--------------------------------------------------------------------------
%                             Wetland Albedo 
%--------------------------------------------------------------------------
%For this step, start with beam and diffuse wetland albedos are equal
albedoBeamWetland(indWetland) = albedoLakeDiff;
albedoDiffWetland(indWetland) = albedoLakeDiff;

%Modify wetland albedo for ice cover
ind = (iceCoveredWetland);
if any(ind)
    dry = ind & ~indIceWet;
    albedoLakeIce(dry) = albedoLakeIceDry;
    albedoLakeIce(indIceWet) = albedoLakeIceWet;
    albedoBeamWetland(ind) = albedoLakeIce(ind); %assume equal to diffuse, for now
    albedoDiffWetland(ind) = albedoLakeIce(ind);
end

%Modify wetland beam albedo for the zenith angle
ind = (~iceCoveredWetland) & (coszen > 1e-3) & (indWetland);
if any(ind)
    albedoBeamWetland(ind) = albedoLakeBeam ./ (coszen(ind).^(1.7) + 0.15); %from LSM
end

%Now, for dry wetlands, use the soil albedo formulation
indDry = ~indFlooded;

%Assume wetland soils are wet, not perfect and could be improved later
albedoDiffWetland(indDry) = albedoSoilDry(indDry) / 2;
albedoBeamWetland(indDry) = albedoDiffWetland(indDry);

%--------------------------------------------------------------------------
%                             Upland Snow Albedo 
%--------------------------------------------------------------------------
ind = snowCoveredUpland;
if any(ind)
    [snowAlbedoDiff,snowAlbedoBeam] = deal(zerosGrid);
      
    %Calculate the new snow surface albedo
    [snowAlbedoDiff(ind),snowAlbedoBeam(ind)] = bats_albedo(uplandSnowSurfAge(ind),coszen(ind),albedoNewSnowVis,albedoNewSnowNIR);

    ind1 = (snowThickUpland < snowGndAlbedoExtDep);
    if any(ind1)
        %Calculate the net snowpack albedo that takes into account exponential
        %light extinction with depth
        albedoDiffUpland(ind1) = shallow_snow_albedo(snowAlbedoDiff(ind1),...
            albedoDiffUpland(ind1),snowThickUpland(ind1),snowGndAlbedoExtDep);
        albedoBeamUpland(ind1) = shallow_snow_albedo(snowAlbedoBeam(ind1),...
            albedoBeamUpland(ind1),snowThickUpland(ind1),snowGndAlbedoExtDep);
    end
    ind1 = ~ind1 & (indUpland);
    if any(ind1)
        albedoDiffUpland(ind1) = snowAlbedoDiff(ind1);
        albedoBeamUpland(ind1) = snowAlbedoBeam(ind1);
    end
end

%--------------------------------------------------------------------------
%                             Wetland Snow Albedo 
%--------------------------------------------------------------------------
ind = snowCoveredWetland;
if any(ind)
    [snowAlbedoDiff,snowAlbedoBeam] = deal(zerosGrid);
      
    %Calculate the new snow surface albedo
    [snowAlbedoDiff(ind),snowAlbedoBeam(ind)] = bats_albedo(wetlandSnowSurfAge(ind),coszen(ind),albedoNewSnowVis,albedoNewSnowNIR);

    ind1 = (snowThickWetland < snowGndAlbedoExtDep);
    if any(ind1)
        %Calculate the net snowpack albedo that takes into account exponential
        %light extinction with depth
        albedoDiffWetland(ind1) = shallow_snow_albedo(snowAlbedoDiff(ind1),...
            albedoDiffWetland(ind1),snowThickWetland(ind1),snowGndAlbedoExtDep);
        albedoBeamWetland(ind1) = shallow_snow_albedo(snowAlbedoBeam(ind1),...
            albedoBeamWetland(ind1),snowThickWetland(ind1),snowGndAlbedoExtDep);
    end
    ind1 = ~ind1 & (indWetland);
    if any(ind1)
        albedoDiffWetland(ind1) = snowAlbedoDiff(ind1);
        albedoBeamWetland(ind1) = snowAlbedoBeam(ind1);
    end
end

%--------------------------------------------------------------------------
%update the values stored in the data controller
%--------------------------------------------------------------------------
state = update_struct(state,'albedo_canopy_diff',albedoCanopyDiff,'albedo_canopy_beam',albedoCanopyBeam,...
    'albedo_upland_diff',albedoDiffUpland,'albedo_upland_beam',albedoBeamUpland,...
    'albedo_wetland_diff',albedoDiffWetland,'albedo_wetland_beam',albedoBeamWetland);

end

%--------------------------------------------------------------------------
%                             Additional Functions 
%--------------------------------------------------------------------------

function [albedo] = shallow_snow_albedo(albedoSnow,albedoGround,snowThick,lightExtinctDepth)
%this is from the UEB model as well
r = (1 - snowThick/lightExtinctDepth) .* exp(-snowThick / (2*lightExtinctDepth));
albedo = albedoSnow .* (1 - r) + albedoGround .* r;
end


function [diffuse,beam] = bats_albedo(age,coszen,av0,anir0)
%This is the BATS albedo model from the UEB snow model
%Specify BATS constants
b=2;
cs=0.2;
cn=0.5;

ind=(coszen>0.5);
fzen=zeros(size(age));
fzen(ind)=1/b.*((b+1)./(1+2*b*coszen(ind))-1);

fage=age./(1+age);

avd=(1-cs*fage)*av0;
avis=avd+0.4*fzen.*(1-avd);
anird=(1-cn*fage)*anir0;
anir=anird+0.4*fzen.*(1-anird);

diffuse = weighted_mean('arithmetic',avd,anird,0.45,0.55);
beam = weighted_mean('arithmetic',avis,anir,0.45,0.55);
end

