function roughness = wetland_surface_roughness(params,windspeed,resMomentum)
%WETLAND_SURFACE_ROUGHNESS  Calculates surface roughness of wetlands.
%   roughness = wetland_surface_roughness(windspeed,resMomentum)
%
%   This formulation combines elements from the CLM 3.0 manual, as well as
%   from Donelan et al. 1993
%
%   Descriptions of Input Variables:
%   windspeed:  wind speed in m/s
%   resMomentum:resistance to momentum transport (s/m)
%
%   Descriptions of Output Variables:
%   roughness:  sea surface roughness in m
%
%   Example(s):
%   >> wetland_surface_roughness
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-22
% Copyright 2008 Michigan State University.

[alpha,gravConst] = deal_struct(params,'wetland_roughness_alpha','grav_constant');

roughness = alpha / gravConst * (windspeed ./ resMomentum);