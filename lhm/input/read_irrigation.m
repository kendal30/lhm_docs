function [structure,buffers,readFlagTech,readFlagLim] = read_irrigation(structure,buffers,date)
%READ_IRRIGATION Reads time-varying (yearly) irrigation from HDF5 file.
%   readFlag = read_irrigation(date)
%
%   Descriptions of Input Variables:
%   date:   MATLAB serial datenumber of the current timestep
%
%   Descriptions of Output Variables:
%   readFlag: a logical value indicating whether a new irrigation dataset was
%           read in
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

%Fetch variables from the data controller
[irrigTechBuffer,irrigLimBuffer] = deal_struct(buffers,'irrigation_tech','irrigation_limits');
[irrigTechIndex,irrigLimIndex,paths] = deal_struct(structure,'irrigation_tech_index','irrigation_limits_index','paths');

%--------------------------------------------------------------------------
%Irrigation Tech: Match the Date with the Stored Index to get the Data Row
%--------------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = irrigTechBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest year
dateVector = datevec(date);
test = all(dateVector(1) == irrigTechIndex.index(currIndexRow,1));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(irrigTechIndex.index(:,1), dateVector(1),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),'The current date was not found in the index of irrigation');
end
%Re-assign the prevRow variable
irrigTechBuffer.prevIndexRow = currIndexRow;

%--------------------------------------------------------------------------
%Read in the data, if necessary (in nearly all cases, this will not be
%necessary since this dataset changes so slowly)
%--------------------------------------------------------------------------
%Get the data row from the index
dataRow = irrigTechIndex.index(currIndexRow,5);

%Determine if a new dataset must be read
readFlagTech = (dataRow ~= irrigTechBuffer.dataRow);

if readFlagTech
    if isfield(structure,'irrigation');
        irrigation = deal_struct(structure,'irrigation');
    else %to initialize the irrigation structure
        irrigation = struct();
    end
    
    %Read in the weights grid
    tempWeights = h5dataread(paths.input.landscape,'/irrigation_technology/weights',true,...
        [dataRow-1,0,0],[1,irrigTechIndex.weightsSize(2),irrigTechIndex.weightsSize(3)],[1,1,1]);
    irrigation.weightsArray = zeros(size(irrigTechBuffer.lookup,1),irrigTechIndex.weightsSize(3));
    for m = 1:irrigTechIndex.weightsSize(3)
        loopName = irrigTechBuffer.weightNames{m};
        loopName = strrep(loopName,'/','_');
        loopWeight = tempWeights(1,:,m);
        irrigation.weights.(loopName) = loopWeight(irrigTechBuffer.lookup)'; 
        irrigation.weightsArray(:,m) = irrigation.weights.(loopName);
    end
    
    %Add the names to the irrigation array
    irrigation.weightNames = irrigTechBuffer.weightNames;
    
    %Update the data buffer
    irrigTechBuffer.dataRow = dataRow;
    
    %Update the data controller
    structure = update_struct(structure,'irrigation',irrigation);
    buffers = update_struct(buffers,'irrigation_tech',irrigTechBuffer);
end

%--------------------------------------------------------------------------
%Irrigation Limitation: Match the Date with the Stored Index to get the Data Row
%--------------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = irrigLimBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest year
dateVector = datevec(date);
test = all(dateVector(1) == irrigLimIndex.index(currIndexRow,1));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(irrigLimIndex.index(:,1), dateVector(1),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),'The current date was not found in the index of irrigation');
end
%Re-assign the prevRow variable
irrigLimBuffer.prevIndexRow = currIndexRow;

%--------------------------------------------------------------------------
%Read in the data, if necessary (in nearly all cases, this will not be
%necessary since this dataset changes so slowly)
%--------------------------------------------------------------------------
%Get the data row from the index
dataRow = irrigLimIndex.index(currIndexRow,5);

%Determine if a new dataset must be read
readFlagLim = (dataRow ~= irrigLimBuffer.dataRow);

if readFlagLim
    irrigation = deal_struct(structure,'irrigation');
    
    %Read in the limitation grid
    tempLimitation = h5dataread(paths.input.landscape,'/irrigation_limits/data',true,...
        [dataRow-1,0],[1,irrigLimIndex.limitationSize(2)],[1,1]);
    irrigation.limitation = tempLimitation(irrigLimBuffer.lookup)'; 
    
    %Update the data buffer
    irrigLimBuffer.dataRow = dataRow;
    
    %Update the data controller
    structure = update_struct(structure,'irrigation',irrigation);
    buffers = update_struct(buffers,'irrigation_limits',irrigLimBuffer);
end

end
