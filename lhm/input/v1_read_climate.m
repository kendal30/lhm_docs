function [state,buffers] = v1_read_climate(state,structure,params,buffers,date)
%READ_CLIMATE  Reads hourly climate from HDF5 file.
%   read_climate(date)
%
%   Reads the next hour of climate inputs from the HDF5 storage and outputs
%   distributed grids.
%
%   Descriptions of Input Variables:
%   none, reads all necessary inputs from the data controller
%
%   Descriptions of Output Variables:
%   none, writes all necessary outputs to the data controller.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

[climBuffer] = deal_struct(buffers,'climate');
[paths,climIndex,trimStruct,numInnerSteps,zerosGrid] = deal_struct(structure,...
    'paths','climate_index','trim_struct','num_inner_timesteps','zeros_grid');

%Specify list of variables to read in, and the corresponding output names
varnames = struct('in',{'windspeed','airTemp','airVapPress',...
    'radBeam','radDiff','sunZenithCos','radLong','pressure',...
    'rain','snow','soilTempShallow','soilTempDeep','',''},...
    'calc',{'','','','','','','','','','','','','precip','densMoistAir'},...
    'out',{'windspeed','temp_air','vap_pressure_air',...
    'rad_short_beam_in','rad_short_diff_in','sun_zenith_cos','rad_long_in','pressure',...
    'rain','snow','temp_soil_shallow','temp_soil_deep','precip','dens_moist_air'});

%Initialize variables
data = struct();
noRain = false;
noSnow = false;

%Loop through the list of variable names
for m = 1:length(varnames)
    if ~isempty(varnames(m).in)
        %For convenience
        loopVar = varnames(m).in;
        loopIndex = climIndex.(loopVar);
        loopBuffer = climBuffer.(loopVar);

        %------------------------------------------------------------------
        %Match the Date with the Stored Index to get the Data Row
        %------------------------------------------------------------------
        %Check to see if the next row in the index matches the current date
        currIndexRow = loopBuffer.prevIndexRow + 1;

        %Check to see if the current date is equal to the index date, to
        %the nearest hour
        dateVector = datevec(date);
        test = all(dateVector(1:4) == loopIndex.index(currIndexRow,1:4));

        %If this guess of the correct row is wrong, then find the correct
        %row in the index, this is slow, but luckily it shouldn't happen
        %often
        if ~test
            %Find the correct row
            currIndexRow = find(ismember(loopIndex.index(:,1:4), dateVector(1:4),'rows'),1,'first');
            assert_LHM(~isempty(currIndexRow),['The current date was not found in the index of ',varnames(m).in]);
        end
        %Re-assign the prevRow variable
        loopBuffer.prevIndexRow = currIndexRow;

        %------------------------------------------------------------------
        %Test to see if data needs to be read in for this timestep
        %------------------------------------------------------------------
        dataRow = loopIndex.index(currIndexRow,5);
        if dataRow == -1 || dataRow == -99999
            %This flag indicates values are all zeros
            %Actually, -99999 indicates that the value is unavailable,
            %which can only occur for input fluxes
            tempData = zerosGrid;
            if strcmp(loopVar,'rain');noRain = true;end
            if strcmp(loopVar,'snow');noSnow = true;end
        elseif dataRow > 0
            %--------------------------------------------------------------
            %Lookup Sheet Handling
            %--------------------------------------------------------------
            %Determine the sheet of the lookup table to use
            lookupSheet = loopIndex.index(currIndexRow,6);

            %Check to see if the lookup table needs to be read in, or if
            %the stored one is the same as the current
            readSheet = ~(loopBuffer.lookupSheet == lookupSheet);

            if readSheet
                %Read in the lookup table
                lookup = h5dataread(paths.input.climate,['/',loopVar,'/lookup'],...
                    false,[0,0,lookupSheet-1],[loopIndex.lookupSize(1),loopIndex.lookupSize(2),1],[1,1,1]);
                %Trim the lookup table to the model domain
                size(lookup);
                lookup = trim_domain(trimStruct,lookup);

                %Save this lookup sheet to the climBuffer
                loopBuffer.lookup = lookup;
                loopBuffer.lookupSheet = lookupSheet;
            else
                lookup = loopBuffer.lookup;
            end

            %--------------------------------------------------------------
            %Data Buffer Handling
            %--------------------------------------------------------------
            %Determine if the data row is contained within the rows in the
            %buffer
            buffRow = find(ismember(loopBuffer.dataRows,dataRow),1,'first');
            %If not, read in a new buffer starting at the data row
            if isempty(buffRow)
                %Determine the number of rows to read in
                numRows = min(loopIndex.dataSize(1) - dataRow + 1, loopBuffer.bufferLength);
                %Read in the new rows
                loopBuffer.dataRows = (dataRow : dataRow + numRows - 1);
                loopBuffer.data = h5dataread(paths.input.climate,...
                    ['/',loopVar,'/data'],true,[dataRow-1,0],...
                    [numRows,loopIndex.dataSize(2)],[1,1]);
                %Update the buffRow
                buffRow = 1;
            end

            %Now, from the buffer, pull a single row for the tempData
            tempData = loopBuffer.data(buffRow,:)';

            %Use the lookup table to create the data grid from tempData
            tempData = tempData(lookup);
        else
            error_LHM(['An invalid data row value was found in the index table of ',loopVar]);
        end

        %Save tempData to the climIndex data struct
        data.(loopVar) = tempData;

        %Update the buffer with the loopBuffer
        climBuffer.(loopVar) = loopBuffer;

        %For air pressure
        if strcmp(loopVar,'pressure')
            %Calculate nearest 24 hour pressure range
            maxRow = size(loopBuffer.data,1);
            if buffRow <= numInnerSteps
                rangeRowStart = 1;
                rangeRowEnd = numInnerSteps;
            elseif buffRow > (maxRow - numInnerSteps);
                rangeRowStart = maxRow - numInnerSteps + 1;
                rangeRowEnd = maxRow;
            else
                rangeRowStart = buffRow - numInnerSteps + 1;
                rangeRowEnd = buffRow;
            end

            %Calculate the daily pressure range, for now just calculate it from the
            %range of the buffered data
            pressureRange = range(loopBuffer.data(rangeRowStart:rangeRowEnd,:),1)';
            state.pressure_range = pressureRange(lookup);
        end
    end
end

% Rescale from m/hr to m/timestep for snow and rain
data.rain = data.rain * timestepLength/3600;
data.snow = data.snow * timestepLength/3600;

%Calculate secondary quantities from these
if noSnow && noRain
    data.precip = zerosGrid;
else
    data.precip = data.rain + data.snow;
end
data.densMoistAir = moist_air_density(params,data.airTemp,data.airVapPress,data.pressure);

%Now, write these out to the data_controller
for m = 1:length(varnames)
    if ~isempty(varnames(m).out)
        if ~isempty(varnames(m).in)
            state = update_struct(state,varnames(m).out,data.(varnames(m).in));
        elseif ~isempty(varnames(m).calc)
            state = update_struct(state,varnames(m).out,data.(varnames(m).calc));
        end
    end
end

%Update the data_controller with the buffer
buffers = update_struct(buffers,'climate',climBuffer);
end
