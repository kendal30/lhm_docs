function [structure,buffers,readFlag] = read_impervious(structure,buffers,date)
%READ_IMPERVIOUS  Reads time-varying (yearly) impervious surfaces from HDF5 file.
%   readFlag = read_impervious(date)
%
%   Descriptions of Input Variables:
%   date:   MATLAB serial datenumber of the current timestep
%
%   Descriptions of Output Variables:
%   readFlag: a logical value indicating whether a new impervious dataset was
%           read in
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

%Fetch variables from the data controller
[impervBuffer] = deal_struct(buffers,'impervious');
[impervIndex,paths] = deal_struct(structure,'impervious_index','paths');

%--------------------------------------------------------------------------
%Match the Date with the Stored Index to get the Data Row
%--------------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = impervBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest year
dateVector = datevec(date);
test = all(dateVector(1) == impervIndex.index(currIndexRow,1));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(impervIndex.index(:,1), dateVector(1),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),'The current date was not found in the index of impervious');
end
%Re-assign the prevRow variable
impervBuffer.prevIndexRow = currIndexRow;

%--------------------------------------------------------------------------
%Read in the data, if necessary (in nearly all cases, this will not be
%necessary since this dataset changes so slowly)
%--------------------------------------------------------------------------
%Get the data row from the index
dataRow = impervIndex.index(currIndexRow,5);

%Determine if a new dataset must be read
readFlag = (dataRow ~= impervBuffer.dataRow);

if readFlag
    impervFrac = struct();
    
    %Read in the weights grid
    tempWeights = h5dataread(paths.input.landscape,'/impervious/weights',true,...
        [dataRow-1,0,0],[1,impervIndex.weightsSize(2),impervIndex.weightsSize(3)],[1,1,1]);
    for m = 1:impervIndex.weightsSize(3)
        loopName = impervBuffer.weightNames{m};
        loopWeight = tempWeights(1,:,m);
        impervFrac.(loopName) = loopWeight(impervBuffer.lookup)'; 
    end
    
    %Update the data buffer
    impervBuffer.dataRow = dataRow;
    
    %Update the data controller
    structure = update_struct(structure,'fraction_impervious',impervFrac);
    buffers = update_struct(buffers,'impervious',impervBuffer);
end
end
