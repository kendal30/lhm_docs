function [state,structure,buffers,readFlag] = read_lai(state,structure,buffers,date)
%READ_LAI  Reads time-varying (daily) LAI from HDF5 storage.
%   readFlag = read_lai(date)
%
%   Descriptions of Input Variables:
%   date:   MATLAB serial datenumber of the current timestep
%
%   Descriptions of Output Variables:
%   readFlag: a logical value indicating whether a new LAI dataset was
%           read in
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

%Fetch variables from the data controller
[laiBuffer] = deal_struct(buffers,'lai');
[laiIndex,paths,irrigActive,zerosGrid] = deal_struct(structure,'lai_index','paths','irrigation_enabled','zeros_grid');

%--------------------------------------------------------------------------
%Match the Date with the Stored Index to get the Data Row
%--------------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = laiBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest day
dateVector = datevec(date);
test = all(dateVector(1:3) == laiIndex.index(currIndexRow,1:3));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(laiIndex.index(:,1:3), dateVector(1:3),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),'The current date was not found in the index of LAI');
end
%Re-assign the prevRow variable
laiBuffer.prevIndexRow = currIndexRow;

%--------------------------------------------------------------------------
%Read in the data, if necessary, for the two grids
%--------------------------------------------------------------------------
%Determine the data rows and weights
thisRow = laiIndex.index(currIndexRow,5:8);
[dataRow1,dataRow2,weight1,weight2] = deal(thisRow(1),thisRow(2),thisRow(3),thisRow(4));

weight1 = double(weight1);
weight2 = double(weight2);

%Fetch the data, either from the buffer or from disk
[lai1,laiIrrig1,laiBuffer,readFlag1] = fetch_lai(dataRow1,laiBuffer,laiIndex,paths,irrigActive);
[lai2,laiIrrig2,laiBuffer,readFlag2] = fetch_lai(dataRow2,laiBuffer,laiIndex,paths,irrigActive);

%Write the read flag to return to the calling function
readFlag = readFlag1 || readFlag2;

%Now, interpolate between the two datasets, by taking the weighted average
%Direct calculation is simple, and faster than using function
lai = (lai1 * weight1 + lai2 * weight2) / (weight1 + weight2);
% lai = weighted_mean('arithmetic',lai1,lai2,weight1,weight2);

if irrigActive
    laiIrrig = (laiIrrig1 * weight1 + laiIrrig2 * weight2) / (weight1 + weight2);
else
    laiIrrig = zerosGrid;
end

%Convert all NaN values to 0
lai(isnan(lai)) = 0;

%Update the data_controller
state = update_struct(state,'lai',lai,'lai_irrig',laiIrrig);
buffers = update_struct(buffers,'lai',laiBuffer);

end

function [laiGrid,laiGridIrrig,laiBuffer,readFlag] = fetch_lai(dataRow,laiBuffer,laiIndex,paths,irrigActive)
if dataRow > 0
    %--------------------------------------------------------------
    %Data Buffer Handling
    %--------------------------------------------------------------
    %Determine if the data row is contained within the rows in the
    %buffer
    buffRow = find(ismember(laiBuffer.dataRows,dataRow),1,'first');
    
    %If not, read in a new buffer starting at the data row
    if isempty(buffRow)
        %Determine the number of rows to read in
        numRows = min(laiIndex.dataSize(1) - dataRow + 1, laiBuffer.bufferLength);
        %Read in the new rows
        laiBuffer.dataRows = (dataRow : dataRow + numRows - 1);
        laiBuffer.data = h5dataread(paths.input.landscape,...
            '/lai/data',true,[dataRow-1,0],...
            [numRows,laiIndex.dataSize(2)],[1,1]);
        if irrigActive
            laiBuffer.dataIrrig = h5dataread(paths.input.landscape,...
                '/laiIrrig/data',true,[dataRow-1,0],...
                [numRows,laiIndex.dataSize(2)],[1,1]);
        end
        %Update the buffRow
        buffRow = 1;
    end
    
    %Now, from the buffer, pull a single row and lookup
    laiGrid = laiBuffer.data(buffRow,:)';
    laiGrid = laiGrid(laiBuffer.lookup);
    
    if irrigActive
        %Repeat for laiGridIrrig, from the buffer, pull a single row and lookup
        laiGridIrrig = laiBuffer.dataIrrig(buffRow,:)';
        laiGridIrrig = laiGridIrrig(laiBuffer.lookup);
    else
        laiGridIrrig = false;
    end
    
    %Set the readFlag
    readFlag = true;
else
    error_LHM('An invalid data row value was found in the index table of lai');
end
end
