function [structure,buffers,readFlag] = read_landuse(structure,buffers,date)
%READ_LANDUSE Reads time-varying (yearly) landuse from HDF5 file.
%   readFlag = read_landuse(date)
%
%   Descriptions of Input Variables:
%   date:   MATLAB serial datenumber of the current timestep
%
%   Descriptions of Output Variables:
%   readFlag: a logical value indicating whether a new LAI dataset was
%           read in
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

%Fetch variables from the data controller
[luBuffer] = deal_struct(buffers,'land_use');
[luIndex,paths] = deal_struct(structure,'land_use_index','paths');

%--------------------------------------------------------------------------
%Match the Date with the Stored Index to get the Data Row
%--------------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = luBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest year
dateVector = datevec(date);
test = all(dateVector(1) == luIndex.index(currIndexRow,1));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(luIndex.index(:,1), dateVector(1),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),'The current date was not found in the index of land use');
end
%Re-assign the prevRow variable
luBuffer.prevIndexRow = currIndexRow;

%--------------------------------------------------------------------------
%Read in the data, if necessary (in nearly all cases, this will not be
%necessary since this dataset changes so slowly)
%--------------------------------------------------------------------------
%Get the data row from the index
dataRow = luIndex.index(currIndexRow,5);

%Determine if a new dataset must be read
readFlag = (dataRow ~= luBuffer.dataRow);

if readFlag
    landuse = struct();
    %Read in the majority grid
    tempMajority = h5dataread(paths.input.landscape,'/landuse/majority',true,...
        [dataRow-1,0],[1,luIndex.majoritySize(2)],[1,1]);
    landuse.majority = tempMajority(luBuffer.lookup)'; 
    
    %Read in the weights grid
    tempWeights = h5dataread(paths.input.landscape,'/landuse/weights',true,...
        [dataRow-1,0,0],[1,luIndex.weightsSize(2),luIndex.weightsSize(3)],[1,1,1]);
    landuse.weightsArray = zeros(size(luBuffer.lookup,1),luIndex.weightsSize(3));
    for m = 1:luIndex.weightsSize(3)
        loopName = luBuffer.weightNames{m};
        loopWeight = tempWeights(1,:,m);
        landuse.weights.(loopName) = loopWeight(luBuffer.lookup)'; 
        landuse.weightsArray(:,m) = landuse.weights.(loopName);
    end
    sumWeights = sum(landuse.weightsArray,2);
    
    %Renormalize to 1
    for m = 1:luIndex.weightsSize(3)
        loopName = luBuffer.weightNames{m};
        landuse.weights.(loopName) = landuse.weights.(loopName) ./ sumWeights;
        landuse.weightsArray(:,m) = landuse.weights.(loopName);
    end
    
    %Save out the landuse names
    landuse.weightNames = luBuffer.weightNames;
    
    %Update the data buffer
    luBuffer.dataRow = dataRow;
    
    %Update the data controller
    structure = update_struct(structure,'land_use',landuse);
    buffers = update_struct(buffers,'land_use',luBuffer);
end
end
