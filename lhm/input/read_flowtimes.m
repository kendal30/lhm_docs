function [structure,buffers,readFlag] = read_flowtimes(structure,buffers,date)
%READ_FLOWTIMES Reads time-varying (yearly) flowtimes from HDF5 file.
%   readFlag = read_flowtimes(date)
%
%   Descriptions of Input Variables:
%   date:   MATLAB serial datenumber of the current timestep
%
%   Descriptions of Output Variables:
%   readFlag: a logical value indicating whether a new flowtimes dataset was
%           read in
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2018-06-12
% Copyright 2018 Michigan State University.

%Fetch variables from the data controller
[ftBuffer] = deal_struct(buffers,'flowtimes');
[ftIndex,paths] = deal_struct(structure,'flowtimes_index','paths');

%--------------------------------------------------------------------------
%Match the Date with the Stored Index to get the Data Row
%--------------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = ftBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest year
dateVector = datevec(date);
test = all(dateVector(1) == ftIndex.index(currIndexRow,1));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(ftIndex.index(:,1), dateVector(1),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),'The current date was not found in the index of flowtimes');
end
%Re-assign the prevRow variable
ftBuffer.prevIndexRow = currIndexRow;

%--------------------------------------------------------------------------
%Read in the data, if necessary (in nearly all cases, this will not be
%necessary since this dataset changes so slowly)
%--------------------------------------------------------------------------
%Get the data row from the index
dataRow = ftIndex.index(currIndexRow,5);

%Determine if a new dataset must be read
readFlag = (dataRow ~= ftBuffer.dataRow);

if readFlag
    %Read in the flowtimes grid
    tempFlowtimes = h5dataread(paths.input.landscape,'/flowtimes/data',true,...
        [dataRow-1,0],[1,ftIndex.flowtimesSize(2)],[1,1]);
    flowtimes = tempFlowtimes(ftBuffer.lookup)'; 
        
    %Update the data buffer
    ftBuffer.dataRow = dataRow;
    
    %Update the data controller
    structure = update_struct(structure,'runoff_flowtimes',flowtimes);
    buffers = update_struct(buffers,'flowtimes',ftBuffer);
end
end
