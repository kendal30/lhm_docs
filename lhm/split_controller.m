function [modules] = split_controller(structure,modules,joinFlag)
% This function controls the creation or stitching together of model splits

if ~joinFlag %this is the start run
    % List the split init files
    initFiles = dir('**/split*INIT*.mat');

    for m = 1:length(initFiles)
        cd(structure.paths.source); % Change to the source code directory if not already there
        fprintf('Spawning split %d out of %d\n',m,structure.num_splits);
        thisInit = [initFiles(m).folder,filesep,initFiles(m).name];
        initiate_str = '!matlab -nosplash -minimize -r "lhm_controller(''start'',''%s'',''%s'')"&';
        eval(sprintf(initiate_str,thisInit,structure.paths.input.dir));
    end

    % Turn the diary off
    diary off

    % Exit this MATLAB instance
    exit force % to bypass any quit script

elseif joinFlag
    % List the split start files
    completeSplits = dir('**/model_complete.txt');
    
    if length(completeSplits) < structure.num_splits
        % Not all of the models have finished, quite this one
        exit force % to bypass any quit script
    end

    % Change the surface controller to blank for the join functionality, this allows groundwater to continue
    modules.mod_surface_controller = str2func('blank');
    
    % Get the indices of each split
    splitInd = cell(4,1);
    for m = 1:structure.num_splits
        splitInd{m} = structure.split_slice==m;
    end
    
    %----------------------------------------------------------------------
    % Join OUTF
    %----------------------------------------------------------------------   
    % For each group containing data, join them, different approach for
    % each.
    % Grid first
    disp('Joining Grid Outputs')
    
    % Get the list of files and file structure
    [fileInfo,joinFiles,outFile] = list_OUTF_files(structure);
    
    % Join them
    join_OUTF_grid(fileInfo,splitInd,outFile,joinFiles);
    
    % Zones ---------------------------------------------------------------
    disp('Zone joining is not yet supported')
    
    % Points --------------------------------------------------------------
    disp('Point joining is not yet supported')
    
    %----------------------------------------------------------------------
    % PERC, TRANSP, EVAP files
    %----------------------------------------------------------------------
    types = {'PERC','EVAP','TRANSP'};
    disp('Joining groundwater coupling files');
    for m = 1:length(types)
        type = types{m};
    
        % Get the list of files and file structure
        [fileInfo,joinFiles,outFile] = list_groundwater_files(structure,type);
        
        % Join them
        join_groundwater_files(fileInfo,splitInd,outFile,joinFiles,type);
    end
end

end


%----------------------------------------------------------------------
% Join functions for the OUTF files
%----------------------------------------------------------------------
function [fileInfo,joinFiles,outFile] = list_OUTF_files(structure)
% First, stitch together the OUTF files
listOUTF = dir('**/split*OUTF*.h5');
joinFiles = cell(structure.num_splits,1);
for m = 1:structure.num_splits
    joinFiles{m} = [listOUTF(m).folder,filesep,listOUTF(m).name];
end

% Get the info of the output file
outFile = structure.paths.output.output;
fileInfo = h5info(outFile);
end


function join_OUTF_grid(fileInfo,splitInd,outFile,joinFiles)
thisType = 'grid';
typeInd = find(strcmp(['/',thisType],{fileInfo.Groups(:).Name}));

% Get the different grid names
gridNames = {fileInfo.Groups(typeInd).Groups(:).Name};
    
for m = 1:length(gridNames) % For each grid
    h = waitbar(0,sprintf('Joining grid set ''%s''',gridNames{m}));
    
    % Get the dataset names
    datasetNames = {fileInfo.Groups(typeInd).Groups(m).Groups.Groups(:).Name};
    
    % Get the stats for this grid
    statNames = {fileInfo.Groups(typeInd).Groups(m).Groups.Groups(1).Datasets(:).Name};
    
    % Get the number of output intervals, have to flip the size because
    % they are oppositely ordered in the HDF5 file
    intervalInd = find(strcmp('interval',{fileInfo.Groups(typeInd).Groups(m).Datasets(:).Name}));
    sizeIntervals = fliplr(fileInfo.Groups(typeInd).Groups(m).Datasets(intervalInd).Dataspace.Size); %#ok<*FNDSB>
    numIntervals = sizeIntervals(1);
    
    for n = 1:length(datasetNames) % For each dataset
        waitbar(n/length(datasetNames),h);
        
        % Get the size of the output, will be for one row (time slice)
        % only
        thisSize = fliplr(fileInfo.Groups(typeInd).Groups(m).Groups.Groups(n).Datasets(1).Dataspace.Size);
        thisSize(1) = numIntervals;
        
        for o = 1:length(statNames) % For each statistic
            % Get the full path to this statistic
            thisStat = [datasetNames{n},'/',statNames{o}];
            
            % Get the dataset attributes
            [~,thisAttribs] = h5dataread(outFile,thisStat,false);
            
            % Create a zeros array that is single precision
            thisJoinDataset = zeros(thisSize,thisAttribs.class);
            
            for p = 1:length(joinFiles) % For each split
                % Read in this dataset
                datasetInd = find(contains(joinFiles,sprintf('split_%d_OUTF',p)));
                thisDataset = h5dataread(joinFiles{datasetInd},thisStat,false);
                
                if size(thisDataset,1) > 1 % if the data were never written, this can happen
                    % Add this to the overall dataset
                    if ndims(thisDataset)==2 %#ok<*ISMAT>
                        thisJoinDataset(:,splitInd{p}) = thisDataset;
                    else
                        thisJoinDataset(:,splitInd{p},:) = thisDataset;
                    end
                end
            end % splits
            
            % Write to the join file
            if size(thisDataset,1)>1
                h5datawrite(outFile,thisStat,thisJoinDataset,thisAttribs,false,...
                    zeros(1,ndims(thisDataset)),thisSize);
            end
            
        end % stats
    end % datasets
    if ishandle(h);close(h);end
end %grids

end


%----------------------------------------------------------------------
% Join functions for the PERC, TRANSP, EVAP files
%----------------------------------------------------------------------
function [fileInfo,joinFiles,outFile] = list_groundwater_files(structure,type)
% First, stitch together the OUTF files
listFiles = dir(sprintf('**/split*%s*.h5',type));
joinFiles = cell(structure.num_splits,1);
for m = 1:structure.num_splits
    joinFiles{m} = [listFiles(m).folder,filesep,listFiles(m).name];
end

% Get the info of the output file
outFile = structure.paths.output.(lower(type));
fileInfo = h5info(outFile);
end


function join_groundwater_files(fileInfo,splitInd,outFile,joinFiles,type)
% Get the name of this dataset
datasetName = fileInfo.Groups.Name(2:end);

% Get the intervals from the main file
outIndex = h5dataread(outFile,sprintf('/%s/index',datasetName));

% Read in the dataset as-is to get attributes and sizes
readGroup = sprintf('/%s/data',datasetName);
[thisTemplate,thisAttribs] = h5dataread(outFile,readGroup,false);
            
% Split into years
[years,~,~] = datevec(outIndex(:,1));
uniqueYears = unique(years);

% Read in one year at a time for each split file
h = waitbar(0,sprintf('Joining %s files',type));
for m = 1:length(uniqueYears)
    waitbar(m/length(uniqueYears),h);
    
    thisYear = uniqueYears(m);
    thisDates = ismember_dates(years,thisYear,3);
    startRow = find(thisDates,1,'first');
    
    % Make a zeros dataset of the right class
    thisSize = [sum(thisDates),size(thisTemplate,2)];
    thisJoinDataset = zeros(thisSize,thisAttribs.class);
    
    % Now, join the files
    for n = 1:length(joinFiles) 
        datasetInd = find(contains(joinFiles,sprintf('split_%d_%s',n,type)));
        
        % Get the index for this file
        splitIndex = h5dataread(joinFiles{datasetInd},sprintf('/%s/index',datasetName));
        [splitYears,~,~] = datevec(splitIndex(:,1));
        splitDates = ismember_dates(splitYears,thisYear,3);
        splitSize = [sum(splitDates),sum(splitInd{n})];
        startSplitRow = find(splitDates,1,'first');
                
        thisDataset = h5dataread(joinFiles{datasetInd},readGroup,false,...
            [startSplitRow-1,0],splitSize);
        
        if size(thisDataset,1) > 1 % if the data were never written, this can happen
            % Add this to the overall dataset
            thisJoinDataset(:,splitInd{n}) = thisDataset;
        end
    end
    
    % Write to the join file
    if size(thisDataset,1)>1
        h5datawrite(outFile,readGroup,thisJoinDataset,thisAttribs,false,...
            [startRow-1,0],thisSize);
    end
end
if ishandle(h);close(h);end
end
