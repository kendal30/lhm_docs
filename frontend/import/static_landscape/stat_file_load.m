function stat_file_load(dirInfo,fileInfo,readGroups)
% This function is called by the static_import_controller

% Build the output filename
staticName = fileInfo{1};
staticVersion = fileInfo{2};
IDType = 'STAT'; % shouldn't change
compileDate = now; % only change if needed
staticName = strrep(staticName,'_','-');
staticVersion = strrep(staticVersion,'_','-');
outFilename = [IDType,'_',staticName,'_',staticVersion,'_',datestr(compileDate,'yyyymmdd'),'.h5'];

% Parse out input directories
baseDir = dirInfo{1};
nameSubmodel = dirInfo{2};
workingDir = [baseDir,filesep,'Import_Working'];
outFile = [workingDir,filesep,outFilename];
if islogical(nameSubmodel)
    inputDir1 = [baseDir,filesep,'Model_Layers',filesep,'Surface'];
    inputDir2 = [baseDir,filesep,'Model_Layers',filesep,'Groundwater'];
else
    inputDir1 = [baseDir,filesep,'Submodel_Layers',filesep,nameSubmodel,filesep,'Surface'];
    inputDir2 = [baseDir,filesep,'Submodel_Layers',filesep,nameSubmodel,filesep,'Groundwater'];
end
    
% Check to see that the input directories exist
assert(exist(workingDir,'dir')==7,'Working directory does not exist');
assert(exist(inputDir1,'dir')==7,'Surface Model Layers directory does not exist');
assert(exist(inputDir2,'dir')==7,'Groundwater Model Layers directory does not exist');

% Check to make sure that the output file is specified correctly
if exist(outFile,'file') > 0
    error('Output file already exists, specify a new file name');
end

% Determine if a slice array should be read
loadSlice = false;
if exist([inputDir1,filesep,'slice.tif'],'file')>0
    loadSlice = true;
end

%--------------------------------------------------------------------------
% Load all grids and save
%--------------------------------------------------------------------------
% Specify nanFill as true, this will fill values with either a specific
% `nanFillVal` or the mean of the dataset
nanFill = true;

sources = struct();
% Model geometry
sources.structure.model_grid = struct('path',inputDir1,'nanFillVal',0);
if loadSlice 
    sources.structure.slice = struct('path',inputDir1,'nanFillVal',0);
end
sources.structure.latitude = struct('path',inputDir1);
sources.structure.longitude = struct('path',inputDir1);

% Wetlands and streams structure
% Here, nanFillVal specifies what value to use for NaNs, otherwise if not 
% specified, then the mean/median will be used as appropriate
sources.structure.wetland_bottom_depth = struct('path',inputDir1,'nanFillVal',0);
sources.structure.wetland_complex = struct('path',inputDir1,'nanFillVal',0);
sources.structure.stream_bottom_depth = struct('path',inputDir1,'nanFillVal',0);
sources.structure.fraction_wetland = struct('path',inputDir1,'nanFillVal',0);
sources.structure.fraction_stream = struct('path',inputDir1,'nanFillVal',0);
sources.structure.index_internally_drained = struct('path',inputDir1,'nanFillVal',0);

% Elevation
sources.topography.slope = struct('path',inputDir1);
sources.topography.aspect = struct('path',inputDir1);
sources.topography.elevation_min = struct('path',inputDir1);
sources.topography.elevation_mean = struct('path',inputDir1);
sources.topography.elevation_up = struct('path',inputDir1);
sources.topography.elevation_wet = struct('path',inputDir1);

% Runoff model
sources.runoff.stream_velocity = struct('path',inputDir1,'nanFillVal',0);
sources.runoff.runoff_gauges = struct('path',inputDir1,'nanFillVal',0);
sources.runoff.runoff_downstream_watershed = struct('path',inputDir1,'nanFillVal',0);
sources.runoff.runoff_watershed = struct('path',inputDir1,'nanFillVal',0);
sources.runoff.runoff_overland_flowlength = struct('path',inputDir1); % for the new sediment yield code 
sources.runoff.runoff_overland_flowslope = struct('path',inputDir1); % for the new sediment yield code
sources.runoff.runoff_internal_basin = struct('path',inputDir1,'nanFillVal',0);
sources.runoff.runoff_internal_sink = struct('path',inputDir1,'nanFillVal',0);
sources.runoff.runoff_subsurface_flowtimes = struct('path',inputDir1,'nanFillVal',0);

% Model initialization
sources.state.starting_heads = struct('path',inputDir1);

% Miscellaneous parameters
sources.params = struct();

% Soils information
sources.soil.albedo_soil_dry = struct('path',inputDir1); % this one is "albedo" first to keep it associated with other similar parameters
sources.soil.soil_infil_cap = struct('path',inputDir1);
sources.soil.soil_texture = struct('path',inputDir1);
sources.soil.soil_lay_top = struct('path',inputDir1);
sources.soil.soil_lay_bot = struct('path',inputDir1);
sources.soil.soil_bub_press = struct('path',inputDir1);
sources.soil.soil_field_cap = struct('path',inputDir1);
sources.soil.soil_ksat = struct('path',inputDir1);
sources.soil.soil_k0 = struct('path',inputDir1);
sources.soil.soil_L = struct('path',inputDir1);
sources.soil.soil_pore_size = struct('path',inputDir1);
sources.soil.soil_theta_r = struct('path',inputDir1);
sources.soil.soil_porosity = struct('path',inputDir1);
sources.soil.soil_alpha = struct('path',inputDir1);
sources.soil.soil_N = struct('path',inputDir1);
sources.soil.soil_wilting_point = struct('path',inputDir1);
sources.soil.soil_pct_fraggt10 = struct('path',inputDir1);
sources.soil.soil_pct_frag3to10 = struct('path',inputDir1);
sources.soil.soil_pct_org = struct('path',inputDir1);
sources.soil.soil_pct_sand = struct('path',inputDir1);
sources.soil.soil_pct_clay = struct('path',inputDir1);

% Groundwater Model
sources.groundwater.ibound = struct('path',inputDir2);
sources.groundwater.start_heads = struct('path',inputDir2);
sources.groundwater.hk = struct('path',inputDir2);
sources.groundwater.vani = struct('path',inputDir2);
sources.groundwater.spec_yield = struct('path',inputDir2);
sources.groundwater.spec_stor = struct('path',inputDir2);
sources.groundwater.bottom = struct('path',inputDir2);
sources.groundwater.model_top = struct('path',inputDir2);
sources.groundwater.drn = struct('path',inputDir2,'nanFillVal',0);
sources.groundwater.drn_elev = struct('path',inputDir2);
sources.groundwater.drn_cond = struct('path',inputDir2);
sources.groundwater.riv = struct('path',inputDir2,'nanFillVal',0);
sources.groundwater.riv_elev = struct('path',inputDir2);
sources.groundwater.riv_bot = struct('path',inputDir2);
sources.groundwater.riv_cond = struct('path',inputDir2);
sources.groundwater.ghb = struct('path',inputDir2,'nanFillVal',0);
sources.groundwater.ghb_elev = struct('path',inputDir2);
sources.groundwater.ghb_cond = struct('path',inputDir2);
sources.groundwater.chd = struct('path',inputDir2,'nanFillVal',0);
sources.groundwater.chd_elev = struct('path',inputDir2);
sources.groundwater.et_extinct_depth = struct('path',inputDir2);
sources.groundwater.et_surf = struct('path',inputDir2);

% Read in the groundwater model table inputs
sources.groundwater.table.drn = struct('path',inputDir2);
sources.groundwater.table.drn_elev = struct('path',inputDir2);
sources.groundwater.table.riv = struct('path',inputDir2);
sources.groundwater.table.riv_elev = struct('path',inputDir2);
sources.groundwater.table.ghb = struct('path',inputDir2);
sources.groundwater.table.ghb_elev = struct('path',inputDir2);
sources.groundwater.table.chd = struct('path',inputDir2);
sources.groundwater.table.chd_elev = struct('path',inputDir2);

% Turn more off in case it's on, otherwise this can hang
flagMore = false;
if strcmpi(get(0,'more'),'on')
    more off
    flagMore = true;
end

%--------------------------------------------------------------------------
% Load the datasets and save to the output file
%--------------------------------------------------------------------------
% Create the field names and types for the time-varying tables
tableFields = {'presence_id','time_start','value'};
fieldTypes = {'int32','datetime','double'};

% Loop through grid types
for m = 1:length(readGroups)
    disp(['Reading group ',readGroups{m}])

    % Initialize arrays
    gridGroup = struct();
    tableGroup = struct(); % Currently, only used with groundwater
    
    % Loop through all of the grids
    gridNames = fieldnames(sources.(readGroups{m}));
    tablePresent = false;
    for n = 1:length(gridNames)
        % Check to see if this is a 'table' group
        if ~strcmpi(gridNames{n},'table')
            disp(['    ',gridNames{n}])
            % Call stat_obs_load_grid to create the grid struct
            if isfield(sources.(readGroups{m}).(gridNames{n}),'grid')
                thisGridName = sources.(readGroups{m}).(gridNames{n}).grid;
            else % assume the grid name is the same as the structure group
                thisGridName = gridNames{n};
            end
            gridGroup.(gridNames{n}) = stat_obs_load_grid(thisGridName,sources.(readGroups{m}).(gridNames{n}),nanFill);
        else
            tablePresent = true

            % Loop through all of the table fields
            tableNames = fieldnames(sources.(readGroups{m}).(gridNames{n}));
            for o = 1:length(tableNames)
                % Read in the table, has fields 'presence_ID', 'time_start', 'value'
                thisTableFile = [sources.(readGroups{m}).(gridNames{n}).(tableNames{o}).path,filesep,tableNames{o},'.csv']
                if exist(thisTableFile,'file') > 0
                    % Read the table with options
                    readOpts = delimitedTextImportOptions('VariableNames',obsFields,'VariableTypes',fieldTypes);
                    thisData = readtable(inFile,readOpts);
                    thisData(1,:) = []; %remove the first row

                    % Convert the time_start field to a datenum
                    thisData.time_start = datenum(thisData.time_start);

                    % Save to the table group
                    tableGroup.(tableNames{o}) = readtable(thisTableFile);
                end
            end
        end
    end
    
    % Save this grid group
    stat_obs_write_grid_group(outFile,(readGroups{m}),gridGroup);

    % Save this table group, if present
    if tablePresent
        stat_obs_write_table_group(outFile,(readGroups{m}),tableGroup);
    end
    
end

% Set more back on if it was on already
if flagMore
    more on
end

% Display a final status message pointing to the written file
fprintf('STAT file % s written to % s',outFilename,workingDir)

end

