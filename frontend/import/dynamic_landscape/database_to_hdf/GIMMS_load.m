function GIMMS_load(workingDir,outFilename,sources,grids)
%Here, there are only two things the user must change:
%1) Everything in the "Specify these inputs" area
%2) The conversion of grid names to hours in the "Run through the list of
%grids..." section

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
gridsDir = grids.gimms.inDir;
clipGridLoc = grids.gimms.clip;
modelGridLoc = grids.model;
outFile = [workingDir,filesep,outFilename];
outGroup = ['/',sources.name];
outClass = sources.class;
gridUnits = sources.units;
gridMult = sources.scale;
gridOffset = sources.offset;
nanVal = sources.nan;

%--------------------------------------------------------------------------
%Read in both the data grid and the model grid headers, calculate lookup map
%--------------------------------------------------------------------------
%Read in the grids and their header metadata
clipGeoLoc = import_grid(clipGridLoc,'header');
modelGeoLoc = import_grid(modelGridLoc,'header');

%Now, run the lookup mapping function
lookup = cell_statistics(clipGeoLoc,modelGeoLoc);
lookup = uint32(lookup);

%--------------------------------------------------------------------------
%Parse the grids directory, create a structure containing all input grids
%--------------------------------------------------------------------------
dirStruct = dir([gridsDir,'\**\w001001.adf']);

%--------------------------------------------------------------------------
%Run through the list of grids, importing NDVI values, writing out grid hour
%--------------------------------------------------------------------------
numGrids = length(dirStruct);
numCells = clipGeoLoc.rows*clipGeoLoc.cols;
gridHour = zeros(numGrids,1); %Middle date of composite
badGrids = false(numGrids,1);
data = zeros(numGrids,numCells,'single');
h = waitbar(0,'Importing GIMMS NDVI Grids');
for m = 1:numGrids
    waitbar(m/numGrids,h);
    tempGrid = import_grid(dirStruct(m).name);
    tempGrid = single(tempGrid);
    tempGrid(tempGrid == intmin('int16')) = NaN;
    tempGrid(tempGrid>0) = tempGrid(tempGrid>0) * 10; %for some reason this seems necessary to get values in proper range
    tempGrid = tempGrid / 10000;
    tempGrid(tempGrid < -0.5) = 0; %assign water cells a value of 0
    tempGrid(tempGrid < 0) = NaN; %all remaining cells are noData, see documentation
    badGrids(m) = (sum(isnan(tempGrid(:))) >= numCells*2/3); %If 2/3 of the grid cells are NaN, throw it away
    data(m,:) = reshape(tempGrid(:),1,[]);
    %Parse the grid name to identify the date
    tempGridName = fliplr(strtok(fliplr(dirStruct(m).name(1:end-12)),'\'));
    if tempGridName(end) == 'a'
        daysShift = -7;
    elseif tempGridName(end) == 'b'
        daysShift = 7;
    else
        error('There is a problem with this grid, its name format does not match the others')
    end
    gridHour(m) = datenum(tempGridName(1:7),'yymmmdd') + daysShift;
end
if ishandle(h); close(h); end

%Sort the data
[gridHour,sortOrder] = sort(gridHour);
data = data(sortOrder,:);

%Remove badGrids rows from the data array
data = data(~badGrids,:);

%--------------------------------------------------------------------------
%Create the index array
%--------------------------------------------------------------------------
index = zeros(length(gridHour),2);
index(:,1) = gridHour;
index(badGrids,2) = NaN; %indicating missing grid (or bad values)
test = (~badGrids);
index(test,2) = (1:size(data,1)); %row indices of the grids

%--------------------------------------------------------------------------
%Write the array to an HDF5 file
%--------------------------------------------------------------------------
attribs = struct('class',outClass,'units',gridUnits,'scale',gridMult,'offset',gridOffset,'nan',nanVal);
import_write_dataset(outFile,outGroup,data,attribs,index,false,lookup,false);
end
