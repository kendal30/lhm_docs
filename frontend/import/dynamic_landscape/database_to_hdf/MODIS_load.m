function MODIS_load(workingDir,modelHours,outFilename,sources,grids)
%Here, there are only two things the user must change:
%1) Everything in the "Specify these inputs" area
%2) The conversion of grid names to hours in the "Run through the list of
%grids..." section

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
gridsDir = grids.modis.inDir;
clipGridLoc = grids.modis.clip;
modelGridLoc = grids.model;
outFile = [workingDir,filesep,outFilename];
outGroup = ['/',sources.name];
outClass = sources.class;
gridUnits = sources.units;
gridMult = sources.scale;
gridOffset = sources.offset;
nanVal = sources.nan;


%--------------------------------------------------------------------------
%Read in both the MODIS LAI clip grid and the model grid headers, calculate lookup map
%--------------------------------------------------------------------------
%Read in the grids and their header metadata
modelGeoLoc = import_grid(modelGridLoc,'header');
[~,clipGeoLoc] = import_grid(clipGridLoc);%,'box',modelGeoLoc); %will read in a header limited to the modelGeoLoc domain

%Get the file extension on the import MODIS grids
[~,~,fileExt] = fileparts(clipGridLoc);

%Now, need to test if the model is finer or coarser than the LAI data
if clipGeoLoc.cellsizeX < modelGeoLoc.cellsizeX
    %Lookup table just refers to model cells, we will upscale LAI
    lookup = reshape((1:modelGeoLoc.rows*modelGeoLoc.cols),modelGeoLoc.rows,modelGeoLoc.cols);
    upscaleLAI = true;

    %Precalculate the spatial weighting for upscaling
    [~,~,precalc] = cell_statistics(modelGeoLoc,clipGeoLoc,ones(clipGeoLoc.rows,clipGeoLoc.cols),'mean','weight');
else
    %Run the lookup mapping function, LAI will be saved at native
    %resolution for now
    lookup = cell_statistics(clipGeoLoc,modelGeoLoc);
    lookup = uint32(lookup);
    upscaleLAI = false;
end

%--------------------------------------------------------------------------
%Parse the grids directory, create a structure containing all input grids
%--------------------------------------------------------------------------
%First, test to see whether the grids are stored as .tif
if ~isempty(fileExt)
    dirStruct = dir([gridsDir,'\**\*',fileExt]);
    testImgTif = true;
else %otherwise, run as GRID format
    dirStruct = dir([gridsDir,'\**\w001001.adf']);
    testImgTif = false;
end

%--------------------------------------------------------------------------
%Run through the list of grids, importing LAI values, writing out grid hour
%--------------------------------------------------------------------------
numGrids = length(dirStruct);
numCellsIn = clipGeoLoc.rows*clipGeoLoc.cols;
if upscaleLAI
    numCellsOut = modelGeoLoc.rows*modelGeoLoc.cols;
else
    numCellsOut = clipGeoLoc.rows*clipGeoLoc.cols;
end
gridHour = zeros(numGrids,1);
badGrids = false(numGrids,1);
lastNaN = [];
data = zeros(numGrids,numCellsOut,outClass);
h = waitbar(0,'Importing MODIS LAI Grids');
for m = 1:numGrids
    waitbar(m/numGrids,h);

    %Parse this grid name
    if testImgTif
        [~,tempGridName,~] = fileparts(dirStruct(m).name);
    else
        tempGridName = fliplr(strtok(fliplr(dirStruct(m).name(1:end-12)),'\'));
    end

    % Parse the time
    parseName = regexp(tempGridName,grids.modis.nameFormat,'names');
    gridHour(m) = datenum(parseName.date,grids.modis.dateFormat);
    %indStartDate = strfind(tempGridName,'.A') + 2;
  	%gridHour(m) = datenum(tempGridName(indStartDate:indStartDate+3),'yyyy') + str2double(tempGridName(indStartDate+4:indStartDate+6));
    %gridHour(m) = datenum(tempGridName(1:4),'yyyy') + str2double(tempGridName(end-2:end));

    %Test to see if this grid is within the model timeframe, if so, read in
    if (gridHour(m)>=modelHours(1)) && (gridHour(m)<=modelHours(end))
        tempGrid = import_grid([dirStruct(m).folder,filesep,dirStruct(m).name]);%,'box',modelGeoLoc);
        if ~isempty(tempGrid)
            badGrids(m) = (sum(tempGrid(:) >= 200) >= numCellsIn*2/3); %If 2/3 of the grid cells are NaN, throw it away
            if numel(tempGrid) ~= numCellsIn
                badGrids(m) = true; %This grid is mismatched, reject
            end
        else
            badGrids(m) = true;
        end
    else
        badGrids(m) = true;
        gridHour(m) = NaN;
    end

    %If this is a good grid, then keep it
    if ~badGrids(m)
        %Check to see if we need to upscale, then store
        if upscaleLAI
            %Need to preserve NaN cells, need to put some logic in to only
            %update when needed and save computation time
            tempNaN = (tempGrid >= 200);
            if isempty(lastNaN)
                lastNaN = zeros(size(tempNaN)); %initialize lastNaN
                tempUpscaleNaN = cell_statistics(modelGeoLoc,clipGeoLoc,tempNaN,'mean','weight',precalc); %initialize tempUpscaleNaN
            end
            if ~all(tempNaN(:) == lastNaN(:)) %don't want to have to recalculate tempUpscaleNaN each time
                tempUpscaleNaN = cell_statistics(modelGeoLoc,clipGeoLoc,tempNaN,'mean','weight',precalc);
                lastNaN = tempNaN;
            end

            %Now upscale, and then replace NaN cells afterward
            tempGrid(tempNaN) = NaN;
            tempUpscale = cell_statistics(modelGeoLoc,clipGeoLoc,tempGrid,'mean','weight',precalc);
            tempUpscale(tempUpscaleNaN > 0.75) = str2double(nanVal);
            data(m,:) = reshape(tempUpscale(:),1,[]);
        else
            tempGrid(tempGrid >= 200) = str2double(nanVal);
            data(m,:) = reshape(tempGrid(:),1,[]);
        end

    end
end
if ishandle(h); close(h); end

%Trim the arrays to those within the model period
test = isnan(gridHour);
gridHour(test) = [];
badGrids(test) = [];
data(test,:) = [];

%Sort the data
[gridHour,sortOrder] = sort(gridHour);
data = data(sortOrder,:);
badGrids = badGrids(sortOrder);

%Remove badGrids rows from the data array
data = data(~badGrids,:);

% Keep only the LAI cells in the model domain
keepCells = unique(lookup);
data = data(:,keepCells);
[~,lookup] = ismember(lookup,unique(lookup));


%--------------------------------------------------------------------------
%Create the index array
%--------------------------------------------------------------------------
index = zeros(length(gridHour),2);
index(:,1) = gridHour;
index(badGrids,2) = NaN; %indicating missing grid (or bad values)
test = (~badGrids);
index(test,2) = (1:size(data,1)); %row indices of the grids

%--------------------------------------------------------------------------
%Write the array to an HDF5 file
%--------------------------------------------------------------------------
attribs = struct('class',outClass,'units',gridUnits,'scale',gridMult,'offset',gridOffset,'nan',nanVal);
import_write_dataset(outFile,outGroup,data,attribs,index,false,lookup,false);
end
