function lu_load(workingDir,modelHours,outFilename,sources,grids)
% LU_LOAD  Loads land use grids and stores them in an intermediate HDF5 file
%   lu_load()
% 
%   Descriptions of Input Variables:
%   none, all are specified below
% 
%   Descriptions of Output Variables:
%   none, all are written to an HDF5 file
% 
%   Example(s):
%   >> lu_load
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-23
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
% Specify these inputs here for clarity
%--------------------------------------------------------------------------
luGridDir = grids.lu.inDir;
luGridNames = grids.lu.grids;
luStart = grids.lu.start;
luMap = grids.lu.map;
outGroupLu = ['/',sources.lu.name];

ftGridDir = grids.flowtimes.inDir;
ftGridNames = grids.flowtimes.grids;
ftStart = grids.flowtimes.start;
outGroupFlowtimes = ['/',sources.flowtimes.name];

if grids.imperv.use
    impervGridDir = grids.imperv.inDir;
    impervGridTypeNames = grids.imperv.grids.type;
    impervGridPercNames = grids.imperv.grids.percent;
    impervStart = grids.imperv.start;
    impervMap = grids.imperv.map;
    outGroupImperv = ['/',sources.imperv.name];
end

% TODO: move these down below later when adding more irrigation grid types
if grids.irrig.use
    irrigMap = grids.irrig.map;

    irrigGridDirActive = grids.irrig.inDir.active;
    irrigGridDirTech = grids.irrig.inDir.tech;
    irrigGridDirLim = grids.irrig.inDir.limit;
    
    irrigGridActiveNames = grids.irrig.grids.active;
    irrigGridTechNames = grids.irrig.grids.tech;
    irrigGridLimNames = grids.irrig.grids.limit;
 
    irrigStartActive = grids.irrig.start.active;
    irrigStartTech = grids.irrig.start.tech;
    irrigStartLim = grids.irrig.start.limit;
    
    outGroupIrrigTech = ['/',sources.irrig.tech.name];
    outGroupIrrigLim = ['/',sources.irrig.lim.name];
end

%--------------------------------------------------------------------------
% Read in grid headers and build lookup table
%--------------------------------------------------------------------------
% Read in the model grid header data
modelGeoLoc = import_grid(grids.model,'header');

% Now, run the lookup mapping function
lookup = reshape((1:prod([modelGeoLoc.rows,modelGeoLoc.cols])),modelGeoLoc.rows,modelGeoLoc.cols);
lookup = uint32(lookup);

% Prepare the output file name
outFile = [workingDir,filesep,outFilename];

%--------------------------------------------------------------------------
% Loop through lu grids, building the majority and weights arrays
%--------------------------------------------------------------------------
% Initialize loop variables
numGrids = length(luGridNames);
luIndex = zeros(numGrids,3);
luMajority = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols,'uint8');
luWeights = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols,length(luMap));
dispName = 'Landuse';
h = waitbar(0,sprintf('Reading in %s grids',dispName));
for m = 1:numGrids
    waitbar(m/numGrids,h);

    % Read in the data
    if m == 1
        [thisLU,lastGeoLoc,precalc,numCells,thisMajority] = read_weights(luGridDir,luGridNames{m},...
            luMap,modelGeoLoc,[255,128],NaN,false,dispName);
    else
        [thisLU,lastGeoLoc,precalc,thisMajority] = read_weights(luGridDir,luGridNames{m},...
            luMap,modelGeoLoc,[255,128],NaN,false,dispName,lastGeoLoc,precalc,numCells);
    end

    % Save data to output array
    luWeights(m,:,:) = thisLU;
    luMajority(m,:) = reshape(thisMajority,1,[]);

    % Renormalize weights to 1
    sumWeights = sum(luWeights(m,:,:),3);
    test = sumWeights > 0 & sumWeights < 1;
    luWeights(m,test,:) = luWeights(m,test,:) ./ repmat(sumWeights(test),[1,1,size(luWeights,3)]);

    % Correct landuse cells with weights of 0, assign them as the average
    % mixture of cells - this should not occur, but occasionally edge
    % effects can cause it.
    avgWeight = sum(luWeights(m,test,:),2) ./ sum(test);
    test = (sumWeights == 0);
    luWeights(m,test,:) = repmat(avgWeight,[1,sum(test),1]);

    % Get the index
    [thisIndex] = get_index(luStart,m,numGrids,modelHours(1),modelHours(end),dispName);
    luIndex(m,:) = thisIndex;
end
if ishandle(h); close(h); end
clear thisWeights thisMajority precalc numCells lastGeoLoc

% Write the outputs---------------------------------------------------------
% Create the output group
groupExist = h5groupcreate(outFile,outGroupLu);

if groupExist
    % If it exists, must rename old file or stop
    get_overwrite_pref(outFile,outGroupLu);
    % If we're still going, that means the user decided to
    % overwrite
    % rename the current file
    [outFilePath,outFileName,outFileExt] = fileparts(outFile);
    oldFileName = ['old_',outFileName,outFileExt];
    oldFile = [outFilePath,filesep,oldFileName];
    if exist(oldFile,'file')
        delete(oldFile);
    end
    system(['ren ',outFile,' ',oldFileName]);
end

% Write the LU group
write_group_weights(outFile,outGroupLu,sources.lu,luIndex,lookup,{luMap(:).name},luWeights,luMajority)

% Clear written datasets
clear luWeights luMajority


%--------------------------------------------------------------------------
% Loop through flowtimes grids, building the flowtimes arrays
%--------------------------------------------------------------------------
% Initialize loop variables
numGrids = length(ftGridNames);
ftIndex = zeros(numGrids,3);
ftFlowtimes = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols,'uint32');
dispName = 'Flowtimes';
h = waitbar(0,sprintf('Reading in %s grids',dispName));
for m = 1:numGrids
    waitbar(m/numGrids,h);

    % Read in the data 
    if m == 1
        [thisFlowtimes,lastGeoLoc,precalc] = read_data(ftGridDir,ftGridNames{m},...
            modelGeoLoc,[],NaN);
    else
        [thisFlowtimes,lastGeoLoc,precalc] = read_data(ftGridDir,ftGridNames{m},...
            modelGeoLoc,[],NaN,lastGeoLoc,precalc);
    end

    % Reshape for output array
    ftFlowtimes(m,:) = reshape(thisFlowtimes,1,[]);

    % Get the index
    [thisIndex] = get_index(ftStart,m,numGrids,modelHours(1),modelHours(end),dispName);
    ftIndex(m,:) = thisIndex;
end
if ishandle(h); close(h); end
clear thisFlowtimes precalc lastGeoLoc

% Write the outputs---------------------------------------------------------
write_group_data(outFile,outGroupFlowtimes,sources.flowtimes,ftIndex,lookup,ftFlowtimes)

% Clear written datasets
clear ftFlowtimes flowtimes tempGridFlowtimes


%--------------------------------------------------------------------------
% Loop through imperv grids, building the majority and weights arrays
%--------------------------------------------------------------------------
if grids.imperv.use
    % Initialize loop variables
    numGrids = length(impervGridTypeNames);
    impervIndex = zeros(numGrids,3);
    impervWeights = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols,length(impervMap));
    dispName = 'Impervious';
    h = waitbar(0,sprintf('Reading in %s grids',dispName));
    for m = 1:numGrids
        waitbar(m/numGrids,h);

        % Prepare the impervious percent array to weight the types with
        thisGridPerc = import_grid([impervGridDir,filesep,impervGridPercNames{m}],'box',modelGeoLoc);
        thisGridPerc(thisGridPerc < 0) = 0;
        thisGridPerc(thisGridPerc > 100) = 100;
        thisGridPerc = double(thisGridPerc); % convert to double
        thisGridPerc = thisGridPerc ./ 100; % convert to fraction

        % Read in the data
        if m == 1
            [thisImperv,lastGeoLoc,precalc,numCells] = read_weights(impervGridDir,impervGridTypeNames{m},...
                impervMap,modelGeoLoc,255,NaN,thisGridPerc,dispName);
        else
            [thisImperv,lastGeoLoc,precalc,numCells] = read_weights(impervGridDir,impervGridTypeNames{m},...
                impervMap,modelGeoLoc,255,NaN,thisGridPerc,dispName,lastGeoLoc,precalc,numCells);
        end

        % Save data to output array
        impervWeights(m,:,:) = thisImperv;

        % Get the index
        [thisIndex] = get_index(impervStart,m,numGrids,modelHours(1),modelHours(end),dispName);
        impervIndex(m,:) = thisIndex;
    end
    if ishandle(h); close(h); end
    clear thisImperv precalc numCells lastGeoLoc

    % Write the outputs---------------------------------------------------------
    write_group_weights(outFile,outGroupImperv,sources.imperv,impervIndex,lookup,{impervMap(:).name},impervWeights)

    % Clear written datasets
    clear impervWeights
end


%--------------------------------------------------------------------------
% Loop through irrigation grids, active, then tech, then limit
%--------------------------------------------------------------------------
% The LHM technology input is the product of the technology and the activity
if grids.irrig.use
    % Irrigation active grids-----------------------------------------------
    numGrids = length(irrigGridActiveNames);
    irrigActiveIndex = zeros(numGrids,3);
    irrigActive = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols,'single');
    dispName = 'Irrigation activity';
    h = waitbar(0,sprintf('Reading in %s grids',dispName));
    for m = 1:numGrids
        waitbar(m/numGrids,h);

        % Read in the data 
        if m == 1
            [thisActive,lastGeoLoc,precalc] = read_data(irrigGridDirActive,irrigGridActiveNames{m},...
                modelGeoLoc,255,NaN);
        else
            [thisActive,lastGeoLoc,precalc] = read_data(irrigGridDirActive,irrigGridActiveNames{m},...
                modelGeoLoc,255,NaN,lastGeoLoc,precalc);
        end
    
        % Reshape for output array
        irrigActive(m,:) = reshape(thisActive,1,[]);

        % Get the index
        [thisIndex] = get_index(irrigStartActive,m,numGrids,modelHours(1),modelHours(end),dispName);
        irrigActiveIndex(m,:) = thisIndex;
    end
    if ishandle(h); close(h); end
    clear thisActive precalc lastGeoLoc

    % Irrigation technology grids-------------------------------------------
    numGrids = length(irrigGridTechNames);
    irrigTechIndex = zeros(numGrids,3);
    irrigTechWeights = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols,length(irrigMap),'single');
    dispName = 'Irrigation tech';
    h = waitbar(0,sprintf('Reading in %s grids',dispName));
    for m = 1:numGrids
        waitbar(m/numGrids,h);

        % Read in the data
        if m == 1
            [thisTech,lastGeoLoc,precalc,numCells] = read_weights(irrigGridDirTech,irrigGridTechNames{m},...
                irrigMap,modelGeoLoc,255,NaN,false,dispName);
        else
            [thisTech,lastGeoLoc,precalc,numCells] = read_weights(irrigGridDirTech,irrigGridTechNames{m},...
                irrigMap,modelGeoLoc,255,NaN,false,dispName,lastGeoLoc,precalc,numCells);
        end

        % Save data to output array
        irrigTechWeights(m,:,:) = thisTech;

        % Get the index
        [thisIndex] = get_index(irrigStartTech,m,numGrids,modelHours(1),modelHours(end),dispName);
        irrigTechIndex(m,:) = thisIndex;
    end
    if ishandle(h); close(h); end
    clear thisTech precalc numCells lastGeoLoc

    % Combine activity and technology grids--------------------------------------
    % Fist, combine the indices of both
    combIndex = zeros(length(modelHours),4);
    combIndex(:,1) = modelHours;

    rowsActive = find(ismember_dates(modelHours,irrigActiveIndex(:,1),6));
    for m = 1:length(rowsActive)
        firstRow = rowsActive(m);
        if m < length(rowsActive)
            lastRow = rowsActive(m+1);
        else
            lastRow = length(modelHours);
        end
        combIndex(firstRow:lastRow,3) = irrigActiveIndex(m,3);
    end

    rowsTech = find(ismember_dates(modelHours,irrigTechIndex(:,1),6));
    for m = 1:length(rowsTech)
        firstRow = rowsTech(m);
        if m < length(rowsTech)
            lastRow = rowsTech(m+1);
        else
            lastRow = length(modelHours);
        end
        combIndex(firstRow:lastRow,4) = irrigTechIndex(m,3);
    end

    % Now, drop all rows of the combined index where the two indexes don't change
    [~,uniqueRows] = unique(combIndex(:,3:4),'first','rows');
    combIndex = combIndex(uniqueRows,:);
    
    % Now, set the end time of the rows
    combIndex(1:end-1,2) = combIndex(2:end,1);
    combIndex(end,2) = modelHours(end);
    
    % Next, loop through the combined index, multiplying active and tech
    % grids
    numTypes = size(irrigTechWeights,3);
    numComb = size(combIndex,1);
    irrigCombWeights = zeros(size(combIndex,2),size(irrigTechWeights,2),numTypes,'single');
    for m = 1:numComb
        indActive = combIndex(m,3);
        indTech = combIndex(m,4);
        % Multiply this set
        irrigCombWeights(m,:,:) = repmat(irrigActive(indActive,:),[1,1,numTypes]) .* ...
            irrigTechWeights(indTech,:,:);
    end
    
    % Prepare the index for writing out
    irrigCombIndex = [combIndex(:,1:2),(1:numComb)'];
    
    % Remove NaNs
    irrigCombWeights(isnan(irrigCombWeights)) = 0;
    
    % Irrigation Technology (incorporates activity)
    write_group_weights(outFile,outGroupIrrigTech,sources.irrig.tech,irrigCombIndex,lookup,{irrigMap(:).name},irrigCombWeights)
    
    % Clear written datasets
    clear irrigTechWeights irrigCombWeights irrigActive
    
    
    % Read limitation grids-------------------------------------------------------
    numGrids = length(irrigGridLimNames);
    irrigLimIndex = zeros(numGrids,3);
    irrigLim = zeros(numGrids,modelGeoLoc.rows * modelGeoLoc.cols);
    dispName = 'Irrigation limitation';
    h = waitbar(0,sprintf('Reading in %s grids',dispName));
    for m = 1:numGrids
        waitbar(m/numGrids,h);

        % Read in the data 
        if m == 1
            [thisLim,lastGeoLoc,precalc] = read_data(irrigGridDirLim,irrigGridLimNames{m},...
                modelGeoLoc,255,NaN);
        else
            [thisLim,lastGeoLoc,precalc] = read_data(irrigGridDirLim,irrigGridLimNames{m},...
                modelGeoLoc,255,NaN,lastGeoLoc,precalc);
        end
    
        % Reshape for output array
        irrigLim(m,:) = reshape(thisLim,1,[]);

        % Get the index
        [thisIndex] = get_index(irrigStartLim,m,numGrids,modelHours(1),modelHours(end),dispName);
        irrigLimIndex(m,:) = thisIndex;
    end
    if ishandle(h); close(h); end
    clear thisLim precalc lastGeoLoc

    % Write the outputs-----------------------------------------------------
    % Remove NaNs
    irrigLim(isnan(irrigLim)) = 0;

    % Irrigation Limitation
    write_group_data(outFile,outGroupIrrigLim,sources.irrig.lim,irrigLimIndex,lookup,irrigLim)

    % Clear written datasets
    clear irrigLim
end

end



%--------------------------------------------------------------------------
% Helper functions
%--------------------------------------------------------------------------
function [thisData,thisGeoLoc,precalc] = read_data(gridDir,gridName,modelGeoLoc,zeroVals,nanVal,lastGeoLoc,precalc)
% Import the limitation grids
[tempGrid,thisGeoLoc] = import_grid([gridDir,filesep,gridName],'box',modelGeoLoc);
tempGrid = single(tempGrid);

% Clean up the grids
for m = 1:length(zeroVals)
    tempGrid(tempGrid == zeroVals(m)) = 0;
end
tempGrid(tempGrid < 0) = 0;

% Get the limitations, use precalculated weights if possible
thisGeoLoc.nan = nanVal; % this will be treated as a NaN value for the purposes of the zonal statistics calculation
if nargin == 5
    [thisData,~,precalc] = cell_statistics(modelGeoLoc,thisGeoLoc,tempGrid,{'mean'},{'weight'});
elseif ~((thisGeoLoc.rows==lastGeoLoc.rows) && (thisGeoLoc.cols==lastGeoLoc.cols))
    [thisData,~,precalc] = cell_statistics(modelGeoLoc,thisGeoLoc,tempGrid,{'mean'},{'weight'});
else
    [thisData,~] = cell_statistics(modelGeoLoc,thisGeoLoc,tempGrid,{'mean'},{'weight'},precalc);
end
end


function [thisWeights,thisGeoLoc,precalc,numCells,thisMajority] = read_weights(gridDir,gridName,gridMap,modelGeoLoc,zeroVals,nanVal,...
    weightGrid,dispName,lastGeoLoc,precalc,numCells)
% Import the LU grids
[tempGrid,thisGeoLoc] = import_grid([gridDir,filesep,gridName],'box',modelGeoLoc);

% Clean up the grids
for m = 1:length(zeroVals)
    tempGrid(tempGrid == zeroVals(m)) = 0;
end
tempGrid(tempGrid < 0) = 0;

% First, determine the number of fine cells within the model grid
thisGeoLoc.nan = nanVal; % this will be treated as a NaN value for the purposes of the zonal statistics calculation
if nargin == 8
    [numCells,~,precalc] = cell_statistics(modelGeoLoc,thisGeoLoc,ones(size(tempGrid)),{'sum'},{'weight'});
elseif ~((thisGeoLoc.rows==lastGeoLoc.rows) && (thisGeoLoc.cols==lastGeoLoc.cols))
    [numCells,~,precalc] = cell_statistics(modelGeoLoc,thisGeoLoc,ones(size(tempGrid)),{'sum'},{'weight'});
end

% This is a two-step process, first, select each type individually,
% then, calculate the weights for that type.
h2 = waitbar(0,sprintf('Calculating weights for %s Grids',dispName));
thisWeights = zeros(modelGeoLoc.rows * modelGeoLoc.cols,length(gridMap));
for n = 1:length(gridMap) % for each technology
    waitbar(n/length(gridMap),h2);
    match = single(str2double(gridMap(n).integer) == tempGrid);
    if ~islogical(weightGrid)
        match = match .* weightGrid;
    end
    if any(match(:))
        [tempWeights,~] = cell_statistics(modelGeoLoc,thisGeoLoc,match,{'sum'},{'weight'},precalc);
        tempWeights = tempWeights ./ numCells;
        thisWeights(:,n) = reshape(tempWeights,1,[]);
    else
        thisWeights(:,n) = 0;
    end
end
if ishandle(h2); close(h2); end

if nargout == 5
    % Now, determine the majority landuse
    [thisMajority,~] = cell_statistics(modelGeoLoc,thisGeoLoc,tempGrid,{'majority'},{'weight'},precalc);

    % Fill in any 0 cells
    test = (thisMajority == 0);
    thisMajority(test) = mode(thisMajority(~test));
end

end


% Get the index
function [thisIndex] = get_index(gridStartList,gridNum,numGrids,modelStart,modelEnd,gridName)
thisIndex = zeros(1,3);

% Save the index
thisIndex(1) = datenum(gridStartList{gridNum});
if gridNum == 1
    assert(thisIndex(1)<=modelStart,sprintf('%s grids start later than the model',gridName));
end
if gridNum < numGrids
    thisIndex(2) = datenum(gridStartList{gridNum+1});
else
    thisIndex(2) = modelEnd;
end
thisIndex(3) = gridNum;
end


function write_group_data(outFile,outGroup,outSources,outIndex,outLookup,outData)
    % Create the group
    h5groupcreate(outFile,outGroup);
    % Write the index table
    attribs = struct('class',class(outIndex));
    h5datawrite(outFile,[outGroup,'/index'],outIndex,attribs);
    % Write the lookup array
    attribs = struct('class',class(outLookup));
    h5datawrite(outFile,[outGroup,'/lookup'],outLookup,attribs);
    % Write out the limitations array
    attribs = struct('class',outSources.class,'scale',outSources.scale,...
        'offset',outSources.offset,'nan',outSources.nan);
    h5datawrite(outFile,[outGroup,'/data'],outData,attribs);
end

function write_group_weights(outFile,outGroup,outSources,outIndex,outLookup,outNames,outWeights,outMajority)
    % Create the group
    h5groupcreate(outFile,outGroup);
    % Write the index table
    attribs = struct('class',class(outIndex));
    h5datawrite(outFile,[outGroup,'/index'],outIndex,attribs);
    % Write the lookup array
    attribs = struct('class',class(outLookup));
    h5datawrite(outFile,[outGroup,'/lookup'],outLookup,attribs);
    % Write out the names arrays
    attribs = struct('class','char');
    namesGroup = [outGroup,'/names'];
    h5groupcreate(outFile,namesGroup);
    for m = 1:size(outWeights,3)
        h5datawrite(outFile,[namesGroup,'/',num2str(m)],outNames{m},attribs);
    end
    % Write out the weights array
    attribs = struct('class',outSources.class,'scale',outSources.scale,...
        'offset',outSources.offset,'nan',outSources.nan);
    h5datawrite(outFile,[outGroup,'/weights'],outWeights,attribs,true);
    if nargin == 8
        % Write out the majority array
        attribs = struct('class',class(outMajority),'nan','0','units','none');
        h5datawrite(outFile,[outGroup,'/majority'],outMajority,attribs);
    end
end