function AVHRR_load(workingDir,outFilename,sources,grids)
%Here, there are only two things the user must change:
%1) Everything in the "Specify these inputs" area
%2) The conversion of grid names to hours in the "Run through the list of
%grids..." section

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
gridsDir = grids.avhrr.inDir;
clipGridLoc = grids.avhrr.clip;
modelGridLoc = grids.model;
outFile = [workingDir,filesep,outFilename];
outGroup = ['/',sources.name];
outClass = sources.class;
gridUnits = sources.units;
gridMult = sources.scale;
gridOffset = sources.offset;
nanVal = sources.nan;

%--------------------------------------------------------------------------
%Read in both the MODIS LAI clip grid and the model grid headers, calculate lookup map
%--------------------------------------------------------------------------
%Read in the grids and their header metadata
clipGeoLoc = import_grid(clipGridLoc,'header');
modelGeoLoc = import_grid(modelGridLoc,'header');

%Now, run the lookup mapping function
lookup = cell_statistics(clipGeoLoc,modelGeoLoc);
lookup = uint32(lookup);

%--------------------------------------------------------------------------
%Parse the grids directory, create a structure containing all input grids
%--------------------------------------------------------------------------
dirStruct = dir([gridsDir,'\**\w001001.adf']);

%--------------------------------------------------------------------------
%Run through the list of grids, importing NDVI values, writing out grid hour
%--------------------------------------------------------------------------
numGrids = length(dirStruct);
numCells = clipGeoLoc.rows*clipGeoLoc.cols;
gridHour = zeros(numGrids,1);  %Middle date of composite
badGrids = false(numGrids,1);
data = zeros(numGrids,numCells,outClass);
h = waitbar(0,'Importing AVHRR NDVI Grids');
for m = 1:numGrids
    waitbar(m/numGrids,h);
    tempGrid = import_grid(dirStruct(m).name);
    badGrids(m) = (sum(tempGrid(:)>200) >= numCells*2/3); %If 2/3 of the grid cells are NaN, throw it away
    if ~badGrids(m)
        tempGrid(tempGrid > 200) = str2double(nanVal);
        tempGrid(tempGrid < 100) = 100; %assign water cells a value of 0
        data(m,:) = reshape(tempGrid(:),1,[]);
    end
    tempGridName = fliplr(strtok(fliplr(dirStruct(m).name(1:end-12)),'\'));
    gridHour(m,:) = datenum(tempGridName(3:4),'yy') + ....
        (str2double(tempGridName(end-5:end-3))+str2double(tempGridName(end-2:end)))/2;
    if gridHour(m,2) < gridHour(m,1) %in this case, the grid spans the end of the year
        gridHour(m,2) = gridHour(m,2) + 365;
    end
end
if ishandle(h); close(h); end

%Sort the data
[gridHour,sortOrder] = sort(gridHour);
data = data(sortOrder,:);

%Remove badGrids rows from the data array
data = data(~badGrids,:);

%--------------------------------------------------------------------------
%Create the index array
%--------------------------------------------------------------------------
index = zeros(length(gridHour),2);
index(:,1) = gridHour;
index(badGrids,2) = NaN; %indicating missing grid (or bad values)
test = (~badGrids);
index(test,2) = (1:size(data,1)); %row indices of the grids

%--------------------------------------------------------------------------
%Write the array to an HDF5 file
%--------------------------------------------------------------------------
attribs = struct('class',outClass,'units',gridUnits,'scale',gridMult,'offset',gridOffset,'nan',nanVal);
import_write_dataset(outFile,outGroup,data,attribs,index,false,lookup,false);
end
