function lai_spatial_correction(workingDir,outFilename,sources)
%LAI_SPATIAL_CORRECTION  Corrects the spatial mismatch between LAI datasets
%   lai_spatial_correction()
%
%   This function uses NDVI-derived LAI (herein referred to as NDVI
%   usually) and MODIS-derived LAI to perform a spatial correction on the
%   NDVI-derived LAI data.
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> lai_spatial_correction
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

warning off all; %the interp1 routine has a NaN warning that I don't care to see
%--------------------------------------------------------------------------
%Read in the lai index
%--------------------------------------------------------------------------
laiFile = [workingDir,filesep,sources.lai.data{1}];
laiDataset = sources.lai.name{1};
laiIndex = h5dataread(laiFile,['/',laiDataset,'/index'],false);

%Make sure that the ndviIndex is trimmed to only include dates with
%rows present
laiIndex = laiIndex(laiIndex(:,2)>0,:);

%Get the size of the lai array
laiSize = h5varget(laiFile,['/',laiDataset,'/data'],false);

%--------------------------------------------------------------------------
%Outer loop for two ndvi datasets
%--------------------------------------------------------------------------
ndviDatasets = sources.ndvi.name;
for m = 1:length(ndviDatasets)
    ndviDataset = ndviDatasets{m};
    ndviFile = [workingDir,filesep,sources.ndvi.data{m}];

    %----------------------------------------------------------------------
    %Read in the NDVI-derived LAI index and determine the overlap period
    %----------------------------------------------------------------------
    ndviIndex = h5dataread(ndviFile,['/',ndviDataset,'/index'],false);

    %Make sure that the ndviIndex is trimmed to only include dates with
    %rows present
    ndviIndex = ndviIndex(ndviIndex(:,2)>0,:);

    %Now determine the overlap period
    indBracket = bracket_index(laiIndex(:,1),ndviIndex(1,1),ndviIndex(end,1));

    %Find this start and end indices of the overlap period in each dataset
    laiStart = laiIndex(indBracket(1),2);
    laiEnd = laiIndex(indBracket(2),2);

    ndviStart = ndviIndex(find(ndviIndex(:,1)>=laiIndex(indBracket(1),1),1,'first'),2);
    ndviEnd = ndviIndex(find(ndviIndex(:,1)<laiIndex(indBracket(2),1),1,'last'),2);

    %----------------------------------------------------------------------
    %Read in the data for the overlap periods with reformatting
    %----------------------------------------------------------------------
    %get the dataset size
    ndviSize = h5varget(ndviFile,['/',ndviDataset,'/data'],false);

    %Read in the chunk of overlap data
    ndviOverlap = h5dataread(ndviFile,['/',ndviDataset,'/data'],true,...
        [ndviStart - 1,0],[ndviEnd - ndviStart + 1,ndviSize(2)],[1,1]);
    %Convert the data to single precision to save memory
    ndviOverlap = single(ndviOverlap);

    laiOverlap = h5dataread(laiFile,['/',laiDataset,'/data'],true,...
        [laiStart - 1,0],[laiEnd - laiStart + 1,laiSize(2)],[1,1]);
    %Convert the data to single precision to save memory
    laiOverlap = single(laiOverlap);

    %----------------------------------------------------------------------
    %Interpolate the data to a set of standard Julian days for averaging
    %----------------------------------------------------------------------
    %Make an array dates to interpolate to
    startDate = datevec(laiIndex(laiStart,1));
    endDate = datevec(laiIndex(laiEnd,1));
    interpYears = (startDate(1):endDate(1));
    interpYears = datenum(interpYears,zeros(size(interpYears)),zeros(size(interpYears)));
    numDays = length(sources.standardDays);
    numYears = length(interpYears);
    interpDates = repmat(interpYears',1,numDays) + repmat(sources.standardDays,numYears,1);
    interpDates = reshape(interpDates',[],1);

    %Interpolate each dataset, but only the rows that aren't all NaNs
    ndviInterp = zeros(length(interpDates),size(ndviOverlap,2)) + NaN;
    keepCols = ~all(isnan(ndviOverlap),1);
    ndviInterp(:,keepCols) = interp1(ndviIndex(ndviStart:ndviEnd,1),ndviOverlap(:,keepCols),interpDates,'linear');
    laiInterp = zeros(length(interpDates),size(laiOverlap,2)) + NaN;
    keepCols = ~all(isnan(ndviOverlap),1);
    laiInterp(:,keepCols) = interp1(laiIndex(laiStart:laiEnd,1),laiOverlap(:,keepCols),interpDates,'linear');

    %Clear the original overlap data to save memory
    clear ndviOverlap laiOverlap

    %----------------------------------------------------------------------
    %Loop through the datasets, calculating the difference between them
    %----------------------------------------------------------------------
    numDates = length(sources.standardDays);
    [ratioMap] = deal(zeros(numDates,size(laiInterp,2)));
    minLai = zeros(1,size(laiInterp,2)) + 2;
    maxLai = zeros(1,size(laiInterp,2));
    for n = 1:numDates
        %Find the row indexes to use in calculating the average
        rowInd = (round(date2doy(interpDates)) == sources.standardDays(n));

        %Determine the ratio of the two datasets
        ratio = laiInterp(rowInd,:) ./ ndviInterp(rowInd,:);

        %Determine the minimum and maximum cell values
        minLai = min(minLai,min(laiInterp(rowInd,:),[],1));
        maxLai = max(maxLai,max(laiInterp(rowInd,:),[],1));

        %Calculate the cross-year average of this ratio map, and save
        ratioMap(n,:) = nanmean(ratio,1);
    end
    clear ndviInterp laiInterp

    %Want no NaN values, just 1
    ratioMap(isnan(ratioMap)) = 1;

    %Don't want NaN in min and max either
    minLai(isnan(minLai)) = 0;
    minLai(minLai == 2) = 0;
    maxLai(isnan(maxLai)) = 7;

    %Now, smooth the ratioMap values
    testCols = find(~all(ratioMap==1,1));
    h = waitbar(0,'Smoothing the ratio map values');
    for n = 1:length(testCols)
        waitbar(n/length(testCols),h);
        ratioMap(:,testCols(n)) = smoothn(ratioMap(:,testCols(n)),3);
    end
    if ishandle(h); close(h); end
    %----------------------------------------------------------------------
    %Read in the entire NDVI dataset
    %----------------------------------------------------------------------
    [ndvi,ndviAttribs] = h5dataread(ndviFile,['/',ndviDataset,'/data'],false);
    ndviLookup = h5dataread(ndviFile,['/',ndviDataset,'/lookup'],false);


    %----------------------------------------------------------------------
    %Loop through the entire dataset, applying the correction and saving to
    %a new variable
    %----------------------------------------------------------------------
    ndviCorrected = zeros(size(ndvi),class(ndvi));
    h = waitbar(0,['Applying Correction to Dataset ',ndviDataset]);
    for n = 1:size(ndvi,1)
        waitbar(n/size(ndvi,1),h);
        %Get the current DOY
        currDOY = date2doy(ndviIndex(n,1));

        %Interpolate a ratio map at this date
        interpRatio = interp1(sources.standardDays,ratioMap,currDOY,'linear');

        %Convert the current dataset to a single-precision with NaNs
        currData = single(ndvi(n,:));
        currData(currData==str2double(ndviAttribs.nan)) = NaN;
        currData = currData / str2double(ndviAttribs.scale);

        %Apply the correction
        tempCorrect = currData .* interpRatio;

        %Set in range of min and max
        tempCorrect = max(minLai,tempCorrect);
        tempCorrect = min(maxLai,tempCorrect);

        %Change the datatype of tempCorrect
        tempCorrect = tempCorrect * str2double(ndviAttribs.scale);
        tempCorrect(isnan(tempCorrect)) = str2double(ndviAttribs.nan);
        tempCorrect = uint8(tempCorrect);

        %Save this corrected datast
        ndviCorrected(n,:) = tempCorrect;
    end
    if ishandle(h); close(h); end
    %----------------------------------------------------------------------
    %Save the dataset
    %----------------------------------------------------------------------
    outFile = [workingDir,filesep,outFilename];
    tempGroup = ['/',ndviDataset];
    ndviAttribs.class = class(ndviCorrected);
    import_write_dataset(outFile,tempGroup,ndviCorrected,ndviAttribs,ndviIndex,false,ndviLookup,false);

    %Clear some space
    clear ndviCorrected ndviIndex ndviLookup minLai maxLai ratioMap ndvi
end

%Turn the warnings back on
warning on all;
