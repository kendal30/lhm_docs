function julian_fill_landscape(workingDir,outFilename,modelHours,sources)
%JULIAN_FILL_LANDSCAPE  One-line description here, please.
%   julian_fill_landscape()
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> julian_fill_landscape
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
averageDays = sources.averageDays; %julian days to calculate LAI

%--------------------------------------------------------------------------
%Read in the input dataset
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,sources.lai.data];
tempGroup = ['/',sources.lai.name];

%Read in the index
index = h5dataread(inFile,[tempGroup,'/index'],false);
%Read in the lookup table
lookup = h5dataread(inFile,[tempGroup,'/lookup'],false);

%Read in the data
[lai,laiAttrib] = h5dataread(inFile,[tempGroup,'/data'],false);
lai(lai==255) = 0;

%--------------------------------------------------------------------------
%Interpolate the available data to match the specified LAI days
%--------------------------------------------------------------------------
%Make sure the index only has values to match the grid days
index = index(index(:,2) > 0,:);

%Now, calculate the julian day of year of the index dates
indexDOY = [date2doy(index(:,1)),index(:,2)];

%Now, sort the index in ascending order
indexSort = sortrows(indexDOY);

%Determine unique values of julian days
uniqueDays = unique(round(indexSort(:,1)));

%Loop through this array and average grids that occur on the same julian
%day
laiDOY = zeros(length(uniqueDays),size(lai,2),sources.lai.class);
h = waitbar(0,'Generating LAI maps for Julian Days');
for m = 1:length(uniqueDays)
    waitbar(m/length(uniqueDays),h);
    laiRow = indexSort((uniqueDays(m) == round(indexSort(:,1))),2);
    tempData = single(lai(laiRow,:));
    tempData(tempData == 0) = NaN;
    tempMean = nanmean(tempData,1);
    tempMean(isnan(tempMean)) = 0;
    tempMean = round(tempMean);
    laiDOY(m,:) = tempMean;
end
if ishandle(h); close(h); end

%Now, do the much slower step of smoothing the data for each not-all-zero
%column
testCol = find(~all(laiDOY==0,1));
h = waitbar(0,'Smoothing time-series for each grid cell');
for m = 1:length(testCol)
    waitbar(m/length(testCol),h);
    tempSmooth = smoothn(laiDOY(:,testCol(m)),3);
    laiDOY(:,testCol(m)) = round(tempSmooth);
end
if ishandle(h); close(h); end

%Now, do the interpolation to get the averages at the specified days
laiInterp = interp1(uniqueDays,single(laiDOY),averageDays,'linear','extrap');
laiInterp(laiInterp<0) = 0;
laiInterp = uint8(round(laiInterp)); %this might have to change if sources.lai.class is changed
clear laiDOY

%--------------------------------------------------------------------------
%Determine chunks of time that need to be filled
%--------------------------------------------------------------------------
%Make an array of ideal dates
startDate = datevec(modelHours(1));
endDate = datevec(modelHours(end));
modelYears = (startDate(1):endDate(1));
modelYears = datenum(modelYears,zeros(size(modelYears)),zeros(size(modelYears)));
numDays = length(averageDays);
numYears = length(modelYears);
idealDates = repmat(modelYears',1,numDays) + repmat(averageDays,numYears,1);
idealDates = reshape(idealDates',[],1);
trimDates = bracket_index(idealDates,modelHours(1),modelHours(end));
idealDates = idealDates(trimDates(1):trimDates(2));

%Remove any rows from this array that are within 30 days of an LAI dataset
keepRow = false(size(idealDates));
for m = 1:size(idealDates,1)
    minDiff = min(abs(index(:,1) - idealDates(m)));
    keepRow(m) = (minDiff > 30);
end

%Find the chunks of missing/present data
chunksDiff = find((diff(keepRow) ~= 0));
chunkDates = [datenum(startDate);idealDates(chunksDiff);datenum(endDate)];

numChunks = length(chunksDiff)+1;
chunks = true(numChunks,1);
if keepRow(1)
    chunks(1:2:end) = false; %this indicates that data is missing, filling is required
else
    chunks(2:2:end) = true;
end

%Loop through the chunks, compositing a new dataset
laiFilled = zeros(size(lai,1) + sum(keepRow),size(lai,2),sources.lai.class);
indexFilled = zeros(size(index,1) + sum(keepRow),2);
currIndex = 0;
for m = 1:numChunks
    if chunks(m) %this indicates that data exists for this period
        %Determine which LAI indices to use
        laiStart = find(index(:,1) >= chunkDates(m),1,'first');
        laiEnd = find(index(:,1) < chunkDates(m+1),1,'last');
        numDates = laiEnd - laiStart + 1;

        %Now, add these to the filled dataset
        indexFilled(currIndex + 1:currIndex + numDates,1) = index(laiStart:laiEnd,1);
        laiFilled(currIndex + 1:currIndex + numDates,:) = lai(laiStart:laiEnd,:);
        currIndex = currIndex + numDates;
    else %this indicates the the dataset must get filled
        %Create the new dataset
        newStart = find(idealDates > chunkDates(m),1,'first');
        newEnd = find(idealDates <= chunkDates(m+1),1,'last');
        newDates = idealDates(newStart:newEnd);
        newDOY = date2doy(newDates);
        numDates = size(newDates,1);
        newDataset = zeros(numDates,size(laiInterp,2),sources.lai.class);
        for n = 1:numDates
            newDataset(n,:) = laiInterp(averageDays==newDOY(n),:);
        end

        %Now, add these to the filled dataset
        indexFilled(currIndex + 1:currIndex + numDates,1) = newDates;
        laiFilled(currIndex + 1:currIndex + numDates,:) = newDataset;
        currIndex = currIndex + numDates;
    end
end
%Drop the all 0 end if it exists, must be due to a date overlap
if ~any(indexFilled(end,:))
    indexFilled(end,:) = [];
    laiFilled(end,:) = [];
end
clear lai laiInterp

%Complete the index
indexFilled(:,2) = (1:size(laiFilled,1));

%Replace the 0 values with the 255 NaN value
laiFilled(laiFilled==0) = 255;

%--------------------------------------------------------------------------
%Write out the filled dataset
%--------------------------------------------------------------------------
outFile = [workingDir,filesep,outFilename];
laiAttrib.class = class(laiFilled);
import_write_dataset(outFile,tempGroup,laiFilled,laiAttrib,indexFilled,false,lookup,false);
end
