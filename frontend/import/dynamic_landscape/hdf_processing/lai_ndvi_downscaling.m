function lai_ndvi_downscaling(workingDir,outFilename,sources,grids)
%LAI_NDVI_DOWNSCALING  Downscales LAI and NDVI using finer resolution land use
%   lai_ndvi_downscaling()
%
%   Descriptions of Input Variables:
%   none, all are specified below
%
%   Descriptions of Output Variables:
%   none, all are written to an intermediate hdf5 file
%
%   Example(s):
%   >> lai_ndvi_downscaling
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-29
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
luGridFile = sources.luFile;
luMap = grids.lu.lu.map;

modelGridFile = grids.model;
modelLuFile = sources.modelLuFile;

ndviLaiFile = sources.ndviLaiFile;

datatype = sources.datatypes;

%The interpolation method sometimes throws warnings to screen that cannot
%be deactivated
more off

%--------------------------------------------------------------------------
%Load the information from the ndvi->lai function
%--------------------------------------------------------------------------
load([workingDir,filesep,ndviLaiFile],'params','laiTest');
rangeLai = cell(length(laiTest),1); %#ok<USENS>
for m = 1:length(laiTest)
    rangeLai{m} = [min(laiTest{m}(:)),max(laiTest{m}(:))];
end
%Overwrite water and barren
rangeLai{5} = [0,0];
rangeLai{7} = [0,0];
params{5} = [0,0.5];
params{7} = [0,0.5];

%--------------------------------------------------------------------------
%Load the land use grid and read in the header info
%--------------------------------------------------------------------------
%Import the LU grid
[luGrid,luGeoLoc] = import_grid(luGridFile); %all LU grids must have the same header

%Clean up the grid
luGrid(luGrid == 255) = 0;
luGrid(luGrid < 0) = 0;
luGrid = uint8(luGrid);

%--------------------------------------------------------------------------
%Load the model grid and lu weights for the model dimensions
%--------------------------------------------------------------------------
%Read in the grids and the header metadata
modelGeoLoc = import_grid(modelGridFile,'header');

%Define the model x/y grid
xGridModel = repmat((modelGeoLoc.left + ((0:modelGeoLoc.cols-1) + 1/2) * modelGeoLoc.cellsizeX),...
    modelGeoLoc.rows,1);
yGridModel = repmat((modelGeoLoc.top - ((0:modelGeoLoc.rows-1) - 1/2) * modelGeoLoc.cellsizeY)',...
    1,modelGeoLoc.cols);

%Read in the lu weights
luWeightsModel = h5dataread([workingDir,filesep,modelLuFile],'/landuse/weights');

%Reshape the match appropriate dimensions
luWeightsModel = reshape(luWeightsModel,modelGeoLoc.rows,modelGeoLoc.cols,length(luMap));

%Generate a lookup table to be used for the downscaled data
modelLookup = reshape((1:prod([modelGeoLoc.rows,modelGeoLoc.cols])),modelGeoLoc.rows,modelGeoLoc.cols);
modelLookup = uint32(modelLookup);

%--------------------------------------------------------------------------
%Loop through the datasets
%--------------------------------------------------------------------------
for m = 1:length(datatype)
    for n = 1:length(sources.(datatype{m}).data)
        %Read in the grids and the header metadata
        geoLoc = import_grid(sources.(datatype{m}).grid{n},'header');

        %Now, run zonal statistics to identify prototype points
        luGeoLoc.nan = 0; %this will be treated as a NaN value for the purposes of the zonal statistics calculation
        [tempStats,mask] = cell_statistics(geoLoc,luGeoLoc,luGrid,'partition','weight'); %#ok<NASGU>

        %Calculate percentages of land use in each ndvi cell
        protoPts = false(geoLoc.rows,geoLoc.cols,length(luMap));
        for o = 1:length(luMap)
            match = find(str2double(luMap(o).integer) == tempStats.partition.index);
            protoPts(:,:,o) = tempStats.partition.array{match} > ...
                sources.(datatype{m}).threshold{n}(o);
        end

        %Clean up some memory
        clear tempStats

        %Define the x and y grids
        xGrid = repmat((geoLoc.left + ((0:geoLoc.cols-1) + 1/2) * geoLoc.cellsizeX),...
            geoLoc.rows,1);
        yGrid = repmat((geoLoc.top - ((0:geoLoc.rows-1) - 1/2) * geoLoc.cellsizeY)',...
            1,geoLoc.cols);

        %Now, load the dataset
        tempDatafile = [workingDir,filesep,sources.(datatype{m}).data{n}];
        tempGroup = ['/',sources.(datatype{m}).name.in{n}];
        [data,dataAttribs] = h5dataread(tempDatafile,[tempGroup,'/data']);
        index = h5dataread(tempDatafile,[tempGroup,'/index']);
        lookup = h5dataread(tempDatafile,[tempGroup,'/lookup'],false);

        %Remove any NaN values from the indeces
        index = index(index(:,2)>0,:);

        %Create the output variable in the HDF5 file, write index and
        %lookup tables
        outFile = [workingDir,filesep,outFilename];
        outGroup = ['/',sources.(datatype{m}).name.out{n}];

        %Create the group
        h5groupcreate(outFile,outGroup);

        %Write the index table
        attribs = struct('class',class(index));
        h5datawrite(outFile,[outGroup,'/index'],index,attribs)

        %Write out the stationInfo table
        attribs = struct('class',class(modelLookup));
        h5datawrite(outFile,[outGroup,'/lookup'],modelLookup,attribs);

        %Build the correct dataAttrib
        dataAttribs.offset = '0';
        dataAttribs.scale = '10';
        dataAttribs.class = 'uint8';
        dataAttribs.units = '1';
        dataAttribs.nan = '255';

        %Now, create an output array of the appropriate size, this will be
        %populated in the loop below
        h5varcreate(outFile,[outGroup,'/data'],[size(data,1),prod([modelGeoLoc.rows,modelGeoLoc.cols])],...
            'uint8',true);

        %Write the output attributes
        h5attput(outFile,[outGroup,'/data/scale'],dataAttribs.scale);
        h5attput(outFile,[outGroup,'/data/units'],dataAttribs.units);
        h5attput(outFile,[outGroup,'/data/nan'],dataAttribs.nan);

        %Loop through the rows of the dataset
        h = waitbar(0,['Looping through rows of the ',sources.(datatype{m}).name.in{n},' dataset']);
        for o = 1:size(data,1)
            waitbar(o/size(data,1),h);

            %Slice out a single scene and reshape to the original
            %dimensions
            loopData = reshape(data(o,:),geoLoc.rows,geoLoc.cols);

            %Loop through the land use classes, except water and barren
            loopInterp = zeros(modelGeoLoc.rows,modelGeoLoc.cols,size(protoPts,3));
            for p = 1:size(protoPts,3)
                if luMap(p).interp
                    %Select data values at prototype points, x and y too
                    loopSelData = loopData(protoPts(:,:,p));
                    loopX = xGrid(protoPts(:,:,p));
                    loopY = yGrid(protoPts(:,:,p));

                    %Replace any points that are NaN
                    test = ~isnan(loopSelData);
                    loopSelData= loopSelData(test);
                    loopX = loopX(test);
                    loopY = loopY(test);

                    %interpolate to the full grid
                    if length(loopSelData) > 2
                        loopInterp(:,:,p) = griddata(loopX,loopY,loopSelData,xGridModel,yGridModel,'nearest');
                    else
                        loopInterp(:,:,p) = mean(loopSelData);
                    end
                end
            end

            %Composite the values based on their weights
            loopComposite = sum(loopInterp .* luWeightsModel,3)./ sum(luWeightsModel,3);
            loopComposite(isnan(loopComposite)) = 0;

            %Calculate the upscaled average
            tempComposite = loopComposite;
            tempComposite(tempComposite==0) = NaN;
            [downscaledMean,mask] = cell_statistics(geoLoc,modelGeoLoc,tempComposite,'mean','weight'); %#ok<NASGU>

            %Determine the raio between the original data and this new
            %downscaled mean
            ratio = ones(size(loopData));
            test = ~(downscaledMean<0.1) & ~isnan(downscaledMean);
            ratio(test) = loopData(test) ./ downscaledMean(test);

            %Reshape to the model coordinates
            ratio = ratio(lookup);

            %Manually restrict the ratio to no more than 3, problems are
            %caused by boundaries
            maxRatio = 3;
            ratio(ratio > maxRatio) = maxRatio;

            %Handle LAI and NDVI differently, NDVI must first be converted
            %to LAI
            if strcmpi(datatype{m},'ndvi')
                for p = 1:size(loopInterp,3)
                    %Apply this ratio to the individual LU ndvi maps
                    loopInterp(:,:,p) = loopInterp(:,:,p) .* ratio;

                    %Now, apply the ndvi->LAI Function
                    loopInterp(:,:,p) = params{p}(1) * exp(params{p}(2) * loopInterp(:,:,p));

                    %Limit values to those obtained in the data
                    loopInterp(:,:,p) = limit_vals(rangeLai{p},loopInterp(:,:,p));
                end
                adjComposite = sum(loopInterp .* luWeightsModel,3) ./ sum(luWeightsModel,3);
            else
                %Apply this ratio to the downscaled data
                adjComposite = loopComposite .* ratio;

                %Manually limit the values of adjComposite to the range in
                %loopComposite
                adjComposite = limit_vals([min(loopComposite(:)),max(loopComposite(:))],adjComposite);
            end

            %Reshape this array for writing
            adjComposite = reshape(adjComposite,1,[]);

            %Finally, write out this dataset to the appropriate row
            h5datawrite(outFile,[outGroup,'/data'],adjComposite,dataAttribs,true,...
                [o-1,0],[1,size(adjComposite,2)],[1,1]);

            %Clear some loop variables
            clear loopComposite ratio downscaledMean loopInterp loopSelData loopX loopY adjComposite loopData
        end
        if ishandle(h); close(h); end     

        %Clear up some memory
        clear protoPts data
    end
end

%Turn more back on
more on
end
