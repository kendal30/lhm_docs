function ndvi_lai_function(workingDir,outFilename,sources,grids)
%NDVI_LAI_FUNCTION  Creates NDVI-LAI conversion functions
%   ndvi_lai_function()
%
%   Descriptions of Input Variables:
%   none, all are specified below
%
%   Descriptions of Output Variables:
%   none, saved out to intermediate .mat file
%
%   Example(s):
%   >> ndvi_lai_function
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-23
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
luGridFile = sources.luFile;
ndviGrid = grids.avhrr.clip;
laiGrid = grids.modis.clip;

luMap = grids.lu.lu.map;

%--------------------------------------------------------------------------
%Import grid headers, convert to proper format
%--------------------------------------------------------------------------
%Read in the grids and their header metadata
luGeoLoc = import_grid(luGridFile,'header'); %all LU grids must have the same header
ndviGeoLoc = import_grid(ndviGrid,'header');
laiGeoLoc = import_grid(laiGrid,'header');

%--------------------------------------------------------------------------
%Load the land use grid, perform zonal statistics, select prototype points
%--------------------------------------------------------------------------
%Import the LU grid
[luGrid] = import_grid(luGridFile);

%Clean up the grid
luGrid(luGrid == 255) = 0;
luGrid(luGrid < 0) = 0;
luGrid = uint8(luGrid);

%Now, run zonal statistics
luGeoLoc.nan = 0; %this will be treated as a NaN value for the purposes of the zonal statistics calculation
tempStats = cell_statistics(ndviGeoLoc,luGeoLoc,luGrid,{'partition'},{'weight'});

%Calculate percentages of land use in each ndvi cell
luWeights = zeros(ndviGeoLoc.rows,ndviGeoLoc.cols,length(luMap));
for m = 1:length(luMap)
    match = find(str2double(luMap(m).integer) == tempStats.partition.index);
    luWeights(:,:,m) = tempStats.partition.array{match};
end

%Select prototype points, and reshape to match the ndvi and lai data
protoPts = luWeights > sources.protoThresh;
protoPts = reshape(protoPts,1,prod([ndviGeoLoc.rows,ndviGeoLoc.cols]),length(luMap));

%Clean up some memory
clear tempStats luWeights

%--------------------------------------------------------------------------
%Load data, and select only portion of data within overlap period
%--------------------------------------------------------------------------
%Load data
ndvi = h5dataread([workingDir,filesep,sources.ndvi.data],['/',sources.ndvi.name,'/data']);
ndviIndex = h5dataread([workingDir,filesep,sources.ndvi.data],['/',sources.ndvi.name,'/index']);
lai = h5dataread([workingDir,filesep,sources.lai.data],['/',sources.lai.name,'/data']);
laiIndex = h5dataread([workingDir,filesep,sources.lai.data],['/',sources.lai.name,'/index']);

%Select overlap period
ndviIndex = ndviIndex(ndviIndex(:,2)>0,:);
laiIndex = laiIndex(laiIndex(:,2)>0,:);
ndviRange = [min(ndviIndex(:,1)),max(ndviIndex(:,1))];
laiRange = [min(laiIndex(:,1)),max(laiIndex(:,1))];
range = [max(ndviRange(1),laiRange(1)),min(ndviRange(2),laiRange(2))];
ndviBracket = bracket_index(ndviIndex(:,1),range(1),range(2));
laiBracket = bracket_index(laiIndex(:,1),range(1),range(2));

%Trim the index arrays and data arrays
ndviIndex = ndviIndex(ndviBracket(1):ndviBracket(2),:);
laiIndex = laiIndex(laiBracket(1):laiBracket(2),:);
ndvi = ndvi(ndviIndex(1,2):ndviIndex(end,2),:);
lai = lai(laiIndex(1,2):laiIndex(end,2),:);

%--------------------------------------------------------------------------
%Interpolate the LAI data to the NDVI grids using cell_statistics
%--------------------------------------------------------------------------
laiInterpSpace = zeros(size(lai,1),size(ndvi,2));
h = waitbar(0,'Spatially interpolating LAI grids to NDVI grid dimensions');
for m = 1:size(lai,1)
    waitbar(m/size(lai,1),h);
    tempLai = reshape(lai(m,:),laiGeoLoc.rows,laiGeoLoc.cols);
    [tempInterp,mask] = cell_statistics(ndviGeoLoc,laiGeoLoc,tempLai,'mean','weight');
    laiInterpSpace(m,:) = reshape(tempInterp,1,[]);
end
if ishandle(h); close(h); end
clear lai

%--------------------------------------------------------------------------
%Loop through the LU types, fitting an exponential function to the data
%--------------------------------------------------------------------------
%First, interpolate the lai values to match the ndvi array
laiInterp = interp1(laiIndex(:,1),laiInterpSpace,ndviIndex(:,1),'linear','extrap');
clear laiInterpSpace

numTypes = length(luMap);
params = cell(numTypes,1);
laiTest = cell(numTypes,1);
ndviTest = cell(numTypes,1);
for m = 1:numTypes
    %Next, select the points that are prototypes from each array
    ndviTemp = ndvi(:,protoPts(1,:,m));
    laiTemp = laiInterp(:,protoPts(1,:,m));

    %Now, select about 150 of those columns, randomly
    if size(ndviTemp,2) > 150
        randSel = rand_index(size(ndviTemp),2,150);
        ndviTemp = ndviTemp(:,randSel);
        laiTemp = laiTemp(:,randSel);
    end

    %We only want ~nan values
    nanNdvi = isnan(ndviTemp);
    nanLai = isnan(laiTemp);
    test = ~nanNdvi & ~nanLai;

    %Fit a logarithmic function to these data
    if ~isempty(test)
        objective_function('initialize','exp_func',ndviTemp(test),laiTemp(test),'MAE');
        tempParams = fminsearchbnd('objective_function',[0.5,0.5],[0,0],[100,100]);
    else
        tempParams = [0.5,0.5];
    end

    %Plot these
    subplot(3,3,m)
    plot(ndviTemp(test),laiTemp(test),'.');
    hold on
    x = (0:0.01:max(ndviTemp(test)));
    plot(x,exp_func(x,tempParams),'r-');

    %Set the bounds
    axis([0,1,0,7]);

    %Do some other formatting
    title(luMap(m).name)
    if m == 1
        ylabel('LAI');
        xlabel('NDVI');
    end

    %Save the data and output
    params{m} = tempParams;
    laiTest{m} = laiTemp(test);
    ndviTest{m} = ndviTemp(test);
end

save([workingDir,filesep,outFilename],'params','laiTest','ndviTest');
