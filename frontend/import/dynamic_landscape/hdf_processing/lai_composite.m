function lai_composite(workingDir,outFilename,modelHours,sources)
%LAI_COMPOSITE  Composites different LAI estimates into one continuous dataset
%   lai_composite()
%
%   Descriptions of Input Variables:
%   none, all are specified below and read from intermediate HDF5 files
%
%   Descriptions of Output Variables:
%   none, all are written to an output HDF5 file
%
%   Example(s):
%   >> lai_composite
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-01
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
outType = sources.composite.lai.class;
outMult = sources.composite.lai.scale;
nanVal = sources.composite.lai.nan;
units = sources.composite.lai.units;
offset = sources.composite.lai.offset;

%--------------------------------------------------------------------------
%Read in just the indices to determine when a given dataset will be used
%--------------------------------------------------------------------------
%First, create the priority index
numHours = length(modelHours);
priorityIndex = [modelHours,repmat(-1,numHours,1),repmat(0,numHours,1)];

numSources = length(sources.lai.name);
index = cell(numSources,1);
for m = 1:numSources
    %For convenience, store the name of this dataset
    dataset = sources.lai.name{m};
    
    %Open the index
    index{m} = h5dataread([workingDir,filesep,sources.lai.data{m}],['/',dataset,'/index']);
    
    %Trim the index to only defined data
    index{m} = index{m}(index{m}(:,2)>0,:);
    
    %Determine the min/max range of the indices in this dataset
    range = [min(index{m}(:,1)),max(index{m}(:,1))];
    
    %Compare this range to the priority index to determine which portion of
    %it will be used
    rangeHours = (range(1):1/24:range(2))';
    testMember = ismember_dates(priorityIndex(:,1),rangeHours,4,true);
    
    %For all instances in which this dataset is highest priority, set the
    %index accordingly with this datasets' integer value
    highest = sources.lai.priority(m) > priorityIndex(:,2);
    priorityIndex(highest & testMember,2) = sources.lai.priority(m);
    priorityIndex(highest & testMember,3) = m;
end

%--------------------------------------------------------------------------
%Build the index for the output dataset
%--------------------------------------------------------------------------
%Remove the priority column, it's no longer needed
priorityIndex = priorityIndex(:,[1,3]);

%Insert a new column at the end that will correspond to the rows of the
%appropriate datafile to read in 
priorityIndex = [priorityIndex,zeros(size(priorityIndex,1),1)];

for m = 1:numSources
    %Determine the dates that will be used by this dataset
    test = (priorityIndex(:,2) == m);
    dates = priorityIndex(test,1);
    
    %Calculate which of the dates in this dataset fall within this range
    thisDates = ismember_dates(index{m}(:,1),dates,4,true);
    keepDates = ismember_dates(dates,index{m}(thisDates,1),4,true);
    
    %Split the index into two pieces that will be reassembled later
    otherIndex = priorityIndex(~test,:);
    thisIndex = priorityIndex(test,:);
    thisIndex(~keepDates,:) = [];
    
    %Insert the rows from this datasets' index into thisIndex
    thisIndex(:,3) = index{m}(thisDates,2);
    
    %Reassemble the priorityIndex
    priorityIndex = sortrows([otherIndex;thisIndex]);
end

%Remove any values that are missing
priorityIndex = priorityIndex(priorityIndex(:,3)~=0,:);

%--------------------------------------------------------------------------
%Create the output dataset, assign its attributes, and write its index and
%lookup arrays
%--------------------------------------------------------------------------
%First, determine the size of the output dataset
dataSize = h5varget([workingDir,filesep,sources.lai.data{1}],['/',sources.lai.name{1},'/data'],false);
outSize = [size(priorityIndex,1),dataSize(2)];

%Create the output group
outFile = [workingDir,filesep,outFilename];
outGroup = ['/',sources.composite.lai.name];
h5groupcreate(outFile,outGroup);

%Create the output dataset
h5varcreate(outFile,[outGroup,'/data'],outSize,outType,true);

%Assign it the appropriate attributes
attribs = struct('nan',nanVal,'scale',outMult,'units',units,'offset',offset);

attribList = fieldnames(attribs);
for m = 1:length(attribList)
    h5attput(outFile,[outGroup,'/data/',attribList{m}],attribs.(attribList{m}));
end

%Create the output index
outIndex = [priorityIndex(:,1),(1:outSize(1))'];
attribs = struct('class',class(outIndex));
h5datawrite(outFile,[outGroup,'/index'],outIndex,attribs);

%Read in one of the lookup tables only, they are all identical as they were
%created by lai_ndvi_downscaling
lookup = h5dataread([workingDir,filesep,sources.lai.data{1}],['/',sources.lai.name{1},'/lookup'],false);
%Write it out as a 32-bit integer
attribs = struct('class',class(lookup));
h5datawrite(outFile,[outGroup,'/lookup'],lookup,attribs,false);

%--------------------------------------------------------------------------
%Read in the respective datasets and write them to the output file
%--------------------------------------------------------------------------
%First, break the priorityIndex into chunks of the identical datatype
blocks = [0;find(diff(priorityIndex(:,2))~=0);size(priorityIndex,1)];

%Then, read in each block of data and write it to the output file
for m = 1:length(blocks)-1
    dataNum = priorityIndex(blocks(m)+1,2);
    
    %Build the start and count vectors to use for reading
    rows = priorityIndex(blocks(m)+1:blocks(m+1),3);
    start = [min(rows) - 1,0];
    count = [size(rows,1),outSize(2)];
    stride = [1,1];
    
    %Read in the dataset
    [data,dataAttribs] = h5dataread([workingDir,filesep,sources.lai.data{dataNum}],...
        ['/',sources.lai.name{dataNum},'/data'],false,start,count,stride);
    
    %Build the start vector to use for writing
    start = [blocks(m)+1-1,0];
    
    %Write the dataset
    dataAttribs.class = class(data);
    h5datawrite(outFile,[outGroup,'/data'],data,dataAttribs,false,start,count,stride);
end

