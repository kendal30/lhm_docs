function ndvi_adjust_gimms(workingDir,outFilename,sources)
%NDVI_ADJUST_GIMMS  Adjusts the finer 1-km NDVI to match the averages of the GIMMS product
%   ndvi_adjust_gimms()
%
%   Descriptions of Input Variables:
%   none, all are specified below or read in
%
%   Descriptions of Output Variables:
%   none, all are written out to an intermediate HDF5 file
%
%   Example(s):
%   >> ndvi_adjust_gimms
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-18
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Read in the data, and read in the clip grid headers as well
%--------------------------------------------------------------------------
%Read in the NDVI 1-km data
[ndvi,ndviAttribs] = h5dataread([workingDir,filesep,sources.ndvi.data],['/',sources.ndvi.name.in,'/data']);
ndviIndex = h5dataread([workingDir,filesep,sources.ndvi.data],['/',sources.ndvi.name.in,'/index']);
ndviLookup = h5dataread([workingDir,filesep,sources.ndvi.data],['/',sources.ndvi.name.in,'/lookup'],false);

%read in the GIMMS data
[gimms,gimmsAttribs] = h5dataread([workingDir,filesep,sources.gimms.data],['/',sources.gimms.name.in,'/data']);
gimmsIndex = h5dataread([workingDir,filesep,sources.gimms.data],['/',sources.gimms.name.in,'/index']);
gimmsLookup = h5dataread([workingDir,filesep,sources.gimms.data],['/',sources.gimms.name.in,'/lookup'],false);

%Remove the NaN values from the indeces
ndviIndex = ndviIndex(ndviIndex(:,2)>0,:);
gimmsIndex = gimmsIndex(gimmsIndex(:,2)>0,:);

%Read in the headers from the clip grids
ndviGeoLoc = import_grid(sources.ndvi.clipGrid,'header');
gimmsGeoLoc = import_grid(sources.gimms.clipGrid,'header');

%Also, calculate a lookup table from the finer to coarser data
[lookup,mask] = cell_statistics(gimmsGeoLoc,ndviGeoLoc);

%--------------------------------------------------------------------------
%Loop through the AVHRR grids
%--------------------------------------------------------------------------
ndviReweight = zeros(size(ndvi),class(ndvi));
numGrids = size(ndvi,1);
h = waitbar(0,'Reweighting AVHRR NDVI grids');
for m = 1:numGrids
    waitbar(m/numGrids,h);
    tempTime = ndviIndex(m,1);
    tempNdvi = ndvi(m,:);
    tempNdvi = reshape(tempNdvi,ndviGeoLoc.rows,ndviGeoLoc.cols);

    %Calculate an interpolated NDVI grid from the GIMMS dataset based on
    %the times of the corresponding indexes
    prevGimmsInd = find(tempTime >= gimmsIndex(:,1),1,'last');

    prevGimms = gimms(gimmsIndex(prevGimmsInd,2),:);
    nextGimms = gimms(gimmsIndex(prevGimmsInd+1,2),:);

    prevGimmsTime = gimmsIndex(prevGimmsInd,1);
    nextGimmsTime = gimmsIndex(prevGimmsInd+1,1);

    %Check to make sure that the difference between the current NDVI grid
    %time and either of the previous times does not exceed 30 days
    testPrev = (tempTime - prevGimmsTime) < 30;
    testNext = (nextGimmsTime - tempTime) < 30;
    %Build the tempGimms array based on these two test conditions
    if testPrev && testNext
    tempGimms = prevGimms + (nextGimms - prevGimms) * ...
        (tempTime - prevGimmsTime) / (nextGimmsTime - prevGimmsTime);
    elseif testPrev
        tempGimms = prevGimms;
    elseif testNext
        tempGimms = nextGimms;
    end
    %If either of the tests is passed, reshape the tempGimms array, or make
    %an all NaNs array.  This will have the effect of performing no
    %reweighting on the data
    if testPrev || testNext
        tempGimms = reshape(tempGimms,gimmsGeoLoc.rows,gimmsGeoLoc.cols);
    else
        tempGimms = zeros(gimmsGeoLoc.rows,gimmsGeoLoc.cols) + NaN;
    end

    %Calculate the average of the current NDVI grid within the GIMMS grid
    %cells
    zoneStat = cell_statistics(gimmsGeoLoc,ndviGeoLoc,tempNdvi,'mean','weight');

    %Reweight the NDVI grid by dividing the current mean and multiplying by
    %tempGimms, only do this for non NaN cells in gimms
    weight = tempGimms - zoneStat;

    %Now, smooth the weight grid using a gaussian filtered smooth function
    weight = weight(lookup);
    weightSmooth = smoothn(weight,[5,5],'gaussian');

    %Fill in any nan values that were created by smoothing with the
    %unsmoothed grid
    test = isnan(weightSmooth) & ~isnan(weight);
    weightSmooth(test) = weight(test);

    %Replace any remaining NaN values with 0
    weightSmooth(isnan(weightSmooth)) = 0;

    %Reweight tempNdvi
    tempNdvi(mask) = tempNdvi(mask) + reshape(weightSmooth,[],1);

    %Write the reweighted data to the new ndvi array
    ndviReweight(m,:) = tempNdvi(:);
end
if ishandle(h); close(h); end

%--------------------------------------------------------------------------
%Write the data out to a new file, including GIMMS even though there were
%no changes
%--------------------------------------------------------------------------
outFile = [workingDir,filesep,outFilename];
ndviAttribs.class = sources.ndvi.class;
import_write_dataset(outFile,['/',sources.ndvi.name.out],ndviReweight,ndviAttribs,ndviIndex,false,ndviLookup);
gimmsAttribs.class = sources.gimms.class;
import_write_dataset(outFile,['/',sources.gimms.name.out],gimms,gimmsAttribs,gimmsIndex,false,gimmsLookup);
end
