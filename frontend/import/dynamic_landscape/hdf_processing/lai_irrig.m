function lai_irrig(workingDir,outFilename,sources)
%LAI_IRRIG  Produces an irrigated LAI product
%   lai_irrig()
%
%   Descriptions of Input Variables:
%   none, all are specified below
%
%   Descriptions of Output Variables:
%   none, all are written to an intermediate hdf5 file
%
%   Example(s):
%   >> lai_downscaling
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2017-12-1
% Copyright 2017 Michigan State University.


%The interpolation method sometimes throws warnings to screen that cannot
%be deactivated
more off

%--------------------------------------------------------------------------
%Load the land use grid and read in the header info
%--------------------------------------------------------------------------
%Read in the LAI dataset
lai = h5dataread([workingDir,filesep,sources.lai.data],['/',sources.lai.name,'/data']);
laiIndex = h5dataread([workingDir,filesep,sources.lai.data],['/',sources.lai.name,'/index']);
laiLookup = h5dataread([workingDir,filesep,sources.lai.data],['/',sources.lai.name,'/lookup']);
numLAI = size(lai,1);
limLAI = [min(lai(:)),max(lai(:))];

%Read in the irrigation dataset
irrigWeights = h5dataread([workingDir,filesep,sources.irrigTech.data],['/',sources.irrigTech.name,'/weights']);
irrigIndex = h5dataread([workingDir,filesep,sources.irrigTech.data],['/',sources.irrigTech.name,'/index']);
irrigLookup = h5dataread([workingDir,filesep,sources.irrigTech.data],['/',sources.irrigTech.name,'/lookup']);
irrigWeights = sum(irrigWeights,3); %use all irrigation types

%Read in the landuse dataset
luWeights = h5dataread([workingDir,filesep,sources.lu.data],['/',sources.lu.name,'/weights']);
luIndex = h5dataread([workingDir,filesep,sources.lu.data],['/',sources.lu.name,'/index']);
luLookup = h5dataread([workingDir,filesep,sources.lu.data],['/',sources.lu.name,'/lookup']);
luWeights = sum(luWeights(:,:,sources.lu.include),3); %only use the specific landuse classes

%Define the x and y grids, just use model i,j
modelSize = size(luLookup);
xGrid = repmat((1:modelSize(1))',[1,modelSize(2)]);
yGrid = repmat((1:modelSize(2)),[modelSize(1),1]);

%--------------------------------------------------------------------------
%Loop through the lai dataset, creating the irrigated LAI array
%--------------------------------------------------------------------------
%Define proto points for the lu and irrigation datasets for all times
protoPtsIrrig = irrigWeights > sources.threshold.irrigMin;
protoPtsLU = luWeights > sources.threshold.nonIrrigAgMin;

%Create loop variables, and the waitbar
[prevIndIrrig,prevIndLU] = deal(0);
laiIrrig = zeros([numLAI,prod(modelSize)]);
h = waitbar(0,'Calculating irrigated LAI');
for m = 1:numLAI
    waitbar(m/numLAI,h);
     
    %Get this LAI as a grid, rather than a vector
    thisLAI = lai(m,:);
    thisLAI = thisLAI(laiLookup);
    
    %Determine the irrigation and LU indices for this LAI time
    indIrrig = find(laiIndex(m,1)>=irrigIndex(:,1) & laiIndex(m,1)<irrigIndex(:,2),1,'first');
    indLU = find(laiIndex(m,1)>=luIndex(:,1) & laiIndex(m,1)<luIndex(:,2),1,'first');
    
    %Select our proto pts and create the interpolation objects if different
    %than previous, first for Irrig
    if indIrrig ~= prevIndIrrig
        prevIndIrrig = indIrrig; %reset this
        
        thisProtoPtsIrrig = protoPtsIrrig(indIrrig,:); %extract out this row
        thisIrrigWeights = irrigWeights(indIrrig,:);
        
        thisProtoPtsIrrig = thisProtoPtsIrrig(irrigLookup); %convert to grid form
        thisIrrigWeights = thisIrrigWeights(irrigLookup);
        
        xIrrig = xGrid(thisProtoPtsIrrig); %get the x,y locations
        yIrrig = yGrid(thisProtoPtsIrrig); 
        dataIrrig = thisIrrigWeights(thisProtoPtsIrrig); %get the data
        
        %Make the interpolation objects, we use two in order to handle
        %sparse data cases better
        interpIrrigNearest = TriScatteredInterp(xIrrig,yIrrig,dataIrrig,'nearest');
        interpIrrigNatural = TriScatteredInterp(xIrrig,yIrrig,dataIrrig,'natural');
        
        %Interpolate weights and then composite
        protoWeightsIrrigNearest = interpIrrigNearest(xGrid,yGrid);
        protoWeightsIrrigNatural = interpIrrigNatural(xGrid,yGrid);
        test = isnan(protoWeightsIrrigNatural);
        protoWeightsIrrig = protoWeightsIrrigNatural;
        protoWeightsIrrig(test) = protoWeightsIrrigNearest(test);
    end
    
    %Now for LU, don't need to interpolate like above however, because we
    %don't need the prototype points weight grid
    if indLU ~= prevIndLU
        prevIndLU = indLU; %reset this
        
        thisProtoPtsLU = protoPtsLU(indLU,:); %extract out this row        
        thisProtoPtsLU = thisProtoPtsLU(luLookup); %convert to grid form
        
        xLU = xGrid(thisProtoPtsLU); %get the x,y locations
        yLU = yGrid(thisProtoPtsLU); 
    end
    
    %Get the LAI at the proto points
    thisLAIIrrig = thisLAI(thisProtoPtsIrrig);
    thisLAILU = thisLAI(thisProtoPtsLU);
    
    %Now, make the objects for LAI interpolation
    interpIrrigNearest = TriScatteredInterp(xIrrig,yIrrig,thisLAIIrrig,'nearest');
    interpIrrigNatural = TriScatteredInterp(xIrrig,yIrrig,thisLAIIrrig,'natural');
    interpLUNearest = TriScatteredInterp(xLU,yLU,thisLAILU,'nearest');
    interpLUNatural = TriScatteredInterp(xLU,yLU,thisLAILU,'natural');   
    
    %Interpolate LAI for both the Irrig and LU prototype points
    interpLAIIrrigNearest = interpIrrigNearest(xGrid,yGrid);
    interpLAIIrrigNatural = interpIrrigNatural(xGrid,yGrid);
    interpLAILUNearest = interpLUNearest(xGrid,yGrid);
    interpLAILUNatural = interpLUNatural(xGrid,yGrid);
    
    %Composite as before
    test = isnan(interpLAIIrrigNatural);
    interpLAIIrrig = interpLAIIrrigNatural;
    interpLAIIrrig(test) = interpLAIIrrigNearest(test);
    test = isnan(interpLAILUNatural);
    interpLAILU = interpLAILUNatural;
    interpLAILU(test) = interpLAILUNearest(test);
    
    %Assure that interpLAIIrrig >= interpLAILU
    interpLAIIrrig = max(interpLAILU,interpLAIIrrig);
    
    %Now, correct for the Irrigation weight not equaling 1
    %Assume that interpLAIIrrig = correctLAIIrrig .* protoWeightsIrrig + interpLAILU .* (1 - protoWeightsIrrig)
    correctLAI = (interpLAIIrrig - interpLAILU .* (1-protoWeightsIrrig)) ./ protoWeightsIrrig;
    testNan = isnan(correctLAI); %only happens when the protoWeightsIrrig == 0, which really shouldn't happen
    correctLAI(testNan) = interpLAILU(testNan);
    
    %Assume that laiIrrig can never be less than the original LAI
    %for that cell
    correctLAI = max(correctLAI,thisLAI);
    
    %Truncate values to be within range of the LAI dataset
    correctLAI = limit_vals(limLAI,correctLAI);
    
    %Save this in the loop array
    laiIrrig(m,:) = correctLAI(:);
end
if ishandle(h);close(h);end

%--------------------------------------------------------------------------
%Write the output and finish up
%--------------------------------------------------------------------------
% %Create the output group
% h5groupcreate([workingDir,filesep,outFilename],['/',sources.laiIrrig.name]);
% 
% %Write the data array
% laiAttrib.class = class(laiIrrig);
% h5datawrite([workingDir,filesep,outFilename],['/',sources.laiIrrig.name,'/data'],laiIrrig,laiAttrib,true);

outFile = [workingDir,filesep,outFilename];
outGroup = ['/',sources.laiIrrig.name];
laiAttrib.class = class(laiIrrig);
import_write_dataset(outFile,outGroup,laiIrrig,laiAttrib,laiIndex,false,laiLookup,true);

%Turn more back on
more on
end