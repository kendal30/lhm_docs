function landscape_hdf_write(workingDir,modelHours,landscapeName,landscapeVersion,IDType,compileDate,sources)
%LANDSCAPE_HDF_WRITE  Writes the LAND HDF5 file
%   landscape_hdf_write(input)
%
%   Descriptions of Input Variables:
%   none, all are specified below
%
%   Descriptions of Output Variables:
%   none, all are written to HDF5 file
%
%   Example(s):
%   >>
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-11
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
startDate = datestr(modelHours(1),'yyyy-mm-dd HH:MM:SS');
endDate = datestr(modelHours(end),'yyyy-mm-dd HH:MM:SS');

%--------------------------------------------------------------------------
%Create the output filename
%--------------------------------------------------------------------------
%Create the serialID and outFile name
serialID = serial_ID_gen(IDType,landscapeName,landscapeVersion,compileDate);
outFilename = [IDType,'_',landscapeName,'_',landscapeVersion,'_',datestr(compileDate,'yyyymmdd'),'.h5'];
outFile = [workingDir,filesep,outFilename];

%--------------------------------------------------------------------------
%Create the info group and write info to it
%--------------------------------------------------------------------------
h5groupcreate(outFile,'/info');
attribs = struct('class','char');
h5datawrite(outFile,'/info/landscape_name',landscapeName,attribs);
h5datawrite(outFile,'/info/landscape_version',landscapeVersion,attribs);
h5datawrite(outFile,'/info/compile_date',datestr(compileDate,'yyyy-mm-dd'),attribs);
h5datawrite(outFile,'/info/serial_ID',serialID,attribs);
h5datawrite(outFile,'/info/time_start',startDate,attribs);
h5datawrite(outFile,'/info/time_end',endDate,attribs);

%--------------------------------------------------------------------------
%Build the basic indexes for both LAI and LU
%--------------------------------------------------------------------------
modelHours = (datenum(startDate):1/24:datenum(endDate))';
[luIndex,ftIndex,impervIndex,irrigLimIndex,irrigTechIndex] = deal([modelHours,zeros(size(modelHours))]);
laiIndex = [modelHours,zeros(size(modelHours,1),4)];

%--------------------------------------------------------------------------
%Write out the LU dataset
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,sources.lu.data];
tempGroup = ['/',sources.lu.name];

% Read in the data
lu = h5datareadlhm(inFile,tempGroup,false);
assert(isfield(lu.attribs,'nan'),'Nan val not defined for lu.weights');

%Read in the majority  attribs
[~,majAttrib] = h5dataread(inFile,[tempGroup,'/majority'],false);
%Check the attributes to make sure they are properly formatted
assert(isfield(majAttrib,'nan'),'Nan val not defined for lu.majority');

%Build the properly formatted index
numLU = size(lu.index,1);
%Identify blocks of model days covered by each LU map
for m = 1:numLU
    indBlock = bracket_index(floor(luIndex(:,1)),lu.index(m,1),lu.index(m,2));
    luIndex(indBlock(1):indBlock(2),2) = m;
end

%Make sure that all data rows are defined
assert(~any(luIndex(:,2) == 0),'A data row in the LU index has not been defined');

% Overwrite the index with this one
lu.index = luIndex;

% Write the data
write_group_weights(outFile,tempGroup,lu);

%Clear some items from memory
clear lu

%--------------------------------------------------------------------------
%Write out the FLOWTIMES dataset
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,sources.flowtimes.data];
tempGroup = ['/',sources.flowtimes.name];

% Read in the data
flowtimes = h5datareadlhm(inFile,tempGroup,false);

% Check the attributes to make sure they are properly formatted
assert(isfield(flowtimes.attribs,'nan'),'Nan val not defined for flowtimes.attribs');

% Build the properly formatted index
numFlowtimes = size(flowtimes.index,1);
% Identify blocks of model days covered by each flowtimes map
for m = 1:numFlowtimes
    indBlock = bracket_index(floor(ftIndex(:,1)),flowtimes.index(m,1),flowtimes.index(m,2));
    ftIndex(indBlock(1):indBlock(2),2) = m;
end

% Make sure that all data rows are defined
assert(~any(ftIndex(:,2) == 0),'A data row in the flowtimes index has not been defined');

% Overwrite the index with this one
flowtimes.index = ftIndex;

% Write the data
write_group_data(outFile,tempGroup,flowtimes);

%Clear some items from memory
clear flowtimes

%--------------------------------------------------------------------------
%Write out the IMPERVIOUS dataset
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,sources.imperv.data];
tempGroup = ['/',sources.imperv.name];

% Read in the data
impervious = h5datareadlhm(inFile,tempGroup,false);
assert(isfield(impervious.attribs,'nan'),'Nan val not defined for impervious.weights');

%Build the properly formatted index
numImperv = size(impervious.index,1);
%Identify blocks of model days covered by each impervious map
for m = 1:numImperv
    indBlock = bracket_index(floor(impervIndex(:,1)),impervious.index(m,1),impervious.index(m,2));
    impervIndex(indBlock(1):indBlock(2),2) = m;
end

%Make sure that all data rows are defined
assert(~any(impervIndex(:,2) == 0),'A data row in the Impervious index has not been defined');

% Overwrite the index with this one
impervious.index = impervIndex;

% Write the data
write_group_weights(outFile,tempGroup,impervious);

%Clear some items from memory
clear impervious

%--------------------------------------------------------------------------
%Write out the IRRIGATION_TECHNOLOGY dataset
%--------------------------------------------------------------------------
if sources.irrigTech.use
    inFile = [workingDir,filesep,sources.irrigTech.data];
    tempGroup = ['/',sources.irrigTech.name];
    
    % Read in the data
    irrigTech = h5datareadlhm(inFile,tempGroup,false);
    assert(isfield(irrigTech.attribs,'nan'),'Nan val not defined for irrigTech.weights');
    
    % Build the properly formatted index
    numIrrig = size(irrigTech.index,1);
    % Identify blocks of model days covered by each irrig map
    for m = 1:numIrrig
        indBlock = bracket_index(floor(irrigTechIndex(:,1)),irrigTech.index(m,1),irrigTech.index(m,2));
        irrigTechIndex(indBlock(1):indBlock(2),2) = m;
    end
    
    % Make sure that all data rows are defined
    assert(~any(irrigTechIndex(:,2) == 0),'A data row in the Irrig Tech index has not been defined');
    
    % Overwrite the index with this one
    irrigTech.index = irrigTechIndex;

    % Write the data
    write_group_weights(outFile,tempGroup,irrigTech);
    
    %Clear some items from memory
    clear irrigTech
end

%--------------------------------------------------------------------------
%Write out the IRRIGATION_LIMITATION dataset
%--------------------------------------------------------------------------
if sources.irrigLim.use
    inFile = [workingDir,filesep,sources.irrigLim.data];
    tempGroup = ['/',sources.irrigLim.name];
    
    % Read in the data
    limitation = h5datareadlhm(inFile,tempGroup,false);
    
    %Build the properly formatted index
    numIrrig = size(limitation.index,1);
    %Identify blocks of model days covered by each LU map
    for m = 1:numIrrig
        indBlock = bracket_index(floor(irrigLimIndex(:,1)),limitation.index(m,1),limitation.index(m,2));
        irrigLimIndex(indBlock(1):indBlock(2),2) = m;
    end

    %Make sure that all data rows are defined
    assert(~any(irrigLimIndex(:,2) == 0),'A data row in the limitation index has not been defined');
    
    % Overwrite the index with this one
    limitation.index = irrigLimIndex;

    % Write the data
    write_group_data(outFile,tempGroup,limitation);
    
    %Clear some items from memory
    clear limitation
end

%--------------------------------------------------------------------------
%Write out the LAI dataset
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,sources.lai.data];
tempGroup = ['/',sources.lai.name];

% Read in the data
lai = h5datareadlhm(inFile,tempGroup,false);

%Check to see if any rows in the LAI data are all nan, if so, remove them
%and renumber the index
badRows = all(lai.data==str2double(lai.attribs.nan),2);
if any(badRows)
    lai.data = lai.data(~badRows,:);
    lai.index = lai.index(~badRows,:);
    lai.index(:,2) = (1:size(lai,1));
end

%Build the properly formatted interpolation index
numHours = length(modelHours);
for m = 1:numHours
    dateDiff = laiIndex(m,1) - lai.index(:,1);
    
    %Determine the index of the previous and next grid dates
    gridPrev = find(dateDiff > 0,1,'last');
    if isempty(gridPrev) %this is the first model Hour
        gridPrev = 0;
    end
    gridNext = gridPrev + 1;
    if gridNext > size(lai.index,1)
        gridNext = 0;
    end
    
    %Determine the difference between this date and the previous and next
    %dates
    [diffPrev,diffNext] = deal(NaN);
    if gridPrev > 0
        diffPrev = abs(dateDiff(gridPrev));
    end
    if gridNext > 0
        diffNext = abs(dateDiff(gridNext));
    end
    
    %Don't want any values out of range here
    if gridPrev == 0, gridPrev = 1;end
    if gridNext == 0, gridNext = gridPrev;end
    
    %Now, determine the weight of each grid for interpolation purposes
    [weightPrev,weightNext] = deal(0);
    if isnan(diffPrev)
        weightNext = 1;
    elseif isnan(diffNext)
        weightPrev = 1;
    else
        weightPrev = 1 - diffPrev / (diffPrev + diffNext);
        weightNext = 1 - weightPrev;
    end
    
    %Finally, build the index itself
    laiIndex(m,2:end) = [gridPrev,gridNext,weightPrev,weightNext];
end
% Overwrite the index with the interpolation index
lai.index = laiIndex;

% Write the data
write_group_data(outFile,tempGroup,lai);

%--------------------------------------------------------------------------
%Write out the Irrigated LAI dataset
%--------------------------------------------------------------------------
if sources.laiIrrig.use
    inFile = [workingDir,filesep,sources.laiIrrig.data];
    tempGroup = ['/',sources.laiIrrig.name];
    
    % Read in the data
    [laiIrrig] = h5datareadlhm(inFile,tempGroup,false);
    
    % Going to use the LAI index, so select the rows from this to match
    if any(badRows)
        laiIrrig.data = laiIrrig.data(~badRows,:);
    end
    
    % Write the data
    write_group_data(outFile,tempGroup,laiIrrig);
end

% Display a final status message pointing to the written file
fprintf('LAND file %s written to %s',outFilename,workingDir)
end


function write_group_data(outFile,outGroup,outStruct)
    % Create the group
    h5groupcreate(outFile,outGroup);
    % Write the index table
    attribs = struct('class',class(outStruct.index));
    h5datawrite(outFile,[outGroup,'/index'],outStruct.index,attribs,false);
    % Write the lookup array
    attribs = struct('class',class(outStruct.lookup));
    h5datawrite(outFile,[outGroup,'/lookup'],outStruct.lookup,attribs,false);
    % Write out the limitations array
    h5datawrite(outFile,[outGroup,'/data'],outStruct.data,outStruct.attribs,false);
end

function write_group_weights(outFile,outGroup,outStruct)
    % Create the group
    h5groupcreate(outFile,outGroup);
    % Write the index table
    attribs = struct('class',class(outStruct.index));
    h5datawrite(outFile,[outGroup,'/index'],outStruct.index,attribs,false);
    % Write the lookup array
    attribs = struct('class',class(outStruct.lookup));
    h5datawrite(outFile,[outGroup,'/lookup'],outStruct.lookup,attribs,false);
    % Write out the names arrays
    attribs = struct('class','char');
    namesGroup = [outGroup,'/names'];
    h5groupcreate(outFile,namesGroup);
    for m = 1:size(outStruct.weights,3)
        h5datawrite(outFile,[namesGroup,'/',num2str(m)],outStruct.names{m},attribs);
    end
    % Write out the weights array
    h5datawrite(outFile,[outGroup,'/weights'],outStruct.weights,outStruct.attribs,false);
    if isfield(outStruct,'majority')
        % Write out the majority array
        attribs = struct('class',class(outStruct.majority),'nan','0','units','none');
        h5datawrite(outFile,[outGroup,'/majority'],outStruct.majority,attribs,false);
    end
end