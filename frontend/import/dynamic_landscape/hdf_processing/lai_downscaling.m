function lai_downscaling(workingDir,outFilename,sources,grids)
%LAI_DOWNSCALING  Downscales LAI using finer resolution land use
%   lai_downscaling()
%
%   Descriptions of Input Variables:
%   none, all are specified below
%
%   Descriptions of Output Variables:
%   none, all are written to an intermediate hdf5 file
%
%   Example(s):
%   >> lai_downscaling
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-29
% Copyright 2008 Michigan State University.


%The interpolation method sometimes throws warnings to screen that cannot
%be deactivated
more off

%--------------------------------------------------------------------------
%Load the land use grid and read in the header info
%--------------------------------------------------------------------------
%Define for convenience
numLuCats = length(grids.lu.lu.map);
numLuGrids = length(grids.lu.lu.grids);

%Import the LU grid
luGeoLoc = import_grid([grids.lu.lu.inDir,filesep,grids.lu.lu.grids{1}],'header');
luGrids = zeros(length(grids.lu.lu.grids),luGeoLoc.rows,luGeoLoc.cols);
for m = 1:numLuGrids
    luGrids(m,:,:) = import_grid([grids.lu.lu.inDir,filesep,grids.lu.lu.grids{m}]); %all LU grids must have the same header
end

%Clean up the grid
luGrids(luGrids == 255) = 0;
luGrids(luGrids < 0) = 0;
luGrids = uint8(luGrids);

%Read in the lu weights and the lu index
luWeightsModel = h5dataread([workingDir,filesep,sources.modelLuFile],'/landuse/weights');
luIndex = h5dataread([workingDir,filesep,sources.modelLuFile],'/landuse/index');
luLookup = h5dataread([workingDir,filesep,sources.modelLuFile],'/landuse/lookup');

%Reshape the match appropriate dimensions
luWeightsModel = reshape(luWeightsModel,[numLuGrids,size(luLookup),numLuCats]);

%--------------------------------------------------------------------------
%Load the model grid and lu weights for the model dimensions
%--------------------------------------------------------------------------
%Read in the grids and the header metadata
modelGeoLoc = import_grid(grids.model,'header');

%Define the model x/y grid
xGridModel = repmat((modelGeoLoc.left + ((0:modelGeoLoc.cols-1) + 1/2) * modelGeoLoc.cellsizeX),...
    modelGeoLoc.rows,1);
yGridModel = repmat((modelGeoLoc.top - ((0:modelGeoLoc.rows-1) - 1/2) * modelGeoLoc.cellsizeY)',...
    1,modelGeoLoc.cols);

%--------------------------------------------------------------------------
%Loop through the datasets
%--------------------------------------------------------------------------
%Define for clarity
datatype = sources.datatypes;
for m = 1:length(datatype)
    for n = 1:length(sources.(datatype{m}).data)
        %Read in the grids and the header metadata
        geoLoc = import_grid(sources.(datatype{m}).grid{n},'header');

        %Calculate percentages of land use in each cell
        protoPts = false(numLuGrids,geoLoc.rows,geoLoc.cols,numLuCats);

        for p = 1:numLuGrids %do this for each luGrid, even though we may
            %Now, run zonal statistics to identify prototype points
            luGeoLoc.nan = 0; %this will be treated as a NaN value for the purposes of the zonal statistics calculation
            [tempStats,mask] = cell_statistics(geoLoc,luGeoLoc,squeeze(luGrids(p,:,:)),'partition','weight'); %#ok<NASGU>

            for o = 1:numLuCats
                match = (str2double(grids.lu.lu.map(o).integer) == tempStats.partition.index);
                tempThreshold = sources.(datatype{m}).threshold{n}(o);
                protoPts(p,:,:,o) = tempStats.partition.array{match} > tempThreshold;
                numPts = sum(sum(protoPts(p,:,:,o)));
                %Gradually reduce the threshold until there are a few prototype
                %pts for each landcover
                while numPts == 0
                    tempThreshold = tempThreshold / 1.5;
                    protoPts(p,:,:,o) = tempStats.partition.array{match} > tempThreshold;
                    numPts = sum(sum(protoPts(p,:,:,o)));
                end
            end
        end

        %Clean up some memory
        clear tempStats

        %Define the x and y grids
        xGrid = repmat((geoLoc.left + ((0:geoLoc.cols-1) + 1/2) * geoLoc.cellsizeX),...
            geoLoc.rows,1);
        yGrid = repmat((geoLoc.top - ((0:geoLoc.rows-1) - 1/2) * geoLoc.cellsizeY)',...
            1,geoLoc.cols);

        %Now, load the dataset
        tempDatafile = [workingDir,filesep,sources.(datatype{m}).data{n}];
        tempGroup = ['/',sources.(datatype{m}).name.in{n}];
        [data,dataAttribs] = h5dataread(tempDatafile,[tempGroup,'/data']);
        index = h5dataread(tempDatafile,[tempGroup,'/index']);
        lookup = h5dataread(tempDatafile,[tempGroup,'/lookup'],false);

        %Remove any NaN values from the indeces
        index = index(index(:,2)>0,:);

        %Create the output variable in the HDF5 file, write index and
        %lookup tables
        outFile = [workingDir,filesep,outFilename];
        outGroup = ['/',sources.(datatype{m}).name.out{n}];

        %Create the group
        groupExist = h5groupcreate(outFile,outGroup);

        if groupExist
            %If it exists, must rename old file or stop
            get_overwrite_pref(outFile,outGroup);
            %If we're still going, that means the user decided to
            %overwrite
            %rename the current file
            [outFilePath,outFileName,outFileExt] = fileparts(outFile);
            oldFileName = ['old_',outFileName,outFileExt];
            oldFile = [outFilePath,filesep,oldFileName];
            if exist(oldFile,'file');
                delete(oldFile);
            end
            system(['ren ',outFile,' ',oldFileName]);

            %Create the group again, this will create the new file too
            h5groupcreate(outFile,outGroup);
        end

        %Write the index table
        attribs = struct('class',class(index));
        h5datawrite(outFile,[outGroup,'/index'],index,attribs)

        %Write out the stationInfo table
        attribs = struct('class',class(luLookup));
        h5datawrite(outFile,[outGroup,'/lookup'],luLookup,attribs);

        %Build the correct dataAttrib
        dataAttribs.offset = '0';
        dataAttribs.scale = '10';
        dataAttribs.class = 'uint8';
        dataAttribs.units = '1';
        dataAttribs.nan = '255';

        %Now, create an output array of the appropriate size, this will be
        %populated in the loop below
        h5varcreate(outFile,[outGroup,'/data'],[size(data,1),prod([modelGeoLoc.rows,modelGeoLoc.cols])],...
            'uint8',true);

        %Write the output attributes
        h5attput(outFile,[outGroup,'/data/scale'],dataAttribs.scale);
        h5attput(outFile,[outGroup,'/data/units'],dataAttribs.units);
        h5attput(outFile,[outGroup,'/data/nan'],dataAttribs.nan);

        %Loop through the rows of the dataset
        h = waitbar(0,['Looping through rows of the ',sources.(datatype{m}).name.in{n},' dataset']);
        for o = 1:size(data,1)
            waitbar(o/size(data,1),h);

            %Slice out a single scene and reshape to the original
            %dimensions
            loopData = reshape(data(o,:),geoLoc.rows,geoLoc.cols);

            %Make sure that we are using the right set of protoPts and
            %luWeightsModel
            indLuGrid = find(index(o,1)>=luIndex(:,1) & index(o,1)<luIndex(:,2),1,'first');
            thisProtoPts = squeeze(protoPts(indLuGrid,:,:,:));
            thisLuWeightsModel = squeeze(luWeightsModel(indLuGrid,:,:,:));

            %Loop through the land use classes, except for those expected
            %to have LAI = 0
            loopInterp = zeros(modelGeoLoc.rows,modelGeoLoc.cols,numLuCats);
            for p = 1:numLuCats
                if grids.lu.lu.map(p).interp
                    %Select data values at prototype points, x and y too
                    loopSelData = loopData(thisProtoPts(:,:,p));
                    loopX = xGrid(thisProtoPts(:,:,p));
                    loopY = yGrid(thisProtoPts(:,:,p));

                    %Replace any points that are NaN
                    test = ~isnan(loopSelData);
                    loopSelData= loopSelData(test);
                    loopX = loopX(test);
                    loopY = loopY(test);

                    %interpolate to the full grid
                    if length(loopSelData) > 2
                        tempInterp = TriScatteredInterp(loopX,loopY,loopSelData,'nearest');
                        nearest = tempInterp(xGridModel,yGridModel);
                        tempInterp = TriScatteredInterp(loopX,loopY,loopSelData,'natural');
                        natural = tempInterp(xGridModel,yGridModel);
                        test = isnan(natural);
                        composite = natural;
                        composite(test) = nearest(test);
                        loopInterp(:,:,p) = composite;
%                         loopInterp(:,:,p) = griddata(loopX,loopY,loopSelData,xGridModel,yGridModel,'cubic');
                    else
                        loopInterp(:,:,p) = mean(loopSelData);
                    end
                end
            end

            %Composite the values based on their weights
            loopComposite = sum(loopInterp .* thisLuWeightsModel,3)./ sum(thisLuWeightsModel,3);
            loopComposite(isnan(loopComposite)) = 0;

            %Calculate the upscaled average
            tempComposite = loopComposite;
            tempComposite(tempComposite==0) = NaN;
            [downscaledMean,mask] = cell_statistics(geoLoc,modelGeoLoc,tempComposite,'mean','weight'); %#ok<NASGU>

            %Determine the raio between the original data and this new
            %downscaled mean
            ratio = ones(size(loopData));
            test = ~(downscaledMean<0.1) & ~isnan(downscaledMean);
            ratio(test) = loopData(test) ./ downscaledMean(test);

            %Reshape to the model coordinates
            ratio = ratio(lookup);

            %Manually restrict the ratio to no more than 3, problems are
            %caused by boundaries
            maxRatio = 3;
            ratio(ratio > maxRatio) = maxRatio;

            %Apply this ratio to the downscaled data
            adjComposite = loopComposite .* ratio;

            %Manually limit the values of adjComposite to the range in
            %loopComposite
            adjComposite = limit_vals([min(loopComposite(:)),max(loopComposite(:))],adjComposite);

            %Reshape this array for writing
            adjComposite = reshape(adjComposite,1,[]);

            %Finally, write out this dataset to the appropriate row
            h5datawrite(outFile,[outGroup,'/data'],adjComposite,dataAttribs,true,...
                [o-1,0],[1,size(adjComposite,2)],[1,1]);

            %Clear some loop variables
            clear loopComposite ratio downscaledMean loopInterp loopSelData loopX loopY adjComposite loopData
        end
        if ishandle(h); close(h); end

        %Clear up some memory
        clear protoPts data
    end
end

%Turn more back on
more on
end
