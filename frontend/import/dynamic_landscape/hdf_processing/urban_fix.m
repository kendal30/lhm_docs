function urban_fix(workingDir,outFilename,sources)
%URBAN_FIX  Corrects urban LAI in areas where satellite LAI estimates are
%bad
%   This function also registers LAI to model coordinates if needed
%
%   output = urban_fix(input)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> urban_fix
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-01-21
% Copyright 2010 Michigan State University.

%--------------------------------------------------------------------------
%For convenience
%--------------------------------------------------------------------------
luFile = [workingDir,filesep,sources.lu.data];
impervFile = [workingDir,filesep,sources.imperv.data];
laiFile = [workingDir,filesep,sources.lai.data];

%--------------------------------------------------------------------------
%Open the necessary datasets
%--------------------------------------------------------------------------
%Landuse
luWeights = h5dataread(luFile,['/',sources.lu.name,'/weights']);
luLookup = h5dataread(luFile,['/',sources.lu.name,'/lookup']);

urban = luWeights(1,:,sources.lu.urban);
urban = urban(luLookup);
water = luWeights(1,:,sources.lu.water);
water = water(luLookup);

%Impervious
impervWeights = h5dataread(impervFile,['/',sources.imperv.name,'/weights']);
impervLookup = h5dataread(impervFile,['/',sources.imperv.name,'/lookup']);

imperv = sum(impervWeights,3);
imperv = imperv(impervLookup);

%LAI
[laiIn,laiAttribs] = h5dataread(laiFile,['/',sources.lai.name,'/data']);
laiLookup = h5dataread(laiFile,['/',sources.lai.name,'/lookup'],false);
laiIndex = h5dataread(laiFile,['/',sources.lai.name,'/index'],false);

%Define the x and y grids, just use model i,j
modelSize = size(luLookup);
xGrid = repmat((1:modelSize(1))',[1,modelSize(2)]);
yGrid = repmat((1:modelSize(2)),[modelSize(1),1]);

%Get LAI data into model coordinates, if it hasn't been done already in the
%downscaling step
if size(laiIn,2) ~= numel(laiLookup)
    lai = zeros(size(laiIn,1),numel(laiLookup),class(laiIn));
    for m = 1:size(lai,1)
        thisLaiIn = laiIn(m,:);
        thisLai = thisLaiIn(laiLookup);
        lai(m,:) = thisLai(:);
    end

    %Make a new lookup table in model coordinates
    laiLookup = reshape((1:size(lai,2)),size(laiLookup));
else
    lai = laiIn;
end
clear laiIn

%--------------------------------------------------------------------------
%Make sure all dataset dimensions match
%--------------------------------------------------------------------------
water = water(:)';
urban = urban(:)';
imperv = imperv(:)';

assert(all([length(water),length(urban),length(imperv)]==repmat(size(lai,2),1,3)),...
    'Sizes of grids must match, must be model dimensions');

%--------------------------------------------------------------------------
%Now, identify areas to fix, and prototype areas to use for correction
%--------------------------------------------------------------------------
%Calculate laiMean
meanLai = nanmean(lai,1);
meanLai(isnan(meanLai)) = 0;

%Test for bad cells
indFix = (imperv > sources.threshold.impervMin) & (meanLai < sources.threshold.laiBadMax) & ...
    (water < sources.threshold.waterMax);

%Ind prototypes
indProto = (imperv > sources.threshold.urbanMin) & (meanLai > sources.threshold.laiBadMax) & ...
    (water < sources.threshold.waterMax);
gridProto = indProto(luLookup);

if sum(indProto) > 1 %otherwise, there's nothing to correct
    %--------------------------------------------------------------------------
    %Loop through the dataset, correcting urban areas
    %--------------------------------------------------------------------------
    %Get impervious-corrected LAI from prototypes
    numLai = size(lai,1);
    laiImpervCorrect = lai ./ repmat((1 - imperv),numLai,1);

%     %Calculate the mean for each grid of the prototype points
%     meanFix = nanmean(laiImpervCorrect(:,indProto),2);

    %Select data values at prototype points, x and y too
    protoData = laiImpervCorrect(:,indProto);
    protoX = xGrid(gridProto);
    protoY = yGrid(gridProto);

    %Now, correct LAI, multiplying by the impervious fraction
    h = waitbar(0,'Correcting urban LAI in rows of the LAI dataset');
    for m = 1:numLai
        waitbar(m/numLai,h);

        %Set up the interpolation objects and interpolate
        tempInterp = TriScatteredInterp(protoX,protoY,protoData(m,:)','nearest');
        nearest = tempInterp(xGrid,xGrid);
        tempInterp = TriScatteredInterp(protoX,protoY,protoData(m,:)','natural');
        natural = tempInterp(xGrid,xGrid);
        test = isnan(natural);
        composite = natural;
        composite(test) = nearest(test);
        composite = composite(:)'; %flatten

        %Map to the cells that need fixing
        lai(m,indFix) = composite(indFix) .* (1 - imperv(indFix));
        %         lai(m,indFix) = meanFix(m) .* (1 - imperv(indFix));
    end
    if ishandle(h); close(h); end
end

%--------------------------------------------------------------------------
%Write the output dataset
%--------------------------------------------------------------------------
outFile = [workingDir,filesep,outFilename];
outGroup = ['/',sources.lai.name];
import_write_dataset(outFile,outGroup,lai,laiAttribs,laiIndex,false,laiLookup);

end
