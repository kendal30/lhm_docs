function lai_model_calibrate(workingDir,outFilename,sources,grids)

%Specify inputs
laiFile = sources.lai.data;
luFile = sources.lu.data;
tempFile = sources.clim.data;
statFile = sources.stat.data;


x0 = sources.params.x0;
lb = sources.params.lb;
ub = sources.params.ub;
scaleParams = sources.params.scaleParams;

% PPsen = 0.95; %fraction of photo period that triggers senescence, fix from data

%Land use types to optimize
luOptim = sources.params.luOptim;
luWeightSheet = sources.params.luWeightSheet;

%Threshold weight for each LU prototype
weightThresh = sources.params.threshInit;
minThresh = sources.params.threshMin;

%Get the times for rise and fall periods
risePeriod = sources.params.risePeriod;
fallPeriod = sources.params.fallPeriod;

%Assign values from grids
% singlePointSun = true; %this makes the values of latitude, longitude, and elevation just a single
%point, the photo period function needs to be rewritten to handle large
%areas

%--------------------------------------------------------------------------
%Read in the LU data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,luFile];
tempGroup = '/landuse';

%Read in the weights table
luWeights = h5dataread(inFile,[tempGroup,'/weights'],true);

%--------------------------------------------------------------------------
%Read in the LAI data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,laiFile];
tempGroup = '/lai';

%Read in the whole LAI data structure
lai = h5datareadlhm(inFile,tempGroup,true);

%--------------------------------------------------------------------------
%Read in the airTemp data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,tempFile];
tempGroup = '/airTemp';

% Read in the whole airTemp data structure
airTemp = h5datareadlhm(inFile,tempGroup,true);

% Get the overlap in LAI and airTemp data, this will be used for our
% calibration period
laiTime = lai.index(:,1);
airTime = airTemp.index(:,1);
calibBounds = [max(airTime(1),laiTime(1)),min(airTime(end),laiTime(end))];

% Subset to match the LAI dataset
[airTemp] = subset_lhm_input_data(airTemp,calibBounds(1),calibBounds(end));
[lai] = subset_lhm_input_data(lai,calibBounds(1),calibBounds(end));

%For convenience
airTime = airTemp.index(:,1);
laiTime = lai.index(:,1);

%--------------------------------------------------------------------------
%Read in the precip data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,tempFile];
tempGroup = '/rain';

%Read in the whole airTemp data structure
rain = h5datareadlhm(inFile,tempGroup,true);

% Subset to match the LAI dataset
[rain] = subset_lhm_input_data(rain,calibBounds(1),calibBounds(end));

%For convenience
rainIndex = rain.index(~rain.zeros,:);

%--------------------------------------------------------------------------
%Read in the soils data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,statFile];
tempGroup = '/soil/soil_ksat/data';

% Read in the soils data
soilKsat = h5dataread(inFile,tempGroup);

% Use the 2nd layer
soilKsat = soilKsat(:,:,2);

%--------------------------------------------------------------------------
%Read in the grids
%--------------------------------------------------------------------------
% if ischar(grids.latitude)
%     latitude = import_grid(grids.latitude);
%     latitude(isinf(latitude)) = NaN;
% else
%     latitude = grids.latitude;
% end
% if ischar(grids.longitude)
%     longitude = import_grid(grids.longitude);
%     longitude(isinf(longitude)) = NaN;
% else
%     longitude = grids.longitude;
% end

if ischar(grids.elevation)
    elevation = import_grid(grids.elevation);
    elevation(isinf(elevation)) = NaN;
    elevation(elevation<-300) = NaN;
else
    elevation = grids.elevation;
end
% Build the model domain mask
domainMask = ~isnan(elevation);
% 
% if singlePointSun
%     latitude = nanmedian(latitude(:));
%     longitude = nanmedian(longitude(:));
%     elevation = nanmedian(elevation(:));
% end

% %--------------------------------------------------------------------------
%Define the optimization period (only rise and fall times)
%--------------------------------------------------------------------------
%Specify years to use for calibration, could modify this into a cal/val set easily
%calibYears = [2000,2014]; %use all data

%Calculate indexes
laiDoy = date2doy(laiTime);
%yearsInd = (laiTime >= datenum(calibYears(1),0,0)) & (laiTime <= datenum(calibYears(2),0,0));
yearsInd = ones(size(laiTime));

riseInd = (laiDoy >= risePeriod(1)) & (laiDoy <= risePeriod(2)) & yearsInd;
fallInd = (laiDoy >= fallPeriod(1)) & (laiDoy <= fallPeriod(2)) & yearsInd;
optPeriod = riseInd | fallInd;

%--------------------------------------------------------------------------
%Loop through the LU types
%--------------------------------------------------------------------------
%Get the fractional photo period
% photoPeriod = photo_period(latitude,longitude,elevation);
%fracPhotoPeriod = photoPeriod ./ max(photoPeriod);

modelParams = struct();
h = waitbar(0,'Optimizing LAI model parameters');
for m = 1:length(luOptim)
    waitbar(m/length(luOptim),h);

    % Determine which points to use, masked to the model domain
    prototypePts = reshape((luWeights(1,:,luWeightSheet(m)) > weightThresh),[],1) &...
        domainMask(:);
    prototypeInd = find(prototypePts);

    % Limit these to a random subset of 1000 if more than that
    if length(prototypeInd) > 1000
        subsInd = rand_index(size(prototypeInd),[],1000);
        prototypeInd = prototypeInd(subsInd);
    elseif length(prototypeInd) < 100  %Add a few more points
        thisWeightThresh = weightThresh * 0.9; %Lower the weight threshold to include more points
        while (length(prototypeInd) < 100) && (thisWeightThresh > minThresh)
            prototypePts = reshape((luWeights(1,:,luWeightSheet(m)) > thisWeightThresh),[],1) &...
                domainMask(:);
            prototypeInd = find(prototypePts);
            thisWeightThresh = thisWeightThresh * 0.9; %Lower the weight threshold to include more points
        end
    end

    %Get the soil ksat of each LU point
    thisKsat = soilKsat(prototypeInd);
    
    if length(prototypeInd) > 10
        %Select a limited subset of LAI data
        thisLAI = single(lai.data(:,prototypeInd));
        thisLAI(thisLAI == 255) = NaN;
        thisLAI(thisLAI > 8) = NaN;
        thisLAI(thisLAI == 0) = NaN;

        %Now, get airTemp data for matching points, this will require a few
        %steps
        %First, determine which model cells those prototype points reside in
        airTempLookup = reshape(airTemp.lookup,[numel(lai.lookup),1,size(airTemp.lookup,3)]);
        airTempLookup = airTempLookup(prototypeInd,1,:);

        %Now, expand the airTemp data to this model subset
        thisAirTemp = expand_lhm_input_data(airTemp.index,airTemp.data,airTempLookup);

        %Repeat with the rain data
        rainLookup = reshape(rain.lookup,[numel(lai.lookup),1,size(rain.lookup,3)]);
        rainLookup = rainLookup(prototypeInd,1,:);
        thisRain = expand_lhm_input_data(rainIndex,rain.data,rainLookup); %use rainIndex because it's trimmed for zeros
        allRain = zeros(size(rain.index,1),size(thisRain,2));
        allRain(~rain.zeros,:) = thisRain;
        
        %Now, select only the portion of the lai data within the calibration
        %period
        calibDays = laiTime(optPeriod);
        calibLAI = thisLAI(optPeriod,:);
        calibLAI = reshape(calibLAI,[],1);
        calibNan = isnan(calibLAI);
        calibLAI = calibLAI(~calibNan); %this will be matched for the modeled values below in lai_model_call
        
        %Get daily average temperatures to speed up the
        %optimization, with some small loss of accuracy on the degree day calcs
        [days,dailyMeanTemp] = time_series_aggregate(airTime,thisAirTemp,'days','mean');
        [~,dailyRain] = time_series_aggregate(rain.index(:,1),allRain,'days','sum');
        %Get the annual maximum DOY
        [yearsSplit,yearlySplit] = time_series_aggregate(laiTime,thisLAI,'years','split');
        [~,dailyTempSplit] = time_series_aggregate(days,dailyMeanTemp,'years','split');
        [~,dailyRainSplit] = time_series_aggregate(days,dailyRain,'years','split');
        [yearlyMinVal,yearlyMaxVal,yearlyGSStart,yearlyTempAccum,yearlyRainAccum] = ...
            deal(zeros([length(yearsSplit),size(thisLAI,2)]));
        
        for n = 1:length(yearsSplit)
            trimFinal = false;
            if size(yearlySplit{n},1)<10 %this isn't a complete year
                trimFinal = true;
            else
                %Get the max and min vals
                yearlyMinVal(n,:) = percentile(yearlySplit{n},10);
                yearlyMaxVal(n,:) = percentile(yearlySplit{n},90); %nanmax(yearlySplit{n});
                
                %Quantify the onset of the growing season
                repDOY = repmat((1:size(dailyTempSplit{n},1))',[1,size(yearlyGSStart,2)]);
                preMaxDOY = (risePeriod(2) >= repDOY) .* repDOY;
                testFrozen = dailyTempSplit{n} < 0;
                yearlyGSStart(n,:) = max(preMaxDOY .* testFrozen) + 1;
                
                %Quantify the accumulated temperature and rain after the GS start to
                %the max DOY
                repGSStartDOY = repmat(yearlyGSStart(n,:),[size(dailyTempSplit{n},1),1]);
                yearlyTempAccum(n,:) = sum(dailyTempSplit{n} .* (repDOY >= repGSStartDOY) .* (repDOY <= risePeriod(2)));
                yearlyRainAccum(n,:) = nansum(dailyRainSplit{n} .* (repDOY >= repGSStartDOY) .* (repDOY <= risePeriod(2)));
            end
        end

        %Remove the trimmed values
        if trimFinal
            yearlyMinVal = yearlyMinVal(1:end-1,:);
            yearlyMaxVal = yearlyMaxVal(1:end-1,:);
            %yearlyMaxDOY = yearlyMaxDOY(1:end-1,:);
            yearlyGSStart = yearlyGSStart(1:end-1,:);
            yearlyTempAccum = yearlyTempAccum(1:end-1,:);
            yearlyRainAccum = yearlyRainAccum(1:end-1,:);
        end
        
%         % Get the spatial patterns
%         %spatialMinVal = nanmedian(yearlyMinVal,1);
%         spatialMaxDOY = nanmean(yearlyMaxDOY,1);
%         spatialGSStart = nanmean(yearlyGSStart,1);
%         spatialTempAccum = nanmean(yearlyTempAccum,1);
%         spatialRainAccum = nanmean(yearlyRainAccum,1);
%         spatialMaxVal = nanmean(yearlyMaxVal,1);
%         
%         % Get the response variable
%         responseTemporal = yearlyMaxVal ./ repmat(spatialMaxVal,[size(yearlyMaxVal,1),1]);
%   
%         % Collapse across years
%         avgMinVal = nanmedian(yearlyMinVal,2);
%         avgMaxDOY = nanmedian(yearlyMaxDOY,2);
%         avgGSStart = nanmedian(yearlyGSStart,2);
%         avgTempAccum = nanmedian(yearlyTempAccum,2);
%         avgRainAccum = nanmedian(yearlyRainAccum,2);
%         responseTemporal = nanmean(responseTemporal,2);
%           
%         % -- For the temporal parameters
%         predictorsTemporal = [avgMaxDOY(:),avgGSStart(:),avgTempAccum(:),avgRainAccum(:)];
%         responseTemporal = responseTemporal(:);
%         
%         testKeep= ~isnan(responseTemporal);
%         responseTemporal = responseTemporal(testKeep);
%         predictorsTemporal = predictorsTemporal(testKeep,:);
%         
%         % Fit a linear model        
%         linearModelTemporal = fitlm(predictorsTemporal,responseTemporal);
%         predictTemporal = predict(linearModelTemporal,predictorsTemporal);
%         
%         % -- Now for the spatial parameters
%         predictorsSpatial = [spatialMaxDOY(:),spatialGSStart(:),...
%             spatialTempAccum(:),spatialRainAccum(:),thisKsat(:)];
%         responseSpatial = spatialMaxVal(:);
%         
%         % Fit a regression tree to the spatial part
%         treeSpatial = fitrtree(predictorsSpatial,log(responseSpatial));
%         predictSpatial = predict(treeSpatial,predictorsSpatial);
%         
%         % Now, composite the spatial and temporal
%         predictMaxVal = repmat(exp(predictSpatial)',[size(predictTemporal,1),1]) .* ...
%             repmat(predictTemporal,[1,size(predictSpatial,1)]);
        
        % --- Linear model tree
        % Make the combined variables
        ksatComb = repmat(thisKsat(:)',size(yearlyGSStart,1),1);
        predictorsComb = [yearlyGSStart(:),yearlyTempAccum(:),yearlyRainAccum(:),ksatComb(:)];
        responseComb = yearlyMaxVal(:);
        
        % Build the model
        lmtParams = m5pparams2('modelTree',true,'aggressivePruning',true,...
            'minLeafSize',10);
        lmtComb = m5pbuild(predictorsComb,responseComb,lmtParams,[],[],[],false);
        predictLmt = m5ppredict(lmtComb,predictorsComb);
        
        % Find the annual maximum, start of year values, end of year values, and
        % day of maximum
        minVal = mean(yearlyMinVal(:));
        maxVal = reshape(predictLmt,size(yearlyMaxVal));

        %Prep the objective_function script, set optimization options
        objective_function('initialize',@lai_model_call,{days,dailyMeanTemp,...
            calibDays,calibNan,scaleParams,minVal,maxVal},calibLAI,'NSEMAE');
        optimOptions = optimset('TolFun',0.001);

        %Now, actually run the optimization
        modelParams.(luOptim{m}).params = fminsearchbnd('objective_function',...
            x0.*scaleParams,lb.*scaleParams,ub.*scaleParams,optimOptions);
        modelParams.(luOptim{m}).params = modelParams.(luOptim{m}).params./scaleParams;
        modelParams.(luOptim{m}).minVal = minVal;
        modelParams.(luOptim{m}).numPoints = size(thisLAI,2);

        %Now, save the models for the spatial and temporal variability of
        %minimum and maximum values
        modelParams.(luOptim{m}).maxVal.model = lmtComb;
        modelParams.(luOptim{m}).maxVal.obs = yearlyMaxVal;
        modelParams.(luOptim{m}).maxVal.sim = maxVal;
        modelParams.(luOptim{m}).maxVal.doy = risePeriod(2);
        
        %Save out the test data and model
        modelParams.(luOptim{m}).laiTime = laiTime;
        modelParams.(luOptim{m}).laiAvg = nanmean(thisLAI,2);
        [modDays,modLai] = lai_model(days,dailyMeanTemp,{minVal,maxVal,modelParams.(luOptim{m}).params});
        modelParams.(luOptim{m}).modDays = modDays;
        modelParams.(luOptim{m}).modLaiAvg = nanmean(modLai,2);

        %Also, calculate the correlation coefficient of the entire array
        laiFit = interp1(modDays,modLai,laiTime);
        test = ~isnan(thisLAI) & ~isnan(laiFit);
        R = corrcoef(thisLAI(test),laiFit(test));
        modelParams.(luOptim{m}).R2 = R(2)^2;

%         if m == 1 %start here for subsequent loops
%             x0 = modelParams.(luOptim{m}).params;
%         end
    end
end
if ishandle(h); close(h); end

%Save the output
outFile = [workingDir,filesep,outFilename];
save(outFile,'modelParams');

end


function [laiCalib] = lai_model_call(airTime,airTemp,calibDays,calibNan,scaleParams,minVal,maxVal,params)

%Run the LAI model;
[modelDays,modelLai] = lai_model(airTime,airTemp,{minVal,maxVal,params./scaleParams});

%Collapse to annual cycle
%[modelDOY,modelDOYLai] = time_series_aggregate(modelDays,modelLai,'days','mean','years','median');

%Interpolate these to the LAI dates
laiCalib = interp1(modelDays,modelLai,calibDays);
%laiCalib = nanmedian(interp1(modelDOY,modelDOYLai,calibDOY),2);

%Collapse into 1D
laiCalib = reshape(laiCalib,[],1);
laiCalib = laiCalib(~calibNan);
end

function [photoPeriod] = photo_period(latitude,longitude,elevation)
%Calculate the daily photo period
minutes = datenum('1/1/2000') + (0:1:366*24*60-1)/(24*60);
[~,zen] = sun_position(minutes,latitude,longitude,elevation);
aboveHorizon = zen < 90;
[~,photoPeriod] = time_series_aggregate(minutes,aboveHorizon,'days','sum');
end
