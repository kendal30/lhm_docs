function [days,lai] = lai_model(time,temp,params)
%LAI_MODEL Calculates LAI using only hourly/daily temperatures
%   [days,lai] = lai_model(time,temp,params)
%
%   This LAI model requires only daily or hourly average temperatures, and
%   outputs daily LAI.  It has seven described below.  The model is:
%
%   Days 0 - 180: LAI = L_min* exp(GDD *A1) + A2 * GDD
%   Days 181 - 366: LAI = L_max* exp(SDD *-A3)
%
%   LAImodel = concatenate(LAImodel1,LAImodel2)
%
%   LAImodel = constrain(LAImodel,[L_min,L_max])
%
%   (For Daily data)
%   GDD = sum(max((T - T_GDD),0))
%   SDD = sum(max((T_SDD - T),0))
%   
%   (For Hourly data)
%   GDD = sum(max((T - T_GDD),0)/24)
%   SDD = sum(max((T_SDD - T),0)/24)
%
%   For GDD and SDD calcs, T = min(T,T_GDD) or T = max(T,T_SS), such that
%   GDD and SDD are always positive or 0.
%   
%   Parameters:
%   minLAI:     (L_min), minimum LAI value, typically occuring at the beginning
%               and ending of the year
%   maxLAI:     (L_max), maximum LAI value, typically occurring late
%               June/early July
%   riseExp:    (A1), multiplicative constant on the exponential component
%               of the rising limb
%   riseSlope:  (A2), slope of the linear portion of the rising limb
%   senExp:    (A3), multiplicative constant on the exponential falling
%               limb, note that this is a positive value
%   gddThresh:  (T_GDD), threshold above which growing degree days begin to
%               accumulate, varies depending on land cover
%   sddThresh:  (T_SDD), threshold below which "senescence" degree days
%               begin to accumulate, varies depending on land cover
%
%   Descriptions of Input Variables:
%   time:       input time, can be either daily or hourly, in MATLAB
%               datenum format
%   temp  :     input temperatures, must be either daily or hourly averages
%   params:     Model parameters vector, params = [minLAI, maxLAI, riseExp,
%               riseSlope, senExp, gddThresh, sddThresh]
%
%   Descriptions of Output Variables:
%   days:       MATLAB serial time vector of the output days
%   lai:        modeled daily LAI
%
%   Example(s):
%   >>
%
%   See also: degree_days lai_model_calibrate

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-01-26
% Copyright 2009 Michigan State University.

% Hard-code a few parameters
defaultGSEnd = 320; %if no GS end is triggered, this would be from a bad threshSen
defaultMidPoint = 180; 
defaultEarlyMature = 150; %earliest maturation time

%Parse the params
minLAImean = params{1}; %scalar
maxLAIyearly = params{2}; %annual value, across all points

% These are optimized
riseExp = params{3}(1);
riseSlopeLin = params{3}(2);
senExp = params{3}(3);
matExp = params{3}(4);
gddThresh = params{3}(5);
sddThreshSen = params{3}(6);
sddThreshMat = params{3}(7);
gddMature = params{3}(8);
%ppFactor = params{3}(8); %photoperiod factor, reduces the effect of photoperiod fraction on gdd and sdd

% Specify the tolerance on the logistic function
logisticTol = 0.01;

%First, if given hours as inputs, go to days, get mean of temperatures
if diff(time(1:2))<1
    [days,temp] = time_series_aggregate(time,temp,'days','mean');
else
    days = time;
end

%Distribute the photoperiod values in space and time
doyDays = date2doy(days);
% fracPhotoPeriod = 1 - (max(photoPeriod) - photoPeriod) / max(photoPeriod) * ppFactor;
% modPhotoPeriod = fracPhotoPeriod(doyDays,:);
% if size(modPhotoPeriod,2) < size(temp,2) %assume spatially invariate photo period values
%     modPhotoPeriod = repmat(modPhotoPeriod,1,size(temp,2));
% end

%Get the growing degree days, and cooling degree days, will need to reset
%between gaps
[~,gdd] = degree_days(days,temp,gddThresh,1,'continuous');
doyDays = repmat(doyDays,[1,size(temp,2)]);

%Get minimum and maximum value distributions
minLAI = repmat(minLAImean,size(days,1),size(temp,2));

% First, need to get the year index for the days
[year,~,~,~,~,~] = datevec(days);
yearInd = year - year(1) + 1;
% Now extract maxLAI for each day, based on the year of that day
maxLAI = maxLAIyearly(yearInd,:);

%Scale by photoperiod, reset values
gdd = [zeros(1,size(gdd,2));diff(gdd)];
% gdd = gdd .* modPhotoPeriod;
gdd = cumsum(gdd);
gdd = temporal_reset_values(days,gdd,'years');

% Solve the value of the rise offset needed to achieve continuity 
% using the logistic function (essentially, what delta LAI value I want to
% have when GDD = 0
deltaLAIRise = (maxLAI - minLAI);
riseOffset = log((deltaLAIRise - logisticTol)/logisticTol);

%Fit the rising portion of the LAI curve, (linear +) exponential functions,
%limit vals
gddFitExp = deltaLAIRise .* (1 ./ (1 + 1 ./ exp(riseExp * gdd - riseOffset)));
gddFitExp(gdd==0) = 0;
gddFitLin = min(riseSlopeLin * gdd, deltaLAIRise);
gddFit = minLAI + max(gddFitExp,gddFitLin);

%Identify the DOY threshold for senescence, limit to no earlier than day
%150
testMature = (gdd >= gddMature) & (doyDays >= defaultEarlyMature);
%testMature = (gddFit >= (maxLAI-0.005)) & (doyDays >= 150);

%Identify the DOY threshold for the end of senescence (i.e. start of next
%GS)
%TODO: I am pretty sure that this code block below could be cleaned up
%considerably
gsStartFunc = @(testCond) find(any(testCond,2),1,'first');
gsEndFunc = @(testCond) find(any(testCond,2),1,'first');
[years,doyGsStart] = time_series_aggregate(days,gddFit>(1.05*minLAI),'years',gsStartFunc);
[~,doyGsEnd] = time_series_aggregate(days,(temp<sddThreshSen & doyDays>defaultMidPoint),'years',gsEndFunc);
if all(doyGsEnd(:)==0)
    doyGsEnd(:) = defaultGSEnd;
elseif any(doyGsEnd(:)==0)
    indZero = doyGsEnd == 0;
    avgDoyGsEnd = mean(doyGsEnd(~indZero));
    doyGsEnd(indZero) = avgDoyGsEnd;
end
[yearsGDD,~,~] = datevec(years);
[~,yearsLoc] = ismember(year,yearsGDD);
yearsLoc = repmat(yearsLoc,1,size(doyGsEnd,2)) + repmat((0:size(doyGsEnd,1):(size(doyGsEnd,2)-1)*size(doyGsEnd,1)),size(yearsLoc,1),1);
yearlyGsStart = doyGsStart(yearsLoc);
yearlyGsEnd = doyGsEnd(yearsLoc);
testGrow = doyDays>yearlyGsStart & ~testMature & doyDays<yearlyGsEnd; %doyDays<doySenStart;

%Determine the maximum LAI value to start from for each year and cell
[~,yearlyMax] = time_series_aggregate(days,gddFit,'years','max');
maxLAIYearly = yearlyMax(yearsLoc);

%Then, for SDDMat
[~,sddMat] = degree_days(days,temp,sddThreshMat,-1,'continuous');
sddMat = [zeros(1,size(sddMat,2));diff(sddMat)];
sddMat(testGrow) = 0;
% sddMat = sddMat .* modPhotoPeriod;
sddMat = cumsum(sddMat);
sddMat = temporal_reset_values(days-100,sddMat,'years'); %offset, reset every half year
sddMat(testGrow) = 0;
sddMat(1:100,:) = 0; %handle the first year

%Then, for SDDSen
[~,sddSen] = degree_days(days,temp,sddThreshSen,-1,'continuous');
sddSen = [zeros(1,size(sddSen,2));diff(sddSen)];
sddSen(testGrow) = 0;
% sddSen = sddSen .* modPhotoPeriod;
sddSen = cumsum(sddSen);
sddSen = temporal_reset_values(days-100,sddSen,'years'); %offset, reset every half year
sddSen(testGrow) = 0;
sddSen(1:100,:) = 0; %handle the first year

% Solve the value of the rise offset needed to achieve continuity 
% using the logistic function (essentially, what delta LAI value I want to
% have when GDD = 0
deltaLAIFall = (maxLAIYearly - minLAI);
fallOffset = log((deltaLAIFall - logisticTol)/logisticTol);

%Fit a second one, this is the maturation phase
sddFitMat = maxLAIYearly - deltaLAIFall .* (1 ./ (1 + 1 ./ (exp(matExp*sddMat - fallOffset))));%.^fallSlope;
sddFitMat(testGrow) = maxLAIYearly(testGrow);

%Fit the falling portion of the LAI curve, exponential function
sddFitSen = maxLAIYearly - deltaLAIFall .* (1 ./ (1 + 1 ./ (exp(senExp*sddSen - fallOffset))));%.^fallSlope;
sddFitSen(testGrow) = maxLAIYearly(testGrow);

%Get the combined maturation and senescence phase
sddFit = min(sddFitSen,sddFitMat);

%Merge to get LAI
lai = min(gddFit,sddFit);
