function lai_scenario_run(workingDir,outFilename,modelHours,sources,grids)
%LAI_SCENARIO_RUN  One-line description here, please.
%   lai_scenario_run(workingDir,outFilename,modelHours,sources,grids)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> julian_fill_landscape
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Specify inputs
%--------------------------------------------------------------------------
% Get the files to read in
luFile = sources.lu.data;
tempFile = sources.airTemp.data;
rainFile = sources.rain.data;
statFile = sources.stat.data;
modelFile = sources.laiModel;

% Prepare the lai model times
startDate = datevec(modelHours(1));
endDate = datevec(modelHours(end));
years = (startDate(1):endDate(1))';
monthDay = zeros(length(years),length(sources.laiDays));
modelDates = datenum(repmat(years,1,length(sources.laiDays)),monthDay,monthDay) + ...
    repmat(sources.laiDays,length(years),1);
modelDates = reshape(modelDates',[],1);
modelDatevec = datevec(modelDates);

%For convenience
numYears = length(years);

%--------------------------------------------------------------------------
%Read the calibrated LAI model
%--------------------------------------------------------------------------
% Load LAI Model
load([workingDir,filesep,modelFile]) %variable is modelParams

%--------------------------------------------------------------------------
%Read in the LU data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,luFile];
tempGroup = '/landuse';

% Read in the weights table
lu = h5datareadlhm(inFile,tempGroup,true);

%For convenience
luTypes = lu.names;

%--------------------------------------------------------------------------
%Read in the airTemp data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,tempFile];
tempGroup = '/airTemp';

% Read in the whole airTemp data structure
airTemp = h5datareadlhm(inFile,tempGroup,true);

% For convenience, get the airTemp times as a datevec
airTime = airTemp.index(:,1);
airTimeVec = datevec(airTime);

%--------------------------------------------------------------------------
%Read in the precip data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,rainFile];
tempGroup = '/rain';

% Read in the whole rain data structure
rain = h5datareadlhm(inFile,tempGroup,true);

% For convenience, get the rain times as a datevec
rainTime = rain.index(:,1);
rainTimeVec = datevec(airTime);

%--------------------------------------------------------------------------
%Read in the soils data
%--------------------------------------------------------------------------
inFile = [workingDir,filesep,statFile];
tempGroup = '/soil/soil_ksat/data';

% Read in the soils data
soilKsat = h5dataread(inFile,tempGroup);

% Use the 2nd layer
soilKsat = soilKsat(:,:,2);


%--------------------------------------------------------------------------
%Loop through years, calculating then writing to disk
%--------------------------------------------------------------------------
%Initialize lastRow information for writing yearly data
lastRow = 0;

h = waitbar(0,'Creating yearly LAI maps');
for m = 1:numYears
    waitbar(m/numYears,h);

    % Get the time indeces for this year
    thisYear = years(m);
    thisModelInd = (modelDatevec(:,1) == thisYear);
    thisModelDates = modelDates(thisModelInd);
    thisNumModelDates = sum(thisModelInd);
    thisAirInd = (airTimeVec(:,1) == thisYear);
    thisRainInd = (rainTimeVec(:,1) == thisYear);
    
    % Get the landuse map for this year
    thisYearNum = datenum(thisYear,1,1);
    thisIndLu = find(thisYearNum>=lu.index(:,1) & thisYearNum<=lu.index(:,2));
    thisLuSheet = lu.index(thisIndLu,3);
    luWeights = squeeze(lu.weights(thisLuSheet,:,:));
    
    % Subset to match the LAI dataset
    thisAirTempDataset = subset_lhm_input_data(airTemp,airTime(find(thisAirInd,1,'first')),...
        airTime(find(thisAirInd,1,'last')));
    thisRainDataset = subset_lhm_input_data(rain,rainTime(find(thisRainInd,1,'first')),...
        rainTime(find(thisRainInd,1,'last')));
    zerosRain = zeros(size(thisRainDataset.index,1),size(thisRainDataset.data,2));
    zerosRain(~thisRainDataset.zeros,:) = thisRainDataset.data;
    
    % Calculate daily average temperatures to speed up the model and allow
    % for the data to be expanded to the full model domain within memory
    [tempDays,tempDaily] = time_series_aggregate(thisAirTempDataset.index(:,1),...
        thisAirTempDataset.data,'days','mean');
    [~,rainDaily] = time_series_aggregate(thisRainDataset.index(:,1),...
        zerosRain,'days','sum');
    
    % Get a daily index for each
    tempIndexDaily = thisAirTempDataset.index(1:24:end,:);
    tempIndexDaily(:,2) = (1:length(tempDays));
    rainIndexDaily = thisRainDataset.index(1:24:end,:);
    rainIndexDaily(:,2) = (1:length(tempDays));
    
    % Expand each to the full model domain
    thisAirTemp = expand_lhm_input_data(tempIndexDaily,tempDaily,thisAirTempDataset.lookup);
    thisAirTemp = reshape(thisAirTemp,[size(thisAirTemp,1),size(thisAirTemp,2)*size(thisAirTemp,3)]);
    thisRain = expand_lhm_input_data(rainIndexDaily,rainDaily,thisRainDataset.lookup);
    thisRain = reshape(thisRain,[size(thisRain,1),size(thisRain,2)*size(thisRain,3)]);
   
    %----------------------------------------------------------------------
    %For the active LU types, calculate the LAI model
    %----------------------------------------------------------------------
    modelLAI = struct();
    [numDays,numCells] = size(thisAirTemp);
    for n = 1:length(luTypes)
        if strcmpi(luTypes{n},'water')
            modelLAI.(luTypes{n}) = 0;
        else
            % For convenience
            thisModel = modelParams.(luTypes{n});

            % Compute the variables for the maximum LAI model
            % Quantify the onset of the growing season
            repDOY = repmat((1:size(thisAirTemp,1))',[1,numCells]);
            preMaxDOY = (thisModel.maxVal.doy >= repDOY) .* repDOY;
            testFrozen = thisAirTemp < 0;
            yearlyGSStart = max(preMaxDOY .* testFrozen) + 1;

            % Quantify the accumulated temperature and rain after the GS start to
            % the max DOY
            repGSStartDOY = repmat(yearlyGSStart,[numDays,1]);
            yearlyTempAccum = sum(thisAirTemp .* (repDOY >= repGSStartDOY) .* ...
                (repDOY <= thisModel.maxVal.doy));
            yearlyRainAccum = nansum(thisRain .* (repDOY >= repGSStartDOY) .* ...
                (repDOY <= thisModel.maxVal.doy));

            % Get the modeled annual maximum for this luType
            predictors = [yearlyGSStart(:),yearlyTempAccum(:),yearlyRainAccum(:),soilKsat(:)];
            modelMaxLAI = m5ppredict(thisModel.maxVal.model,predictors);

            % Run the LAI model
            [modDays,modLai] = lai_model(tempDays,thisAirTemp,{thisModel.minVal,modelMaxLAI,thisModel.params});

            % Resample to the target dates
            modelLAI.(luTypes{n}) = interp1(modDays,modLai,thisModelDates);
        end
    end

    %--------------------------------------------------------------------------
    %Calculate composite LAI maps
    %--------------------------------------------------------------------------
    compositeLAI = zeros(thisNumModelDates,size(modLai,2),'uint8');

    % Make the LAI map
    for n = 1:length(luTypes)
        thisLAI = modelLAI.(luTypes{n});
        thisWeights = repmat(luWeights(:,n)',[thisNumModelDates,1]);
        compositeLAI = compositeLAI + uint8(thisLAI .* thisWeights * 10);
    end
    clear thisWeights thisLAI

    %Build the index
    index = [thisModelDates,lastRow+(1:thisNumModelDates)'];

    %--------------------------------------------------------------------------
    %Write out the final dataset
    %--------------------------------------------------------------------------
    outFile = [workingDir,filesep,outFilename];
    tempGroup = sources.lai.name;

    attribs = struct('class','');
    if m == 1 %only on the first loop
        %Create the output group
        groupExist = h5groupcreate(outFile,tempGroup);

        if groupExist
            %If it exists, must rename old file or stop
            get_overwrite_pref(outFile,tempGroup);
            %If we're still going, that means the user decided to
            %overwrite
            %rename the current file
            [outFilePath,outFileName,outFileExt] = fileparts(outFile);
            oldFileName = ['old_',outFileName,outFileExt];
            oldFile = [outFilePath,filesep,oldFileName];
            if exist(oldFile,'file')
                delete(oldFile);
            end
            system(['ren ',outFile,' ',oldFileName]);

            %Create the group again, this will create the new file too
            h5groupcreate(outFile,tempGroup);
        end

        %Write the lookup table
        attribs.class = class(lu.lookup);
        h5datawrite(outFile,[tempGroup,'/lookup'],lu.lookup,attribs,false);
    end

    %Write the output index
    attribs.class = class(index);
    indexShape = size(index);
    h5datawrite(outFile,[tempGroup,'/index'],index,attribs,false,[lastRow,0],indexShape,[]);

    %Write the output data
    attribs = sources.lai;
    attribs = rmfield(attribs,'name');
    outShape = size(compositeLAI);
    h5datawrite(outFile,[tempGroup,'/data'],compositeLAI,attribs,false,[lastRow,0],outShape,[]);

    %Update last row storage information
    lastRow = lastRow + thisNumModelDates;
end
end