function obs_file_load(dirInfo,fileInfo,outputGrids,obsTables)
% This function is called by the observations_import_controller

% Parse the inputs
% Build the output filename
obsName = fileInfo{1};
obsVersion = fileInfo{2};
IDType = 'OBS'; % shouldn't change
compileDate = now; % only change if needed
obsName = strrep(obsName,'_','-');
obsVersion = strrep(obsVersion,'_','-');
outFilename = [IDType,'_',obsName,'_',obsVersion,'_',datestr(compileDate,'yyyymmdd'),'.h5'];

% Parse out input directories
baseDir = dirInfo{1};
nameSubmodel = dirInfo{2};
workingDir = [baseDir,filesep,'Import_Working'];
outFile = [workingDir,filesep,outFilename];
if islogical(nameSubmodel)
    inputDirGrids = [baseDir,filesep,'Model_Layers',filesep,'Surface'];
    inputDirObs = [baseDir,filesep,'Model_Layers',filesep,'Observations'];
else
    inputDirGrids = [baseDir,filesep,'Submodel_Layers',filesep,nameSubmodel,filesep,'Surface'];
    inputDirObs = [baseDir,filesep,'Submodel_Layers',filesep,nameSubmodel,filesep,'Observations'];    
end

% Check to see that the input directories exist
assert(exist(workingDir,'dir')==7,'Working directory does not exist');
assert(exist(inputDirGrids,'dir')==7,'Surface Model Layers directory does not exist');
assert(exist(inputDirObs,'dir')==7,'Observations Tables/Grids directory does not exist');

% Check to make sure that the output file is specified correctly
if exist(outFile,'file') > 0
    error('Output file already exists, specify a new file name');
end

% Specify the names of the observations tables to load, comma separated list
tables.dir = inputDirObs;
tables.names = obsTables;

% Output Grids, the name after "grids_out." can be changed to whatever you want, and is used
% in *_specify_output.m
numOutGrids = length(outputGrids)/2;
if rem(numOutGrids,1)>0
    error('outputGrids must be specified as name,value pairs')
end
gridGroups = struct();
for m = 1:numOutGrids
    gridGroups.grids_out.(outputGrids{m*2-1}) = struct('path',inputDirGrids,'grid',outputGrids{m*2},'multiLay',false);
end

%--------------------------------------------------------------------------
% Load table data
%--------------------------------------------------------------------------
% Create the observations structure
obsFields = {'id','loc_id','loc_name','set_name','variable','type','x','y','z_top','z_bot','time_start','time_end','time_zone',...
    'aggregate','value','units','weight','grid','grid_id'};
fieldTypes = {'int32','string','string','string','string','string','double','double','double','double','datetime','datetime','double',...
    'string','double','string','double','string','int32'};

observations = struct();
for m = 1:length(tables.names)
    inFile = [tables.dir,filesep,tables.names{m}];
    fprintf('Reading obs table % s\n',tables.names{m});
    
    % Read in the CSV
    readOpts = delimitedTextImportOptions('VariableNames',obsFields,'VariableTypes',fieldTypes);
    thisData = readtable(inFile,readOpts);
    thisData(1,:) = [];
    
    % Concatenate the tables
    if m == 1
        observations = thisData;
    else
        observations = cat(1,observations,thisData);
    end
end

if ~isempty(fieldnames(observations))
    disp('Processing observations');
    % Uniquely process certain fields
    % This field will be used as a structure array fieldname, so it cannot
    % contain spaces
    observations.type = strrep(observations.type,' ','_');
    observations.set = strrep(observations.set_name,' ','_');
    observations = rmfield(observations,'set_name');
    observations.loc_name = strrep(observations.loc_name,' ','_');
    observations.loc_name = strrep(observations.loc_name,'.','');
    observations.loc_name = strrep(observations.loc_name,',','');
    observations.loc_id = strrep(observations.loc_id,' ','_');
    
    % Correct the times for the time zone, and remove that field
    observations.time_start = datenum(observations.time_start) + observations.time_zone/24;
    observations.time_end = datenum(observations.time_end) + observations.time_zone/24;
    observations.time_zone = [];
    
    % Process into a grouped structure array
    % Convert to a structured array (want to eventually use tables throughout)
    observations = table2struct(observations,'ToScalar',true);
    
    % Split the observations array based on type
    [observations] = split_struct(observations,'type');

    obsTypes = fieldnames(observations);
    for o = 1:length(obsTypes)
        thisType = obsTypes{o};

        % Split the observations array based on variable
        [thisObservations] = split_struct(observations.(thisType),'variable');

        % Sort the values based on the time_start field
        thisObservations = sort_struct(thisObservations,'time_start'); % #ok<NASGU>
        
        % Save this to the OUTF.h5 file
        outVars = fieldnames(thisObservations);
        for m = 1:length(outVars)
            outVar = outVars{m};
            outGroup = [thisType,'/',outVar];
            if m == 1
                h5groupcreate(outFile,thisType);
            end
            h5groupcreate(outFile,outGroup);
            outDataGroup = thisObservations.(outVar);
            fields = fieldnames(outDataGroup);
            for n = 1:length(fields)
                attribs = struct('class',class(outDataGroup.(fields{n})));
                h5datawrite(outFile,[outGroup,'/',fields{n}],outDataGroup.(fields{n}),attribs);
            end
        end
    end
end

% Determine which grid_obs to read in
obsTypes = fieldnames(observations);
readObsGrids = struct();
for m = 1:length(obsTypes)
    uniqueGrids = unique(observations.(obsTypes{m}).grid);
    for n = 1:length(uniqueGrids)
        if ~strcmpi(uniqueGrids{n},'n/a')
            [~,thisName,~] = fileparts(uniqueGrids{n});
            readObsGrids.(thisName).path = tables.dir;
            readObsGrids.(thisName).name = uniqueGrids{n};
            readObsGrids.(thisName).multiLay = false;
        end
    end
end
gridGroups.grids_obs = readObsGrids;

% Now load the grids in
gridGroupNames = fieldnames(gridGroups);
for m = 1:length(gridGroupNames)
    disp(['Reading group ',gridGroupNames{m}])

    % Initialize array
    gridGroup = struct();
    
    % Loop through all of the grids
    gridNames = fieldnames(gridGroups.(gridGroupNames{m}));
    for n = 1:length(gridNames)
        % Call load_grid to create the grid struct, will also load all layers of
        % a multi-layer grid
        if isfield(gridGroups.(gridGroupNames{m}).(gridNames{n}),'grid')
            thisGridName = gridGroups.(gridGroupNames{m}).(gridNames{n}).grid;
        else % assume the grid name is the same as the structure group
            thisGridName = gridNames{n};
        end
        gridGroup.(gridNames{n}) = stat_obs_load_grid(thisGridName,gridGroups.(gridGroupNames{m}).(gridNames{n}));
        disp(gridNames{n})
    end
    
    % Save this grid group
    stat_obs_write_grid_group(outFile,(gridGroupNames{m}),gridGroup);
    
    % Remove the saved array
    clear gridGroup
end

% Display a final status message pointing to the written file
fprintf('OBS file %s written to %s',outFilename,workingDir)

end

