function import_write_dataset(outFile,outGroup,data,dataAttribs,index,stationsXY,lookup,rescale)
%This function provides common dataset writing functionality to input
%preparation scripts

if nargin < 7
    error('If stationsXY or lookup are not to be written, must specify as false');
elseif nargin < 8
    rescale = true;
end

oldPrefix = 'old_';

%Create the group, this will also attempt to create the file
groupExist = h5groupcreate(outFile,outGroup);

%Prompt user for overwrite if it does exist
if groupExist
    get_overwrite_pref(outFile,outGroup);
end

%If we're still going, that means the user decided to overwrite
if groupExist
    %rename the current file
    [outFilePath,outFileName,outFileExt] = fileparts(outFile);
    oldFileName = [oldPrefix,outFileName,outFileExt];
    oldFile = [outFilePath,filesep,oldFileName];
    if exist(oldFile,'file')
        delete(oldFile);
    end
    system(['ren ',outFile,' ',oldFileName]);

    %Create the group again, this will create the new file too
    h5groupcreate(outFile,outGroup);
end

%Write the data table
h5datawrite(outFile,[outGroup,'/data'],data,dataAttribs,rescale);

%Write the index table
attribs = struct('class','double');
h5datawrite(outFile,[outGroup,'/index'],index,attribs)

%Write out the lookup table if it exists
if ~islogical(lookup)
    attribs = struct('class','uint32');
    h5datawrite(outFile,[outGroup,'/lookup'],lookup,attribs);
end

%Write out the stationInfo table
if ~islogical(stationsXY)
    attribs = struct('class','single');
    h5datawrite(outFile,[outGroup,'/stationsXY'],stationsXY,attribs);
end
end
