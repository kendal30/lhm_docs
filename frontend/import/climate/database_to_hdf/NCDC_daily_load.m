function NCDC_daily_load(modelHours,mysql,UTC,workingDir,outFilename,sources,tables)
%Here, there are two things the user must change:
%1) Everything in the "Specify these inputs" area
%2) Check the conversions in "convert values in the table as needed"

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
%Specify intermediate output HDF file information
outFile = [workingDir,filesep,outFilename];

%Build the stations table array
tables.stations.columns = {tables.stations.stationID,tables.stations.modelX,tables.stations.modelY,...
    tables.stations.lat,tables.stations.lon,tables.stations.elev};
tables.stations.vecs = {'stationID','modelX','modelY','lat','lon','elev'};

%Specify intermediate output HDF file information
dataName = sources.precip.name;
dataClass = sources.precip.class;
dataUnits = sources.precip.units;
dataScale = sources.precip.scale;
dataOffset = sources.precip.offset;
dataNan = sources.precip.nan;

%--------------------------------------------------------------------------
%Pull the data from the database
%--------------------------------------------------------------------------
%Open the instance
myopen(mysql.host,mysql.user,mysql.pass);
dbopen(mysql.db);

%Build the where statement
whereStatement = ['WHERE `',tables.where.elemCol,'` = ''',tables.where.elemVal,''' AND ',...
    tables.data.table,'.`',tables.data.stationID,'` = ',tables.stations.table,'.`',tables.stations.stationID,'`'];
queryResult = tbread(tables.data.table,tables.data.vecs,tables.data.cols,whereStatement);
queryStations = tbread(tables.stations.table,tables.stations.vecs,tables.stations.columns);
myclose;

%--------------------------------------------------------------------------
%Assemble the queried data into a single table
%--------------------------------------------------------------------------
queryDayhr = cat(2,queryResult.dayhr1,queryResult.dayhr2,queryResult.dayhr3,queryResult.dayhr4,...
    queryResult.dayhr5,queryResult.dayhr6,queryResult.dayhr7,queryResult.dayhr8,...
    queryResult.dayhr9,queryResult.dayhr10,queryResult.dayhr11,queryResult.dayhr12,...
    queryResult.dayhr13,queryResult.dayhr14,queryResult.dayhr15,queryResult.dayhr16,...
    queryResult.dayhr17,queryResult.dayhr18,queryResult.dayhr19,queryResult.dayhr20,...
    queryResult.dayhr21,queryResult.dayhr22,queryResult.dayhr23,queryResult.dayhr24,...
    queryResult.dayhr25,queryResult.dayhr26,queryResult.dayhr27,queryResult.dayhr28,...
    queryResult.dayhr29,queryResult.dayhr30,queryResult.dayhr31);

queryData = cat(2,queryResult.day1,queryResult.day2,queryResult.day3,queryResult.day4,...
    queryResult.day5,queryResult.day6,queryResult.day7,queryResult.day8,...
    queryResult.day9,queryResult.day10,queryResult.day11,queryResult.day12,...
    queryResult.day13,queryResult.day14,queryResult.day15,queryResult.day16,...
    queryResult.day17,queryResult.day18,queryResult.day19,queryResult.day20,...
    queryResult.day21,queryResult.day22,queryResult.day23,queryResult.day24,...
    queryResult.day25,queryResult.day26,queryResult.day27,queryResult.day28,...
    queryResult.day29,queryResult.day30,queryResult.day31);

queryFlags = cat(2,queryResult.flag1,queryResult.flag2,queryResult.flag3,queryResult.flag4,...
    queryResult.flag5,queryResult.flag6,queryResult.flag7,queryResult.flag8,...
    queryResult.flag9,queryResult.flag10,queryResult.flag11,queryResult.flag12,...
    queryResult.flag13,queryResult.flag14,queryResult.flag15,queryResult.flag16,...
    queryResult.flag17,queryResult.flag18,queryResult.flag19,queryResult.flag20,...
    queryResult.flag21,queryResult.flag22,queryResult.flag23,queryResult.flag24,...
    queryResult.flag25,queryResult.flag26,queryResult.flag27,queryResult.flag28,...
    queryResult.flag29,queryResult.flag30,queryResult.flag31);

%--------------------------------------------------------------------------
%Process flags
%--------------------------------------------------------------------------
%First, handle flags 'A' and 'B' which indicate an accumulation period
%greater than a single day, this includes the 'S' flag.  These will be set
%to 'M' for missing instead since there are not that many in the dataset
%I'm currently using.  An alternative to this would be to distribute the
%data in that period (As in done in hourly).
test = ismember(queryFlags,{'A','B','S'});
queryData(test) = -99999;
queryFlags(test) = {'M'};

%Now, handle trace values, by setting them equal to half the minimum value.
% This will allow for weighting of hourly data to work properly since these
% are not 0 observations.
test = ismember(queryFlags,'T');
queryData(test) = min(queryData(queryData(:)>0)) / 2;

%Now, handle missing values, set these as NaN
test = ismember(queryFlags,'M');
queryData(test) = NaN;

%There are a few values without flags but with bad values besides
test = (queryData == 99999);
queryData(test) = NaN;

%--------------------------------------------------------------------------
%Convert units to meters
%--------------------------------------------------------------------------
%Determine the fraction of the inch
inchFrac = zeros(size(queryResult.unit,1),1);
inchFrac(ismember(queryResult.unit,'HI')) = 100;
inchFrac(ismember(queryResult.unit,'TI')) = 10;

%Replicate this to match the size of the data array
inchFrac = repmat(inchFrac,1,size(queryData,2));

%Convert units
queryData = unit_conversions(queryData./inchFrac,'in','m');

%--------------------------------------------------------------------------
%Build observation time record
%--------------------------------------------------------------------------
%The dayhr column is 4 digits, the first two are the day, the second is the
%hour
queryDay = floor(queryDayhr/100);
queryHr = rem(queryDayhr,100);

%Many observations do not have an attached hour, assign these based on two
%rules: 1) equal to previous hour if that was not also a 99, or 2) equal to
%the most common hour other than 99
%First rule
diffHr = [zeros(size(queryHr,1),1),diff(queryHr,1,2)];
test = (diffHr ~= 0) & (queryHr == 99);
queryHr(test) = queryHr([test(:,2:end),false(size(queryHr,1),1)]);

%Second rule
test = (queryHr == 99);
[uniqueHr,countUnique] = count_unique(queryHr);
queryHr(test) = uniqueHr(countUnique==max(countUnique)); %assign the mode to these values

%Get the queryYear and month
queryYear = repmat(floor(queryResult.yearmonth/100),1,size(queryDay,2));
queryMonth = repmat(rem(queryResult.yearmonth,100),1,size(queryDay,2));
queryMinSec = zeros(size(queryYear));

%Build queryTime
queryTime = datenum(queryYear,queryMonth,queryDay,queryHr,queryMinSec,queryMinSec) - UTC/24;

%--------------------------------------------------------------------------
%Now, assemble the station XY table
%--------------------------------------------------------------------------
%identify unique stations
allStations = count_unique(queryResult.stationID);

%Identify those from the station table, and get proper order
[queryInd,position] = ismember(queryStations.stationID,allStations);

%Be careful with the sorting done for allStations
stationsXY = zeros(length(allStations),6);
stationsXY(position(queryInd),1) = allStations;
stationsXY(position(queryInd),2) = queryStations.modelX(queryInd);
stationsXY(position(queryInd),3) = queryStations.modelY(queryInd);
stationsXY(position(queryInd),4) = queryStations.lat(queryInd);
stationsXY(position(queryInd),5) = queryStations.lon(queryInd);
stationsXY(position(queryInd),6) = queryStations.elev(queryInd);
stationsXY = sortrows(stationsXY,1);

%--------------------------------------------------------------------------
%Parse the query output into multi-column table format
%--------------------------------------------------------------------------
%Replicate the stationID array
stationID = repmat(queryResult.stationID,1,size(queryTime,2));

%Now, reshape all necessary arrays
stationID = stationID(:);
queryTime = queryTime(:);
queryData = queryData(:);

%Remove missing values
test = ~isnan(queryData);
stationID = stationID(test);
queryTime = queryTime(test);
queryData = queryData(test);

%Create a model days array
modelDays = (floor(modelHours(1)):floor(modelHours(end)))';

%Create station data table
[rawData,obsTime] = deal(zeros(length(modelDays),length(allStations)) + NaN); %missing values will be NaN

%now, loop through the station data filling in the table
for m = 1:length(allStations)
    %get the indices from the query for this station
    queryInd = (stationID == allStations(m));
    
    %For convenience
    thisTime = queryTime(queryInd);
    thisData = queryData(queryInd);
    
    %Reject any duplicate dates
    [uniqueDates,countUnique] = count_unique(thisTime);
    nonUnique = uniqueDates(countUnique > 1);
    test = ismember(thisTime,nonUnique);
    thisTime(test) = [];
    thisData(test) = [];
    
    %Sort the data
    thisArray = sortrows([thisTime,thisData]);
    thisTime = thisArray(:,1);
    thisData = thisArray(:,2);
    
    %Keep only the portion of the data within modelHours
    keepRange = bracket_index(thisTime,modelHours(1),modelHours(end));
    
    if ~isempty(keepRange)
        %Again for convenience
        thisTime = thisTime(keepRange(1):keepRange(2));
        thisData = thisData(keepRange(1):keepRange(2));
        
        %Determine which model days are present in this days
        thisDays = floor(thisTime);
        [presentInd,dataLoc] = ismember_dates(modelDays,thisDays,3,true);
                
        %Fill in the data array appropriately, data already sorted
        rawData(presentInd,m) = thisData(dataLoc(presentInd));
        obsTime(presentInd,m) = thisTime(dataLoc(presentInd));
    end
end

%--------------------------------------------------------------------------
%Shift observations when not recorded at the dominant hour
%--------------------------------------------------------------------------
%Identify the observation hours and baseline hour for correction
missingStation = isnan(rawData);
obsHour = round(rem(obsTime,1)*24);
baseHour = mode(obsHour(~missingStation));

%Only correct a station if its observation hour differs by 6 or more hours
correctStations = (abs((obsHour - baseHour)) < 6);
noCorrect = ~any(correctStations,2);
timeShift = sign(obsHour - baseHour);
timeShift(correctStations) = 0;

%For each station, identify closest correct station for all times
closestStation = zeros(size(rawData));
for m = 1:length(allStations)
    %Build a distance table
    thisDist = sqrt((stationsXY(m,2)-stationsXY(:,2)).^2 + (stationsXY(m,3)-stationsXY(:,3)).^2);
    thisTable = [thisDist,(1:length(allStations))'];
    thisTable = sortrows(thisTable,1);
        
    %Identify the closest active station for each hour
    %Now, loop through checking to make sure closest station is correct at
    %that time    
    test = true(size(closestStation,1),1);
    closestCorrect = false(size(closestStation,1),1);
    closeRow = 0;
    while any(test) && closeRow < length(allStations)
        closeRow = closeRow + 1;
        thisClose = thisTable(closeRow,2);
        closestStation(test,m) = thisClose;
        
        closestCorrect(test) = correctStations(test,thisClose);
        test = ~closestCorrect & ~noCorrect;
    end
end

%For incorrect station values
correctData = rawData;
baselineData = zeros(size(rawData)); %store this just for evaluation purposes
for m = 1:length(allStations)
    %Extract a complete timeseries of closest station values
    closeInd = sub2ind(size(rawData),(1:size(rawData,1))',closestStation(:,m));
    baselineCorrect = rawData(closeInd);
    
    %Get the fraction of precipitation occurring on each day relative to
    %the next for the baseline data
    tempCorrect = baselineCorrect;
    tempCorrect(isnan(tempCorrect)) = 0;
    thisFrac = zeros(size(baselineCorrect));
    thisFrac(1:end-1) = tempCorrect(1:end-1) ./ (tempCorrect(1:end-1) + tempCorrect(2:end));
    thisFrac(isnan(thisFrac)) = 0;
    thisFrac(end) = 1;
    
    %Determine indices to correct
    timeInd = find(~correctStations(:,m) & ~isnan(rawData(:,m)));
    timeInd(timeInd==1) = []; %don't correct first or last days
    timeInd(timeInd==size(rawData,1)) = [];
    
    %Want time shift as either -1 or 0 for next calculation
    thisShift = timeShift(:,m);
    thisShift(thisShift==1) = 0; %want negative 1 if earlier than baseline, 0 if later
    
    %Now create an time-corrected daily field for this station
    tempRaw = rawData(:,m);
    tempRaw(isnan(tempRaw)) = 0;
    thisCorrect = rawData(:,m);
    thisCorrect(timeInd) = (1 - thisFrac(timeInd-1)) .* tempRaw(timeInd+thisShift(timeInd)) + ...
        thisFrac(timeInd) .* tempRaw(timeInd+1+thisShift(timeInd)); %This shift simply asks, what fraction
    %of the baseline precip occurred on each day, and shifts the raw precip
    %to be corrected using those fractions, it's not perfect, but it is
    %better than nothing
    
    %Store
    correctData(:,m) = thisCorrect;
    baselineData(:,m) = baselineCorrect;
end
data = correctData;

%Build an output data index
index = [(modelDays-1)+(baseHour+1)/24,modelDays+baseHour/24,zeros(length(modelDays),1)];

%--------------------------------------------------------------------------
%Clean up data, update stationsXY and index tables
%--------------------------------------------------------------------------  
%Remove stations with no data, and index with no data across stations
testNotNan = ~isnan(data);
keepStations = any(testNotNan,1);
keepRows = any(testNotNan,2);
index(~keepRows,3) = NaN;

%Set row index of rows with all zero values == -1, and remove those rows
zeroRows = all((data==0) | isnan(data),2);
index(zeroRows,3) = -1;

%Remove 0 and NaN rows, and bad stations
data = data(keepRows & ~zeroRows,keepStations);

%Set remaining row index values
test = (index(:,3)==0);
index(test,3) = (1:size(data,1))';

%Update stationsXY table
stationsXY = [stationsXY(keepStations',:),(1:sum(keepStations))'];

%--------------------------------------------------------------------------
%Write out the data to an intermediate HDF5 file
%--------------------------------------------------------------------------
%Write out the dataset
tempGroup = ['/',dataName];
attribs = struct('units',dataUnits,'class',dataClass,...
        'scale',dataScale,'offset',dataOffset,'nan',dataNan);
import_write_dataset(outFile,tempGroup,data,attribs,index,stationsXY,false);
end