function NEXRAD_load(workingDir,outFilename,sources,grids)
%Here, there are three things the user must change:
%1) Everything in the "Specify these inputs" area
%2) The conversion of grid names to hours in the "Run through the list of
%grids..." section
%3) The conversion of units in the final grid loading section

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
gridsDir = grids.nexrad.inDir;
clipGridLoc = grids.nexrad.clip;
modelGridLoc = grids.model;

outFile = [workingDir,filesep,outFilename];

outGroup = sources.precip.name;
outClass = sources.precip.class;
gridUnits = sources.precip.units;
gridMult = sources.precip.scale;
nanVal = sources.precip.nan;
offset = sources.precip.offset;

%--------------------------------------------------------------------------
%Read in both the NEXRAD clip grid and the model grid headers, calculate lookup map
%--------------------------------------------------------------------------
%Read in the grids header metadata
clipGeoLoc = import_grid(clipGridLoc,'header');
modelGeoLoc = import_grid(modelGridLoc,'header');

%Now, run the lookup mapping function
lookup = cell_statistics(clipGeoLoc,modelGeoLoc);
lookup = uint32(lookup);

%--------------------------------------------------------------------------
%Parse the grids directory, create a structure containing all input grids
%--------------------------------------------------------------------------
dirStruct = dir([gridsDir,'\**\w001001.adf']);

%--------------------------------------------------------------------------
%Run through the list of grids, checking to see if precip is > 0, writing
%out grid hour
%--------------------------------------------------------------------------
numGrids = length(dirStruct);
nonZero = false(numGrids,1);
gridHour = zeros(numGrids,1);
h = waitbar(0,'Checking for nonzero precip events or bad grids');
for m = 1:numGrids
    waitbar(m/numGrids,h);
    tempGrid = import_grid(dirStruct(m).name);
    tempGridName = fliplr(strtok(fliplr(dirStruct(m).name(1:end-12)),'\'));
    if length(tempGridName) == 12
        gridHour(m) = datenum([tempGridName(2:5),tempGridName(6:7),tempGridName(8:9),tempGridName(10:11)],...
            'yyyymmddHH');
        nonZero(m) = any(tempGrid(:)>0);
    end
end
if ishandle(h); close(h); end

%If gridHour == 0, then the grid was not a NEXRAD grid
test = ~(gridHour == 0);
dirStruct = dirStruct(test);
gridHour = gridHour(test);
nonZero = nonZero(test);

%Check for duplicates (should not be any,  but nevertheless)
test = ~[false;(diff(gridHour) == 0)];
dirStruct = dirStruct(test);
gridHour = gridHour(test);
nonZero = nonZero(test);

%--------------------------------------------------------------------------
%Go back through the grids, importing all those with nonZero values
%--------------------------------------------------------------------------
numNonZero = sum(nonZero);
numCells = numel(tempGrid);
nonZeroGrids = find(nonZero);
badGrids = false(numNonZero,1);
data = zeros(numNonZero,numCells,outClass);
h = waitbar(0,'Importing precip grids');
for m = 1:numNonZero
    waitbar(m/numNonZero,h);
    tempGrid = import_grid(dirStruct(nonZeroGrids(m)).name);
    tempGrid(tempGrid<0) = NaN;
    badGrids(m) = (sum(isnan(tempGrid(:))) >= numCells*2/3); %If 2/3 of the grid cells are NaN, throw it away
    if ~badGrids(m)
        tempGrid = unit_conversions(tempGrid,'mm','m'); %Input grids are units mm
        tempGrid = uint16(tempGrid * str2double(gridMult));
        tempGrid(tempGrid<0) = str2double(nanVal);
        data(m,:) = reshape(tempGrid(:),1,[]);
    end
end
if ishandle(h); close(h); end

%Remove badGrids rows from the data array
data = data(~badGrids,:);

%--------------------------------------------------------------------------
%Create the index array
%--------------------------------------------------------------------------
index = zeros(length(gridHour),2);
index(:,1) = gridHour;
index(~nonZero,2) = -1; %flag indicating zero input
index(nonZeroGrids(badGrids),2) = NaN; %indicating missing grid (or bad values)
test = (index(:,2) == 0);
index(test,2) = (1:size(data,1)); %row indices of the grids

%Sort the data, as they may not be date-ordered
index = sortrows(index);
data = data(index(index(:,2)>0,2),:);

%Renumber the index
index(index(:,2)>0,2) = (1:size(data,1));

%--------------------------------------------------------------------------
%Write the array to an HDF5 file
%--------------------------------------------------------------------------
%Create the output data variable in the HD5 File, specify 'false' in the
%h5datawrite call because the transforms have already been done
attribs = struct('class',outClass,'units',gridUnits,'scale',gridMult,'nan',nanVal,'offset',offset);
import_write_dataset(outFile,outGroup,data,attribs,index,false,lookup,false);
end
