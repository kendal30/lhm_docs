function NLDAS_load_bootstrap_monthly(modelHours,workingDir,outFilename,sources,grids,shapeFiles)
%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
extractVars = {'precip','airTemp','solarShortwave','windspeedU','windspeedV','specHum','pressure','solarLongwave'};
extractSpatialAgg = shapeFiles.nldas;
extractSpatialStat = {'raw','raw','raw','raw','raw','raw','raw','raw'};
extractTemporalAgg = 'hours';
extractTemporalStat = {'sum','mean','mean','mean','mean','mean','mean','mean'};
warning('off','TimeSeriesAggregate:no_aggregation')

%--------------------------------------------------------------------------
%First, prepare the latitude, longitude, and elevation data, and lookup
%grid
%--------------------------------------------------------------------------
%Get the indices for each model year
[allYears,~,~,~,~,~] = datevec(modelHours);
years = unique(allYears);
numYears = length(years);

%Specify some output information the datasets
outFile = [workingDir,filesep,outFilename];
outDatasets = {'radBeam','radDiff','sunZenithCos',...
    'airTemp','precip','windspeed',...
    'relHum','pressure','radLong'};
outDatasetNames = {sources.radBeam.name,sources.radDiff.name,sources.sunZenithCos.name,...
    sources.airTemp.name,sources.precip.name,sources.windspeed.name,...
    sources.relHum.name,sources.pressure.name,sources.radLong.name};
outIndices = {'radBeam','radDiff','other',...
    'other','precip','other',...
    'other','other','other'};
outSpecialIndex = {'precip','radBeam','radDiff'};

%Load the year/month bootstrap file
dataBootstrap = read_excel(sources.fileMonths,sources.fileTab,'table');
inBootstrap = [[dataBootstrap(:).Out_Year]',[dataBootstrap(:).Out_Month]',...
    [dataBootstrap(:).In_Year]',[dataBootstrap(:).In_Month]'];

%--------------------------------------------------------------------------
%Read in the NLDAS data yearly
%--------------------------------------------------------------------------
%Initialize the counter
lastRow = struct('precip',0,'radBeam',0,'radDiff',0,'other',0);

h = waitbar(0,'Importing NLDAS data by year');
for m = 1:numYears
    waitbar(m/numYears,h);
    %Determine the base index for this year
    thisYear = years(m);
    index.other = repmat(modelHours(allYears==thisYear),1,2);
    index.other(:,2) = 0;
    index.other(:,3) = 1;

    for n = 1:12 %get the monthly value
        %Determine which month to extract
        rowBootstrap = find((inBootstrap(:,1)==thisYear) & (inBootstrap(:,2)==n),1);

        %Now, build hourly arrays
        thisOutTime = (datenum(inBootstrap(rowBootstrap,1),inBootstrap(rowBootstrap,2),1):1/24:...
            datenum(inBootstrap(rowBootstrap,1),inBootstrap(rowBootstrap,2)+1,1)-1/24)';
        thisExtractTime = (datenum(inBootstrap(rowBootstrap,3),inBootstrap(rowBootstrap,4),1):1/24:...
            datenum(inBootstrap(rowBootstrap,3),inBootstrap(rowBootstrap,4)+1,1)-1/24)';

        %If this is a leap year, handle properly
        if n == 2
            if length(thisOutTime) < length(thisExtractTime) %outTime is not a leap year, but extract time is
                thisExtractTime = thisExtractTime(1:length(thisOutTime)); %Don't include February 29th
            elseif length(thisOutTime) > length(thisExtractTime) %outTime is a leap year, but extract time isn't
                thisExtractTime = (datenum(inBootstrap(rowBootstrap,3),inBootstrap(rowBootstrap,4),1):1/24:...
                    datenum(inBootstrap(rowBootstrap,3),inBootstrap(rowBootstrap,4)+1,1)+23/24)'; %add March 1st
            end
        end

        %Get the range for the extract function
        extractRange = [thisExtractTime(1),thisExtractTime(end)];

        if m==1 && n == 1
            [thisExtractTimeActual,thisOutData,headerNLDAS,precalcExtract] = reanalysis_extract_yearly_files(extractVars,extractSpatialAgg,...
                extractSpatialStat,extractRange,extractTemporalAgg,extractTemporalStat,0);

            %Create the lookup grid to map the NLDAS grid (regular Lat/Lon)
            %coordinates to the model grid (regular linear units in projection)
            %Read in the Latitude, Longitude, and elevaton grids for the model
            latModel = import_grid(grids.latitude);
            lonModel = import_grid(grids.longitude);
            if isa(grids.elevation,'string') || isa(grids.elevation,'char')
                elevModel = import_grid(grids.elevation);
            else
                elevModel = grids.elevation + zeros(size(latModel)); %assume a number was provided
            end

            %Identify data cells that are all NaNs, these are absent from the
            %Reanalysis. Assume that this is uniform across years.
            testNan = all(isnan(thisOutData{1}),3);
            headerNLDAS.mask(testNan) = false;

            %Run the knnsearch tool to build the lookup grid
            modelXY = [lonModel(:),latModel(:)];
            nldasXYMask = [headerNLDAS.gridX(headerNLDAS.mask),headerNLDAS.gridY(headerNLDAS.mask)];
            lookup = knnsearch(nldasXYMask,modelXY);
            lookup = uint16(reshape(lookup,size(latModel)));

            %Now, determine the elevation of each NLDAS cell
            %Run knnsearch in reverse
            nldasXY = [headerNLDAS.gridX(:),headerNLDAS.gridY(:)];
            lookupReverse = reshape(knnsearch(modelXY,nldasXY),size(headerNLDAS.gridX));
            elevNLDAS = elevModel(lookupReverse);

            %Prepare some arrays used below
            location = struct('latitude',headerNLDAS.gridY(headerNLDAS.mask),'longitude',...
                headerNLDAS.gridX(headerNLDAS.mask),'altitude',elevNLDAS(headerNLDAS.mask));

            %Save output header data
            header = headerNLDAS; %#ok<NASGU>
            save([workingDir,filesep,sources.fileHeader],'header');
        else
            [thisExtractTimeActual,thisOutData] = reanalysis_extract_yearly_files(extractVars,extractSpatialAgg,extractSpatialStat,...
                extractRange,extractTemporalAgg,extractTemporalStat,0,precalcExtract);
        end

        %Check for missing hours
        thisMissingHours = ~ismember_dates(thisExtractTime,thisExtractTimeActual,4,false);
        thisOutTime(thisMissingHours) = [];

        %Concatenate months
        if n == 1
            %Create the annual outData array
            outTime = thisOutTime;
            outData = thisOutData;
        else
            outTime = cat(1,outTime,thisOutTime);
            for o = 1:length(outData)
                outData{o} = cat(3,outData{o},thisOutData{o});
            end
        end
    end
    %Save outputs for debugging
%     save('temp.mat','outData','headerNLDAS','precalcExtract','location','lookup','outTime','-v7.3');


    %Check to make sure that outTime match extractTimes
    missingHours = ~ismember_dates(index.other(:,1),outTime,4,false);
    index.other(missingHours,:) = [];

    %Restructure the data
    outStruct = struct();
    for n = 1:length(extractVars)
        shapeData = size(outData{n});
        outStruct.(extractVars{n}) = reshape(permute(outData{n},[3,1,2]),[shapeData(3),shapeData(2)*shapeData(1)]);
        outStruct.(extractVars{n}) = outStruct.(extractVars{n})(:,headerNLDAS.mask(:)); %now mask the data
    end

    %Calculate some quantities from NLDAS data-----------------------------
    %Calculate windspeed from two components
    outStruct.windspeed = (outStruct.windspeedU.^2 + outStruct.windspeedV.^2).^(1/2);

    %Calculate the sun position at each time for all locations
    [~,zenith,~] = sun_position(outTime,location.latitude,...
        location.longitude,location.altitude); %need time offset here because station data records at end of hour, previous hour's average
    outStruct.sunZenithCos = max(0,cos(unit_conversions(zenith,'deg','rad')));

    %Calculate the diffuse/beam split
    outStruct.diffFrac = diffuse_beam_split(outStruct.radShort,outStruct.sunZenithCos);
    outStruct.radDiff = outStruct.radShort .* outStruct.diffFrac;
    outStruct.radBeam = outStruct.radShort - outStruct.radDiff;

    %Calculate relative humidity from specific humidity
    satVapPress = sat_vap_pressure(outStruct.airTemp,'goff');
    outStruct.relHum = outStruct.specHum .* outStruct.pressure ./ (0.622 * satVapPress) * 100;
    outStruct.relHum(outStruct.relHum>100) = 100;

    %Clear out zeros and build indices-------------------------------------
    for n = 1:length(outSpecialIndex)
        thisDatasetName = outSpecialIndex{n};

        %Determine zeros of precipitation and shortwave radiation
        zeros.(thisDatasetName) = all(outStruct.(thisDatasetName)==0 | isnan(outStruct.(thisDatasetName)),2);

        %Create data arrays without those rows
        outStruct.(thisDatasetName) = outStruct.(thisDatasetName)(~zeros.(thisDatasetName),:);

        %Make new indices filling in zero grids with -1
        index.(thisDatasetName) = index.other;
        index.(thisDatasetName)(zeros.(thisDatasetName),2) = -1;

        %Get the size of each data array
        numActive.(thisDatasetName) = sum(~zeros.(thisDatasetName));

        %Now, complete the index rows
        index.(thisDatasetName)(~zeros.(thisDatasetName),2) = (lastRow.(thisDatasetName)+1:lastRow.(thisDatasetName)+ numActive.(thisDatasetName));
    end
    %Now do the others
    numActive.other = size(outStruct.radShort,1);
    index.other(:,2) = (lastRow.other+1:lastRow.other+numActive.other);

    %--------------------------------------------------------------------------
    %Write out the datasets
    %--------------------------------------------------------------------------
    for n = 1:length(outDatasets)
        tempGroup = ['/',outDatasetNames{n}];
        attribs = struct('class','');
        if m == 1 %only on the first loop
            %Create the output group
            groupExist = h5groupcreate(outFile,tempGroup);

            if groupExist
                %If it exists, must rename old file or stop
                get_overwrite_pref(outFile,tempGroup);
                %If we're still going, that means the user decided to
                %overwrite
                %rename the current file
                [outFilePath,outFileName,outFileExt] = fileparts(outFile);
                oldFileName = ['old_',outFileName,outFileExt];
                oldFile = [outFilePath,filesep,oldFileName];
                if exist(oldFile,'file');
                    delete(oldFile);
                end
                system(['ren ',outFile,' ',oldFileName]);

                %Create the group again, this will create the new file too
                h5groupcreate(outFile,tempGroup);
            end

            %Write the lookup table
            attribs.class = class(lookup);
            h5datawrite(outFile,[tempGroup,'/lookup'],lookup,attribs,false);
        end

        %Write the output index
        attribs.class = class(index.(outIndices{n}));
        indexShape = size(index.(outIndices{n}));
        h5datawrite(outFile,[tempGroup,'/index'],index.(outIndices{n}),attribs,false,[lastRow.other,0],indexShape,[]);

        %Write the output data
        attribs = sources.(outDatasets{n});
        attribs = rmfield(attribs,'name');
        outShape = size(outStruct.(outDatasets{n}));
        h5datawrite(outFile,[tempGroup,'/data'],outStruct.(outDatasets{n}),attribs,true,[lastRow.(outIndices{n}),0],outShape,[]);
    end

    for n = 1:length(outSpecialIndex)
        thisDatasetName = outSpecialIndex{n};
        %Update the last rows counter
        lastRow.(thisDatasetName) = lastRow.(thisDatasetName) + numActive.(thisDatasetName);
    end
    lastRow.other = lastRow.other + numActive.other;
end
if ishandle(h); close(h); end

%Clean up if successful
warning('on','TimeSeriesAggregate:no_aggregation')
% delete('temp.mat');
end
