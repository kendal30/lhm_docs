function weather_station_load(modelHours,mysql,UTC,workingDir,outFilename,sources,tables)
%One thing might need changing by the user:
%1) Check the conversions in "convert values in the table as needed"

%--------------------------------------------------------------------------
%Assemble these inputs
%--------------------------------------------------------------------------
%Specify intermediate output HDF file information
outFile = [workingDir,filesep,outFilename];

%Input datatypes
dataTypes = fieldnames(sources);
dataTypes(strcmpi(dataTypes,'file')) = [];

%Build the table array
table.table = tables.data.table;
table.dataCols = {tables.data.stationID,tables.data.date};
table.dataVecs = {'stationID','date'};
for m = 1:length(dataTypes)
    table.dataCols{2+m} = sources.(dataTypes{m}).tableCol;
    table.dataVecs{2+m} = sources.(dataTypes{m}).name;
end
table.dataInd = (3:length(dataTypes)+2);
table.dateInd = 2;

%Build the stations table array
table.stations.table = tables.stations.table;
table.stations.columns = {tables.stations.stationID,tables.stations.numID,tables.stations.modelX,...
    tables.stations.modelY,tables.stations.lat,tables.stations.lon,tables.stations.elev};
table.stations.vecs = {'stationID','numID','modelX','modelY','lat','lon','elev'};

%Build the outSpec arrays
[dataName,dataClass,dataUnits,dataScale,dataOffset,dataNan] = deal(cell(length(dataTypes),1));
for m = 1:length(dataTypes)
    dataName{m} = sources.(dataTypes{m}).name;
    dataClass{m} = sources.(dataTypes{m}).class;
    dataUnits{m} = sources.(dataTypes{m}).units;
    dataScale{m} = sources.(dataTypes{m}).scale;
    dataOffset{m} = sources.(dataTypes{m}).offset;
    dataNan{m} = sources.(dataTypes{m}).nan;
end

%--------------------------------------------------------------------------
%Pull the data from the database
%--------------------------------------------------------------------------
myopen(mysql.host,mysql.user,mysql.pass);
dbopen(mysql.db);
queryResult = tbread(table.table,table.dataVecs,table.dataCols);
myclose;

%--------------------------------------------------------------------------
%Convert values in the table as needed
%--------------------------------------------------------------------------
%Parse the input modelHours, and convert to UTC
queryDate = datenum(queryResult.(table.dataVecs{table.dateInd}),'yyyy-mm-dd HH:MM:SS') - UTC/24;

%--------------------------------------------------------------------------
%Parse the query output into multi-column table format
%--------------------------------------------------------------------------
%identify unique stations
allStations = unique(queryResult.stationID);

%create data, stations, and index tables
data = struct();
stations = struct();
index = struct();
for m = table.dataInd
    data.(table.dataVecs{m}) = zeros(length(modelHours),length(allStations)) + NaN; %missing values will be NaN
    index.(table.dataVecs{m}) = [modelHours,zeros(length(modelHours),1)];
    stations.(table.dataVecs{m}) = allStations;
end

%now, loop through the station data filling in the table
for m = 1:length(allStations)
    %get the indices from the query for this station
    queryInd = strcmp(allStations{m},queryResult.stationID);
    %get the indices for the master stationData table
    [dataInd,tempDataInd] = ismember_dates(modelHours,queryDate(queryInd),4,true);
    tempDataInd = tempDataInd(tempDataInd~=0);
    if any(dataInd)
        %Assemble the station data
        for n = table.dataInd
            tempInd = find(queryInd);
            data.(table.dataVecs{n})(dataInd,m) = queryResult.(table.dataVecs{n})(tempInd(tempDataInd));
        end
    end
end

%--------------------------------------------------------------------------
%Remove stations with no data, and index with no data across stations
%--------------------------------------------------------------------------
for m = table.dataInd
    keepStations = any(~isnan(data.(table.dataVecs{m})),1);
    keepRows = any(~isnan(data.(table.dataVecs{m})),2);
    data.(table.dataVecs{m}) = data.(table.dataVecs{m})(keepRows,keepStations);
    index.(table.dataVecs{m})(~keepRows,2) = NaN;
    index.(table.dataVecs{m})(keepRows,2) = (1:size(data.(table.dataVecs{m}),1))';
    stations.(table.dataVecs{m}) = stations.(table.dataVecs{m})(keepStations);
end

%--------------------------------------------------------------------------
%Now, assemble the station XY table
%--------------------------------------------------------------------------
myopen(mysql.host,mysql.user,mysql.pass);
dbopen(mysql.db);
queryOutput = tbread(table.stations.table,table.stations.vecs,table.stations.columns);
myclose

queryInd = true(size(queryOutput.stationID));
position = zeros(size(queryOutput.stationID));
for m = 1:length(queryInd);
    tempTest = strcmp(queryOutput.stationID{m},allStations);
    queryInd(m) = any(tempTest);
    if queryInd(m)
        position(m) = find(tempTest);
    end
end
stationInfo = zeros(length(allStations),6);
%Be careful with the sorting done for allStations
stationInfo(position(queryInd),1) = queryOutput.numID(queryInd);
stationInfo(position(queryInd),2) = queryOutput.modelX(queryInd);
stationInfo(position(queryInd),3) = queryOutput.modelY(queryInd);
stationInfo(position(queryInd),4) = queryOutput.lat(queryInd);
stationInfo(position(queryInd),5) = queryOutput.lon(queryInd);
stationInfo(position(queryInd),6) = queryOutput.elev(queryInd);

%--------------------------------------------------------------------------
%For each dataset, create a station XY table
%--------------------------------------------------------------------------
for m = table.dataInd
    matchStations = ismember(allStations,stations.(table.dataVecs{m}));
    stationsXY.(table.dataVecs{m}) = [stationInfo(matchStations,:),(1:sum(matchStations))'];
end

%--------------------------------------------------------------------------
%Write out the data to an intermediate HDF5 file
%--------------------------------------------------------------------------
%Write out the datatype groups
for m = table.dataInd
    tempInd = m - table.dataInd(1) + 1;
    %Write out the dataset
    tempGroup = ['/',dataName{tempInd}];
    attribs = struct('units',dataUnits{tempInd},'class',dataClass{tempInd},...
        'scale',dataScale{tempInd},'offset',dataOffset{tempInd},'nan',dataNan{tempInd});
    import_write_dataset(outFile,tempGroup,data.(table.dataVecs{m}),attribs,...
        index.(table.dataVecs{m}),stationsXY.(table.dataVecs{m}),false);
end
end