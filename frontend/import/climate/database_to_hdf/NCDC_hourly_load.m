function NCDC_hourly_load(modelHours,mysql,UTC,workingDir,outFilename,sources,tables)
%Here, there are two things the user must change:
%1) Everything in the "Specify these inputs" area
%2) Check the conversions in "convert values in the table as needed"

%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
%Specify intermediate output HDF file information
outFile = [workingDir,filesep,outFilename];

%specify data table information
table.table = tables.data.table;
table.dataCols = tables.data.cols;
table.dataVecs = tables.data.vecs;

%Build the stations table array
table.stations.table = tables.stations.table;
table.stations.columns = {tables.stations.stationID,tables.stations.modelX,tables.stations.modelY,...
    tables.stations.lat,tables.stations.lon,tables.stations.elev};
table.stations.vecs = {'stationID','modelX','modelY','lat','lon','elev'};

%Specify intermediate output HDF file information
dataName = sources.precip.name;
dataClass = sources.precip.class;
dataUnits = sources.precip.units;
dataScale = sources.precip.scale;
dataOffset = sources.precip.offset;
dataNan = sources.precip.nan;

%--------------------------------------------------------------------------
%Pull the data from the database
%--------------------------------------------------------------------------
myopen(mysql.host,mysql.user,mysql.pass);
dbopen(mysql.db);
whereStatement = ['WHERE ',tables.data.table,'.`',tables.data.stationID,'` = ',tables.stations.table,'.`',tables.stations.stationID,'`'];
queryResult = tbread(table.table,table.dataVecs,table.dataCols,whereStatement);
myclose;

%--------------------------------------------------------------------------
%Assemble the queried data into a single table
%--------------------------------------------------------------------------
queryHr = cat(2,queryResult.hr1,queryResult.hr2,queryResult.hr3,queryResult.hr4,...
    queryResult.hr5,queryResult.hr6,queryResult.hr7,queryResult.hr8,...
    queryResult.hr9,queryResult.hr10,queryResult.hr11,queryResult.hr12,...
    queryResult.hr13,queryResult.hr14,queryResult.hr15,queryResult.hr16,...
    queryResult.hr17,queryResult.hr18,queryResult.hr19,queryResult.hr20,...
    queryResult.hr21,queryResult.hr22,queryResult.hr23,queryResult.hr24);

queryData = cat(2,queryResult.hour1,queryResult.hour2,queryResult.hour3,queryResult.hour4,...
    queryResult.hour5,queryResult.hour6,queryResult.hour7,queryResult.hour8,...
    queryResult.hour9,queryResult.hour10,queryResult.hour11,queryResult.hour12,...
    queryResult.hour13,queryResult.hour14,queryResult.hour15,queryResult.hour16,...
    queryResult.hour17,queryResult.hour18,queryResult.hour19,queryResult.hour20,...
    queryResult.hour21,queryResult.hour22,queryResult.hour23,queryResult.hour24);

queryFlags = cat(2,queryResult.flag1,queryResult.flag2,queryResult.flag3,queryResult.flag4,...
    queryResult.flag5,queryResult.flag6,queryResult.flag7,queryResult.flag8,...
    queryResult.flag9,queryResult.flag10,queryResult.flag11,queryResult.flag12,...
    queryResult.flag13,queryResult.flag14,queryResult.flag15,queryResult.flag16,...
    queryResult.flag17,queryResult.flag18,queryResult.flag19,queryResult.flag20,...
    queryResult.flag21,queryResult.flag22,queryResult.flag23,queryResult.flag24);

%--------------------------------------------------------------------------
%Convert units to meters, convert NaNs
%--------------------------------------------------------------------------
%Convert NaN values
queryData(queryData == -99999) = NaN;
queryData(queryData == 99999) = NaN;

%Determine the fractions of the inch
inchFrac = zeros(size(queryResult.unit,1),1);
inchFrac(ismember(queryResult.unit,{'HI','HT'})) = 100;

%Determine the precision (in data units)
queryPrecis = zeros(size(queryResult.unit,1),1);
queryPrecis(ismember(queryResult.unit,'HI')) = 0.01;
queryPrecis(ismember(queryResult.unit,'HT')) = 0.1;

%Replicate this to match the size of the data array
inchFrac = repmat(inchFrac,1,size(queryData,2));
queryPrecis = repmat(queryPrecis,1,size(queryData,2));

%Convert units
queryData = unit_conversions(queryData./inchFrac,'in','m');
queryPrecis = unit_conversions(queryPrecis,'in','m');

%--------------------------------------------------------------------------
%Build observation time record
%--------------------------------------------------------------------------
%Build appropriately-sized year, month, and day arrays
queryYear = repmat(queryResult.year,1,size(queryHr,2));
queryMonth = repmat(queryResult.month,1,size(queryHr,2));
queryDay = repmat(queryResult.day,1,size(queryHr,2));

%Convert hours to decimal
queryHr = floor(queryHr/100);

%Build empty minutes and seconds arrays
queryMinSec = zeros(size(queryHr));

%Build queryTime
queryTime = datenum(queryYear,queryMonth,queryDay,queryHr,queryMinSec,queryMinSec) - UTC/24;

%--------------------------------------------------------------------------
%Parse the query output into multi-column table format
%--------------------------------------------------------------------------
%identify unique stations
allStations = unique(queryResult.stationID);

%Replicate the stationID array
stationID = repmat(queryResult.stationID,1,size(queryTime,2));

%Now, reshape all necessary arrays
stationID = stationID(:);
queryTime = queryTime(:);
queryData = queryData(:);
queryFlags = queryFlags(:);

%create station data table and index
[data,precision] = deal(zeros(length(modelHours),length(allStations)) + NaN); %missing values will be NaN
index = [modelHours,zeros(length(modelHours),1)];
stations = allStations;

%now, loop through the station data filling in the table
for m = 1:length(allStations)
    %get the indices from the query for this station
    queryInd = (stationID == allStations(m));
    queryInd = find(queryInd);
    
    %For convenience
    thisTime = queryTime(queryInd);
    thisData = queryData(queryInd);
    thisPrecis = queryPrecis(queryInd);
    
    %Reject any duplicate dates
    [uniqueDates,countUnique] = count_unique(thisTime);
    nonUnique = uniqueDates(countUnique > 1);
    test = ismember(thisTime,nonUnique);
    thisTime(test) = [];
    thisData(test) = [];
    thisPrecis(test) = [];
    queryInd(test) = [];
    
    %Sort the data, keep track of the index sorting to use for flags
    thisArray = sortrows([thisTime,thisData,thisPrecis,queryInd]);
    thisTime = thisArray(:,1);
    thisData = thisArray(:,2);
    thisPrecis = thisArray(:,3);
    queryInd = thisArray(:,4);
    
    %Keep only the portion of the data within modelHours
    keepRange = bracket_index(thisTime,modelHours(1),modelHours(end));
    
    if ~isempty(keepRange)
        thisTime = thisTime(keepRange(1):keepRange(2));
        thisData = thisData(keepRange(1):keepRange(2));
        thisPrecis = thisPrecis(keepRange(1):keepRange(2));
        queryInd = queryInd(keepRange(1):keepRange(2));
        
        %Get the queryFlags
        thisFlags = queryFlags(queryInd);
        
        %Now, look through the flags and find periods where the 'a' and 'A'
        %or 'C' accumulation flags are used, distribute precipitation
        %within those
        startAccum = find(ismember(thisFlags,'a'));
        endAccum = find(ismember(thisFlags,'A'));
        
        if ~isempty(startAccum) || ~isempty(endAccum)
            %Have to line these up, because the start and end of the dataset
            %have been chopped, an open bracket might lie on either end
            if startAccum(1) > endAccum(1)
                startAccum = [1;startAccum]; %#ok<AGROW>
            end
            if startAccum(end) > endAccum(end);
                endAccum = [endAccum;length(thisData)]; %#ok<AGROW>
            end
            
            %Loop through the dataset, and replace the accumulated values as
            %averages across the accumulation period
            for n = 1:length(startAccum)
                %Check to see if the accumulation period is longer than 12
                %hours, if so, fill with NaN
                %Otherwise, write the entire range with the average of that value
                %across the accumulation period
                if endAccum(n) - startAccum(n) + 1 > 12
                    thisData(startAccum(n):endAccum(n)) = NaN;
                    thisPrecis(startAccum(n):endAccum(n)) = NaN;
                else
                    %Identify the accumulated value
                    thisData(startAccum(n):endAccum(n)) = thisData(endAccum(n))./ (endAccum(n) - startAccum(n) + 1);
                end
            end
        end
        
        %Now, look through those flags and find periods where the '[' or
        %'{' bracket indicates deleted
        startMissing = find(ismember(thisFlags,'['));
        endMissing = find(ismember(thisFlags,']'));
        startDeleted = find(ismember(thisFlags,'{'));
        endDeleted = find(ismember(thisFlags,'}'));
        
        %Add the two types together, and sort
        startNan = sort([startMissing;startDeleted]);
        endNan = sort([endMissing;endDeleted]);
        
        if ~isempty(startNan) || ~isempty(endNan)
            %Have to line these up, because the start and end of the dataset
            %have been chopped, an open bracket might lie on either end
            if startNan(1) > endNan(1)
                endNan = endNan(2:end);
            end
            if startNan(end) > endNan(end);
                startNan = startNan(1:end-1);
            end
            
            %Now, for all pairs of startNan, endNan, fill in the data with NaN
            for n = 1:length(startNan)
                %Overwrite these all with NaNs
                thisData(startNan(n):endNan(n)) = NaN;
                thisPrecis(startNan(n):endNan(n)) = NaN;
            end
        end
        
        
        %Get indices of thisData within the data array
        [presentInd] = ismember_dates(modelHours,thisTime,4,true);
        
        %Record the data
        data(presentInd,m) = thisData;
        precision(presentInd,m) = thisPrecis;
    end
end

%--------------------------------------------------------------------------
%Remove stations with no data, and index with no data across stations
%--------------------------------------------------------------------------
keepStations = any(~isnan(data),1);
keepRows = any(~isnan(data),2);
index(~keepRows,2) = NaN;

%--------------------------------------------------------------------------
%Set row index of rows with all zero values == -1, and remove those rows
%--------------------------------------------------------------------------
zeroRows = all((data==0) | isnan(data),2);
index(zeroRows,2) = -1;

%--------------------------------------------------------------------------
%Remove 0 and NaN rows, and bad stations
%--------------------------------------------------------------------------
data = data(keepRows & ~zeroRows,keepStations);
precision = precision(keepRows & ~zeroRows,keepStations);
stations = stations(keepStations);

%--------------------------------------------------------------------------
%Set remaining row index values
%--------------------------------------------------------------------------
test = (index(:,2)==0);
index(test,2) = (1:size(data,1))';

%--------------------------------------------------------------------------
%Now, assemble the station XY table
%--------------------------------------------------------------------------
myopen(mysql.host,mysql.user,mysql.pass);
dbopen(mysql.db);
queryOutput = tbread(table.stations.table,table.stations.vecs,table.stations.columns);
myclose

if iscell(queryOutput.stationID)
    queryOutput.stationID = str2num(cell2mat(queryOutput.stationID)); %#ok<ST2NM>
end
[queryInd,position] = ismember(queryOutput.stationID,allStations);
stationInfo = zeros(length(allStations),6);  %include columns for lat and lon here
%Be careful with the sorting done for allStations
stationInfo(position(queryInd),1) = allStations;
stationInfo(position(queryInd),2) = queryOutput.modelX(queryInd);
stationInfo(position(queryInd),3) = queryOutput.modelY(queryInd);
stationInfo(position(queryInd),4) = queryOutput.lat(queryInd);
stationInfo(position(queryInd),5) = queryOutput.lon(queryInd);
stationInfo(position(queryInd),6) = queryOutput.elev(queryInd);

%--------------------------------------------------------------------------
%For each dataset, create a station XY table
%--------------------------------------------------------------------------
matchStations = ismember(allStations,stations);
stationsXY = [stationInfo(matchStations,:),(1:sum(matchStations))'];

%--------------------------------------------------------------------------
%Write out the data to an intermediate HDF5 file
%--------------------------------------------------------------------------
%Write out the dataset
tempGroup = ['/',dataName];
attribs = struct('units',dataUnits,'class',dataClass,...
        'scale',dataScale,'offset',dataOffset,'nan',dataNan);
import_write_dataset(outFile,tempGroup,data,attribs,index,stationsXY,false);

%Write an additional precision field for later use
attribs = struct('class','single');
h5datawrite(outFile,[tempGroup,'/precision'],precision,attribs);
end