function GLDAS_load(modelHours,workingDir,outFilename,sources,grids,shapeFiles,overwrite)

%Validate inputs
if nargin < 7
    overwrite = false;
end


%--------------------------------------------------------------------------
%Specify these inputs
%--------------------------------------------------------------------------
extractSpec.vars = {'precip','airTemp','solarShortwave','windspeed','specHum','pressure','solarLongwave'};
extractSpec.outName = {'precip','airTemp','radShort','windspeed','specHum','pressure','radLong'};
extractSpec.spatialAgg = shapeFiles.reanExtract;
extractSpec.spatialStat = {'raw','raw','raw','raw','raw','raw','raw'};
extractSpec.temporalAgg = 'hours';
extractSpec.temporalStat = {'sum','mean','mean','mean','mean','mean','mean'};
extractSpec.UTC = 0;
warning('off','TimeSeriesAggregate:no_aggregation')

%--------------------------------------------------------------------------
%First, prepare the latitude, longitude, and elevation data, and lookup grid
%--------------------------------------------------------------------------
%Get the indices for each model year
[allYears,~,~,~,~,~] = datevec(modelHours);
years = unique(allYears);
numYears = length(years);

%Specify some output information the datasets
outFile = [workingDir,filesep,outFilename];
outDatasets = {'radBeam','radDiff','sunZenithCos',...
    'airTemp','precip','windspeed',...
    'relHum','pressure','radLong'};
outDatasetNames = {sources.radBeam.name,sources.radDiff.name,sources.sunZenithCos.name,...
    sources.airTemp.name,sources.precip.name,sources.windspeed.name,...
    sources.relHum.name,sources.pressure.name,sources.radLong.name};
outIndices = {'radBeam','radDiff','other',...
    'other','precip','other',...
    'other','other','other'};
outSpecialIndex = {'precip','radBeam','radDiff'};

%--------------------------------------------------------------------------
%Read in the reanalysis data yearly
%--------------------------------------------------------------------------
%Initialize the counter
[lastDataRow,lastIndexRow,numActiveData,numActiveIndex] = deal(struct('precip',0,'radBeam',0,'radDiff',0,'other',0));

h = waitbar(0,'Importing GLDAS data by year');
for m = 1:numYears
    waitbar(m/numYears,h);
    %Determine the base index for this year
    thisYear = years(m);
    indexTemplate = repmat(modelHours(allYears==thisYear),1,2);
    indexTemplate(:,2) = 0;
    indexTemplate(:,3) = 1;

    %Retrieve the data from the reanalysis archive
    extractSpec.times = [indexTemplate(1,1),indexTemplate(end,1)];
    if m==1
        [outTime,outData,headerRean,precalcExtract] = reanalysis_extract_yearly_files(sources.dir,extractSpec);

        %Create the lookup grid to map the reanalysis grid (regular Lat/Lon)
        %coordinates to the model grid (regular linear units in projection)
        %Read in the Latitude, Longitude, and elevaton grids for the model
        latModel = import_grid(grids.latitude);
        lonModel = import_grid(grids.longitude);
        if isa(grids.elevation,'string') || isa(grids.elevation,'char')
            elevModel = import_grid(grids.elevation);
        else
            elevModel = grids.elevation + zeros(size(latModel)); %assume a number was provided
        end

        %Identify data cells that are all NaNs, these are absent from the
        %Reanalysis. Assume that this is uniform across years.
        testNan = all(isnan(outData{1}),3);
        headerRean.mask(testNan) = false;

        %Run the knnsearch tool to build the lookup grid
        modelXY = [lonModel(:),latModel(:)];
        reanXYMask = [headerRean.gridX(headerRean.mask),headerRean.gridY(headerRean.mask)];
        lookup = knnsearch(reanXYMask,modelXY);
        lookup = uint16(reshape(lookup,size(latModel)));

        %Now, determine the elevation of each reanalysis cell
        %Run knnsearch in reverse
        reanXY = [headerRean.gridX(:),headerRean.gridY(:)];
        lookupReverse = reshape(knnsearch(modelXY,reanXY),size(headerRean.gridX));
        elevRean = elevModel(lookupReverse);

        %Prepare some arrays used below
        location = struct('latitude',headerRean.gridY(headerRean.mask),'longitude',...
            headerRean.gridX(headerRean.mask),'altitude',elevRean(headerRean.mask));

        %Save output header data
        header = headerRean;
        save([workingDir,filesep,sources.fileHeader],'header');
    else
        [outTime,outData] = reanalysis_extract_yearly_files(sources.dir,extractSpec,precalcExtract);
    end

    %Save outputs for debugging
    %     save('temp.mat','outData','headerRean','precalcExtract','location','lookup','outTime','-v7.3');


    %Restructure the data
    extractVars = extractSpec.vars;
    extractNames = extractSpec.outName;
    outStruct = struct();
    for n = 1:length(extractVars)
        shapeData = size(outData{n});
        outStruct.(extractNames{n}) = reshape(permute(outData{n},[3,1,2]),[shapeData(3),shapeData(2)*shapeData(1)]);
        outStruct.(extractNames{n}) = outStruct.(extractNames{n})(:,headerRean.mask(:)); %now mask the data
    end

    %Calculate some quantities from GLDAS data-----------------------------
    %Calculate the sun position at each time for all locations
    [~,zenith,~] = sun_position(outTime,location.latitude,...
        location.longitude,location.altitude); %need time offset here because station data records at end of hour, previous hour's average
    outStruct.sunZenithCos = max(0,cos(unit_conversions(zenith,'deg','rad')));

    %Calculate the diffuse/beam split
    outStruct.diffFrac = diffuse_beam_split(outStruct.radShort,outStruct.sunZenithCos);
    outStruct.radDiff = outStruct.radShort .* outStruct.diffFrac;
    outStruct.radBeam = outStruct.radShort - outStruct.radDiff;

    %Calculate relative humidity from specific humidity
    satVapPress = sat_vap_pressure(outStruct.airTemp,'goff');
    outStruct.relHum = outStruct.specHum .* outStruct.pressure ./ (0.622 * satVapPress) * 100;
    outStruct.relHum(outStruct.relHum>100) = 100;

    %Clear out zeros and build indices-------------------------------------
    %First update the template for the index
    [presentHours,indHours] = ismember_dates(indexTemplate(:,1),outTime,4,false);
    index.other = indexTemplate;
    index.other(presentHours,2) = (indHours(presentHours)) + lastDataRow.other;

    %Now, increment for each present data row
    lastLoopRow = lastDataRow.other;
    for n = 1:length(index.other)
        if index.other(n,2)==0
            index.other(n,2) = lastLoopRow;
        else
            lastLoopRow = index.other(n,2);
        end
    end

    %Update the numActiveData array
    numActiveData.other = size(outStruct.airTemp,1);
    numActiveIndex.other = size(index.other,1);

    for n = 1:length(outSpecialIndex)
        thisDatasetName = outSpecialIndex{n};

        %Determine zeros of precipitation and shortwave radiation
        zerosVals.(thisDatasetName) = all(outStruct.(thisDatasetName)==0 | isnan(outStruct.(thisDatasetName)),2);

        %Create data arrays without those rows
        outStruct.(thisDatasetName) = outStruct.(thisDatasetName)(~zerosVals.(thisDatasetName),:);

        %Make new indices filling in zero grids with -1
        thisNotZeroTime = outTime(~zerosVals.(thisDatasetName));
        thisZeroTime = outTime(zerosVals.(thisDatasetName));
        [notZeroHours,indHoursNotZero] = ismember_dates(indexTemplate(:,1),thisNotZeroTime,4,false);
        [zeroHours] = ismember_dates(indexTemplate(:,1),thisZeroTime,4,false);
        index.(thisDatasetName) = indexTemplate;
        index.(thisDatasetName)(notZeroHours,2) = (indHoursNotZero(notZeroHours)) + lastDataRow.(thisDatasetName);
        index.(thisDatasetName)(zeroHours,2) = -1;

        %Now, increment for each present data row
        lastLoopRow = lastDataRow.(thisDatasetName);
        for o = 1:length(index.(thisDatasetName))
            if index.(thisDatasetName)(o,2)==0
                index.(thisDatasetName)(o,2) = lastLoopRow;
            else
                lastLoopRow = index.(thisDatasetName)(o,2);
            end
        end

        %Get the size of each data array
        numActiveData.(thisDatasetName) = sum(~zerosVals.(thisDatasetName));
        numActiveIndex.(thisDatasetName) = size(index.(thisDatasetName),1);
    end

    %--------------------------------------------------------------------------
    %Write out the datasets
    %--------------------------------------------------------------------------
    for n = 1:length(outDatasets)
        tempGroup = ['/',outDatasetNames{n}];
        attribs = struct('class','');
        if m == 1 %only on the first loop
            %Create the output group
            groupExist = h5groupcreate(outFile,tempGroup);

            if groupExist
                %If it exists, must rename old file or stop, provide option
                %to go forward without checking
                if ~overwrite
                    get_overwrite_pref(outFile,tempGroup);
                end
                %If we're still going, that means the user decided to
                %overwrite
                %rename the current file
                [outFilePath,outFileName,outFileExt] = fileparts(outFile);
                oldFileName = ['old_',outFileName,outFileExt];
                oldFile = [outFilePath,filesep,oldFileName];
                if exist(oldFile,'file')
                    delete(oldFile);
                end
                system(['ren ',outFile,' ',oldFileName]);

                %Create the group again, this will create the new file too
                h5groupcreate(outFile,tempGroup);
            end

            %Write the lookup table
            attribs.class = class(lookup);
            h5datawrite(outFile,[tempGroup,'/lookup'],lookup,attribs,false);
        end

        %Write the output index
        attribs.class = class(index.(outIndices{n}));
        indexShape = size(index.(outIndices{n}));
        h5datawrite(outFile,[tempGroup,'/index'],index.(outIndices{n}),attribs,false,[lastIndexRow.other,0],indexShape,[]);

        %Write the output data
        attribs = sources.(outDatasets{n});
        attribs = rmfield(attribs,'name');
        outShape = size(outStruct.(outDatasets{n}));
        h5datawrite(outFile,[tempGroup,'/data'],outStruct.(outDatasets{n}),attribs,true,[lastDataRow.(outIndices{n}),0],outShape,[]);
    end

    for n = 1:length(outSpecialIndex)
        thisDatasetName = outSpecialIndex{n};
        %Update the last rows counters
        lastDataRow.(thisDatasetName) = lastDataRow.(thisDatasetName) + numActiveData.(thisDatasetName);
        lastIndexRow.(thisDatasetName) = lastIndexRow.(thisDatasetName) + numActiveIndex.(thisDatasetName);
    end
    lastDataRow.other = lastDataRow.other + numActiveData.other;
    lastIndexRow.other = lastIndexRow.other + numActiveIndex.other;
end
if ishandle(h); close(h); end

%Clean up if successful
warning('on','TimeSeriesAggregate:no_aggregation')
% delete('temp.mat');
end
