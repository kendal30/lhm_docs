function subset_clim_file(file1,dateRange,climateName,climateVersion,IDType,compileDate)
%This function takes a temporal subset of a climate file (not a spatial
%subset), and creates a new one
%
% input dateRange is a two-element vector, [start,end] as matlab
% datenumbers
%

%Get a list of groups to subset
datasetInfo = hdf5info(file1);
groupNames = {datasetInfo.GroupHierarchy.Groups(:).Name};
keepGroup = ~strcmpi(groupNames,'/info');
groupNames = groupNames(keepGroup);

%Get the dates from each of file1 and file2
tempData1 = h5datareadlhm(file1,groupNames{1},false);

%Determine the dates from the first dataset to use
indTime1 = bracket_index(tempData1.index(:,1),dateRange(1),dateRange(2));

%Clean up
clear tempData*

%Build the subset file
[workingDir,~,~] = fileparts(file1);
serialID = serial_ID_gen(IDType,climateName,climateVersion,compileDate);
outFilename = [IDType,'_',climateName,'_',climateVersion,'_',datestr(compileDate,'yyyymmdd'),'.h5'];
outFile = [workingDir,filesep,outFilename];

h5groupcreate(outFile,'/info');
attribs = struct('class','char');
h5datawrite(outFile,'/info/climate_name',climateName,attribs);
h5datawrite(outFile,'/info/climate_version',climateVersion,attribs);
h5datawrite(outFile,'/info/compile_date',datestr(compileDate,'yyyy-mm-dd'),attribs);
h5datawrite(outFile,'/info/serial_ID',serialID,attribs);
h5datawrite(outFile,'/info/time_start',datestr(dateRange(1),'yyyy-mm-dd HH:MM:SS'),attribs);
h5datawrite(outFile,'/info/time_end',datestr(dateRange(2),'yyyy-mm-dd HH:MM:SS'),attribs);

%Loop through the indices
for m = 1:length(groupNames)
    %Read in the data for this group
    thisData1 = h5datareadlhm(file1,groupNames{m},false);
        
    %Trim the index, and build the trim dataset
    thisDataTrim = thisData1;
    thisDataTrim.index = thisDataTrim.index(indTime1(1):indTime1(2),:);
    thisDataTrim.events = thisDataTrim.events(indTime1(1):indTime1(2));
    
    %Determine the needed data range and trim the data
    indData1 = [min(thisDataTrim.index(thisDataTrim.events,2)),...
        max(thisDataTrim.index(thisDataTrim.events,2))];
    thisDataTrim.data = thisDataTrim.data(indData1(1):indData1(2),:);
    
    %Determine the needed lookup arrays, build the translation table, and
    %select out those lookup arrays
    indLookup1 = unique(thisDataTrim.index(:,3));
    if length(indLookup1) == 1 
        testSingleLookup = true;  %test for a single lookup array
    end
    if ~testSingleLookup
        transLookup = [indLookup1,(1:length(indLookup1))'];
        thisDataTrim.lookup = thisDataTrim.lookup(:,:,indLookup1);
    end
    
    %Modify the index for the new data and lookup arrays
    thisDataTrim.index(thisDataTrim.events,2) = (1:size(thisDataTrim.data,1));
    
    if ~testSingleLookup
        [~,transLoc] = ismember(thisDataTrim.index(thisDataTrim.events,3),transLookup(:,1));
        thisDataTrim.index(thisDataTrim.events,3) = transLookup(transLookup(transLoc,2));
    end
    
    %Write the output data, index, stationsXY, and lookup
    h5groupcreate(outFile,groupNames{m});
    h5datawrite(outFile,[groupNames{m},'/data'],thisDataTrim.data,thisDataTrim.attribs,false);
    %Write the output index
    attribs = struct('class',class(thisDataTrim.index));
    h5datawrite(outFile,[groupNames{m},'/index'],thisDataTrim.index,attribs,false);
    %Write the output stationsXY, if present
    if ~islogical(thisDataTrim.stationsXY)
        attribs.class = class(thisDataTrim.stationsXY);
        h5datawrite(outFile,[groupNames{m},'/stationsXY'],thisDataTrim.stationsXY,attribs,false);
    end
    %Write the lookup table
    attribs.class = class(thisDataTrim.lookup);
    h5datawrite(outFile,[groupNames{m},'/lookup'],thisDataTrim.lookup,attribs,false);
end

%Clean up
clear thisData*