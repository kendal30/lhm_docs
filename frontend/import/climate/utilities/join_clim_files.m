function join_clim_files(file1,file2,climateName,climateVersion,IDType,compileDate)
%This function joins two CLIM files, with the second one specified
%overwriting any dates in the first, the output will be written to outFile

%Get a list of groups to join
datasetInfo = hdf5info(file1);
groupNames = {datasetInfo.GroupHierarchy.Groups(:).Name};
keepGroup = ~strcmpi(groupNames,'/info');
groupNames = groupNames(keepGroup);
    
%Get the dates from each of file1 and file2
tempData1 = h5datareadlhm(file1,groupNames{1},false);
tempData2 = h5datareadlhm(file2,groupNames{1},false);
dateRange = [tempData1.index(1,1),tempData2.index(end,1)];

%Determine the dates from the first dataset to use
indTime1 = bracket_index(tempData1.index(:,1),tempData1.index(1,1),tempData2.index(1,1),[1,0]); %want all dates less than or equal to start of second dataset

%Clean up
clear tempData*

%Build the joined files
[workingDir,~,~] = fileparts(file1);
serialID = serial_ID_gen(IDType,climateName,climateVersion,compileDate);
outFilename = [IDType,'_',climateName,'_',climateVersion,'_',datestr(compileDate,'yyyymmdd'),'.h5'];
outFile = [workingDir,filesep,outFilename];

h5groupcreate(outFile,'/info');
attribs = struct('class','char');
h5datawrite(outFile,'/info/climate_name',climateName,attribs);
h5datawrite(outFile,'/info/climate_version',climateVersion,attribs);
h5datawrite(outFile,'/info/compile_date',datestr(compileDate,'yyyy-mm-dd'),attribs);
h5datawrite(outFile,'/info/serial_ID',serialID,attribs);
h5datawrite(outFile,'/info/time_start',datestr(dateRange(1),'yyyy-mm-dd HH:MM:SS'),attribs);
h5datawrite(outFile,'/info/time_end',datestr(dateRange(2),'yyyy-mm-dd HH:MM:SS'),attribs);

%Loop through the indices
for m = 1:length(groupNames)
    %Read in the data for this group
    thisData1 = h5datareadlhm(file1,groupNames{m},false);
    thisData2 = h5datareadlhm(file2,groupNames{m},false);
    
    %Trim the index, and build the trim dataset
    thisDataTrim = thisData1;
    thisDataTrim.index = thisDataTrim.index(indTime1(1):indTime1(2),:);
    thisDataTrim.events = thisDataTrim.events(indTime1(1):indTime1(2));
    
    %Determine the needed data range and trim the data
    indData1 = [min(thisDataTrim.index(thisDataTrim.events,2)),...
        max(thisDataTrim.index(thisDataTrim.events,2))];
    thisDataTrim.data = thisDataTrim.data(indData1(1):indData1(2),:);
    
    %Determine the needed lookup arrays, build the translation table, and
    %select out those lookup arrays
    indLookup1 = unique(thisDataTrim.index(:,3));
    if length(indLookup1) == 1 %test for identical lookup arrays
        testSameLookup = all(thisDataTrim.lookup(:) == thisData2.lookup(:));
    end
    if ~testSameLookup
        transLookup = [indLookup1,(1:length(indLookup1))'];
        thisDataTrim.lookup = thisDataTrim.lookup(:,:,indLookup1);
    end
    
    %Modify the index for the new data and lookup arrays
    thisDataTrim.index(thisDataTrim.events,2) = (1:size(thisDataTrim.data,1));
    
    if ~testSameLookup
        [~,transLoc] = ismember(thisDataTrim.index(thisDataTrim.events,3),transLookup(:,1));
        thisDataTrim.index(thisDataTrim.events,3) = transLookup(transLookup(transLoc,2));
    end
    
    %Modify the index in the second dataset to account for the first
    thisData2.index(thisData2.events,2) = thisData2.index(thisData2.events,2) + max(thisDataTrim.index(:,2));
    if ~testSameLookup
        thisData2.index(:,3) = thisData2.index(:,3) + max(thisDataTrim.index(:,3));
    end
    
    %Now, add the second dataset to this array
    thisDataComp = thisDataTrim;
    thisDataComp.index = cat(1,thisDataTrim.index,thisData2.index);
    thisDataComp.data = cat(1,thisDataTrim.data,thisData2.data);
    if ~testSameLookup
        thisDataComp.lookup = cat(3,thisDataTrim.lookup,thisData2.lookup);
    end
    
    %Write the output data, index, stationsXY, and lookup
    h5groupcreate(outFile,groupNames{m});
    h5datawrite(outFile,[groupNames{m},'/data'],thisDataComp.data,thisDataComp.attribs,false);
    %Write the output index
    attribs = struct('class',class(thisDataComp.index));
    h5datawrite(outFile,[groupNames{m},'/index'],thisDataComp.index,attribs,false);
    %Write the output stationsXY, if present
    if ~islogical(thisDataComp.stationsXY)
        attribs.class = class(thisDataComp.stationsXY);
        h5datawrite(outFile,[groupNames{m},'/stationsXY'],thisDataComp.stationsXY,attribs,false);
    end
    %Write the lookup table
    attribs.class = class(thisDataComp.lookup);
    h5datawrite(outFile,[groupNames{m},'/lookup'],thisDataComp.lookup,attribs,false);
end

%Clean up
clear thisData*