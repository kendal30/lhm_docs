function downscaled_data_load(modelHours,workingDir,outFilename,sources,grids)

%Get the indices for each model year
[allYears,~,~,~,~,~] = datevec(modelHours);
years = unique(allYears);
numYears = length(years);

%Specify some output information the datasets
outFile = [workingDir,filesep,outFilename];

%This is some information about the particular downscaled dataset, change if
%using a different input dataset. Conversion format specification
%is [multiplier, add. offset]
varsDownscale = struct();
varsDownscale.airTemp = struct('name','tas','convert',[1,0],'filePrefix','T'); %already in C
varsDownscale.precip = struct('name','pr','convert',[1/1000/24,0],'filePrefix','P'); %mm/d -> m/hr
%Specify the path to the downscaled data and the folder structure
filesDownscale = struct();
filesDownscale.path = [sources.inDir,filesep,sources.scenario];
filesDownscale.extension = 'nc';
filesDownscale.conventionFile = [sources.scenario,'-<yyyy>-<var>']; %options in
%convention are yyyy,mm,dd,HH,MM, var (var indicates the filePrefix for
%each variable, specified in the varsRean above, omit if not using).
%Surround each statement by < >.
%Note, if these aren't all implemented here, see reanalysis_extract for
%other implementations of these

%Load the nldas_header.mat file to match the downscaled data to
load([workingDir,filesep,sources.nldas.fileHeader],'header');

%Run the nctools setup script
setup_nctoolbox;
warning('off','NCGEODATASET:GEOVARIABLE');

%Parse the file naming convention
tokensFile = regexp(filesDownscale.conventionFile,'<(\w+).*?>','tokens');
tokenStruct = struct('var','','yyyy','','mm','','dd','','HH','','MM','');

%For each year
inVars = fieldnames(varsDownscale);
lastRow = struct('index',0);
for m = 1:length(inVars)
    lastRow.(inVars{m}) = 0;
end

h = waitbar(0,'Importing downscaled data by year');
for m=1:numYears
    waitbar(m/numYears,h);

    %Determine the desired times for this year
    thisYear = years(m);

    %Build the index for this year
    index = repmat(modelHours(allYears==thisYear),1,2);
    index(:,2) = 0;
    index(:,3) = 1;

    for n = 1:length(inVars)
        thisVar = inVars{n};

        %Get the name of this dataset
        tokenStruct.yyyy= thisYear;
        tokenStruct.var = varsDownscale.(thisVar).filePrefix;
        thisFile = filesDownscale.conventionFile;
        for o = 1:length(tokensFile)
            thisFile = regexprep(thisFile,['<',tokensFile{o}{1},'>'],num2str(tokenStruct.(tokensFile{o}{1}),...
                ['%0',num2str(length(tokensFile{o}{1})),'.f']));
        end
        thisFile = [thisFile,'.',filesDownscale.extension]; %#ok<AGROW>
        thisFile = [filesDownscale.path,filesep,thisFile]; %#ok<AGROW>

        %Open the file
        try
            thisDataset = ncgeodataset(thisFile);
        catch
            disp(thisFile);
        end

        %Build the lookup table first
        if m == 1 && n ==1
            %Load the latitude and longitude arrays
            varLat = thisDataset.geovariable('latitude');
            varLon = thisDataset.geovariable('longitude');

            %Turn them into gridded values
            [lonDownscale,latDownscale] = meshgrid(varLon.data(:),flipud(varLat.data(:))); %need to flip latitude

            %Change longitude to -180:0 instead of 181:360
            lonDownscale(lonDownscale>180) = lonDownscale(lonDownscale>180) - 360;

            %Create the lookup grid to map the downscaled (regular Lat/Lon)
            %coordinates to the model grid (regular linear units in projection)
            %Read in the Latitude and Longitude grids for the model
            latModel = import_grid(grids.latitude);
            lonModel = import_grid(grids.longitude);

            %Clear out the -Inf values to make a mask
            maskModel = (latModel >= -90) & (latModel <= 90);
            latModel(~maskModel) = NaN;
            lonModel(~maskModel) = NaN;

            %Build the XY arrays we will need for the lookups (a little
            %complex because of the two part nature of matching NLDAS)
            nldasXY = [header.gridX(:),header.gridY(:)];
            downscaleXY = [lonDownscale(:),latDownscale(:)];
            modelXY = [lonModel(:),latModel(:)];

            %First, will need to create a lookup grid to match downscaled
            %data to NLDAS
            lookupNLDAS = knnsearch(downscaleXY,nldasXY);
%             lookupNLDAS = uint16(reshape(lookupNLDAS,size(header.gridX)));

            %We already have an NLDAS mask set, so build the lookup from
            %NLDAS-> model using the mask
            nldasXYmask = [header.gridX(header.mask),header.gridY(header.mask)];
            lookup = knnsearch(nldasXYmask,modelXY);
            lookup = uint16(reshape(lookup,size(latModel)));

            %Don't use the method below here for now, because I am using
%             %the NLDAS mask instead
%             %Run the knnsearch tool to build the lookup grid
%             lookupPrelim = knnsearch(downscaleXY,modelXY);
%             lookupPrelim = uint16(reshape(lookupPrelim,size(latModel)));
%
%             %Build a mask for the downscaled data based on the cells
%             %present in the lookup (we don't need to keep all cells for smaller domain models)
%             maskDownscale = false(size(lonDownscale));
%             presDownscale = unique(lookupPrelim(maskModel));
%             maskDownscale(presDownscale) = true;
%
%             %Run knnsearch again, this time on the masked data
%             downscaleXYMask = [lonDownscale(maskDownscale),latDownscale(maskDownscale)];
%             lookup = knnsearch(downscaleXYMask,modelXY);
%             lookup = uint16(reshape(lookup,size(latModel)));
        end

        %Load the dataset
        varData = thisDataset.geovariable(varsDownscale.(thisVar).name);
        thisData = double(flipdim(squeeze(varData.data(1,:,:,:)),2)); %flip the latitude dimension

        %Convert the data
        thisConversion = varsDownscale.(thisVar).convert;
        thisData = thisData*thisConversion(1) + thisConversion(2);

        %Reshape and mask the data
        sizeData = size(thisData);
        thisData = reshape(thisData,[sizeData(1),sizeData(2) * sizeData(3)]);
        thisData = thisData(:,lookupNLDAS); %first get to NLDAS grid
        thisData = thisData(:,header.mask(:)); %then, mask to NLDAS mask

        %Remove all zero rows for precip
        if strcmpi(thisVar,{'precip'})
            %Correct negative values, assume 0
            testNegative = thisData < 0;
            thisData(testNegative) = 0;

            %Test for all zero rows
            zeroRows = all(thisData<eps | isnan(thisData),2);

            %Remove these rows
            thisData(zeroRows,:) = [];

            %Correct index
            index(zeroRows,2) = -1;
        end

        %Finish the index
        testNotZero = (index(:,2) ~= -1);
        index(testNotZero,2) = (1:sum(testNotZero));

        %Check to make sure that the length of the index matches the data
        %size
        assert(max(index(:,2))==size(thisData,1),'Index is longer than dataset hours, check')

        %Write the output data
        %--------------------------------------------------------------------------
        %Write out the datasets
        %--------------------------------------------------------------------------
        outDatasetName = sources.(thisVar).name;

        tempGroup = ['/',outDatasetName];
        attribs = struct('class','');
        if m == 1 %only on the first loop
            %Create the output group
            groupExist = h5groupcreate(outFile,tempGroup);

            if groupExist
                %If it exists, must rename old file or stop
                get_overwrite_pref(outFile,tempGroup);
                %If we're still going, that means the user decided to
                %overwrite
                %rename the current file
                [outFilePath,outFileName,outFileExt] = fileparts(outFile);
                oldFileName = ['old_',outFileName,outFileExt];
                oldFile = [outFilePath,filesep,oldFileName];
                if exist(oldFile,'file');
                    delete(oldFile);
                end
                system(['ren ',outFile,' ',oldFileName]);

                %Create the group again, this will create the new file too
                h5groupcreate(outFile,tempGroup);
            end

            %Write the lookup table
            attribs.class = class(lookup);
            h5datawrite(outFile,[tempGroup,'/lookup'],lookup,attribs,false);
        end

        %Write the output index
        attribs.class = class(index);
        indexShape = size(index);
        h5datawrite(outFile,[tempGroup,'/index'],index,attribs,false,[lastRow.index,0],indexShape,[]);

        %Write the output data
        attribs = sources.(thisVar);
        attribs = rmfield(attribs,'name');
        outShape = size(thisData);
        h5datawrite(outFile,[tempGroup,'/data'],thisData,attribs,true,[lastRow.(thisVar),0],outShape,[]);

        %Increment the last row counters
        if n == length(inVars)
            lastRow.index = lastRow.index + indexShape(1);
        end
        lastRow.(thisVar) = lastRow.(thisVar) + outShape(1);
    end
end

warning('on','NCGEODATASET:GEOVARIABLE');
end
