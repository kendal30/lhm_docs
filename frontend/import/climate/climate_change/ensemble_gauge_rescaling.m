function ensemble_gauge_rescaling(workingDir,outFilename,sources)
%ENSEMBLE_GAUGE_RESCALING  Using ensemble GCM anomalies, rescales gauge
%precip and temp
%   ensemble_gauge_rescaling()
%
%   Descriptions of Input Variables:
%   none, all are specified below.
%
%   Descriptions of Output Variables:
%   none, all are written to the specified hdf5 file.
%
%   Example(s):
%   >> julian_fill_climate
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-04
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Load the anomaly data and the precipitation and temperature indexes
%--------------------------------------------------------------------------
%Load the anomaly data, variables will be 'anomalies', 'baseVals', and
%'grids'
[testDir,~,~] = fileparts(sources.anomaly);
if ~isempty(testDir)
    load(sources.anomaly);
else
    load([workingDir,filesep,sources.anomaly]);
end

%Load the GCM grid points in model coordinates
gcmGridPts.x = grids.ensemble.meshX(:);
gcmGridPts.y = grids.ensemble.meshY(:);

%Load the precipitation and temperature data
gaugeDataNames = sources.dataTypes;
gcmDataNames = sources.gcmVars;

gaugeData = struct();
%which would occur for distributed inputs
for m = 1:length(gaugeDataNames)
    gaugeData.(gaugeDataNames{m}) = h5datareadlhm([workingDir,filesep,sources.(gaugeDataNames{m}).data],...
        ['/',sources.(gaugeDataNames{m}).name]);
    %If needed, expand data to all model hours with values
    thisData = gaugeData.(gaugeDataNames{m});
    thisData.data = thisData.data(thisData.index(thisData.events,2),:);
    gaugeData.(gaugeDataNames{m}) = thisData;
    
    %Check that there are lat and lon in the dataset or there is a lookup
    %table that matches the dataset grid
    if islogical(sources.(gaugeDataNames{m}).grid)
        gaugeData.(gaugeDataNames{m}).nearestInd = knnsearch([gcmGridPts.x,gcmGridPts.y],...
            gaugeData.(gaugeDataNames{m}).stationsXY(:,2:3));
    else
        %Get the file type
        [~,~,extension] = fileparts(sources.(gaugeDataNames{m}).grid);
        if strcmpi(extension,'.mat')
            loadGeoLoc = load([workingDir,filesep,sources.(gaugeDataNames{m}).grid]);
            tempGeoLoc = loadGeoLoc.header;
            thisGridX = tempGeoLoc.gridX(tempGeoLoc.mask);
            thisGridY = tempGeoLoc.gridY(tempGeoLoc.mask);
        else
            %Bring in the grid and build the x and y arrays
            tempGeoLoc = import_grid(sources.(gaugeDataNames{m}).grid,'header');
            
            thisGridX = tempGeoLoc.left+(tempGeoLoc.cellsizeX/2:tempGeoLoc.cellsizeX:tempGeoLoc.cellsizeX*tempGeoLoc.cols);
            thisGridY = tempGeoLoc.top-(tempGeoLoc.cellsizeY/2:tempGeoLoc.cellsizeY:tempGeoLoc.cellsizeY*tempGeoLoc.rows);
            thisGridX = reshape(repmat(thisGridX,[tempGeoLoc.rows,1]),[],1);
            thisGridY = reshape(repmat(thisGridY',[1,tempGeoLoc.cols]),[],1);
        end
        
        %Find the nearest neighbors
        gaugeData.(gaugeDataNames{m}).nearestInd = knnsearch([gcmGridPts.x,gcmGridPts.y],[thisGridX,thisGridY]);
    end
end

%Squeeze out index values that are -1 or NaN
indices = struct();
for m = 1:length(gaugeDataNames)
    indices.(gaugeDataNames{m}) = gaugeData.(gaugeDataNames{m}).index(gaugeData.(gaugeDataNames{m}).events,:);
end

%--------------------------------------------------------------------------
%Identify appropriate fineGrid cells from GCM ensembles for each gauge
%--------------------------------------------------------------------------
anomaly = struct();
for m = 1:length(gaugeDataNames)
    %Extract the anomaly
    anomaly.(gaugeDataNames{m}) = zeros(12,size(gaugeData.(gaugeDataNames{m}).data,2));
    for n = 1:12
        thisMonthAnomaly = anomalies.(sources.forecast).(gcmDataNames{m}).(sources.scenario)(:,:,n);
        anomaly.(gaugeDataNames{m})(n,:) = thisMonthAnomaly(gaugeData.(gaugeDataNames{m}).nearestInd);
    end
end

%--------------------------------------------------------------------------
%Rescale data according to appropriate anomaly
%--------------------------------------------------------------------------
rescale = struct();
for m = 1:length(gaugeDataNames)
    %Get the month of the gaugeData
    [~,month,~,~,~,~] = datevec(indices.(gaugeDataNames{m})(:,1));
    
    %Rescale the gauges monthly    
    rescale.(gaugeDataNames{m}) = zeros(size(gaugeData.(gaugeDataNames{m}).data));
    for n = 1:12
        thisAnomaly = anomaly.(gaugeDataNames{m})(n,:);
        thisMonthData = (month == n);
        if any(strcmpi(gcmDataNames{m},{'pr','rsds'}))
            rescale.(gaugeDataNames{m})(thisMonthData,:) = gaugeData.(gaugeDataNames{m}).data(thisMonthData,:) .* ...
                repmat(thisAnomaly,sum(thisMonthData),1);
        else
            rescale.(gaugeDataNames{m})(thisMonthData,:) = gaugeData.(gaugeDataNames{m}).data(thisMonthData,:) + ...
                repmat(thisAnomaly,sum(thisMonthData),1);
        end
    end
end

%--------------------------------------------------------------------------
%Write the output datasets
%--------------------------------------------------------------------------
outFile = [workingDir,filesep,outFilename];
for m = 1:length(gaugeDataNames)
    import_write_dataset(outFile,['/',sources.(gaugeDataNames{m}).name],...
        rescale.(gaugeDataNames{m}),gaugeData.(gaugeDataNames{m}).attribs,...
        gaugeData.(gaugeDataNames{m}).index,gaugeData.(gaugeDataNames{m}).stationsXY,...
        gaugeData.(gaugeDataNames{m}).lookup);
end
end