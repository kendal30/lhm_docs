function ncdc_snow_partition(modelHours,workingDir,outFilename,sources)
%NCDC_SNOW_PARTITION  Partitions NCDC data into rain and snow.
%   ncdc_snow_partition
%
%   Descriptions of Input Variables:
%   none, all inputs are specified within the function, by editing the
%   code.
%
%   Descriptions of Output Variables:
%   none, all outputs are written to an intermediate hdf5 file.
%
%   Example(s):
%   >> ncdc_snow_partition
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-27
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Load data
%--------------------------------------------------------------------------
%Load the precip data
tempFile = [workingDir,filesep,sources.precip.data];
tempGroup = ['/',sources.precip.name];
[precipData,precipAttribs] = h5dataread(tempFile,[tempGroup,'/data'],sources.method.rescale);
if ~sources.method.rescale
    classFunc = str2func(class(precipData));
end
precipIndex = h5dataread(tempFile,[tempGroup,'/index'],true);
precipXY = h5dataread(tempFile,[tempGroup,'/stationsXY'],true);
precipLookup = h5dataread(tempFile,[tempGroup,'/lookup'],false);

%Load the air temperature data
tempFile = [workingDir,filesep,sources.airTemp.data];
tempGroup = ['/',sources.airTemp.name];
[airTempData] = h5dataread(tempFile,[tempGroup,'/data'],true);
airTempIndex = h5dataread(tempFile,[tempGroup,'/index'],true);
airTempLookup = h5dataread(tempFile,[tempGroup,'/lookup'],false);

%--------------------------------------------------------------------------
%If precip data not spatially explicit, extract airTemp grid indeces to use
%for stations
%--------------------------------------------------------------------------
if ~islogical(precipXY)
    %Get the model grid header
    [modelHeader] = import_grid(sources.method.modelGrid,'header');

    %Convert stationsXY list to model i,j
    stationIJ = floor([(precipXY(:,2) - modelHeader.left) ./  modelHeader.cellsizeX + 1,...
        (modelHeader.top - precipXY(:,3)) ./ modelHeader.cellsizeY + 1]);

    %Convert IJ subscript to indeces
    stationInd = sub2ind([modelHeader.rows,modelHeader.cols],stationIJ(:,1),stationIJ(:,2));
else
    %Determine how many model cells contain values from each precip cell
    uniquePrecipCells = count_unique(precipLookup);
end

%--------------------------------------------------------------------------
%Create the rain and snow indexes, determine which model hours to run,
%create data arrays
%--------------------------------------------------------------------------
[rainIndex,snowIndex] = deal(precipIndex);

%Determine which hours have precipitation
precipHours = precipIndex(precipIndex(:,2)>0,1);
precipInd = ismember_dates(modelHours,precipHours,4,true);

%Determine which hours have temperatures less than the frozenThreshold
frozenHours = airTempIndex(any(airTempData < sources.method.frozenThreshold,2),1);
frozenInd = ismember_dates(modelHours,frozenHours,4,true);

%Determine the overlap
snowInd = precipInd & frozenInd;
numSnow = sum(snowInd);

%Fill the snowIndex with 0 for nonFrozen hours, re-fill data rows column
snowIndex(~snowInd & precipInd,2) = -1;
snowIndex(snowInd,2) = (1:numSnow);

%Create data arrays
rainData = precipData;
snowData = zeros(numSnow,size(precipData,2),class(precipData)) + NaN;

%--------------------------------------------------------------------------
%Loop through the snowInd, calculate rain/snow partition
%--------------------------------------------------------------------------
%Get the indexes
airTempLoopInd = airTempIndex(ismember_dates(airTempIndex(:,1),modelHours(snowInd),4,true),2);
airTempLookupInd = airTempIndex(ismember_dates(airTempIndex(:,1),modelHours(snowInd),4,true),3);
rainLoopInd = rainIndex(ismember_dates(rainIndex(:,1),modelHours(snowInd),4,true),2);
precipLoopInd = rainLoopInd;
precipLookupInd = rainIndex(ismember_dates(rainIndex(:,1),modelHours(snowInd),4,true),3);
snowLoopInd = snowIndex(ismember_dates(snowIndex(:,1),modelHours(snowInd),4,true),2);

h = waitbar(0,'Calculating rain/snow partitioning');
for m = 1:numSnow
    waitbar(m/numSnow,h);

    %Convert this hour of air temperature data to a grid
    thisAirTemp = airTempData(airTempLoopInd(m),:);
    thisAirTempLookup = airTempLookup(:,:,airTempLookupInd(m));
    if m > 1
        if precipLookupInd(m) ~= precipLookupInd(m-1)
            thisPrecipLookup = precipLookup(:,:,precipLookupInd(m));
            uniquePrecipCells = count_unique(thisPrecipLookup);
        end
    else
        thisPrecipLookup = precipLookup(:,:,precipLookupInd(m));
        uniquePrecipCells = count_unique(thisPrecipLookup);
    end

    %Run rain/snow partition
    thisPrecip = precipData(precipLoopInd(m),:);
    if ~sources.method.rescale
        thisPrecip = single(thisPrecip);
        thisPrecip(thisPrecip==str2double(precipAttribs.nan)) = NaN;
    end
    if ~islogical(precipLookup)
        %Find where each cell falls within the airTemp lookup, each cell might
        %have multiple airTemp stations, so use the majority
        %Upscale This
        stationInd = accumarray(thisPrecipLookup(:),thisAirTempLookup(:),[],@min);
        stationInd(stationInd==0) = [];
    end
    [thisRain,thisSnow] = rain_snow_partition(thisPrecip(uniquePrecipCells),thisAirTemp(stationInd));

    if ~sources.method.rescale
        %Convert NaNs back to values
        thisRain(isnan(thisRain)) = str2double(precipAttribs.nan);
        thisSnow(isnan(thisSnow)) = str2double(precipAttribs.nan);

        %Convert class
        thisRain = classFunc(thisRain);
        thisSnow = classFunc(thisSnow);
    end

    %Store in the data arrays
    rainData(rainLoopInd(m),uniquePrecipCells') = thisRain;
    snowData(snowLoopInd(m),uniquePrecipCells') = thisSnow;
end
if ishandle(h); close(h); end

%--------------------------------------------------------------------------
%Write out the datasets
%--------------------------------------------------------------------------
%Write the datasets
tempFile = [workingDir,filesep,outFilename];
import_write_dataset(tempFile,['/',sources.rain.name],rainData,precipAttribs,rainIndex,precipXY,precipLookup,sources.method.rescale);
import_write_dataset(tempFile,['/',sources.snow.name],snowData,precipAttribs,snowIndex,precipXY,precipLookup,sources.method.rescale);
end
