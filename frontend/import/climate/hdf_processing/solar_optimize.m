function solar_optimize(modelHours,workingDir,sources)

if sources.model
    skyCovData = h5dataread([workingDir,filesep,sources.skyCov.data],['/',sources.skyCov.name,'/data']);
    skyCovIndex = h5dataread([workingDir,filesep,sources.skyCov.data],['/',sources.skyCov.name,'/index']);
    stations = h5dataread([workingDir,filesep,sources.skyCov.data],['/',sources.skyCov.name,'/stationsXY']);
    pressureData = h5dataread([workingDir,filesep,sources.pressure.data],['/',sources.pressure.name,'/data']);
    pressureIndex = h5dataread([workingDir,filesep,sources.pressure.data],['/',sources.pressure.name,'/index']);
    irradiance = 1366; %W/m^2, average annual across model period, eventually replace with data.
else
    stations = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/stationsXY']);
end

airTempData = h5dataread([workingDir,filesep,sources.airTemp.data],['/',sources.airTemp.name,'/data']);
airTempIndex = h5dataread([workingDir,filesep,sources.airTemp.data],['/',sources.airTemp.name,'/index']);
relHumData = h5dataread([workingDir,filesep,sources.relHum.data],['/',sources.relHum.name,'/data']);
relHumIndex = h5dataread([workingDir,filesep,sources.relHum.data],['/',sources.relHum.name,'/index']);
[mawnData,solarAttribs] = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/data']);
mawnIndex = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/index']);
mawnXY = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/stationsXY']);

%--------------------------------------------------------------------------
%Calculate a few needed quantities
%--------------------------------------------------------------------------
numStations = size(stations,1);
numTimes = size(modelHours,1);
dayOfYear = date2doy(datenum(modelHours));

%--------------------------------------------------------------------------
%Regularize the input data, clean NaNs, fill gaps
%--------------------------------------------------------------------------
%Regularize all input data (same size array, filled caps, removed NaNs
if sources.model
    pressure = regularize_data(pressureData,pressureIndex,modelHours,dayOfYear,numStations,true,true);
    skyCov = regularize_data(skyCovData,skyCovIndex,modelHours,dayOfYear,numStations,false,false);
end
airTemp = regularize_data(airTempData,airTempIndex,modelHours,dayOfYear,numStations,true,true);
relHum = regularize_data(relHumData,relHumIndex,modelHours,dayOfYear,numStations,true,true);

%Clean a few items from MAWN data
mawnData(mawnData<0) = NaN;

%--------------------------------------------------------------------------
%Calculate sun position, clear fraction, cloud transmissivity, real ground
%radiation, and longwave radiation
%--------------------------------------------------------------------------
%Calculate the sun position at each time for all locations
location = struct('latitude',stations(:,4)','longitude',stations(:,5)','altitude',stations(:,6)');
[azimuth,zenith,distance] = sun_position(modelHours,location.latitude,location.longitude,location.altitude);
zenithCos = cos(unit_conversions(zenith,'deg','rad'));

%For simplicity, send identically-dimensioned structures into
%real_ground_radiation
location.latitude = repmat(location.latitude,numTimes,1);
location.longitude = repmat(location.longitude,numTimes,1);
location.altitude = repmat(location.altitude,numTimes,1);
climate = struct('relHum',relHum,'airTemp',airTemp,'pressure',pressure);
sun = struct('azimuth',azimuth,'zenithCos',zenithCos,'distance',distance,'time',repmat(modelHours,1,numStations));

%Calculate hourly exoatmospheric irradiance from either constant or time-series values
hourlyIrr = hourly_irradiance(sun,irradiance,'constant');

%Calculate hourly clear-sky transmissivity
[tauBClear,tauDClear] = yang_koike_clear_sky(sun,climate,location);


mawnHours = mawnIndex(mawnIndex(:,2)>0,1);
%Ignore incomplete months
maskFunc = @(x)sum(isnan(x),1)<=4*24;
[mawnMonths,mawnMonthly,mawnMask] = time_series_aggregate(mawnHours,mawnData,'months',{'mean',maskFunc});
mawnMonthly(~mawnMask) = NaN;

%Don't want the first partial month from MAWN
mawnMonths = mawnMonths(2:end);
mawnMonthly = mawnMonthly(2:end,:);


objInput = struct('skyCov',skyCov,'modelHours',modelHours,'airTemp',airTemp,...
    'mawnMonths',mawnMonths,'mawnMonthly',mawnMonthly,'hourlyIrr',hourlyIrr,...
    'tauBClear',tauBClear,'tauDClear',tauDClear,'mawnXY',mawnXY,'stations',stations);

%Now, optimize this
objective_function('initialize',@optimizer,objInput,1,'MAE');
fminsearch(@objective_function,[0.036,0.154,2.4]);

end

function meanVal = optimizer(input,params)

b1 = params(1);
b2 = params(2);
c = params(3);

skyCov = input.skyCov;
modelHours = input.modelHours;
airTemp = input.airTemp;
mawnMonths = input.mawnMonths;
mawnMonthly = input.mawnMonthly;
hourlyIrr = input.hourlyIrr;
tauBClear = input.tauBClear;
tauDClear = input.tauDClear;
mawnXY = input.mawnXY;
stations = input.stations;

%Parse the sky conditions observations into a clear fraction
clearFrac = conditions_parse(skyCov);

%Now, calculate the daily cloud transmissivty using a bristow-campbell
%model
[modelDays,tauCDaily] = bristow_campbell(modelHours,airTemp,[b1,b2,c]);

%Calculate daily clearFrac from daily transmissivity
[clearFracModel] = angstrom_solar(tauCDaily,'daily','inverse');

%Normalize hourly clear fractions
clearFracOrig = clearFrac;
test = true;
while any(test)
    [clearFracDays,clearFracDaily] = time_series_aggregate(modelHours,clearFrac,'days','mean');
    normFac = clearFracModel ./ clearFracDaily;
    if ~any(normFac > 1.05)
        break
    end
    normFac = reshape(permute(repmat(normFac,[1,1,24]),[3,1,2]),size(clearFrac));
    
    %Can only increase those with values less than 1 already
    avail = (clearFrac < 1);
    clearFrac(avail) = clearFrac(avail) .* normFac(avail);
    
    %Limit to 1
    test = (clearFrac > 1);
    clearFrac(test) = 1;
end

%Calculate hourly cloud transmissivity
[tauCHourly] = angstrom_solar(clearFrac,'hourly','forward');

%Normalize hourly transmissivities
tauCHourlyOrig = tauCHourly;
test = true;
while any(test)
    [tauCDays,tauCDailyModel] = time_series_aggregate(modelHours,tauCHourly,'days','mean');
    normFac = tauCDaily ./ tauCDailyModel;
    if ~any(normFac > 1.05)
        break
    end
    normFac = reshape(permute(repmat(normFac,[1,1,24]),[3,1,2]),size(tauCHourly));
    
    %Can only increase those with values less than 1 already
    avail = (tauCHourly < 1);
    tauCHourly(avail) = tauCHourly(avail) .* normFac(avail);
    
    %Limit to 1
    test = (tauCHourly > 1);
    tauCHourly(test) = 1;
end

%Calculate beam and diffuse radiation components, in J/m^2/s
radBeam = hourlyIrr .* tauBClear .* tauCHourly;
radDiff = hourlyIrr .* tauDClear .* tauCHourly;
solar = radBeam + radDiff;

%Determine the match between the two sets
[solarMonths,solarMonthly] = time_series_aggregate(modelHours,solar,'months','mean');
mawnIndMonths = ismember_dates(solarMonths,mawnMonths,2,true);

%Find which solar radiation station is closest to each of the MAWN
%stations
solarMonthlyClose = zeros(size(solarMonthly,1),size(mawnMonthly,2));
for m = 1:size(mawnXY,1)
    thisDist = sqrt((stations(:,2)-mawnXY(m,2)).^2+(stations(:,3)-mawnXY(m,3)).^2);
    thisClose = find(thisDist==min(thisDist));
    solarMonthlyClose(:,m) = solarMonthly(:,thisClose); %#ok<FNDSB>
end

%Calculate the daily ratio of solar/mawn
calcFracMonthly = solarMonthlyClose(mawnIndMonths,:) ./ mawnMonthly;

%Calculate the mean across all stations
calcFracMonthly = nanmean(calcFracMonthly,2);

%Then, for NaN months, use the cross-year monthly average
%Get the monthly average value
[month,monthlyFrac] = time_series_aggregate(solarMonths(mawnIndMonths),calcFracMonthly,'months','mean','years','mean'); %#ok<ASGLU>

%Objective function
meanVal = mean(monthlyFrac);

end

function [clearFrac] = conditions_parse(skyConditions)
%The values of skyConditions are set in AWOS_load
table = [0.9,0.1,0.5,0.25,0.75,2.5/8,0.5]; %CLR, OVC, POB, OBS, BKN, SCT, NULL
%For now, set 'NULL' to 0.5, change to actual statistical mean value
testNan = isnan(skyConditions);
skyConditions(testNan) = 7;
%leave NaNs in place, fill like other datasets
clearFrac = table(skyConditions);
clearFrac(testNan) = NaN;
end

function [data] = clean_nans_mean(data,matchStationAvg)
numStations = size(data,2);
[tempAvg] = nanmean(data,2);
%Now, fill NaN values with the average
tempNan = isnan(data);
testNotAllNan = ~all(tempNan,2);
for m = 1:numStations
    tempInd = tempNan(:,m) & testNotAllNan;
    if matchStationAvg %this serves to guaurantee the the long-term average 
        %at a station is maintained even with filled data
        stationAvg = mean(data(~tempNan(:,m),m));
    end
    data(tempInd,m) = tempAvg(tempInd);
    if matchStationAvg
        newAvg = mean(data(tempInd,m));
        if ~isnan(stationAvg)
            data(tempInd,m) = data(tempInd,m) / newAvg * stationAvg;
        end
    end
end
end

function [data] = fill_gaps(modelHours,dayOfYear,data,period)
numStations = size(data,2);
notNan = ~any(isnan(data),2);
testNan = ~notNan;
[dataDoy,dataAvg] = time_series_aggregate(modelHours(notNan),mean(data(notNan),2),period,'mean','years','mean');
%Round both dayOfYear and dataDoy in order to better match values
dayOfYear = floor(dayOfYear) + round(rem(dayOfYear,1)*100)/100;
dataDoy = floor(dataDoy) + round(rem(dataDoy,1)*100)/100;
%Convert dayOfYear into indices of the julian-day averages
[test,doyLoc] = ismember(dayOfYear,dataDoy); %#ok<ASGLU>
data(testNan & test,:) = repmat(dataAvg(doyLoc(testNan & test)),1,numStations);
%Check to see if any NaNs remain
testNan = any(isnan(data),2);
notNan = ~testNan;
%Linearly interpolate the data
data = interp1(modelHours(notNan),data(notNan,:),modelHours);
%If any other NaNs remain, fill with closest previous value
testNan = isnan(data);
if any(testNan(:))
    findNan = find(testNan);
    for n = 1:length(findNan)
        data(findNan(n)) = data(findNan(n) - 1);
    end
end
end

function [data] = regularize_data(dataIn,index,modelHours,dayOfYear,numStations,cleanFlag,fillFlag)
%Initialize the output data
data = zeros(size(modelHours,1),numStations) + NaN;
%Determine which dates are present in the input dataset
tempInd = ismember_dates(modelHours,index(~isnan(index(:,2)),1),4,true);
%Place those data in to the regularized array
data(tempInd,1:size(dataIn,2)) = dataIn;
if cleanFlag
    %Clean NaNs if some stations are NaN, but not all
    data = clean_nans_mean(data,false);
end
if fillFlag
    %Fill gaps in the data where all stations are nan
    data = fill_gaps(modelHours,dayOfYear,data,'hours');
end
end