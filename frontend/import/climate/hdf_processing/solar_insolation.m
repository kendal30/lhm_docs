function solar_insolation(modelHours,workingDir,outFilename,sources)
%SOLAR_INSOLATION  Calculates real-ground diffuse and beam radiation.
%   solar_insolation()
%
%   Descriptions of Input Variables:
%   none, all come from specified input intermediate hdf5 files
%
%   Descriptions of Output Variables:
%   none, all are saved to specified output intermediate hdf5 files
%
%   Example(s):
%   none
%
%   See also: real_ground_radiation sun_position

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Load the input data and indices
%--------------------------------------------------------------------------
%If the complete model is to be run, i.e. generating shortwave radiation data
%from scratch, then load sky cover
if sources.model
    skyCovData = h5dataread([workingDir,filesep,sources.skyCov.data],['/',sources.skyCov.name,'/data']);
    skyCovIndex = h5dataread([workingDir,filesep,sources.skyCov.data],['/',sources.skyCov.name,'/index']);
    stations = h5dataread([workingDir,filesep,sources.skyCov.data],['/',sources.skyCov.name,'/stationsXY']);
else
    stations = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/stationsXY']);
end

%Air Temperature Data Load
airTempData = h5dataread([workingDir,filesep,sources.airTemp.data],['/',sources.airTemp.name,'/data']);
airTempIndex = h5dataread([workingDir,filesep,sources.airTemp.data],['/',sources.airTemp.name,'/index']);

%Relative Humidity Data Load
relHumData = h5dataread([workingDir,filesep,sources.relHum.data],['/',sources.relHum.name,'/data']);
relHumIndex = h5dataread([workingDir,filesep,sources.relHum.data],['/',sources.relHum.name,'/index']);

%Solar Radiation Data Load
[solarData,solarDataAttribs] = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/data']);
solarDataIndex = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/index']);
solarDataXY = h5dataread([workingDir,filesep,sources.radShort.data],['/',sources.radShort.name,'/stationsXY']);
irradiance = 1366; %W/m^2, average annual across model period, eventually replace with data.

%Barometric Pressure Data Load, or generate
if isfield(sources,'pressure');
    pressureData = h5dataread([workingDir,filesep,sources.pressure.data],['/',sources.pressure.name,'/data']);
    pressureIndex = h5dataread([workingDir,filesep,sources.pressure.data],['/',sources.pressure.name,'/index']);
else
    pressureData = barometric_formula(airTempData,repmat(stations(:,6)',size(airTempData,1),1));
    pressureIndex = airTempIndex;
end

%--------------------------------------------------------------------------
%Calculate a few needed quantities
%--------------------------------------------------------------------------
numStations = size(stations,1);
numTimes = size(modelHours,1);
dayOfYear = date2doy(datenum(modelHours));

%--------------------------------------------------------------------------
%Regularize the input data, clean NaNs, fill gaps
%--------------------------------------------------------------------------
%Regularize all input data (same size array, filled caps, removed NaNs
if sources.model
    skyCov = regularize_data(skyCovData,skyCovIndex,modelHours,dayOfYear,numStations,false,false);
else
    solarRegular = regularize_data(solarData,solarDataIndex,modelHours,dayOfYear,numStations,false,false);
end
airTemp = regularize_data(airTempData,airTempIndex,modelHours,dayOfYear,numStations,true,true);
relHum = regularize_data(relHumData,relHumIndex,modelHours,dayOfYear,numStations,true,true);
pressure = regularize_data(pressureData,pressureIndex,modelHours,dayOfYear,numStations,true,true);

%Clean a few items from solar radiation data
solarData(solarData<0) = NaN;

%--------------------------------------------------------------------------
%Calculate sun position, clear fraction, cloud transmissivity, real ground
%radiation, and longwave radiation
%This is done even for measured solar radiation because I need diffuse and
%beam components split
%--------------------------------------------------------------------------
%Calculate the sun position at each time for all locations
location = struct('latitude',stations(:,4)','longitude',stations(:,5)','altitude',stations(:,6)');
[azimuth,zenith,distance] = sun_position(modelHours-0.5/24,location.latitude,...
    location.longitude,location.altitude); %need time offset here because station data records at end of hour, previous hour's average
zenithCos = max(0,cos(unit_conversions(zenith,'deg','rad')));

%Create input structures
location.latitude = repmat(location.latitude,numTimes,1);
location.longitude = repmat(location.longitude,numTimes,1);
location.altitude = repmat(location.altitude,numTimes,1);
climate = struct('relHum',relHum,'airTemp',airTemp,'pressure',pressure);
sun = struct('azimuth',azimuth,'zenith',zenith,'zenithCos',zenithCos,'distance',distance,'time',...
    repmat(modelHours,1,numStations)-0.5/24);

%Calculate hourly exoatmospheric irradiance from either constant or time-series values
hourlyIrr = hourly_irradiance(sun,irradiance,'constant');

%Calculate hourly clear-sky transmissivity
[tauBClear,tauDClear] = yang_koike_clear_sky(sun,climate,location);


if sources.model %If real-ground radiation should be modeled
    %Parse the sky conditions observations into a clear fraction
    clearFrac = conditions_parse(skyCov);
else %Use the difference between clear-sky modeled radiation and observed as a first estimate
    simDataDiff = hourlyIrr .* (tauBClear + tauDClear) - solarRegular;
    simDataDiff(simDataDiff<0) = 0;
    simFrac = simDataDiff ./ solarRegular;
    simFrac(simFrac > 1) = 1;
    clearFrac = 1 - simFrac;
    clearFrac(isnan(clearFrac)) = 1;    
end

%Now, calculate the daily cloud transmissivty using a bristow-campbell
%model
[~,tauCDaily] = bristow_campbell(modelHours,airTemp,[0.0364,0.164,2.2136]);

%Calculate daily clearFrac from daily transmissivity
[clearFracModel] = angstrom_solar(tauCDaily,'daily','inverse');

%Normalize hourly clear fractions
test = true;
while any(test)
    [~,clearFracDaily] = time_series_aggregate(modelHours,clearFrac,'days','mean');
    normFac = clearFracModel ./ clearFracDaily;
    if ~any(normFac > 1.05)
        break
    end
    normFac = reshape(permute(repmat(normFac,[1,1,24]),[3,1,2]),size(clearFrac));
    
    %Can only increase those with values less than 1 already
    avail = (clearFrac < 1);
    clearFrac(avail) = clearFrac(avail) .* normFac(avail);
    
    %Limit to 1
    test = (clearFrac > 1);
    clearFrac(test) = 1;
end

%Calculate hourly cloud transmissivity
[tauCHourly] = angstrom_solar(clearFrac,'hourly','forward');

%Normalize hourly transmissivities
test = true;
while any(test)
    [~,tauCDailyModel] = time_series_aggregate(modelHours,tauCHourly,'days','mean');
    normFac = tauCDaily ./ tauCDailyModel;
    if ~any(normFac > 1.05)
        break
    end
    normFac = reshape(permute(repmat(normFac,[1,1,24]),[3,1,2]),size(tauCHourly));
    
    %Can only increase those with values less than 1 already
    avail = (tauCHourly < 1);
    tauCHourly(avail) = tauCHourly(avail) .* normFac(avail);
    
    %Limit to 1
    test = (tauCHourly > 1);
    tauCHourly(test) = 1;
end

%Calculate beam and diffuse radiation components, in J/m^2/s
%First, calculate the difference between cloud transmissivity and clear
%fraction, this is the fraction of light that becomes diffuse radiation
%via transmission through clouds or effectively random scattering from
%them
cloudBodyTrans = tauCHourly - clearFrac;
cloudBodyTrans(cloudBodyTrans<0) = 0;

%Calculate transmitted and reflected beam radiation
radBeamIn = hourlyIrr .* tauBClear;
radBeam = radBeamIn .* clearFrac;
radBeamReflect = radBeam - radBeamIn;

%Some of this is scattered and becomes diffuse light, adding to the
%apparent incident radiation at top-of-cloud, add in the reflected
%diffuse radiation (only twice)
radDiffIn = (hourlyIrr + radBeamReflect) .* tauDClear;
radDiffReflect1 = radDiffIn .* (1-clearFrac);
radDiffAdd1 = radDiffReflect1 .* tauDClear;
radDiffReflect2 = radDiffAdd1 .* (1-clearFrac);
radDiffAdd2 = radDiffReflect2 .* tauDClear;
radDiffIn = radDiffIn + radDiffAdd1 + radDiffAdd2;

%Now, the cloud reflects part of this,
%but also there is a certain amount of transmitted beam and diffuse radiation that
%becomes diffuse on its path through the cloud
radDiff = radDiffIn .* clearFrac + (radBeamIn + radDiffIn) .* cloudBodyTrans;

%Total solar
solar = radBeam + radDiff;
    
%Calculate longwave radiation
[longwave] = longwave_calc(clearFrac,airTemp,relHum);

%--------------------------------------------------------------------------
%Compare calculated radiation to measured, and apply scale factor
%--------------------------------------------------------------------------
solarHours = solarDataIndex(solarDataIndex(:,2)>0,1);
solarDataIndAll = ismember_dates(modelHours,solarHours,4,true);
%Calculate daily radiation from both solar radiation data and the models
if sources.model
    %Find which solar radiation station is closest to each of the solarData
    %stations
    closeStation = zeros(size(solarDataXY,1),1);
    for m = 1:size(solarDataXY,1)
        thisDist = sqrt((stations(:,2)-solarDataXY(m,2)).^2+(stations(:,3)-solarDataXY(m,3)).^2);
        closeStation(m) = find(thisDist==min(thisDist));
    end
       
    %First, determine the julian hour values
    [~,solarJHourly] = time_series_aggregate(modelHours,solar,'hours','mean','years','mean');
    [~,solarDataJHourly] = time_series_aggregate(solarHours,solarData,'hours','mean','years','mean');
    
    %Now, reshape into arrays for hourly data correction
    solarJHourlyReshape = reshape(solarJHourly,[24,366,size(solarJHourly,2)]);
    solarDataJHourlyReshape = reshape(solarDataJHourly,[24,366,size(solarDataJHourly,2)]);
    
    solarJHourlyReshape = permute(solarJHourlyReshape,[2,1,3]);
    solarDataJHourlyReshape = permute(solarDataJHourlyReshape,[2,1,3]);
    
    %Now, smooth these values monthly
    fakeDate = datenum('1/1/2008') + (0:365)'; %this is a leap year
    solarJMonthly = zeros(12,24,size(solarJHourly,2));
    for n = 1:size(solarJHourly,2)
            [~,solarJMonthly(:,:,n)] = time_series_aggregate(fakeDate,solarJHourlyReshape(:,:,n),'months','mean');
    end
    solarDataJMonthly = zeros(12,24,size(solarDataJHourly,2));
    for n = 1:size(solarDataJHourly,2)
            [~,solarDataJMonthly(:,:,n)] = time_series_aggregate(fakeDate,solarDataJHourlyReshape(:,:,n),'months','mean');
    end

    %Now, calculate the differences between nearby solarData and awos stations,
    %then average, result is hourly differences monthly
    diffMonthly = zeros(12,24,length(closeStation));
    for m = 1:length(closeStation)
        thisDiff = solarDataJMonthly(:,:,m) ./ solarJMonthly(:,:,closeStation(m));
        thisDiff(solarJMonthly(:,:,closeStation(m))==0)=1;
        diffMonthly(:,:,m) = thisDiff;
    end
    diffMonthly(diffMonthly>2) = 2;
    diffMonthly(diffMonthly<0.5) = 0.5;
  
    diffMonthly = mean(diffMonthly,3);
    
    %Now, apply these corrections to all solar stations
    [~,m,~,H,~,~] = datevec(modelHours); 
    ind = sub2ind(size(diffMonthly),m,H+1);
    correctFactor = repmat(diffMonthly(ind),1,size(solar,2));
    solarCorrect = solar .* correctFactor;
    
    %Constrain the maximum value
    [~,solarDataMonthly] = time_series_aggregate(solarHours,nanmean(solarData,2),'months','max','years','max');
    ind = repmat(sub2ind(size(solarDataMonthly),m),1,size(solar,2));
    test = solarCorrect > solarDataMonthly(ind);
    solarCorrect(test) = solarDataMonthly(ind(test));
    
    %Rescale the calculated solar radiation to this mean
    diffFrac = radDiff ./ solar;
    diffFrac(solar==0) = 1;
    radDiff = solarCorrect .* diffFrac;
    radBeam = solarCorrect .* (1 - diffFrac);   
end

%Determine beam/diffuse split for measured data using fraction from modeled
if sources.model   
    %Fill gaps in the diffuse fraction dataset
    diffFrac = fill_gaps(modelHours,dayOfYear,nanmean(diffFrac,2),'hours');
    
    %Apply this to the solarData data
    solarDataDiff = solarData .* repmat(diffFrac(solarDataIndAll),1,size(solarData,2));
else
    %Calculate the modeled diffuse fraction
    diffFrac = radDiff ./ solar;
    diffFrac(isnan(diffFrac)) = 1;
        
    %Calculate the diffuse fraction
    solarDataDiff = solarData .* diffFrac(solarDataIndAll,:);
end
%Calculate the beam component as the remainder
solarDataBeam = solarData - solarDataDiff;
    
%--------------------------------------------------------------------------
%Populate indices and assign attribs
%--------------------------------------------------------------------------
[beamIndex,diffIndex,longIndex,zenithIndex] = deal([modelHours,(1:size(radBeam,1))']);

%Check for NaN rows and remove from indeces
if sources.model
    keepRows = any(~isnan(radBeam),2);
    radBeam = radBeam(keepRows,:);
    beamIndex(~keepRows,2) = NaN;
    beamIndex(keepRows,2) = (1:sum(keepRows));
    
    keepRows = any(~isnan(radDiff),2);
    radDiff = radDiff(keepRows,:);
    diffIndex(~keepRows,2) = NaN;
    diffIndex(keepRows,2) = (1:sum(keepRows));
end

keepRows = any(~isnan(longwave),2);
longwave = longwave(keepRows,:);
longIndex(~keepRows,2) = NaN;
longIndex(keepRows,2) = (1:sum(keepRows));

keepRows = any(~isnan(zenithCos),2);
zenithCos = zenith(keepRows,:);
zenithIndex(~keepRows,2) = NaN;
zenithIndex(keepRows,2) = (1:sum(keepRows));

%--------------------------------------------------------------------------
%Check the datasets for NaN rows
%--------------------------------------------------------------------------
assert(~any(all(isnan(solarDataBeam),2)),'solarDataBeam contains NaN rows');
assert(~any(all(isnan(solarDataDiff),2)),'solarDataDiff contains NaN rows');

%--------------------------------------------------------------------------
%Make sure all data values are positive
%--------------------------------------------------------------------------
if sources.model
    assert(~any(radBeam(:)<0),'radBeam contains negative values');
    assert(~any(radDiff(:)<0),'radDiff contains negative values');
end
assert(~any(longwave(:)<0),'longwave contains negative values');
assert(~any(solarDataBeam(:)<0),'solarDataBeam contains negative values');
assert(~any(solarDataDiff(:)<0),'solarDataDiff contains negative values');

%--------------------------------------------------------------------------
%Write out the datasets
%--------------------------------------------------------------------------
outFile = [workingDir,filesep,outFilename];
%Write the datasets
solarDataAttribs.class = 'single';
if sources.model
    import_write_dataset(outFile,sources.output.radBeamMod.name,radBeam,solarDataAttribs,beamIndex,stations,false);
    import_write_dataset(outFile,sources.output.radDiffMod.name,radDiff,solarDataAttribs,diffIndex,stations,false);
end
import_write_dataset(outFile,sources.output.radLong.name,longwave,solarDataAttribs,longIndex,stations,false);
import_write_dataset(outFile,sources.output.radBeamMeas.name,solarDataBeam,solarDataAttribs,solarDataIndex,solarDataXY,false);
import_write_dataset(outFile,sources.output.radDiffMeas.name,solarDataDiff,solarDataAttribs,solarDataIndex,solarDataXY,false);
solarDataAttribs.units = 'deg';
import_write_dataset(outFile,sources.output.sunZenithCos.name,zenithCos,solarDataAttribs,zenithIndex,stations,false);
end



function [clearFrac] = conditions_parse(skyConditions)
%The values of skyConditions are set in AWOS_load
table = [0.9,0.1,0.5,0.25,0.75,2.5/8,0.5]; %CLR, OVC, POB, OBS, BKN, SCT, NULL
%For now, set 'NULL' to 0.5, change to actual statistical mean value
testNan = isnan(skyConditions);
skyConditions(testNan) = 7;
%leave NaNs in place, fill like other datasets
clearFrac = table(skyConditions);
clearFrac(testNan) = NaN;
end

function [longwave] = longwave_calc(clearFrac,airTemp,relHum)
stefanBoltzmann = 5.6704e-8; %Js^-1m^-2K^-4
%calculate the emissivity of the atmosphere, 
cloudFrac = 1 - clearFrac;
vapPress = sat_vap_pressure(airTemp) .* ...
    (unit_conversions(relHum,'pcent','dec'));  %actual air  vapor pressure from relative humidity
emissivity = atmospheric_emissivity(vapPress,cloudFrac);
longwave = emissivity * stefanBoltzmann .* (unit_conversions(airTemp,'C','K')).^4;
end

function [emissivity] = atmospheric_emissivity(vapPress,cloud_frac)
%from Hostetler and Bartlein 1990, from Henderson-Sellers 1986 (see H&B)
%vapor pressure must be in Pa, and cloud frac as 0<=cloud_frac<=1
emissivity = zeros(size(vapPress));
cloud_frac_1 = (cloud_frac < 0.4);
cloud_frac_2 = (~cloud_frac_1);
emissivity_1 = 0.84 - (0.1 - 9.973e-6 * vapPress(cloud_frac_1)) .* (1 - cloud_frac(cloud_frac_1)) + ...
    3.491e-5 * vapPress(cloud_frac_1);
emissivity_2 = 0.87 - (0.175 - 29.92e-6 * vapPress(cloud_frac_2)) .* (1 - cloud_frac(cloud_frac_2)) + ...
    2.693e-5 * vapPress(cloud_frac_2);
emissivity(cloud_frac_1) = emissivity_1;
emissivity(cloud_frac_2) = emissivity_2;
end

function [data] = clean_nans_mean(data,matchStationAvg)
numStations = size(data,2);
[tempAvg] = nanmean(data,2);
%Now, fill NaN values with the average
tempNan = isnan(data);
testNotAllNan = ~all(tempNan,2);
for m = 1:numStations
    tempInd = tempNan(:,m) & testNotAllNan;
    if matchStationAvg %this serves to guaurantee the the long-term average 
        %at a station is maintained even with filled data
        stationAvg = mean(data(~tempNan(:,m),m));
    end
    data(tempInd,m) = tempAvg(tempInd);
    if matchStationAvg
        newAvg = mean(data(tempInd,m));
        if ~isnan(stationAvg)
            data(tempInd,m) = data(tempInd,m) / newAvg * stationAvg;
        end
    end
end
end

function [data] = fill_gaps(modelHours,dayOfYear,data,period)
numStations = size(data,2);
notNan = ~any(isnan(data),2);
testNan = ~notNan;
[dataDoy,dataAvg] = time_series_aggregate(modelHours(notNan),mean(data(notNan),2),period,'mean','years','mean');
%Round both dayOfYear and dataDoy in order to better match values
dayOfYear = floor(dayOfYear) + round(rem(dayOfYear,1)*100)/100;
dataDoy = floor(dataDoy) + round(rem(dataDoy,1)*100)/100;
%Convert dayOfYear into indices of the julian-day averages
[test,doyLoc] = ismember(dayOfYear,dataDoy); 
data(testNan & test,:) = repmat(dataAvg(doyLoc(testNan & test)),1,numStations);
%Check to see if any NaNs remain
testNan = any(isnan(data),2);
notNan = ~testNan;
%Get the dataset maximum and minima
dataMax = repmat(nanmax(data,1),size(data,1),1);
dataMin = repmat(nanmin(data,1),size(data,1),1);
%Linearly interpolate the data, and constrain to max and min
data = interp1(modelHours(notNan),data(notNan,:),modelHours,'linear','extrap');
testOver = data>dataMax;
testUnder = data<dataMin;
data(testOver) = dataMax(testOver);
data(testUnder) = dataMin(testUnder);
%If any other NaNs remain, fill with closest previous value
testNan = isnan(data);
if any(testNan(:))
    findNan = find(testNan);
    for n = 1:length(findNan)
        data(findNan(n)) = data(findNan(n) - 1);
    end
end
end

function [data] = regularize_data(dataIn,index,modelHours,dayOfYear,numStations,cleanFlag,fillFlag)
%Initialize the output data
data = zeros(size(modelHours,1),numStations) + NaN;
%Determine which dates are present in the input dataset
tempInd = ismember_dates(modelHours,index(~isnan(index(:,2)),1),4,true);
%Place those data in to the regularized array
data(tempInd,1:size(dataIn,2)) = dataIn;
if cleanFlag
    %Clean NaNs if some stations are NaN, but not all
    data = clean_nans_mean(data,false);
end
if fillFlag
    %Fill gaps in the data where all stations are nan
    data = fill_gaps(modelHours,dayOfYear,data,'hours');
end
end