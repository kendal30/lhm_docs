function derived_inputs_ldas(workingDir,sources)
% DERIVED_INPUTS_CLIMATE  Calculates temp. dependent secondary climate inputs
%   derived_inputs_nldas()
% 
%   Note, this script also calculates the rain/snow partition
% 
%   Descriptions of Input Variables:
%   none, all are specified below
% 
%   Descriptions of Output Variables:
%   none, all are written to an intermediate HDF5 file
% 
%   Example(s):
%   >> calculate_derived_inputs
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2014-09-23
% Copyright 2014 Michigan State University.

%--------------------------------------------------------------------------
% Load data (load precip later, to save memory)
%--------------------------------------------------------------------------
% Load the airTemp and relHum data
airTemp = h5datareadlhm([workingDir,filesep,sources.airTemp.data],sources.airTemp.name,true);
relHum = h5datareadlhm([workingDir,filesep,sources.relHum.data],sources.relHum.name,true);

% Reduce data size
airTemp.data = single(airTemp.data);
relHum.data = single(relHum.data);

% For convenience
index = airTemp.index;
lookup = airTemp.lookup;

% Specify some output information the datasets
outFile = [workingDir,filesep,sources.outFile];

%--------------------------------------------------------------------------
% Calculate quantities and write to disk in real time to save memory
%--------------------------------------------------------------------------
% Then, calculate others
satVapPress = sat_vap_pressure(airTemp.data,'goff');
satVapPress = single(satVapPress);

% If this is run after a climate change ensemble, the air temperature array
% size may be larger than the relative humidity, expand relative humidty if
% so. Also convert to fraction instead of percent
if size(satVapPress,1) > size(relHum.data,1)
    tempRelHum = relHum.data(relHum.index(relHum.events,2),:) / 100;
else
    tempRelHum = relHum.data / 100;
end
clear relHum

% Calculate airVapPress and write out
airVapPress = satVapPress .* tempRelHum;
clear satVapPress
airVapPress(airVapPress < 1) = 1; % Make sure no very low values of airVapPress

write_dataset(outFile,sources.airVapPress.name,airVapPress,index,lookup,sources.airVapPress);
clear airVapPress

% Calculate the wet bulb temperature and write out
wetBulb = wet_bulb_temperature(airTemp.data,tempRelHum);
write_dataset(outFile,sources.wetBulb.name,wetBulb,index,lookup,sources.wetBulb);
clear tempRelHum wetBulb


% Now, handle precipitation-related data
precip = h5datareadlhm([workingDir,filesep,sources.precip.data],sources.precip.name,true);
precip.data = single(precip.data);

% Expand precip and airTemp data to their full index
tempPrecip = zeros(size(precip.index,1),size(precip.data,2),'single') + NaN;
tempPrecip(precip.events,:) = precip.data(precip.index(precip.events,2),:);
tempAirTemp = airTemp.data(airTemp.index(airTemp.events,2),:);
clear precip airTemp

% Run the rain/snow partition
[rain,snow] = rain_snow_partition(tempPrecip,tempAirTemp);
clear tempPrecip

% Get the index and write out the rain dataset
[rain,rainIndex] = prepare_special_data_index(rain,index);
write_dataset(outFile,sources.rain.name,rain,rainIndex,lookup,sources.rain);
clear rain rainIndex

% Get the snow density
snowDensity = new_snowfall_density(tempAirTemp);
clear tempAirTemp

% Insert NaN if no snow (will be removed later)
snowDensity(all(snow==0 | isnan(snow),2),:) = NaN;

% Prepare the special index for each and write out
[snow,snowIndex] = prepare_special_data_index(snow,index);
write_dataset(outFile,sources.snow.name,snow,snowIndex,lookup,sources.snow);
clear snow snowIndex

[snowDensity,snowDensIndex] = prepare_special_data_index(snowDensity,index);
write_dataset(outFile,sources.snowDensity.name,snowDensity,snowDensIndex,lookup,sources.snowDensity);
clear snowDensity snowDensIndex

end


function [data,index] = prepare_special_data_index(data,baseIndex)
    % Determine zerosTest of these data
    zerosTest = all(data==0 | isnan(data),2);

    % Create data arrays without those rows
    data = data(~zerosTest,:);

    % Make new indices filling in zero grids with -1
    index = baseIndex;
    index(zerosTest,2) = -1;

    % Get the size of each data array
    numActive = sum(~zerosTest);

    % Now, complete the index rows
    index(~zerosTest,2) = (1:numActive);
end


function write_dataset(outFile,outName,data,index,lookup,source)

tempGroup = ['/',outName];
attribs = struct('class','');

% Create the output group
h5groupcreate(outFile,tempGroup);

% Write the lookup table
attribs.class = class(lookup);
h5datawrite(outFile,[tempGroup,'/lookup'],lookup,attribs,false);

% Write the output index
attribs.class = class(index);
h5datawrite(outFile,[tempGroup,'/index'],index,attribs,false);

% Write the output data
attribs = source;
attribs = rmfield(attribs,'name');
h5datawrite(outFile,[tempGroup,'/data'],data,attribs,true);


end