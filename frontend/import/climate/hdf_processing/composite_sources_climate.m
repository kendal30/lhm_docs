function composite_sources_climate(modelHours,workingDir,outFilename,sources)
%COMPOSITE_SOURCES_CLIMATE  Composites similar datatypes from multiple sources into a single file
%   composite_sources_climate()
%
%   Descriptions of Input Variables:
%   none, all are specified within the first code block
%
%   Descriptions of Output Variables:
%   none, all are written to the specified output HDF5 file
%
%   Example(s):
%   >> composite_sources_climate
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-29
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Read in the data one type at a time, and save arrays to an HDF5 file
%--------------------------------------------------------------------------
outFile = [workingDir,'\',outFilename];

%Determine the number of distinct datatypes defined above
dataTypes = fieldnames(sources);
for m = 1:length(dataTypes);
    %Determine the number of data sources within this type
    numType = length(sources.(dataTypes{m}).data);
    %Initiailize some values used in this loop
    columnCount = 0;
    [loopData,loopAttribs,loopEvents,loopZeros,loopIndex,loopXY,loopLookup,columnOffset] = deal(cell(numType,1));
    loopPriority = zeros(numType,1);
    %----------------------------------------------------------------------
    %Read in the datasets, index tables, XY tables or lookup tables
    %----------------------------------------------------------------------
    for n = 1:numType
        %Define these for convenience
        tempFile = [workingDir,'\',sources.(dataTypes{m}).data{n}];
        tempVar = sources.(dataTypes{m}).name{n};
        
        %Read in the dataset
        tempData = h5datareadlhm(tempFile,['/',tempVar],false); %make sure that no transformation occurs
        
        %Store
        loopData{n} = tempData.data;
        loopAttribs{n} = tempData.attribs;
        loopEvents{n} = tempData.events;
        loopZeros{n} = tempData.zeros;
        loopIndex{n} = tempData.index;
        loopXY{n} = tempData.stationsXY;
        loopLookup{n} = tempData.lookup;
        
        %Keep track of the total number of data columns
        if n ==1
            columnOffset{n} = 0;
        else
            columnOffset{n} = size(loopData{n-1},2) + columnOffset{n-1};
        end
        columnCount = columnCount + size(loopData{n},2);

        %Record the priority of this dataset
        loopPriority(n) = sources.(dataTypes{m}).priority(n);
    end
    
    %All attributes must be identical within a data type
    if numType > 1
        attribFields = fieldnames(loopAttribs{1});
        for n = 2:numType
            for o = 1:length(attribFields)
                assert(strcmp(loopAttribs{1}.(attribFields{o}),loopAttribs{n}.(attribFields{o})),...
                    'All attributes for like datatypes must be identical, check ',dataTypes{m});
            end
        end
    end
    
    %----------------------------------------------------------------------
    %Create the output data, index, lookup, and xy arrays
    %----------------------------------------------------------------------
    %First, loop through the data to determine the size of the data array
    %to create
    noDataRow = true(size(modelHours,1),1);
    maxPriority = zeros(size(modelHours,1),1) - 1;
    [rowInd,loopRow,loopIndexTrim] = deal(cell(numType,1));
    
    for n = 1:numType
        %First, remove all rows with NaNs from the loopIndex table
        loopIndexTrim{n} = loopIndex{n}(loopEvents{n} | loopZeros{n},:);
        
        %Determine the row indeces of the dataset in the master table
        [rowInd{n},loopRow{n}] = ismember_dates(modelHours,loopIndexTrim{n}(:,1),4,true);
        
        %Check to see if the data exceeds all existing datasets in priority
        higherPriority = (loopPriority(n) > maxPriority) & rowInd{n};
        
        %Update the maxPriority array
        maxPriority(higherPriority) = loopPriority(n);
        
        %Check to see if the data is lower in priority than existing
        %datasets, set the rowInd to false if so
        lowerPriority = (loopPriority(n) < maxPriority) & rowInd{n};
        if any(lowerPriority)
            rowInd{n}(lowerPriority) = false;
        end
        
        %Finally, determine if the data rows will be all Zeros
        noDataRow(rowInd{n}) = noDataRow(rowInd{n}) & loopZeros{n}(rowInd{n});
    end
    dataRow = ~noDataRow;
    
    %Initialize output arrays
    data = zeros(sum(dataRow),columnCount,class(loopData{n})) + str2double(loopAttribs{1}.nan);
    index = [modelHours,zeros(length(modelHours),1) + NaN,...
        zeros(length(modelHours),1) + NaN];
    lookup = [];
    xy = [];
    
    for n = 1:numType
        %Check to see if the -1 flag exists in the loopIndex{n} table, and
        %set the index table accordingly, remove these rows from rowInd
        if any(loopZeros{n})
            rowZero = ismember_dates(modelHours,loopIndex{n}(loopZeros{n},1),4,true);
            assert(all(isnan(index(rowZero & rowInd{n},3))),...
                'A data source with the data == 0 flag in its index cannot share a row with another data source');
            index(rowZero & rowInd{n},(2:3)) = -1;
            rowInd{n}(rowZero) = false;
        end

        %Insert the data into the loopData table
        tempColumns = columnOffset{n}+(1:size(loopData{n},2));
        data(rowInd{n}(dataRow),tempColumns) = ...
            loopData{n}(loopIndexTrim{n}(loopRow{n}(rowInd{n} & dataRow),2),:);
        
        %Fill in the index array
        index(rowInd{n},2) = find(rowInd{n}(dataRow));
        
        %If there is a lookup table defined for this dataset, store it as
        %the lookup table in the index table.  Otherwise, store the xy table data
        %This is somewhat complicated, but if there is a lookup table defined, then this
        %dataset must be either lower priority than an existing dataset or
        %higher priority than all existing.  In other words, you cannot
        %have two equal priority datasets with different lookup tables.
        if ~islogical(loopLookup{n})
            assert(all(higherPriority(rowInd{n}) | lowerPriority(rowInd{n})),...
                'A data source with a lookup table cannot share a row with another data source')
            %Renumber the lookup table according to new column numbering
            loopLookup{n} = loopLookup{n} + columnOffset{n};
            if ~isempty(lookup)
                currentSheet = size(lookup,3) + 1;
                loopLookup = cat(lookup,loopLookup{n},3);
            else
                currentSheet = 1;
                lookup = loopLookup{n};
            end
            index(rowInd{n},3) = currentSheet;
        else
            tempXY = [loopXY{n}(:,1:end-1),tempColumns']; %Here, column indices are 
            if ~isempty(xy)
                xy = cat(1,xy,tempXY);
            else
                xy = tempXY;
            end
        end      
    end
    
    %Check to make sure there are no all-Nan rows in the data array
    assert(~any(all(isnan(data),2)),[dataTypes{m},' contains NaN rows']);
    
    %Clear up some memory
    clear loopData
    %----------------------------------------------------------------------
    %Save the data to an intermediate HDF5 file
    %----------------------------------------------------------------------
    group = dataTypes{m};
    if isempty(lookup),lookup=false;end
    if isempty(xy),xy=false;end
    import_write_dataset(outFile,group,data,loopAttribs{1},index,xy,lookup,false);
    
    %Clear up some memory
    clear data index lookup xy
end
