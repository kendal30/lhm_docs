function lookup_map_generation(workingDir,outFilename,sources)
%LOOKUP_MAP_GENERATION  Generates lookup maps given a climate HDF5 file
%   lookup_map_generation()
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> lookup_map_generation
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-29
% Copyright 2008 Michigan State University.

more off %one of the functions this calls has unsuppressable warnings

%--------------------------------------------------------------------------
%Load model grid and build necessary data structure from header
%--------------------------------------------------------------------------
modelHeader = import_grid(sources.modelGrid,'header');
sources = rmfield(sources,'modelGrid');

%--------------------------------------------------------------------------
%Prepare these inputs for convenience
%--------------------------------------------------------------------------
datasets = fieldnames(sources);
outFile = [workingDir,filesep,outFilename];

%--------------------------------------------------------------------------
%Loop through dataGroups
%--------------------------------------------------------------------------
for m = 1:length(datasets)
    %Determine these for convenience
    dataset = datasets{m};
    inFile = [workingDir,filesep,sources.(dataset).data];
    tempGroup = ['/',sources.(dataset).name];

    %Read in the data and attributes
    [data,attribs] = h5dataread(inFile,[tempGroup,'/data'],false);
    %Read in the index
    index = h5dataread(inFile,[tempGroup,'/index'],false);
    %Read in the stationsXY table, if present
    stationsXY = h5dataread(inFile,[tempGroup,'/stationsXY'],false);
    %Read in the lookup table, if present
    lookup = h5dataread(inFile,[tempGroup,'/lookupPartial'],false);
    
    %Peruse the index and look for all combinations of stations present
    if ~islogical(stationsXY)
        stationData = data(:,stationsXY(:,end));
         
        present = zeros(size(stationData,1),size(stationsXY,1)+2);
        %The first column of the present array is the row number of the
        %station data
        present(:,1) = (1:size(stationData,1))';
        %The second column is the index array row number
        present(:,2) = find(index(:,2)>0);
        %The second through last columns are logical flags if the station
        %data is not NaN 
        present(:,3:end) = ~(stationData==str2double(attribs.nan));
        %Sort the station data in ascending order along each column
        %individually 
        present = sortrows(present,-(3:size(present,2)));
        
        %Remove any rows that are all absent (will occur if existing lookup
        %table is present)
        testNaN = all(isnan(present(:,3:end)),2);
        if any(testNaN)
            %these will be at the bottom of the present array becuase of
            %sorting
            present = present(1:find(testNaN,1,'first')-1,:); 
        end
        
        %Determine how many unique combinations of stations there are and
        %the breakpoints of those combinations
        breakpoints = [0;find(any(diff(present(:,3:end),1)~=0,2));size(present,1)];
        uniques = numel(breakpoints)-1;
        
        %Create the lookup table 3-D array, or expand the one already
        %present
        if islogical(lookup)
            %Determine the what class of data will be required for the
            %lookup table
            if size(stationsXY,1) > 255
                lookupClass = 'uint16';
            else
                lookupClass = 'uint8';
            end
            lookup = zeros(modelHeader.rows,modelHeader.cols,uniques,lookupClass);
            currSheet = 0;
        else
            oldSheets = size(lookup,3);
            lookupClass = class(lookup);
            lookup(:,:,oldSheets+1:oldSheets+uniques) = zeros(modelHeader.rows,modelHeader.cols,uniques,lookupClass);
            currSheet = oldSheets;
        end
        
        %Now, loop through the unique combinations of stations and create
        %lookup tables for each
        for n = 1:uniques
            %Subset the stationsXY array for only those stations that are
            %present
            stationXY = stationsXY(logical(present(breakpoints(n)+1,3:end)),:);
            if ~isempty(stationXY)
                %Handle non-unique station locations properly
                [uniqueXY] = unique(stationXY(:,2:3),'rows','first');
                assert(size(uniqueXY,1)==size(stationXY,1),'Duplicate station values detected');
                %Create theissen polygons for those stations
                theissen = theissen_polygons(modelHeader,stationXY(:,2:3));

                %Modify the values of the theissen array according to the
                %actual column values in the data array
                columnMap = stationXY(:,end);
                theissen = columnMap(theissen);
                %Add this theissen polygon array to the lookup table 3-D array
                currSheet = currSheet + 1;
                lookup(:,:,currSheet) = theissen;
                %Now, write the lookup sheet to the index array
                indexInd = present(breakpoints(n)+1:breakpoints(n+1),2);
                index(indexInd,3) = currSheet;
            end
        end
    end
    
    %Write the output
    import_write_dataset(outFile,tempGroup,data,attribs,index,stationsXY,lookup,false);
end

more on