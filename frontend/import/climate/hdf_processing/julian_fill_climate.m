function julian_fill_climate(workingDir,outFilename,sources)
%JULIAN_FILL_CLIMATE  Fills missing data using julian day/hour averages
%   julian_fill_climate()
%
%   Descriptions of Input Variables:
%   none, all are specified below.
%
%   Descriptions of Output Variables:
%   none, all are written to the specified hdf5 file.
%
%   Example(s):
%   >> julian_fill_climate
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-04
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Loop through datasets
%--------------------------------------------------------------------------
datasets = fieldnames(sources);
for m = 1:length(datasets);
    %Load data
    dataset = h5datareadlhm([workingDir,filesep,sources.(datasets{m}).data],...
        ['/',sources.(datasets{m}).name]);
    data = dataset.data;
    dataAttribs = dataset.attribs;
    index = dataset.index;
    stationsXY = dataset.stationsXY;
    testPresent = dataset.events;
    
    %Create a new filled data array
    numStations = size(data,2);
    filledData = zeros(size(index,1),numStations) + NaN;
    filledData(testPresent,:) = data;
    
    %From index tables, derive hours present
    hoursPresent = index(testPresent,1);

    %Calculate julian day-of-year averages
    [avgDOY,avgData] = time_series_aggregate(hoursPresent,data,'hours','mean','years','mean');
    
    %Fill any missing data periods in a given station with mean of other
    %station values
    avgData = clean_nans_mean(avgData,true);
    avgData = nanmean(avgData,2);
    
    %Determine missing hours
    testNan = repmat(isnan(index(:,2)),1,numStations);
    
    %Check to see if a given station is online/offline yet
    testOnline = true(size(testNan));
    for n = 1:numStations
        stationStart = find(~isnan(filledData(:,n)),1,'first');
        stationEnd = find(~isnan(filledData(:,n)),1,'last')+1;
        if stationEnd > size(filledData,1),stationEnd = size(filledData,1);end
        
        %Modify testNan to say "false" if before stationStart or after
        %stationEnd, these are not missing hours
        testOnline(1:stationStart,n) = false;
        testOnline(stationEnd:end,n) = false;
    end
    
    %Force a single station to be filled, if no others will
    noneOnline = all(~testOnline,2);
    allNans = all(~testNan,2);
    testNan(noneOnline & allNans,1) = true;
    
    %Fill missing hours (all stations) with julian day-of-year averages
    dataDOY = date2doy(index(:,1));
    %Resample the avgData array to dataDOY
    avgDataInterp = repmat(interp1(avgDOY,avgData,dataDOY,'linear','extrap'),1,numStations);
    avgDataInterp = limit_vals(range(avgData(:)),avgDataInterp);
    %Fill the data
    filledData(testNan) = avgDataInterp(testNan);
    
    %Check that there are indeed no more NaNs in the dataset
    assert(~any(all(isnan(filledData),2)),['There are still NaN rows present in ',datasets{m}]);
    
    %Make sure that there are no negative data values, except for airTemp
    if ~strcmpi(datasets{m},{'airTemp','soilTempShallow','soilTempDeep','wetBulb','sunZenithCos'})
        assert(~any(filledData(:)<0),['There are negative values present in ',datasets{m}]);
    end
    
    %Update the index to reflect that the dataset has been filled
    index(:,2) = (1:size(filledData,1));
    
    %Write out filled dataset
    import_write_dataset([workingDir,filesep,outFilename],['/',sources.(datasets{m}).name],...
        filledData,dataAttribs,index,stationsXY,false);
end

end

function [data] = clean_nans_mean(data,matchStationAvg)
numStations = size(data,2);
[tempAvg] = nanmean(data,2);
%Now, fill NaN values with the average
tempNan = isnan(data);
testNotAllNan = ~all(tempNan,2);
for m = 1:numStations
    tempInd = tempNan(:,m) & testNotAllNan;
    if matchStationAvg %this serves to guaurantee the the long-term average 
        %at a station is maintained even with filled data
        stationAvg = mean(data(~tempNan(:,m),m));
    end
    data(tempInd,m) = tempAvg(tempInd);
    if matchStationAvg
        newAvg = mean(data(tempInd,m));
        if ~isnan(stationAvg)
            data(tempInd,m) = data(tempInd,m) / newAvg * stationAvg;
        end
    end
end
end