function climate_hdf_write(workingDir,climateName,climateVersion,IDType,compileDate,sources)
%CLIMATE_HDF_WRITE  Generates a properly-formatted climate HDF5 file.
%   climate_hdf_write()
%
%   Descriptions of Input Variables:
%   none, all inputs specified below.
%
%   Descriptions of Output Variables:
%   none, all outputs written to final hdf5 file
%
%   Example(s):
%   >> climate_hdf_write
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-29
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Create the output filename and get start/end times
%--------------------------------------------------------------------------
%Create the serialID and outFile name
serialID = serial_ID_gen(IDType,climateName,climateVersion,compileDate);
outFilename = [IDType,'_',climateName,'_',climateVersion,'_',datestr(compileDate,'yyyymmdd'),'.h5'];
outFile = [workingDir,filesep,outFilename];

%Read in the first index and get start and end times
datasets = fieldnames(sources);
index = h5dataread([workingDir,filesep,sources.(datasets{1}).data],...
    ['/',sources.(datasets{1}).name,'/index']);
startDate = datestr(index(1,1),'yyyy-mm-dd HH:MM:SS');
endDate = datestr(index(end,1),'yyyy-mm-dd HH:MM:SS');

%--------------------------------------------------------------------------
%Create the info group and write info to it
%--------------------------------------------------------------------------
h5groupcreate(outFile,'/info');
attribs = struct('class','char');
h5datawrite(outFile,'/info/climate_name',climateName,attribs);
h5datawrite(outFile,'/info/climate_version',climateVersion,attribs);
h5datawrite(outFile,'/info/compile_date',datestr(compileDate,'yyyy-mm-dd'),attribs);
h5datawrite(outFile,'/info/serial_ID',serialID,attribs);
h5datawrite(outFile,'/info/time_start',startDate,attribs);
h5datawrite(outFile,'/info/time_end',endDate,attribs);

%--------------------------------------------------------------------------
%Loop through the datasets, writing out as it goes
%--------------------------------------------------------------------------
for m = 1:length(datasets)
    %Determine these for convenience
    dataset = datasets{m};
    inFile = [workingDir,filesep,sources.(dataset).data];
    tempGroup = ['/',sources.(dataset).name];

    %Read in the data and attributes
    [data,attribs] = h5dataread(inFile,[tempGroup,'/data'],false);
    %Read in the index
    index = h5dataread(inFile,[tempGroup,'/index'],false);
    %Read in the stationsXY table, if present
    stationsXY = h5dataread(inFile,[tempGroup,'/stationsXY'],false);
    %Read in the lookup table, if present
    lookup = h5dataread(inFile,[tempGroup,'/lookup'],false);
    
    %Check the attributes to make sure they are properly formatted
    assert(isfield(attribs,'units'),['Units not defined for ',dataset]);
    assert(isfield(attribs,'nan'),['Nan val not defined for ',dataset]);
    
    %Create the output group
    h5groupcreate(outFile,tempGroup);
    
    %Write the output data, index, stationsXY, and lookup
    h5datawrite(outFile,[tempGroup,'/data'],data,attribs,false);
    %Write the output index
    attribs = struct('class',class(index));
    h5datawrite(outFile,[tempGroup,'/index'],index,attribs,false);
    %Write the output stationsXY, if present
    if ~islogical(stationsXY)
        attribs.class = class(stationsXY);
        h5datawrite(outFile,[tempGroup,'/stationsXY'],stationsXY,attribs,false);
    end
    %Write the lookup table
    attribs.class = class(lookup);
    h5datawrite(outFile,[tempGroup,'/lookup'],lookup,attribs,false);
end

% Display a final status message pointing to the written file
fprintf('CLIM file %s written to %s',outFilename,workingDir)

end