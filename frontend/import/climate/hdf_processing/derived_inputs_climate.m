function derived_inputs_climate(workingDir,outFilename,sources)
%DERIVED_INPUTS_CLIMATE  Calculates derived (secondary) climate inputs
%   derived_inputs_climate()
%
%   Descriptions of Input Variables:
%   none, all are specified below
%
%   Descriptions of Output Variables:
%   none, all are written to an intermediate HDF5 file
%
%   Example(s):
%   >> calculate_derived_inputs
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-04
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Load data
%--------------------------------------------------------------------------
airTemp = h5datareadlhm([workingDir,filesep,sources.airTemp.data],sources.airTemp.name);
%Note, for index and stationXY, both airTemp and relHum have the same set,
%otherwise I would have to composite them first
relHum = h5datareadlhm([workingDir,filesep,sources.relHum.data],sources.relHum.name);

%--------------------------------------------------------------------------
%Calculate quantities
%--------------------------------------------------------------------------
%First, match stations and indices
[stationMatch,stationMatchOrder] = ismember(airTemp.stationsXY(:,1),relHum.stationsXY(:,1));
[indexMatch,indexMatchOrder] = ismember(airTemp.index(airTemp.events,1),relHum.index(relHum.events,1));

%Then, calculate quantities
airVapPress = sat_vap_pressure(airTemp.data(indexMatch,stationMatch)) .* ...
    relHum.data(indexMatchOrder(indexMatch),stationMatchOrder(stationMatch)) / 100;
wetBulb = wet_bulb_temperature(airTemp.data(indexMatch,stationMatch),...
    relHum.data(indexMatchOrder(indexMatch),stationMatchOrder(stationMatch)) / 100);
snowDens = new_snowfall_density(airTemp.data);
if isfield(sources,'pressure')
    pressure = barometric_formula(airTemp.data,repmat(airTemp.stationsXY(:,6)',[size(airTemp.data,1),1]));
end
%Make sure no very low values of airVapPress
airVapPress(airVapPress < 1) = NaN;

%Build index and stationsXY tables
[indexAirVapPress,indexWetBulb,indexSnowDens] = deal(airTemp.index);
tempIndex = airTemp.index(airTemp.events,:);
tempIndex(indexMatch,2) = (1:size(airVapPress,1))';
tempIndex(~indexMatch,2) = NaN;
indexAirVapPress(airTemp.events,:) = tempIndex;

tempIndex = airTemp.index(airTemp.events,:);
tempIndex(indexMatch,2) = (1:size(wetBulb,1))';
tempIndex(~indexMatch,2) = NaN;
indexWetBulb(airTemp.events,:) = tempIndex;

%Build stationsXY tables
[stationsAirVapPress,stationsWetBulb] = deal([airTemp.stationsXY(stationMatch,1:end-1),(1:sum(stationMatch))']);

stationsSnowDens = airTemp.stationsXY;
if isfield(sources,'pressure')
    stationsPressure = stationsSnowDens;
    indexPressure = indexSnowDens;
end

%--------------------------------------------------------------------------
%Check the datasets for full NaN rows
%--------------------------------------------------------------------------
assert(~any(all(isnan(airVapPress),2)),'airVapPress contains NaN rows');
assert(~any(all(isnan(wetBulb),2)),'wetBulb contains NaN rows');
assert(~any(all(isnan(snowDens),2)),'snowDens contains NaN rows');
if isfield(sources,'pressure');
    assert(~any(all(isnan(pressure),2)),'pressure contains NaN rows');
end

%--------------------------------------------------------------------------
%Make sure all data values are positive
%--------------------------------------------------------------------------
assert(~any(airVapPress(:)<0),'airVapPress contains negative values');
assert(~any(snowDens(:)<0),'snowDens contains negative values');
if isfield(sources,'pressure');
    assert(~any(pressure(:)<0),'pressure contains negative values');
end

%--------------------------------------------------------------------------
%Write out arrays
%--------------------------------------------------------------------------
outFile = [workingDir,filesep,outFilename];
attribs = struct('class',sources.airVapPress.class,'units',sources.airVapPress.units,...
    'scale',sources.airVapPress.scale,'offset',sources.airVapPress.offset,'nan',sources.airVapPress.nan);
import_write_dataset(outFile,['/',sources.airVapPress.name],airVapPress,attribs,indexAirVapPress,stationsAirVapPress,false);

attribs = struct('class',sources.wetBulb.class,'units',sources.wetBulb.units,...
    'scale',sources.wetBulb.scale,'offset',sources.wetBulb.offset,'nan',sources.wetBulb.nan);
import_write_dataset(outFile,['/',sources.wetBulb.name],wetBulb,attribs,indexWetBulb,stationsWetBulb,false);

attribs = struct('class',sources.snowDensity.class,'units',sources.snowDensity.units,...
    'scale',sources.snowDensity.scale,'offset',sources.snowDensity.offset,'nan',sources.snowDensity.nan);
import_write_dataset(outFile,['/',sources.snowDensity.name],snowDens,attribs,indexSnowDens,stationsSnowDens,false);

if isfield(sources,'pressure')
    attribs = struct('class',sources.pressure.class,'units',sources.pressure.units,...
        'scale',sources.pressure.scale,'offset',sources.pressure.offset,'nan',sources.pressure.nan);
    import_write_dataset(outFile,['/',sources.pressure.name],pressure,attribs,indexPressure,stationsPressure,false);
end

end