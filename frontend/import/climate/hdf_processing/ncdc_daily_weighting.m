function ncdc_daily_weighting(workingDir,outFile,sources)
%NCDC_DAILY_WEIGHTING  Uses NCDC daily precipitation data to correct hourly data
%   ncdc_daily_weighting()
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> lookup_map_generation
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-03-20
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Load model grid and build necessary data structure from header
%--------------------------------------------------------------------------
%Read in the grids header metadata
precipGeoLoc = import_grid(sources.precipGrid,'header');
modelGeoLoc = import_grid(sources.modelGrid,'header');

%Now, run the lookup mapping function
gridLookup = cell_statistics(precipGeoLoc,modelGeoLoc);
gridLookup = uint16(gridLookup);

%Build the x and y arrays
xArray = modelGeoLoc.left + (0:modelGeoLoc.cellsizeX:modelGeoLoc.cellsizeX*(modelGeoLoc.cols-1)) + modelGeoLoc.cellsizeX/2;
yArray = fliplr(modelGeoLoc.bottom + (0:modelGeoLoc.cellsizeY:modelGeoLoc.cellsizeY*(modelGeoLoc.rows-1)))' + modelGeoLoc.cellsizeY/2;

%Get precalcuated information for upscaling
[~,~,upscalePrecalc] = cell_statistics(precipGeoLoc,modelGeoLoc,zeros(modelGeoLoc.rows,modelGeoLoc.cols),'fast_mean','majority');

%--------------------------------------------------------------------------
%Read in precipitation data
%--------------------------------------------------------------------------
%Hourly Data
tempFile = [workingDir,filesep,sources.precip_hourly.data];
tempGroup = ['/',sources.precip_hourly.name];
hourly = h5datareadlhm(tempFile,tempGroup,false);
hourlyData = hourly.data;
hourlyAttribs = hourly.attribs;
hourlyIndex = hourly.index;
hourlyLookup = hourly.lookup;
hourlyEvents = hourly.events;

%Daily Data (aggregated to some longer timeframe, i.e. weekly)
tempFile = [workingDir,filesep,sources.precip_daily.data];
tempGroup = ['/',sources.precip_daily.name];
daily = h5datareadlhm(tempFile,tempGroup,false);
dailyData = daily.data;
dailyAttribs = daily.attribs;
dailyIndex = daily.index;
dailyXY = daily.stationsXY;

%Set a small delta value for use later in reweighting
hourlyDelta = 1; %this works if the values are rescaled by 100000 or some other large number, and stored as integers

%--------------------------------------------------------------------------
%Determine mean intensity of hourly precip for each julian day
%--------------------------------------------------------------------------
%Build an hourly precip input properly for looking at intensity
precipHourly = double(hourlyData);
precipHourly(hourlyData==str2double(hourlyAttribs.nan)) = NaN;
precipHourly(hourlyData==0) = NaN;

%Determine the hour of rainfall events, shift so that local noon is 0
timeHourly = repmat(hourlyIndex(hourlyEvents,1),1,size(precipHourly,2));
timeHourly = floor(rem(timeHourly,1)*24);
timeHourly(isnan(precipHourly)) = NaN;
timeHourly = timeHourly + sources.UTC - 12;
timeHourly(timeHourly<0) = timeHourly(timeHourly<0) + 24;
timeHourly(timeHourly>24) = timeHourly(timeHourly>24) - 24;

%Determine average intensity and time of day of occurrence
[~,dailyIntense] = time_series_aggregate(hourlyIndex(hourlyEvents,1),...
    precipHourly,'days','mean','years','median');
[~,dailyTimeOccur] = time_series_aggregate(hourlyIndex(hourlyEvents,1),...
    timeHourly,'days','median','years','mode');

%Only use stations with a reasonable number of points
shortRecord = (sum(~isnan(precipHourly)))<(size(precipHourly,1)/4);
avgIntense = nanmean(dailyIntense(:,~shortRecord),2);
%Fill in NaN values
test = isnan(avgIntense);
xVals = (1:length(avgIntense));
avgIntense(test) = interp1(xVals(~test),avgIntense(~test),xVals(test));
%Add the 366th day if not already present
if length(avgIntense)<366
    avgIntense(366) = avgIntense(365);
end
%Smooth
avgIntense = smoothn(avgIntense,[14+1,1]);

%Determine the average time of day of occurrence, shift back to UTC normal
avgTimeOccur = nanmedian(dailyTimeOccur,2);
avgTimeOccur = smoothn(avgTimeOccur,[14+1,1]);
avgTimeOccur = avgTimeOccur - sources.UTC + 12;
avgTimeOccur(avgTimeOccur>24) = avgTimeOccur(avgTimeOccur>24) - 24;
avgTimeOccur(avgTimeOccur<0) = avgTimeOccur(avgTimeOccur<0) + 24;
%Add the 366th day if not already present
if length(avgTimeOccur)<366
    avgTimeOccur(366) = avgTimeOccur(365);
end
%--------------------------------------------------------------------------
%Create weight maps
%--------------------------------------------------------------------------
%Create data arrays
blankGrid = zeros(size(hourlyLookup,1),size(hourlyLookup,2));
finalData = zeros(size(hourlyData,1),precipGeoLoc.rows*precipGeoLoc.cols,class(hourlyData));
addData = zeros(0,precipGeoLoc.rows*precipGeoLoc.cols,class(hourlyData));
numWeights = size(dailyIndex,1);
allDaily = zeros([size(blankGrid),numWeights],'single');
classFunc = str2func(class(hourlyData));

%Initialize arrays
previousStations = false(1,size(dailyData,2));
addIndex = hourlyIndex;
addIndex(:,2) = NaN;

%Define constant for later calculation
delTime = 0.15/24; %allow for a quarter-hour time discrepancy to avoid floating point issues

h = waitbar(0,'Creating Weight Maps');
for m = 1:numWeights
    waitbar(m/numWeights,h);

    %Determine which daily stations are active this time
    dailyInd = dailyIndex(m,3);
    if dailyInd > 0
        thisStations = (dailyData(dailyInd,:) ~= str2double(dailyAttribs.nan));
        thisXY = dailyXY(thisStations',2:3);
        thisData = double(dailyData(dailyInd,thisStations));

        %Get the weight grid, if it's different than the previous one
        if ~all(thisStations == previousStations)
            thisMaxNeighb = min(sum(thisStations),sources.method.maxNeighbors);
            try
                distWeight = euclidian_weight(xArray,yArray,thisXY,-2,thisMaxNeighb); %inverse-squared weighting, only thisMaxNeighb
            catch
                disp(thisXY)
            end
        end
        previousStations = thisStations;

        %Interpolate this daily data array to a grid via inverse distance
        %weighting
        thisDaily = idw(thisData,distWeight);
        activeDaily = (thisDaily > 0);
    else
        thisDaily = blankGrid;
    end
    allDaily(:,:,m) = single(thisDaily);

% %Delete this second loop eventually, down to %------------
% % end
% % close(h)
% load tempAllDaily.mat allDaily
% h = waitbar(0,'Creating Weight Maps');
%
% for m = 1:numWeights
%     waitbar(m/numWeights,h);
%     thisDaily = double(allDaily(:,:,m));
%     activeDaily = (thisDaily > 0);
% %------------
    %Create the thisHourly grid
    %Determine the indeces of hourly grids for this daily-summed grid
    thisIndRange = bracket_index(hourlyIndex(:,1),dailyIndex(m,1)-delTime,dailyIndex(m,2)+delTime);
    numHours = diff(thisIndRange)+1;
    numActive = sum(hourlyIndex(thisIndRange(1):thisIndRange(2),2)>0);
    if ~isempty(thisIndRange) && (numActive > 0)
        %Now, loop through all of the hours in the daily grids and sum them
        thisHourly = repmat(blankGrid,[1,1,numActive]);
        loopIndices = zeros(numActive,1);
        loopIndHourly = 0;
        for n = 1:numHours;
            loopInd = thisIndRange(1) + n - 1;
            if hourlyIndex(loopInd,2) > 0
                loopIndHourly = loopIndHourly + 1;
                loopIndices(loopIndHourly) = loopInd;
                loopHourly = double(hourlyData(hourlyIndex(loopInd,2),:));
                loopLookup = hourlyLookup(:,:,hourlyIndex(loopInd,3));
                thisHourly(:,:,loopIndHourly) = loopHourly(loopLookup);
            end
        end
        sumHourly = sum(thisHourly,3);
        activeHourly = (sumHourly > 0);

        %Add a tiny amount to all cells in hours where some have values,
        %this allows the reweighting to function properly (reweighting can
        %set positive values to 0, but cannot set 0 values positive).
        indDelta = activeDaily & (~activeHourly);
        sumHourly(indDelta) = hourlyDelta;
        thisHourly(repmat(indDelta,[1,1,numActive])) = hourlyDelta / numActive;
    else
        sumHourly = blankGrid;
        activeHourly = logical(blankGrid);
    end

    %Now, do the reweighting of hourly grids
    if any(activeHourly(:)) %Determine the weights grid
        if any(activeDaily(:))
            %Calculate the weight grid
            dailyWeight = thisDaily./sumHourly;
            dailyWeight(isnan(dailyWeight)) = 1;
            dailyWeight(isinf(dailyWeight)) = 1;
        else
            dailyWeight = blankGrid + 1; %otherwise, weights are all 1
        end

        %Now, reweight the hourly values
        thisHourlyWeighted = thisHourly .* repmat(dailyWeight,[1,1,numActive]);

        %Assert that the sum of this is reasonably close to the daily grid
        if any(activeDaily(:))
            try
                test = abs(sum(thisHourlyWeighted,3)-thisDaily);
                assert(all(test(:)<1e-9))
            catch
                disp(test);
            end
        end

        %Now, loop through all the hours in the daily grids, upsample to
        %the precip grid scale, save to final data
        for n = 1:numActive
            %Upscale This
            loopUpscale = cell_statistics(precipGeoLoc,modelGeoLoc,...
                thisHourlyWeighted(:,:,n),'fast_mean','majority',upscalePrecalc);

            %Back-convert NaNs to their nanval
            loopUpscale(isnan(loopUpscale)) = str2double(dailyAttribs.nan);

            %Assign to the finalData array, after converting the class
            finalData(hourlyIndex(loopIndices(n),2),:) = classFunc(loopUpscale(:));
        end
    elseif any(activeDaily(:)) %Use the average intensity and rainfall time to assign daily data back to hourly
        %First, determine the number of hours of precipitation in each
        %cell
        dayInd = (floor(date2doy(dailyIndex(m,1))));
        numHoursEvent = ceil(thisDaily ./ avgIntense(dayInd));
        numHoursEvent(numHoursEvent > numHours) = numHours;

        %Determine the hours of match between hourly and daily data
        startHour = round(rem(hourlyIndex(thisIndRange(1),1),1)*24);
        if floor(hourlyIndex(thisIndRange(1),1)) < floor(hourlyIndex(thisIndRange(2),1))
            startHour = startHour - 24;
        end
        endHour = round(rem(hourlyIndex(thisIndRange(2),1),1)*24);

        %Now, since the observation day may end not at 2400, shift
        %avgTimeOccur of this event to match
        thisTimeOccur = avgTimeOccur(dayInd);
        if thisTimeOccur > endHour
            thisTimeOccur = thisTimeOccur - 24;
        end

        %Center these on the average occurrence time, if possible
        startTime = floor(thisTimeOccur - numHoursEvent / 2);
        ind1 = (startTime < startHour); %don't allow the event to start before the first hour of the day
        startTime(ind1) = ceil(startTime(ind1) + (startHour - startTime(ind1)));
        endTime = startTime + numHoursEvent - 1;
        ind2 = (endTime > endHour); %don't allow the event to end before the last hour of the day
        endTime(ind2) = floor(endTime(ind2) - (endTime(ind2) - endHour));

        %Now, determine the actual intensity and maximum duration
        thisIntense = thisDaily ./ (endTime - startTime + 1);
        maxHours = max(endTime(:) - startTime(:) + 1);

        %Create an hourly matrix
        hourlyAdd = repmat(blankGrid,[1,1,maxHours]);

        hourlyInd = 0;
        activeHours = false([1,numHours]);
        for n = 1:numHours
            loopHour = startHour + n - 1;
            ind = (loopHour >= startTime) & (loopHour <= endTime) & activeDaily;
            if any(ind(:))
                activeHours(n) = true;
                hourlyInd = hourlyInd + 1;
                loopGrid = blankGrid;
                loopGrid(ind) = thisIntense(ind);
                hourlyAdd(:,:,hourlyInd) = loopGrid;
            end
        end

        %Assert that the sum of this is reasonably close to the daily grid
        try
            test = abs(sum(hourlyAdd,3)-thisDaily);
            assert(all(test(:)<1e-9));
        catch
            disp(test);
            break
        end

        %Upscale the hourly matrix
        hourlyUpscale = zeros([maxHours,precipGeoLoc.rows*precipGeoLoc.cols],class(hourlyData));
        for n = 1:maxHours
            %Upscale it
            loopUpscale = cell_statistics(precipGeoLoc,modelGeoLoc,...
                hourlyAdd(:,:,n),'fast_mean','majority',upscalePrecalc);
            %Back-convert NaNs to their nanval
            loopUpscale(isnan(loopUpscale)) = str2double(dailyAttribs.nan);
            %Assign to the finalData array, after converting the class
            hourlyUpscale(n,:) = classFunc(loopUpscale(:));
        end

        %Using this matrix, determine which hours are active and record
        addIndex(thisIndRange(1):thisIndRange(2),2) = activeHours;

        %Store to the addData grid
        addData = cat(1,addData,hourlyUpscale);
    end
end
if ishandle(h); close(h); end

%--------------------------------------------------------------------------
%Add in the additional synthetic hourly data created from the daily weight
%maps
%--------------------------------------------------------------------------
%Insert the additional data into the final data array
finalComp = zeros(size(hourlyIndex,1),precipGeoLoc.rows*precipGeoLoc.cols,class(hourlyData));
finalComp(hourlyIndex(:,2) > 0,:) = finalData;
finalComp(addIndex(:,2) > 0,:) = addData;
finalData = finalComp;

%Remove zero rows and update the index
keepRows = any(finalData > 0,2);
hourlyIndex(:,2) = -1;
hourlyIndex(keepRows,2) = 1;
hourlyIndex(hourlyIndex(:,2) > 0, 2) = (1:sum(keepRows));
hourlyIndex(hourlyIndex(:,2) > 0, 3) = 1; %set lookup row
finalData = finalData(keepRows,:);

%--------------------------------------------------------------------------
%Save result
%--------------------------------------------------------------------------
tempFile = [workingDir,filesep,outFile];
tempGroup = ['/',sources.precip_hourly.name];

%Write the index and lookup arrays to the HDF5 file
import_write_dataset(tempFile,tempGroup,finalData,hourlyAttribs,hourlyIndex,false,gridLookup,false);
end
