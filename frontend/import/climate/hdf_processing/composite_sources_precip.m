function composite_sources_precip(modelHours,workingDir,outFilename,sources)
%COMPOSITE_SOURCES_PRECIP  Composites gauge and radar precipitation data
%   composite_sources_precip(modelHours,workingDir,outFilename,sources)
%
%   Descriptions of Input Variables:
%
%   Descriptions of Output Variables:
%   none, all are written to the specified output HDF5 file
%
%   Example(s):
%   >> composite_sources_climate
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-03-23
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Read in the data, indexes, and lookup grids
%--------------------------------------------------------------------------
gauge = h5datareadlhm([workingDir,filesep,sources.gauge.data],['/',sources.gauge.name],sources.method.rescale);
radar = h5datareadlhm([workingDir,filesep,sources.radar.data],['/',sources.radar.name],sources.method.rescale);

%--------------------------------------------------------------------------
%Check consistency of attributes
%--------------------------------------------------------------------------
attribFields = fieldnames(gauge.attribs);
for m = 1:length(attribFields)
    assert((strcmp(gauge.attribs.(attribFields{m}),radar.attribs.(attribFields{m}))) &&...
        (strcmp(gauge.attribs.(attribFields{m}),sources.precip.(attribFields{m}))) &&...
        (strcmp(gauge.attribs.(attribFields{m}),sources.precip.(attribFields{m}))),...
        'All attributes for like datatypes must be identical, check and must match the output source attribs');
end

%--------------------------------------------------------------------------
%Determine which dataset will be used
%--------------------------------------------------------------------------
%For each period of useRadar, use NEXRAD instead
[useRadarData,useRadarZeros] = deal(false([length(modelHours),1]));
radarDataInd = [];
for m = 1:size(sources.method.useRadar,1)
    indRange = bracket_index(radar.index(:,1),sources.method.useRadar(m,1),sources.method.useRadar(m,2)-1/48); %to make sure not to include the second date
    thisIndex = radar.index(indRange(1):indRange(2),:);
    thisDataIndex = thisIndex(thisIndex(:,2)>0,:);
    [thisRadarData,thisRadarInd] = ismember_dates(modelHours,thisDataIndex(:,1),4,false);
    thisRadarZeros = ismember_dates(modelHours,thisIndex(thisIndex(:,2)==-1,1),4,false);
    
    %Add to overall list
    useRadarData = useRadarData | thisRadarData;
    useRadarZeros = useRadarZeros | thisRadarZeros;
    radarDataInd = [radarDataInd;thisDataIndex(thisRadarInd(thisRadarData),2)]; %#ok<AGROW>
end

%Now, for gauge data, excluding NEXRAD periods
gaugeDataIndex = gauge.index(gauge.index(:,2)>0,:);
[useGaugeData,gaugeDataInd] = ismember_dates(modelHours,gaugeDataIndex(:,1),4,false);
useGaugeZeros = ismember_dates(modelHours,gauge.index(gauge.index(:,2)==-1,1),4,false);

useGaugeData = useGaugeData & ~(useRadarData | useRadarZeros); 
useGaugeZeros = useGaugeZeros & ~(useRadarData | useRadarZeros);
gaugeDataInd = gaugeDataIndex(gaugeDataInd(useGaugeData),2);

%--------------------------------------------------------------------------
%Build the index table
%--------------------------------------------------------------------------
index = [modelHours,zeros(size(modelHours))+NaN,zeros(size(modelHours))];
  
%Fill in the data rows field
index((useRadarData | useGaugeData),2) = (1:sum(useGaugeData | useRadarData));
index((useRadarZeros | useGaugeZeros),2) = -1;
%--------------------------------------------------------------------------
%Build a composite data table
%--------------------------------------------------------------------------
%Make the empty composite data table
compositeData = zeros(sum(useGaugeData | useRadarData),max(size(radar.data,2),size(gauge.data,2)),sources.precip.class) + NaN;

%Fill with radar data
compositeData(index(useRadarData,2),1:size(radar.data,2)) = radar.data(radarDataInd,:);

%Fill with gauge data
compositeData(index(useGaugeData,2),1:size(gauge.data,2)) = gauge.data(gaugeDataInd,:);

%Clear up some arrays to save memory
clear gaugeData radarData

%--------------------------------------------------------------------------
%Build lookup array, fill in index
%--------------------------------------------------------------------------
%Check to see if the lookup tables are identical
if (numel(gauge.lookup) == numel(radar.lookup)) && all(gauge.lookup(:) == radar.lookup(:))
    index(:,3) = 1;
    lookup = gauge.lookup;
else
    index(useGaugeData,3) = gaugeDataIndex(gaugeDataInd,3);
    lookup = gauge.lookup;
    index(useRadarData,3) = size(gauge.lookup,3)+1;
    lookup = cat(3,lookup,radar.lookup);
end

%--------------------------------------------------------------------------
%Build lookup array, fill in index
%--------------------------------------------------------------------------
%Error check
assert(~any(index(index(:,2)>0,3)==0),'Some lookup entries for precip events are empty')

%----------------------------------------------------------------------
%Save the data to an intermediate HDF5 file
%----------------------------------------------------------------------
tempFile = [workingDir,filesep,outFilename];
tempGroup = ['/',sources.precip.name];

import_write_dataset(tempFile,tempGroup,compositeData,gauge.attribs,index,false,lookup,sources.method.rescale);
end
