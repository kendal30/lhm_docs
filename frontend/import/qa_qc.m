function qa_qc(workingDir,outFile,sources)
%QA_QC  Perform quality analysis and quality control cleaning of input data
%   qa_qc_climate()
%
%   Clean input data according to one of these methods:
%   manual_outlier:     Intervals of outlier data are manually specified
%   correct_range:      Manually adjust the range of a dataset by
%               truncation
%   flags:      Though not presently implemented, this function would read
%               in a dataset along with a set of qa/qc flags and then 
%
%   Descriptions of Input Variables:
%   none, presently
%
%   Descriptions of Output Variables:
%   none, all are written to the specified output hdf5 file.
%
%   Example(s):
%   >> qa_qc
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-07
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Clean source data using specified methods, save to intermediate HDF5 file
%--------------------------------------------------------------------------
datasets = fieldnames(sources);
for m = 1:length(datasets)
    %Load the data
    tempFile = [workingDir,'\',sources.(datasets{m}).file];
    tempGroup = ['/',sources.(datasets{m}).name];
    [data,attribs] = h5dataread(tempFile,[tempGroup,'/data'],sources.(datasets{m}).scaleTrans);
    index = h5dataread(tempFile,[tempGroup,'/index']);
    
    %Load the lookup table, or stationsXY table
    stationsXY = h5dataread(tempFile,[tempGroup,'/stationsXY'],false);
    if islogical(stationsXY)
        lookup = h5dataread(tempFile,[tempGroup,'/lookup'],false);
    else
        lookup = false;
    end
    
    %Load the precision information, if available
    precision = h5dataread(tempFile,[tempGroup,'/precision'],false);
    
    %Determine the nanVal
    if sources.(datasets{m}).scaleTrans
        nanVal = NaN;
    else
        nanVal = str2double(attribs.nan);
    end
    
    %Process the dataset
    for n = 1:length(sources.(datasets{m}).method)
        %Send the data to the qaqc function
        tempMethod = str2func(sources.(datasets{m}).method{n}.name);
        [index,data,stationsXY] = tempMethod(index,data,precision,stationsXY,sources.(datasets{m}).method{n},nanVal);
        
        %Clean up the data, index, and stationsXY tables
        [index,data,precision,stationsXY] = remove_nan_rows_cols(index,data,precision,stationsXY,nanVal);
    end
        
    %Save the data to an intermediate HDF5 file
    tempFile = [workingDir,'\',outFile];
    import_write_dataset(tempFile,tempGroup,data,attribs,index,stationsXY,lookup,sources.(datasets{m}).scaleTrans);
end
end

%--------------------------------------------------------------------------
%Helper functions
%--------------------------------------------------------------------------
function [index,data,precision,stationsXY] = remove_nan_rows_cols(index,data,precision,stationsXY,nanVal)       
%Now, check to see which columns or rows are all NaN
if isnan(nanVal)
    keepRows = any(~isnan(data),2);
    keepCols = any(~isnan(data),1);
else
    keepRows = any(data~=nanVal,2);
    keepCols = any(data~=nanVal,1);
end
data = data(keepRows,:);
if ~islogical(stationsXY) %only remove columns if this is station data we're talking about
    data = data(:,keepCols);
end

%Change the index for those rows to NaN
events = (index(:,end)>0); %some indeces have 3 columns, others 2
tempIndex = index(events,:);
tempIndex(keepRows,end) = (1:size(data,1))'; 
tempIndex(~keepRows,end) = NaN;
index(events,:) = tempIndex;

%Remove those entries from the stationsXY table
if ~islogical(stationsXY)
    stationsXY = stationsXY(keepCols,:);
    stationsXY(:,end) = (1:size(stationsXY,1));
end

%Remove bad cols from the precision table
if ~islogical(precision)
    precision = precision(keepRows,:);
    if ~islogical(stationsXY)
        precision = precision(:,keepCols);
    end
end
end

%--------------------------------------------------------------------------
%QA/QC methods
%--------------------------------------------------------------------------
function [index,data,stationsXY] = manual_outlier(index,data,precision,stationsXY,method,nanVal) %#ok<INUSL,DEFNU>
%Identify which stations columns to work on
[stationsCheck,stationCols] = ismember(method.stations,stationsXY(:,1));
stationCols(~stationsCheck) = [];

%Loop through the stations
for m = 1:length(method.stations)
    %Loop through the intervals
    for n = 1:size(method.intervals{m},1)
        %Examine the index to determine the row indeces of data within the
        %specified outlier removal intervals
        indBracket = bracket_index(index(:,1),method.intervals{m}(n,1),method.intervals{m}(n,2));
        %Determine the data indices of the hours within the bracketed
        %interval
        dataInd = index(indBracket(1):indBracket(2),2);
        dataInd(isnan(dataInd)) = [];
        %Blank out those data values with NaNs
        data(dataInd,stationCols(m)) = nanVal;
    end
end

end

function [index,data,stationsXY] = correct_range(index,data,precision,stationsXY,method,nanVal) %#ok<INUSL,DEFNU>
if method.discard
    belowRange = data < method.range(1);
    aboveRange = data > method.range(2);
    data(belowRange) = nanVal;
    data(aboveRange) = nanVal;   
else
    %Truncate the dataset in one fell swoop
    [data] = limit_vals(method.range,data);
end
end

function [index,data,stationsXY] = exceeds_threshold(index,data,precision,stationsXY,method,nanVal) %#ok<INUSL,DEFNU>
%This function checks to see if any values in an hour exceed a threshold,
%if so then either that single value is thrown away, or the entire hour is
%discarded according to the option 'discardHour'

if method.discardHour
    %Identify the rows in the data that exceed the maximum
    test = any(data > method.threshold,2);
    
    %Set those row values to NaN
    data(test,:) = nanVal;
else
    %Identify the individual values that exceed the maximum
    test = data > method.threshold;
    %Set those data values to NaN
    data(test) = nanVal;
end
end

function [index,data,stationsXY] = station_compare_zeros(index,data,precision,stationsXY,method,nanVal) %#ok<INUSL,DEFNU>
%This function compares each station to all others, and removes it if the
%fraction of times the station records 0 when all other stations do not is
%greater than the specified threshold

%Get the number of stations
numStations = size(data,2);
badStations = false(numStations,1);
for m = 1:numStations
    this = data(:,m);
    others = data;
    others(:,m) = [];
    loneZeros = sum((this==0) & (nansum(others>0,2)==(nansum(~isnan(others),2))));
    thisCount = sum(~isnan(this));
    if (loneZeros/thisCount) > method.threshold
        badStations(m) = true;
    end
end

%Remove bad stations
data(:,badStations) = nanVal;
end

function [index,data,stationsXY] = station_compare_std(index,data,precision,stationsXY,method,nanVal) %#ok<INUSL,DEFNU>
%This function compares each station to all others, and removes individual
%observation values if that value lies outside the cross-station mean by a
%specified amount

numStations = size(data,2);
for m = 1:numStations
    this = data(:,m);
    others = data;
    others(:,m) = [];
    compareStd = nanstd(others,2);
    compareMean = nanmean(others,2);
    compareNumPres = sum(~isnan(others),2);
    thisSigma = abs(this - compareMean) ./ compareStd;
    ind = (thisSigma > method.threshold) & ~isnan(thisSigma) & (compareNumPres > method.minNum);
    data(ind,m) = nanVal;
end
end

function [index,data,stationsXY] = precision_threshold(index,data,precision,stationsXY,method,nanVal) %#ok<DEFNU>
%This function stations values if a precision threshold is not met

%Identify stations with precision below the threshold
lowPrecision = (precision > method.threshold);

%Either throw out all values, or just those below the precision threshold
if method.discardAll
    omitFlag = lowPrecision;
else
    omitFlag = lowPrecision & (data <= precision);
end
%Remove those station values
data(omitFlag) = nanVal;
end

function [index,data,stationsXY] = diff_outlier(index,data,precision,stationsXY,method,nanVal) %#ok<DEFNU,INUSL>
%Removes temporal outliers from grids, also remove manually-identified bad
%grids
inClass = class(data);

%Need the data to be at least int16 here, instead of uint8
data = int16(data);

%Trim the index to match available data
index = index(index(:,2)>0,:);

%Remove dates specified for manual removal
if ~isempty(method.remove)
    removeDates = datenum(method.remove,'mm/dd/yyyy');
    indRemove = ismember_dates(index(:,1),removeDates,3,false);
    keepData = ~indRemove;
    data = data(index(keepData,2),:);
    index = index(keepData,:);
end

%Handle NaNs properly
testNan = (data == nanVal) | (data == 0); %be careful with this zero here!
test = ~all(testNan,1) & any(testNan,1) & sum(testNan,1)<size(testNan,1)/3; %want only columns that aren't all nan
testFind = find(test);
testIndex = index(index(:,2)>0,:);
for m = 1:length(testFind)
    tempData = single(data(:,testFind(m)));
    tempIndex = testIndex(:,1);
    tempNan = testNan(:,testFind(m));
    tempData = tempData(~tempNan);
    tempIndex = tempIndex(~tempNan);
    tempData = interp1(tempIndex,tempData,testIndex(:,1));
    data(:,testFind(m)) = int16(tempData);
end

%Compare the current value to the two previous values
dataDiff = diff(data,1);
prev = dataDiff(1:end-1,:);
next = dataDiff(2:end,:);
clear dataDiff %need to preserve memory here

%First, remove values that are less than a minimum value
%Determine if the datapoint values are less than a minimum value
remove = (data(2:end-1,:) < method.min);
removeFull = [false(1,size(remove,2));remove;false(1,size(remove,2))];
prevVal = data(1:end-2,:);
data(removeFull) = (next(remove) + prev(remove))/2 + prevVal(remove);

%Now, apply the remaining filter
%Compare the current value to the two previous values
dataDiff = diff(data,1);
prev = dataDiff(1:end-1,:);
next = dataDiff(2:end,:);
clear dataDiff %need to preserve memory here

%Find cells with differences that are unequal in sign, this means that the
%current datapoint is not in between the neighboring (in time) two
signsDiffer = xor(sign(prev)>0,sign(next)>0);

%Calculate the max difference between the previous and next values
minDiff = min(abs(prev),abs(next));

%Determine if the mean difference, as a proportion of current LAI, exceeds
%a threshold
exceeds = (minDiff > method.threshold);

%Datapoints to remove have to both exceed the threshold and have differing
%signs on the diff
remove = (exceeds & signsDiffer);

%Check to see if more than half of the datapoints in any given row are
%marked "remove", if so, remove the entire grid.
numCols = size(data,2);
keepRows = sum(remove,2) < (numCols/2);
%Now, trim all of the necessary arrays
data = data([true;keepRows;true],:);
index = index([true;keepRows;true],:);
%The second column of index now needs to be renumbered
index(:,2) = (1:size(data,1));

%Now, recalculate everything after bad rows are removed
dataDiff = diff(data,1);
prev = dataDiff(1:end-1,:);
next = dataDiff(2:end,:);
clear dataDiff %need to preserve memory here

%Find cells with differences that are unequal in sign, this means that the
%current datapoint is not in between the neighboring (in time) two
signsDiffer = xor(sign(prev)>0,sign(next)>0);

%Calculate the max difference between the previous and next values
minDiff = min(abs(prev),abs(next));

%Determine if the mean difference, as a proportion of current LAI, exceeds
%a threshold
exceeds = (minDiff > method.threshold);

%Datapoints to remove have to both exceed the threshold and have differing
%signs on the diff
remove = (exceeds & signsDiffer);

%Remove the datapoints and interpolate in 1 dimension across the remaining
%points
%We'll be working with the entire data matrix, so pad remove with 0s
if method.fill
    removeFull = [false(1,size(remove,2));remove;false(1,size(remove,2))];
    prevVal = data(1:end-2,:);
    data(removeFull) = (next(remove) + prev(remove))/2 + prevVal(remove);
end

reclass = str2func(inClass);
data = reclass(data);
end