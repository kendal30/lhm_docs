function [groundwater,structure] = initialize_modflow(configFiles,structure)
% INITIALIZE_MODFLOW  Initializes the MODFLOW input data
%   groundwater = initialize_modflow(structure)
% 
%   Descriptions of Input Variables:
%   structure: LHM structure array
% 
%   Descriptions of Output Variables:
%   groundwater: structured array used by LHM
% 
%   Example(s):
%   >> initialize_modflow
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

% For clarity
paths = structure.paths;

% Fetch the input parameters
groundwaterStruct = configFiles.model.groundwater;
groundwaterParams = configFiles.params.groundwater;
groundwater = merge_struct(groundwaterStruct, groundwaterParams);

% Load stored grids
[groundwater.grids,header] = load_grid_params(paths,'groundwater','STAT');

% Finish preparing stored grids, checks NaNs -- this might fail if these
% grids are not present
if groundwater.package.evt.active
    groundwater.grids.et_surf(isnan(groundwater.grids.et_surf)) = 0;
    groundwater.grids.et_extinct_depth(isnan(groundwater.grids.et_extinct_depth)) = 0;
end
groundwater.grids.model_top(isnan(groundwater.grids.model_top)) = 0;
groundwater.grids.bottom(isnan(groundwater.grids.bottom)) = 0;
groundwater.grids.start_heads(isnan(groundwater.grids.start_heads)) = 0;
if groundwater.package.drn.active
    groundwater.grids.drn_elev(isnan(groundwater.grids.drn_elev)) = 0;
end

% Add the model start and end dates, and model name, to the groundwater structure array
groundwater.structure.start_date = structure.dates(1);
groundwater.structure.end_date = structure.dates(end);
groundwater.structure.model_name = [structure.group_name,'_',structure.run_name];
groundwater.structure.optim_enabled = structure.groundwater_optim_enabled;

% Build model stress periods
[groundwater.structure,groundwater.package] = build_stress_periods(groundwater.structure,groundwater.package);

% Build the output control arrays
[groundwater.structure] = build_oc_arrays(groundwater.structure,'save_budget');
[groundwater.structure] = build_oc_arrays(groundwater.structure,'save_head');

% Convert time and length units into strings
groundwater.structure.timeunits = parse_timeunits(groundwater.structure.timeunits);
groundwater.structure.lenunits = parse_lenunits(groundwater.structure.lenunits);

% Load well list and schedule if present, finish inputs to that file
if groundwater.package.wel.active
    % Call the preparation function
    groundwater.package.wel.inputs = init_wells(paths,groundwater.structure,groundwater.package.wel.inputs,...
        header,groundwater.grids.model_top,groundwater.grids.bottom);
end

% Clean up the params handling
% Remove empty params
fields = fieldnames(groundwater.params);
for m = 1:length(fields)
    if isempty(groundwater.params.(fields{m}))
        groundwater.params = rmfield(groundwater.params,fields{m});
    end
end

% Populate the mult field with values from optim, if active
if isfield(groundwater,'optim')
    groundwater.mult = groundwater.optim.mult;
    multFields = fieldnames(groundwater.mult);
    for m = 1:length(multFields)        
        % Set the 'curr' field, which will be used to actually write the multiplier values
        if ~any(strcmpi('curr',multFields))
            groundwater.mult.(multFields{m}).curr = groundwater.mult.(multFields{m}).start;
        end
    end
else
    origMult = groundwater.mult;
    groundwater.mult = struct();
    % Set the 'curr' field, which will be used to actually write the multiplier values
    multFields = fieldnames(origMult);
    for m = 1:length(multFields)
        groundwater.mult.(multFields{m}).curr = cell2mat(origMult.(multFields{m}));
    end
end


% Update the paths structure to include the optimization paths
if groundwater.structure.optim_enabled
    % Specify the optimization files
    paths.init.optim.notebook = 'modflow_optimization.mlx';
    paths.init.optim.function = 'optim_wrapper.m';
    paths.init.optim.helper = cell(2,1);
    paths.init.optim.helper{1} = [groundwater.package.bas.initFunc,'.m'];

    packageNames = {'upw','lpf'};
    for m = 1:length(packageNames)
        if groundwater.package.(packageNames{m}).active
            paths.init.optim.helper{2} = [groundwater.package.(packageNames{m}).initFunc,'.m'];
        end
    end
    % Update structure to return it
    structure.paths = paths;
end


% Deactive the ZON file if not needed
fields = fieldnames(groundwater.params);
if isempty(fields)
    groundwater.package.zon.active = false;
end

% Clean up the nan handling
grids = fieldnames(groundwater.grids);
for m = 1:length(grids)
    test = isnan(groundwater.grids.(grids{m}));
    groundwater.grids.(grids{m})(test) = groundwater.structure.nanVal;
end

% Finish building the package array
groundwater.package = init_file_names(groundwater.structure.model_name,groundwater.package);

% Place this entire array as the input to the nam package
groundwater.package.nam.inputs = groundwater.package;


%--------------------------------------------------------------------------
% Observations Preparation
%--------------------------------------------------------------------------
% Load the observations
obsTable = h5obstableread(paths.init.obs,'point/head',true); %speciy true to return a structured array instead of a table

% Build the head obs array
if ~islogical(obsTable)
    groundwater.package.hob.inputs = head_observations_prep(groundwater.package.hob.inputs,obsTable,...
        header,groundwater.grids.bottom,groundwater.grids.model_top,groundwater.structure.stress_step_table);
end

%--------------------------------------------------------------------------
% Create MODFLOW Input Files
%--------------------------------------------------------------------------
execFile = which(groundwater.structure.modflow_exec);
copyfile(execFile,[paths.init.groundwater,filesep,groundwater.structure.modflow_exec]);

groundwater = write_input_files(paths,groundwater);

% Remove the grids field from groundwater to save memory--unless optimizing
if ~groundwater.structure.optim_enabled
    groundwater = rmfield(groundwater,'grids');
end
end

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Sub-functions
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Write output files
function [groundwater] = write_input_files(paths,groundwater)
% Write the MODFLOW files
packages = groundwater.package;
packageNames = fieldnames(packages);
for m = 1:length(packageNames)
    if packages.(packageNames{m}).active && packages.(packageNames{m}).init
        thisFunc = str2func(packages.(packageNames{m}).initFunc); % build the function handle to write this input file
    	packages.(packageNames{m}) = thisFunc(paths.init.groundwater,packages.(packageNames{m}),groundwater.structure,...
            groundwater.params,groundwater.constants,groundwater.mult,groundwater.grids);
    end
end
% Update the packages part of groundwater
groundwater.package = packages;

end

%--------------------------------------------------------------------------
% Build the inputs for the HOB file
%--------------------------------------------------------------------------
function [structure] = head_observations_prep(structure,obsTable,gridHeader,...
    layBot,top,stressTable)
% Determine the total number of points
structure.nh = length(obsTable.loc_id);

% Then, add the OBSNAM
structure.obsnam = obsTable.loc_id;
% Make sure the name does not exceed 12 characters in length
for m = 1:structure.nh
    if length(structure.obsnam{m}) > 12
        structure.obsnam{m} = structure.obsnam{m}(1:12);
    end
end

% Next, assign the head observations and the statistic
structure.hobs = obsTable.value;
structure.statistic = obsTable.weight;

% Initialize the arrays to be computed within loops
[structure.row,structure.column,structure.layer,structure.plot_symbol,structure.irefsp,...
    structure.coff,structure.roff,structure.toffset] = deal(zeros(structure.nh,1));

% Determine the plot symbols
% List the unique input sets
sets = unique(obsTable.set_name);
for m = 1:length(sets)
    thisSet = strcmp(sets{m},obsTable.set_name);
    structure.plot_symbol(thisSet) = m;
end

% Set up the model grid arrays
modelX = gridHeader.left + (0:gridHeader.cols)'*gridHeader.cellsizeX;
modelY = gridHeader.top - (0:gridHeader.rows)'*gridHeader.cellsizeY;

% Determine the i,j for all of the input points
for m = 1:structure.nh
    testX = find((obsTable.x(m) >= modelX(1:end-1)) & (obsTable.x(m) < modelX(2:end)),1,'first');
    testY = find((obsTable.y(m) <= modelY(1:end-1)) & (obsTable.y(m) > modelY(2:end)),1,'first');
    if ~isempty(testX),structure.column(m) = testX;end
    if ~isempty(testY),structure.row(m) = testY;end
end

% Determine the row and column offsets for each point, -0.5 and +0.5
% indicate the edge of each cell
testX = structure.column > 0;
structure.coff(testX) = -0.5 + (obsTable.x(testX) - modelX(structure.column(testX)))/gridHeader.cellsizeX;
testY = structure.row > 0;
structure.roff(testY) = -0.5 + (modelY(structure.row(testY)) - obsTable.y(testY))/gridHeader.cellsizeY;

% Determine the reference stress period
for m = 1:structure.nh
    testT = find((obsTable.time_start(m) >= stressTable(:,3)) & ...
        (obsTable.time_start(m) < stressTable(:,4)),1,'first');
    if ~isempty(testT),structure.irefsp(m) = stressTable(testT,1);end
end

% Determine toffset, in days (actually, the product of this and the
% multiplier tomulth should yield model units)
stressPeriodsInd = [true;logical(diff(stressTable(:,1)))];
stressPeriodsStart = stressTable(stressPeriodsInd,3);
testT = structure.irefsp > 0;
structure.toffset(testT) = obsTable.time_start(testT) - stressPeriodsStart(structure.irefsp(testT));

% Now, determine the layer
testInd = testX & testY;
obsInd = sub2ind([gridHeader.rows,gridHeader.cols],structure.row(testInd),structure.column(testInd));
prevLayer = false(size(obsInd));
numLay = size(layBot,3);

% For now, average the z_top and z_bot values for each observation
% in the future, this could made to span multiple layers
obsTable.z = (obsTable.z_top + obsTable.z_bot)/2;

for m = 1:numLay
    % Calculate the depth of the bottom of this layer
    botDepth = top - layBot(:,:,m);
    % Check to see if all observations are shallower than this layer bottom
    shallower = -obsTable.z(testInd) <= botDepth(obsInd);
    % An observation is in this layer if it is shallower than the current
    % bottom, and not in a previous layer
    inLayer = ~prevLayer & shallower;
    % For all points in this layer, assign the layer number
    structure.layer(inLayer) = m;
    % Update prevLayer to include those points in this layer
    prevLayer = prevLayer | inLayer;
end

% Finally, throw out any points with invalid col, row, or layer values
badRow = (structure.row < 1) | (structure.row > gridHeader.rows);
badCol = (structure.column < 1) | (structure.column > gridHeader.cols);
badLay = (structure.layer < 1) | (structure.layer > numLay);
badTime = (structure.irefsp < 1) | (structure.irefsp > max(stressTable(:,1)));

% Bad points are those that meet any of these criteria
badPoints = badRow | badCol | badLay | badTime;
goodPoints = ~badPoints;

% Modify the output arrays
structure.nh = sum(goodPoints);
fieldList = {'obsnam','layer','row','column','irefsp','toffset','roff','coff',...
    'hobs','statistic','plot_symbol'};
for m = 1:length(fieldList)
    structure.(fieldList{m}) = structure.(fieldList{m})(goodPoints);
end
end

%--------------------------------------------------------------------------
% Prepare wells
%--------------------------------------------------------------------------
function [inputs] = init_wells(paths,structure,inputs,gridHeader,layTop,layBot)
% Load the schedule
    wellIndex = h5dataread(paths.input.landscape,'/schedules/well/index'); % columns are start, end, scheduleRow
    wellList = h5dataread(paths.input.landscape,'/schedules/well/list'); % columns are well_ID, field_ID, model_x, model_y, screen_top (elev), screen_bot (elev)
    wellSchedule = h5dataread(paths.input.landscape,'/schedules/well/schedule'); % rows are pump interval, columns are well pumped rate during interval, assumes daily or finer

    % Match model x and y and screen to layer, row, and column
    % First, match x and y to row, and col
    % Set up the model grid arrays
    modelX = gridHeader.left + (0:gridHeader.cols)'*gridHeader.cellsizeX;
    modelY = gridHeader.top - (0:gridHeader.rows)'*gridHeader.cellsizeY;

    % Determine the i,j for all of the input points
    wellIJK = zeros(size(wellList,1),3); % (row,col,lay)
    outsideDomain = false(size(wellList,1));
    for m = 1:size(wellList,1)
        testX = find((wellList(m,3) >= modelX(1:end-1)) & (wellList(m,3) < modelX(2:end)),1,'first');
        testY = find((wellList(m,4) <= modelY(1:end-1)) & (wellList(m,4) > modelY(2:end)),1,'first');
        if ~isempty(testX)
            wellIJK(m,2) = testX;
        else
            outsideDomain(m) = true;
        end
        if ~isempty(testY)
            wellIJK(m,1) = testY;
        else
            outsideDomain(m) = true;
        end
    end

    % Remove any wells that are outside of the domain
    wellList = wellList(~outsideDomain,:);
    wellSchedule = wellSchedule(:,~outsideDomain);
    wellIJK = wellIJK(~outsideDomain,:);

    % Determine stress periods
    stressTable = structure.stress_step_table;
    wellScheduleSP = zeros(structure.nper,size(wellSchedule,2));
    for m = 1:structure.nper
        stressStart = stressTable(sum(structure.perlen(1:m))-structure.perlen(m)+1,3);
        stressEnd = stressTable(sum(structure.perlen(1:m)),4);
        thisInd = (wellIndex(:,1) >= stressStart) & (wellIndex(:,2) <= stressEnd);
        sumInd = unique(wellIndex(thisInd,3));
        sumInd(sumInd==-1) = [];
        if ~isempty(sumInd)
            wellScheduleSP(m,:) = sum(wellSchedule(sumInd,:),1)/structure.perlen(m); % average pump rate
        end
    end

    % Remove wells that never pump during the model period
    inactiveWells = sum(wellScheduleSP,1) == 0;
    wellList = wellList(~inactiveWells,:);
    wellScheduleSP = wellScheduleSP(:,~inactiveWells);
    wellIJK = wellIJK(~inactiveWells,:);

    % Then, match layers and split wells as necessary in order to spread
    % across multiple layers
    [layScreenTop,layScreenBot] = deal(zeros(size(wellList,1),1));
    outsideDomain = false(size(layScreenTop));
    wellScheduleSPSplit = [];
    wellIJKSplit = [];
    for m = 1:size(wellList,1)
        % Determine the layer of the top of the well screen
        layScreenTop(m) = find(squeeze(wellList(m,5) <= layTop(wellIJK(m,1),wellIJK(m,2),:)) & ...
            squeeze(wellList(m,5) >= layBot(wellIJK(m,1),wellIJK(m,2),:)));
        % Now the bottom of the well screen
        layScreenBot(m) = find(squeeze(wellList(:,6) <= layTop(wellIJK(m,1),wellIJK(m,2),:)) & ...
            squeeze(wellList(:,6) >= layBot(wellIJK(m,1),wellIJK(m,2),:)));
        if isempty(layScreenTop(m)) && ~isempty(layScreenBot(m)) % case where top is screened above the top of the highest layer of the aquifer
            layScreenTop(m) = 1;
            wellList(m,5) = layTop(wellIJK(m,1),wellIJK(m,2),1);
        elseif ~isempty(layScreenTop(m)) && isempty(layScreenBot(m)) % case where bottom is screened above the bottom of the lowest layer of the aquifer
            layScreenBot(m) = size(layBot,3);
            wellList(m,5) = layBot(wellIJK(m,1),wellIJK(m,2),end);
        elseif isempty(layScreenTop(m)) && isempty(layScreenBot(m))
            if all(wellList(m,5:6) > layTop(wellIJK(m,1),wellIJK(m,2),1)) % well not screened in model
                outsideDomain(m) = true;
            elseif all(wellList(m,5:6) < layBot(wellIJK(m,1),wellIJK(m,2),end)) % well not screened in model
                outsideDomain(m) = true;
            else % well spans entire model, truncate to match model
                layScreenTop(m) = 1;
                layScreenBot(m) = size(layBot,3);
                wellList(m,5) = layTop(wellIJK(m,1),wellIJK(m,2),1);
                wellList(m,6) = layBot(wellIJK(m,1),wellIJK(m,2),end);
            end
        end
        if ~outsideDomain(m)
            % Now, split up the pumping between layers
            if layScreenTop(m)~=layScreenBot(m)
                error('This part of the code has not been finished');
                % Need to update wellScheduleSP, wellIJK to reflect
                % split, also create new arrays to append later
            else
                wellIJK(m,3) = layScreenTop(m);
            end
        end
    end

    % Remove wells that are outside the domain
    wellScheduleSP = wellScheduleSP(:,~outsideDomain);
    wellIJK = wellIJK(~outsideDomain,:);

    % Append on the new splits
    wellScheduleSP = [wellScheduleSP,wellScheduleSPSplit];
    wellIJK = [wellIJK;wellIJKSplit];

    % Prepare outputs of this function (inputs to wel_write.m)
    inputs.IJK = wellIJK;
    inputs.Q = -wellScheduleSP; % negative value indicates pumping
    % Count active number of wells
    inputs.ITMP = sum(wellScheduleSP>0,2);
    inputs.MXACTW = max(inputs.ITMP);
end

%--------------------------------------------------------------------------
% Build Output Control Arrays
%--------------------------------------------------------------------------
function [structure] = build_oc_arrays(structure,arrayType)
if ischar(structure.(arrayType)) || ~iscell(structure.(arrayType){1})
    % Save the specification information
    specData = structure.(arrayType);

    % Create the cellSpec array
    cellSpec = cell(structure.nper,1);

    % By default, all stress periods are zeros
    saveDefault = 0;
    if ischar(specData)
        if strcmp('all',specData)
            saveDefault=1;
        end
    end

    % Fill the cellSpec array with 0s, unless string option 'all' is
    % specified
    for i=1:structure.nper
        cellSpec{i}=repmat(saveDefault,1,structure.nstp(i)); % 
    end

    % Now, parse specData
    if ischar(specData)
        switch specData
            case 'first'
                cellSpec{1}(1)=1;
            case 'last'
                cellSpec{end}(end)=1;
            case 'all'
            otherwise
                error(['Unrecognized ',arrayType,' string specification'])
        end
    elseif iscell(specData)
        specData = sortrows(datenum(specData,'mm/dd/yyyy'));
        for m = 1:length(specData)
            tableInd = (specData(m) >= structure.stress_step_table(:,3)) & ...
                (specData(m) < structure.stress_step_table(:,4));
            cellSpec{structure.stress_step_table(tableInd,1)}(structure.stress_step_table(tableInd,2)) = 1;
        end
    end

    % Save to the structure array
    structure.(arrayType) = cellSpec;
elseif iscell(structure.(arrayType))
    % This must already contain a complete specification, verify
    assert(length(structure.(arrayType))==structure.nper,[arrayType,' array must be of length nper']);

    % Loop through and determine if lengths of timesteps are correct
    for m = 1:nper
        structure.(arrayType){m} = datenum(structure.(arrayType){m},'mm/dd/yyyy');
        assert(length(structure.(arrayType){m})==structure.nstp(m),['Numeric arrays in ',arrayType,' cell array must be of length nstp(per)']);
    end
else
    error(['Unrecognized ',arrayType,' specification'])
end
end

%--------------------------------------------------------------------------
% Build stress periods for DIS
%--------------------------------------------------------------------------
function [structure,package] = build_stress_periods(structure,package)
% For convenience
perlen = structure.perlen;
nstp = structure.nstp;
nper = structure.nper;
simulation_type = structure.simulation_type;
tsmult = structure.tsmult;
start_date = structure.start_date;
end_date = structure.end_date;
timeunits_str = structure.timeunits;


if ischar(perlen) % Specified by a key word
    perlen_str=perlen;
    nstp_str=nstp;

    stress_period_type={};
    nper=0;
    nstp=[];

    switch simulation_type
        case 'RAMP'
            % build SS portion
            [stress_period_type,nper,nstp,perlen]=...
                SS_stress_period_build(stress_period_type,nper,nstp,1);
            % build TR portion
            [stress_period_type,nper,nstp,perlen]=...
                TR_stress_period_build_string(stress_period_type,nper,nstp,perlen,...
                start_date,end_date,perlen_str,nstp_str,timeunits_str);
        case 'SS'
            [stress_period_type,nper,nstp,perlen]=...
                SS_stress_period_build(stress_period_type,nper,nstp,end_date-start_date+1);
        case 'TR'
            perlen=[];
            [stress_period_type,nper,nstp,perlen]=...
                TR_stress_period_build_string(stress_period_type,nper,nstp,perlen,...
                start_date,end_date,perlen_str,nstp_str,timeunits_str);
    end
elseif ~strcmp(simulation_type,'SS') && (length(perlen)==1) % Specifying a uniform length stress period
    stress_period_length=perlen;
    timesteps_per_SP=nstp;

    stress_period_type={};
    nper=0;
    nstp=[];
    perlen=[];
    switch simulation_type
        case 'RAMP'
            % build SS portion
            [stress_period_type,nper,nstp,perlen]=...
                SS_stress_period_build(stress_period_type,nper,nstp,1);
            % build TR portion
            [stress_period_type,nper,nstp,perlen]=...
                TR_stress_period_build_numeric(stress_period_type,nper,nstp,perlen,...
                start_date,end_date,stress_period_length,timesteps_per_SP);
%         case 'SS'
%             [stress_period_type,nper,nstp,perlen]=...
%                 SS_stress_period_build(stress_period_type,nper,nstp,perlen);
        case 'TR'
            [stress_period_type,nper,nstp,perlen]=...
                TR_stress_period_build_numeric(stress_period_type,nper,nstp,perlen,...
                start_date,end_date,stress_period_length,timesteps_per_SP);
    end
end

% Update the structure array
structure.stress_period_type = stress_period_type;
structure.nper = nper;
structure.nstp = nstp;
structure.perlen = perlen;

% Process the tsmult array
if ischar(structure.tsmult)
    if strcmp(tsmult,'automatic')
        structure.tsmult = ones(1,nper);
    else
        error('Unrecognized option of "tsmult"');
    end
end

% Create the time step and stress period table, note, this calculation works
% only if the timeunits are days
% [stress period, time step, start date, end date]
stress_step_table = zeros(sum(nstp), 4);
index = 0;
for h=1:nper
    for i=1:nstp(h)
        index = index + 1;
        stress_step_table(index,1) = h;
        stress_step_table(index,2) = i;
        if index == 1
            stress_step_table(index,3) = start_date;
        else
            stress_step_table(index,3) = stress_step_table(index-1,4);
        end
        stress_step_table(index,4) = stress_step_table(index,3) + perlen(h) / nstp(h);
    end
end
structure.stress_step_table = stress_step_table;
end

%--------------------------------------------------------------------------
% Build transient stress periods based on string inputs
%--------------------------------------------------------------------------
function [stress_period_type,nper,nstp,perlen]=...
    TR_stress_period_build_string(stress_period_type,nper,nstp,perlen,...
    model_start_time,model_end_time,perlen_str,nstp_str,timeunits_str)

% Parse the dates into date vectors
[Ys,~,~,~,~,~]=datevec(model_start_time); % #ok<NASGU>
[Ye,~,~,~,~,~]=datevec(model_end_time); % #ok<NASGU>

switch lower(perlen_str)
    case 'years' % this should work, but it's not yet tested
        % calculate the number of years in the model
        nper_TR=Ye-Ys+1;

        % Calculate an array that represents each year in the model
        model_years=(Ys:1:Ye);
        % Check if the timeunits are in years, if so, this becomes
        % trivial
        if strcmp(timeunits_str,'years')
            perlen=model_years;
            nstp=perlen;
        else
            % Decide if the model year is a leap year
            leap_year=((rem(model_years,4)==0) & (rem(model_years,100)~=0)) | (rem(model_years,400)==0);
            % Create an array for the number of days in each model year
            temp_days=(leap_year.*366)+(~leap_year*365);
            temp_perlen=temp_days;
            % Shorten the first and last stress periods if necessary
            % Get the Julian Day of the start and end dates
            start_julian=date2doy(model_start_time);
            end_julian=date2doy(model_end_time);
            temp_perlen(1)=(temp_perlen(1)-ceil(start_julian)+1);
            temp_perlen(end)=ceil(end_julian);
            % Now, convert the perlen array to the proper timeunits
            switch timeunits_str
                case 'days'
                    perlen_TR=temp_perlen;
                case 'hours'
                    perlen_TR=temp_perlen*24;
                case 'minutes'
                    perlen_TR=temp_perlen*24*60;
                case 'seconds'
                    perlen_TR=temp_perlen*24*60*60;
            end
            % Now, calculate the nstp array, assuming that hours is
            % the finest desirable timestep with yearly stress periods
            switch nstp_str
                case 'months' % this produces timesteps that are roughly 1 month, though not exactly calendar months
                    nstp_TR=ceil(temp_perlen./temp_days)*12;
                case 'weeks'
                    nstp_TR=ceil(temp_perlen./temp_days)*52;
                case 'days'
                    nstp_TR=temp_perlen;
                case 'hours'
                    nstp_TR=temp_perlen*24;
            end
        end
    case 'months' % does not yet work, must recode to get it working

    case 'weeks'
        % calculate the number of weeks in the model
        nper_TR=ceil((model_end_time-model_start_time+1)/7);
        % Initialize an array representing stress period lengths in
        % days
        temp_perlen=ones(1,nper_TR)*7;
        % Shorten the final stress period if necessary
        temp_perlen(end)=ceil(model_end_time-model_start_time+1)-(nper_TR-1)*7;
        % Now, convert the perlen array to the proper timeunits
        switch timeunits_str
            case 'days'
                perlen_TR=temp_perlen;
            case 'hours'
                perlen_TR=temp_perlen*24;
            case 'minutes'
                perlen_TR=temp_perlen*24*60;
            case 'seconds'
                perlen_TR=temp_perlen*24*60*60;
        end
        % Now, calculate the nstp array, assuming that minutes is
        % the finest desirable timestep with weekly stress periods
        switch nstp_str
            case 'weeks'
                nstp_TR=ones(length(perlen_TR));
            case 'days'
                nstp_TR=temp_perlen;
            case 'hours'
                nstp_TR=temp_perlen*24;
            case 'minutes'
                nstp_TR=temp_perlen*24*60;
        end
    case 'days' % does not yet work, must recode to get it working
    case 'hours' % does not yet work, must recode to get it working
    case 'minutes' % does not yet work, must recode to get it working
    case 'seconds' % does not yet work, must recode to get it working

end
% Prepare the output by appending the transient portion
perlen=[perlen,perlen_TR];
nstp=[nstp,nstp_TR];
% assign the stress_period_type array
start_index=length(stress_period_type);
for m=1:nper_TR
    stress_period_type{start_index+m}='TR';
end
nper=nper+nper_TR;
end

%--------------------------------------------------------------------------
% Build Steady State stress periods
%--------------------------------------------------------------------------
function [stress_period_type,nper,nstp,perlen,tsmult]=...
    SS_stress_period_build(stress_period_type,nper,nstp,perlen,tsmult)
start_index=length(stress_period_type);
stress_period_type{start_index+1}='SS';
nper=nper+1;
nstp(start_index+1)=1;
perlen(start_index+1)=perlen;
tsmult(start_index+1)=1;
end

%--------------------------------------------------------------------------
% Build transient stress periods based on numeric inputs
%--------------------------------------------------------------------------
function [stress_period_type,nper,nstp,perlen]=...
    TR_stress_period_build_numeric(stress_period_type,nper,nstp,perlen,...
    start_date,end_date,stress_period_length,timesteps_per_SP)
num_days=end_date-start_date+1;
% Calculate the number of stress periods
nper_TR=ceil(num_days/stress_period_length);

% Decide if the model simulation length is an integer number of
% identical-length stress periods (this will not be the case in some
% cases only when the stress periods per day is less than 1)
test=rem(nper_TR/stress_period_length,num_days);
if test==0 % all stress periods are identical length
    perlen_TR=repmat(stress_period_length,1,nper_TR);
else % the last stress period is shorter
    perlen_TR=[repmat(stress_period_length,1,nper_TR-1),...
        (num_days-(nper_TR-1)*stress_period_length)];
end
nper=nper+nper_TR;
perlen=[perlen,perlen_TR];
nstp=[nstp,repmat(timesteps_per_SP,1,nper_TR)];
% assign the stress_period_type array
start_index=length(stress_period_type);
for m=1:nper_TR
    stress_period_type{start_index+m}='TR';
end
end

%--------------------------------------------------------------------------
% Parse model time units string
%--------------------------------------------------------------------------
function [timeunits]=parse_timeunits(string)
if ischar(string)
    switch lower(string)
        case 'seconds'
            timeunits=1;
        case 'minutes'
            timeunits=2;
        case 'hours'
            timeunits=3;
        case 'days'
            timeunits=4;
        case 'years'
            timeunits=5;
    end
end
end

%--------------------------------------------------------------------------
% Parse model length units string
%--------------------------------------------------------------------------
function [lenunits]=parse_lenunits(string)
if ischar(string)
    switch lower(string)
        case 'feet'
            lenunits=1;
        case 'meters'
            lenunits=2;
        case 'centimeters'
            lenunits=3;
    end
end
end

%--------------------------------------------------------------------------
% Build model file names based on the ILHM modelName
%--------------------------------------------------------------------------
function package = init_file_names(modelName,package)
packageTypes = fieldnames(package);
for m = 1:length(packageTypes)
    package.(packageTypes{m}).name = [modelName,package.(packageTypes{m}).extension];
end
end
