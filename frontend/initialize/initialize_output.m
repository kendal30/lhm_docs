function outBuffer = initialize_output(configFiles,structure)
% INITIALIZE_OUTPUT  Creates the output structure used by the output_controller
%   output = initialize_output(structure)
% 
%   Descriptions of Input Variables:
%   structure: LHM structured-array 'structure'
% 
%   Descriptions of Output Variables:
%   outBuffer: output buffer structured array used by the output_controller
% 
%   Example(s):
%   >> initialize_output
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
% Get the model output specification and data catalog
%--------------------------------------------------------------------------
% Read in the specified YAML file
specify = configFiles.model.output;

% Read in the data catalog
catalog = readtable([structure.paths.init.lhm,filesep,'data_catalog.csv'],...
                    'ReadVariableNames',true,'ReadRowNames',true);
                    
% Append to specify using the OBS file
specify = build_obs_specify(specify,structure,catalog);

%--------------------------------------------------------------------------
% Load output grids
%--------------------------------------------------------------------------
% Import gridded params
grids = load_grid_params(structure.paths,'grids_out','OBS');

%--------------------------------------------------------------------------
% Build the model info array
%--------------------------------------------------------------------------
info.group_name = structure.group_name;
info.run_name = structure.run_name;
info.run_date = '0000-00-00 00:00:00'; % this will be updated when the model is run
info.serial_id = structure.serial_ID;
info.time_start = datestr(structure.dates_timesteps(1),'yyyy-mm-dd HH:MM:SS');
info.time_end = datestr(structure.dates_timesteps(end),'yyyy-mm-dd HH:MM:SS');

% Write this to the file
outFile = structure.paths.init.output.files.output;
write_structure(info,outFile,'/info');

%--------------------------------------------------------------------------
% Build the geolocation array
%--------------------------------------------------------------------------
% This is intended to be a much more formal description than this, but this will
% do for now
geolocation.projection = structure.projection;
geolocation.left = structure.left;
geolocation.bot = structure.bot;
geolocation.cellsize = structure.cell_resolution;
geolocation.num_col = structure.num_col;
geolocation.num_row = structure.num_row;

% Write this to the file
write_structure(geolocation,outFile,'/geolocation');

%--------------------------------------------------------------------------
% Build the structure array
%--------------------------------------------------------------------------
% This is intended to be a much more complete description of the structure,
% this will do for now
outStructure.grids.lhm.z.surface = extend_domain(structure.trim_struct,structure.index_all);
outStructure.grids.lhm.z.stream = extend_domain(structure.trim_struct,structure.thickness_wetland(:,1)); % this isn't exactly right
outStructure.grids.lhm.z.soil.center = extend_domain(structure.trim_struct,structure.thickness_soil);
outStructure.grids.lhm.z.wetland.center = extend_domain(structure.trim_struct,structure.thickness_wetland);
% outStructure.grids.lhm.z.groundwater.center = []; % TODO: complete this for groundwater grid

% Write this to the file
write_structure(outStructure,outFile,'/structure');

%--------------------------------------------------------------------------
% Build the individual type arrays
%--------------------------------------------------------------------------
outBuffer = struct();
if ~isempty(specify.grid.names)
    outBuffer.grid = build_output_struct(structure,grids,specify.grid,catalog,'grid',outFile);
end
if ~isempty(specify.zone.names)
    outBuffer.zone = build_output_struct(structure,grids,specify.zone,catalog,'zone',outFile);
end
if ~isempty(specify.point.names)
    outBuffer.point = build_output_struct(structure,grids,specify.point,catalog,'point',outFile);
end
if ~isempty(specify.station.names)
    outBuffer.station = build_output_struct(structure,grids,specify.station,catalog,'station',outFile);
end
end


%--------------------------------------------------------------------------
% Helper functions
%--------------------------------------------------------------------------
function write_structure(structure,outFile,outName)
% Write the structure of the OUTF file, which will be populated later
h5groupcreate(outFile,outName);

fields = fieldnames(structure);
for m = 1:length(fields)
    tempGroup = [outName,'/',fields{m}];
    if isstruct(structure.(fields{m}))
        write_structure(structure.(fields{m}),outFile,tempGroup);
    else
        attribs.class = class(structure.(fields{m}));
        h5datawrite(outFile,tempGroup,structure.(fields{m}),attribs);
    end
end
end


function [specify] = build_obs_specify(specify, structure, catalog)
% This function reads the OBS file and builds a specification structure to
% output the correct datasets and intervals. Right now it only works with
% station and point types

% In addition to the fields used for grids and zones
% Will need to specify the locations of points and stations
% specify.ID is the observation ID, Nobs x 1
% specify.find is a find array of size Nobs x 1, with values = model cells
% specify.x is the x-locations, of size Nobs x 1
% specify.y is the y-locations, of size Nobs x 1

% Note: interval for stations must by Mx2, where M is the number of timesteps in the station
% interval spec. For points, the interval must be Nx2, where N is the number of observation points,
% sorted in time ascending order.

% Figure out which datasets are present, get the h5info
obsFile = structure.paths.init.obs;
info = h5info(obsFile);

% Loop through the info structure, getting observation tables
obsTable = struct('point',[],'station',[]);

% Now, read in the point and station observation tables, if they are in the data catalog
for m = 1:length(info.Groups)
    switch info.Groups(m).Name
        case {'/point','/station'}
            thisType = strtok(info.Groups(m).Name,'/');
            for n = 1:length(info.Groups(m).Groups)
                thisGroup = info.Groups(m).Groups(n).Name;
                thisVar = fliplr(strtok(fliplr(thisGroup),'/'));
                % Check to see if this variable is in the data catalog
                if ismember(thisVar,catalog.Properties.RowNames)
                    % Read in the observation table
                    obsTable.(thisType).(thisVar) = h5obstableread(obsFile,thisGroup);
                end
            end
    end
end

% Create a blank structured array
blankStruct = struct('station',struct('names','','grids','','vals','','outputs','','stats','',...
    'intervalStart','','intervalEnd','','ID','','find','','x','','y',''),...
    'point',struct('names','','grids','','vals','','outputs','','stats','',...
    'interval','','ID','','find','','x','','y',''));

% Now, loop through the point and station types
obsTypes = fieldnames(obsTable);
for m = 1:length(obsTypes)
    thisType = obsTypes{m};
    specify.(thisType) = blankStruct.(thisType);
    
    if length(obsTable.(thisType))>=1
        % Get the variables in this type
        varNames = fieldnames(obsTable.(thisType));

        % Need a composite index for set/var combos
        indSetVar = 0;

        % Assign the fields from the obsTable
        for n = 1:length(varNames)
            % Get the set name
            thisVar = varNames{n};

            % Find out how many set names we have within this variable
            setNames = unique(obsTable.(thisType).(thisVar).set);

            % Loop through set names
            for o = 1:length(setNames)
                % Increment the composite index
                indSetVar = indSetVar + 1;

                % Build the output set name for this var/obs set
                % combination, and assign to specify
                thisSet = setNames{o};
                outSet = [thisSet,'__',thisVar];

                % Assign what we know so far
                specify.(thisType).names{indSetVar} = outSet;
                specify.(thisType).outputs{indSetVar} = {thisVar};
                specify.(thisType).stats{indSetVar} = {'value'};
                specify.(thisType).rescale{indSetVar} = 0;

                % Get the obsTable subset for this set
                indThisObs = strcmpi(obsTable.(thisType).(thisVar).set,thisSet);
                thisObs = obsTable.(thisType).(thisVar)(indThisObs,:);

                % Get the grid and grid vals that define these points
                gridName = unique(thisObs.grid);
                specify.(thisType).grids{indSetVar} = gridName{1};
                specify.(thisType).vals{indSetVar} = uint32(thisObs.grid_id);
                
                % Set the intervals
                switch thisType
                    case 'point'
                        specify.(thisType).interval{indSetVar} = [min(thisObs.time_start),max(thisObs.time_end)];
                    case 'station'
                        specify.(thisType).intervalStart{indSetVar} = thisObs.time_start;
                        specify.(thisType).intervalEnd{indSetVar} = thisObs.time_end;
                end

                % Save the point IDs
                specify.(thisType).ID{indSetVar} = thisObs.id;

                % Assign the x, y, and z coordinates
                specify.(thisType).x{indSetVar} = thisObs.x;
                specify.(thisType).y{indSetVar} = thisObs.y;

                % Finally, get the find array to relate x, y locations to
                % model cells
                % First, compute the cell row and columns
                thisCol = ceil((thisObs.x - structure.left) / structure.cell_resolution);
                thisRow = structure.num_row - floor((thisObs.y - structure.bot) / structure.cell_resolution);

                thisFind = sub2ind([structure.num_row,structure.num_col],thisRow,thisCol);
                specify.(thisType).find{indSetVar} = thisFind;
            end
        end
    end
end

end

function [output] = build_output_struct(structure, grids, specify, catalog, outType, outFile)
% Build the output buffer arrays
defaultAttribs = struct('class','single');
% Determine the size of the buffer, want to write to disk not too often,
% may need to adjust this parameter
minBuff = 96; % number of timesteps in the buffer--i.e. 96 hours if hourly output
dateFormat = 'yyyy/mm/dd HH:MM:SS';

%% Create the output group for this datatype
output = struct();
outNames = specify.names;
h5groupcreate(outFile,['/',outType]);

% Process the vals array properly
if length(specify.vals)>length(outNames)
    specify.vals = {cell2mat(specify.vals)};
else
    for m = 1:length(outNames)
        if iscell(specify.vals{m})
            specify.vals{m} = cell2mat(specify.vals{m});
        end
    end
end

for m = 1:length(outNames)
    % Force the output statistic to be 'value' for point datasets
    if any(strcmpi(outType,{'point'}))
        specify.stats{m} = {'value'};
    end

    % For convenience
    outName = outNames{m};
    datasetNames = specify.outputs{m};
    numDatasets = length(datasetNames);
    statsNames = specify.stats{m};
    numStats = length(statsNames);

    % Clear structures
    [attributes,datasets,zones,internal] = deal(struct());

    %% Specify the dataset attributes
    attributes.xy_grid = 'lhm'; % for now, this is the only option

    %% Define the interval table
    try
        switch lower(outType)
            case {'grid','station'} % each have discrete output intervals fully specified, allows defined temporal aggregation
                % Check to make sure that the specification is agnostic of row or column vector specification
                % force column vector. This works for either the explicit interval spec. or the range spec
                if size(specify.intervalStart{m},2)>1
                    specify.intervalStart{m} = specify.intervalStart{m}';
                end
                
                % Create the interval array
                if isnumeric(specify.intervalStart{m})
                    % Assume already datenum
                    interval = [specify.intervalStart{m},specify.intervalEnd{m}];
                else
                    interval = [datenum(specify.intervalStart{m},dateFormat),datenum(specify.intervalEnd{m},dateFormat)];
                end
            case {'zone','point'} % has a range of output intervals w/ no temporal aggregation
                if length(specify.interval{m})==2
                    % For a range of dates
                    interval = (datenum(specify.interval{m}(1),dateFormat):structure.timestep_length/86400:datenum(specify.interval{m}(2),dateFormat))';
                else
                    % For explicit times
                    interval = datenum(specify.interval{m},dateFormat);
                end
        end
    catch
        disp('Interval table creation failed, check interval definitions');
    end

    %% Define the lookup table, both internal and external
    switch lower(outType)
        case 'grid' % Number the internal logical lookup array
            % First, internal
            tempInternal = trim_domain(structure.trim_struct,(grids.(specify.grids{m}) == specify.vals{m}));
            % Then, external
            lookup = uint32(tempInternal);
            lookup(tempInternal==1) = (1:sum(tempInternal));
            lookup = extend_domain(structure.trim_struct,lookup);
        case 'zone'
            % First external
            lookup = grids.(specify.grids{m});
            % Then internal
            numZones = length(specify.vals{m});
            tempInternal = false([size(lookup),numZones]);
            for n = 1:numZones
                allZone = find(lookup == specify.vals{m}(n));
                thisInternal = tempInternal(:,:,1);
                thisInternal(allZone(rand_index(size(allZone),1,specify.sampleSize{m}))) = true;
                tempInternal(:,:,n) = thisInternal;
            end
            tempInternal = trim_domain(structure.trim_struct,tempInternal);
        case 'point'
            % Only have internal lookup for points and stations
            origNum = reshape((1:prod(structure.trim_struct.origSize)),structure.trim_struct.origSize);
            trimNum = trim_domain(structure.trim_struct,origNum);

            % Build the internal lookup table
            [~,tempInternal] = ismember(uniqueStations,trimNum);
            numPoints = length(tempInternal);
        case 'station'
            % Only have internal lookup for points and stations
            origNum = reshape((1:prod(structure.trim_struct.origSize)),structure.trim_struct.origSize);
            trimNum = trim_domain(structure.trim_struct,origNum);

            % Get the unique stations, and arbitrary stationID,
            uniqueFind = unique(specify.find{m});

            % Build the internal lookup table
            [uniquePresent,uniqueNum] = ismember(uniqueFind,trimNum);
            tempInternal = uniqueNum(uniquePresent);

            % Get an arbitrary stationID with those stations in the trimmed
            % domain, used to link to output data 2nd dimension. Station ID
            % = 0 means not in the domain
            [~,stationID] = ismember(specify.find{m},uniqueFind(uniquePresent));
            numPoints = length(tempInternal);
    end

    %% Build the index table
    switch lower(outType)
        case 'point'
            index = [specify.ID{m},specify.x{m},specify.y{m}];
        case 'station'
            index = [specify.ID{m},stationID,specify.x{m},specify.y{m}];
    end

    %% Define the zone names, if necessary
    if strcmpi(outType,'zone')
        for n = 1:length(specify.vals{m})
            zones.(['zone',num2str(specify.vals{m}(n))]) = specify.zones{m}{n};
        end
    end

    %% Determine sizes of the output arrays
    % Determine number of columns in the model output
    switch lower(outType)
        case 'grid'
            numCol = size(structure.zeros_grid,1);
        case 'zone'
            numCol = numZones;
        case 'station'
            numCol = numPoints;
        case 'point'
            numCol = 1; %each row in the point output corresponds to a single observation location
    end
    sizes.surface = numCol;
    sizes.wetland = [numCol,size(structure.zeros_grid_wetland,2)]; %has vertical layers
    sizes.stream = numCol;
    sizes.soil = [numCol,size(structure.zeros_grid_soil,2)]; %has vertical layers
	sizes.groundwater = [numCol,1]; % TODO: Change for groundwater model

    % Determine the number of rows in the model output
    numRow = size(interval,1);

    %% Create the internal structure for the dataset
    % Save the temporary lookup arrays to the output structure
    internal.lookup = tempInternal;

    % Stats and datasets
    internal.stats = statsNames;
    internal.outputs = datasetNames;

    % Save the attributes here, will be the same for all datasets
    internal.attributes = defaultAttribs;

    % Determine if this dataset will be rescaled or not
    internal.rescale = logical(specify.rescale{m});

    % Determine if each model timestep is within a specified output interval
    switch lower(outType)
        case {'grid','station'} % allows temporal aggregation
            internal.outputTimestep = false(size(structure.dates_timesteps));
            for n = 1:size(interval,1)
                thisRange = bracket_index(structure.dates_timesteps,interval(n,1),interval(n,2));
                internal.outputTimestep(thisRange(1):thisRange(2)) = true;
            end
        case {'zone','point'} % no temporal aggregation
            internal.outputTimestep = ismember_dates(structure.dates_timesteps,interval,4);
    end

    % For points, create an array with the output timestep for each point
    if strcmpi(outType,'point')
        [~,internal.pointsTimestep] = ismember_dates(interval,structure.dates_timesteps,4);
        %TODO: check that the output here is of length==numPoints, and that it maps to model timesteps 
        % in which each point has an observation
    end

    % Also, define an internal variable with the interval length in model
    % timesteps, assumes model timesteps are specified in seconds
    switch lower(outType) % allows temporal aggregation
        case {'grid','station'}
            internal.intervalLength = round((interval(:,2) - interval(:,1))*86400/structure.timestep_length);
        case {'zone','point'} % no temporal aggregation
            internal.intervalLength = ones(length(interval),1);
    end
    internal.maxBufferRows = ceil(minBuff/min(internal.intervalLength));

    % Fill in some start values here
    internal.lastRowHDF5 = 0;
    internal.lastBufferRow = 0;
    internal.lastIntervalStep = 0;
    internal.currInterval = 1;

    % This part of the internal array refers to individual datasets
    internal.rescaleFlag = false(length(specify.outputs{m}),1);
    internal.domainFlag = zeros(length(specify.outputs{m}),1,'uint8');


    %% Create the datasets structure
    % Loop through the output datasets
    for n = 1:numDatasets
        % For convenience
        datasetName = datasetNames{n};
        gridType = catalog.GRID{datasetName};
        domainType = catalog.DOMAIN{datasetName};

        % Write the attributes of the dataset from the catalog
        datasets.(datasetName).attributes.z_grid = gridType;

        % Loop through the output stats
        for o = 1:numStats
            % For convenience
            statsName = statsNames{o};
            tempBuffSize = [min(numRow,internal.maxBufferRows),sizes.(gridType)];
            % Populate the datasets array
            datasets.(datasetName).(statsName).buffSize = tempBuffSize;
            datasets.(datasetName).(statsName).data = zeros([tempBuffSize(2:end),1]);
            datasets.(datasetName).(statsName).buffer = zeros(tempBuffSize,defaultAttribs.class);
            datasets.(datasetName).(statsName).fhandle = str2func(statsName);
            datasets.(datasetName).(statsName).attributes = defaultAttribs;
            datasets.(datasetName).(statsName).attributes.units = catalog.UNITS{datasetName};
            datasets.(datasetName).(statsName).attributes.rescale = num2str(internal.rescaleFlag(n));
            datasets.(datasetName).(statsName).attributes.domain = domainType;
        end

        % Update the internal array for rescaling by the output controller
        if internal.rescale
            switch domainType
                case 'all'
                    internal.domainFlag(n) = -1;
                case 'wetland'
                    internal.rescaleFlag(n) = true;
                    internal.domainFlag(n) = 2;
                case 'upland'
                    internal.rescaleFlag(n) = true;
                    internal.domainFlag(n) = 1;
                case 'stream'
                    internal.rescaleFlag(n) = true;
                    internal.domainFlag(n) = 0;
            end
        end
    end


    %% Now, write the arrays to file
    % Create the dataset group
    tempGroup = [outType,'/',outName];
    h5groupcreate(outFile,tempGroup);

    % Write the dataset attributes
    attribList = fieldnames(attributes);
    for n = 1:length(attribList)
        h5attput(outFile,[tempGroup,'/',attribList{n}],attributes.(attribList{n}));
    end

    % Write the interval table
    intervalAttribs.class = class(interval);
    h5datawrite(outFile,[tempGroup,'/interval'],interval,intervalAttribs);

    % Write the lookup table, if needed
    if any(strcmpi(outType,{'grid','zone'}))
        lookupAttribs.class = class(lookup);
        h5datawrite(outFile,[tempGroup,'/lookup'],lookup,lookupAttribs);
    end

    % Write the index table, if needed
    if any(strcmpi(outType,{'point','station'}))
        indexAttribs.class = class(index);
        h5datawrite(outFile,[tempGroup,'/index'],index,indexAttribs);
    end
    
    % Write zones group, if needed
    if any(strcmpi(outType,{'zone'}))
        h5groupcreate(outFile,[tempGroup,'/zones']);
        zoneNames = fieldnames(zones);
        for n = 1:length(zoneNames)
            zonesAttribs.class = class(zones.(zoneNames{n}));
            h5datawrite(outFile,[tempGroup,'/zones/',zoneNames{n}],zones.(zoneNames{n}),zonesAttribs);
        end
    end

    % Write the datasets
    tempGroup = [tempGroup,'/datasets']; % #ok<AGROW>
    h5groupcreate(outFile,tempGroup);
    for n = 1:numDatasets
        datasetName = datasetNames{n};
        % Create the dataset group
        h5groupcreate(outFile,[tempGroup,'/',datasetName]);
        % Write attributes
        attribList = fieldnames(datasets.(datasetName).attributes);
        for o = 1:length(attribList)
            h5attput(outFile,[tempGroup,'/',datasetName,'/',attribList{o}],datasets.(datasetName).attributes.(attribList{o}));
        end
        % Remove attributes from the dataset, no longer needed
        datasets.(datasetName) = rmfield(datasets.(datasetName),'attributes');
        % Create the data arrays
        for o = 1:numStats
            statsName = statsNames{o};
            datasets.(datasetName).(statsName).hdf5loc = [tempGroup,'/',datasetName,'/',statsName];
            h5datawrite(outFile,datasets.(datasetName).(statsName).hdf5loc,...
                datasets.(datasetName).(statsName).buffer,...
                datasets.(datasetName).(statsName).attributes);
        end
    end

    %% Save datasets and internal to become the output buffer
    output.(outName) = datasets;
    % Assert that no existing dataset is named 'internal'
    assert(~isfield(datasets,'internal'),'The keyword ''internal'' is reserved, change output variable name')
    output.(outName).internal = internal;
end
end
