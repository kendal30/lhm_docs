function modules = initialize_modules(configFiles)

%Get the module names
moduleNames = configFiles.model.modules; 

%Convert to function handles
moduleFields = fieldnames(moduleNames);
modules = struct();
for m = 1:length(moduleFields)
    if ~strcmpi(moduleFields{m},'modulesVersion')
        modules.(moduleFields{m}) = str2func(moduleNames.(moduleFields{m}));
    else
        modules.(moduleFields{m}) = moduleNames.(moduleFields{m});
    end
end
