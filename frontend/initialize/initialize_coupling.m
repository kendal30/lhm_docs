function initialize_coupling(structure,buffers)
%Creates groundwater coupling output files

%Create the lookup table
lookup = (1:size(structure.zeros_grid,1))';
lookup = extend_domain(structure.trim_struct,lookup);
lookup = uint32(lookup);

%Create each of the output files
create_output_file(buffers.groundwater.transpiration,structure.paths.init.output.files.transp,...
    'transpiration',structure.dates,lookup);
create_output_file(buffers.groundwater.deep_percolation,structure.paths.init.output.files.perc,...
    'deep_percolation',structure.dates,lookup);

end


function create_output_file(outStruct,outFile,outName,outDates,lookup)
%First, create the output index
numDates = length(outDates);
index = [reshape(outDates,numDates,1),(1:numDates)'];

%Now, determine the size of the output array
% outSize = [numDates,size(outStruct.data,1)];
%For now, change outSize first dimension to the size of the output buffer
dataSize = size(outStruct.data);
outSize = [dataSize(2),dataSize(1),dataSize(3:end)];

%Now, create the output group
tempGroup = ['/',outName];
h5groupcreate(outFile,tempGroup);

%Create the output dataset
h5varcreate(outFile,[tempGroup,'/data'],outSize,outStruct.attributes.class,true);

%Write the output attributes
%First, remove class
outAttribs = outStruct.attributes;
outAttribs = rmfield(outAttribs,'class');
attribs = fieldnames(outAttribs);
%Then, write the attributes
for m = 1:length(attribs)
    h5attput(outFile,[tempGroup,'/data/',attribs{m}],outAttribs.(attribs{m}));
end

%Write out the index
attribs = struct('class',class(index));
h5datawrite(outFile,[tempGroup,'/index'],index,attribs);

%Write out the lookup table
attribs = struct('class',class(lookup));
h5datawrite(outFile,[tempGroup,'/lookup'],lookup,attribs);
end