function [structure,modules] = initialize_structure(configFiles,paths,modelInfo,modules)
% INITIALIZE_STRUCTURE  Initializes the structure parameters for ILHM
%   [structure] = initialize_structure(configFile,paths,modelInfo,modules)
% 
%   Descriptions of Input Variables:
%   input:
% 
%   Descriptions of Output Variables:
%   output:
% 
%   Example(s):
%   >> initialize_structure
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

% Add the name of the lookup table for the air exchange depth, located in
% helper/physical
airExchangeFile = 'exchange_depth.mat';

%--------------------------------------------------------------------------
% Initialize "structure" array, add passed parameters
%--------------------------------------------------------------------------
% Initialize the structure array
structure = modelInfo;

% Add passed parameters
structure.paths = paths;
structure.serial_ID = serial_ID_gen(structure.domain_name,structure.group_name,...
    structure.run_name,structure.init_time);

%--------------------------------------------------------------------------
% Load data from the configFile
%--------------------------------------------------------------------------
% Read in the structure information array
structParams = configFiles.model.structure;
structure = merge_struct(structure,structParams);

% Deactivate modules, if need be.
if ~structure.sediment_yield_enabled
    modules.sediment_yield = str2func('blank');
end

if ~structure.irrigation_enabled
    modules.irrigation = str2func('blank');
end

if ~structure.groundwater_enabled
    modules.mod_groundwater_controller = str2func('blank');
end

% Make some settings consistent
if ~structure.groundwater_enabled
    structure.groundwater_optim_enabled = false;
end

%--------------------------------------------------------------------------
% Hard-code the physical units of the model
%--------------------------------------------------------------------------
structure.units_length = 'm';
structure.units_time = 's';
structure.units_mass = 'kg';
structure.units_temp = 'C';

%--------------------------------------------------------------------------
% Load grids
%--------------------------------------------------------------------------
% Import gridded params
[gridStruct, gridHeaders] = load_grid_params(paths,'structure','STAT');

% Assign the projection information for the model
structure.projection = gridHeaders.proj;
structure.left = gridHeaders.left;
structure.bot = gridHeaders.bottom;
structure.cell_resolution = gridHeaders.cellsizeX;
structure.num_col = gridHeaders.cols;
structure.num_row = gridHeaders.rows;

% Do some cleanup here, if necessary, I don't want this here! It should be
% upstream
gridsFix = {'fraction_stream','stream_bottom_depth','index_internally_drained','wetland_bottom_depth'};
for m = 1:length(gridsFix)
    testNan = isnan(gridStruct.(gridsFix{m}));
    gridStruct.(gridsFix{m})(testNan) = 0;
end

% Load runoff params
runoffGrids = load_grid_params(paths,'runoff','STAT');

% Merge structures together
structure = merge_struct(structure,gridStruct);
structure = merge_struct(structure,runoffGrids);

%--------------------------------------------------------------------------
% Load topography grids
%--------------------------------------------------------------------------
% Import gridded params
topographyGrids = load_grid_params(paths,'topography','STAT');

% Make the topography array
structure.topography = topographyGrids;

% Address cells that are essentially fully wetlands or uplands to assure that
% there are no NaN cells in either array within the model boundary
testNanUp = isnan(structure.topography.elevation_up);
testNanWet = isnan(structure.topography.elevation_wet);

structure.topography.elevation_up(testNanUp) = structure.topography.elevation_wet(testNanUp);
structure.topography.elevation_wet(testNanWet) = structure.topography.elevation_up(testNanWet);

%--------------------------------------------------------------------------
% Calculate root zone thickness, unsaturated zone thickness, and
% disconnected wetlands
%--------------------------------------------------------------------------
% Load the starting heads grid
stateGrids = load_grid_params(paths,'state','STAT');

% Calculate total unsaturated thickness array
totalUnsatThickness = max(0,structure.topography.elevation_mean - stateGrids.starting_heads);
meanThick = nanmean(totalUnsatThickness(:));
totalUnsatThickness(isnan(totalUnsatThickness)) = meanThick;

% Calculate root zone thickness array
structure.root_zone_thickness = max(structure.min_soil_thickness,min(structure.max_soil_thickness,totalUnsatThickness));

% Now for unsaturated zone thickness
structure.unsat_thickness = max(0,totalUnsatThickness-structure.root_zone_thickness);

%--------------------------------------------------------------------------
% Calculate some structure parameters
%--------------------------------------------------------------------------
% Calculate a few parameters
structure.model_start = datenum(structure.model_start,'yyyy/mm/dd HH:MM:SS');
structure.model_end = datenum(structure.model_end,'yyyy/mm/dd HH:MM:SS');
structure.dates = (floor(structure.model_start):floor(structure.model_end));
structure.dates_timesteps = (structure.model_start:structure.timestep_length/86400:structure.model_end);

% Determine if the model is running in test mode
structure.test_mode = logical(structure.test_mode);

% Initialize the timestep state
structure.inner_timestep = 0;
structure.outer_timestep = 0;
structure.timestep = 0;
structure.inner_timestep_hours = floor(structure.timestep_length/3600);
structure.num_inner_timesteps = floor(86400/structure.timestep_length);

% Compute the dimensions of the model array, take into account slice and splits
if structure.num_splits == 1
    if ~isfield(structure,'slice')
        structure.slice = structure.model_grid;
    end
else
    if structure.this_split == 0 % this is the master model
        structure.slice = structure.split_slice; % Prepared by initialize_split
    else
        structure.slice = (structure.split_slice == structure.this_split); % Prepared by initialize_split
    end
    structure.run_full_domain = 0; % override this here
end
structure.index_all = (structure.model_grid > 0) & ((structure.slice > 0) | structure.run_full_domain);

%--------------------------------------------------------------------------
% Create internal structured arrays for use by other functions
%--------------------------------------------------------------------------
% Generate the trim struct
trim_struct = trim_domain(structure.index_all);

% Create the climate and landscape structured arrays
structure.climate_index = init_index_climate(paths.init.climate);
structure.lai_index = init_index_lai(paths.init.landscape);
structure.land_use_index = init_index_lu(paths.init.landscape);
if structure.runoff_enabled
    structure.flowtimes_index = init_index_flowtimes(paths.init.landscape);
end
structure.impervious_index = init_index_impervious(paths.init.landscape);
if structure.irrigation_enabled
    [structure.irrigation_limits_index,structure.irrigation_tech_index] = init_index_irrigation(paths.init.landscape);
end

% Read in the first landuse grid for use by the initialize_output function
structure.land_use = read_landuse_start(paths,structure.land_use_index);

%--------------------------------------------------------------------------
% Build zeros grids and spatially distribute some structure parameters
%--------------------------------------------------------------------------
% Build zeros grids
structure.zeros_grid = zeros(structure.num_row,structure.num_col);
structure.zeros_grid_soil = repmat(structure.zeros_grid,[1,1,structure.num_layers_soil]);
structure.zeros_grid_wetland = repmat(structure.zeros_grid,[1,1,structure.num_layers_wetland]);

% Spatially-distribute some structure variables
structure.thickness_bed_sediment = structure.thickness_bed_sediment + structure.zeros_grid;

%--------------------------------------------------------------------------
% Calculate some index arrays that are not updated in update_coverage
%--------------------------------------------------------------------------
% Calculate the initial upland and stream fraction
[structure.fraction_wetland, structure.fraction_stream] = limit_vals([0,1],structure.fraction_wetland, structure.fraction_stream);
structure.fraction_stream = min(structure.fraction_stream, 1-structure.fraction_wetland); % make sure the stream + wetland fraction doesn't exceed 1
structure.fraction_upland = 1 - structure.fraction_stream - structure.fraction_wetland;

% Calculate permanent, shallow, and deep wetlands
structure.index_permanent_wetlands = logical(structure.fraction_wetland > 0);
structure.index_shallow_wetland = structure.index_permanent_wetlands & ...
    (structure.wetland_bottom_depth <= structure.shallow_wetland_cutoff);
structure.index_deep_wetland = structure.index_permanent_wetlands & ...
    (structure.wetland_bottom_depth > structure.shallow_wetland_cutoff);

% Calculate internally/externally drained cells
structure.index_internally_drained = logical(structure.index_internally_drained);
structure.index_externally_drained = ~structure.index_internally_drained;

%--------------------------------------------------------------------------
% Trim the structure parameters to the domain, then add the trim_struct to
% "structure"
%--------------------------------------------------------------------------
% Trim the structure variables to the domain
[structure] = trim_domain(trim_struct,structure);
structure.trim_struct = trim_struct;

%--------------------------------------------------------------------------
% Build soil and wetland depth arrays
%--------------------------------------------------------------------------
% Build the lake and soil thickness and depth arrays
[structure.thickness_soil] = soil_layering(structure.zeros_grid_soil,structure.num_layers_soil,...
    structure.soil_first_layer_thickness,structure.root_zone_thickness);
[structure.thickness_wetland,structure.wetland_depth] = wetland_layering(...
    structure.zeros_grid_wetland,structure.num_layers_wetland,...
    structure.wetland_first_layer_thickness,structure.wetland_bottom_depth,...
    structure.index_shallow_wetland,structure.index_deep_wetland);

% Air Exchange depth handling
structure.soil_air_exchange_structure = load(airExchangeFile);

% Add stream depths in as weighted average with shallow wetland depths
indShallow = structure.index_shallow_wetland | structure.fraction_stream > 0;
if any(indShallow)
    tempThick = structure.thickness_wetland(indShallow,1);
    tempThick = weighted_mean('arithmetic',structure.stream_bottom_depth(indShallow),tempThick,...
        structure.fraction_stream(indShallow),structure.fraction_wetland(indShallow));
    structure.thickness_wetland(indShallow,1) = tempThick;
    structure.wetland_depth(indShallow,1) = tempThick;
end

%--------------------------------------------------------------------------
% Build the runoff structures
%--------------------------------------------------------------------------
% Next, do the internally-drained basin routing
[structure] = internal_basin_routing(structure);

end

%--------------------------------------------------------------------------
% Gridded parameter loading function
%--------------------------------------------------------------------------
function landuse = read_landuse_start(paths,luIndex)
landuse = struct();
% Read in the lookup array
lookup = h5dataread(paths.init.landscape,'/landuse/lookup',true);

% Read in the majority grid
tempMajority = h5dataread(paths.init.landscape,'/landuse/majority',true,...
    [0,0],[1,luIndex.majoritySize(2)],[1,1]);
landuse.majority = tempMajority(lookup); % transpose to match ILHM dimensions

end
%--------------------------------------------------------------------------
% Index structure initialization functions
%--------------------------------------------------------------------------
function [indexStruct] = init_index_climate(h5file)
% get the hdf5 file info using the MATLAB builtin function
info = h5info(h5file);
groups = info.Groups;

% Initialize loop variables
indexStruct = struct();
for m = 1:length(groups)
    dataName = groups(m).Name(2:end); % ignore leading slash
    if ~strcmpi(dataName,'info')
        tempIndex = h5dataread(h5file,[groups(m).Name,'/index']);
        tempDatevec = datevec(tempIndex(:,1));
        indexStruct.(dataName).index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
        indexStruct.(dataName).dataSize = h5varget(h5file,[groups(m).Name,'/data'],false);
        indexStruct.(dataName).lookupSize = h5varget(h5file,[groups(m).Name,'/lookup'],false);
    end
end
end

function [indexStruct] = init_index_lai(h5file)
group = '/lai';

tempIndex = h5dataread(h5file,[group,'/index']);
tempDatevec = datevec(tempIndex(:,1));
indexStruct.index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
indexStruct.dataSize = h5varget(h5file,[group,'/data'],false);
end

function [indexStruct] = init_index_lu(h5file)
group = '/landuse';

tempIndex = h5dataread(h5file,[group,'/index']);
tempDatevec = datevec(tempIndex(:,1));
indexStruct.index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
indexStruct.majoritySize = h5varget(h5file,[group,'/majority'],false);
indexStruct.weightsSize = h5varget(h5file,[group,'/weights'],false);
end

function [indexStruct] = init_index_impervious(h5file)
group = '/impervious';

tempIndex = h5dataread(h5file,[group,'/index']);
tempDatevec = datevec(tempIndex(:,1));
indexStruct.index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
indexStruct.weightsSize = h5varget(h5file,[group,'/weights'],false);
end

function [indexStruct] = init_index_flowtimes(h5file)
group = '/flowtimes';

tempIndex = h5dataread(h5file,[group,'/index']);
tempDatevec = datevec(tempIndex(:,1));
indexStruct.index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
indexStruct.flowtimesSize = h5varget(h5file,[group,'/data'],false);
end

function [indexLimStruct,indexTechStruct] = init_index_irrigation(h5file)
% First, do limitation
group = '/irrigation_limits';

tempIndex = h5dataread(h5file,[group,'/index']);
if ~islogical(tempIndex)
    tempDatevec = datevec(tempIndex(:,1));
    indexLimStruct.index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
    indexLimStruct.limitationSize = h5varget(h5file,[group,'/data'],false);
else
    indexLimStruct = [];
end

% Then, do technology
group = '/irrigation_technology';

tempIndex = h5dataread(h5file,[group,'/index']);
if ~islogical(tempIndex)
    tempDatevec = datevec(tempIndex(:,1));
    indexTechStruct.index = int32([tempDatevec(:,1:4),tempIndex(:,2:end)]);
    indexTechStruct.weightsSize = h5varget(h5file,[group,'/weights'],false);
else
    indexTechStruct = [];
end

end
%--------------------------------------------------------------------------
% Layer structure initialization functions
%--------------------------------------------------------------------------
function [delZ,z] = wetland_layering(zerosGrid,numLayers,botSurfLayer,botDepth,shallowWetlands,deepWetlands)
% In this function, layers are the column index

% Initialize variables
delZ = zerosGrid;

% The first layer thickness is specified
delZ(deepWetlands,1) = botSurfLayer;
delZ(shallowWetlands,1) = botDepth(shallowWetlands);

% build remaining layers, contingent upon change in depth between layers not
% exceeding a factor of 2
if any(deepWetlands)
    maxChange = 2;
    for m = 2:numLayers
        delZ(deepWetlands,m) = (botDepth(deepWetlands) - sum(delZ(deepWetlands,1:m-1),2)) / (numLayers+1-m);
        % Test to see if depth change exceeds factor on thickening
        test = (delZ(:,m) > (delZ(:,m-1) * maxChange)) & deepWetlands;
        if any(test)
            delZ(test,m) = delZ(test,m-1) * maxChange;
        end
        % Test to see if depth change exceeds change factor on thinning
        test = (delZ(:,m) < (delZ(:,m-1) / maxChange)) & deepWetlands;
        if any(test)
            delZ(test,m) = delZ(test,m-1) / maxChange;
        end
    end
end
% Finally, calculate the layer depths, node centered
z = cumsum(delZ,2) - delZ / 2;
end

function [delZ,z] = soil_layering(zerosGrid,numLayers,botSurfLayer,botDepth)
% Initialize variables
delZ = zerosGrid;

% The first layer thickness is specified
if iscell(botSurfLayer)
    for m = 1:length(botSurfLayer)
        delZ(:,m) = botSurfLayer{m};
    end
    startLayer = length(botSurfLayer) + 1;
else
    delZ(:,1) = botSurfLayer;
    startLayer = 2;
end

% Build remaining layers, contingent upon change in depth between layers not
% exceeding a factor of 2
maxChange = 2;

if startLayer <= numLayers
    for m = startLayer:numLayers
        delZ(:,m) = (botDepth(:) - sum(delZ(:,1:m-1),2)) / (numLayers+1-m);
        % Test to see if specified soil thicknesses exceed calculated maximum depth, if so
        % keep last layer thickness.
        test = delZ(:,m) < 0;
        if any(test)
            delZ(test,m) = delZ(test,m-1);
        end
        % Test to see if depth change exceeds factor on thickening
        test = (delZ(:,m) > (delZ(:,m-1) * maxChange));
        if any(test)
            delZ(test,m) = delZ(test,m-1) * maxChange;
        end
        % Test to see if depth change exceeds change factor on thinning
        test = (delZ(:,m) < (delZ(:,m-1) / maxChange));
        if any(test)
            delZ(test,m) = delZ(test,m-1) / maxChange;
        end
    end
end

% Finally, calculate the layer depths, node centered
z = cumsum(delZ,2) - delZ / 2;
end

%--------------------------------------------------------------------------
% Runoff routing routines
%--------------------------------------------------------------------------
% Now, internally drained basin routing
function [structure] = internal_basin_routing(structure)

% For convenience, initialize or define here
indExternal = structure.index_externally_drained;
fracWetland = structure.fraction_stream + structure.fraction_wetland;
fracUpland = 1 - fracWetland;
structure.runoff_internal_frac = structure.zeros_grid;

% % Build the arrays for internally drained basin routing
% First, guarantee consistency between what is considered
% internally/externally drained and the runoff internal basins/sinks
structure.runoff_internal_basin(indExternal) = NaN;
testNoBasin = structure.index_internally_drained & isnan(structure.runoff_internal_basin);
testMax = nanmax(structure.runoff_internal_basin);

if ~isnan(testMax)
    % For cells that are in fact internally drained but were not assigned a
    % basin, due to some inconsistency in scaling, create a new artificial
    % basin for them, and set all cells to sinks so that the effect is to
    % route water to themselves, more or less, this is a small population
    structure.runoff_internal_basin(testNoBasin) = testMax + 1;
    structure.runoff_internal_sink(testNoBasin) = testMax + 1;

    % Accumarray can't handle 0 values, shift up 1 if 0 is a basin ID
    if nanmin(structure.runoff_internal_basin)==0
        structure.runoff_internal_basin = structure.runoff_internal_basin + 1;
        structure.runoff_internal_sink = structure.runoff_internal_sink + 1;
    end

    % Build the internal basin accumulation ID array, modify the
    % runoff_internal_basin a bit to match needs of accumarray
    structure.runoff_internal_basin_max = nanmax(structure.runoff_internal_basin);
    structure.runoff_internal_basin(indExternal) = structure.runoff_internal_basin_max+1; % accumarray can't handle NaN
    structure.runoff_internal_sink(indExternal) = structure.runoff_internal_basin_max+1; % accumarray can't handle NaN

    % First, we need to make sure that every internally-drained basin does
    % indeed have a sink, set entire basin to sink in these cases
    testBasinSize = accumarray(structure.runoff_internal_basin,structure.runoff_internal_basin>0);
    testSinkSize = accumarray(structure.runoff_internal_basin,structure.runoff_internal_sink==structure.runoff_internal_basin);
    testNoSink = (testBasinSize>0) & (testSinkSize==0);
    gridNoSink = testNoSink(structure.runoff_internal_basin);
    structure.runoff_internal_sink(gridNoSink) = structure.runoff_internal_basin(gridNoSink);

    % Define the fraction of internal basin runoff routed to each cell
    % If wetlands are present, will route to these first
    testWetlands = fracWetland > 0.05; % must have greater than 5% wetland, otherwise this really is an upland process
    sinkWetlands = (testWetlands .* structure.runoff_internal_sink) > 0;
    sumSinkWetlands = accumarray(structure.runoff_internal_basin,sinkWetlands.*fracWetland); % multiply by proportion of cell that is wetland
    sumSinkWetlands(end) = 0; % this is the externally-drained portion
    structure.index_runoff_internal_wetland = (sumSinkWetlands(structure.runoff_internal_basin) > 0) & sinkWetlands;

    % Populate the internal runoff fraction array, this is the fraction of
    % internal basin runoff routed to each cell in the sink, first for wetlands
    tempFrac = 1 ./ sumSinkWetlands(structure.runoff_internal_basin);
    structure.runoff_internal_frac(structure.index_runoff_internal_wetland) = tempFrac(structure.index_runoff_internal_wetland).*...
        fracWetland(structure.index_runoff_internal_wetland); % finish weighting by cell wetland proportion

    % Now, handle the basins without wetlands in the sink
    sinkUplands = (~testWetlands .* structure.runoff_internal_sink) > 0;
    sumSinkUplands = accumarray(structure.runoff_internal_basin,sinkUplands.*fracUpland);
    sumSinkUplands(sumSinkWetlands > 0) = 0; % if there are sinkWetlands in this basin, set sumSinkUplands to 0
    sumSinkUplands(end) = 0; % this is the externally-drained portion
    structure.index_runoff_internal_upland = (sumSinkUplands(structure.runoff_internal_basin) > 0) & sinkUplands;

    % Update this portion of the internal_runoff_frac array
    tempFrac = 1./ sumSinkUplands(structure.runoff_internal_basin);
    structure.runoff_internal_frac(structure.index_runoff_internal_upland) = tempFrac(structure.index_runoff_internal_upland).*...
        fracUpland(structure.index_runoff_internal_upland); % finish weighting by cell upland proportion
else
    structure.index_runoff_internal_upland = structure.zeros_grid;
end
end
