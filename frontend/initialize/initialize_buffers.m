function [buffers] = initialize_buffers(structure)
%INITIALIZE_BUFFERS Initializes input and output buffers
%   initialize_buffers(structure)
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> input_structure
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.


%Initialize input buffers
buffers.climate = init_buffer_climate(structure.paths.init.climate);
buffers.lai = init_buffer_lai(structure.paths.init.landscape);
buffers.land_use = init_buffer_lu(structure.paths.init.landscape);
buffers.flowtimes = init_buffer_flowtimes(structure.paths.init.landscape);
buffers.impervious = init_buffer_impervious(structure.paths.init.landscape);
[buffers.irrigation_limits,buffers.irrigation_tech] = init_buffer_irrigation(structure.paths.init.landscape);

%Initialize output buffers
buffers.groundwater = init_buffer_groundwater(structure.zeros_grid);

%Trim the buffers to the domain
buffers = trim_domain(structure.trim_struct,buffers);

end

%--------------------------------------------------------------------------
%Buffer initialization functions
%--------------------------------------------------------------------------
function [buffStruct] = init_buffer_groundwater(zerosGrid)
bufferLength = 12; %hold up to 12 days worth of grids

attributes = struct('class','int32','units','m/day','nan','-99999',...
    'scale','100000000');
%Want to reduce output size, so set the precision to int32, multiple of 1e9
%This results in a total cumulative error of < 0.001%

%First, create a template buffer
template.data = repmat(zerosGrid,[1,bufferLength]);
template.thisHour = 0; %number of counted hours so far in this day
template.thisDay = 0; %number of days counted so far within this buffer
template.maxDays = bufferLength;
template.attributes = attributes;
template.lastRow = 0;

%There are three variables, wetland transpiration and evaporation potential
%And deep percolation
buffStruct.transpiration = template;
buffStruct.deep_percolation = template;
end

function [buffStruct] = init_buffer_climate(h5file)
bufferLength = 96;

%get the hdf5 file info using the MATLAB builtin function
info = h5info(h5file);
groups = info.Groups;

%Initialize loop variables
buffStruct = struct();
for m = 1:length(groups)
    dataName = groups(m).Name(2:end); %ignore leading slash
    if ~strcmpi(dataName,'info')
        buffStruct.(dataName).data = []; 
        buffStruct.(dataName).dataRows = 0;
        buffStruct.(dataName).bufferLength = bufferLength;
        buffStruct.(dataName).lookup = [];
        buffStruct.(dataName).lookupSheet = 0;
        buffStruct.(dataName).prevIndexRow = 0;
    end
end
end

function [buffStruct] = init_buffer_lai(h5file)
bufferLength = 6;
group = '/lai';

%Initialize loop variables
buffStruct = struct();
buffStruct.data = [];
buffStruct.dataRows = 0;
buffStruct.bufferLength = bufferLength;
buffStruct.lookup = h5dataread(h5file,[group,'/lookup'],false);
buffStruct.prevIndexRow = 0;
end

function [buffStruct] = init_buffer_lu(h5file)
group = '/landuse';

%Initialize loop variables
buffStruct = struct();
buffStruct.dataRow = 0;
buffStruct.lookup = h5dataread(h5file,[group,'/lookup'],false);
buffStruct.prevIndexRow = 0;

%Get info on the file
info = h5info(h5file);
groups = info.Groups;

%Read in the weight names
for m = 1:length(groups)
    if strcmpi(groups(m).Name,group)
        namesInfo = groups(m).Groups.Datasets;
        weightNames = cell(length(namesInfo),1);
        for n = 1:length(namesInfo)
            weightNum = str2double(namesInfo(n).Name);
            weightNames{weightNum} = h5dataread(h5file,['/',groups(m).Groups.Name,'/',namesInfo(n).Name]);
        end
    end
end
buffStruct.weightNames = weightNames;
end

function [buffStruct] = init_buffer_impervious(h5file)
group = '/impervious';

%Initialize loop variables
buffStruct = struct();
buffStruct.dataRow = 0;
buffStruct.lookup = h5dataread(h5file,[group,'/lookup'],false);
buffStruct.prevIndexRow = 0;

%Get info on the file
info = h5info(h5file);
groups = info.Groups;

%Read in the weight names
for m = 1:length(groups)
    if strcmpi(groups(m).Name,group)
        namesInfo = groups(m).Groups.Datasets;
        weightNames = cell(length(namesInfo),1);
        for n = 1:length(namesInfo)
            weightNum = str2double(namesInfo(n).Name);
            weightNames{weightNum} = h5dataread(h5file,['/',groups(m).Groups.Name,'/',namesInfo(n).Name]);
        end
    end
end
buffStruct.weightNames = weightNames;
end

function [buffStruct] = init_buffer_flowtimes(h5file)
group = '/flowtimes';

%Initialize loop variables
buffStruct = struct();
buffStruct.dataRow = 0;
buffStruct.lookup = h5dataread(h5file,[group,'/lookup'],false);
buffStruct.prevIndexRow = 0;

end

function [buffLimStruct,buffTechStruct] = init_buffer_irrigation(h5file)
group = '/irrigation_limits';

%Initialize loop variables
buffLimStruct = struct();
buffLimStruct.lookup = h5dataread(h5file,[group,'/lookup'],false);
if ~islogical(buffLimStruct.lookup)
    buffLimStruct.dataRow = 0;
    buffLimStruct.prevIndexRow = 0;
end


%Now do irrigation tech
group = '/irrigation_technology';

%Initialize loop variables
buffTechStruct = struct();
buffTechStruct.lookup = h5dataread(h5file,[group,'/lookup'],false);
if ~islogical(buffTechStruct.lookup)
    buffTechStruct.dataRow = 0;
    buffTechStruct.prevIndexRow = 0;
    
    %Get info on the file
    info = h5info(h5file);
    groups = info.Groups;
    
    %Read in the weight names
    for m = 1:length(groups)
        if strcmpi(groups(m).Name,group)
            namesInfo = groups(m).Groups.Datasets;
            weightNames = cell(length(namesInfo),1);
            for n = 1:length(namesInfo)
                weightNum = str2double(namesInfo(n).Name);
                weightNames{weightNum} = h5dataread(h5file,['/',groups(m).Groups.Name,'/',namesInfo(n).Name]);
            end
        end
    end

    buffTechStruct.weightNames = weightNames;
end
end