function params = initialize_params(configFiles,structure,moduleVer)
% INITIALIZE_PARAMS  Initialize the params arrays from inputs
%   params = initialize_params(structure,moduleVer))
% 
%   Descriptions of Input Variables:
%   structure:  a structured array containing the model structural
%               information
%   moduleVer:  a string with model version number
% 
%   Descriptions of Output Variables:
%   params:     a structured-array containing output parameter values
% 
%   Example(s):
%   none
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
% Load scalar params from the config file
%--------------------------------------------------------------------------
params = configFiles.params.scalar;

%--------------------------------------------------------------------------
% Import gridded parameters
%--------------------------------------------------------------------------
% Import the grids from the static file
gridParams= load_grid_params(structure.paths,'params','STAT');

% Trim gridParams to the domain
gridParams = trim_domain(structure.trim_struct,gridParams);

% Store pre-defined grid params
params = merge_struct(params,gridParams);

%--------------------------------------------------------------------------
% Import soil layered parameters and convert to model layering
%--------------------------------------------------------------------------
% Import the grids from the static file
soilParams = load_grid_params(structure.paths,'soil','STAT');

% Trim soilParams to the domain
soilParams = trim_domain(structure.trim_struct,soilParams);

% Fill NaN values at this point--shouldn't be any, but for some reason the Mich model and the Mekong
% model had some, so the filling step in the soil preparation scripts should be evaluated
% this is really just a band-aid solution, but should only affect areas really close to the boundary
soilParams = fill_nans(soilParams);

% Convert the soil params to LHM layering
soilParams = soil_params_layering(soilParams,structure.thickness_soil,...
    structure.num_layers_soil,structure.zeros_grid_soil);

% Assure that soil percentages add up to 1
soilParams.soil_pct_clay = min(soilParams.soil_pct_clay,100-soilParams.soil_pct_sand);

% Adjust some parameters for depth variation
[soilParams.soil_ksat,soilParams.soil_porosity,soilParams.soil_field_cap] =...
    depth_adjust(soilParams.soil_ksat,soilParams.soil_porosity,soilParams.soil_field_cap,...
    structure.thickness_soil,params.soil_ksat_reduction_length,params.soil_porosity_reduction_length);

% Store soil grid params
params = merge_struct(params,soilParams);

%--------------------------------------------------------------------------
% Read in tables for LU and Depression Capacity Mapping
%--------------------------------------------------------------------------
% Create the luMapTable
params.map_table_lu = lu_map_table_create(configFiles);
params.map_table_dep_cap = dep_cap_map_table_create(configFiles);
params.map_table_irrig = irrig_map_table_create(configFiles);
params.map_table_cov_fac = cov_fac_map_table_create(configFiles);

%--------------------------------------------------------------------------
% Distribute some scalar parameters
%--------------------------------------------------------------------------
if strcmpi(moduleVer,'2')
    params.bed_sediment_porosity = structure.zeros_grid + params.bed_sediment_porosity;
    params.bed_sediment_density = structure.zeros_grid + params.bed_sediment_density;
elseif strcmpi(moduleVer,'1')
    params.litter_fraction = structure.zeros_grid;  % this is necessary for the update_coverage module
end

%--------------------------------------------------------------------------
% Calculate derived parameters
%--------------------------------------------------------------------------
params.diff_coeff_vapor = params.therm_cond_air / params.heat_cap_air; % units are kg/s/m, must divided by kg/m^3 for most uses
params.diff_coeff_water = params.therm_cond_water / params.heat_cap_water; % units are kg/s/m, must divided by kg/m^3 for most uses

% Assure that soil_wilting_point > soil_theta_r resid
params.soil_wilting_point = max(params.soil_wilting_point,params.soil_theta_r);

% Assure that soil_field_cap > soil_min_field_cap
params.soil_field_cap = max(params.soil_field_cap,params.soil_min_field_cap);

% Convert fractions to depths by multiplying by layer thicknesses
params.soil_sat_cap = params.soil_porosity .* structure.thickness_soil;
params.soil_field_cap = params.soil_field_cap .* structure.thickness_soil;
params.soil_wilting_point = params.soil_wilting_point .* structure.thickness_soil;
params.soil_water_resid = params.soil_theta_r .* structure.thickness_soil;
params.soil_theta_avail = params.soil_porosity - params.soil_theta_r;
params.soil_infil_cap = params.soil_infil_cap .* cos(atan(structure.topography.slope/100)); % Input slope is percent slope, convert to rise/run

% Calculate irrigation parameters
params.soil_plant_avail_water = params.soil_field_cap - params.soil_wilting_point;
params.soil_plant_avail_water = max(params.soil_plant_avail_water,params.soil_min_plant_avail);
params.soil_irrigation_thresh = sum(params.soil_wilting_point(:,1:params.irrigation_check_layer_max) + ...
    params.irrigation_thresh_apply .* params.soil_plant_avail_water(:,1:params.irrigation_check_layer_max),2);

if isscalar(params.irrigation_event_max)
    params.irrigation_event_max = repmat(params.irrigation_event_max,size(structure.zeros_grid));
end

% Calculate thermal conductivity of bed sediments
if strcmpi(moduleVer,'2')
    ind = (structure.fraction_wetland > 0);
    weightOrganic = (1 - params.bed_sediment_porosity(ind)) .* structure.thickness_bed_sediment(ind);
    weightWater = 1 - weightOrganic;
    [params.bed_sediment_therm_cond,params.bed_sediment_heat_cap,params.bed_sediment_wet_density] = ...
        deal(structure.zeros_grid);
    params.bed_sediment_therm_cond(ind) = weighted_mean('geometric',...
        params.therm_cond_organic+structure.zeros_grid(ind),params.therm_cond_water+structure.zeros_grid(ind),...
        weightOrganic,weightWater);
    params.bed_sediment_heat_cap(ind) = weighted_mean('geometric',...
        params.heat_cap_organic+structure.zeros_grid(ind),params.heat_cap_water+structure.zeros_grid(ind),...
        weightOrganic,weightWater);
    params.bed_sediment_wet_density(ind) = weighted_mean('geometric',...
        params.bed_sediment_density(ind),params.density_water+structure.zeros_grid(ind),...
        weightOrganic,weightWater);
end

if strcmpi(moduleVer,'1')
    %--------------------------------------------------------------------------
    % Import ueb parameters
    %--------------------------------------------------------------------------
    uebParams = configFiles.params.ueb;

    % Store ueb params as a substruct
    params.ueb = uebParams;
end

%--------------------------------------------------------------------------
% Set up the MUSLE static parameters
%--------------------------------------------------------------------------
params = MUSLE_setup(params,structure);

end


%--------------------------------------------------------------------------
% Helper Functions
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Parameter Mapping Functions
%--------------------------------------------------------------------------
function soilParams = soil_params_layering(inParams,soilThick,numLayers,zerosGrid)
% This function requires only two special variables: soil_lay_top,
% soil_lay_bot, all others are flexible

% Save the input soil parameter data layer top and bottom
inputTop = inParams.soil_lay_top / 100; % these are in cm, be careful if this changes!
inputBot = inParams.soil_lay_bot / 100; % these are in cm, be careful if this changes!

% Calculate the lhmTop and lhmBot arrays
lhmBot = cumsum(soilThick,2);
lhmTop = lhmBot - soilThick;

% Calculate the weight of the input soil data in ILHM layer
inputWeight = cell([1,numLayers]);
numInLayers = size(inputTop,2);
for m = 1:numLayers
    topDiff = inputTop - repmat(lhmTop(:,m),[1,numInLayers]);
    botDiff = inputBot - repmat(lhmBot(:,m),[1,numInLayers]);
    layerThick = repmat(soilThick(:,m),[1,numInLayers]);
    inputWeight{m} = ((layerThick - (topDiff > 0) .* topDiff + (botDiff < 0) .* botDiff) ./ layerThick);
    inputWeight{m}(inputWeight{m} < 0) = 0;
end

% Remove soil_lay_top and soil_lay_bot from inParams
inParams = rmfield(inParams,{'soil_lay_top','soil_lay_bot'});

% Determine which fields to calculate
mapFields = fieldnames(inParams);

% Loop through the fields, calculating the new ILHM layer-weighted averages
soilParams = inParams;
for m = 1:length(mapFields)
    thisParam = mapFields{m};
    tempValue = zerosGrid;
    thisNumLayers = size(inParams.(thisParam),2);
    if thisNumLayers > 1
        for n = 1:numLayers
            tempValue(:,n) = sum(inParams.(thisParam) .* inputWeight{n},2) ./ sum(inputWeight{n},2);
        end
    else
        tempValue = inParams.(thisParam);
    end
    soilParams.(thisParam) = tempValue;
end

end

function [ksat,porosity,fieldCap] = depth_adjust(ksat,porosity,fieldCap,...
    thickness,ksatExp,porosityExp)

% Depth of center of the layer
depth = cumsum(thickness,2) - thickness/2;

% This is from Harr 1977, but is similar to what's done in Olesson et al 2004, p. 115
ksat = ksat .* exp(-depth / ksatExp);
porosity = porosity .* exp(-depth / porosityExp);
fieldCap = fieldCap .* exp(-depth / porosityExp);
end

%--------------------------------------------------------------------------
% Map table creation functions
%--------------------------------------------------------------------------
function luMapTable = lu_map_table_create(configFiles)
% Read in the map data
inStruct = configFiles.tables.map_lu;

% Specify the DBF colname to ILHM name mapping
nameMap = struct('ID','LU_ID','name','LU_NAME','root_beta','ROOTBETA',...
    'canopy_height_max','CANMAX','canopy_height_min','CANMIN','albedo_green','ALBGREEN',...
    'albedo_brown','ALBBROWN','stem_area','STEMAREA','canopy_clumping_index','CANCLUMP',...
    'stomatal_conductance_max','CONDMAX','litter_thickness','LITTHICK','litter_fraction','LITFRAC');

% Loop through the name map array, creating the luMapTable in the proper
% format
mapFields = fieldnames(nameMap);
luMapTable = struct();
for m = 1:length(mapFields)
    thisField = mapFields{m};
    if strcmpi(thisField,{'name'}) % all fields that are character go here
        luMapTable.(thisField) = {inStruct(:).(nameMap.(thisField))};
    else
        luMapTable.(thisField) = [inStruct(:).(nameMap.(thisField))];
    end
end
end

function mapTable = dep_cap_map_table_create(configFiles)
% Read in the map data
mapTable = configFiles.tables.map_dep_cap;
end

function mapTable = irrig_map_table_create(configFiles)
% Read in the map data
mapTable = configFiles.tables.map_irrig;
end

function mapTable = cov_fac_map_table_create(configFiles)
% Read in the map data
mapTable = configFiles.tables.map_cov_fac;
end

%--------------------------------------------------------------------------
% Set up parameters for the MUSLE calculations
%--------------------------------------------------------------------------
function params = MUSLE_setup(params,structure)
% This function sets up the static parameters needed for calculations using
% the Modified Universal Soil Loss Equation (MUSLE).  See Williams(1975),
% Williams(1995), and the SWAT 2009 Documentation for more information on
% the parameters, as well as the algorithm and its development.  Like SWAT,
% LHM estimates the grain size distribution of the eroded soil based on
% Foster et al. (1981).
% Created: 9/12/2013 by Travis Dahl (dahlt@msu.edu)
% Last Modified: 1/7/2019 by Travis Dahl
% 
% MUSLEParameters returns:
% 

% Read inputs
[pctSand, pctClay, pctOrgC, pctFragGt10, pctFrag3to10] = deal_struct(params, ...
    'soil_pct_sand','soil_pct_clay', 'soil_pct_org', 'soil_pct_fraggt10', 'soil_pct_frag3to10');
[cellSize,hillLength,hillSlope] = deal_struct(structure,...
    'cell_resolution','runoff_overland_flowlength', 'runoff_overland_flowslope');

% Units:
% hillLength: slope length (m)
% hillSlope: slope as rise over run (m/m)
% cellSize: m
% pct*: % (0-100)
% support_practice: unitless
% XXX coarse_fragment: ???

% First, want only one layer from each of the soil grids
pctSand = pctSand(:,1);
pctClay = pctClay(:,1);
pctOrgC = pctOrgC(:,1);
pctFragGt10 = pctFragGt10(:,1);
pctFrag3to10 = pctFrag3to10(:,1);

% Make some internal calculations
pctSilt = 100 - pctSand - pctClay;
pctRock = pctFrag3to10 + pctFragGt10;
hillSlope = hillSlope / 100; % comes in as % 
hillLength(hillLength > cellSize) = cellSize; % cap maximum hillslope length

% K: Soil erodibility factor
% According to the SWAT Theoretical Documentation, Version 2009 (pgs.
% 252-256), there are at least 3 different ways to estimate this parameter:
% 1) The USLE soil erodibility factor is
%    (0.013 metric ton * m^2 * hr/(m^3 * metric ton * cm))
% 2) Wischmeier, et al. (1971) calculation
% 3) Williams (1995) equation
%    K_USLE = f_csand * f_cl-si * f_orgc * f_hisand
% For this application, option #3 has been chosen based on the
% availabilityof the required data.
%   f_csand: Coarse Sand Factor
%   NOTE: Williams (1995) gives -0.0256 while the SWAT 2009
%   documentation lists -0.256
soil_erodability = (0.2 + 0.3 * exp(-0.0256 * pctSand .* (1 - pctSilt / 100)));
%   f_cl-si: Clay-Silt Factor
soil_erodability = soil_erodability .* ((pctSilt ./ (pctClay + pctSilt)) .^ 0.3);
%   f_orgc: Organic Carbon Factor
soil_erodability = soil_erodability .* (1 - 0.25 * pctOrgC ./ (pctOrgC + exp(3.72 - 2.95 * pctOrgC)));
%   f_hisand: High Sand Factor
sandFac = (1 - pctSand / 100);
soil_erodability = soil_erodability .* (1 - (0.7 * sandFac) ./ (sandFac + exp(-5.51 + 22.9 * sandFac)));

% C: Cover and management factor
% C is relative to clean-tilled, continuous fallow.  SWAT estimates it
% on a daily basis using:
%    C_USLE = exp([ln(0.8) - ln(C_USLE,mn)] * exp[-0.00115 * rsd_surf]
%    + ln[C_USLE,mn])
% rsd_surf: surface residue (kg/ha)
% The minimum C factor (C_USLE,mn) is estimated based on the work of
% Arnold and Williams (1995):
%    C_USLE,mn = 1.463 * ln(C_USLE,aa) + 0.1034
% C_USLE,aa: average annual C factor
% For now, this uses ILHM's data to calculate surface biomass and residue,
% rather than the SWAT methodology.  Two separate CalcCoverFactor functions
% (calc_cover_factor and dynamic_cover_factor) are called within the canopy
%  evolution module as needed to calculate and update the C factor.

% P: support practice factor
% While this value can be changed to model the effects of certain BMPs,
% it can probably be set to a default value of 1 for the majority of
% situations.  For more information, see SWAT Theoretical
% Documentation, Version 2009 (pgs. 257-259).
support_practice = 1;

% LS: topographic factor (land slope)
% From USDA Agriculture Handbook #537, Wischmeier & Smith (1978)
% According to Wischmeier & Smith (1978): "Slope length is defined as
% the distance from the point of origin of overland flow to the point
% where either the slope gradient decreases enough that deposition
% begins, or the runoff water enters a well-defined channel that may be
% part of a drainage network or a constructed channel (40). A change in
% land cover or a substantial change in gradient along a slope does not
% begin a new slope length for purposes of soil loss estimation."
% (40) Smith & Wischmeier (1957) "Factors affecting sheet and rill
% erosion". American Geophysical Union Transactions 38:889-896
%- Estimated as the average flowpath length to the stream within each grid
% cell
%- Slope is estimated as the average slope within each grid cell
% The follwing equations are from Williams (1995)
m = 0.3 * hillSlope ./ (hillSlope + exp(-1.47 - 61.09 * hillSlope)) + 0.2;
land_slope = (hillLength / 22.1) .^ m .* (65.41 * hillSlope .^ 2 + 4.56 * hillSlope + 0.065);

% CFRG: coarse fragment factor
% pctRock is % rock in first soil layer
% The equation used here is from Williams (1995) and attributed to
% Simanton et al. (1984).
coarse_fragment = exp(-0.03 * pctRock);

% sedArea
% Convert cell size (in m) to hectares and raise to the 0.12 power
sedArea = (cellSize ^ 2 / 10000) .^ 0.12;

% K_P_LS_CFRG
% Multiply all of the factors together that don't change during the
% course of the simulation
K_P_LS_CFRG_sedArea = soil_erodability .* support_practice .* land_slope .* coarse_fragment * sedArea;

% Grain Size Distribution
[pctErodedSand, pctErodedSilt, pctErodedClay] = calcGrainSizeDist(...
    (pctSand / 100), (pctSilt / 100), (pctClay / 100));

% Modify params for function output
params.musle_K_P_LS_CFRG_sedArea = K_P_LS_CFRG_sedArea;
params.pctErodedSand = pctErodedSand;
params.pctErodedSilt = pctErodedSilt;
params.pctErodedClay = pctErodedClay;

end

function [pctErodedSand, pctErodedSilt, pctErodedClay] = ...
    calcGrainSizeDist(pctSand, pctSilt, pctClay)
% This function calculates the estimated contribution of each grain size to
% the eroded sediment.  It is based on Foster et al. (1980) and Foster et al.
% (1981) and is the same methodology used by SWAT and AnnAGNPS.
% Created: 1/9/2019 by Travis Dahl (dahlt@msu.edu)
% Last Modified: 1/9/2019  by Travis Dahl
% 
% NOTE: The input and output arguments are in decimal percent (e.g. 90% =
% 0.9).
% Based on Foster et al. (1981)
% "the model estimates the distribution from the primary particle size
% distribution of the soil mass using the following equations:
%   PSA = SAO (1.0 - CLO)^2.49
%   PSI = 0.13 SIO
%   PCL = 0.2 CLO
%   SAG = 2 CLO if CLO < 0.25
%   SAG = 0.28 (CLO - 0.25) + 0.5 if 0.25 <= CLO <= 0.50
%   SAG = 0.57 if 0.5 < CLO
%   LAG = 1.0 - PSA - PSI - PCL - SAG
% if LAG < 0.0, multiply PSA, PS, PCL, and SAG by same ratio to make:
%   LAG = 0.0
% The primary particle composition of the aggregates is estimated from:
%   CLSAG = SAG [CLO/(CLO + SIO)]
%   SISAG = SAG [SIO/(CLO + SIO)]
%   SASAG = 0.0
%   CLLAG = CLO-PCL-CLSAG
%   SILAG = SIO - PSI - SISAG
%   SALAG = SAO - PSA
% where CLSAG, SISAG, and SASAG = fractions of the total for the sediment
% of, respectively, primary clay, silt, and sand in the small aggregates in
% the sediment load, and CLLAG, SILAG, and SALAG are corresponding
% fractions for the large aggregates.
% 
% If the fraction of clay in the large aggregate based on the mass of the
% large aggregate and not on the total mass of sediment is less than 0.5
% times CLO, the distribution of the particle types is recomputed. A sum of
% Gamma is computed whereby:
%   Gamma = PCL + PSI + PSA
% The fractions PSA, PSI, and PCL are not changed. The new SAG is:
%   SAG = (0.3 + 0.5 Gamma)(CLO + SIO)/[1-0.5(CLO + SIO)] "
% Foster (1980) phrases the last bit slightly differently: "If the clay in
% the large aggregate expressed as a fraction for that particle alone is
% less than 0.5 time ORCL..."
% The code here interprets this as:
%   if CLLAG / (CLLAG + SILAG + SALAG) < 0.5 CLO
% 
% The basic elements (sand/silt/clay)
pctErodedSand = pctSand .* (1 - pctClay).^2.49;
pctErodedSilt = 0.13 .* pctSilt;
pctErodedClay = 0.2 .* pctClay;
% Small Aggregates
smallAgg = pctClay;
smallAgg(smallAgg < 0.25) = 2 .* pctClay(smallAgg < 0.25);
subsetSmallAgg = pctClay >= 0.25 & pctClay <= 0.5;
smallAgg(subsetSmallAgg) = 0.28 .* (smallAgg(subsetSmallAgg) - 0.25) + 0.5;
subsetSmallAgg = pctClay > 0.5;
smallAgg(subsetSmallAgg) = 0.57;
clear subsetSmallAgg;
% Large aggregates
largeAgg = 1 - pctErodedSand - pctErodedSilt - pctErodedClay - smallAgg;

% If largeAgg < 0.0, multiply pctErodedSand, pctErodedSilt, pctErodedClay,
% and smallAgg by the same ratio to make largeAgg = 0.0
subsetLargeAgg = largeAgg < 0;
if any(subsetLargeAgg)
%     largeAgg(subsetLargeAgg) = 0;
    zeroLargeAggFactor = 1 ./ (pctErodedSand(subsetLargeAgg) + ...
        pctErodedSilt(subsetLargeAgg)+ pctErodedClay(subsetLargeAgg) + ...
        smallAgg(subsetLargeAgg));
    pctErodedSand(subsetLargeAgg) = pctErodedSand(subsetLargeAgg) .* ...
        zeroLargeAggFactor;
    pctErodedSilt(subsetLargeAgg) = pctErodedSilt(subsetLargeAgg) .* ...
        zeroLargeAggFactor;
    pctErodedClay(subsetLargeAgg) = pctErodedClay(subsetLargeAgg) .* ...
        zeroLargeAggFactor;
    smallAgg(subsetLargeAgg) = smallAgg(subsetLargeAgg) .* zeroLargeAggFactor;
end
clear subsetLargeAgg;

% Aggregate composition
% Small Aggregates
[claySmallAgg, siltSmallAgg] = grain_size_small_agg(smallAgg, pctClay, pctSilt);

% Large Aggregates
[clayLargeAgg, siltLargeAgg, sandLargeAgg] = grain_size_large_agg( ...
    pctClay, pctSilt, pctSand, pctErodedClay, pctErodedSilt, pctErodedSand, ...
    claySmallAgg, siltSmallAgg);

% Check for < 50% clay in large aggregate
subsetLargeAgg = (clayLargeAgg ./ (clayLargeAgg + siltLargeAgg + sandLargeAgg)) < 0.5;
if any(subsetLargeAgg)
    % gamma = pctClay(subsetLargeAgg) + pctSilt(subsetLargeAgg) + pctSand(subsetLargeAgg);
    gamma = pctErodedClay(subsetLargeAgg) + pctErodedSilt(subsetLargeAgg) + pctErodedSand(subsetLargeAgg);
    smallAgg(subsetLargeAgg) = (0.3 + 0.5 .* gamma) .* (pctClay(subsetLargeAgg) + ...
        pctSilt(subsetLargeAgg)) ./ (1 - 0.5 .* (pctClay(subsetLargeAgg) + ...
        pctSilt(subsetLargeAgg)));

    % Re-calc smallAgg/largeAgg
    [claySmallAgg(subsetLargeAgg), siltSmallAgg(subsetLargeAgg)] = grain_size_small_agg(...
        smallAgg(subsetLargeAgg), pctClay(subsetLargeAgg), pctSilt(subsetLargeAgg));
    [clayLargeAgg(subsetLargeAgg), siltLargeAgg(subsetLargeAgg), sandLargeAgg(subsetLargeAgg)] = ...
        grain_size_large_agg(pctClay(subsetLargeAgg), pctSilt(subsetLargeAgg), ...
        pctSand(subsetLargeAgg), pctErodedClay(subsetLargeAgg), pctErodedSilt(subsetLargeAgg), ...
        pctErodedSand(subsetLargeAgg), claySmallAgg(subsetLargeAgg), siltSmallAgg(subsetLargeAgg));
end
clear subsetLargeAgg;

% Since the aggregates are likely to break up as soon as they enter moving
% water, we'll combine the components of the aggregates with the other
% sand/silt/clay.  (This is the same assumption made by AnnAGNPS.
pctErodedSand = pctErodedSand + sandLargeAgg;
pctErodedSilt = pctErodedSilt + siltSmallAgg + siltLargeAgg;
pctErodedClay = pctErodedClay + claySmallAgg + clayLargeAgg;

% Some minor error-checking to make sure the percentages are reasonable
% XXX Note: w/MATLAB >= R2018A, sum(sum(array)) can also be written as sum(array, 'all')
assert(sum(sum(pctErodedSand < 0)) == 0, 'pctErodedSand has negative values');
assert(sum(sum(pctErodedSand > 1)) == 0, 'pctErodedSand has values > 100% ');
assert(sum(sum(pctErodedSilt < 0)) == 0, 'pctErodedSilt has negative values');
assert(sum(sum(pctErodedSilt > 1)) == 0, 'pctErodedSilt has values > 100% ');
assert(sum(sum(pctErodedClay < 0)) == 0, 'pctErodedClay has negative values');
assert(sum(sum(pctErodedClay > 1)) == 0, 'pctErodedClay has values > 100% ');
assert(sum(sum((pctErodedSand + pctErodedSilt + pctErodedClay) > 1+eps('double'))) == 0, ...
    'Eroded grain size distributions exceeds 100% for some cells');
assert(sum(sum((pctErodedSand + pctErodedSilt + pctErodedClay) < 1-eps('double'))) == 0, ...
    'Eroded grain size distributions exceeds 100% for some cells');

clear sandLargeAgg siltSmallAgg siltLargeAgg claySmallAgg clayLargeAgg;
end

function [smallAggClay, smallAggSilt] = grain_size_small_agg(smallAgg, pctClay, ...
    pctSilt)
% This function estimates the proportion of the eroded small aggregates
% that are made up of silt and clay.  It implements the methodology
% outlined in Foster (1981).
% Created: 12/27/2018 by Travis Dahl (dahlt@msu.edu)
% Last Modified: 12/27/2018 by Travis Dahl

% CLSAG = SAG [CLO/(CLO + SIO)]
smallAggClay = smallAgg .* (pctClay ./ (pctClay + pctSilt));
%       SISAG = SAG [SIO/(CLO + SIO)]
smallAggSilt = smallAgg .* (pctSilt ./ (pctClay + pctSilt));

end

function [largeAggClay, largeAggSilt, largeAggSand] = grain_size_large_agg(...
    pctClay, pctSilt, pctSand, pctErodedClay, pctErodedSilt, ...
    pctErodedSand, claySmallAgg, siltSmallAgg)
% This function estimates the proportion of the eroded large aggregates
% that are made up of silt and clay.  It implements the methodology
% outlined in Foster (1981).
% Created: 12/27/2018 by Travis Dahl (dahlt@msu.edu)
% Last Modified: 1/2/2019 by Travis Dahl

%   CLLAG = CLO-pctErodedClay-CLSAG
largeAggClay = pctClay - pctErodedClay - claySmallAgg;
%   SILAG = SIO - pctErodedSilt - SISAG
largeAggSilt = pctSilt - pctErodedSilt - siltSmallAgg;
%   SALAG = SAO - pctErodedSand
largeAggSand = pctSand - pctErodedSand;

end


%--------------------------------------------------------------------------
% Fill NaN values
%--------------------------------------------------------------------------

function soilParams = fill_nans(soilParams)
% This function fills nan values with the layer means

fields = fieldnames(soilParams);
for m = 1:length(fields)
    thisField = fields{m};
    thisNumLay = size(soilParams.(thisField),2);
    for n = 1:thisNumLay
        testNan = isnan(soilParams.(thisField)(:,n));
        thisMean = mean(soilParams.(thisField)(~testNan,n));
        soilParams.(thisField)(testNan,n) = thisMean;
    end
end

end