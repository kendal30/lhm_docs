function [paths,modelInfo] = initialize_paths(pathConfig,configFile)
%--------------------------------------------------------------------------
% Read in the paths yml
%--------------------------------------------------------------------------
addpath(genpath(pathConfig))
runParams = read_config(configFile,'run');
frontendParams = read_config(runParams.configNotebook,'frontend');

%--------------------------------------------------------------------------
% Create the paths structured array
%--------------------------------------------------------------------------
paths = struct();

% Get the groupName, runName, domainName, and moduleVer
modelInfo.group_name = runParams.groupName;
modelInfo.run_name = runParams.runName;
modelInfo.domain_name = runParams.domainName;
modelInfo.init_time = now;
modelInfo.modflow_only = runParams.modflowOnly;

% Sanitize the group_name, run_name, and domain_name
modelInfo.group_name = sanitize_string(modelInfo.group_name);
modelInfo.run_name = sanitize_string(modelInfo.run_name);
modelInfo.domain_name = sanitize_string(modelInfo.domain_name);

% Modify group_name for optim and modflow runs
if modelInfo.modflow_only
    modelInfo.run_name = [modelInfo.run_name,'-modflow'];
end


% Specify subdirectories
paths.subdirs.source = 'source';
paths.subdirs.groundwater = 'groundwater';
paths.subdirs.groundwater_optim = 'groundwater_optim';
paths.subdirs.config = 'config';
paths.subdirs.split = 'split'; % this will get an underscore and split number appended later

% Specify prefixes
paths.init.prefix.archive = modelInfo.domain_name;
paths.init.prefix.init = 'INIT';
paths.init.prefix.optim = 'OPTIM';
paths.init.prefix.diary = 'LOG';
paths.init.prefix.output = 'OUTF';
paths.init.prefix.dump = 'DUMP';
paths.init.prefix.perc = 'PERC';
paths.init.prefix.transp = 'TRANSP';
paths.init.prefix.runoff = 'RUNF';
paths.init.prefix.simobs = 'OUTS';
paths.init.prefix.split = 'split'; % this will get an underscore and split number appended later

% Specify suffixes
paths.init.suffix.archive = '.zip';
paths.init.suffix.init = '.mat';
paths.init.suffix.optim = '.mat';
paths.init.suffix.diary = '.txt';
paths.init.suffix.output = '.h5';
paths.init.suffix.dump = '.mat';
paths.init.suffix.perc = '.h5';
paths.init.suffix.transp = '.h5';
paths.init.suffix.runoff = '.h5';
paths.init.suffix.simobs = '.mat';

% Specify configuration files
paths.init.files.notebook = runParams.configNotebook;
paths.init.files.model = runParams.configModel;
paths.init.files.params = runParams.configParams;
paths.init.files.run = which(configFile);
[filePath,~,~] = fileparts(paths.init.files.run);

% Handle the param tables
paramTables = read_config(runParams.configParams,'map');
cd(filePath)
paths.init.files.tables = paramTables(:); % list the params tables
paramTableFields = fieldnames(paths.init.files.tables);
for m = 1:length(paramTableFields)
    paths.init.files.tables.(paramTableFields{m}) = which(paths.init.files.tables.(paramTableFields{m}));
end
cd(pathConfig);

% Specify input parameter paths
paths.input.climate = runParams.CLIM;
paths.input.landscape = runParams.LAND;
paths.input.static = runParams.STAT;
paths.input.obs = runParams.OBS;

% Base of filenames
modelBase = [modelInfo.group_name,'_',modelInfo.run_name,'_',datestr(modelInfo.init_time,'yyyymmdd')];

% Create output filenames
paths.output.diary = [paths.init.prefix.diary,'_',modelBase,paths.init.suffix.diary];
paths.output.output = [paths.init.prefix.output,'_',modelBase,paths.init.suffix.output];
paths.output.dump = [paths.init.prefix.dump,'_',modelBase,paths.init.suffix.dump];
paths.output.perc = [paths.init.prefix.perc,'_',modelBase,paths.init.suffix.perc];
paths.output.transp = [paths.init.prefix.transp,'_',modelBase,paths.init.suffix.transp];
paths.output.runoff = [paths.init.prefix.runoff,'_',modelBase,paths.init.suffix.runoff];
paths.output.simobs = [paths.init.prefix.simobs,'_',modelBase,paths.init.suffix.simobs];
paths.output.start = 'model_start.txt';
paths.output.complete = 'model_complete.txt';

% Get the base LHM path
dirModel = frontendParams.pathModel;
dirInput = [dirModel,filesep,'Input'];
dirInit = [dirModel,filesep,'Init'];

% Input files needed for initialization
paths.init.climate = [dirInput,filesep,runParams.CLIM];
paths.init.landscape = [dirInput,filesep,runParams.LAND];
paths.init.static = [dirInput,filesep,runParams.STAT];
paths.init.obs = [dirInput,filesep,runParams.OBS];
paths.init.lhm = frontendParams.pathCode;
paths.init.frontend = frontendParams.pathFrontend;

% Directories to create initialization archive file
paths.init.baseDir = [dirInit,filesep,modelInfo.group_name];
paths.init.dir = [paths.init.baseDir,filesep,modelInfo.run_name];
paths.init.source = [paths.init.dir,filesep,paths.subdirs.source];
paths.init.groundwater = [paths.init.dir,filesep,paths.subdirs.groundwater];
paths.init.groundwater_optim = [paths.init.dir,filesep,paths.subdirs.groundwater_optim];
paths.init.config = [paths.init.dir,filesep,paths.subdirs.config];

% Names of initialization files and archive
paths.init.archive = [dirInit,filesep,paths.init.prefix.archive,'_',modelBase,paths.init.suffix.archive];
paths.init.initFile = [paths.init.prefix.init,'_',modelBase,paths.init.suffix.init];
% These below need to get replaced by the split codes later
paths.init.output.files.init = [paths.init.dir,filesep,paths.init.initFile];
paths.init.output.files.output = [paths.init.dir,filesep,paths.output.output];
paths.init.output.files.perc = [paths.init.dir,filesep,paths.output.perc];
paths.init.output.files.transp = [paths.init.dir,filesep,paths.output.transp];
paths.init.output.files.runoff = [paths.init.dir,filesep,paths.output.runoff];

% Names and paths of optimization files
paths.init.optimFile = [paths.init.prefix.optim,'_',modelBase,paths.init.suffix.optim];
paths.init.output.files.optim = [paths.init.dir,filesep,paths.subdirs.groundwater_optim,filesep,paths.init.optimFile];

% Create the directories needed for initialization, remove it if already
% present and user consents
if exist(paths.init.dir,'dir')==7
    % Prompt to delete preparation directory
    promptString = ['Preparation directory ',strrep(paths.init.dir,'\','\\'),' already exists, delete? (y/n)'];
		if usejava('desktop')
			contPrompt = input(promptString,'s');
		else
			contPrompt = 'y';
		end
    switch lower(contPrompt)
        case 'y'
            rmdir(paths.init.dir,'s')
        case 'n'
            disp('Continuing on top of existing directory, proceed carefully')
            return
    end
end
mkdir(paths.init.dir)
mkdir(paths.init.source)
mkdir(paths.init.config)

end


function str = sanitize_string(str)
    % Convert numeric values to string
    if ~ischar(str),str = num2str(str);end

    % Remove spaces, dashes, and slashes
    str = strrep(str,' ','_');
    str = strrep(str,'_','-');
    str = strrep(str,'/','-');
end