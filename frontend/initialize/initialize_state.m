function [state,structure] = initialize_state(configFiles,structure,params,moduleVer)
% INITIALIZE_STATE  Initializes state variables for ILHM.
%   state = initialize_state(structure,params)
% 
%   This function initializes state variables using custom
%   settings defined in input_structure.
% 
%   Descriptions of Input Variables:
%   structure:  previosuly-defined structure array
%   params:     previously-defined params array
%   moduleVer:  a string with model version number
% 
%   Descriptions of Output Variables:
%   state:      A structured array containing all of the state variables in the
%               model.  State will later be updated with model fluxes as well.
%               But, none are required at initialization.
%   structure:  Updated structure array
% 
%   Example(s):
%   none
% 
%   See also: initialize_params

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
% Define lists of parameters to initialize
%--------------------------------------------------------------------------
initScalar = {'date','timestep','outer_timestep','inner_timestep'};

if strcmpi(moduleVer,'2')
    initSoilGrid = {'therm_cond_soil','temp_soil','soil_frozen_fraction','water_soil',...
        'matric_potential_soil','soil_unsat_conductivity','transp_upland'};
    initSurfaceGrid = {'temp_bed_sediment',... % Bed Sediment
        'rad_long_upland_up','rad_long_wetland_up',... % Longwave radiation
        'water_snow_upland','snow_density_upland','snow_thickness_upland',... % Snowpack upland
        'snow_frozen_fraction_upland','temp_snow_upland','snow_surf_age_upland',...
        'water_snow_wetland','snow_density_wetland','snow_thickness_wetland',... % Snowpack wetland
        'snow_frozen_fraction_wetland','temp_snow_wetland','snow_surf_age_wetland',...
        'temp_ice_pack','water_ice_pack','water_ice_pack_ponding','ice_pack_thickness',... % Ice pack
        'ice_pack_precip_store',...
        'runon_internal_upland','runon_internal_wetland',...
        'therm_cond_snow_upland','therm_cond_snow_wetland','therm_cond_litter',... % Thermal conductances
        'therm_cond_bed_sediments',...
        'temp_interf_upland','temp_interf_wetland','vap_pressure_interf_upland',...
        'vap_pressure_interf_wetland',... % Interface temperatures and vapor pressures
        'water_litter','temp_litter','litter_frozen_fraction','rough_height_litter',...
        'matric_potential_litter',... % Litter
        'water_table_depth_wetland','water_table_depth_upland',... % Groundwater
        'sediment_yield_upland',...
        'water_depression','latent_canopy_transp_upland','water_wetland','sun_zenith_cos',... % Others
        'irrigation_annual_water','irrigation_active','irrigation_event_water',...
		'irrigation_canopy_partition','irrigation_accum_et','irrigation_drift_evap',...
        'irrigation_event_size','irrigation_intensity','irrigation_present',...
        'irrigation_canopy_partition','irrigation_wait_days',...
        'irrigation_last_apply','irrigation_water_applied','irrigation_water_subsurface',...% Irrigation
        'evap_soil','evap_canopy','soil_infiltration','deep_percolation_upland',...% Any necessary fluxes
        'water_balance','energy_balance'}; % Internal ILHM variables
    initLakeGrid = {'temp_wetland','therm_cond_wetland'};
    initCanopyGrid = {'temp_canopy','water_canopy'};

elseif strcmpi(moduleVer,'1')
    initSoilGrid = {'water_soil','transp_upland','soil_unsat_conductivity',};
    initSurfaceGrid = {'water_snow_upland','snow_surf_age_upland','snow_heat_storage_upland',... % Snowpack upland
        'snow_thickness_upland','snow_frozen_fraction_upland','snow_density_upland',...
        'q_cond_ground',...
        'water_ice_pack','water_ice_pack_ponding','ice_pack_thickness',...
        'ice_pack_precip_store',... % Ice pack
        'runon_internal_upland','runon_internal_wetland',...
        'water_table_depth_wetland','water_table_depth_upland',... % Groundwater
        'sediment_yield_upland',...
        'water_depression','sun_zenith_cos','impermeable_frozen',... % Others
        'soil_evap_depth','soil_air_conductivity',...
        'irrigation_annual_water','irrigation_active','irrigation_event_water',...
		'irrigation_canopy_partition','irrigation_accum_et','irrigation_drift_evap',...
        'irrigation_event_size','irrigation_intensity','irrigation_present',...
        'irrigation_canopy_partition','irrigation_wait_days',...
        'irrigation_last_apply','irrigation_water_applied','irrigation_water_subsurface',...% Irrigation
        'water_litter','water_snow_wetland','water_wetland',... % For compatibility with v2 modules
        'evap_soil','evap_canopy','soil_infiltration','deep_percolation_upland',...% Any necessary fluxes
        'water_balance'}; % Internal ILHM variables
    initLakeGrid = {'temp_wetland'};
    initCanopyGrid = {'water_canopy'};
end



%--------------------------------------------------------------------------
% Initialize each array type as appropriate zeros
%--------------------------------------------------------------------------
for m = 1:length(initScalar)
    state.(initScalar{m}) = 0;
end
for m = 1:length(initSoilGrid)
    state.(initSoilGrid{m}) = structure.zeros_grid_soil;
end
for m = 1:length(initSurfaceGrid)
    state.(initSurfaceGrid{m}) = structure.zeros_grid;
end
for m = 1:length(initLakeGrid)
    state.(initLakeGrid{m}) = structure.zeros_grid_wetland;
end
for m = 1:length(initCanopyGrid)
    state.(initCanopyGrid{m}) = structure.zeros_grid;
end

%--------------------------------------------------------------------------
% Load grids
%--------------------------------------------------------------------------
% Import gridded params
stateGrids = load_grid_params(structure.paths,'state','STAT');

% Trim to the domain
stateGrids = trim_domain(structure.trim_struct,stateGrids);

%--------------------------------------------------------------------------
% Define indexes for convenience
%--------------------------------------------------------------------------
indAll = (structure.index_all);
indUpland = (structure.fraction_upland > 0);
indWetland = (structure.fraction_wetland > 0) | (structure.fraction_stream > 0);
% indStream = (structure.fraction_stream > 0);
indSoil = repmat(indUpland,1,structure.num_layers_soil);

% Initialize these as well
structure.index_upland = indUpland;
structure.index_wetland = indWetland;

%--------------------------------------------------------------------------
% Initialize the domain fractions
%--------------------------------------------------------------------------
state.fraction_wetland = structure.fraction_wetland;
state.fraction_upland = structure.fraction_upland;
state.fraction_stream = structure.fraction_stream;

%--------------------------------------------------------------------------
% Load the initial conditions module
%--------------------------------------------------------------------------
init = configFiles.model.state;

%--------------------------------------------------------------------------
% Wetland and bed sediment state, and streams
%--------------------------------------------------------------------------
% Deep lake temperatures
deepIndices = structure.index_deep_wetland;
lakeLayers = structure.num_layers_wetland;
layerTemps = (init.deepWetTempTop:(init.deepWetTempBot-init.deepWetTempTop)/(lakeLayers-1):init.deepWetTempBot);
for m = 1:lakeLayers
    state.temp_wetland(deepIndices,m) = layerTemps(m);
end

if strcmpi(moduleVer,'2')
    % Shallow lake temperatures
    shallowIndices = structure.index_shallow_wetland;
    state.temp_wetland(shallowIndices) = init.shallowWetTemp;

    % Bed sediment temperatures
    state.temp_bed_sediment(deepIndices) = init.deepWetTempBot;
    state.temp_bed_sediment(shallowIndices) = init.shallowWetTemp;

    % Stream velocity
    state.stream_velocity = structure.stream_velocity;
end

% Calculate water density, be careful that this is density stable!
state.wetland_water_density = water_density(state.temp_wetland);

% Wetland water table depth (positive from wetland bottom)
state.water_table_depth_wetland = structure.topography.elevation_min - stateGrids.starting_heads;
state.water_table_depth_upland = structure.topography.elevation_mean - stateGrids.starting_heads;

% Disconnected wetlands
structure.index_disconnected_wetlands = structure.index_wetland & (state.water_table_depth_wetland > params.wetland_disconn_thresh);
structure.index_connected_wetlands = structure.index_wetland & ~structure.index_disconnected_wetlands;

% Wetland water storage
state.water_wetland = init.wetlandWater + zeros(size(structure.wetland_bottom_depth));
structure.index_wetland_flooded = false(size(structure.wetland_bottom_depth));

%--------------------------------------------------------------------------
% Canopy state
%--------------------------------------------------------------------------
state.water_canopy(indAll) = init.canopyWater;

if strcmpi(moduleVer,'2')
    state.temp_canopy(indAll) = init.canopyTemp;
end

%--------------------------------------------------------------------------
% Longwave Radiative Fluxes
%--------------------------------------------------------------------------
if strcmpi(moduleVer,'2')
    state.rad_long_upland_up(indUpland) = init.radLongUplandUp;
    state.rad_long_wetland_up(indWetland) = init.radLongWetlandUp;
end

%--------------------------------------------------------------------------
% Snow packs, ice pack
%--------------------------------------------------------------------------
% Upland snow
state.water_snow_upland(indUpland) = init.uplandSnow;
state.snow_surf_age_upland(indUpland) = init.uplandSnowSurfAge;
if strcmpi(moduleVer,'2')
    state.snow_density_upland(indUpland) = init.uplandSnowDens;
    state.snow_thickness_upland(indUpland) = state.water_snow_upland(indUpland) ./ ...
        state.snow_density_upland(indUpland);
    state.snow_frozen_fraction_upland(indUpland) = init.uplandSnowFrozen;
    state.temp_snow_upland(indUpland) = init.uplandSnowTemp;

elseif strcmpi(moduleVer,'1')
    state.snow_heat_storage_upland(indUpland) = init.uplandSnowHeatStorage;
end

% Initialize the structure variable
structure.index_snow_upland = (state.water_snow_upland > 0);

% Wetland snow
if strcmpi(moduleVer,'2')
    state.water_snow_wetland(indWetland) = init.wetlandSnow;
    state.snow_density_wetland(indWetland) = init.wetlandSnowDens;
    state.snow_thickness_wetland(indWetland) = state.water_snow_wetland(indWetland) ./ ...
        state.snow_density_wetland(indWetland);
    state.snow_frozen_fraction_wetland(indWetland) = init.wetlandSnowFrozen;
    state.temp_snow_wetland(indWetland) = init.wetlandSnowTemp;
    state.snow_surf_age_wetland(indWetland) = init.wetlandSnowSurfAge;
    % Initialize the structure variable
    structure.index_snow_wetland = (state.water_snow_wetland > 0);
end

% Wetland ice
if strcmpi(moduleVer,'2')
    state.temp_ice_wetland(indWetland) = init.wetlandIceTemp;
end
state.water_ice_pack(indWetland) = init.wetlandIce;
state.water_ice_pack_ponding(indWetland) = init.wetlandIcePonding;
state.ice_pack_thickness(indWetland) = init.wetlandIceThick;

% Initialize the structure variable
structure.index_ice_pack = (state.water_ice_pack > 0);
structure.index_ice_pack_ponding = (state.water_ice_pack_ponding > 0);

if strcmpi(moduleVer,'2')
    %--------------------------------------------------------------------------
    % Thermal conductances
    %--------------------------------------------------------------------------
    state.therm_cond_snow_upland(indUpland) = init.thermCondSnowUpland;
    state.therm_cond_snow_wetland(indWetland) = init.thermCondSnowWetland;
    state.therm_cond_litter(indUpland) = init.thermCondLitter;
    state.therm_cond_soil(indSoil) = init.thermCondSoil;
    state.therm_cond_bed_sediments(indWetland) = params.bed_sediment_therm_cond(indWetland);
    state.therm_cond_wetland(indWetland,:) = init.thermCondWetland;

    %--------------------------------------------------------------------------
    % Interface temperatures and vapor pressures
    %--------------------------------------------------------------------------
    state.temp_interf_upland(indUpland) = init.uplandInterface;
    state.temp_interf_wetland(indWetland) = init.wetlandInterface;
    state.vap_pressure_interf_upland(indUpland) = sat_vap_pressure(...
        state.temp_interf_upland(indUpland)) .* init.uplandInterfRelHum;
    state.vap_pressure_interf_wetland(indWetland) = sat_vap_pressure(...
        state.temp_interf_upland(indWetland)) .* init.wetlandInterfRelHum;

    %--------------------------------------------------------------------------
    % Litter
    %--------------------------------------------------------------------------
    % Just initialize these for the whole domain, slight problem here, but
    % litter_fraction isn't defined yet
    state.water_litter(indUpland) = init.litterWater;
    state.temp_litter(indUpland) = init.litterTemp;
    state.litter_frozen_fraction(indUpland) = init.litterFrozenFrac;
    state.rough_height_litter(indUpland) = params.litter_rough_height;
    state.matric_potential_litter(indUpland) = params.litter_dry_potential;
end

%--------------------------------------------------------------------------
% Soil
%--------------------------------------------------------------------------
% Soil Water
state.water_soil(indSoil) = params.soil_field_cap(indSoil) .* init.soilWaterFieldCapMult;

% Soil layer temperatures
if strcmpi(moduleVer,'2')
    soilLayers = structure.num_layers_soil;
    layerTemps = (init.soilTempTop:(init.soilTempBot-init.soilTempTop)/(soilLayers-1):init.soilTempBot);
    for m = 1:soilLayers
        state.temp_soil(indUpland,m) = layerTemps(m);
    end
    state.soil_frozen_fraction(indSoil) = init.soilFrozenFrac;

elseif strcmpi(moduleVer,'1')
    state.impermeable_frozen(indUpland) = init.impermeableFrozen;
end

% Get the initial soil matric potential, kTheta
theta = state.water_soil(indSoil)./structure.thickness_soil(indSoil);
[ktheta,~,psi,~] = van_genuchten_model(theta,params.soil_porosity(indSoil),...
    params.soil_theta_r(indSoil),params.soil_alpha(indSoil),...
    params.soil_N(indSoil),params.soil_ksat(indSoil),params.soil_k0(indSoil),params.soil_L(indSoil)); % #ok<NASGU,ASGLU>
state.soil_unsat_conductivity(indSoil) = ktheta;
if strcmpi(moduleVer,'2')
    state.matric_potential_soil(indSoil) = psi;
end

% Get the initial kAir
thetaAir = params.soil_porosity(indUpland,1) - state.water_soil(indUpland,1)./structure.thickness_soil(indUpland,1);
[kAir] = van_genuchten_model(thetaAir,params.soil_porosity(indUpland,1),...
    params.soil_theta_r(indUpland,1),params.soil_alpha(indUpland,1),...
    params.soil_N(indUpland,1),params.soil_ksat(indUpland,1),params.soil_k0(indUpland,1),params.soil_L(indUpland,1)); % #ok<NASGU,ASGLU>
state.soil_air_conductivity(indUpland,1) = kAir;

%--------------------------------------------------------------------------
% Runoff routed value arrays
%--------------------------------------------------------------------------
% watershed_runoff and watershed_evap_demand have to be initialized in
% the canopy_evolution_module because they depend on properties
% that change with landuse (i.e. runoff_flowtimes).
% Initialize watershed throughflow and watershed baseflow only
if structure.runoff_enabled
    [runoffGauges,dates,throughflowFlowtimes] = deal_struct(structure,...
        'runoff_gauges','dates','runoff_subsurface_flowtimes');

    % Determine the number of gauges
    gauges = count_unique(runoffGauges);
    gauges = gauges(gauges>0);
    numGauges = length(gauges);

    % Initialize them, daily
    state.watershed_throughflow = cell(numGauges,1);
    maxFlowtime = max(throughflowFlowtimes(:));
    numTimestepsFlowtimeDaily = ceil(maxFlowtime / 86400); %because it's daily
    numTimestepsModelDaily = length(dates);
    for m = 1:numGauges
        state.watershed_throughflow{m} = zeros(numTimestepsModelDaily + numTimestepsFlowtimeDaily - 1,1);
    end
    state.watershed_baseflow = state.watershed_throughflow;

    % These will be properly initialized in the canopy_evolution_module
    state.watershed_runoff = {};
    state.watershed_evap_demand = {};
else
    state.watershed_throughflow = {};
    state.watershed_baseflow = {};
    state.watershed_runoff = {};
    state.watershed_evap_demand = {};
end

%--------------------------------------------------------------------------
% Others
%--------------------------------------------------------------------------
state.water_depression(indUpland) = init.depressionWater;
if strcmpi(moduleVer,'2')
    state.latent_canopy_transp_upland(indUpland) = init.latentFluxUpland;
    % wetland doesn't need to be defined because stomatal_conductance doesn't use it
end
state.sun_zenith_cos(indAll) = init.sunZenithCos;
state.musle_qPeak = structure.zeros_grid;
state.musle_qSum = structure.zeros_grid;

end
