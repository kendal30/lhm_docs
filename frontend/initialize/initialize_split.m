function [splitPaths,splitArray] = initialize_split(configFiles,paths)

% Read in the split array, and model_grid
[gridStruct, ~] = load_grid_params(paths,'structure','STAT');

% Check to see if a slice was provided, if not, set as model grid
if ~isfield(gridStruct,'slice')
    sliceArray = gridStruct.model_grid;
else
    sliceArray = gridStruct.slice;
end

% Now, check to see if we are running the full domain, overwrite slice if so
if configFiles.model.structure.run_full_domain
    sliceArray = gridStruct.model_grid;
end

% Determine whether we actually need to continue
numSplits = configFiles.model.structure.num_splits;

if numSplits == 1
    splitPaths = struct();
    splitArray = (sliceArray > 0);
    return
end

% Overwrite the runoff code currently, eventually work around this
if configFiles.model.structure.runoff_enabled
    disp('Turning runoff off, currently not compatible with split')
    configFiles.model.structure.runoff_enabled = 0;
end

% Check to see if a split array is specified, if it is, then incorporate the
% slice properly. If it is not, build a split array
splitArray = zeros(size(sliceArray));
if isfield(gridStruct,'split')
    % Use the slice array to initialize the split array
    splitArray(sliceArray>0) = gridStruct.split(sliceArray>0);
    
    % Now, renumber using unique values of the array
    splitVals = count_unique(splitArray);
    splitVals = splitVals(splitVals>0);
    for m = 1:length(splitVals)
        splitArray(splitArray==splitVals(m)) = m;
    end
else
    % Incorporate the model slice 
    splitArray(sliceArray>0) = 1;

    % Pass this to the spatial clustering algorithm
    [splitArray] = spatial_clustering(splitArray,numSplits);    
end

% Set up a structure to change the paths later in initialization_controller
splitPaths = struct('dir',[],'files',[]);
for m = 1:numSplits
    % Create a split sub-path
    thisPath = [paths.init.dir,filesep,sprintf('%s_%d',paths.subdirs.split,m)];
    splitPaths(m).dir = thisPath;
    % Create a prefix for each split file
    thisPrefix = sprintf('%s_%d_',paths.init.prefix.split,m);
    splitPaths(m).output.diary = [thisPrefix,paths.output.diary];
    splitPaths(m).output.output = [thisPrefix,paths.output.output];
    splitPaths(m).output.dump = [thisPrefix,paths.output.dump];
    splitPaths(m).output.perc = [thisPrefix,paths.output.perc];
    splitPaths(m).output.transp = [thisPrefix,paths.output.transp];
    splitPaths(m).output.runoff = [thisPrefix,paths.output.runoff];

    splitPaths(m).files.init = [thisPath,filesep,thisPrefix,paths.init.initFile];
    splitPaths(m).files.output = [thisPath,filesep,thisPrefix,paths.output.output];
    splitPaths(m).files.perc = [thisPath,filesep,thisPrefix,paths.output.perc];
    splitPaths(m).files.transp = [thisPath,filesep,thisPrefix,paths.output.transp];
    splitPaths(m).files.runoff = [thisPath,filesep,thisPrefix,paths.output.runoff];

end
end


function [clusterMap] = spatial_clustering(modelGrid,numGroups)
% This algorithm initially splits the grid into nearest neighbor groups
% Then, it iteratively enlarges the clusters, shifting the centroids
% to achieve roughly equal areas

% Parameter that control the function of this algorithm
gain = 2; %to make centroid move aggressively
stopThresh = 0.05; %percent discrepancy between biggest and smallest group
mobileFrac = 1.4; %allowing more cells to move allows quicker convergence

% Compute number of cells desired in each cluster
numCells = floor(nansum(modelGrid(:)>0)/numGroups);

% Get the i, j and ind
[modInd] = find(modelGrid);
[modI,modJ] = find(modelGrid);

% Run kmeans
[clusters,centroids] = kmeans([modI,modJ],numGroups);

%% Map the clusters
clusterMap = modelGrid;
clusterMap(modInd) = clusters;

% Loop through the clusters, moving the centroids as needed to achieve
% nearest equal size grouping
allInd = (1:length(clusters))';
diffRange = 1;
count = 0;
while diffRange > stopThresh
    % Compute distance from each cell to the cluster centroids
    [nearCentroids] = knnsearch(centroids,[modI,modJ],'K',numGroups);
    
    % Count number of cells in each cluster
    [~,groupCount] = count_unique(clusters);
    [~,sortOrder] = sort(groupCount,'ascend');
    
    % Each cluster needing cells then gets a chance to select the closest
    % free cells, must be second-closest centroid though
    %moveCells = zeros(size(clusters));
    for m = 1:numGroups
        sortInd = sortOrder(m);
        if groupCount(sortInd) <= numCells * mobileFrac
            numMove = ceil((numCells - groupCount(sortInd))*mobileFrac);
            indSecond = ((nearCentroids(allInd,1)==sortInd) | (nearCentroids(allInd,2)==sortInd)) & ...
                (clusters(allInd)~=sortInd);  % Determine if this centroid is the first or second-nearest for those cells
            thisInd = allInd(indSecond);
            [~,thisDistance] = knnsearch(centroids(sortInd,:),[modI(thisInd),modJ(thisInd)]);
            [~,indSort] = sort(thisDistance); % Sort cells by distance from this centroid
            numMove = min(numMove,length(indSort));
            indMove = indSort(1:numMove); %within the subset thisInd
            indMoveOrig = thisInd(indMove); %within allInd
            clusters(indMoveOrig) = sortInd;
            %moveCells(indMoveOrig) = sortInd; 
        end
    end
    % Move cells to new clusters, if claimed;
    %clustersOrig = clusters;
    %clusters = moveCells;
    %indUnclaimed = clusters==0;
    %clusters(indUnclaimed) = clustersOrig(indUnclaimed);
    
    % Calculate new centroid
    clusterMap = modelGrid;
    clusterMap(modInd) = clusters;
    clusterMap(isnan(clusterMap)) = 0;
    oldCentroids = centroids;
    centroids = regionprops(clusterMap,  'Centroid');
    centroids = fliplr(reshape([centroids(:).Centroid],[2,numGroups])');
    
    % Get the movement vector, apply gain
    vec = (centroids - oldCentroids);
    centroids = oldCentroids + vec * gain;
    
    count = count + 1;
    [~,groupCount] = count_unique(clusters);
    diffRange = (max(groupCount) - min(groupCount))/mean(groupCount);
end
end