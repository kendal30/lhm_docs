function initialization_controller(pathConfig,configFile)
% INITIALIZATION_CONTROLLER  Creates the LHM initialization file.
%   initialization_controller(configFile)
% 
%   The input(s) must be whole or partial LHM configuration XLS files. The
%   first one supplied should be viewed as the base parameter set, while
%   all others supply refinements of these parameters, overwriting those of
%   the base parameter set. This can be used for running multiple related
%   scenarios with potentially only slightly varied parameters.
% 
%   The data_catalog should not be in the modified config files.
% 
%   Descriptions of Input Variables:
%   specFile:   name of the .XLS LHM configuration file, can be a cell
%               array of strings
% 
%   Descriptions of Output Variables:
%   none, all are written to the initialization archive
% 
%   Example(s):
%   >> initialization_controller('c:\users\anthony\inputs\ilhm_config.xls');
% 
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-09
% Copyright 2008 Michigan State University.

% Get the paths structured array
[paths,modelInfo] = initialize_paths(pathConfig,configFile);
initPaths = paths.init;

fprintf('Preparing run %s of group %s\n',modelInfo.run_name,modelInfo.group_name);

% Read in the config files
disp('Reading the config files');
configFiles = read_config_files(paths.init.files);

% Determine which modules LHM will run
[modules] = initialize_modules(configFiles);

% Set up the model splits
[splitPaths,splitArray] = initialize_split(configFiles,paths);
modelInfo.split_slice = splitArray;
modelInfo.this_split = 0;
numSplit = configFiles.model.structure.num_splits;

disp('Initializing model structure, parameters, and state')
%--------------------------------------------------------------------------
% Initialize the base model (or only model if not using splits)

% Call the init_model_split function
[structure,buffers,params,state] = init_model_split(configFiles,paths,modelInfo,modules);

% Initialize MODFLOW
if structure.groundwater_enabled
    disp('    Initializing groundwater model input files');
    
    % Create the directory for the groundwater model
    if structure.groundwater_optim_enabled
        paths.init.groundwater = paths.init.groundwater_optim;
        structure.paths = paths;
    end
    mkdir(paths.init.groundwater)

    % Now, initialize MODFLOW. Modifies the structure.paths variable if optimization enabled
    [groundwater,structure] = initialize_modflow(configFiles,structure);
    paths = structure.paths;
else
    groundwater = [];
end

% Save the LHM data structures for initialization/optimization data to a .mat file
if ~structure.groundwater_optim_enabled
    disp('    Saving INIT .mat file')
    structure.paths = rmfield(structure.paths,'init');  % Clean up the paths array to remove initialization cruft
    save(paths.init.output.files.init,'structure','buffers','params','state','modules','groundwater');
else
    disp('    Saving OPTIM .mat file')
    save(paths.init.output.files.optim,'groundwater');
end

%-----------------------------------------------------------------
% Now, process the model splits
if numSplit > 1 && (~structure.groundwater_optim_enabled && ~modelInfo.modflow_only)
    origPaths = paths.init;
    % Set up a loop to run this next piece as many times as needed to split the model into sub-processes
    for m = 1:numSplit
        fprintf('Initializing model split %d of %d\n',m,numSplit);

        % Set the path structure for this model
        paths.init.dir = splitPaths(m).dir;
        paths.init.output.files = splitPaths(m).files;
        
        thisFilenames = fieldnames(splitPaths(m).output);
        for n = 1:length(thisFilenames)
            paths.output.(thisFilenames{n}) = splitPaths(m).output.(thisFilenames{n});
        end
        modelInfo.this_split = m;

        % Create the directory for this
        mkdir(paths.init.dir);
        
        % Call the init_model_split function
        [structure,buffers,params,state] = init_model_split(configFiles,paths,modelInfo,modules);

        % Save the MATLAB INIT file
        disp('    Saving INIT .mat file')
        structure.paths = rmfield(structure.paths,'init');  % Clean up the paths array to remove initialization cruft
        save(paths.init.output.files.init,'structure','buffers','params','state','modules');
    end
    paths.init = origPaths; % restore the paths to the base
end

%--------------------------------------------------------------------------
% Create the initialization archive and delete preparation directory
fprintf('Creating INIT archive:\n\t%s\n',initPaths.archive);

% Create the lhm_start.m file
if (~structure.groundwater_optim_enabled && ~modelInfo.modflow_only)
    create_lhm_start(paths)

    % Copy the source into the preparation source directory
    copy_source(paths.init.lhm, paths.init.source,true);
elseif structure.groundwater_optim_enabled
    % Remove the source directory from the INIT package
    rmdir(paths.init.source);
    rmdir(paths.init.config);

    % Overwrite the paths.init.config location with the optimization location
    paths.init.config = paths.init.groundwater_optim;

    % Copy MODFLOW writing files to the optimization directory
    copy_optim(paths.init.frontend, paths.init.groundwater_optim, paths.init.optim);
end

% Also copy the config files
copy_config(paths.init.files, paths.init.config);

% Create the archive
create_archive(paths)
end

%--------------------------------------------------------------------------
% Helper Functions
%--------------------------------------------------------------------------
function [structure,buffers,params,state] = init_model_split(configFiles,paths,modelInfo,modules)
% Build the model structure structured array
[structure,modules] = initialize_structure(configFiles,paths,modelInfo,modules);

% Build the model params structured array
params = initialize_params(configFiles,structure,modules.modulesVersion);

% Check to see if this is a groundwater model optimization run, and break if so
if structure.groundwater_optim_enabled || modelInfo.modflow_only
    if structure.groundwater_optim_enabled
        disp('    Groundwater optimization run active');
    else
        disp('    MODFLOW only run active');
    end
    buffers = struct();
    state = struct();
    return
end

% Build the initial state structured array
[state,structure] = initialize_state(configFiles,structure,params,modules.modulesVersion);

% Initialize the output and input buffers
buffers = initialize_buffers(structure);

% Now, run the output intialization routine
disp('    Initializing output file');
buffers.output = initialize_output(configFiles,structure);

if structure.groundwater_enabled
    % Create the groundwater coupling file outputs
    disp('    Initializing groundwater coupling output files')
    initialize_coupling(structure,buffers);
end
end


function configStruct = read_config_files(paths,option)
if nargin<2
    option = false; % if not specified, the 'map' option is not used
end
fileTypes = fieldnames(paths);
for m = 1:length(fileTypes)
    thisFile = paths.(fileTypes{m});
    if isstruct(thisFile)
        thisConfig = read_config_files(thisFile,'map');
    else
        thisConfig = read_config(thisFile,'',option);
    end
    if m > 1
        configStruct.(fileTypes{m}) = thisConfig;
    else
        configStruct = struct(fileTypes{m},thisConfig);
    end
end
end


function create_lhm_start(paths)
codeString = ...
    ["function lhm_start(inputDir,varargin)",...
    "mode = 'start';",...
    "%% Set these defaults here",...
    "currDir = pwd;",...
    "startFile = [currDir,filesep,'%s'];",...
    "%% Check to see if this is a restart",...
    "if nargin==2",...
    "\tswitch varargin{1}",...
    "\t\tcase {'restart','respawn'}",...
    "\t\t\tmode = 'respawn';",...
    "\t\t\tdumpFile = dir('DUMP*.mat');",...
    "\t\t\tstartFile = [currDir,filesep,dumpFile.name];",...
    "\t\totherwise",...
    "\t\t\t%% Nothing to change",...
    "\tend",...
    "end",...
    "%% Now run",...
    "cd([currDir,filesep,'source']);",...
    "lhm_controller(mode,startFile,inputDir)"];
fid = fopen([paths.init.dir,filesep,'lhm_start.m'],'wt');
fprintf(fid,strjoin(codeString,'\n'),paths.init.initFile);
fclose(fid);
end


function create_archive(paths)
% Create the archive, delete if already present, delete the preparation
% directory
if exist(paths.init.archive,'file')>0
    delete(paths.init.archive);
end
zip(paths.init.archive,paths.init.baseDir);
rmdir(paths.init.baseDir,'s');
end


function copy_config(configFiles,pathConfig)
% Copies config notebooks and .csv files to the config directory
if ischar(configFiles)
    copyfile(configFiles,pathConfig);
else
    if isstruct(configFiles)
        fields = fieldnames(configFiles);
        numEntries = length(fields);
    else
        numEntries = length(configFiles);
    end
    for m = 1:numEntries
        if isstruct(configFiles)
            copy_config(configFiles.(fields{m}),pathConfig);
        else
            copy_config(configFiles{m},pathConfig); % Call recursively
        end
    end
end
end


function copy_optim(inDir,outDir,optimFiles)
% Copies the optimization live notebook and the MODFLOW writing files to the 
% groundwater optimization directory

% Copy the optimization live notebook and the wrapper
thisDir = [inDir,filesep,'templates',filesep,'notebooks_optim'];
copyfile([thisDir,filesep,optimFiles.notebook],[outDir,filesep,optimFiles.notebook]);
copyfile([thisDir,filesep,optimFiles.notebook],[outDir,filesep,optimFiles.function]);

% Copy the helper MODFLOW file writing utility
thisDir = [inDir,filesep,'helper',filesep,'modflow'];
for m = 1:length(optimFiles.helper)
    copyfile([thisDir,filesep,optimFiles.helper{m}],[outDir,filesep,optimFiles.helper{m}]);
end
end



function copy_source(inDir,outDir,flatFlag)
% COPY_SOURCE  Copies source files from one directory to another
%   copy_source(inDir,outDir,flatFlag)
% 
%   Descriptions of Input Variables:
%   inDir:      input directory
%   outDir:     output directory
%   flatFlag:   true/false flag to copy all files into same directory, like
%               filenames will generate an error. False is the default.

if nargin < 3
    flatFlag = false;
end

% List acceptable filetypes
fileTypes = {'.m','.csv'};% ,'.mexw32','.mexw64'};

allFiles = {};
for m = 1:length(fileTypes)
    dirStruct = dir([inDir,'\**\*',fileTypes{m}]);
    for n = 1:length(dirStruct)
        dirStruct(n).path = [dirStruct(n).folder,filesep,dirStruct(n).name];
    end
    addFiles = struct2cell(dirStruct);
    allFiles = [allFiles,addFiles{end,:}]; % #ok<AGROW>
end

for m = 1:length(allFiles)
    [thisDir,thisFile,thisExt] = fileparts(allFiles{m});
    if flatFlag
        thisOutDir = outDir;
    else
        % Determine the relative path to the current directory
        baseLength = length(inDir);
        thisLength = length(thisDir);
        if thisLength > baseLength
            relPath = thisDir(baseLength+2:end);
            thisOutDir = [outDir,filesep,relPath];
            if ~(exist(thisOutDir,'dir')==7)
                mkdir(thisOutDir)
            end
        else
            thisOutDir = outDir;
        end
    end
    outFile = [thisOutDir,filesep,thisFile,thisExt];
    if exist(outFile,'file')>0
        error(['File ',outFile,' already exists']);
    end
    copyfile(allFiles{m},outFile);
end
end
