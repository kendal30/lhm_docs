function [varargout]=data_controller(varargin)
%This function loads, stores, and passes input parameters and gridded data
%to functions that call it.  The first calling argument must be one of a
%these types: 'params','state','visualization', or the empty string ''.  The second calling argument
%must be one of: 'load', 'pass', 'update', 'list', 'save', 'load disk', or 'wipe'.  The
%empty string is used for arguments that act on all stored data, currently only
%'wipe', 'list', and 'save'.
%
%If 'load' is used, the third argument is a structured array containing
%the state variables.
%
%If 'pass' is used, then string inputs are used to lookup stored values
%held as PERSISTENT.  These variables are then passed as output in the
%order called.
%
%If 'update' is used, then a sequence of calls that goes as "string",
%"variable", "string", "variable" is required.  The string names the
%variable to be updated, and the variable of course passes the updated
%values.  This new value will then be passed along with all future 'pass'
%calls.  'update' can also be used to add variables to the
%params_controller.
%
%If 'list' is used, the function passes its stored parameters back to the
%calling workspace.
%
%If 'save' is used, the third argument must be the save path as a
%structured array. 'load disk' is the inverse operation of 'save' and is
%intended to reload a file created using 'save'.  The third argument must
%be the load path.
%
%If 'wipe' is used with no inputs, than the entire data structure will be
%wiped.  If the first input specifies a stored array, and the second is 
%'wipe', than just that array will be wiped.

persistent data
if (nargin > 1)
    dataset = varargin{1};
    mode = lower(varargin{2});
    if ~ischar(mode),error('The second argument must be a mode declaration, see "help"'),end
    switch mode
        case 'pass'
            if nargout==(nargin-2)
                for m=3:nargin
                    varargout{m-2} = data.(dataset).(varargin{m});
                end
            elseif nargout == 1 %Pass output as a structured array
                varargout{1} = struct();
                for m = 3:nargin
                    varargout{1}.(varargin{m}) = data.(dataset).(varargin{m});
                end
            else
                error('Number of requested variables must match the number of requested output variables')
            end
        case 'update'
            num_update=(nargin-2)/2;
            if floor(num_update)==num_update
                for m=1:num_update
                    if ischar(varargin{2*m+1})
                        data.(dataset).(varargin{2*m+1})=varargin{2*m+2};
                    else
                        error('The number of calling arguments must be a multiple of 2, following the format specified in "help"')
                    end
                end
            else
                error('The number of calling arguments must be a multiple of 2, following the format specified in "help"')
            end
        case 'load'
            data.(dataset)=varargin{3};
        case 'list'
            if isempty(dataset)
                varargout{1}=data;
            else
                if ~isfield(data,dataset)
                    varargout{1} = [];
                else
                    varargout{1}=data.(dataset);
                end
            end
        case 'save'
            output_file = varargin{3};
            if exist(output_file,'file')>1
                if isempty(dataset)
                    save(output_file,'-struct','data','-append');
                else
                    eval([dataset,'=data.(''',dataset,''');']);
                    eval(['save(output_file,''',dataset,''',''-append'');']);
                end
            else
                if isempty(dataset)
                    save(output_file,'-struct','data');
                else
                    eval([dataset,'=data.(''',dataset,''');']);
                    eval(['save(output_file,''',dataset,''');']);
                end
            end
        case 'load disk'
            input_file = varargin{3};
            if isempty(dataset)
                data = load(input_file);
            else
                loadData = load(input_file,dataset);
                data.(dataset) = loadData.(dataset);
            end
        case 'wipe'
            if isempty(dataset)
                data=[];
            else
                if isfield(data,dataset)
                    data = rmfield(data,dataset);
                end
            end
        otherwise
            error('Unrecognized method for data_controller');
    end
else
    error('At least two inputs are required')
end


