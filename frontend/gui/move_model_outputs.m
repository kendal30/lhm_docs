function varargout = move_model_outputs(varargin)
% MOVE_MODEL_OUTPUTS MATLAB code for move_model_outputs.fig
%      MOVE_MODEL_OUTPUTS, by itself, creates a new MOVE_MODEL_OUTPUTS or raises the existing
%      singleton*.
%
%      H = MOVE_MODEL_OUTPUTS returns the handle to a new MOVE_MODEL_OUTPUTS or the handle to
%      the existing singleton*.
%
%      MOVE_MODEL_OUTPUTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MOVE_MODEL_OUTPUTS.M with the given input arguments.
%
%      MOVE_MODEL_OUTPUTS('Property','Value',...) creates a new MOVE_MODEL_OUTPUTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before move_model_outputs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to move_model_outputs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help move_model_outputs

% Last Modified by GUIDE v2.5 12-Jul-2019 07:51:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @move_model_outputs_OpeningFcn, ...
    'gui_OutputFcn',  @move_model_outputs_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before move_model_outputs is made visible.
function move_model_outputs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to move_model_outputs (see VARARGIN)

% Choose default command line output for move_model_outputs
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes move_model_outputs wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = move_model_outputs_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editOrig_Callback(hObject, eventdata, handles)
% hObject    handle to editOrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editOrig as text
%        str2double(get(hObject,'String')) returns contents of editOrig as a double


% --- Executes during object creation, after setting all properties.
function editOrig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editOrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonOrig.
function pushbuttonOrig_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonOrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fullPath] = uigetdir();
set(handles.editOrig,'String',fullPath);


function editNew_Callback(hObject, eventdata, handles)
% hObject    handle to editNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editNew as text
%        str2double(get(hObject,'String')) returns contents of editNew as a double


% --- Executes during object creation, after setting all properties.
function editNew_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonNew.
function pushbuttonNew_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fullPath] = uigetdir();
set(handles.editNew,'String',fullPath);


% --- Executes on button press in checkboxDelete.
function checkboxDelete_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxDelete
toggle = get(hObject,'Value');
if toggle
    set(handles.pushbuttonExec,'String','Move Outputs');
else
    set(handles.pushbuttonExec,'String','Copy Outputs');
end

% --- Executes on button press in pushbuttonExec.
function pushbuttonExec_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonExec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
origPath = get(handles.editOrig,'String');
newBase = get(handles.editNew,'String');
newLocal = get(handles.editLocal,'String');

% Get the options flags
flagCopy = get(handles.checkboxCopy,'Value');
flagDelete = get(handles.checkboxDelete,'Value');
flagBig = get(handles.checkboxBig,'Value');

% Execute the move
[newPath,newLocalPath] = move_model_files(origPath,newBase,newLocal,flagCopy,flagDelete,flagBig);
move_model_paths(newPath,newLocalPath)

% --- Executes on button press in checkboxBig.
function checkboxBig_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxBig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxBig

% --- Executes on button press in checkboxCopy.
function checkboxCopy_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxCopy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxCopy


function editLocal_Callback(hObject, eventdata, handles)
% hObject    handle to editLocal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editLocal as text
%        str2double(get(hObject,'String')) returns contents of editLocal as a double


% --- Executes during object creation, after setting all properties.
function editLocal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editLocal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%--------------------------------------------------------------------------
% Helper Functions
%--------------------------------------------------------------------------
function move_model_paths(newRemotePath,newLocalPath)
%{
This function facilitates moving an LHM simulation output from one location
to another. It assumes that the model files are setup from a base modeling
directory, and then replaces that, and saves the result back to the DUMP
file.
%}

%Get the DUMP file location from the origPath
dumpFile = dir([newRemotePath,filesep,'DUMP*.mat']);
inputDUMP = [dumpFile.folder,filesep,dumpFile.name];
loadStructure = load(inputDUMP, 'structure');
paths = loadStructure.structure.paths;

% Identify the previous base path, essentially everything before the
% 'Output' folder.
if isfield(paths,'ilhm')
    prevBaseEnd = strfind(paths.ilhm,'\Output\')-1;
    prevBase = paths.ilhm(1:prevBaseEnd);
elseif isfield(paths,'lhm')
    prevBaseEnd = strfind(paths.lhm,'\Output\')-1;
    prevBase = paths.lhm(1:prevBaseEnd);
elseif isfield(paths,'source')
    prevBaseEnd = strfind(paths.source,'\Output\')-1;
    prevBase = paths.source(1:prevBaseEnd);
end

% Get the new base path
newBaseEnd = strfind(newLocalPath,'\Output\')-1;
newBase = newLocalPath(1:newBaseEnd);

% Go through, replacing all base inputs with this
if isfield(paths,'ilhm')
    paths.ilhm = strrep(paths.ilhm, prevBase, newBase);
elseif isfield(paths,'lhm')
    paths.lhm = strrep(paths.lhm, prevBase, newBase);
elseif isfield(paths,'source')
    paths.source = strrep(paths.source, prevBase, newBase);
end

% Build the input file names
fileList = fieldnames(paths.input);
for m = 1:length(fileList)
    paths.input.(fileList{m}) = strrep(paths.input.(fileList{m}), prevBase, newBase);
end

% Build the output file names
fileList = fieldnames(paths.output);
for m = 1:length(fileList)
    paths.output.(fileList{m}) = strrep(paths.output.(fileList{m}), prevBase, newBase);
end

% Save this
structure = loadStructure.structure;
structure.paths = paths;
save(inputDUMP, 'structure', '-append');


function [outPath,outLocalPath] = move_model_files(inPath,outBase,outLocalBase,...
    flagCopy,flagDelete,flagBig)

% Parse the inputs
if iscell(inPath)
    inPath = inPath{1};
end
if iscell(outBase)
    outBase = outBase{1};
end
if iscell(outLocalBase)
    outLocalBase = outLocalBase{1};
end

% First, get the path beneath the inBase directory (that just above the
% output folder)
inBaseEnd = strfind(inPath,'\Output\')-1;
inBase = inPath(1:inBaseEnd);
inSubPath = inPath(inBaseEnd+1:end);

% Get the output path
outPath = [outBase,inSubPath];
outLocalPath = [outLocalBase,inSubPath];

if flagCopy
    % Now, get the files to copy
    inFiles = dir([inPath,filesep,'**\*.*']);
    
    % Remove the hidden or non-relevant folders and subfolders
    keepFolders = true(size(inFiles));
    for m = 1:length(keepFolders)
        if any(strcmpi(inFiles(m).name,{'.','..'}))
            keepFolders(m) = false;
        end
        % Remove hidden directories
        if strcmpi(inFiles(m).name(1),'.')
            keepFolders(m) = false;
        end
    end
    inFiles = inFiles(keepFolders);
    
    % If flagBig is true, keep files, otherwise remove them
    if ~flagBig
        copyFiles = inFiles;
        keepFiles = true(size(copyFiles));
        for m = 1:length(keepFiles)
            if any(strcmpi(copyFiles(m).name(end-3:end),{'.ccf','.dat'}))
                keepFiles(m) = false;
            end
        end
        copyFiles = copyFiles(keepFiles);
    end
    
    % Create the folders
    if ~exist(outPath,'dir')
        mkdir(outPath);
    end
    for m = 1:length(inFiles)
        if inFiles(m).isdir
            parentFolder = strrep(inFiles(m).folder,inBase,outBase);
            if ~exist([parentFolder,filesep,inFiles(m).name],'dir')
                mkdir(parentFolder,inFiles(m).name);
            end
        end
    end
    
    % Copy the files
    h = waitbar(0,'Copying model files');
    totalBytes = sum([copyFiles(:).bytes]);
    copiedBytes = 0;
    for m = 1:length(copyFiles)
        waitbar(copiedBytes/totalBytes,h);
        parentFolder = strrep(copyFiles(m).folder,inBase,outBase);
        if ~copyFiles(m).isdir
            if ~exist([parentFolder,filesep,copyFiles(m).name],'file')
                copyfile([copyFiles(m).folder,filesep,copyFiles(m).name],...
                    [parentFolder,filesep,copyFiles(m).name]);
            end
        end
        copiedBytes = copiedBytes + copyFiles(m).bytes;
    end
    if ishandle(h);close(h);end
    
    % If we are deleting the original location
    if flagDelete
        disp('Removing the original files');
        
        % Delete the files
        for m = 1:length(inFiles)
            if ~inFiles(m).isdir
                delete([inFiles(m).folder,filesep,inFiles(m).name]);
            end
        end
        
        % Delete the folders
        for m = length(inFiles):-1:1
            if inFiles(m).isdir
                rmdir([inFolders(m).folder,filesep,inFolders(m).name]);
            end
        end
    end
end
