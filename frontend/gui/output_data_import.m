function varargout = output_data_import(varargin)
% OUTPUT_DATA_IMPORT M-file for output_data_import.fig
%      OUTPUT_DATA_IMPORT, by itself, creates a new OUTPUT_DATA_IMPORT or raises the existing
%      singleton*.
%
%      H = OUTPUT_DATA_IMPORT returns the handle to a new OUTPUT_DATA_IMPORT or the handle to
%      the existing singleton*.
%
%      OUTPUT_DATA_IMPORT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OUTPUT_DATA_IMPORT.M with the given input arguments.
%
%      OUTPUT_DATA_IMPORT('Property','Value',...) creates a new OUTPUT_DATA_IMPORT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before output_data_import_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to output_data_import_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help output_data_import

% Last Modified by GUIDE v2.5 01-Oct-2021 16:06:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @output_data_import_OpeningFcn, ...
                   'gui_OutputFcn',  @output_data_import_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before output_data_import is made visible.
function output_data_import_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to output_data_import (see VARARGIN)

% Choose default command line output for output_data_import
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%switch_view('file',handles);
% UIWAIT makes output_data_import wait for user response (see UIRESUME)
% uiwait(handles.figureVis);


% --- Outputs from this function are returned to the command line.
function varargout = output_data_import_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in selectFile.
function selectFile_Callback(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName,pathName] = uigetfile('*OUTF*.h5');
set(handles.editFile,'String',[pathName,fileName]);
reset_file(handles);
switch_view('file',handles);

function editFile_Callback(hObject, eventdata, handles)
% hObject    handle to editFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFile as text
%        str2double(get(hObject,'String')) returns contents of editFile as a double
reset_file(handles);
switch_view('file',handles);

% --- Executes during object creation, after setting all properties.
function editFile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in selectFileDiff.
function selectFileDiff_Callback(hObject, eventdata, handles)
% hObject    handle to selectFileDiff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName,pathName] = uigetfile('*OUTF*.h5');
set(handles.editFileDiff,'String',[pathName,fileName]);
reset_file(handles);
switch_view('file',handles);


function editFileDiff_Callback(hObject, eventdata, handles)
% hObject    handle to editFileDiff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFileDiff as text
%        str2double(get(hObject,'String')) returns contents of editFileDiff as a double
reset_file(handles);
switch_view('file',handles);

% --- Executes during object creation, after setting all properties.
function editFileDiff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFileDiff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes when selected object is changed in selectView.
function selectView_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in selectView 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

button = get(eventdata.NewValue,'Tag');
newView = lower(button(5:end));
switch_view(newView,handles)


% --- Executes on selection change in listTypes.
function listTypes_Callback(hObject, eventdata, handles)
% hObject    handle to listTypes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listTypes contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listTypes

types = cellstr(get(handles.listTypes,'String'));
thisType = types{get(handles.listTypes,'Value')};
view = get(handles.selectView,'UserData');
hierarchy = view.Hierarchy;
if isfield(hierarchy,thisType)
    if ~isempty(hierarchy.(thisType))
        sets = fieldnames(hierarchy.(thisType));
    else
        sets = 'empty';
    end
else
    sets = 'empty';
end
set(handles.listSets,'String',sets);
set(handles.listSets,'Value',1);

%Clear the stats and datasets fields
set(handles.listDatasets,'String','select a set');
set(handles.listDatasets,'Value',1);
set(handles.listStats,'String','select a dataset');
set(handles.listStats,'Value',1);
set(handles.editVariable,'String','select statistic');
set(handles.importVariable,'UserData',[]);
set(handles.loadVariable,'UserData',[]);
set(handles.loadVariable,'Enable','off');
set(handles.importVariable,'Enable','off');
set(handles.addPlotLayer,'Enable','off');
set(handles.editVariable,'Enable','off');



% --- Executes during object creation, after setting all properties.
function listTypes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listTypes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listSets.
function listSets_Callback(hObject, eventdata, handles)
% hObject    handle to listSets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listSets contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listSets
types = cellstr(get(handles.listTypes,'String'));
thisType = types{get(handles.listTypes,'Value')};
sets = cellstr(get(handles.listSets,'String'));
thisSet = sets{get(handles.listSets,'Value')};

view = get(handles.selectView,'UserData');
hierarchy = view.Hierarchy;
if ~strcmpi(thisSet,'empty')
    if ~isempty(hierarchy.(thisType).(thisSet))
        datasets = fieldnames(hierarchy.(thisType).(thisSet));
    else
        datasets = 'empty';
    end
else
    datasets = 'empty';
end

set(handles.listDatasets,'String',datasets);
set(handles.listDatasets,'Value',1);

%Clear the stats field
set(handles.listStats,'String','select a dataset');
set(handles.listStats,'Value',1);
set(handles.editVariable,'String','select statistic');
set(handles.importVariable,'UserData',[]);
set(handles.loadVariable,'UserData',[]);
set(handles.loadVariable,'Enable','off');
set(handles.importVariable,'Enable','off');
set(handles.addPlotLayer,'Enable','off');
set(handles.editVariable,'Enable','off');

%Check the intervals field and clear if necessary
viewMemory = get(handles.viewMemory,'Value');
if viewMemory
    set(handles.listIntervals,'String','');
    set(handles.listIntervals,'Value',1);
    set(handles.listPlotLayers,'String','');
    set(handles.listPlotLayers,'Value',1);
    set(handles.listPlotLayers,'UserData',[]);
end


% --- Executes during object creation, after setting all properties.
function listSets_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listSets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in listDatasets.
function listDatasets_Callback(hObject, eventdata, handles)
% hObject    handle to listDatasets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listDatasets contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listDatasets

types = cellstr(get(handles.listTypes,'String'));
thisType = types{get(handles.listTypes,'Value')};
sets = cellstr(get(handles.listSets,'String'));
thisSet = sets{get(handles.listSets,'Value')};
datasets = cellstr(get(handles.listDatasets,'String'));
thisDataset = datasets{get(handles.listDatasets,'Value')};
view = get(handles.selectView,'UserData');
hierarchy = view.Hierarchy;
if ~strcmpi(thisSet,'empty')
    if ~strcmpi(thisDataset,'empty')
        if ~isempty(hierarchy.(thisType).(thisSet).(thisDataset))
            stats = hierarchy.(thisType).(thisSet).(thisDataset);
        else
            stats = 'empty';
        end
    else
        stats = 'empty';
    end
else
    stats = 'empty';
end

set(handles.listStats,'String',stats);
set(handles.listStats,'Value',1);

%Reset the workspace variable field
set(handles.editVariable,'String','select statistic');
set(handles.importVariable,'UserData',[]);
set(handles.loadVariable,'UserData',[]);
set(handles.loadVariable,'Enable','off');
set(handles.importVariable,'Enable','off');
set(handles.addPlotLayer,'Enable','off');
set(handles.editVariable,'Enable','off');

% --- Executes during object creation, after setting all properties.
function listDatasets_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listDatasets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listStats.
function listStats_Callback(hObject, eventdata, handles)
% hObject    handle to listStats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listStats contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listStats

types = cellstr(get(handles.listTypes,'String'));
thisType = types{get(handles.listTypes,'Value')};
sets = cellstr(get(handles.listSets,'String'));
thisSet = sets{get(handles.listSets,'Value')};
datasets = cellstr(get(handles.listDatasets,'String'));
thisDataset = datasets{get(handles.listDatasets,'Value')};
stats = cellstr(get(handles.listStats,'String'));
thisStat = stats{get(handles.listStats,'Value')};
view = get(handles.selectView,'UserData');
hierarchy = view.Hierarchy;
if ~strcmpi(thisSet,'empty')
    if ~strcmpi(thisDataset,'empty')
        if ~strcmpi(thisStat,'empty')
            if ~isempty(hierarchy.(thisType).(thisSet).(thisDataset))
                variable = struct('dataset',thisDataset,'stat',thisStat);
                paths = struct('var',['/',thisType,'/',thisSet,'/datasets/',thisDataset,'/',thisStat],...
                    'lookup',['/',thisType,'/',thisSet,'/lookup'],'interval',['/',thisType,'/',thisSet,'/interval']);
                pathStruct = struct('type',thisType,'set',thisSet,'dataset',thisDataset,'stat',thisStat);
            else
                variable = [];
                paths = [];
            end
        else
            variable = [];
            paths = [];
        end
    else
        variable = [];
        paths = [];
    end
else
    variable = [];
    paths = [];
end

set(handles.editVariable,'String',[variable.dataset,'_',variable.stat]);
set(handles.addPlotLayer,'UserData',variable);
set(handles.importVariable,'UserData',paths);
set(handles.loadVariable,'UserData',pathStruct);

viewMemory = get(handles.viewMemory,'Value');
if ~viewMemory
    set(handles.loadVariable,'Enable','on');
    set(handles.editVariable,'Enable','on');
else 
    set(handles.loadVariable,'Enable','off');
    set(handles.importVariable,'Enable','on');
    set(handles.addPlotLayer,'Enable','on');
    set(handles.editVariable,'Enable','on');
end
set(handles.importVariable,'Enable','on');

% --- Executes during object creation, after setting all properties.
function listStats_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listStats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    

function editVariable_Callback(hObject, eventdata, handles)
% hObject    handle to editVariable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editVariable as text
%        str2double(get(hObject,'String')) returns contents of editVariable as a double


% --- Executes during object creation, after setting all properties.
function editVariable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editVariable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in importVariable.
function importVariable_Callback(hObject, eventdata, handles)
% hObject    handle to importVariable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Import the specified variable, and export it to the workspace with the
%specified name
outName = get(handles.editVariable,'String');
pathStruct = get(handles.loadVariable,'UserData');

%Get the stored data, use a try/catch in case this variable has yet to be
%imported
try
    stored = data_controller('gui','pass','stored');
catch
    stored = struct();
end
geolocation = data_controller('gui','pass','geolocation');


%Determine if this variable is already loaded into the workspace or not
if isfield(stored,pathStruct.type) && isfield(stored.(pathStruct.type),pathStruct.set) && ...
        isfield(stored.(pathStruct.type).(pathStruct.set),pathStruct.dataset) && ...
        isfield(stored.(pathStruct.type).(pathStruct.set).(pathStruct.dataset),pathStruct.stat)
    outData = stored.(pathStruct.type).(pathStruct.set).(pathStruct.dataset).(pathStruct.stat);
    outData.geolocation = geolocation;
    
    %export to the workspace
    assignin('base', outName, outData); 
else
   % Load the variable and call this function again
   loadVariable_Callback(hObject,eventdata,handles);
   importVariable_Callback(hObject,eventdata,handles);
end


% --- Executes on button press in loadVariable.
function loadVariable_Callback(hObject, eventdata, handles)
% hObject    handle to loadVariable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Load the specified variable into memory
varPath = get(handles.importVariable,'UserData');
pathStruct = get(handles.loadVariable,'UserData');
hdf5File = get(handles.editFile,'String');

%Load the variable
h = msgbox('Wait while the data is loaded into memory');
outData = struct('data',[],'lookup',[],'interval',[],'units','');
outData.data = h5dataread(hdf5File,varPath.var);
outData.units = h5attget(hdf5File,varPath.var,'units');
% Load the interval table for this dataset
outData.interval = h5dataread(hdf5File,varPath.interval);
% Load the lookup table for gridded types, or index for point
try
    outData.lookup = h5dataread(hdf5File,varPath.lookup);
    outData.lookup(outData.lookup==0) = 1;
    
    outData.index = [];
catch
    outData.index = h5dataread(hdf5File,varPath.index);
    outData.lookup = [];
end

%Check to see if this is a differencing mode
diffFile = get(handles.editFileDiff,'String');
if ~isempty(diffFile)
    diffData = h5dataread(diffFile,varPath.var);
    if ~diffData
        error('Dataset missing from difference file')
    end
    assert(all(size(diffData)==size(outData.data)),'Grids and intervals must match for differencing')
    outData.data = outData.data - diffData;
end
 
close(h)
%Now, check what's stored in the data controller
gui = data_controller('gui','list');
gui.stored.(pathStruct.type).(pathStruct.set).(pathStruct.dataset).(pathStruct.stat) = outData;
try
    prevDatasets = gui.hierarchy.(pathStruct.type).(pathStruct.set).(pathStruct.dataset);
catch
    prevDatasets = {};
end
if ~ismember(pathStruct.stat,prevDatasets)
    gui.hierarchy.(pathStruct.type).(pathStruct.set).(pathStruct.dataset) = cat(1,prevDatasets,pathStruct.stat);
end

data_controller('gui','load',gui);

% --- Executes on button press in addPlotLayer.
function addPlotLayer_Callback(hObject, eventdata, handles)
% hObject    handle to addPlotLayer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

variable = get(handles.addPlotLayer,'UserData');
pathStruct = get(handles.loadVariable,'UserData');

% If the view is memory, add plot layer to listbox
if get(handles.viewMemory,'Value')
    variableString = [variable.dataset,'_',variable.stat];
    populate_layers(variableString,pathStruct,handles);
end

listPlotLayers_Callback(hObject, eventdata, handles)


% --- Executes on selection change in listPlotLayers.
function listPlotLayers_Callback(hObject, eventdata, handles)
% hObject    handle to listPlotLayers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listPlotLayers contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listPlotLayers

pathStruct = get(handles.listPlotLayers,'UserData');
ind = get(handles.listPlotLayers,'Value');

% Populate intervals
if length(ind)>1
    populate_intervals(pathStruct{ind(1)},handles);
else
    populate_intervals(pathStruct{ind},handles);
end

% Reset the edit data range fields
set(handles.editDataMax,'Value',0);
set(handles.editDataMax,'String','max');
set(handles.editDataMin,'Value',0);
set(handles.editDataMin,'String','min');

% --- Executes during object creation, after setting all properties.
function listPlotLayers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listPlotLayers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in rescalePlotLayers.
function rescalePlotLayers_Callback(hObject, eventdata, handles)
% hObject    handle to rescalePlotLayers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rescalePlotLayers



% --- Executes on button press in clearPlotList.
function clearPlotList_Callback(hObject, eventdata, handles)
% hObject    handle to clearPlotList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.listPlotLayers,'Value',1)
set(handles.listPlotLayers,'String','')
set(handles.listPlotLayers,'UserData',[]);


% --- Executes on selection change in listIntervals.
function listIntervals_Callback(hObject, eventdata, handles)
% hObject    handle to listIntervals (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listIntervals contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listIntervals


% --- Executes during object creation, after setting all properties.
function listIntervals_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listIntervals (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function [indIntervals,thisData,varStruct] = get_plot_data(handles)
% This function prepares data for plotting based on selected layers and
% intervals

% Get the data from storage
stored = data_controller('gui','pass','stored');

% Get the dataset to load
varStruct = get(handles.listPlotLayers,'UserData');
indData = get(handles.listPlotLayers,'Value');

if length(indData)==1
    loopVarStruct = varStruct{indData};
    thisData = stored.(loopVarStruct.type).(loopVarStruct.set).(loopVarStruct.dataset).(loopVarStruct.stat);
else
    for m = 1:length(indData)
       loopVarStruct = varStruct{m};
       loopData = stored.(loopVarStruct.type).(loopVarStruct.set).(loopVarStruct.dataset).(loopVarStruct.stat); 
       if ndims(loopData.data) ==  3
           loopData.data = sum(loopData.data,3);
       end
       if m == 1
           thisData = loopData;
       else
           thisData.data = thisData.data + loopData.data;
       end
    end
end

% Trim varStruct for downstream use
varStruct = varStruct(indData);

% Get the data intervals to use
indIntervals = get(handles.listIntervals,'Value');

% If dataset is incomplete, use only relevant inds
indIntervals(indIntervals > size(thisData.data,1)) = [];

% Set the data range fields
% Reset the edit data range fields
if ~get(handles.editDataMax,'Value')
    set(handles.editDataMax,'String',max(thisData.data(:)));
end
if ~get(handles.editDataMin,'Value')
    set(handles.editDataMin,'String',min(thisData.data(:)));
end


function titleString = get_plot_title(varStruct,prefix)
if nargin < 2
    prefix = '';
end

for m = 1:length(varStruct)
    varString = [strrep(varStruct{m}.dataset,'_',' '),' (',varStruct{m}.stat,')'];
    if m == 1
        if length(varStruct) > 1
            varString = [prefix,'sum of ',varString];
        end
        titleString = {varString};
    else
        if length(varStruct) > 1
            varString = ['and ',varString]; 
        end
        titleString = cat(1,titleString,varString);
    end
end

function editDataMin_Callback(hObject, eventdata, handles)
% hObject    handle to editDataMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDataMin as text
%        str2double(get(hObject,'String')) returns contents of editDataMin as a double

if ~isempty(get(handles.editDataMin,'String'))
    set(handles.editDataMin,'Value',1);
else
    set(handles.editDataMin,'Value',0);
    set(handles.editDataMin,'String','min');
end

% --- Executes during object creation, after setting all properties.
function editDataMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDataMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDataMax_Callback(hObject, eventdata, handles)
% hObject    handle to editDataMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDataMax as text
%        str2double(get(hObject,'String')) returns contents of editDataMax as a double
if ~isempty(get(handles.editDataMax,'String'))
    set(handles.editDataMax,'Value',1);
else
    set(handles.editDataMax,'Value',0);
    set(handles.editDataMax,'String','max');
end

% --- Executes during object creation, after setting all properties.
function editDataMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDataMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotMain.
function plotMain_Callback(hObject, eventdata, handles)
% hObject    handle to plotMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Determine with we are plotting raw or aggregate
if get(handles.plotAggSelect,'Value')
    plotAgg(handles);
else

    % Get the geolocation data
    geolocation = data_controller('gui','pass','geolocation');

    % Get the data
    [ind,thisData,varStruct] = get_plot_data(handles);

    %Check to see what the plot type is, and plot appropriately
    if strcmpi(varStruct{1}.type,'grid')
        if length(ind) > 1
            msgbox('Choose "aggregate" or "Build Movie" to visualize multiple intervals of grid data','help');
            return
        end
        %Get the X and Y data
        xCoords = (geolocation.left+geolocation.cellsize/2:geolocation.cellsize:...
            geolocation.left+geolocation.cellsize*geolocation.num_col-geolocation.cellsize/2);
        yCoords = (geolocation.bot+geolocation.cellsize*geolocation.num_row-geolocation.cellsize/2:-geolocation.cellsize:...
            geolocation.bot+geolocation.cellsize/2);
        %Plot
        plotData = sum(thisData.data(ind,:,:),3);
        %For visualization, set the 1st column equal to the minimum value
        plotData(1) = nanmin(plotData);
        imagesc(xCoords',yCoords',plotData(thisData.lookup),'Parent',handles.axisPlot);colorbar
        set(gca,'YDir','normal');
        set(findobj('type','image'),'hittest','off');

        %Save data to the export handle
        set(handles.export,'UserData',plotData(thisData.lookup));
    else
        plotDates = thisData.interval(ind,1);
        plotData = sum(thisData.data(ind,:,:),3);
        plot(handles.axisPlot,plotDates,plotData)
        datetick

        %Save data to the export handle
        set(handles.export,'UserData',{plotDates,plotData});
    end

    % Set the y or c axis limits
    % Reset the edit data range fields
    if ~get(handles.editDataMax,'Value')
        set(handles.editDataMax,'String',max(plotData(:)));
    end
    if ~get(handles.editDataMin,'Value')
        set(handles.editDataMin,'String',min(plotData(:)));
    end
    minData = str2double(get(handles.editDataMin,'String'));
    maxData = str2double(get(handles.editDataMax,'String'));
    
    if maxData > minData % does not happen if data are all 0
        if strcmpi(varStruct{1}.type,'grid')
            set(gca,'CLim',[minData,maxData])
        else
            set(gca,'YLim',[minData,maxData])
        end
    end
    
    %Form the title
    titleString = get_plot_title(varStruct);
    title(titleString);

    %Activate the export button
    set(handles.export,'Enable','on')
    
    % Activate the min/max fields
    set(handles.editDataMin,'Enable','on')
    set(handles.editDataMax,'Enable','on')

    %Save this function as last plotted
    set(handles.axisPlot,'UserData','plotRaw');
end

% --- Executes on button press in plotAgg.
function plotAgg(handles)
% hObject    handle to plotAgg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the geolocation data
geolocation = data_controller('gui','pass','geolocation');

% Get the data
[ind,thisData,varStruct] = get_plot_data(handles);

%Get the aggregation period and statistic
aggPeriod = get(handles.editAggPeriod,'String');
aggStat = get(handles.editAggStat,'String');

%Throw an error if necessary
if isempty(aggStat) || isempty(aggPeriod) || strcmpi(aggStat,'agg stat')
    msgbox('Specify a supported agg period and agg stat before plotting','error');
    return
end

%Check to see what the plot type is, and plot appropriately
rawData = sum(thisData.data(ind,:,:),3);
if strcmpi(varStruct{1}.type,'grid')
    %Convert the agg stat to a function
    aggFunc = str2func(aggStat);
    [~,plotData] = time_series_aggregate(thisData.interval(ind,1),rawData,aggPeriod,'sum');
    switch aggStat
        case {'min','max','std'}
            plotData = aggFunc(plotData,[],1);
        otherwise
            plotData = aggFunc(plotData,1);
    end
    %For visualization, set the 1st column equal to the minimum value
    plotData(1) = nanmin(plotData);
    
    %Get the X and Y data
    xCoords = (geolocation.left+geolocation.cellsize/2:geolocation.cellsize:...
        geolocation.left+geolocation.cellsize*geolocation.num_col-geolocation.cellsize/2);
    yCoords = (geolocation.bot+geolocation.cellsize*geolocation.num_row-geolocation.cellsize/2:-geolocation.cellsize:...
        geolocation.bot+geolocation.cellsize/2);
    imagesc(xCoords',yCoords',plotData(thisData.lookup),'Parent',handles.axisPlot);colorbar
    set(gca,'YDir','normal');
    set(findobj('type','image'),'hittest','off');
    
    % Set the color axis
    % Reset the edit data range fields
    if ~get(handles.editDataMax,'Value')
        set(handles.editDataMax,'String',max(plotData(:)));
    end
    if ~get(handles.editDataMin,'Value')
        set(handles.editDataMin,'String',min(plotData(:)));
    end
    minData = str2double(get(handles.editDataMin,'String'));
    maxData = str2double(get(handles.editDataMax,'String'));
    set(gca,'CLim',[minData,maxData])
    
    %Save data to the export handle
    set(handles.export,'UserData',plotData(thisData.lookup));
else
    rawData = sum(thisData.data(ind,:,:),3);
    [plotDates,plotData] = time_series_aggregate(thisData.interval(ind,1),rawData,aggPeriod,aggStat);
    plot(handles.axisPlot,plotDates,plotData)
    datetick
    
    % Set the y limits
    if ~get(handles.editDataMax,'Value')
        set(handles.editDataMax,'String',max(plotData(:)));
    end
    if ~get(handles.editDataMin,'Value')
        set(handles.editDataMin,'String',min(plotData(:)));
    end
    minData = str2double(get(handles.editDataMin,'String'));
    maxData = str2double(get(handles.editDataMax,'String'));
    set(gca,'YLim',[minData,maxData])
    
    %Save data to the export handle
    set(handles.export,'UserData',{plotDates,plotData});
end

%Form the title
titleString = get_plot_title(varStruct,[aggPeriod,' ',aggStat,' of ']);
title(titleString);

%Activate the export button
set(handles.export,'Enable','on')

% Activate the min/max fields
set(handles.editDataMin,'Enable','on')
set(handles.editDataMax,'Enable','on')

%Save this function as last plotted
set(handles.axisPlot,'UserData','plotAgg');


% --- Executes on button press in buildMovie.
function buildMovie_Callback(hObject, eventdata, handles)
% hObject    handle to buildMovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Get selected indices
ind = get(handles.listIntervals,'Value');

stored = data_controller('gui','pass','stored');
varStruct = get(handles.loadVariable,'UserData');

thisData = stored.(varStruct.type).(varStruct.set).(varStruct.dataset).(varStruct.stat);

%If dataset is incomplete, use only relevant inds
ind(ind > size(thisData.data,1)) = [];

%going to use the plotMain functionality
set(handles.axisPlot,'UserData','plotRaw');

%Check to see what the plot type is, and plot appropriately
rawData = sum(thisData.data(ind,:,:),3);

%Get the maximum values of the data
minData = nanmin(rawData(:));
maxData = nanmax(rawData(:));

%Date strings
dates = get(handles.listIntervals,'String');

if strcmpi(varStruct.type,'grid')

    for m = 1:length(ind)
        %Set the list intervals with the current interval
        set(handles.listIntervals,'Value',ind(m));
        
        %Run the plot function
        [sepFig,sepAxis] = plot_separately(hObject, eventdata, handles);

        % Set the color axis
        if get(handles.editDataMin,'Value')
            minData = str2double(get(handles.editDataMin,'String'));
        end
        if get(handles.editDataMax,'Value')
            maxData = str2double(get(handles.editDataMax,'String'));
        end
    
        %Set the cAxis
        caxis(sepAxis,[minData,maxData])
        
        %Add the Date
        prevAxes = gca;
        axes(sepAxis);
        axis off
        box off
        if minData == 0
            thisCmap = colormap;
            %Get the color value of the figure
            thisCmap(1,:) = get(gcf,'Color');
            colormap(thisCmap);
        end
        xLim = get(sepAxis,'XLim');
        yLim = get(sepAxis,'YLim');
        text(xLim(1) + diff(xLim)/20,yLim(2)-diff(yLim)/20,dates{ind(m)},'Color','k')
        axes(prevAxes);
        
        F(m) = getframe(sepFig);
        
        %Close the separate figure
        close(sepFig);
    end
    
    %Save data to the export handle
    set(handles.export,'UserData',F);
    set(handles.playMovie,'UserData',F);
else
    msgbox('"Build Movie" only supports grid data types','help');
        return
end

%Activate the playMovie button
set(handles.playMovie,'Enable','on')

%Reset the list of intervals
set(handles.listIntervals,'Value',ind);

%Save this function as last plotted
set(handles.axisPlot,'UserData','buildMovie');

% --- Executes on button press in playMovie.
function playMovie_Callback(hObject, eventdata, handles)
% hObject    handle to playMovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

movieData = get(handles.playMovie,'UserData');

newFig = figure();
movie(newFig,movieData,2,4);
close(newFig);

% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%export function
%plotMain data, for now, assume if grid then plotmain the first selected interval

%Get the variable data
varStruct = get(handles.loadVariable,'UserData');

%Get the stored plot data
plotData = get(handles.export,'UserData');

%Get the geolocation data
geolocation = data_controller('gui','pass','geolocation');
grid = data_controller('gui','pass','grid');

%Output
if strcmpi(get(handles.axisPlot,'UserData'),'buildMovie')
    %Get output filename and location
    defaultFileName = [get(handles.editVariable,'String'),'.mp4'];
    [fileName,pathName] = uiputfile(defaultFileName);
        
    %Write out the movie
    v = VideoWriter([pathName,filesep,fileName],'MPEG-4');
    v.FrameRate = 4;
    v.Quality = 100;
    open(v);
    for m = 1:size(plotData,3)
        writeVideo(v,plotData(:,:,m));
    end
    close(v)
    %movie2avi(plotData,[pathName,filesep,fileName],'fps',4,'quality',100);
else
    if strcmpi(varStruct.type,'grid')
        %Build output header
        header = struct('cols',geolocation.num_col,'rows',geolocation.num_row,...
            'left',geolocation.left,'bottom',geolocation.bot,...
            'cellsize',geolocation.cellsize','noData',NaN);
        
        %Get output filename and location
        defaultFileName = [get(handles.editVariable,'String'),'.asc'];
        [fileName,pathName] = uiputfile(defaultFileName);
        
        %Mask to the surface
        plotData(grid.surface==0) = NaN;
        
        %Write output file
        h = msgbox('Exporting data, please wait');
        asciiwrite(plotData,[pathName,filesep,fileName],header)
        close(h);
    else
        %Build output header
        header = ['date time, ',get(handles.editVariable,'String')];
        
        %Get output filename and location
        defaultFileName = [get(handles.editVariable,'String'),'.csv'];
        [fileName,pathName] = uiputfile(defaultFileName);
        
        %Transform dates to datestr
        plotData{1} = datestr(plotData{1},'yyyy/mm/dd HH:MM:SS');
        
        %Write output file
        h = msgbox('Exporting data, please wait');
        fid = fopen([pathName,filesep,fileName],'wt');
        fprintf(fid,'%s\n',header);
        for m = 1:length(plotData{1})
            fprintf(fid,'%s,%g\n',plotData{1}(m,:),plotData{2}(m));
        end
        fclose(fid);
        close(h);
    end
end



% --- Executes on button press in plotAggSelect.
function plotAggSelect_Callback(hObject, eventdata, handles)
% hObject    handle to plotAggSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotAggSelect


function editAggPeriod_Callback(hObject, eventdata, handles)
% hObject    handle to editAggPeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAggPeriod as text
%        str2double(get(hObject,'String')) returns contents of editAggPeriod as a double


% --- Executes during object creation, after setting all properties.
function editAggPeriod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAggPeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editAggStat_Callback(hObject, eventdata, handles)
% hObject    handle to editAggStat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAggStat as text
%        str2double(get(hObject,'String')) returns contents of editAggStat as a double


% --- Executes during object creation, after setting all properties.
function editAggStat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAggStat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%--------------------------------------------------------------------------
%Helper Functions
%--------------------------------------------------------------------------
%Resets the state of file contents lists when a new file is selected
function reset_file(handles)

colormap parula

%Load the HDF5 file info and store the hierarchy in the UserData field of
%the editFile object
hdf5File = get(handles.editFile,'String');
info = hdf5info(hdf5File);

%Now, process the info into a useable structure
hierarchy = struct('grid',[],'zone',[],'point',[],'station',[]);
fields = fieldnames(hierarchy);
for m = 1:length(info.GroupHierarchy.Groups) %This is the type level
    pathElements = path_split(info.GroupHierarchy.Groups(m).Name);
    thisType = pathElements{end};
    if ismember(thisType,fields) %only want select types
        for n = 1:length(info.GroupHierarchy.Groups(m).Groups) %This is the set level
            pathElements = path_split(info.GroupHierarchy.Groups(m).Groups(n).Name);
            thisSet = pathElements{end};
            for o = 1:length(info.GroupHierarchy.Groups(m).Groups(n).Groups) %This is the set elements level
                pathElements = path_split(info.GroupHierarchy.Groups(m).Groups(n).Groups(o).Name);
                thisSub = pathElements{end};
                if ismember(thisSub,{'datasets'})
                    for p = 1:length(info.GroupHierarchy.Groups(m).Groups(n).Groups(o).Groups) %This is the dataset level
                        pathElements = path_split(info.GroupHierarchy.Groups(m).Groups(n).Groups(o).Groups(p).Name);
                        thisDataset = pathElements{end};
                        thisStats = cell([length(info.GroupHierarchy.Groups(m).Groups(n).Groups(o).Groups(p).Datasets),1]);
                        for q = 1:length(info.GroupHierarchy.Groups(m).Groups(n).Groups(o).Groups(p).Datasets) %This is the stats level
                            pathElements = path_split(info.GroupHierarchy.Groups(m).Groups(n).Groups(o).Groups(p).Datasets(q).Name);
                            thisStats{q} = pathElements{end};
                        end
                        hierarchy.(thisType).(thisSet).(thisDataset) = thisStats;
                    end
                end
            end
        end
    end
end

%Read in the geolocation and info fields
gui = read_geoloc_info(hdf5File,info);

%Read in the grid structure
grid = read_grid_structure(hdf5File,info);
gui.grid = grid;

%Save this to the UserData in editFile
set(handles.editFile,'UserData',hierarchy);

%Now, set the Output Types
set(handles.listTypes,'String',fields);

%Clear the data controller
data_controller('gui','wipe');
data_controller('gui','load',gui);

%Clear the other listboxes
set(handles.listTypes,'Value',1);
set(handles.listSets,'String','select a type');
set(handles.listSets,'Value',1);
set(handles.listDatasets,'String','select a set');
set(handles.listDatasets,'Value',1);
set(handles.listStats,'String','select a dataset');
set(handles.listStats,'Value',1);
set(handles.editVariable,'String','select statistic');
set(handles.importVariable,'UserData',[]);
set(handles.loadVariable,'UserData',[]);
set(handles.export,'UserData',[]);
set(handles.playMovie,'UserData',[]);

%Disable the playMovie and export buttons
set(handles.playMovie,'Enable','off')
set(handles.export,'Enable','off')

%Set the view to file, and clear it
set(handles.selectView,'UserData',[]);
set(handles.viewFile,'Value',1);
set(handles.viewMemory,'Value',0);

%Clear the intervals
set(handles.listIntervals,'String','');
set(handles.listIntervals,'Value',1);
set(handles.listPlotLayers,'String','');
set(handles.listPlotLayers,'Value',1);

function pathElements = path_split(string)
%This handles the HDF paths appropriately
tempElements = regexp(string,'/','split');
pathElements = tempElements(2:end);

function switch_view(newView,handles)
%Get the saved view
view = get(handles.selectView,'UserData');

%Determine if this is being called after reset_file
resetView = isempty(view);

%Get the current state
currView.String.Types = cellstr(get(handles.listTypes,'String'));
currView.String.Sets = cellstr(get(handles.listSets,'String'));
currView.String.Datasets = cellstr(get(handles.listDatasets,'String'));
currView.String.Stats = cellstr(get(handles.listStats,'String'));
currView.Value.Types = get(handles.listTypes,'Value');
currView.Value.Sets = get(handles.listSets,'Value');
currView.Value.Datasets = get(handles.listDatasets,'Value');
currView.Value.Stats = get(handles.listStats,'Value');
currView.Enable.export = get(handles.export,'Enable');
currView.Enable.playMovie = get(handles.playMovie,'Enable');

%Get the new view
if ~resetView
    view = view.Previous;
    view.Previous = currView;
    
    %Get the variables hierarchy
    if strcmpi(newView,'memory')
        %Reload hierarchy from the data_controller if the current view is memory
        gui = data_controller('gui','list');
        if ~isempty(gui)
            if isfield(gui,'hierarchy')
                view.Hierarchy = gui.hierarchy;
            else
                view.Hierarchy = [];
            end
        end
    else
        %Get hierarchy from the file if current view is file
        view.Hierarchy = get(handles.editFile,'UserData');
    end
else
    view = currView;
    %Get the variables hierarchy
    view.Hierarchy = get(handles.editFile,'UserData');
    view.Previous = currView;
end

%Save this view
set(handles.selectView,'UserData',view);

%Set the fields for the new view
if strcmpi(newView,'memory')
    set(handles.listTypes,'String',view.String.Types);
    set(handles.listTypes,'Value',view.Value.Types);
    set(handles.listSets,'String',view.String.Sets);
    set(handles.listSets,'Value',view.Value.Sets);
    set(handles.listDatasets,'String',view.String.Datasets);
    set(handles.listDatasets,'Value',view.Value.Datasets);
    set(handles.listStats,'String',view.String.Stats);
    set(handles.listStats,'Value',view.Value.Stats);   
else
    if ~isempty(view)
        set(handles.listTypes,'String',view.String.Types);
        set(handles.listTypes,'Value',view.Value.Types);
    end
    set(handles.listSets,'String','select a type');
    set(handles.listSets,'Value',1);
    set(handles.listDatasets,'String','select a set');
    set(handles.listDatasets,'Value',1);
    set(handles.listStats,'String','select a dataset');
    set(handles.listStats,'Value',1);
    set(handles.editVariable,'String','select statistic');
    set(handles.editVariable,'UserData',[]);
end
   
%If the new view is File, then disable everything else, otherwise enable
if ~strcmpi(newView,'memory')
    %Enable/Disable
    set(handles.listIntervals,'Enable','off')
    set(handles.listPlotLayers,'Enable','off')
    set(handles.rescalePlotLayers,'Enable','off')
    set(handles.clearPlotList,'Enable','off')
    set(handles.editVariable,'Enable','off')
    set(handles.plotMain,'Enable','off')
    set(handles.plotAggSelect,'Enable','off')
    set(handles.editAggPeriod,'Enable','off')
    set(handles.editAggStat,'Enable','off')
    set(handles.export,'Enable','off')
    set(handles.playMovie,'Enable','off')
    set(handles.buildMovie,'Enable','off')
    set(handles.importVariable,'Enable','off')
    set(handles.addPlotLayer,'Enable','off')
    set(handles.loadVariable,'Enable','off')
    set(handles.editDataMin,'Enable','off')
    set(handles.editDataMax,'Enable','off')
else
    set(handles.listIntervals,'Enable','on')
    set(handles.listPlotLayers,'Enable','on')
    set(handles.rescalePlotLayers,'Enable','on')
    set(handles.clearPlotList,'Enable','on')
    set(handles.editVariable,'Enable','on')
    set(handles.plotMain,'Enable','on')
    set(handles.plotAggSelect,'Enable','on')
    set(handles.editAggPeriod,'Enable','on')
    set(handles.editAggStat,'Enable','on')
    set(handles.buildMovie,'Enable','on')
    if ~isempty(view)
        set(handles.export,'Enable',view.Enable.export);
        set(handles.playMovie,'Enable',view.Enable.playMovie);
    end
    set(handles.importVariable,'Enable','off')
    set(handles.addPlotLayer,'Enable','off')
    set(handles.loadVariable,'Enable','off')
    set(handles.editDataMin,'Enable','off')
    set(handles.editDataMax,'Enable','off')
end

function populate_layers(varString,pathStruct,handles)
% Get the previous list
prevLayers = get(handles.listPlotLayers,'String');

if isempty(prevLayers)
    set(handles.listPlotLayers,'String',{varString})
    set(handles.listPlotLayers,'Value',1);
    set(handles.listPlotLayers,'UserData',{pathStruct});
else
    prevPathStruct = get(handles.listPlotLayers,'UserData');
    set(handles.listPlotLayers,'String',cat(1,prevLayers,{varString}))
    set(handles.listPlotLayers,'UserData',cat(1,prevPathStruct,{pathStruct}))
end



function populate_intervals(varStruct,handles)
%Only repopulate if needed (i.e. if interval list is empty)
test = isempty(get(handles.listIntervals,'String'));
if test
    h = msgbox('Wait while intervals are populated');
    data = data_controller('gui','pass','stored');
    intervalList = data.(varStruct.type).(varStruct.set).(varStruct.dataset).(varStruct.stat).interval;
    if size(intervalList,2) > 1
        intervalStrings = datestr(intervalList(:,1));
    else
        intervalStrings = datestr(intervalList);
    end
    set(handles.listIntervals,'String',cellstr(intervalStrings))
    close(h);
end

function stored = read_grid_structure(hdf5File,info)
% Read the structure group
stored = struct();
for m = 1:length(info.GroupHierarchy.Groups)
    thisGroup = info.GroupHierarchy.Groups(m);
    if any(strcmpi(thisGroup.Name,{'/structure'}))
        thisGroup = info.GroupHierarchy.Groups(m).Groups.Groups.Groups; %points to /structure/grids/ilhm/z
        thisSet = thisGroup.Name(2:end);
        stored.soil = h5dataread(hdf5File,[thisSet,'/soil/center']);
        stored.wetland = h5dataread(hdf5File,[thisSet,'/wetland/center']);
        stored.stream = h5dataread(hdf5File,[thisSet,'/stream']);
        stored.surface = h5dataread(hdf5File,[thisSet,'/surface']);
    end
end

function stored = read_geoloc_info(hdf5File,info)
%Read the geolocation data
stored = struct();
for m = 1:length(info.GroupHierarchy.Groups)
    thisGroup = info.GroupHierarchy.Groups(m);
    if any(strcmpi(thisGroup.Name,{'/geolocation','/info'}))
        thisSet = thisGroup.Name(2:end);
        for n = 1:length(thisGroup.Datasets)
            thisVar = thisGroup.Datasets(n).Name;
            thisName = fliplr(strtok(fliplr(thisVar),'/'));
            stored.(thisSet).(thisName) = h5dataread(hdf5File,thisVar);
        end
    end
end

function [newFig,newAxes] = plot_separately(hObject, eventdata, handles)

%Get the current extent and colormap settings
xLimCurr = get(handles.axisPlot,'XLim');
yLimCurr = get(handles.axisPlot,'YLim');
colormapCurr = colormap;

%Get the last plotting function
lastFunc = get(handles.axisPlot,'UserData');

newFig = figure('Position',[1,100,560,420]);
newAxes = gca;
storeAxis = handles.axisPlot;
handles.axisPlot = newAxes;

if strcmpi(lastFunc,'plotAgg')
    plotAgg(handles);
elseif strcmpi(lastFunc,'plotRaw')
    plotMain_Callback(hObject,eventdata,handles);
end

%Set the extent and colormap
set(handles.axisPlot,'XLim',xLimCurr);
set(handles.axisPlot,'YLim',yLimCurr);
colormap(colormapCurr);


%reset the current axes and figure
figure(handles.figureVis);
axes(storeAxis);
handles.axisPlot = storeAxis;

%--------------------------------------------------------------------------
%Context menu Functions
%--------------------------------------------------------------------------
% --------------------------------------------------------------------
function contextColormap_Callback(hObject, eventdata, handles)
% hObject    handle to contextColormap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
colormapeditor


% --------------------------------------------------------------------
function contextHold_Callback(hObject, eventdata, handles)
% hObject    handle to contextHold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hold(handles.axisPlot);
state = get(hObject,'Checked');
switch state
    case 'on'
        state = 'off';
    case 'off'
        state = 'on';
end
set(hObject,'Checked',state);


% --------------------------------------------------------------------
function contextSeparate_Callback(hObject, eventdata, handles)
% hObject    handle to contextSeparate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figSeparate = plot_separately(hObject, eventdata, handles);


% --------------------------------------------------------------------
function plotContext_Callback(hObject, eventdata, handles)
% hObject    handle to plotContext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function contextCopy_Callback(hObject, eventdata, handles)
% hObject    handle to contextCopy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figSeparate = plot_separately(hObject, eventdata, handles);

%Save it to the clipboard
print(figSeparate,'-dbitmap');

%Close it
close(figSeparate)

