%This script uses non-rescaled outputs, i.e. within-landunit values
%--------------------------------------------------------------------------
%Specify Inputs
%--------------------------------------------------------------------------
inputDir = 'D:\Users\Anthony\Modeling\ILHM\Output\test-v1\fullGrid13';
inputFilename = 'OUTF_test-v1_fullGrid13_20090409.h5';
inputFile = [inputDir,filesep,inputFilename];

type = 'grid';
namespace = 'expanded';
uplandRDatasets= {'deep_percolation_upland'};
uplandETDatasets = {'evap_canopy','evap_depression','evap_soil','evap_snow_upland','transp_upland'};
precipDatasets = {'precip'};
dataType = 'plus';

luNums = [1,2,3,4,8];
luNames = {'urban','ag','shrub','deciduous','coniferous'};
pureThresh = 0.75; %threshold weight for a pure landuse class

inputStatic = 'D:\Users\Anthony\Modeling\ILHM\Input\STAT_MRW-4lay-soil_12_20090315.mat';
inputLandscape = 'D:\Users\Anthony\Modeling\ILHM\Input\LAND_MRW-1998-LU_2_20081125.h5';
inputDistance = '\\Hydroflux\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Analysis_Layers\Distance_From_Lakeshore\distgrid';
inputClim = 'D:\Users\Anthony\Modeling\ILHM\Input\CLIM_MRW-1980-2007_dailyWeight-2_20090326.h5';
airTempGroup = 'airTemp';

climCycleDir = '\\hydroflux\F\Data\Other_Downloaded\Climate_SST_Indices_1950_2008_CPC_NOAA';
climCycleFiles = {'ao_cpc_noaa_1950_2008.txt','enso3.4_cpc_noaa_1950_2008.txt',...
    'nao_cpc_noaa_1950_2008.txt','pna_cpc_noaa_1950_2008.txt'};
climCycleNames = {'ao','enso','nao','pna'};

outputDir = 'D:\Users\Anthony\Graphics\4_08_09_Thesis_Recalc';
outputFilename = 'Multiple_regressions.mat';
outputFile = [outputDir,filesep,outputFilename];

outputFileR = [outputDir,filesep,'statR.txt'];
outputFileR_RMSE = [outputDir,filesep,'statR_RMSE.txt'];
outputFileET = [outputDir,filesep,'statET.txt'];
outputFileET_RMSE = [outputDir,filesep,'statET_RMSE.txt'];

%% Lookup Table, Interval Table
%--------------------------------------------------------------------------
%Load lookup table and the interval table, and the MRW grid
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputFile,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Get trimStruct
[dummy,trimStruct] = trim_domain(~mask,mask);

%Load the interval table
interval = h5dataread(inputFile,['/',type,'/',namespace,'/','interval']);

%Build a monthly interval array, skipping incomplete water years
monthlyInterval = interval(10:end-3,:);

%%
%--------------------------------------------------------------------------
%Load the rescale grids
%--------------------------------------------------------------------------
rescaleGrids = load(inputStatic,'water_fraction','stream_fraction');
rescaleGrids.stream_fraction.data = min(rescaleGrids.stream_fraction.data, 1-rescaleGrids.water_fraction.data);
wetFrac = trim_domain(~mask,rescaleGrids.water_fraction.data + rescaleGrids.stream_fraction.data)';
upFrac = 1 - wetFrac;
clear rescaleGrids
rescaleGrids = struct('upland',upFrac,'wetland',wetFrac,'all',upFrac+wetFrac);
%% Geolocation Information
%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputFile,'/geolocation/bot');
cellsize = h5dataread(inputFile,'/geolocation/cellsize');
left = h5dataread(inputFile,'/geolocation/left');
numCol = h5dataread(inputFile,'/geolocation/num_col');
numRow = h5dataread(inputFile,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);
%% LU Weights
%--------------------------------------------------------------------------
%Load the LU weights
%--------------------------------------------------------------------------
%Load the land use weights
luWeights = h5dataread(inputLandscape,'/landuse/weights');
luLookup = h5dataread(inputLandscape,'/landuse/lookup');

%Transform grid to 3-D
luWeightsGrid = zeros([size(lookup),size(luWeights,3)]);
for m = 1:size(luWeights,3)
    thisWeight = luWeights(1,:,m);
    luWeightsGrid(:,:,m) = thisWeight(luLookup);
end

%Trim to domain
luWeights = trim_domain(double(~mask),luWeightsGrid);

%Clean up
clear luWeightsGrid thisWeight loadData
%% Distance Grid
%--------------------------------------------------------------------------
%Load the distance grid, create classes
%--------------------------------------------------------------------------
distance = import_grid(inputDistance);
distance(distance<0) = 0; %these are NaN cells in the input grid

%Convert to km from
distance = unit_conversions(distance,'m','km');

%Trim this to the domain
distance = trim_domain(double(~mask),distance);

%% Soil Field Capacity
%--------------------------------------------------------------------------
%Load the soil texture grid, generate indices
%--------------------------------------------------------------------------
loadData = load(inputStatic,'field_cap');
soilsLayers = loadData.field_cap.data;

%This is a multi-layer grid, get the mode of the texture
soilsMean = mean(soilsLayers,3);

%Trim this to the domain
soils = trim_domain(double(~mask),soilsMean);

%Clean up
clear soilsLayers soilsMean loadData

%% Climate Cycles
%--------------------------------------------------------------------------
%Load the climate cycles data
%--------------------------------------------------------------------------
climCycles = struct();
for m = 1:length(climCycleNames)
    [months,data] = read_monthly_table([climCycleDir,filesep,climCycleFiles{m}],false);
    climCycles.(climCycleNames{m}).months = months;
    climCycles.(climCycleNames{m}).data = data;
end
%% Precip Data
%Get grid size, and make a zerosGrid
gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],false);
zerosGrid = zeros(gridSize);

%Get the precip data
precip = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],true,...
    [0,0],[gridSize(1),gridSize(2)],[1,1]);

%Yearly aggregation
[years,yearlyPrecip] = time_series_aggregate(interval(:,1),precip,'water years','sum');
years = years(2:end-1);
yearlyPrecip = yearlyPrecip(2:end-1,:); %don't want partial water years

%Trim the monthly data
monthlyPrecip = precip(10:end-3,:);

%Clean up
clear precip

%% Air Temperature Data
%Load the airTemp index and lookup tables
tempIndex = h5dataread(inputClim,[airTempGroup,'/index']);
tempLookup = h5dataread(inputClim,[airTempGroup,'/lookup'],false);

%Trim the lookup tables to the domain
tempLookup = trim_domain(double(~mask),tempLookup)';

%Get the desired range of values, and monthly min and max indices
trimInd = bracket_index(tempIndex(:,1),monthlyInterval(1),monthlyInterval(end));
tempIndexTrim = tempIndex(trimInd(1):trimInd(2),:);
[months,minInd,maxInd] = time_series_aggregate(tempIndexTrim(:,1),(1:size(tempIndexTrim,1))','months',{'min','max'});

%Create an empty monthly average air temperature array
monthlyTemp = zeros(size(monthlyPrecip));

%Read in all of the airTemp data
tempData = h5dataread(inputClim,[airTempGroup,'/data'],true);

%Loop through the hours, and sum temperatures, divide by number of hours
%later
h = waitbar(0,'Getting Monthly Temperatures');
for m = 1:length(months)
    waitbar(m/length(months),h);
    for n = minInd(m):maxInd(m)
        thisData = tempData(tempIndexTrim(n,2),:);
        monthlyTemp(m,:) = monthlyTemp(m,:) + thisData(tempLookup(tempIndexTrim(n,3),:));
    end
    monthlyTemp(m,:) = monthlyTemp(m,:) / (maxInd(m) - minInd(m) + 1);
end
close(h)

%Get yearly data
[years,yearlyTemp] = time_series_aggregate(months,monthlyTemp,'water years','mean');

%% Upland R
%Replicate the wetlandFrac array
uplandFrac = 1 - repmat(wetlandFrac',gridSize(1),1);

thisData = zerosGrid;
for m = 1:length(uplandRDatasets)
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    %Add this to the entire array
    thisData = thisData + tempData;
end

%Summarize the data yearly
[years,yearlyR] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyR = yearlyR(2:end-1,:);

%Clean up
clear thisData

%% Upland ET
thisData = zerosGrid;
for m = 1:length(uplandETDatasets)
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],false);
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        %Add this to the total array
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            %Add this to the total array
            thisData = thisData + tempData;
        end
    end
end
%Summarize the data yearly
[years,yearlyET] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyET = yearlyET(2:end-1,:);

%Clean up
clear thisData

%% Output File
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'months','years','monthlyPrecip','monthlyTemp','yearlyET',...
    'yearlyR','distance','luWeights','soils','climCycles','yearlyTemp','yearlyPrecip');
%% Prepare Data for Multiple Regressions
%Reshape the yearlyR, yearlyPrecip, yearlyTemp, yearlyET, monthlyPrecip, monthlyTemp
yearlyR = yearlyR';
yearlyPrecip = yearlyPrecip';
yearlyTemp = yearlyTemp';
yearlyET = yearlyET';
monthlyPrecip = monthlyPrecip';
monthlyTemp = monthlyTemp';

%Make static inputs match size of other arrays
yearlySoil = repmat(soils,1,size(yearlyR,2));
yearlyUrb = repmat(luWeights(:,1),1,size(yearlyR,2));
yearlyAg = repmat(luWeights(:,2),1,size(yearlyR,2));
yearlyShrub = repmat(luWeights(:,3),1,size(yearlyR,2));
yearlyDecid = repmat(luWeights(:,4),1,size(yearlyR,2));
yearlyConif = repmat(luWeights(:,8),1,size(yearlyR,2));
yearlyDistance = repmat(distance,1,size(yearlyR,2));

%Convert units
yearlyR = yearlyR * 100;
yearlyPrecip = yearlyPrecip * 100;
yearlyET = yearlyET * 100;
monthlyPrecip = monthlyPrecip * 100;
yearlySoil = yearlySoil * 100;

%Create GS precip and temp arrays
gsMonths = [5,6,7,8,9];
ngsMonths = [1,2,3,4,10,11,12];
monthsDatevec = datevec(months);
gsInd = ismember(monthsDatevec(:,2),gsMonths);
ngsInd = ismember(monthsDatevec(:,2),ngsMonths);

%Select the gs and ngs portions
gsPrecip = monthlyPrecip(:,gsInd);
ngsPrecip = monthlyPrecip(:,ngsInd);
gsTemp = monthlyTemp(:,gsInd);
ngsTemp = monthlyTemp(:,ngsInd);

%Sum/mean monthly
[yearlyGSPrecip,yearlyNGSPrecip,yearlyGSTemp,yearlyNGSTemp] = deal(zeros(size(yearlyPrecip)));
for m = 1:size(yearlyGSPrecip,2)
    yearlyGSPrecip(:,m) = sum(gsPrecip(:,(1:5)+5*(m-1)),2);
    yearlyNGSPrecip(:,m) = sum(ngsPrecip(:,(1:7)+7*(m-1)),2);
    yearlyGSTemp(:,m) = mean(gsTemp(:,(1:5)+5*(m-1)),2);
    yearlyNGSTemp(:,m) = mean(ngsTemp(:,(1:7)+7*(m-1)),2);
end

%Replicate climate cycle states
[yearsClim,yearlyNAO] = time_series_aggregate(climCycles.nao.months,climCycles.nao.data,'years','median');
[yearsClim,yearlyPNA] = time_series_aggregate(climCycles.pna.months,climCycles.pna.data,'years','median');
yearsInd = bracket_index(yearsClim,datenum('1/1/1981'),datenum('1/1/2007'));
yearlyNAO = repmat(yearlyNAO(yearsInd(1):yearsInd(2))',size(yearlyPrecip,1),1);
yearlyPNA = repmat(yearlyPNA(yearsInd(1):yearsInd(2))',size(yearlyPrecip,1),1);

%Compare to calibration for 3e4 randomly selected cells, all years
randInd = sort(rand_index(size(yearlyR),1,3e4))';
calibInd = sub2ind(size(yearlyR),repmat(randInd,1,size(yearlyR,2)),repmat((1:size(yearlyR,2)),size(randInd,1),1));
%Don't use cells that are completely wetland
tempFrac = repmat(rescaleGrids.wetland',1,size(yearlyPrecip,2));
wetlandInd = tempFrac(randInd,:)==1;
calibInd = calibInd(~wetlandInd);

%Define the indices to use, want calibration to exclude last five NEXRAD
%years
bracketInd = bracket_index(years,datenum({'1/1/1980'}),datenum({'12/31/2002'}));
yearsInd = (bracketInd(1):bracketInd(2));
nexradInd = sub2ind(size(yearlyR),repmat(randInd,1,size(yearsInd,2)),repmat(yearsInd,size(randInd,1),1));
%Don't use cells that are completely wetland
nexradInd = nexradInd(~wetlandInd);

%% Run Multiple Regressions
%Calculate nested hierarchy for ET
[modelsET,anovaET] = multiple_regression('calculate',yearlyET(calibInd),...
    {yearlyGSPrecip(calibInd),yearlyNGSPrecip(calibInd)},...
    {yearlyGSTemp(calibInd),yearlyNGSTemp(calibInd)},...
    {yearlySoil(calibInd)},...
    {yearlyUrb(calibInd),yearlyAg(calibInd),yearlyShrub(calibInd),yearlyDecid(calibInd),yearlyConif(calibInd)},...
    {yearlyNAO(calibInd),yearlyPNA(calibInd)});

%Calculate nested hierarchy for R
[modelsR,anovaR] = multiple_regression('calculate',yearlyR(calibInd),...
    {yearlyGSPrecip(calibInd),yearlyNGSPrecip(calibInd)},...
    {yearlyGSTemp(calibInd),yearlyNGSTemp(calibInd)},...
    {yearlySoil(calibInd)},...
    {yearlyUrb(calibInd),yearlyAg(calibInd),yearlyShrub(calibInd),yearlyDecid(calibInd),yearlyConif(calibInd)},...
    {yearlyNAO(calibInd),yearlyPNA(calibInd)});

%Calculate nested hierarchy for ET, NEXRAD
[modelsET_NEXRAD,anovaET_NEXRAD] = multiple_regression('calculate',yearlyET(nexradInd),...
    {yearlyGSPrecip(nexradInd),yearlyNGSPrecip(nexradInd)},...
    {yearlyGSTemp(nexradInd),yearlyNGSTemp(nexradInd)},...
    {yearlySoil(nexradInd)},...
    {yearlyUrb(nexradInd),yearlyAg(nexradInd),yearlyShrub(nexradInd),yearlyDecid(nexradInd),yearlyConif(nexradInd)},...
    {yearlyNAO(nexradInd),yearlyPNA(nexradInd)});

%Calculate nested hierarchy for R, NEXRAD
[modelsR_NEXRAD,anovaR_NEXRAD] = multiple_regression('calculate',yearlyR(nexradInd),...
    {yearlyGSPrecip(nexradInd),yearlyNGSPrecip(nexradInd)},...
    {yearlyGSTemp(nexradInd),yearlyNGSTemp(nexradInd)},...
    {yearlySoil(nexradInd)},...
    {yearlyUrb(nexradInd),yearlyAg(nexradInd),yearlyShrub(nexradInd),yearlyDecid(nexradInd),yearlyConif(nexradInd)},...
    {yearlyNAO(nexradInd),yearlyPNA(nexradInd)});
%% Evaluate Best Models
modelRYearly = multiple_regression('evaluate',modelsR{5},yearlyGSPrecip,yearlyNGSPrecip,...
    yearlyGSTemp,yearlyNGSTemp,yearlySoil,...
    yearlyUrb,yearlyAg,yearlyShrub,yearlyDecid,yearlyConif,yearlyNAO,yearlyPNA);
modelETYearly = multiple_regression('evaluate',modelsET{5},yearlyGSPrecip,yearlyNGSPrecip,...
    yearlyGSTemp,yearlyNGSTemp,yearlySoil,...
    yearlyUrb,yearlyAg,yearlyShrub,yearlyDecid,yearlyConif,yearlyNAO,yearlyPNA);

%% Calculate and Evaluate Parsimonious models
[parsimModelR,parsimAnovaR] = multiple_regression('calculate',yearlyR(calibInd),...
    {yearlyGSPrecip(calibInd),yearlyNGSPrecip(calibInd),yearlySoil(calibInd),...
    yearlyUrb(calibInd),yearlyAg(calibInd),yearlyShrub(calibInd),yearlyDecid(calibInd),yearlyConif(calibInd)});

[parsimModelET,parsimAnovaET] = multiple_regression('calculate',yearlyET(calibInd),...
    {yearlyGSPrecip(calibInd),yearlyNGSPrecip(calibInd),yearlySoil(calibInd),...
    yearlyUrb(calibInd),yearlyAg(calibInd),yearlyShrub(calibInd),yearlyDecid(calibInd),yearlyConif(calibInd)});
%% Output Models
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'models*','modelRYearly','modelETYearly','parsim*','nexradInd','calibInd','-append');


%% Calculate model comparisons and maps
%Calculate mean and RMSE annual
uplands = (rescaleGrids.wetland ~= 1);
residualR = (modelRYearly-yearlyR);
residualET = (modelETYearly-yearlyET);

meanRyearly = mean(residualR(uplands,:),1)';
meanETyearly = mean(residualET(uplands,:),1)';

rmseRyearly = sqrt(mean(residualR(uplands,:).^2,1))';
rmseETyearly = sqrt(mean(residualET(uplands,:).^2,1))';

meanR_ILHM = mean(yearlyR(uplands,:),1)';
meanET_ILHM = mean(yearlyET(uplands,:),1)';

mapRMSE_R = sqrt(mean(residualR.^2,2));
mapRMSE_R(~uplands) = NaN;
mapRMSE_ET = sqrt(mean(residualET.^2,2));
mapRMSE_ET(~uplands) = NaN;

mapModel_R = sqrt(mean(modelRYearly.^2,2));
mapModel_R(~uplands) = NaN;
mapModel_ET = sqrt(mean(modelETYearly.^2,2));
mapModel_ET(~uplands) = NaN;

%% Output Maps
temp = mapModel_R(lookup);
temp(mask) = NaN;
asciiwrite(temp,outputFileR,header);
temp = mapModel_ET(lookup);
temp(mask) = NaN;
asciiwrite(temp,outputFileET,header);
temp = mapRMSE_R(lookup);
temp(mask) = NaN;
asciiwrite(temp,outputFileR_RMSE,header);
temp = mapRMSE_ET(lookup);
temp(mask) = NaN;
asciiwrite(temp,outputFileET_RMSE,header);

%% Plot
%Plot annual mean and rmse
figure;
hold on
patch([years;flipud(years)],[meanET+rmseET;flipud(meanET-rmseET)],[0,0,1],'LineStyle','none')
patch([years;flipud(years)],[meanR+rmseR;flipud(meanR-rmseR)],[0,0.5,0],'LineStyle','none')
plot(years,[meanET,meanR]);

plot(years(yearsInd),meanET(yearsInd)*100,'x','Color',[0,0,1],'MarkerSize',10)
plot(years(yearsInd),meanR(yearsInd)*100,'o','Color',[0,0.5,0],'MarkerSize',8)
plot(years(testYears),meanET(testYears)*100,'x','Color',[0,0.5,0],'MarkerSize',10)
plot(years(testYears),meanR(testYears)*100,'o','Color',[1,0,0],'MarkerSize',8)

%Plot rmse vs. mean
figure;
hold on
plot(meanET*100,rmseET*100,'x','Color',[0,0,1],'MarkerSize',10)
plot(meanR*100,rmseR*100,'o','Color',[0,0.5,0],'MarkerSize',8)
box on
ylabel('Annual RMSE (cm)','FontSize',20)
xlabel('Annual Mean Error, LM - ILHM (cm)','FontSize',20)
set(gca,'XLim',[-0.15,0.15]*100)
set(gca,'XTick',[-0.15,-0.075,0,0.075,0.15]*100)
set(gca,'YTick',[0.06,0.1,0.14,0.18]*100)
set(gca,'FontSize',18)
legend({'Upland ET','Upland DP'})

