%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
scenarioList = struct('biofuel_A1B_2050','D:\Users\Anthony\Modeling\ILHM\Output\biofuel-A1B-2050\1\OUTF_biofuel-A1B-2050_1_20090203.h5',...
    'modern_A1B_2050','D:\Users\Anthony\Modeling\ILHM\Output\modern-A1B-2050\1\OUTF_modern-A1B-2050_1_20090203.h5',...
    'modern_actual_2000','D:\Users\Anthony\Modeling\ILHM\Output\modern-actual-2000\1\OUTF_modern-actual-2000_1_20090203.h5',...
    'reforest_A1B_2050','D:\Users\Anthony\Modeling\ILHM\Output\reforest-A1B-2050\1\OUTF_reforest-A1B-2050_1_20090203.h5',...
    'urban_A1B_2050','D:\Users\Anthony\Modeling\ILHM\Output\urban-A1B-2050\1\OUTF_urban-A1B-2050_1_20090203.h5',...
    'biofuel_A1B_2095','\\Hydroserver\anthony\modeling\ILHM\Output\biofuel-A1B-2095\1\OUTF_biofuel-A1B-2095_1_20090203.h5',...
    'modern_A1B_2095','\\hydroserver\Anthony\modeling\ILHM\Output\modern-A1B-2095\1\OUTF_modern-A1B-2095_1_20090203.h5',...
    'reforest_A1B_2095','\\hydroserver\Anthony\modeling\ILHM\Output\reforest-A1B-2095\1\OUTF_reforest-A1B-2095_1_20090203.h5',...
    'urban_A1B_2095','\\hydroserver\Anthony\modeling\ILHM\Output\urban-A1B-2095\1\OUTF_urban-A1B-2095_1_20090203.h5',...
    'urban_A2_2095','\\hydroserver\Anthony\modeling\ILHM\Output\urban-A2-2095\1\OUTF_urban-A2-2095_1_20090203.h5',...
    'urban_actual_2095','\\hydroserver\Anthony\modeling\ILHM\Output\urban-actual-2095\1\OUTF_urban-actual-2095_1_20090203.h5',...
    'urban_B1_2095','\\hydroserver\Anthony\modeling\ILHM\Output\urban-B1-2095\1\OUTF_urban-B1-2095_1_20090203.h5');
scenarioOrder = {'urban_A1B_2050','reforest_A1B_2050','biofuel_A1B_2050','modern_A1B_2050',...
    'urban_A1B_2095','reforest_A1B_2095','biofuel_A1B_2095','modern_A1B_2095',...
    'urban_A2_2095','urban_actual_2095','urban_B1_2095','modern_actual_2000'};
baseScenario = 'modern_actual_2000';

%Specify the maximum number of years, shouldn't have to do this but I did
%not change the specify_output.m
maxYears = 12;

type = 'grid';
namespace = 'expanded';
allRDatasets = {'deep_percolation_upland','deep_percolation_wetland'};
allETDatasets = {'evap_canopy','evap_depression','evap_ice_pack','evap_soil',...
    'evap_snow_upland','evap_wetland','transp_upland','transp_wetland'};
uplandRDatasets= {'deep_percolation_upland'};
uplandETDatasets = {'evap_canopy','evap_depression','evap_soil','evap_snow_upland','transp_upland'};
precipDatasets = {'precip'};
dataType = 'plus';

%For soil moisture
moistureDataset = {'water_soil'};
soilThickness = 2.5; %m, only works for spatially uniform thickness, modify to read in thickness array if necessary

outputDir = 'D:\Users\Anthony\Graphics\2_10_08_Grids';
outputFilename = 'monthly_scenario_data.mat';
outputFile = [outputDir,filesep,outputFilename];


%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table
%--------------------------------------------------------------------------
inputFile = scenarioList.(scenarioOrder{1});
%Import the lookup table
lookup = h5dataread(inputFile,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputFile,['/',type,'/',namespace,'/','interval']);

%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputFile,'/geolocation/bot');
cellsize = h5dataread(inputFile,'/geolocation/cellsize');
left = h5dataread(inputFile,'/geolocation/left');
numCol = h5dataread(inputFile,'/geolocation/num_col');
numRow = h5dataread(inputFile,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%%
%--------------------------------------------------------------------------
%Loop through datasets, summing together
%--------------------------------------------------------------------------
%Get grid size
gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',allRDatasets{1},'/',dataType],false);
gridSize(1) = maxYears * 12;

%Create output
allData = struct();
allData.interval = interval;
allData.lookup = lookup;
allData.mask = mask;
numYears = gridSize(1) / 12; %<---this will need to be changed if inputs aren't monthly
numScenarios = length(scenarioOrder);

%Build the monthly all R curve
monthlyData = zeros(gridSize(1),1,numScenarios);
averageData = zeros(1,gridSize(2),numScenarios);
h = waitbar(0,'All Recharge Data');
for l = 1:numScenarios
    waitbar(l/numScenarios,h);
    inputFile = scenarioList.(scenarioOrder{l});
    for m = 1:length(allRDatasets)
        %Get grid size
        gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',allRDatasets{m},'/',dataType],false);
        gridSize(1) = maxYears * 12;
        %Load monthly data
        if length(gridSize) == 2
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allRDatasets{m},'/',dataType],true,...
                [0,0],[gridSize(1),gridSize(2)],[1,1]);
            monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
            averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
        else
            for n = 1:gridSize(3) %Loop through the third dimension
                tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allRDatasets{m},'/',dataType],true,...
                    [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
                monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
                averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
            end
        end
    end
end
close(h)
allData.allR.monthly = monthlyData;
allData.allR.average = averageData * 12; %to get annual from monthly

%Build the monthly upland R curve
monthlyData = zeros(gridSize(1),1,numScenarios);
averageData = zeros(1,gridSize(2),numScenarios);
h = waitbar(0,'Upland Recharge Data');
for l = 1:numScenarios
    waitbar(l/numScenarios,h);
    inputFile = scenarioList.(scenarioOrder{l});
    for m = 1:length(uplandRDatasets)
        %Get grid size
        gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],false);
        gridSize(1) = maxYears * 12;
        %Load monthly data
        if length(gridSize) == 2
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],true,...
                [0,0],[gridSize(1),gridSize(2)],[1,1]);
            monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
            averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
        else
            for n = 1:gridSize(3) %Loop through the third dimension
                tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],true,...
                    [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
                monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
                averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
            end
        end
    end
end
close(h)
allData.uplandR.monthly = monthlyData;
allData.uplandR.average = averageData * 12; %to get annual from monthly

%Build the monthly all ET curve
monthlyData = zeros(gridSize(1),1,numScenarios);
averageData = zeros(1,gridSize(2),numScenarios);
h = waitbar(0,'All ET Data');
for l = 1:numScenarios
    waitbar(l/numScenarios,h);
    inputFile = scenarioList.(scenarioOrder{l});
    for m = 1:length(allETDatasets)
        %Get grid size
        gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],false);
        gridSize(1) = maxYears * 12;
        %Load monthly data
        if length(gridSize) == 2
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
                [0,0],[gridSize(1),gridSize(2)],[1,1]);
            monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
            averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
        else
            for n = 1:gridSize(3) %Loop through the third dimension
                tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
                    [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
                monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
                averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
            end
        end
    end
end
close(h)
allData.allET.monthly = monthlyData;
allData.allET.average = averageData * 12; %to get annual from monthly

%Build the monthly upland ET curve
monthlyData = zeros(gridSize(1),1,numScenarios);
averageData = zeros(1,gridSize(2),numScenarios);
h = waitbar(0,'Upland ET Data');
for l = 1:numScenarios
    waitbar(l/numScenarios,h);
    inputFile = scenarioList.(scenarioOrder{l});
    for m = 1:length(uplandETDatasets)
        %Get grid size
        gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],false);
        gridSize(1) = maxYears * 12;
        %Load monthly data
        if length(gridSize) == 2
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                [0,0],[gridSize(1),gridSize(2)],[1,1]);
            monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
            averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
        else
            for n = 1:gridSize(3) %Loop through the third dimension
                tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                    [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
                monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
                averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
            end
        end
    end
end
close(h)
allData.uplandET.monthly = monthlyData;
allData.uplandET.average = averageData * 12; %to get annual from monthly

%Build the monthly precip curve
monthlyData = zeros(gridSize(1),1,numScenarios);
averageData = zeros(1,gridSize(2),numScenarios);
h = waitbar(0,'Precip Data');
for l = 1:numScenarios
    waitbar(l/numScenarios,h);
    inputFile = scenarioList.(scenarioOrder{l});
    for m = 1:length(precipDatasets)
        %Get grid size
        gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{m},'/',dataType],false);
        gridSize(1) = maxYears * 12;
        %Load monthly data
        if length(gridSize) == 2
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{m},'/',dataType],true,...
                [0,0],[gridSize(1),gridSize(2)],[1,1]);
            monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
            averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
        else
            for n = 1:gridSize(3) %Loop through the third dimension
                tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{m},'/',dataType],true,...
                    [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
                monthlyData(:,1,l) = monthlyData(:,1,l) + mean(tempData,2);
                averageData(1,:,l) = averageData(1,:,l) + mean(tempData,1);
            end
        end
    end
end
close(h)
allData.precip.monthly = monthlyData;
allData.precip.average = averageData * 12; %to get annual from monthly

%Build the soil moisture data
maxGrid = zeros(1,gridSize(2),numScenarios);
minGrid = zeros(1,gridSize(2),numScenarios);
h = waitbar(0,'Soil Moisture Data');
for l = 1:numScenarios
    waitbar(l/numScenarios,h);
    inputFile = scenarioList.(scenarioOrder{l});
    for m = 1:length(moistureDataset)
        %Get grid size
        gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',moistureDataset{m},'/',dataType],false);
        gridSize(1) = maxYears * 12;
        
        %Max Moisture
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',moistureDataset{m},'/max'],true,...
                [4,0,n-1],[numYears,gridSize(2),1],[12,1,1]);
            maxGrid(1,:,l) = maxGrid(1,:,l) + mean(tempData,1);
        end
        %Min Moisture
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',moistureDataset{m},'/min'],true,...
                [7,0,n-1],[numYears,gridSize(2),1],[12,1,1]);
            minGrid(1,:,l) = minGrid(1,:,l) + mean(tempData,1);
        end
    end
end
close(h)
allData.soil.max = maxGrid/soilThickness;
allData.soil.min = minGrid/soilThickness;

%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'-struct','allData');

%%
%--------------------------------------------------------------------------
%Create the monthly R plot
%--------------------------------------------------------------------------
figure
hold on;
lineTypes = {'r-','r--','r-.','r:',...
    'b-','b--','b-.','b:',...
    'g-','g--','g-.'};
scenarioOrderPlot = {'urban-A1B-2050','reforest-A1B-2050','biofuel-A1B-2050','modern-A1B-2050',...
    'urban-A1B-2095','reforest-A1B-2095','biofuel-A1B-2095','modern-A1B-2095',...
    'urban-A2-2095','urban-actual-2095','urban-B1-2095'};

%Calculate base
[months,monthsBase] = time_series_aggregate(allData.interval(1:maxYears*12,1),allData.allR.monthly(:,1,12),'months','sum','years','mean');
%Plot the 0 line
% plot(months,months*0,'k-');
%All R
for m = 1:numScenarios-1
    [months,monthlyData] = time_series_aggregate(allData.interval(1:maxYears*12,1),allData.allR.monthly(:,1,m),'months','sum','years','mean');
    plot(months,(monthlyData-monthsBase)*100,lineTypes{m},'LineWidth',1);
end
%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[-3,3]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',14)
ylabel(gca,'Average Monthly Recharge Anomaly (cm/m^2)','FontSize',16);
legend(scenarioOrderPlot)
set(gca,'Box','on')
%%
%--------------------------------------------------------------------------
%Create the monthly ET plot
%--------------------------------------------------------------------------
figure
hold on;
lineTypes = {'r-','r--','r-.','r:',...
    'b-','b--','b-.','b:',...
    'g-','g--','g-.'};
scenarioOrderPlot = {'urban-A1B-2050','reforest-A1B-2050','biofuel-A1B-2050','modern-A1B-2050',...
    'urban-A1B-2095','reforest-A1B-2095','biofuel-A1B-2095','modern-A1B-2095',...
    'urban-A2-2095','urban-actual-2095','urban-B1-2095'};

%Calculate base
[months,monthsBase] = time_series_aggregate(allData.interval(1:maxYears*12,1),allData.allET.monthly(:,1,12),'months','sum','years','mean');
%Plot the 0 line
% plot(months,months*0,'k-');
%All ET
for m = 1:numScenarios-1
    [months,monthlyData] = time_series_aggregate(allData.interval(1:maxYears*12,1),allData.allET.monthly(:,1,m),'months','sum','years','mean');
    plot(months,(monthlyData-monthsBase)*100,lineTypes{m},'LineWidth',1);
end
%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[-1.5,1.5]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',14)
ylabel(gca,'Average Monthly ET Anomaly (cm/m^2)','FontSize',16);
legend(scenarioOrderPlot)
set(gca,'Box','on')

%%
%--------------------------------------------------------------------------
%Create the value-shaded matrix
%--------------------------------------------------------------------------
valueTable = zeros(11,5);
%Calculate base values
basePrecip = sum(allData.precip.monthly(:,1,end),1)/numYears;
baseUplandR = sum(allData.uplandR.monthly(:,1,end),1)/numYears;
baseUplandET = sum(allData.uplandET.monthly(:,1,end),1)/numYears;
baseAllET = sum(allData.allET.monthly(:,1,end),1)/numYears;
baseAllRun = basePrecip - baseAllET;

for m = 1:numScenarios-1
    valueTable(m,1) = sum(allData.precip.monthly(:,1,m),1)/numYears - basePrecip;
    valueTable(m,2) = sum(allData.uplandR.monthly(:,1,m),1)/numYears - baseUplandR;
    valueTable(m,3) = sum(allData.uplandET.monthly(:,1,m),1)/numYears - baseUplandET;
    valueTable(m,4) = sum(allData.allET.monthly(:,1,m),1)/numYears - baseAllET;
    valueTable(m,5) = valueTable(m,1) - valueTable(m,4);
end

%Re-order a couple of columns, makes more sense this way
temp = valueTable(10,:);
valueTable(10,:) = valueTable(11,:);
valueTable(11,:) = temp;

%Now, convert to cm
valueTable = valueTable * 100;

%Labels
rowOrder = {'urban-A1B-2050','reforest-A1B-2050','biofuel-A1B-2050','modern-A1B-2050',...
    'urban-A1B-2095','reforest-A1B-2095','biofuel-A1B-2095','modern-A1B-2095',...
    'urban-A2-2095','urban-B1-2095','urban-actual-2095'};
columnOrder = {'precip','upland R','upland ET','all ET','all Run'};

%Create the value-shaded matrix
value_shaded_matrix(valueTable,columnOrder,rowOrder,'%3.1f','',...
    usercolormap([253,248,224]/255,[253,241,118]/255,[252,179,65]/255,[239,62,42]/255,1.2));