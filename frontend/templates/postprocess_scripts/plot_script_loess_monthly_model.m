%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
evalLAND = 'D:\Users\Anthony\Modeling\ILHM\Input\LAND_bay-harbor_1_20101015.h5';
evalCLIM = 'C:\Users\Anthony\Code\workspace\bay_harbor\composite_precip.h5';
evalSTAT = 'D:\Users\Anthony\Modeling\ILHM\Input\STAT_bay_harbor_no_runoff_1_20101019.mat';
evalGrid = 'C:\Users\Anthony\Data\GIS_Data\Bay_Harbor\Model_Layers\ILHM\ibound';

overlayShape = 'C:\Users\Anthony\Data\GIS_Data\Bay_Harbor\Hydro\selected_lake_polys.shp';

type = 'grid';
namespace = 'expanded';
dataType = 'plus';

uplandRDatasets= {'deep_percolation_upland'};
uplandETDatasets = {'evap_canopy','evap_depression','evap_soil','evap_snow_upland','transp_upland'};
uplandRunDatasets = {'precip_excess_upland'};
precipDatasets = {'precip'};

luNums = [1,2,10,4,8]; %note, down below I'll combine shrub and open (3 and 9) into class 10
luNames = {'urban','ag','shrub_open','deciduous','coniferous'};
pureThresh = 0.75; %threshold weight for a pure landuse class

% texNums = [1,2,3,4];
% texCateg = {1,2,(3:5),(6:12)};
% texNames = {'S','SL','L','F'};

texNums = [1,2,3,4];
% texCateg = struct('min',[5.4,4.4,2.4,0]*10^-5,'max',[6,5.4,4.4,2.4]*10^-5); %these are quartiles
texCateg = struct('min',[52.5,27.5,3.5,0]*10^-6,'max',[60,52.5,27.5,3.5]*10^-6); %these are roughly equal counts/spacing
texNames = {'Coarse','Semi_Coarse','Semi_Fine','Fine'};

distNums = [1,2,3,4];
distBands = struct('min',[0,50,100,150],'max',[50,100,150,250]);
distNames = {'band1','band2','band3','band4'};

%distNums = [1,2,3,4,5,6,7,8,9,10];
%distBands = {[0,25],[25,50],[50,75],[75,100],[100,125],[125,150],[150,175],[175,200],[200,225],[225,250]};
%distNames = {'band1','band2','band3','band4','band5','band6','band7','band8','band9','band10'};

outputFilename = 'LU_Soil_Distance_ET_R_data_noNexrad.mat'; %'LU_Soil_Distance_ET_R_data.mat';
outputFile = [outputDir,filesep,outputFilename];

%% Evaluation Data Load -- USER MUST CHANGE THINGS IN HERE
%Import the evaluation landscape data lookup
evalLandLookup = h5dataread(evalLAND,'/landuse/lookup');
evalLandWeights = h5dataread(evalLAND,'/landuse/weights');

%Now, create an array of land uses matching the luNums above
evalLandWeightsMatch = cat(3,evalLandWeights(:,:,[1,2]),sum(evalLandWeights(:,:,[3,9]),3),evalLandWeights(:,:,[4,8])); %CHANGE THIS
evalLandWeightsReshape = zeros([size(evalLandLookup),size(evalLandWeightsMatch,3)]);
evalUplandFrac = sum(evalLandWeightsMatch,3);
for m = 1:size(evalLandWeightsMatch,3)
    thisWeight = evalLandWeightsMatch(1,:,m);
    evalLandWeightsReshape(:,:,m) = thisWeight(evalLandLookup)./ evalUplandFrac(evalLandLookup);
end
evalLandWeightsReshape(isnan(evalLandWeightsReshape)) = 0;

%Load the evaluation precipitation data
evalPrecip = h5datareadlhm(evalCLIM,'/precip');
evalPrecipLookup = evalPrecip.lookup(:,:,end); %CHANGE THIS

%Determine yearly eval precipitation
[evalPrecipYears,evalPrecipYearly] = time_series_aggregate(evalPrecip.index(evalPrecip.events,1),evalPrecip.data,'water years','sum');

evalPrecipYears = evalPrecipYears(2:end);
evalPrecipYearly = evalPrecipYearly(2:end,:);
evalPrecipYearlyReshape = zeros([size(evalPrecipLookup),size(evalPrecipYearly,1)]);
for m = 1:size(evalPrecipYearlyReshape,3)
    thisPrecip = evalPrecipYearly(m,:);
    evalPrecipYearlyReshape(:,:,m) = thisPrecip(evalPrecipLookup);
end

%Load the soils data
loadData = load(evalSTAT,'soils_grid','ksat','soil_lay_top','soil_lay_bot','unsat_thickness');
evalSoilsLayers = loadData.soils_grid.data;
evalKsatLayers = loadData.ksat.data;
evalThickLayers = loadData.soil_lay_top.data - loadData.soil_lay_bot.data;
evalUnsatThick = loadData.unsat_thickness.data - sum(evalThickLayers,3)./100;
clear loadData

evalKsatVert = weighted_mean('harmonic',evalKsatLayers,evalThickLayers);

%Import the grid header
gridHeader = import_grid(evalGrid,'header');

%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table, and the MRW grid
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputOUTF,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputOUTF,['/',type,'/',namespace,'/','interval']);

%Load the MRW grid, and trim to model domain
loadData = load(inputStatic,'mrw_grid');
mrwGrid = trim_domain(double(~mask),loadData.mrw_grid.data);
mrwGrid(isnan(mrwGrid))=0;
mrwGrid = logical(mrwGrid);
%%
%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputOUTF,'/geolocation/bot');
cellsize = h5dataread(inputOUTF,'/geolocation/cellsize');
left = h5dataread(inputOUTF,'/geolocation/left');
numCol = h5dataread(inputOUTF,'/geolocation/num_col');
numRow = h5dataread(inputOUTF,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);
%%
%--------------------------------------------------------------------------
%Load the Landunit fractions, identify upland areas
%--------------------------------------------------------------------------
%Load the rescale grids
rescaleGrids = load(inputStatic,'water_fraction','stream_fraction');
rescaleGrids.stream_fraction.data = min(rescaleGrids.stream_fraction.data, 1-rescaleGrids.water_fraction.data);
wetFrac = trim_domain(~mask,rescaleGrids.water_fraction.data + rescaleGrids.stream_fraction.data)';
upFrac = 1 - wetFrac;
clear rescaleGrids
rescaleGrids = struct('upland',upFrac,'wetland',wetFrac,'all',upFrac+wetFrac);

%Identify upland cells
uplandCells = rescaleGrids.upland>0;
%%
%--------------------------------------------------------------------------
%Load the LU weights, identify pure classes
%--------------------------------------------------------------------------
%Load the land use weights
luWeights = h5dataread(inputLandscape,'/landuse/weights');
luLookup = h5dataread(inputLandscape,'/landuse/lookup');

%Transform grid to 3-D
luWeightsGrid = zeros([size(lookup),size(luWeights,3)]);
for m = 1:size(luWeights,3)
    thisWeight = luWeights(1,:,m);
    luWeightsGrid(:,:,m) = thisWeight(luLookup);
end
clear luWeights thisWeight

%Trim to domain
luWeightsGrid = trim_domain(double(~mask),luWeightsGrid);

%For each lu type, identify pure class cells
luPure = luWeightsGrid > pureThresh;

%Then, select only those that are within the MRW
for m = 1:size(luPure,2)
    luPure(:,m) = luPure(:,m);% & mrwGrid;
end

%Finally, convert this into a cell array of logical indexes, use only
%upland cells
luFind = cell(1,size(luPure,2));
for m = 1:size(luPure,2);
    luFind{m} = luPure(:,m) & uplandCells';
end


%Combine shrub and open, be careful with this!
luFind{10} = luFind{3} | luFind{9};

%%
%--------------------------------------------------------------------------
%Load the soil texture grid, generate indices
%--------------------------------------------------------------------------
loadData = load(inputStatic,'soils_grid','ksat','soil_lay_top','soil_lay_bot');
soilsLayers = loadData.soils_grid.data;
ksatLayers = loadData.ksat.data;
thickLayers = loadData.soil_lay_top.data - loadData.soil_lay_bot.data;


% %This is a multi-layer grid, get the mode of the texture
% soilsMode = mode(soilsLayers,3);

% %Trim this to the domain
% soilsTrim = trim_domain(double(~mask),soilsMode);

%Trim these to the domain
[ksatGrid,thickGrid] = trim_domain(double(~mask),ksatLayers,thickLayers);

%This is a multi-layer grid, get the harmonic mean of the ksat
ksatVert = weighted_mean('harmonic',ksatGrid,thickGrid);

%Now, get logical indices
soilsFind = cell(1,length(texNums));
for m = 1:length(texNums)
    %soilsFind{m} = ismember(soilsTrim,texCateg{m}) & uplandCells';
    soilsFind{m} = (ksatVert >= texCateg.min(m)) & (ksatVert < texCateg.max(m)) & uplandCells' ;
end
%%
%--------------------------------------------------------------------------
%Load the distance grid, create classes
%--------------------------------------------------------------------------
distance = import_grid(distanceGrid);
distance(distance<0) = 0; %these are NaN cells in the input grid

%Convert to km from
distance = unit_conversions(distance,'m','km');

%Trim this to the domain
distance = trim_domain(double(~mask),distance);

%Now, get the logical indices
distFind = cell(1,length(distNums));
for m = 1:length(distNums)
    distFind{m} = (distance >= distBands.min(m)) & (distance < distBands.max(m)) & uplandCells';
end

%% Final Prep

%Create output
allDataLuSoils = struct();

%Get grid size
gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],false);
zerosGrid = zeros(gridSize');

%%
%--------------------------------------------------------------------------
%Load the precip data
%--------------------------------------------------------------------------
%Get the precip data
precip = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],true,...
    [0,0],[gridSize(1),gridSize(2)],[1,1]);

%Yearly aggregation
[years,yearlyPrecip] = time_series_aggregate(interval(:,1),precip,'water years','sum');
yearlyPrecip = yearlyPrecip(2:end-1,:); %don't want partial water years

%Create a yearlyPrecip array to match size of monthly precip, I could do
%this faster but I'll not in order to keep it simple
yearlyPrecipMonthly = zeros(size(yearlyPrecip).*[12,1]);
yearRows = (1:size(yearlyPrecip,1));
for m = 1:12
    yearlyPrecipMonthly((yearRows-1) * 12 + m,:) = yearlyPrecip;
end
%Add nine months to this at the beginning, and three months to the end to
%match input dataset size
yearlyPrecipMonthly = [ones(9,size(yearlyPrecipMonthly,2));yearlyPrecipMonthly;ones(3,size(yearlyPrecipMonthly,2))];

%Build a monthly interval array, skipping incomplete water years
monthlyInterval = interval(10:end-3,:);

%Summarize the data monthly
monthlyData = precip./yearlyPrecipMonthly;
monthlyData(isnan(monthlyData)) = 0;
monthlyData(isinf(monthlyData)) = 0;
monthlyData(1:9,:) = []; %incomplete water year
monthlyData(end-2:end,:) = []; %incomplete water year
[months,monthlyFrac] = time_series_aggregate(monthlyInterval(:,1),monthlyData,'months','sum','years','mean');
[months,monthlyAvg] = time_series_aggregate(monthlyInterval(:,1),precip(10:end-3,:),'months','sum','years','mean');


%Loop through the desired lu types and save to allData
allDataLuSoils.precip.mean = nanmean(precip,1);
allDataLuSoils.precip.years = years;
allDataLuSoils.precip.months = months;
for m = 1:length(luNums)
    for n = 1:length(texNums)
        for o = 1:length(distNums)
            thisInd = luFind{luNums(m)} & soilsFind{texNums(n)} & distFind{distNums(o)};
            allDataLuSoils.precip.(luNames{m}).(texNames{n}).(distNames{o}).count = sum(thisInd);
            allDataLuSoils.precip.(luNames{m}).(texNames{n}).(distNames{o}).yearlyAvg = mean(yearlyPrecip(:,thisInd),2);
            allDataLuSoils.precip.(luNames{m}).(texNames{n}).(distNames{o}).monthlyAvg = mean(monthlyAvg(:,thisInd),2);
            allDataLuSoils.precip.(luNames{m}).(texNames{n}).(distNames{o}).monthlyFrac = mean(monthlyFrac(:,thisInd),2);
        end
    end
end
%% Upland R
thisData = zerosGrid;
for m = 1:length(uplandRDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    %Rescale, and add this to the entire array
    thisData = thisData + tempData;
end

%Summarize the data yearly
[years,yearlyAvg] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyAvg = yearlyAvg(2:end-1,:);
yearlyFrac = yearlyAvg ./ yearlyPrecip;
yearlyFrac(isnan(yearlyFrac)) = 0;
yearlyFrac(isinf(yearlyFrac)) = 0;

%Summarize the data monthly
monthlyData = thisData./yearlyPrecipMonthly;
monthlyData(isnan(monthlyData)) = 0;
monthlyData(isinf(monthlyData)) = 0;
monthlyData(1:9,:) = []; %incomplete water year
monthlyData(end-2:end,:) = []; %incomplete water year
[months,monthlyFrac] = time_series_aggregate(monthlyInterval(:,1),monthlyData,'months','sum','years','mean');
[months,monthlyAvg] = time_series_aggregate(monthlyInterval(:,1),thisData(10:end-3,:),'months','sum','years','mean');

%Loop through the desired lu types and save to allData
allDataLuSoils.uplandR.mean = nanmean(thisData,1);
allDataLuSoils.uplandR.monthlyProportion = monthlyFrac ./ repmat(sum(monthlyFrac,1),[12,1]);
allDataLuSoils.uplandR.years = years;
allDataLuSoils.uplandR.months = months;
for m = 1:length(luNums)
    for n = 1:length(texNums)
        for o = 1:length(distNums)
            thisInd = luFind{luNums(m)} & soilsFind{texNums(n)} & distFind{distNums(o)};
            allDataLuSoils.uplandR.(luNames{m}).(texNames{n}).(distNames{o}).count = sum(thisInd);
            allDataLuSoils.uplandR.(luNames{m}).(texNames{n}).(distNames{o}).yearlyAvg = mean(yearlyAvg(:,thisInd),2);
            allDataLuSoils.uplandR.(luNames{m}).(texNames{n}).(distNames{o}).yearlyFrac = mean(yearlyFrac(:,thisInd),2);
            allDataLuSoils.uplandR.(luNames{m}).(texNames{n}).(distNames{o}).monthlyAvg = mean(monthlyAvg(:,thisInd),2);
            allDataLuSoils.uplandR.(luNames{m}).(texNames{n}).(distNames{o}).monthlyFrac = mean(monthlyFrac(:,thisInd),2);
        end
    end
end
%% Upland Run
thisData = zerosGrid;
for m = 1:length(uplandRDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRunDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRunDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    %Rescale, and add this to the entire array
    thisData = thisData + tempData;
end

%Summarize the data yearly
[years,yearlyAvg] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyAvg = yearlyAvg(2:end-1,:);
yearlyFrac = yearlyAvg ./ yearlyPrecip;
yearlyFrac(isnan(yearlyFrac)) = 0;
yearlyFrac(isinf(yearlyFrac)) = 0;

%Summarize the data monthly
monthlyData = thisData./yearlyPrecipMonthly;
monthlyData(isnan(monthlyData)) = 0;
monthlyData(isinf(monthlyData)) = 0;
monthlyData(1:9,:) = []; %incomplete water year
monthlyData(end-2:end,:) = []; %incomplete water year
[months,monthlyFrac] = time_series_aggregate(monthlyInterval(:,1),monthlyData,'months','sum','years','mean');
[months,monthlyAvg] = time_series_aggregate(monthlyInterval(:,1),thisData(10:end-3,:),'months','sum','years','mean');

%Loop through the desired lu types and save to allData
allDataLuSoils.uplandRun.mean = nanmean(thisData,1);
allDataLuSoils.uplandRun.monthlyProportion = monthlyFrac ./ repmat(sum(monthlyFrac,1),[12,1]);
allDataLuSoils.uplandRun.years = years;
allDataLuSoils.uplandRun.months = months;
for m = 1:length(luNums)
    for n = 1:length(texNums)
        for o = 1:length(distNums)
            thisInd = luFind{luNums(m)} & soilsFind{texNums(n)} & distFind{distNums(o)};
            allDataLuSoils.uplandRun.(luNames{m}).(texNames{n}).(distNames{o}).count = sum(thisInd);
            allDataLuSoils.uplandRun.(luNames{m}).(texNames{n}).(distNames{o}).yearlyAvg = mean(yearlyAvg(:,thisInd),2);
            allDataLuSoils.uplandRun.(luNames{m}).(texNames{n}).(distNames{o}).yearlyFrac = mean(yearlyFrac(:,thisInd),2);
            allDataLuSoils.uplandRun.(luNames{m}).(texNames{n}).(distNames{o}).monthlyAvg = mean(monthlyAvg(:,thisInd),2);
            allDataLuSoils.uplandRun.(luNames{m}).(texNames{n}).(distNames{o}).monthlyFrac = mean(monthlyFrac(:,thisInd),2);
        end
    end
end
%% Upland ET
thisData = zerosGrid;
for m = 1:length(uplandETDatasets)
    loopData = zerosGrid;
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],false);
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        %Add this to the loop array
        loopData = loopData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            %Add this to the loop array
            loopData = loopData + tempData;
        end
    end
    %Rescale and add to the total array
    thisData = thisData + loopData;
end

%Summarize the data yearly
[years,yearlyAvg] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyAvg = yearlyAvg(2:end-1,:);
yearlyFrac = yearlyAvg ./ yearlyPrecip;
yearlyFrac(isnan(yearlyFrac)) = 0;
yearlyFrac(isinf(yearlyFrac)) = 0;

%Summarize the data monthly
monthlyData = thisData./yearlyPrecipMonthly;
monthlyData(isnan(monthlyData)) = 0;
monthlyData(isinf(monthlyData)) = 0;
monthlyData(1:9,:) = []; %incomplete water year
monthlyData(end-2:end,:) = []; %incomplete water year
[months,monthlyFrac] = time_series_aggregate(monthlyInterval(:,1),monthlyData,'months','sum','years','mean');
[months,monthlyAvg] = time_series_aggregate(monthlyInterval(:,1),thisData(10:end-3,:),'months','sum','years','mean');


%Loop through the desired lu types and save to allData
allDataLuSoils.uplandET.mean = nanmean(thisData,1);
allDataLuSoils.uplandET.monthlyProportion = monthlyFrac ./ repmat(sum(monthlyFrac,1),[12,1]);
allDataLuSoils.uplandET.years = years;
allDataLuSoils.uplandET.months = months;
for m = 1:length(luNums)
    for n = 1:length(texNums)
        for o = 1:length(distNums)
            thisInd = luFind{luNums(m)} & soilsFind{texNums(n)} & distFind{distNums(o)};
            allDataLuSoils.uplandET.(luNames{m}).(texNames{n}).(distNames{o}).count = sum(thisInd);
            allDataLuSoils.uplandET.(luNames{m}).(texNames{n}).(distNames{o}).yearlyAvg = mean(yearlyAvg(:,thisInd),2);
            allDataLuSoils.uplandET.(luNames{m}).(texNames{n}).(distNames{o}).yearlyFrac = mean(yearlyFrac(:,thisInd),2);
            allDataLuSoils.uplandET.(luNames{m}).(texNames{n}).(distNames{o}).monthlyAvg = mean(monthlyAvg(:,thisInd),2);
            allDataLuSoils.uplandET.(luNames{m}).(texNames{n}).(distNames{o}).monthlyFrac = mean(monthlyFrac(:,thisInd),2);
        end
    end
end

%%
%--------------------------------------------------------------------------
%Output a table of counts
%--------------------------------------------------------------------------
counts = zeros(length(luNums),length(texNums),length(distNums));
for m = 1:length(luNums)
    for n = 1:length(texNums)
        for o = 1:length(distNums)
            counts(m,n,o) = allDataLuSoils.uplandR.(luNames{m}).(texNames{n}).(distNames{o}).count;
        end
    end
end
allDataLuSoils.counts = counts;
%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'-struct','allData');

%% Create Tables of Fluxes for Soil Types
component = 'uplandET';
lu = 'ag';
textures = {'Coarse','Semi_Coarse','Semi_Fine','Fine'};
bands = {'band1'};
statType = 'yearlyFrac';

components = {'uplandET','uplandR','uplandRun'};
for o = 1:length(components)
    tables.(components{o}).(lu) = zeros(length(lu),1);
    for m = 1:length(textures)
        for n = 1:length(bands)
            tempData = nanmean(allDataLuSoils.(components{o}).(lu).(textures{m}).(bands{n}).(statType));
            if n == 1
                data = tempData;
            else
                data = cat(2,data,tempData);
            end
        end
        tables.(components{o}).(lu)(m) = nanmean(data);
    end
end

if strcmpi(statType,'yearlyFrac') %normalize to 1
    sumFrac = zeros(length(textures),1);
    for m = 1:length(components)
       sumFrac = sumFrac + tables.(components{m}).(lu);
    end
    for m = 1:length(components)
        tables.(components{m}).(lu) = tables.(components{m}).(lu) ./ sumFrac;
    end
end

%% Create Tables of Fluxes for Land Uses Types

lu = {'urban','ag','shrub','deciduous','coniferous'};
textures = 'Coarse';
bands = {'band3'};
statType = 'yearlyFrac';

components = {'uplandET','uplandR','uplandRun'};
for o = 1:length(components)
    tables.(components{o}).(textures) = zeros(length(lu),1);
    for m = 1:length(lu)
        for n = 1:length(bands)
            tempData = nanmean(allDataLuSoils.(components{o}).(lu{m}).(textures).(bands{n}).(statType));
            if n == 1
                data = tempData;
            else
                data = cat(2,data,tempData);
            end
        end
        tables.(components{o}).(textures)(m) = nanmean(data);
    end
end

if strcmpi(statType,'yearlyFrac') %normalize to 1
    sumFrac = zeros(length(lu),1);
    for m = 1:length(components)
       sumFrac = sumFrac + tables.(components{m}).(textures);
    end
    for m = 1:length(components)
        tables.(components{m}).(textures) = tables.(components{m}).(textures) ./ sumFrac;
    end
end
%%
%--------------------------------------------------------------------------
%Create the 3-plot of precip, recharge, and ET vs distance
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[1 1 400 1000])

%Specify lu, texture, and statType
lu = 'deciduous';
texture = 'S';
statType = 'monthlyAvg';
minMax = [0,10];

%Precip
dataset = 'precip';
%Build the dataset
surface = zeros(12,length(distNums));
for m = 1:length(distNums)
    surface(:,m) = allDataLuSoils.(dataset).(lu).(texture).(distNames{m}).(statType);
end
%Remove NaN columns
testNan = all(isnan(surface),1);
surface(:,testNan) = [];
%Plot it
h1 = subplot(4,1,1);
imagesc(surface*100,minMax)
set(gca,'YDir','normal')
% contourf(surface,(0.01:0.01:0.1),'LineStyle','none')

%Set properties
title(dataset,'FontSize',15);
set(gca,'FontSize',14);
set(gca,'XTick',(0.5:1:0.5+sum(~testNan)+1));
set(gca,'YTick',(1:1:12));
set(gca,'XTickLabel',{'','','','','','','','',''});
set(gca,'YTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
ylabel('Calendar Month','FontSize',15)

%ET
dataset = 'uplandET';
%Build the dataset
surface = zeros(12,length(distNums));
for m = 1:length(distNums)
    surface(:,m) = allDataLuSoils.(dataset).(lu).(texture).(distNames{m}).(statType);
end
%Remove NaN columns
testNan = all(isnan(surface),1);
surface(:,testNan) = [];
%Plot it
h2 = subplot(4,1,2);
imagesc(surface*100,minMax)
set(gca,'YDir','normal')
% contourf(surface,(0.01:0.01:0.1),'LineStyle','none')

%Set properties
title(dataset,'FontSize',15);
set(gca,'FontSize',14);
set(gca,'XTick',(0.5:1:0.5+sum(~testNan)+1));
set(gca,'YTick',(1:1:12));
set(gca,'XTickLabel',{'','','','','','','','',''});
set(gca,'YTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
ylabel('Calendar Month','FontSize',15)


%Recharge
dataset = 'uplandR';
%Build the dataset
surface = zeros(12,length(distNums));
for m = 1:length(distNums)
    surface(:,m) = allDataLuSoils.(dataset).(lu).(texture).(distNames{m}).(statType);
end
%Remove NaN columns
testNan = all(isnan(surface),1);
surface(:,testNan) = [];
%Plot it
h3 = subplot(4,1,3);
imagesc(surface*100,minMax)
set(gca,'YDir','normal')
% contourf(surface,(0.01:0.01:0.1),'LineStyle','none')

%Set properties
title(dataset,'FontSize',15);
set(gca,'FontSize',14);
set(gca,'XTick',(0.5:1:0.5+sum(~testNan)+1));
set(gca,'YTick',(1:1:12));
set(gca,'YTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
xTickLabels = cell(length(distNums)+1,1);
for m = 1:length(distNums)
    xTickLabels{m} = num2str(distBands.min(m));
end
xTickLabels{m+1} = num2str(distBands.max(m));
nans = find(testNan);
xTickLabels(nans+1) = [];
set(gca,'XTickLabel',xTickLabels)
xlabel({'Distance from Lake Michigan','along Prevailing Wind Direction (km)'},'FontSize',15)
ylabel('Calendar Month','FontSize',15)

%Plot the colorbar
h4 = subplot(4,1,4);
colorbar(h4,'peer',h3);
set(gca,'FontSize',14);
set(gca,'XTick',(minMax(1):1:minMax(end)))
xlabel('Monthly Flux (cm)','FontSize',15)

%Resize
set(h3,'Position',[0.15,0.15,0.8,0.23])
set(h2,'Position',[0.15,0.43,0.8,0.23])
set(h1,'Position',[0.15,0.71,0.8,0.23])
set(h4,'Position',[0.15,0.05,0.8,0.03])
%%
%--------------------------------------------------------------------------
%Create the Monthly plot of soil dependence
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,400,700])

%Specify lu, distance, and statType
lu = 'ag';
distanceBands = [4,5,6];
colors = {'g','b','r','k'};


%ET
subplot(2,1,1)
hold on
dataset = 'uplandET';
for m=1:length(texNums)
    for n = 1:length(distanceBands)
        tempData = allDataLuSoils.(dataset).(lu).(texNames{m}).(distNames{distanceBands(n)}).monthlyFrac;
        if n == 1
            data = tempData;
        else
            data = cat(2,data,tempData);
        end
    end
    plot(allDataLuSoils.(dataset).months,nanmean(data,2)*100,colors{m},'LineWidth',0.75);
end

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[1,14]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'','','','','','','','','','','',''});
set(gca,'FontSize',14)
ylabel(gca,'Percent of W.Y. Precip','FontSize',15);
title(dataset,'FontSize',15)
set(gca,'Box','on')

%DP
subplot(2,1,2)
hold on
dataset = 'uplandR';
for m=1:length(texNums)
    for n = 1:length(distanceBands)
        tempData = allDataLuSoils.(dataset).(lu).(texNames{m}).(distNames{distanceBands(n)}).monthlyFrac;
        if n == 1
            data = tempData;
        else
            data = cat(2,data,tempData);
        end
    end
    plot(allDataLuSoils.(dataset).months,nanmean(data,2)*100,colors{m},'LineWidth',0.75);
end

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,10]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',14)
ylabel(gca,'Percent of W.Y. Precip','FontSize',15);
title(dataset,'FontSize',15)
legend(texNames)
xlabel('Calendar Month','FontSize',15);
set(gca,'Box','on')
%%
%--------------------------------------------------------------------------
%Create the Monthly plot of LU dependence
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,800,700])

%Specify lu, distance, and statType
texture = 'Coarse';
distanceBands = [4,5,6];
colors = {'r','m','g','k','b'};


%ET - Sands
subplot(2,2,1)
hold on
dataset = 'uplandET';
dataTitle = 'Upland ET - Sands';
for m=1:length(luNums)
    for n = 1:length(distanceBands)
        tempData = allDataLuSoils.(dataset).(luNames{m}).(texture).(distNames{distanceBands(n)}).monthlyFrac;
        if n == 1
            data = tempData;
        else
            data = cat(2,data,tempData);
        end
    end
    plot(allDataLuSoils.(dataset).months,nanmean(data,2)*100,colors{m},'LineWidth',0.75);
end

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,13]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'','','','','','','','','','','',''});
set(gca,'FontSize',13)
ylabel(gca,'Percent of W.Y. Precip','FontSize',14);
title(dataTitle,'FontSize',14)
set(gca,'Box','on')

%DP - Sands
subplot(2,2,2)
hold on
dataset = 'uplandR';
dataTitle = 'Upland DP - Sands';
for m=1:length(luNums)
    for n = 1:length(distanceBands)
        tempData = allDataLuSoils.(dataset).(luNames{m}).(texture).(distNames{distanceBands(n)}).monthlyFrac;
        if n == 1
            data = tempData;
        else
            data = cat(2,data,tempData);
        end
    end
    plot(allDataLuSoils.(dataset).months,nanmean(data,2)*100,colors{m},'LineWidth',0.75);
end

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,11]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'','','','','','','','','','','',''});
set(gca,'FontSize',13)
title(dataTitle,'FontSize',14)
set(gca,'Box','on')

%Specify lu, distance, and statType
texture = {'Semi_Coarse','Semi_Fine'};
distanceBands = [4,5,6];
colors = {'r','m','g','k','b'};

%ET - Loams
subplot(2,2,3)
hold on
dataset = 'uplandET';
dataTitle = 'Upland ET - Loams';
for m=1:length(luNums)
    for n = 1:length(distanceBands)
        for o = 1:length(texture)
            tempData = allDataLuSoils.(dataset).(luNames{m}).(texture{o}).(distNames{distanceBands(n)}).monthlyFrac;
            if n == 1
                data = tempData;
            else
                data = cat(2,data,tempData);
            end
        end
    end
    plot(allDataLuSoils.(dataset).months,nanmean(data,2)*100,colors{m},'LineWidth',0.75);
end

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,13]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',13)
ylabel(gca,'Percent of W.Y. Precip','FontSize',14);
title(dataTitle,'FontSize',14);
xlabel('Calendar Month','FontSize',14);
set(gca,'Box','on')

%DP - Loams
subplot(2,2,4)
hold on
dataset = 'uplandR';
dataTitle = 'Upland DP - Loams';
for m=1:length(luNums)
    for n = 1:length(distanceBands)
        for o = 1:length(texture)
            tempData = allDataLuSoils.(dataset).(luNames{m}).(texture{o}).(distNames{distanceBands(n)}).monthlyFrac;
            if n == 1
                data = tempData;
            else
                data = cat(2,data,tempData);
            end
        end
    end
    plot(allDataLuSoils.(dataset).months,nanmean(data,2)*100,colors{m},'LineWidth',0.75);
end

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,11]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',13)
title(dataTitle,'FontSize',14);
xlabel('Calendar Month','FontSize',14);
legend(luNames)
set(gca,'Box','on')



%%
%--------------------------------------------------------------------------
%Create the plots of ET and DP vs. soil texture
%--------------------------------------------------------------------------
lineWidth = 1.5;
smoothingParam = 0.85;

%Create the figure
figure
h1 = axes;
% h1 = subplot(2,1,1);
hold on
box on
% h2 = subplot(2,1,2);
% hold on
% box on
colors = {[237,32,36]/255,[223,196,0]/255,[57,83,164]/255,[105,189,69]/255,[0,0,0]/255};

%ET
dataset = 'uplandET';

%Specify the distance band
% distBand = distFind{1};
distBand = distFind{1} | distFind{2} | distFind{3} | distFind{4};

%Bins
% binMin = min(ksatVert);
% binMax = max(ksatVert);
% numBins = 10;
% percentiles = [0,(100/numBins:100/numBins:100)];
% bins = percentile(ksatVert(distBand),percentiles);
% tempWidth = (bins(2) - bins(1))/2;
% bins = [bins(1),bins(1)+tempWidth/2, bins(2:end)];
% diffBins = diff(bins);
% bins = bins(1:find(diffBins==0,1,'first'));

xFactor = 1000 * 60 * 24; %mm/hr
    
    
% thresh = 5;
% maxNum = 200;
%Plot the data
% for m = 1:length(luNums)
%     thisInd = luFind{luNums(m)} & distBand;
% %     bins = percentile(ksatVert(thisInd),percentiles);
%     thisData = allDataLuSoils.(dataset).mean ./ allDataLuSoils.precip.mean;
%     thisData = thisData(thisInd)';
%     thisX = ksatVert(thisInd);
%     evalX = (min(thisX):(max(thisX)-min(thisX))/99:max(thisX))';
%     thisY = loess(thisX,thisData,evalX,smoothingParam);
% %     sortMat = sortrows([thisX,thisY,thisData]);
% %     thisX = sortMat(:,1);
% %     thisY = sortMat(:,2);
% %     thisData = sortMat(:,3);
%     thisNum = size(thisX);
% %     [thisX,thisY,thisNum,thisStd] = deal(zeros(length(bins)-1,1));
% % %     thisX = (bins(1:end-1) + bins(2:end))/2;
% %     thisX = thisX + NaN;
% %     thisNum = thisNum + NaN;
% %     thisStd = thisStd + NaN;
% %     for n = 1:length(bins)-1
% %         loopInd = thisInd & (ksatVert >= bins(n)) & (ksatVert <=bins(n+1));
% %         if sum(loopInd) >= thresh
% % %             thisX(n) = nanmedian(ksatVert(loopInd));%nanmean(ksatVert(loopInd));
% %             thisX(n) = mean(bins(n:n+1));
% %             thisY(n) = nanmean(thisData(loopInd));
% %             thisStd(n) = nanstd(thisData(loopInd));
% %             thisNum(n) = sum(loopInd);
% %         end
% %     end
% %     axes(h1)
% %     patch([thisX;flipud(thisX)],[thisY*100+log10(thisNum);flipud(thisY*100-log10(thisNum))],colors{m},'FaceAlpha',0.5);
% 
% %     plot(h1,thisX*xFactor,thisData*100,'.','Color',[150,150,150]/255);
%     plot(h1,evalX*xFactor,thisY*100,'LineWidth',lineWidth,'Color',colors{m},'LineStyle','-','Marker','none');
%     
% %     plot(h2,thisX,thisNum,'Color',colors{m});
% %     scatter(thisX,thisY*100,thisNum/maxNum*20,colors{m});
% end

%DP
dataset = 'uplandR';

%Plot the data
for m = 1:length(luNums)
    thisInd = luFind{luNums(m)} & distBand;
    thisData = allDataLuSoils.(dataset).mean ./ allDataLuSoils.precip.mean;
    thisData = thisData(thisInd)';
    thisX = ksatVert(thisInd);
    evalX = (min(thisX):(max(thisX)-min(thisX))/99:max(thisX))';
    thisY = loess(thisX,thisData,evalX,smoothingParam);
%     sortMat = sortrows([thisX,thisY,thisData]);
%     thisX = sortMat(:,1);
%     thisY = sortMat(:,2);
%     thisData = sortMat(:,3);
%     thisX = thisX + NaN;
%     for n = 1:length(bins)-1
%         loopInd = thisInd & (ksatVert >= bins(n)) & (ksatVert <=bins(n+1));
%         if sum(loopInd) >= thresh
% %             thisX(n) = nanmedian(ksatVert(loopInd));%nanmean(ksatVert(loopInd));
%             thisX(n) = mean(bins(n:n+1));
%             thisY(n) = nanmean(thisData(loopInd));
%         end
%     end
    plot(h1,evalX*xFactor,thisY*100,'LineWidth',lineWidth,'Color',colors{m},'LineStyle','-','Marker','none');
end

%Specify some properties
% set(h1,'XLim',[bins(1),bins(end)*1.05]*xFactor);
% set(h1,'XLim',[mean(bins(1:2)),mean(bins(end-1:end))]*xFactor);
set(h1,'XLim',[0,80]);
% set(h1,'YLim',[20,80]);
% set(h2,'XLim',[mean(bins(1:2)),mean(bins(end-1:end))]);
% set(h2,'YScale','log');
set(h1,'FontSize',12);
ylabel('% of Annual Precipitation')
xlabel('Soil Vertical Saturated Conductivity (mm/hr)');
legend(luNames)

%% Evaluate the statistical model -- annual
smoothingParam = 0.85;

%For each landuse, evaluate the model for recharge
dataset = 'uplandR';

evalShape = size(evalKsatVert);
weightedY = zeros(evalShape);
for m = 1:length(luNums)
    thisInd = luFind{luNums(m)};
    thisData = allDataLuSoils.(dataset).mean ./ allDataLuSoils.precip.mean;
    thisData = thisData(thisInd)';
    thisX = ksatVert(thisInd);
    
    evalX = evalKsatVert(:);
    evalX(evalX < min(thisX)) = min(thisX);
    evalX(evalX > max(thisX)) = max(thisX);
    evalY = loess(thisX,thisData,evalX,smoothingParam);
    
    weightedY = weightedY + reshape(evalY,evalShape) .* evalLandWeightsReshape(:,:,m);
end
evalAvgRechargeAnnual = weightedY;

%% Evaluate the statistical model -- monthly proportions of annual recharge
smoothingParam = 0.85;

%For each landuse, evaluate the model for recharge
dataset = 'uplandR';

evalShape = size(evalKsatVert);
evalProportionDPMonthly = zeros([evalShape,12]);
for n = 1:12
    weightedY = zeros(evalShape);
    for m = 1:length(luNums)
        thisInd = luFind{luNums(m)};
        thisData = allDataLuSoils.(dataset).monthlyProportion(n,:);
        thisData = thisData(thisInd)';
        thisX = ksatVert(thisInd);
        
        evalX = evalKsatVert(:);
        evalX(evalX < min(thisX)) = min(thisX);
        evalX(evalX > max(thisX)) = max(thisX);
        evalY = loess(thisX,thisData,evalX,smoothingParam);
        
        weightedY = weightedY + reshape(evalY,evalShape) .* evalLandWeightsReshape(:,:,m);
    end
    evalProportionDPMonthly(:,:,n) = weightedY;
end

%% Combine annual and monthly proportions to create time-series of recharge grids, multiply upland fraction back out
numYears = length(evalPrecipYears);
numMonths = 12 * numYears;
startMonth = 10; %water year
annualMonths = [10,11,12,1,2,3,4,5,6,7,8,9];
evalMonthlyDP= zeros([size(evalAvgRechargeAnnual),numMonths]);
index = 0;
for m = 1:numYears
    for n = 1:12
        index = index + 1;
        evalMonthlyDP(:,:,index) = evalProportionDPMonthly(:,:,annualMonths(n)) .* evalAvgRechargeAnnual .* evalPrecipYearlyReshape(:,:,m) .* evalUplandFrac(evalLandLookup);
    end
end

    
%% Do a simple unsaturated zone routing
delayParam = 2.5; %d/m
maxDelay = 6; %months
shiftMonths = floor(evalUnsatThick * delayParam / 30) + 1;
shiftMonths(shiftMonths > maxDelay + 1) = maxDelay;

evalMonthlyRecharge = zeros(size(evalMonthlyDP)+[0,0,maxDelay]); %add maxDelay to the end
for m = 1:size(shiftMonths,1)
    for n = 1:size(shiftMonths,2)
        evalMonthlyRecharge(m,n,(shiftMonths(m,n):shiftMonths(m,n)+numMonths - 1)) = squeeze(evalMonthlyDP(m,n,:));
    end
end

%% Export grids
monthNames = datestr(datenum([2006,2006,2006,repmat(2007,1,12),repmat(2008,1,12),repmat(2009,1,12),repmat(2010,1,9)],repmat(annualMonths,1,4),ones(1,numMonths)));
gridDir = 'C:\Users\Anthony\Project_Files\2010\Bay_Harbor_Consulting\Model_Outputs\Statistical_Model\ASCII_Grids';
gridHeader.cellsize = gridHeader.cellsizeX;
for m = 1:numMonths
    asciiwrite(evalMonthlyRecharge(:,:,m),[gridDir,filesep,monthNames(m,:),'.asc'],gridHeader)
end
%% Export a movie
%Read in the shapefile for overlay
overlay = mexshape(overlayShape);
% for n = 1:length(overlay.Shape)
%     test = isnan(overlay.Shape(n).x);
%     overlay.Shape(n).x(test) = [];
%     overlay.Shape(n).y(test) = [];
% end

%Now, register the grid used here
gridX = gridHeader.left + (0:gridHeader.cols-1) * gridHeader.cellsizeX;
gridY = gridHeader.top - (0:gridHeader.rows-1) * gridHeader.cellsizeY;

aviobj = avifile([outputDir,filesep,'monthly_recharge.avi'],'Compression','none','fps',5,'Quality',75);
figure
set(gcf,'Position',[500,500,800,400],'Color',[1,1,1])
for m = 1:numMonths
    imagesc(gridX,gridY,evalMonthlyRecharge(:,:,m)*100/2.54);caxis([0,0.075*100/2.54]);colorbar
    %Set figure properties
    title(['Monthly Recharge (in): ',monthNames(m,:)],'FontSize',12);
    set(gca,'YDir','normal');
    set(gca,'XTick',[],'YTick',[]);
    axis equal
    axis tight
    %Overlay objects
    for n = 1:length(overlay.Shape)
        patch(overlay.Shape(n).x,overlay.Shape(n).y,[0.5,0.5,0.5],'EdgeColor','w','LineWidth',2,'FaceColor','c');
    end
    
    aviobj = addframe(aviobj,getframe(gcf));
end

close(gcf);
aviobj = close(aviobj);

