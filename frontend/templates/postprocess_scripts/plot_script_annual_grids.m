%This script creates annual Recharge for the entire model

%--------------------------------------------------------------------------
%Specify Dataset Grouping, Uncomment only 1 group
%--------------------------------------------------------------------------
%Recharge
name = 'recharge';
namespace = 'model';
sumDatasets = {'deep_percolation_upland','deep_percolation_wetland'};
domain = {'upland','wetland'};

%Air Temp
% name = 'air_temp';
% namespace = 'expanded';
% sumDatasets = {'temp_air'};
% domain = {'all'};

%Precip
% name = 'precip';
% namespace = 'model';
% sumDatasets = {'rain','snow'};
% domain = {'all','all'};

%Snow
% name = 'snow';
% namespace = 'expanded';
% sumDatasets = {'snow'};
% domain = {'all'};

%Upland Runoff
% name = 'upland_runoff';
% namespace = 'model';
% sumDatasets = {'precip_excess_upland'};
% domain = {'upland'};

%ET
% name = 'ET';
% namespace = 'EBCW';
% sumDatasets = {'evap_canopy','evap_depression','evap_ice_pack','evap_soil',...
%     'evap_snow_upland','evap_wetland','transp_upland','transp_wetland'};
% domain = {'all','upland','wetland','upland',...
%     'upland','wetland','upland','wetland'};

%Upland Recharge
% name = 'upland_recharge';
% namespace = 'model';
% sumDatasets = {'deep_percolation_upland'};
% domain = {'all'};


%Upland ET
% name = 'upland_ET';
% namespace = 'model';
% sumDatasets = {'evap_canopy','evap_depression','evap_soil','evap_snow_upland','transp_upland'};
% domain = {'all','all','all','all','all'};

%Irrigation
% name = 'irrigation';
% namespace = 'model';
% sumDatasets = {'irrigation_water_applied'};
% domain = {'all'};

%--------------------------------------------------------------------------
%Shouldn't need to change these
%--------------------------------------------------------------------------
%Specify the data types from the OUTF
type = 'grid';
dataType = 'plus';

%Load the model and run names from the OUTF
modelName = h5dataread(inputOUTF,'/info/group_name');
runName = h5dataread(inputOUTF,'/info/run_name');

%Build the output names
outputNameAvg = [modelName,'_',runName,'_',name,'_calendar_year_average','.txt'];
outputNameVar = [modelName,'_',runName,'_',name,'_calendar_year_coefvar','.txt'];
outputNameMin = [modelName,'_',runName,'_',name,'_min_calendar_year','.txt'];
outputNameMax = [modelName,'_',runName,'_',name,'_max_calendar_year','.txt'];
outputFileAvg = [outputDir,filesep,outputNameAvg];
outputFileVar = [outputDir,filesep,outputNameVar];
outputFileMin = [outputDir,filesep,outputNameMin];
outputFileMax = [outputDir,filesep,outputNameMax];

%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputOUTF,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputOUTF,['/',type,'/',namespace,'/','interval']);
startIndex = find(dateRange(1) >= interval(:,1) & dateRange(1) <= interval(:,2));
endIndex = find(dateRange(2) >= interval(:,1) & dateRange(2) <= interval(:,2));
numGrids = endIndex - startIndex + 1;

%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputOUTF,'/geolocation/bot');
cellsize = h5dataread(inputOUTF,'/geolocation/cellsize');
left = h5dataread(inputOUTF,'/geolocation/left');
numCol = h5dataread(inputOUTF,'/geolocation/num_col');
numRow = h5dataread(inputOUTF,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%--------------------------------------------------------------------------
%Load the rescale grids
%--------------------------------------------------------------------------
rescaleGrids = load(inputStatic,'structure');
rescaleGrids.structure.fraction_stream.data(isnan(rescaleGrids.structure.fraction_stream.data))=0;
rescaleGrids.stream_fraction.data = min(rescaleGrids.structure.fraction_stream.data, 1-rescaleGrids.structure.fraction_wetland.data);
wetFrac = trim_domain(~mask,rescaleGrids.structure.fraction_wetland.data + rescaleGrids.structure.fraction_stream.data)';
upFrac = 1 - wetFrac;
clear rescaleGrids
rescaleGrids = struct('upland',upFrac,'wetland',wetFrac,'all',upFrac+wetFrac);

%%
%--------------------------------------------------------------------------
%Loop through datasets, summing together
%--------------------------------------------------------------------------
%Get grid size
gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{1},'/',dataType],false);
%Create output
outGrid = zeros(numGrids,gridSize(2));

%Loop through datasets
for m = 1:length(sumDatasets)
    %Get this grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],false);
    
    %Load the data
    if length(gridSize) == 2
        thisGrid = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],true,...
            [startIndex - 1,0],[numGrids,gridSize(2)],[1,1]);
    else
        thisGrid = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],true,...
                [startIndex - 1,0,0],[numGrids,gridSize(2),gridSize(3)],[1,1,1]);
        thisGrid = sum(thisGrid,3);
    end
    
    outGrid = outGrid + thisGrid .* repmat(rescaleGrids.(domain{m}),numGrids,1);
end
clear tempData thisGrid

%%
%--------------------------------------------------------------------------
%Calculate grid mean, standard deviation, and coeffcicient of variation
%--------------------------------------------------------------------------
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),outGrid,'years','sum');
gridMean = mean(yearlyData,1);
gridStd = std(yearlyData,1);
gridCoefVar = gridStd./gridMean;
gridMin = min(yearlyData,1);
gridMax = max(yearlyData,1);

%%
%--------------------------------------------------------------------------
%Output to ASCII files
%--------------------------------------------------------------------------
outMask = zeros(size(mask)) + NaN;
outMask(~mask) = 1;
asciiwrite(gridMean(lookup).*(outMask),outputFileAvg,header);
% asciiwrite(gridCoefVar(lookup).*(outMask),outputFileVar,header);
% asciiwrite(gridMin(lookup).*(outMask),outputFileMin,header);
% asciiwrite(gridMax(lookup).*(outMask),outputFileMax,header);

% %%
% %--------------------------------------------------------------------------
% %Histogram
% %--------------------------------------------------------------------------
% bins = [0,(25:5:65),100]/100;
% plotBins = (22.5:5:67.5);
% data = gridMean(lookup);
% data(mask) = NaN;
% 
% x = histc(data(~isnan(data)),bins);
% bar(plotBins,x(1:end-1)/sum(x));
