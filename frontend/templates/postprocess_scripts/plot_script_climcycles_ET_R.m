%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
inputDir = 'D:\Users\Anthony\Modeling\ILHM\Output\test-v1\fullGrid13';
inputFilename = 'OUTF_test-v1_fullGrid13_20090409.h5';
inputFile = [inputDir,filesep,inputFilename];

climDir = '\\hydroflux\F\Data\Other_Downloaded\Climate_SST_Indices_1950_2008_CPC_NOAA';
climFiles = {'ao_cpc_noaa_1950_2008.txt','enso3.4_cpc_noaa_1950_2008.txt',...
    'nao_cpc_noaa_1950_2008.txt','pna_cpc_noaa_1950_2008.txt'};
climNames = {'ao','enso','nao','pna'};

type = 'grid';
namespace = 'expanded';
uplandRDatasets= {'deep_percolation_upland'};
uplandETDatasets = {'evap_canopy','evap_depression','evap_soil','evap_snow_upland','transp_upland'};
precipDatasets = {'precip'};

dataType = 'plus';

inputStatic = 'D:\Users\Anthony\Modeling\ILHM\Input\STAT_MRW-4lay-soil_12_20090315.mat';

outputDir = 'D:\Users\Anthony\Graphics\4_08_09_Thesis_Recalc';
outputFilename = 'climcycles_data.mat';
outputFile = [outputDir,filesep,outputFilename];

%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table, and the MRW grid
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputFile,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputFile,['/',type,'/',namespace,'/','interval']);

%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputFile,'/geolocation/bot');
cellsize = h5dataread(inputFile,'/geolocation/cellsize');
left = h5dataread(inputFile,'/geolocation/left');
numCol = h5dataread(inputFile,'/geolocation/num_col');
numRow = h5dataread(inputFile,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%%
%--------------------------------------------------------------------------
%Loop the climate cycles data
%--------------------------------------------------------------------------
climCycles = struct();
for m = 1:length(climNames)
    [months,data] = read_monthly_table([climDir,filesep,climFiles{m}],false);
    climCycles.(climNames{m}).months = months;
    climCycles.(climNames{m}).data = data;
end
%%
%--------------------------------------------------------------------------
%Loop through datasets, summing together
%--------------------------------------------------------------------------
%Create output
allData = struct();

%Get grid size
gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],false);
zerosGrid = zeros(gridSize);

%Get the precip data
precip = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],true,...
    [0,0],[gridSize(1),gridSize(2)],[1,1]);
%Yearly aggregation
[years,yearlyPrecip] = time_series_aggregate(interval(:,1),precip,'water years','sum');%only keep MRW columns
yearlyPrecip = yearlyPrecip(2:end-1,:); %don't want partial water years
%Monthly aggregation
[months,monthlyData] = time_series_aggregate(interval(:,1),precip,'months','sum');

%Save yearly averages to the allData struct
allData.precip.years = years;
allData.precip.yearlyAvg = mean(yearlyPrecip,2);
allData.precip.months = months;
allData.precip.monthlyAvg = mean(monthlyData,2);

%Upland R
thisData = zerosGrid;
for m = 1:length(uplandRDatasets)
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    thisData = thisData + tempData;
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyData = yearlyData(2:end-1,:);
yearlyFrac = yearlyData ./ yearlyPrecip;
[months,monthlyData] = time_series_aggregate(interval(:,1),thisData,'months','sum');

%Save yearly averages to the allData struct
allData.uplandR.years = years;
allData.uplandR.yearlyAvg = mean(yearlyData,2);
allData.uplandR.yearlyFrac = mean(yearlyFrac,2);
allData.uplandR.months = months;
allData.uplandR.monthlyAvg = mean(monthlyData,2);

%Upland ET
thisData = zerosGrid;
for m = 1:length(uplandETDatasets)
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],false);
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(:,1),thisData,'water years','sum');
years = years(2:end-1);
yearlyData = yearlyData(2:end-1,:);
yearlyFrac = yearlyData ./ yearlyPrecip;
[months,monthlyData] = time_series_aggregate(interval(:,1),thisData,'months','sum');

%Save yearly averages to the allData struct
allData.uplandET.years = years;
allData.uplandET.yearlyAvg = mean(yearlyData,2);
allData.uplandET.yearlyFrac = mean(yearlyFrac,2);
allData.uplandET.months = months;
allData.uplandET.monthlyAvg = mean(monthlyData,2);

%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'allData','climCycles');

%%
%--------------------------------------------------------------------------
%Test correlation between indices
%--------------------------------------------------------------------------
corrClim = zeros(length(climNames));
for m = 1:length(climNames)
    [seasons,seasonally] = time_series_aggregate(climCycles.(climNames{m}).months,climCycles.(climNames{m}).data,...
        'seasons','mean');
    for n = 1:length(climNames)
        [thisSeasons,thisSeasonally] = time_series_aggregate(climCycles.(climNames{n}).months,climCycles.(climNames{n}).data,...
        'seasons','mean');
        
        thisCorr = corrcoef(seasonally,thisSeasonally);
        corrClim(m,n) = thisCorr(2);
    end
end
%%
%--------------------------------------------------------------------------
%Correlate ET and R with climate cycle data, seasonal cycles, annual fluxes
%--------------------------------------------------------------------------
dataset = 'uplandR';
type = 'yearlyAvg';
[corr,sig] = deal(zeros(length(climNames),4));
figure
set(gcf,'Name',dataset)
for m = 1:length(climNames);
    [seasons,seasonally] = time_series_aggregate(climCycles.(climNames{m}).months,climCycles.(climNames{m}).data,...
        'seasons','mean');
    %If using 'seasons' and not 'quarters':
    seasons = seasons(2:end-1);
    seasonally = seasonally(2:end-1);
    %Add 31 days to seasons to make sure winter falls in the next year
    seasons = seasons + 31;
    for n = 1:4
        thisSeasons = seasons(n:4:end);
        thisSeasonally = seasonally(n:4:end);
        
        %Select overlap indexes
        overlapInd = bracket_index(thisSeasons,allData.(dataset).years(1),allData.(dataset).years(end)+364);
        
        %Calculate correlation coefficient
        [thisCorr,thisSig] = corrcoef(thisSeasonally(overlapInd(1):overlapInd(2)),allData.(dataset).(type));
        corr(m,n) = thisCorr(2);
        sig(m,n) = thisSig(2);
        
        %Plot this
        subplot(4,4,(m-1)*4 + n)
        plot(thisSeasonally(overlapInd(1):overlapInd(2)),allData.(dataset).(type),'o');
        title([climNames{m} ' for season ' num2str(n)]);
    end
end
%%
%--------------------------------------------------------------------------
%Correlate ET and R with climate cycle data, seasonal cycles, seasonal fluxes
%--------------------------------------------------------------------------
dataset = 'uplandET';
type = 'monthlyAvg';
[corr,sig] = deal(zeros(length(climNames),4));
figure
set(gcf,'Name',[dataset, ' seasons vs. seasons'])
for m = 1:length(climNames);
    [seasons,seasonally] = time_series_aggregate(climCycles.(climNames{m}).months,climCycles.(climNames{m}).data,...
        'seasons','mean');
    [dataSeasons,dataSeasonally] = time_series_aggregate(allData.(dataset).months,allData.(dataset).(type),...
        'seasons','mean');
    %If using 'seasons' and not 'quarters':
    dataSeasons = dataSeasons(2:end-1);
    dataSeasonally = dataSeasonally(2:end-1);
    seasons = seasons(2:end-1);
    seasonally = seasonally(2:end-1);
    for n = 1:4
        thisSeasons = seasons(n:4:end);
        thisSeasonally = seasonally(n:4:end);
        
        thisDataSeasons = dataSeasons(n:4:end);
        thisDataSeasonally = dataSeasonally(n:4:end);
        
        %Select overlap indexes
        overlapInd = bracket_index(thisSeasons,thisDataSeasons(1),thisDataSeasons(end)+1);
        
        %Calculate correlation coefficient
        [thisCorr,thisSig] = corrcoef(thisSeasonally(overlapInd(1):overlapInd(2)),thisDataSeasonally);
        corr(m,n) = thisCorr(2);
        sig(m,n) = thisSig(2);
        
        %Plot this
        subplot(4,4,(m-1)*4 + n)
        plot(thisSeasonally(overlapInd(1):overlapInd(2)),thisDataSeasonally,'o');
        title([climNames{m} ' for season ' num2str(n)]);
    end
end
%%
%--------------------------------------------------------------------------
%Correlate ET and R with climate cycle data, seasonal cycles, seasonal
%matrix
%--------------------------------------------------------------------------
dataset = 'precip';
type = 'monthlyAvg';
bracketYears = datenum({'1/1/1980','12/31/2006'});

[corr,sig] = deal(zeros(4,4,length(climNames)));

%Get the flux data
[dataSeasons,dataSeasonally] = time_series_aggregate(allData.(dataset).months,allData.(dataset).(type),...
    'seasons','mean');
%If using 'seasons' and not 'quarters':
dataSeasons = dataSeasons(2:end-1);
dataSeasonally = dataSeasonally(2:end-1);

for o = 1:length(climNames);
    [seasons,seasonally] = time_series_aggregate(climCycles.(climNames{o}).months,climCycles.(climNames{o}).data,...
        'seasons','mean');
    seasons = seasons(2:end-1);
    seasonally = seasonally(2:end-1);
    
    %Set up a new figure
%     figure
%     set(gcf,'Name',[' Y: seasons ',climNames{o},', X: seasons ',dataset])
    for m = 1:4
        thisSeasons = seasons(m:4:end);
        thisSeasonally = seasonally(m:4:end);
        for n = 1:4
            thisDataSeasons = dataSeasons(n:4:end);
            thisDataSeasonally = dataSeasonally(n:4:end);
            
            %Select overlap indexes
            overlapInd = bracket_index(thisSeasons,bracketYears(1),bracketYears(end));
            
            %Calculate correlation coefficient
            [thisCorr,thisSig] = corrcoef(thisSeasonally(overlapInd(1):overlapInd(2)),thisDataSeasonally);
            corr(m,n,o) = thisCorr(2);
            sig(m,n,o) = thisSig(2);
            
            %Plot this
%             subplot(4,4,(m-1)*4 + n)
%             plot(thisSeasonally(overlapInd(1):overlapInd(2)),thisDataSeasonally,'o');
%             title([climNames{o} ' ' num2str(m) ' ' dataset ' ' num2str(n)]);
        end
    end
end
%%
%--------------------------------------------------------------------------
%Correlate ET and R with climate cycle data, annually
%--------------------------------------------------------------------------
dataset = 'uplandR';
type = 'yearlyAvg';
[corr,sig] = deal(zeros(length(climNames),1));
figure
set(gcf,'Name',dataset)
for m = 1:length(climNames);
    [years,yearly] = time_series_aggregate(climCycles.(climNames{m}).months,climCycles.(climNames{m}).data,...
        'water years','mean');
    
    %Select overlap indexes
    overlapInd = bracket_index(years,allData.(dataset).years(1),allData.(dataset).years(end)+364);
    
    %Calculate correlation coefficient
    [thisCorr,thisSig] = corrcoef(yearly(overlapInd(1):overlapInd(2)),allData.(dataset).(type));
    corr(m) = thisCorr(2);
    sig(m) = thisSig(2);
    
    %Plot this
    subplot(4,1,m)
    plot(yearly(overlapInd(1):overlapInd(2)),allData.(dataset).(type),'o');
    title([climNames{m} ' for season ' num2str(n)]);
end
%%
%--------------------------------------------------------------------------
%Multiple regression,annually -- uses median value of index, mean is
%significantly worse
%--------------------------------------------------------------------------
dataset = 'uplandR';
type = 'yearlyAvg';
climNamesMult = {'nao','pna','enso','ao'};
[years1,yearly1] = time_series_aggregate(climCycles.(climNamesMult{1}).months,climCycles.(climNamesMult{1}).data,...
    'years','mean');
[years2,yearly2] = time_series_aggregate(climCycles.(climNamesMult{2}).months,climCycles.(climNamesMult{2}).data,...
    'years','mean');
[years3,yearly3] = time_series_aggregate(climCycles.(climNamesMult{3}).months,climCycles.(climNamesMult{3}).data,...
    'years','mean');
[years4,yearly4] = time_series_aggregate(climCycles.(climNamesMult{4}).months,climCycles.(climNamesMult{4}).data,...
    'years','mean');
yearlyData = allData.(dataset).(type)*100; %in cm

%Select overlap indexes
overlapInd1 = bracket_index(years1,allData.(dataset).years(1),allData.(dataset).years(end)+364);
overlapInd2 = bracket_index(years2,allData.(dataset).years(1),allData.(dataset).years(end)+364);
overlapInd3 = bracket_index(years3,allData.(dataset).years(1),allData.(dataset).years(end)+364);
overlapInd4 = bracket_index(years4,allData.(dataset).years(1),allData.(dataset).years(end)+364);

%Select x1 and x2
x1 = yearly1(overlapInd1(1):overlapInd1(2));
x2 = yearly2(overlapInd2(1):overlapInd2(2));
x3 = yearly3(overlapInd3(1):overlapInd3(2));
x4 = yearly4(overlapInd4(1):overlapInd4(2));

[model,anova] = multiple_regression('calculate',yearlyData(1:end-5),{x1(1:end-5)},{x2(1:end-5)},{x3(1:end-5)},{x4(1:end-5)});

%Calculate table
rmsrTable = zeros(length(model),1);
for m = 1:length(model)
    rmsrTable(m) = sqrt(mean(model{m}.residuals.^2));
end
%%
%--------------------------------------------------------------------------
%Multiple regression,seasonal indexes annual cycles
%--------------------------------------------------------------------------
dataset = 'uplandR';
type = 'yearlyAvg';
climNamesMult = {'nao','pna'};
season = [3,3];
[seasons1,seasonally1] = time_series_aggregate(climCycles.(climNamesMult{1}).months,climCycles.(climNamesMult{1}).data,...
    'seasons','mean');
[seasons2,seasonally2] = time_series_aggregate(climCycles.(climNamesMult{2}).months,climCycles.(climNamesMult{2}).data,...
    'seasons','mean');
yearlyData = allData.(dataset).(type);
%If using 'seasons' and not 'quarters': don't want incomplete seasons
%included
seasons1 = seasons1(2:end-1);
seasonally1 = seasonally1(2:end-1);
seasons2 = seasons2(2:end-1);
seasonally2 = seasonally2(2:end-1);

%Extract only the desired season
thisSeasons1 = seasons1(season(1):4:end);
thisSeasonally1 = seasonally1(season(1):4:end);
thisSeasons2 = seasons2(season(2):4:end);
thisSeasonally2 = seasonally2(season(2):4:end);    

%Last two seasons (Fall and Winter) have calendar year, but water year is
%assigned as jan1 of following year
%Select overlap indexes
overlapInd1 = bracket_index(thisSeasons1,allData.(dataset).years(1)-100,allData.(dataset).years(end)+265);
overlapInd2 = bracket_index(thisSeasons2,allData.(dataset).years(1)-100,allData.(dataset).years(end)+265);

%Select x1 and x2
x1 = thisSeasonally1(overlapInd1(1):overlapInd1(2));
x2 = thisSeasonally2(overlapInd2(1):overlapInd2(2));
model = multiple_regression(yearlyData,x1,x2,allData.precip.yearlyAvg);
