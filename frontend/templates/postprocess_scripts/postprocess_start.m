%--------------------------------------------------------------------------
%Specify Common Inputs and Outputs
%--------------------------------------------------------------------------
%Specify Model Output Directory
inputDir = 'E:\Anthony\Modeling\ILHM\Output\AGU_2012\6-updated_upland_T';
% inputDir = 'F:\Users\Anthony\Modeling\ILHM\Output\CHP\2008-2013-slice-irrig-wait';
% inputDir = 'Y:\Users\anthony\Modeling\ILHM\Output\AGU-2012\1-2095-A1B';


%Specify input grids
% surfaceGrid = 'F:\Users\Anthony\Modeling\ILHM\Prep\2014\CHP\Model_Layers\Surface\ibound.tif';
modflowGrid = 'Z:\Modeling\ILHM\Prep\2014\EMRW\Model_Layers\Groundwater\ibound_lay1';
surfaceGrid = 'Z:\Modeling\ILHM\Prep\2014\EMRW\Model_Layers\Surface\ibound';


%Specify years to use
% dateRange = datenum({'1/1/2009 00:00:00','12/31/2010 23:00:00'},'mm/dd/yyyy HH:MM:SS');
dateRange = datenum({'1/1/1980 00:00:00','12/31/2007 23:00:00'},'mm/dd/yyyy HH:MM:SS');

%Specify the graphics output directory
outputDir = 'F:\Users\Anthony\Graphics\6_16_15_Sea_Grant_Meeting';

%Calculate everything else
OUTF = dir([inputDir,filesep,'OUTF*']);
OUTS = dir([inputDir,filesep,'OUTS*']);
DUMP = dir([inputDir,filesep,'DUMP*']);
inputOUTF = [inputDir,filesep,OUTF.name];
inputOUTS = [inputDir,filesep,OUTS.name];
inputDUMP = [inputDir,filesep,DUMP.name];
%%
% Determine static and landscape files
load(inputDUMP,'structure');
inputStatic = structure.paths.input.static;
inputLandscape = structure.paths.input.landscape;
inputObs = structure.paths.input.obs;
clear structure

% % inputStatic = 'F:\Users\Anthony\Modeling\ILHM\Input\STAT_CHP_1-no-gndwater-runoff-bad-flowacc_20140129.mat';
% % inputLandscape = 'F:\Users\Anthony\Modeling\ILHM\InputLAND_CHP-AGU-2013-current_1_20140129.h5';
% inputStatic = 'F:\Users\Anthony\Modeling\ILHM\Input\STAT_MRW-New-SSURGO_3-et-depth-3m_20121128.mat';
% inputLandscape = 'F:\Users\Anthony\Modeling\ILHM\Input\LAND_MRW-AGU-2012-current_1_20121125.h5';


