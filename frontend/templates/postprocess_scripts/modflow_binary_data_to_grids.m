%This script dumps a MODFLOW binary file to grids, naming them according to
%user-specified conventions

%Specify inputs
inputFilename = 'recharge.dat';
inputDir = 'D:\Users\Anthony\Modeling\ILHM\Output\bay_harbor\no-irrigation-5\groundwater';
inputFile = [inputDir,filesep,inputFilename];

inputHeader = import_grid('C:\Users\Anthony\Data\GIS_Data\Bay_Harbor\Model_Layers\ILHM\ibound','header');

inputDateStart = datenum('01/01/2006','mm/dd/yyyy');
inputDateMult = 1; %multiply by totim to get dates in days

%Specify outputs
outputDir = 'C:\Users\Anthony\Project_Files\2010\Bay_Harbor_Consulting\Model_Outputs\ILHM\ASCII_Grids';
outputPrefix = '';
outputDateform = 'dd-mmm-yyyy';

%Now, read in the file, looping until the end of the file is reached
m = true;
fid = fopen(inputFile);
while m
    kstp = fread(fid,1,'int');
    if ~isempty(kstp)
        kper = fread(fid,1,'int');
        pertim = fread(fid,1,'float32');
        totim = fread(fid,1,'float32');
        text_in = fread(fid,16,'char');
        ncol = fread(fid,1,'int');
        nrow = fread(fid,1,'int');
        nlay = fread(fid,1,'int');
        data = fread(fid,ncol*nrow*nlay,'float32') .* pertim;
        
        %Reformat for output
        outData = reshape(data,ncol,nrow)';
        outDate = datestr(inputDateStart + totim);
        
        %Write output
        asciiwrite(outData,[outputDir,filesep,outputPrefix,datestr(outDate,outputDateform),'.asc'],inputHeader);
    else
        m = false;
    end
end
fclose(fid);
    