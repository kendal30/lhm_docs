%This script plots monthly recharge curves for a number of different
%scenarios

%Specify path and scenario information
basePath = '\\hydroserver\Anthony\modeling\Model_runs\ILHM\Expanded_Model';
scenarios = struct('names',{'Present','2070 Baseline','2070 Slow','2070 Baseline IPCC','2070 Slow IPCC'},...
    'paths',{'modern_baseline_2_rerun','forbase2070_baseline_3','forslow2070_baseline_4',...
    'forbase2070_ipcc_clim_change','forslow2070_ipcc_clim_change'});
inFile = 'ILHM_dump_2005.mat'; %the forslow2070_baseline_4 2006 file is corrupt I think
inVar = 'visualization';
plotVars = {'visualization.means.upland_recharge'};
% plotVars = {'visualization.means.snow_evap','visualization.means.canopy_evap',...
%     'visualization.means.soil_evap','visualization.means.depression_evap',...
%     'visualization.means.transpiration{1}','visualization.means.transpiration{2}'};
normVars = {'visualization.means.precip'};
modelHours = datenum('1/1/1980')+(0:1:236687)'/24;
aggregatePeriod = 'months';
aggregateType = 'sum';
crossPeriod = 'years';
crossType = 'mean';

%Loop through the scenarios
currDir = pwd;
cd(basePath);

numScenarios = length(scenarios);
plotData = cell(numScenarios,1);
normData = cell(numScenarios,1);
for m = 1:numScenarios
    cd(scenarios(m).paths)
    load(inFile,inVar);
    for n = 1:length(plotVars)
        loopData = eval(plotVars{n});
        if n == 1
            tempData = loopData;
        else
            tempData = tempData + loopData;
        end
    end
    tempNormData = eval(normVars{1});
    [modelMonths,plotData{m}] = time_series_aggregate(modelHours,tempData,...
        aggregatePeriod,aggregateType,crossPeriod,crossType);
    [modelMonths,normData{m}] = time_series_aggregate(modelHours,tempNormData,...
        aggregatePeriod,aggregateType,crossPeriod,crossType);
    cd(basePath);
end
cd(currDir)