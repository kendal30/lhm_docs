function [data,climBuffer] = read_climate_variable(varName,date,climIndex,climBuffer,paths,zerosGrid)
%This is a function intended to read in a single climate variable for
%visualization or testing purposes.  The inputs are all the same as the
%actual ILHM read_climate function.

%------------------------------------------------------------------
%Match the Date with the Stored Index to get the Data Row
%------------------------------------------------------------------
%Check to see if the next row in the index matches the current date
currIndexRow = climBuffer.prevIndexRow + 1;

%Check to see if the current date is equal to the index date, to
%the nearest hour
dateVector = datevec(date);
test = all(dateVector(1:4) == climIndex.index(currIndexRow,1:4));

%If this guess of the correct row is wrong, then find the correct
%row in the index, this is slow, but luckily it shouldn't happen
%often
if ~test
    %Find the correct row
    currIndexRow = find(ismember(climIndex.index(:,1:4), dateVector(1:4),'rows'),1,'first');
    assert_LHM(~isempty(currIndexRow),['The current date was not found in the index of ',varnames(m).in]);
end
%Re-assign the prevRow variable
climBuffer.prevIndexRow = currIndexRow;

%------------------------------------------------------------------
%Test to see if data needs to be read in for this timestep
%------------------------------------------------------------------
dataRow = climIndex.index(currIndexRow,5);
if dataRow == -1 || dataRow == -99999
    %This flag indicates values are all zeros
    %Actually, -99999 indicates that the value is unavailable,
    %which can only occur for input fluxes
    data = zerosGrid;
elseif dataRow > 0
    %--------------------------------------------------------------
    %Lookup Sheet Handling
    %--------------------------------------------------------------
    %Determine the sheet of the lookup table to use
    lookupSheet = climIndex.index(currIndexRow,6);
    
    %Check to see if the lookup table needs to be read in, or if
    %the stored one is the same as the current
    readSheet = ~(climBuffer.lookupSheet == lookupSheet);
    
    if readSheet
        %Read in the lookup table
        lookup = h5dataread(paths.input.climate,['/',varName,'/lookup'],...
            false,[0,0,lookupSheet-1],[climIndex.lookupSize(1),climIndex.lookupSize(2),1],[1,1,1]);
        
        %Save this lookup sheet to the climBuffer
        climBuffer.lookup = lookup;
        climBuffer.lookupSheet = lookupSheet;
    else
        lookup = climBuffer.lookup;
    end
    
    %--------------------------------------------------------------
    %Data Buffer Handling
    %--------------------------------------------------------------
    %Determine if the data row is contained within the rows in the
    %buffer
    buffRow = find(ismember(climBuffer.dataRows,dataRow),1,'first');
    %If not, read in a new buffer starting at the data row
    if isempty(buffRow)
        %Determine the number of rows to read in
        numRows = min(climIndex.dataSize(1) - dataRow + 1, climBuffer.bufferLength);
        %Read in the new rows
        climBuffer.dataRows = (dataRow : dataRow + numRows - 1);
        climBuffer.data = h5dataread(paths.input.climate,...
            ['/',varName,'/data'],true,[dataRow-1,0],...
            [numRows,climIndex.dataSize(2)],[1,1]);
        %Update the buffRow
        buffRow = 1;
    end
    
    %Now, from the buffer, pull a single row for the tempData
    data = climBuffer.data(buffRow,:)';
    
    %Use the lookup table to create the data grid from tempData
    data = data(lookup);
else
    error_LHM(['An invalid data row value was found in the index table of ',varName]);
end
end