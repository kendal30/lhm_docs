%This script makes a movie of LAI data

%Specify these inputs
clipGridLoc = 'F:\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\LAI\buffbndgrd';
workingDir = 'F:\users\anthony\code\workspace';
inputFile = 'cleaned_landscape.h5';
outMovie = 'lai_movie.avi';

startDate = '01/01/2004 00:00:00';
endDate = '12/31/2004 23:00:00';

%Read in the header and parse into a more useful format
[clipGrid,laiGeoLoc] = import_grid(clipGridLoc);
mask = clipGrid == ~255;

%Read in the index and determine which rows to pull in
index = h5dataread([workingDir,filesep,inputFile],'/lai/index');
%trim to only dates where data is not NaN
index = index(~isnan(index(:,2)),:);
%Determine start and end indeces
bracketIndex = bracket_index(index(:,1),datenum(startDate,'mm/dd/yyyy HH:MM:SS'),...
    datenum(endDate,'mm/dd/yyyy HH:MM:SS'));
trimIndex = index(bracketIndex(1):bracketIndex(2),:);
readIndex = trimIndex(trimIndex(:,2)~=-1,2);
times = trimIndex(trimIndex(:,2)~=-1,1);

%Get the data
data = h5dataread([workingDir,filesep,inputFile],'/lai/data',true,...
    [readIndex(1)-1,0],[length(readIndex),laiGeoLoc.rows*laiGeoLoc.cols],[1,1]);

%Get the mean of the dataset
timeseries = zeros(size(data,1),1);
for m = 1:size(data,1)
   timeseries(m) = nanmean(data(m,mask)); 
end

%Set up the colormap
maxVal = max(data(:));
scaling=[0,6];
f1 = figure;
set(gcf,'Position',[200,200,500,600])
% test_colormap = fliplr(get(gcf,'Colormap'));
test_colormap=usercolormap([166,120,84],[223,231,192],[178,255,161],[0,132,152])/255;
background_color=[240/255,235/255,219/255];
test_colormap(1,:)=background_color;
set(gcf,'Colormap',test_colormap,'Color',background_color);

%Generate a movie of the data
datestring = {'1/1/2004','2/1/2004','3/1/2004','4/1/2004','5/1/2004','6/1/2004',...
    '7/1/2004','8/1/2004','9/1/2004','10/1/2004','11/1/2004','12/1/2004'};

for m = 1:size(data,1)
    loopFrame = reshape(data(m,:),laiGeoLoc.rows,laiGeoLoc.cols);
    test = (loopFrame<0.25) & mask;
    loopFrame(test) = 0.25;

    g1=subplot(2,1,1);
    imagesc(loopFrame,scaling);
    g2=colorbar;
    g3=subplot(2,1,2);
    plot(times,timeseries);
    hold on
    plot(times(m),timeseries(m),'o','MarkerEdgeColor','k','MarkerFaceColor','g');
    hold off
    
    set(g1,'Position',[0.03,0.23,0.8,0.8]);
    set(g1,'PlotBoxAspectRatio',[1 1 1],'DataAspectRatioMode','auto'); %square
    set(g1,'Visible','off');
    xlabel(g2,'LAI (-)');
    set(g3,'Position',[0.1,0.05,0.85,0.2]);
    ylabel(g3,'Avg. LAI (-)');
    set(g3,'XLim',[datenum(datestring(1)),times(end)]);
    set(g3,'YLim',[min(timeseries),max(timeseries)]*1.1);
    set(g3,'XTick',datenum(datestring));
    set(g3,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'})
    laiMovie(m)=getframe(gcf);
end

movie2avi(laiMovie,[workingDir,filesep,outMovie],'FPS',4,'COMPRESSION','none');