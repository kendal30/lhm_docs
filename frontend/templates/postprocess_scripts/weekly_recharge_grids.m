%this script parses the output recharge file and generates weekly grids of recharge
%in ASCII grid format;

%specify the name and location of the binary-formatted recharge file
recharge_file='recharge.dat';
recharge_directory='D:\Users\Anthony\Modeling\ILHM\Output\bay_harbor\no-irrigation-5\groundwater';

[paths,xllcornerILHM,yllcornerILHM,cellsizeILHM,numColILHM,numRowILHM] = ...
    data_controller('structure','pass',...
    'paths','left','bot','cell_resolution','num_col','num_row');
[structure] = data_controller('groundwater','pass','structure');

%Specify the number of grids contained in the file
num_grids=57;

%Specify how to coarsen the grid
dimILHM = struct('top',yllcornerILHM + cellsizeILHM * (numRowILHM + 1),'left',xllcornerILHM,...
    'cellsizeX',cellsizeILHM,'cellsizeY',cellsizeILHM,'rows',numRowILHM,'cols',numColILHM);
dimMODFLOW = struct('top',structure.yllcorner + structure.cellsize*(structure.num_row+1),'left',structure.xllcorner,...
    'cellsizeX',structure.cellsize,'cellsizeY',structure.cellsize,'rows',structure.num_row,'cols',structure.num_col);
aggFactor = round(dimILHM.cellsizeX / dimMODFLOW.cellsizeX);

%Specify the times of each grid
start_time=datenum('1/1/2006');
days_per_grid=7;
times=start_time+(0:days_per_grid:days_per_grid*(num_grids-1));

%Specify what time range is desired for output
start_out=datenum('1/1/2006');
num_out=57;
start_index=find(abs(start_out-times)==min(abs(start_out-times)));
end_index=start_index+num_out-1;

%Specify the number of bits read per loop
num_bits=32+32+32+32+16*8+32+32+32+structure.num_row*structure.num_col*32;

%Load the ibound grid
% gridIn = load(paths.input.static,'gndwater_ibound');
% ibound = gridIn.gndwater_ibound.data(:,:,1);
gridsIn = load_grid_params(paths.input.static,'groundwater');
ibound = gridsIn.groundwater.ibound.data;
clear gridIn
mask = ibound == 0;
%%
%set up the colormap and axes
scaling=[1e-8,65];
f1 = figure;
set(gcf,'Position',[200,200,500,600])
test_colormap = get(gcf,'Colormap');
%test_colormap=usercolormap([135,38,38],[242,159,34],[255,255,191],[78,81,181],[39,32,122])/255;
background_color=[240/255,235/255,219/255];
test_colormap(1,:)=background_color;
set(gcf,'Colormap',test_colormap,'Color',background_color);
%%
curr_dir=cd;
cd(recharge_directory)
%%
fid=fopen(recharge_file);

%Skip the number of bytes corresponding to the number of skipped grids
fseek(fid,num_bits/8*(start_index-1),'cof');
index=0;
h=waitbar(0,'Preparing Time-Series Plot');
timeseries = zeros(num_grids,1);
for m=start_index:end_index
    index=m-start_index+1;
    waitbar(index/num_out,h);
	kstp=fread(fid,1,'int');
	kper=fread(fid,1,'int');
	pertim=fread(fid,1,'float32');
	totim=fread(fid,1,'float32');
	text_in=fread(fid,16,'char');
	ncol=fread(fid,1,'int');
	nrow=fread(fid,1,'int');
	nlay=fread(fid,1,'int');
	data=fread(fid,ncol*nrow*nlay,'float32');
    data=reshape(data,ncol,nrow)'*pertim;
    max(data(:))
    data(mask) = NaN;
    timeseries(m) = mean(data(~isnan(data)))*1000;
end
close(h)
fclose(fid);
%%
fid=fopen(recharge_file);

%Skip the number of bytes corresponding to the number of skipped grids
fseek(fid,num_bits/8*(start_index-1),'cof');
index=0;
h=waitbar(0,'Reading Recharge Data');
for m=start_index:end_index
    index=m-start_index+1;
    waitbar(index/num_out,h);
    figure(f1);
	kstp=fread(fid,1,'int');
	kper=fread(fid,1,'int');
	pertim=fread(fid,1,'float32');
	totim=fread(fid,1,'float32');
	text_in=fread(fid,16,'char');
	ncol=fread(fid,1,'int');
	nrow=fread(fid,1,'int');
	nlay=fread(fid,1,'int');
	data=fread(fid,ncol*nrow*nlay,'float32');
    data=reshape(data,ncol,nrow)'*pertim;
    data(mask) = 0;
    data=aggregate(dimILHM,dimMODFLOW,data*1000,aggFactor,'mean');
    g1=subplot(2,1,1);
    imagesc(data,scaling);
    g2=colorbar;
    g3=subplot(2,1,2);
    plot(times(start_index:end_index),timeseries(start_index:end_index));
    hold on
    plot(times(m),timeseries(m),'o','MarkerEdgeColor','k','MarkerFaceColor','g');
    hold off
    set(g1,'Position',[0.03,0.23,0.8,0.8]);
    set(g1,'PlotBoxAspectRatio',[1 1 1],'DataAspectRatioMode','auto'); %square
    set(g1,'Visible','off');
    xlabel(g2,'recharge (mm)');
    set(g3,'Position',[0.1,0.05,0.85,0.2]);
    ylabel(g3,'average recharge (mm)');
    set(g3,'XLim',[times(start_index),times(end_index)]);
    set(g3,'YLim',[min(timeseries),max(timeseries)]*1.1);
    set(g3,'XTick',datenum({'1/1/2004','2/1/2004','3/1/2004','4/1/2004','5/1/2004',...
        '6/1/2004','7/1/2004','8/1/2004','9/1/2004','10/1/2004','11/1/2004','12/1/2004'}));
    set(g3,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
    recharge_movie(index)=getframe(gcf);
end
close(h);
fclose(fid);

%%
cd(curr_dir)
%Write out the movie
v = VideoWriter([pathName,filesep,fileName],'MPEG-4');
v.FrameRate = 4;
v.Quality = 100;
open(v);
for m = 1:size(plotData,3)
    writeVideo(v,plotData(:,:,m));
end
close(v)

% movie2avi(recharge_movie,'test_video.avi','FPS',3,'COMPRESSION','none');
