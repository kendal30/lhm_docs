%This script reads the MODFLOW formatted HED file and writes out ascii
%grids for use in ArcGIS

%Specify Inputs and Outputs------------------------------------------------
modelDir = 'E:\Anthony\Modeling\ILHM\Output\AGU_2012\3-current-fullPeriod_updated_source';
outputDir = 'S:\Users\kendal30\Project_Files\2015\EMRW_heads_for_Jeremy';

%Read in model parameters--------------------------------------------------
%Get INIT file, load groundwaterStruct structure
initFile = dir([modelDir,'\INIT*']);
initData = load([modelDir,filesep,initFile.name],'groundwater');
groundwaterStruct = initData.groundwater.structure;
clear initData

%Get model name, start time
modelName = groundwaterStruct.model_name;
modelStartTime = groundwaterStruct.start_date;

%Get projection structure variables
header = struct('cols',groundwaterStruct.num_col,'rows',groundwaterStruct.num_row,...
    'left',groundwaterStruct.xllcorner,'bottom',groundwaterStruct.yllcorner,...
    'cellsize',groundwaterStruct.cellsize,'noData',groundwaterStruct.hnoflo);

%Read in heads and write out grids-----------------------------------------
headsStruct = modflow_format_heads_read([modelDir,filesep,'groundwater',filesep,modelName,'.hed']);
headsSize = size(headsStruct.heads);
if length(headsSize)<4
    headsSize(4)=1;
end
for m = 1:headsSize(4)
    thisTime = modelStartTime + headsStruct.time(m) - 1;
    for n = 1:headsSize(3)
        thisName = [outputDir,filesep,modelName,'_head_lay',num2str(n),'_',datestr(thisTime,'yyyymmdd'),'.asc'];
        asciiwrite(headsStruct.heads(:,:,n,m),thisName,header);
    end
end