%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
%Specify Output Files
outputFilename = 'monthly_ET_R_data.mat'; %'monthly_ET_R_data.mat';
outputFile = [outputDir,filesep,outputFilename];

%Specify input datasets
type = 'grid';
namespace = 'expanded';
dataType = 'plus';

%Precip Datasets
precipDatasets = {'precip'};

%Total Watershed Datasets
allRDatasets = {'deep_percolation_upland','deep_percolation_wetland'};
allRDomain = {'upland','wetland'};

allETDatasets = {'evap_canopy','evap_depression','evap_ice_pack','evap_soil',...
    'evap_snow_upland','evap_wetland','transp_upland','transp_wetland'};
allETDomain = {'all','upland','wetland','upland',...
    'upland','wetland','upland','wetland'};

allRunDatasets = {'precip_excess_upland','precip_excess_wetland'};
allRunDomain = {'upland','wetland'};

%Upland Datasets
uplandRDatasets= {'deep_percolation_upland'};
uplandETDatasets = {'evap_canopy','evap_depression','evap_soil','evap_snow_upland','transp_upland'};
uplandRunDatasets = {'precip_excess_upland'};

%Delay parameters
delayParam = 2.47; %days/meter
%force a maximum stress period delay
maxDelay = 25; %for this model, this is 1/2 year

%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table, and the MRW grid, and rescale
%Grids, and delay grid
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputOUTF,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table, determine index range
interval = h5dataread(inputOUTF,['/',type,'/',namespace,'/','interval']);
startIndex = find(dateRange(1) >= interval(:,1) & dateRange(1) <= interval(:,2));
endIndex = find(dateRange(2) >= interval(:,1) & dateRange(2) <= interval(:,2));

%Load the MRW grid, and trim to model domain
loadData = load(inputStatic,'mrw_grid');
mrwGrid = trim_domain(double(~mask),loadData.mrw_grid.data);
mrwGrid(isnan(mrwGrid))=0;
mrwGrid = logical(mrwGrid);
mrwFind = find(mrwGrid);

%Load the rescale grids
rescaleGrids = load(inputStatic,'water_fraction','stream_fraction');
rescaleGrids.stream_fraction.data = min(rescaleGrids.stream_fraction.data, 1-rescaleGrids.water_fraction.data);
wetFrac = trim_domain(~mask,rescaleGrids.water_fraction.data + rescaleGrids.stream_fraction.data)';
upFrac = 1 - wetFrac;
clear rescaleGrids
rescaleGrids = struct('upland',upFrac,'wetland',wetFrac,'all',upFrac+wetFrac);

%Load geolocation information
bot = h5dataread(inputOUTF,'/geolocation/bot');
cellsize = h5dataread(inputOUTF,'/geolocation/cellsize');
left = h5dataread(inputOUTF,'/geolocation/left');
numCol = h5dataread(inputOUTF,'/geolocation/num_col');
numRow = h5dataread(inputOUTF,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%Delay grid
loadData = load(inputStatic,'unsat_thickness');
thickGrid = trim_domain(double(~mask),loadData.unsat_thickness.data);
delayGrid = thickGrid * delayParam/7; %in weeks
delayGrid(delayGrid>maxDelay) = maxDelay;
delayGrid = round(delayGrid);

%% Load Precip
%Get grid size
gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],false);
zerosGrid = zeros(gridSize);

%Create output
allDataMonthYear = struct();

%Get the precip data
precip = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],true,...
    [0,0],[gridSize(1),gridSize(2)],[1,1]);
%Yearly aggregation
[years,yearlyPrecip] = time_series_aggregate(interval(startIndex:endIndex,1),precip(startIndex:endIndex,mrwFind),'water years','sum');%only keep MRW columns
[months,monthlyPrecip] = time_series_aggregate(interval(startIndex:endIndex,1),precip(startIndex:endIndex,mrwFind),'months','sum','years','nanmedian');
allDataMonthYear.precip.years = years;
allDataMonthYear.precip.yearlyAvg = mean(yearlyPrecip,2);
allDataMonthYear.precip.monthlyAvg = nanmean(monthlyPrecip,2);

%% Load Watershed Totals
%All R
thisData = zerosGrid;
for m = 1:length(allRDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',allRDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',allRDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    thisData = thisData + tempData .* repmat(rescaleGrids.(allRDomain{m}),gridSize(1),1);
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,mrwFind),'water years','sum');
yearlyFrac = yearlyData ./ yearlyPrecip;
[months,monthlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,mrwFind),'months','sum','years','mean');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.allR.years = years;
allDataMonthYear.allR.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.allR.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.allR.months = months;
allDataMonthYear.allR.monthlyAvg = mean(monthlyData,2);

%All ET
thisData = zerosGrid;
for m = 1:length(allETDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],false);
    %Load monthly data
    loopData = zerosGrid;
    if length(gridSize) == 2
        tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        loopData = loopData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            loopData = loopData + tempData;
        end
    end
    thisData = thisData + loopData .* repmat(rescaleGrids.(allETDomain{m}),gridSize(1),1);
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,mrwFind),'water years','sum');
yearlyFrac = yearlyData ./ yearlyPrecip;
[months,monthlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,mrwFind),'months','sum','years','median');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.allET.years = years;
allDataMonthYear.allET.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.allET.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.allET.months = months;
allDataMonthYear.allET.monthlyAvg = mean(monthlyData,2);

%All Runoff - this is a bit special
%Get the surface parts
thisData = zerosGrid;
for m = 1:length(allRunDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',allRunDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',allRunDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    thisData = thisData + tempData .* repmat(rescaleGrids.(allRunDomain{m}),gridSize(1),1);
end
%Get the groundwater part
load(inputOUTS,'postDischargeMonthly','postEvapMonthly');
postDischargeMonthly = double(postDischargeMonthly(startIndex:endIndex,:))/1e6; %to rescale from int32 * 1e6
postEvapMonthly = double(postEvapMonthly(startIndex:endIndex,:))/1e6; %to rescale from int32 * 1e6
thisData = thisData + postDischargeMonthly - postEvapMonthly; %to remove evaporated water from groundwater discharge
clear postDischargeMonthly postEvapMonthly

%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,mrwFind),'water years','sum');
yearlyFrac = yearlyData ./ yearlyPrecip;
[months,monthlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,mrwFind),'months','sum','years','mean');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.allRun.years = years;
allDataMonthYear.allRun.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.allRun.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.allRun.months = months;
allDataMonthYear.allRun.monthlyAvg = mean(monthlyData,2);

%Calculate Annual Storage as Precip - ET - Run
allDataMonthYear.allStorage.years = years;
allDataMonthYear.allStorage.yearlyAvg = allDataMonthYear.precip.yearlyAvg - allDataMonthYear.allET.yearlyAvg - allDataMonthYear.allRun.yearlyAvg;
allDataMonthYear.allStorage.yearlyFrac = 1 - allDataMonthYear.allET.yearlyFrac - allDataMonthYear.allRun.yearlyFrac;
allDataMonthYear.allStorage.months = months;
allDataMonthYear.allStorage.monthlyAvg = allDataMonthYear.precip.monthlyAvg - allDataMonthYear.allET.monthlyAvg - allDataMonthYear.allRun.monthlyAvg;


%% Upland Areas
%Here, exclude all areas with 0 upland fraction
% uplandFind = find(mrwGrid' & (rescaleGrids.upland > 0));
uplandFind = find(rescaleGrids.upland > 0);

%Get upland Precip
[years,uplandYearlyPrecip] = time_series_aggregate(interval(startIndex:endIndex,1),precip(startIndex:endIndex,uplandFind),'water years','sum');%only keep upland columns
[months,monthlyMedian] = time_series_aggregate(interval(startIndex:endIndex,1),precip(startIndex:endIndex,uplandFind),'months','sum','years','nanmedian');
[months,monthlyMean] = time_series_aggregate(interval(startIndex:endIndex,1),precip(startIndex:endIndex,uplandFind),'months','sum','years','nanmean');

allDataMonthYear.precip.uplandYearlyAvg = mean(uplandYearlyPrecip,2);
allDataMonthYear.precip.uplandMonthlyMedian = nanmean(monthlyMedian,2);
allDataMonthYear.precip.uplandMonthlyMean = nanmean(monthlyMean,2);
allDataMonthYear.precip.months = months;

%Upland R
thisData = zerosGrid;
for m = 1:length(uplandRDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    thisData = thisData + tempData;
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'water years','sum');
yearlyFrac = yearlyData ./ uplandYearlyPrecip;
[months,monthlyMedian] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmedian');
[months,monthlyMean] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmean');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.uplandR.years = years;
allDataMonthYear.uplandR.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.uplandR.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.uplandR.months = months;
allDataMonthYear.uplandR.monthlyMedian = nanmean(monthlyMedian,2);
allDataMonthYear.uplandR.monthlyMean = nanmean(monthlyMean,2);

%Save for storage and recharge calculation
storageData.uplandR.all = thisData;

%Upland ET
thisData = zerosGrid;
for m = 1:length(uplandETDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],false);
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'water years','sum');
yearlyFrac = yearlyData ./ uplandYearlyPrecip;
[months,monthlyMedian] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmedian');
[months,monthlyMean] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmean');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.uplandET.years = years;
allDataMonthYear.uplandET.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.uplandET.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.uplandET.months = months;
allDataMonthYear.uplandET.monthlyMedian = nanmean(monthlyMedian,2);
allDataMonthYear.uplandET.monthlyMean = nanmean(monthlyMean,2);

%Save for storage calculation
storageData.uplandET.all = thisData;


%Upland Runoff
thisData = zerosGrid;
for m = 1:length(uplandRunDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRunDatasets{m},'/',dataType],false);
    %Load monthly data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',uplandRunDatasets{m},'/',dataType],true,...
        [0,0],[gridSize(1),gridSize(2)],[1,1]);
    thisData = thisData + tempData;
end
%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'water years','sum');
yearlyFrac = yearlyData ./ uplandYearlyPrecip;
[months,monthlyMedian] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmedian');
[months,monthlyMean] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmean');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.uplandRun.years = years;
allDataMonthYear.uplandRun.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.uplandRun.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.uplandRun.months = months;
allDataMonthYear.uplandRun.monthlyMedian = nanmean(monthlyMedian,2);
allDataMonthYear.uplandRun.monthlyMean = nanmean(monthlyMean,2);

%Save for storage calculation
storageData.uplandRun.all = thisData;

%Calculate Annual Upland Storage as Precip - ET - DP - Run
thisData = precip - storageData.uplandR.all - storageData.uplandET.all - storageData.uplandRun.all;

%Summarize the data, only for columns in the MRW grid
[years,yearlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'water years','sum');
yearlyFrac = yearlyData ./ uplandYearlyPrecip;
[months,monthlyMedian] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmedian');
[months,monthlyMean] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','sum','years','nanmean');

%Save yearly averages to the allDataMonthYear struct
allDataMonthYear.uplandStorage.years = years;
allDataMonthYear.uplandStorage.yearlyAvg = mean(yearlyData,2);
allDataMonthYear.uplandStorage.yearlyFrac = mean(yearlyFrac,2);
allDataMonthYear.uplandStorage.months = months;
allDataMonthYear.uplandStorage.monthlyMedian = nanmean(monthlyMedian,2);
allDataMonthYear.uplandStorage.monthlyMean = nanmean(monthlyMean,2);

storageData.uplandStorage.all = thisData;

%Calculate recharge curve for monthly values
thisData = storageData.uplandR.all;
[months,monthlyMean] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,uplandFind),'months','mean','years','nanmean');

monthlyMean(13,:) = monthlyMean(1,:);
months(13) = 13;

%Resample this weekly
weekInMonth = (1:52)*12.75/52;
[weeklyMean] = interp1(months,monthlyMean,weekInMonth,'linear','extrap');

%Bump based on delay
weekIndex = repmat((1:52)',1,size(weeklyMean,2)) - repmat(delayGrid(uplandFind)',52,1);
testRollover = weekIndex < 1;
weekIndex(testRollover) = weekIndex(testRollover) + 52;

%Add in columns to search from
weekIndex = weekIndex + 52 * repmat((0:size(weekIndex,2)-1),52,1);

%Create new delayed grid
weeklyMeanDelay = weeklyMean(weekIndex);
weeksDate = weekInMonth*365/13+datenum('1/1/2009');
[months,monthlyMeanDelay] = time_series_aggregate(weeksDate',weeklyMeanDelay,'months','mean');

%Finally, calculate average monthly recharge (approximate)
monthlyRechargeDelay = nanmean(monthlyMeanDelay,2);

%Calculate Annual Upland Storage as Precip - ET - DP - Run, includes unsat
%zone
mostData = precip - storageData.uplandET.all - storageData.uplandRun.all;
[months,monthlyMeanMost] = time_series_aggregate(interval(startIndex:endIndex,1),mostData(startIndex:endIndex,uplandFind),'months','sum','years','nanmean');
thisData = monthlyMeanMost - monthlyMeanDelay;
monthlyStorageDelay = nanmean(thisData,2);
%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'-struct','allDataMonthYear');

%%
%--------------------------------------------------------------------------
%Create the monthly plot - annual calendar
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,1120,420])

subplot(1,2,1)
hold on
%Totals
plot(allDataMonthYear.allET.months,allDataMonthYear.allET.monthlyAvg*100,'--','LineWidth',1,'Color',[1,0,0]);
plot(allDataMonthYear.allStorage.months,allDataMonthYear.allStorage.monthlyAvg*100,'--','LineWidth',1,'Color',[0,1,0]);
plot(allDataMonthYear.allRun.months,allDataMonthYear.allRun.monthlyAvg*100,'--','LineWidth',1,'Color',[0,0,1]);

line([allDataMonthYear.allET.months(1),allDataMonthYear.allET.months(end)],[0,0],'Color',[0,0,0]);

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[-5,10]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',18)
ylabel(gca,'Average Monthly Flux (cm/m^2)','FontSize',20);
xlabel(gca,'Calendar Month','FontSize',20);
legend({'Total ET','Total Storage','Total Runoff'})
set(gca,'Box','on')


subplot(1,2,2)
hold on
%Uplands
plot(allDataMonthYear.uplandET.months,allDataMonthYear.uplandET.monthlyMean*100,'-','LineWidth',1,'Color',[1,0,0]);
plot(allDataMonthYear.uplandR.months,allDataMonthYear.uplandR.monthlyMean*100,'-','LineWidth',1,'Color',[0,1,0]);
plot(allDataMonthYear.uplandRun.months,allDataMonthYear.uplandRun.monthlyMean*100,'-','LineWidth',1,'Color',[0,0,1]);
plot(allDataMonthYear.uplandStorage.months,allDataMonthYear.uplandStorage.monthlyMean*100,'-','LineWidth',1,'Color',[1,0,1]);
line([allDataMonthYear.allET.months(1),allDataMonthYear.allET.months(end)],[0,0],'Color',[0,0,0]);

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[-4,8]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',18)
xlabel(gca,'Calendar Month','FontSize',20);
legend({'Upland ET','Upland DP','Upland Runoff'})
set(gca,'Box','on')

%%
%--------------------------------------------------------------------------
%Create the annual plot 
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,1120,840])

%Actual Values
subplot(2,2,1)
hold on
plot(allDataMonthYear.allET.years,allDataMonthYear.allET.yearlyAvg*100,'--','LineWidth',1,'Color',[1,0,0]);
plot(allDataMonthYear.allStorage.years,allDataMonthYear.allStorage.yearlyAvg*100,'--','LineWidth',1,'Color',[0,1,0]);
plot(allDataMonthYear.allRun.years,allDataMonthYear.allRun.yearlyAvg*100,'--','LineWidth',1,'Color',[0,0,1]);
line([allDataMonthYear.allET.years(1),allDataMonthYear.allET.years(end)],[0,0],'Color',[0,0,0]);

%Set properties
set(gca,'XLim',datenum({'1/1/1981','1/1/2007'}));
set(gca,'YLim',[-25,75]);
set(gca,'XTick',datenum({'1/1/1982','1/1/1985','1/1/1988','1/1/1991','1/1/1994',...
    '1/1/1997','1/1/2000','1/1/2003','1/1/2006'}));
set(gca,'YTick',[-25,0,25,50,75]);
set(gca,'FontSize',18)
ylabel(gca,'Annual Flux (cm/m^2)','FontSize',20);
set(gca,'XTickLabel',{'','','','','','','','',''});
legend({'Total ET','Total Storage','Total Runoff'})
set(gca,'Box','on')

%Precip Fractions
subplot(2,2,3)
hold on
plot(allDataMonthYear.allET.years,allDataMonthYear.allET.yearlyFrac*100,'--','LineWidth',1,'Color',[1,0,0]);
plot(allDataMonthYear.allStorage.years,allDataMonthYear.allStorage.yearlyFrac*100,'--','LineWidth',1,'Color',[0,1,0]);
plot(allDataMonthYear.allRun.years,allDataMonthYear.allRun.yearlyFrac*100,'--','LineWidth',1,'Color',[0,0,1]);
line([allDataMonthYear.allET.years(1),allDataMonthYear.allET.years(end)],[0,0],'Color',[0,0,0]);

%Set properties
set(gca,'XLim',datenum({'1/1/1981','1/1/2007'}));
set(gca,'YLim',[-30,100]);
set(gca,'XTick',datenum({'1/1/1982','1/1/1985','1/1/1988','1/1/1991','1/1/1994',...
    '1/1/1997','1/1/2000','1/1/2003','1/1/2006'}));
set(gca,'YTick',[-25,0,25,50,75,100]);
datetick('x','yy','keepticks','keeplimits');
set(gca,'FontSize',18)
ylabel(gca,'Percent of Precipitation','FontSize',20);
xlabel(gca,'Water Year','FontSize',20);
set(gca,'Box','on')

%Actual Values
subplot(2,2,2)
hold on
plot(allDataMonthYear.uplandET.years,allDataMonthYear.uplandET.yearlyAvg*100,'-','LineWidth',1,'Color',[1,0,0]);
plot(allDataMonthYear.uplandR.years,allDataMonthYear.uplandR.yearlyAvg*100,'-','LineWidth',1,'Color',[0,1,0]);
plot(allDataMonthYear.uplandRun.years,allDataMonthYear.uplandRun.yearlyAvg*100,'-','LineWidth',1,'Color',[0,0,1]);
plot(allDataMonthYear.uplandStorage.years,allDataMonthYear.uplandStorage.yearlyAvg*100,'-','LineWidth',1,'Color',[1,0,1]);
line([allDataMonthYear.uplandET.years(1),allDataMonthYear.uplandET.years(end)],[0,0],'Color',[0,0,0]);

%Set properties
set(gca,'XLim',datenum({'1/1/1981','1/1/2007'}));
set(gca,'YLim',[-15,65]);
set(gca,'XTick',datenum({'1/1/1982','1/1/1985','1/1/1988','1/1/1991','1/1/1994',...
    '1/1/1997','1/1/2000','1/1/2003','1/1/2006'}));
set(gca,'YTick',(-15:15:60));
set(gca,'FontSize',18)
set(gca,'XTickLabel',{'','','','','','','','',''});
legend({'Upland ET','Upland DP','Upland Runoff'})
set(gca,'Box','on')

%Precip Fractions
subplot(2,2,4)
hold on
plot(allDataMonthYear.uplandET.years,allDataMonthYear.uplandET.yearlyFrac*100,'-','LineWidth',1,'Color',[1,0,0]);
plot(allDataMonthYear.uplandR.years,allDataMonthYear.uplandR.yearlyFrac*100,'-','LineWidth',1,'Color',[0,1,0]);
plot(allDataMonthYear.uplandRun.years,allDataMonthYear.uplandRun.yearlyFrac*100,'-','LineWidth',1,'Color',[0,0,1]);
plot(allDataMonthYear.uplandStorage.years,allDataMonthYear.uplandStorage.yearlyFrac*100,'-','LineWidth',1,'Color',[1,0,1]);
line([allDataMonthYear.uplandET.years(1),allDataMonthYear.uplandET.years(end)],[0,0],'Color',[0,0,0]);

%Set properties
set(gca,'XLim',datenum({'1/1/1981','1/1/2007'}));
set(gca,'YLim',[-15,75]);
set(gca,'XTick',datenum({'1/1/1982','1/1/1985','1/1/1988','1/1/1991','1/1/1994',...
    '1/1/1997','1/1/2000','1/1/2003','1/1/2006'}));
set(gca,'YTick',(-15:15:75));
datetick('x','yy','keepticks','keeplimits');
set(gca,'FontSize',18)
xlabel(gca,'Water Year','FontSize',20);
set(gca,'Box','on')

%% Composite Monthly/Annual plot for WRR Rapid Communications Paper
figure
set(gcf,'Position',[1 1 800 350])

%Set global options
lineWidth = 1;

fontSizeTitle = 15;
fontSizeAxes = 14;
fontSizeLabels = 15;

colors = {[0,0,0]/255,[237,34,36]/255,[13,177,75]/255,[0,173,239]/255,[58,83,164]/255}; %Precip, ET, DP, Runoff, Storage


%Annual Values
subplot(1,2,1)
hold on
plot(allDataMonthYear.precip.years,allDataMonthYear.precip.yearlyAvg*100,'LineWidth',lineWidth,'Color',colors{1});
plot(allDataMonthYear.uplandET.years,allDataMonthYear.uplandET.yearlyAvg*100,'LineWidth',lineWidth,'Color',colors{2});
plot(allDataMonthYear.uplandR.years,allDataMonthYear.uplandR.yearlyAvg*100,'LineWidth',lineWidth,'Color',colors{3});
plot(allDataMonthYear.uplandRun.years,allDataMonthYear.uplandRun.yearlyAvg*100,'LineWidth',lineWidth,'Color',colors{4});
plot(allDataMonthYear.uplandStorage.years,allDataMonthYear.uplandStorage.yearlyAvg*100,'LineWidth',lineWidth,'Color',colors{5});
%line([allDataMonthYear.precip.years(1),allDataMonthYear.precip.years(end)],[0,0],'Color',[0,0,0]);

%Set properties
% set(gca,'XLim',datenum({'1/1/1997','1/1/2007'}));\
set(gca,'XLim',datenum({'1/1/1981','1/1/2007'}));
set(gca,'YLim',[-15,115]);
% set(gca,'XTick',datenum({'1/1/1997','1/1/1999','1/1/2001','1/1/2003','1/1/2005','1/1/2007'}));
set(gca,'XTick',datenum({'1/1/1982','1/1/1985','1/1/1988','1/1/1991','1/1/1994','1/1/1997','1/1/2000','1/1/2003','1/1/2006'}));
set(gca,'YTick',(-15:15:115));
datetick('x','yy','keepticks','keeplimits');
set(gca,'FontSize',fontSizeAxes)
xlabel(gca,'Water Year','FontSize',fontSizeLabels);
ylabel(gca,'Annual Fluxes (cm)','FontSize',fontSizeLabels);
set(gca,'Box','on')


%Monthly Values
subplot(1,2,2)
hold on
plot(allDataMonthYear.precip.months,allDataMonthYear.precip.uplandMonthlyMean*100,'LineWidth',lineWidth,'Color',colors{1});
% plot(allDataMonthYear.precip.months,allDataMonthYear.precip.uplandMonthlyMedian*100,':','LineWidth',lineWidth,'Color',colors{1});
plot(allDataMonthYear.uplandET.months,allDataMonthYear.uplandET.monthlyMean*100,'LineWidth',lineWidth,'Color',colors{2});
% plot(allDataMonthYear.uplandET.months,allDataMonthYear.uplandET.monthlyMedian*100,':','LineWidth',lineWidth,'Color',colors{2});
plot(allDataMonthYear.uplandR.months,allDataMonthYear.uplandR.monthlyMean*100,':','LineWidth',lineWidth,'Color',colors{3});
plot(allDataMonthYear.uplandR.months,monthlyRechargeDelay*100,'-','LineWidth',lineWidth,'Color',colors{3});
% plot(allDataMonthYear.uplandR.months,allDataMonthYear.uplandR.monthlyMedian*100,':','LineWidth',lineWidth,'Color',colors{3});
plot(allDataMonthYear.uplandRun.months,allDataMonthYear.uplandRun.monthlyMean*100,'LineWidth',lineWidth,'Color',colors{4});
% plot(allDataMonthYear.uplandRun.months,allDataMonthYear.uplandRun.monthlyMedian*100,':','LineWidth',lineWidth,'Color',colors{4});
plot(allDataMonthYear.uplandStorage.months,allDataMonthYear.uplandStorage.monthlyMean*100,':','LineWidth',lineWidth,'Color',colors{5});
plot(allDataMonthYear.uplandStorage.months,monthlyStorageDelay*100,'-','LineWidth',lineWidth,'Color',colors{5});
% plot(allDataMonthYear.uplandStorage.months,allDataMonthYear.uplandStorage.monthlyMedian*100,':','LineWidth',lineWidth,'Color',colors{5});
% line([allDataMonthYear.uplandET.months(1),allDataMonthYear.uplandET.months(end)],[0,0],'LineStyle',':','Color',[0,0,0]);

%Set properties
set(gca,'XLim',[1,12]);
% set(gca,'YLim',[-7,11]);
set(gca,'YLim',[-5,10]);
set(gca,'XTick',(1:12));
set(gca,'YTick',[-3,0,3,6,9]);
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',fontSizeAxes)
xlabel(gca,'Calendar Month','FontSize',fontSizeLabels);
ylabel(gca,'Monthly Fluxes (cm)','FontSize',fontSizeLabels);
set(gca,'Box','on')