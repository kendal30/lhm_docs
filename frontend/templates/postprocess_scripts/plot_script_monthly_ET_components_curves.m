%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Outputs and model values
%--------------------------------------------------------------------------
outputFilename = 'monthly_ET_components_data.mat';
outputFile = [outputDir,filesep,outputFilename];


type = 'grid';
namespace = 'expanded';
allETDatasets = {'evap_canopy','evap_depression','evap_ice_pack','evap_soil',...
    'evap_snow_upland','evap_wetland','transp_upland','transp_wetland'};
rescaleDomain = {'all','upland','wetland','upland',...
    'upland','wetland','upland','wetland'};
dataType = 'plus';

dataNames = {'Canopy','Depression','Ice_Sub','Soil','Snow_Sub','Wetland_Evap','Upland_Transp','Wetland_Transp'};


%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table, and the MRW grid
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputOUTF,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputOUTF,['/',type,'/',namespace,'/','interval']);
startIndex = find(dateRange(1) >= interval(:,1) & dateRange(1) <= interval(:,2));
endIndex = find(dateRange(2) >= interval(:,1) & dateRange(2) <= interval(:,2));

startDatevec = datevec(interval(startIndex,1));
endDatevec = datevec(interval(endIndex,1));
numYears = (endDatevec(1)-startDatevec(1)+1);

%Load the MRW grid, and trim to model domain
loadData = load(inputStatic,'mrw_grid');
[mrwGrid,trimStruct] = trim_domain(double(~mask),loadData.mrw_grid.data);
mrwGrid(isnan(mrwGrid))=0;
mrwGrid = logical(mrwGrid);
mrwFind = find(mrwGrid);

%--------------------------------------------------------------------------
%Load the rescale grids
%--------------------------------------------------------------------------
rescaleGrids = load(inputStatic,'water_fraction','stream_fraction');
rescaleGrids.stream_fraction.data = min(rescaleGrids.stream_fraction.data, 1-rescaleGrids.water_fraction.data);
wetFrac = trim_domain(~mask,rescaleGrids.water_fraction.data + rescaleGrids.stream_fraction.data)';
upFrac = 1 - wetFrac;
clear rescaleGrids
rescaleGrids = struct('upland',upFrac,'wetland',wetFrac,'all',upFrac+wetFrac);

%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputOUTF,'/geolocation/bot');
cellsize = h5dataread(inputOUTF,'/geolocation/cellsize');
left = h5dataread(inputOUTF,'/geolocation/left');
numCol = h5dataread(inputOUTF,'/geolocation/num_col');
numRow = h5dataread(inputOUTF,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%%
%--------------------------------------------------------------------------
%Loop through datasets, summing together
%--------------------------------------------------------------------------
%Get grid size
gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{1},'/',dataType],false);
zerosGrid = zeros(gridSize);

%Specify the trim index
trimInd = mrwFind;

%Create output
allDataComponents = struct();

%All ET
for m = 1:length(allETDatasets)
    %Initialize the dataset
    thisData = zerosGrid;
    
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],false);
    
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
    
    %Rescale to the domain of the dataset
    thisData = thisData.* repmat(rescaleGrids.(rescaleDomain{m}),gridSize(1),1);
    
    %Annual summaries of the data
    [years,yearlyAvg] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,trimInd),'water years','sum');
    
    %Summarize the data, only for columns in the trim index
    [months,monthlyData] = time_series_aggregate(interval(startIndex:endIndex,1),thisData(startIndex:endIndex,trimInd),'months','sum','years','mean');
    
    %Save to allDataComponents struct
    allDataComponents.(dataNames{m}).months = months;
    allDataComponents.(dataNames{m}).monthlyAvg = mean(monthlyData,2);
    allDataComponents.(dataNames{m}).years = years;
    allDataComponents.(dataNames{m}).yearlyAvg = mean(yearlyAvg,2);
    allDataComponents.(dataNames{m}).mean = sum(thisData)./numYears;
end

clear postEvapMonthly thisData
%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'-struct','allDataComponents');

%%
%--------------------------------------------------------------------------
%Create the monthly plot - annual calendar
%--------------------------------------------------------------------------
figure
% colormap('gray')
% colormap(flipud(colormap))
colormap('jet')

area(allDataComponents.Canopy.months,[allDataComponents.Wetland_Evap.monthlyAvg,...
    max(0,allDataComponents.Snow_Sub.monthlyAvg+allDataComponents.Ice_Sub.monthlyAvg),...
    allDataComponents.Soil.monthlyAvg+allDataComponents.Depression.monthlyAvg,...
    allDataComponents.Canopy.monthlyAvg,...
    allDataComponents.Wetland_Transp.monthlyAvg,allDataComponents.Upland_Transp.monthlyAvg]*100)

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[-1,11]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',18)
ylabel(gca,'Monthly Flux (cm/m^2)','FontSize',20);
xlabel(gca,'Calendar Month','FontSize',20);
legend({'Open Water Evap','Sublimation','Soil Evap','Canopy Evap','Wetland Transp','Upland Transp'})
set(gca,'Box','on')

