%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
inputDir = 'D:\Users\Anthony\Modeling\ILHM\Output\test-v1\fullGrid13';
inputFilename = 'OUTF_test-v1_fullGrid13_20090409.h5';
inputFile = [inputDir,filesep,inputFilename];

type = 'zone';
%Solar Radiation
solarNamespace = 'mrw_Energy';
solarDatasets = {'rad_short_beam_in','rad_short_diff_in'};
solarType = 'mean';
%Air Temperature
tempNamespace = 'mrw_Energy';
tempDatasets = {'temp_air'};
tempType = 'mean';
%Precipitation
precipNamespace = 'mrw_Hydro';
precipDatasets = {'rain','snow'};
precipType = 'mean';
%LAI
laiNamespace = 'lu_Uplands';
laiDatasets = {'lai'};
laiType = 'mean';
laiZones = [1,2,3,4];
laiNames = {'urban','ag','open','forest'};
laiColors = {'k','k','k','k'};
laiLineType = {':','-','--','-.'};

outputDir = 'D:\Users\Anthony\Graphics\4_08_09_Thesis_Recalc';
outputFilename = 'climate_means_figure_data.mat';
outputFile = [outputDir,filesep,outputFilename];

%%
%--------------------------------------------------------------------------
%Load the interval table
%--------------------------------------------------------------------------
%Create output
allData = struct();

%Load the interval tables
allData.sun.interval = h5dataread(inputFile,['/',type,'/',solarNamespace,'/','interval']);
allData.temp.interval = h5dataread(inputFile,['/',type,'/',tempNamespace,'/','interval']);
allData.precip.interval = h5dataread(inputFile,['/',type,'/',precipNamespace,'/','interval']);
allData.lai.interval = h5dataread(inputFile,['/',type,'/',laiNamespace,'/','interval']);

%%
%--------------------------------------------------------------------------
%Loop through datasets, summing together
%--------------------------------------------------------------------------
%Fetch the solar radiation data
for m = 1:length(solarDatasets)
    loopData = h5dataread(inputFile,['/',type,'/',solarNamespace,'/datasets/',solarDatasets{m},'/',solarType],true);
    if m == 1
        thisData = loopData;
    else
        thisData = thisData + loopData;
    end
end
allData.sun.(solarType) = thisData;

%Fetch the precipitation data
for m = 1:length(precipDatasets)
    loopData = h5dataread(inputFile,['/',type,'/',precipNamespace,'/datasets/',precipDatasets{m},'/',precipType],true);
    if m == 1
        thisData = loopData;
    else
        thisData = thisData + loopData;
    end
end
allData.precip.(precipType) = thisData;

%Fetch the temperature data
for m = 1:length(tempDatasets)
    loopData = h5dataread(inputFile,['/',type,'/',tempNamespace,'/datasets/',tempDatasets{m},'/',tempType],true);
    if m == 1
        thisData = loopData;
    else
        thisData = thisData + loopData;
    end
end
allData.temp.(tempType) = thisData;

%Fetch the LAI data
for m = 1:length(laiDatasets)
    loopData = h5dataread(inputFile,['/',type,'/',laiNamespace,'/datasets/',laiDatasets{m},'/',laiType],true);
    if m == 1
        thisData = loopData;
    else
        thisData = thisData + loopData;
    end
end
allData.lai.(laiType) = thisData;

%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'-struct','allData');
%%
%--------------------------------------------------------------------------
%Create the monthly plot
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[1 1 400 800])

%Temperature
[months,monthlyMean] = time_series_aggregate(allData.temp.interval(:,1),allData.temp.(tempType),'months','mean','years','mean');
[months,monthlyLow] = time_series_aggregate(allData.temp.interval(:,1),allData.temp.(tempType),'months','mean','years','@(x)percentile(x,5)');
[months,monthlyHigh] = time_series_aggregate(allData.temp.interval(:,1),allData.temp.(tempType),'months','mean','years','@(x)percentile(x,95)');


subplot(4,1,1)
hold on
patch([months;flipud(months)],[monthlyLow;flipud(monthlyHigh)],[0.5,0.5,0.5],'LineStyle','none')
plot(months,monthlyMean,'k-','LineWidth',1)
box on
set(gca,'XLim',[1,12]);
set(gca,'YLim',[-10,25]);
set(gca,'XTickLabel','');
set(gca,'XTick',(1:12));
set(gca,'FontSize',14);
ylabel('Air Temp. (^\circ C)','FontSize',14);

%Precipitation
[months,monthlyMean] = time_series_aggregate(allData.precip.interval(:,1),allData.precip.(precipType),'months','sum','years','mean');
[months,monthlyLow] = time_series_aggregate(allData.precip.interval(:,1),allData.precip.(precipType),'months','sum','years','@(x)percentile(x,5)');
[months,monthlyHigh] = time_series_aggregate(allData.precip.interval(:,1),allData.precip.(precipType),'months','sum','years','@(x)percentile(x,95)');

subplot(4,1,2)
hold on
patch([months;flipud(months)],[monthlyLow;flipud(monthlyHigh)]*100,[0.5,0.5,0.5],'LineStyle','none')
plot(months,monthlyMean*100,'k-','LineWidth',1)
box on
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,20]);
set(gca,'XTickLabel','');
set(gca,'XTick',(1:12));
set(gca,'FontSize',14);
ylabel('Precip. (cm)','FontSize',14);

%Solar Radiation
[months,monthlyMean] = time_series_aggregate(allData.sun.interval(:,1),allData.sun.(solarType),'months','mean','years','mean');
[months,monthlyLow] = time_series_aggregate(allData.sun.interval(:,1),allData.sun.(solarType),'months','mean','years','@(x)percentile(x,5)');
[months,monthlyHigh] = time_series_aggregate(allData.sun.interval(:,1),allData.sun.(solarType),'months','mean','years','@(x)percentile(x,95)');

subplot(4,1,3)
hold on
patch([months;flipud(months)],[monthlyLow;flipud(monthlyHigh)],[0.5,0.5,0.5],'LineStyle','none')
plot(months,monthlyMean,'k-','LineWidth',1)
box on
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,300]);
set(gca,'XTickLabel','');
set(gca,'XTick',(1:12));
set(gca,'FontSize',14);
ylabel('Solar Rad. (W/m^2)','FontSize',14);

%Leaf Area Index
subplot(4,1,4)
hold on
for m = 1:length(laiZones)
    [months,monthlyMean] = time_series_aggregate(allData.lai.interval(:,1),allData.lai.(laiType)(:,laiZones(m)),'months','mean','years','mean');
    plot(months,monthlyMean,[laiColors{m},laiLineType{m}],'LineWidth',1.0);
end

box on
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,5]);
set(gca,'YTick',[0,2.5,5]);
set(gca,'XTickLabel','');
set(gca,'XTick',(1:12));
set(gca,'FontSize',14);
ylabel('LAI (-)','FontSize',14);
legend(laiNames)
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
xlabel('Calendar Months')
