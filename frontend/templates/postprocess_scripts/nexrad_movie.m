%This script makes a movie of NEXRAD data

%Specify these inputs
clipGridLoc = '\\hydroflux\Users\Anthony\Data\GIS_Data\Expanded_Model_Boundary\Precip\NEXRAD\buffbndgrd';
workingDir = '\\hydroflux\users\anthony\code\workspace';
inputFile = 'nexrad.h5';
outMovie = 'nexrad_movie.avi';

startDate = '05/08/2004 00:00:00';
endDate = '05/09/2004 24:00:00';

%Read in the header and parse into a more useful format
[clipGrid,nexradHeader] = import_grid(clipGridLoc);
mask = clipGrid == ~255;

%Read in the index and determine which rows to pull in
index = h5dataread([workingDir,filesep,inputFile],'/precip_nexrad/index');
%trim to only dates where data is not NaN
index = index(~isnan(index(:,2)),:);
%Determine start and end indeces
startIndex = find(ismember_dates(index(:,1),startDate,4,true));
endIndex = find(ismember_dates(index(:,1),endDate,4,true));
trimIndex = index(startIndex:endIndex,:);
readIndex = trimIndex(trimIndex(:,2)~=-1,2);
times = trimIndex(trimIndex(:,2)~=-1,1);

data = h5dataread([workingDir,filesep,inputFile],'/precip_nexrad/data',true,...
    [readIndex(1)-1,0],[length(readIndex),nexradHeader.rows*nexradHeader.cols],[1,1]);

%Get the mean of the dataset
timeseries = zeros(size(data,1),1);
for m = 1:size(data,1)
   timeseries(m) = mean(data(m,mask)); 
end
timeseries = timeseries*1000; %convert to mm

%Set up the colormap
maxVal = max(data(:));
scaling=[0,maxVal*1000];
f1 = figure;
set(gcf,'Position',[200,200,500,600])
test_colormap = get(gcf,'Colormap');
%test_colormap=usercolormap([135,38,38],[242,159,34],[255,255,191],[78,81,181],[39,32,122])/255;
background_color=[240/255,235/255,219/255];
test_colormap(1,:)=background_color;
set(gcf,'Colormap',test_colormap,'Color',background_color);

%Generate a movie of the data
for m = 1:size(data,1)
    loopFrame = reshape(data(m,:),nexradHeader.rows,nexradHeader.cols);
%     test = (loopFrame<0.001) & mask;
%     loopFrame(test) = 0.001;
        
    g1=subplot(2,1,1);
    imagesc(loopFrame*1000,scaling);
    g2=colorbar;
    g3=subplot(2,1,2);
    plot(times,timeseries);
    hold on
    plot(times(m),timeseries(m),'o','MarkerEdgeColor','k','MarkerFaceColor','g');
    hold off
    
    
    set(g1,'Position',[0.03,0.23,0.8,0.8]);
    set(g1,'PlotBoxAspectRatio',[1 1 1],'DataAspectRatioMode','auto'); %square
    set(g1,'Visible','off');
    xlabel(g2,'Precip. (mm/hr)');
    set(g3,'Position',[0.1,0.05,0.85,0.2]);
    ylabel(g3,'Avg. Precip. (mm/hr)');
    set(g3,'XLim',[times(1),times(end)]);
    set(g3,'YLim',[min(timeseries),max(timeseries)]*1.1);
    datetick(g3,'x','yyyy/mm/dd','keeplimits');
    nexradMovie(m)=getframe(gcf);
end

movie2avi(nexradMovie,[workingDir,filesep,outMovie],'FPS',4,'COMPRESSION','none');