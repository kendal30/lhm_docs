dirDischarge = 'F:\Users\Anthony\CVS_working\ilhm\Postprocess';
dirILHM = '\\Hydroserver\anthony\modeling\Model_runs\ILHM\Expanded_Model\modern_baseline_2_rerun';
dirWorking = pwd;

%Reference:
%Gauges: evart, clam, oak grove
%Watersheds: 2, 1, 4
%Hec Basins: [26:35,43],[26],[18:21]
%Locations: Evart, Vogel Center, Oak Grove

%% Load the modeled and gauged data
basinName = 'Evart';
scenarioName = 'modern';
basinList = [26:35,43]; %<--- Change this line
runoffWatershed = 2; %<--- Change this line
fileNetBase = [scenarioName,'_groundwater.txt']; %<--- Change this line
% fileRawBase = 'modern_raw_groundwater.txt'; %<--- Change this line
%Gauge
cd(dirDischarge);
load evart_discharge.mat evart_date evart_daily %<--- Change this line
dateGauge = evart_date; %<--- Change this line
dailyGauge = evart_daily; %<--- Change this line
% [dateGauge,dailyGauge] = time_series_aggregate(date_stitched,flow_stitched,'days','mean');
dailyGauge = unit_conversions(dailyGauge,'cfs','cms');
%Model Surface
cd(dirILHM);
load ILHM_dump_2006.mat state params
hourlyRunoff = state.watershed_runoff{runoffWatershed};
dateRunoff = datenum(params.dates(1)) + [0:length(hourlyRunoff)-1]/24;
[dateDailyILHM,dailyRunoff] = time_series_aggregate(dateRunoff',hourlyRunoff,'days','mean');
%Model Groundwater
groundwaterILHM = importdata(fileNetBase);
dailyBaseILHM = sum(groundwaterILHM(:,basinList),2);
numTimesteps = length(dailyBaseILHM);
%Unevaporated Groundwater
% groundwaterMODFLOW = importdata(fileRawBase);
% dailyMODFLOW = sum(groundwaterMODFLOW(:,basinList),2);
%Combine surface and groundwater components
dailyModelDischarge = dailyRunoff(1:numTimesteps) + dailyBaseILHM;
dateDailyILHM = dateDailyILHM(1:numTimesteps);
cd(dirWorking);
%Generate an array with the ILHM indeces of the active HEC basin
basinIndeces = [];
for m = 1:length(basinList);
    basinIndeces = [basinIndeces, params.hec_basins{basinList(m)-2}]; %#ok<AGROW>
end
%% Plot discharge gauge comparisons
figure
plot(dateDailyILHM,dailyBaseILHM,'k-',dateDailyILHM,dailyModelDischarge,'b-',dateGauge,dailyGauge,'r-');%,dateDailyILHM,dailyMODFLOW,'k--');
axis([datenum('1/1/2000'),datenum('12/31/2004'),0,200])
set(gca,'XTick',datenum({'1/1/2000','5/1/2000','9/1/2000','1/1/2001','5/1/2001','9/1/2001',...
    '1/1/2002','5/1/2002','9/1/2002','1/1/2003','5/1/2003','9/1/2003','1/1/2004','5/1/2004','9/1/2004','1/1/2005'}));
datetick('x','mmm','keepticks','keeplimits')
ylabel('Stream Discharge (m^3/s)')
title(['Modeled and Gauged Discharge at ',basinName,', MI'])
legend('Baseflow','Total Flow','Gauged Flow')%,'Groundwater Discharge')
%% Plot annual errors in net fluxes, by water year
%Calculate the error in net flux by water years
[waterYearsGauge,annualFluxGauge] = time_series_aggregate(dateGauge,dailyGauge,'water years','sum');
[waterYearsModel,annualFluxModel] = time_series_aggregate(dateDailyILHM,dailyModelDischarge,'water years','sum');
periodCompare = find(ismember(waterYearsGauge,waterYearsModel));
error = (annualFluxModel - annualFluxGauge(periodCompare)) ./ annualFluxGauge(periodCompare);
figure
plot(waterYearsModel(2:end-1),error(2:end-1)*100,'b','LineWidth',4); %don't plot the first and last points, because they are incomplete years
hold on
plot(waterYearsModel(2:end-1),error(2:end-1)*100,'c','LineWidth',2);
set(gca,'XLim',[1981,2006]);
set(gca,'XTick',(1982:2:2006));
ylabel('Error in Net Annual Flux, Sim - Obs (%)')
xlabel('Water Year')
title(['Net Annual Flux Error at ',basinName,',MI']);

%% Plot annual Q/P ratio as a function of P, by calendar year, for a basin
%Calculate the annual precipitation in the basin
cd(dirILHM)
h = waitbar(0,'Calculating Annual Basin Precip');
for m = 1:27
    waitbar(m/27,h);
    evalString = ['load ILHM_dump_',num2str(1980+m-1),'.mat visualization'];
    eval(evalString);
    precipGrid = visualization.sums.precip;
    totalPrecip = sum(sum(precipGrid(basinIndeces)));
    if m == 1
        basinPrecip(m) = totalPrecip; %#ok<AGROW>
    else
        basinPrecip(m) = totalPrecip - lastPrecip; %#ok<AGROW>
    end
    lastPrecip = totalPrecip;
end
close(h);
basinPrecip = basinPrecip';
basinPrecip = basinPrecip .* params.cell_resolution.^2;
cd(dirWorking);
%Calculate the annual model flux
[yearsModel,annualFluxModel] = time_series_aggregate(dateDailyILHM,dailyModelDischarge,'years','sum');
annualFluxModel = annualFluxModel * 86400;
%Calcualate the annual Q/P ratio
ratioQP = annualFluxModel ./ basinPrecip;
%Calculate basin-average precip
basinAvgPrecip = basinPrecip/params.cell_resolution.^2/numel(basinIndeces);

%Plot Q/P ratio as a function of P
figure
plot(basinAvgPrecip,ratioQP,'b.');
xlabel('Basin Average Precipitation (m)');
ylabel('Q/P ratio');
title(['Scenario: ',scenarioName,'  Basin: ',basinName]);
%Fit a linear trend to the data and plot the line
p = polyfit(basinAvgPrecip,ratioQP,1);
hold on
sortX = sort(basinAvgPrecip);
plot(sortX,p(1)*sortX+p(2),'r-');
text(100,100,['y = ',num2str(p(1)),'*x + ',num2str(p(2))]);

%% Plot annual Q/P ratio as a function of P, by calendar year, for entire watershed
%Calculate the annual precipitation in the basin
cd(dirILHM)
load ILHM_dump_2006.mat visualization
cd(dirWorking)
dateHourlyILHM = params.dates(1)+(1:1:24*9862)/24-1/24;
dateHourlyILHM = dateHourlyILHM';
precipHourlyILHM = visualization.means.precip*params.cell_resolution^2*sum(params.mrw_grid);
[yearsModel,annualPrecipModel] = time_series_aggregate(dateHourlyILHM,precipHourlyILHM,'water years','sum');
runoffHourlyILHM = visualization.means.precip_excess*params.cell_resolution^2*sum(params.mrw_grid);
[yearsModel,annualFluxRunoff] = time_series_aggregate(dateHourlyILHM,runoffHourlyILHM,'water years','sum');
baseflowDailyILHM = sum(groundwaterILHM(:,3:end),2) * 86400;
[yearsModel,annualFluxBaseflow] = time_series_aggregate(dateDailyILHM,baseflowDailyILHM,'water years','sum');
annualFluxModel = annualFluxRunoff + annualFluxBaseflow;
%Calculate Q/P ratio
ratioQP = annualFluxModel ./ annualPrecipModel;
%Plot Q/P ratio as a function of P
figure
plot(annualPrecipModel(7:end-1)/params.cell_resolution^2/sum(params.mrw_grid),ratioQP(7:end-1),'b.');
xlabel('Basin Average Precipitation (m)');
ylabel('Q/P ratio');
title(['Scenario: ',scenarioName,'  Basin: ',basinName]);
%Fit a linear trend to the data and plot the line
p = polyfit(basinAvgPrecip,ratioQP,1);
hold on
sortX = sort(basinAvgPrecip);
plot(sortX,p(1)*sortX+p(2),'r-');
text(100,100,['y = ',num2str(p(1)),'*x + ',num2str(p(2))]);