%This script creates average soil moisture maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
inputDir = 'D:\Users\Anthony\Modeling\ILHM\Output\test-v1\fullGrid4';
inputFilename = 'OUTF_test-v1_fullGrid4_20090105.h5';
inputFile = [inputDir,filesep,inputFilename];

type = 'grid';
namespace = 'expanded';
sumDatasets = {'water_soil'};
dataType = 'max';
sumMonth = 4; %this only works if output is monthly

soilThickness = 2.5; %m, only works for spatially uniform thickness, modify to read in thickness array if necessary

outputDir = 'D:\Users\Anthony\Graphics\2_10_08_Grids';
outputFilename = 'average_maximum_April_soil_moisture.txt';
outputFile = [outputDir,filesep,outputFilename];

%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputFile,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputFile,['/',type,'/',namespace,'/','interval']);

%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputFile,'/geolocation/bot');
cellsize = h5dataread(inputFile,'/geolocation/cellsize');
left = h5dataread(inputFile,'/geolocation/left');
numCol = h5dataread(inputFile,'/geolocation/num_col');
numRow = h5dataread(inputFile,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%%
%--------------------------------------------------------------------------
%Loop through datasets, summing together
%--------------------------------------------------------------------------
%Get grid size
gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',sumDatasets{1},'/',dataType],false);

%Create output
outGrid = zeros(1,gridSize(2));
numYears = gridSize(1) / 12; %<---this will need to be changed if inputs aren't monthly

for m = 1:length(sumDatasets)
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],false);
    
    for n = 1:gridSize(3) %Loop through the third dimension
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],true,...
            [sumMonth,0,n-1],[numYears,gridSize(2),1],[12,1,1]);
        outGrid = outGrid + mean(tempData,1);
    end
end

%%
%--------------------------------------------------------------------------
%Output to ASCII file
%--------------------------------------------------------------------------
asciiwrite(outGrid(lookup).*(~mask)/soilThickness,outputFile,header);