%%
%--------------------------------------------------------------------------
%Load the sim and observed data
%--------------------------------------------------------------------------
load(inputOUTS,'gauge_flow','head','flow');

%%
%--------------------------------------------------------------------------
%Load grids for catchment areas
%--------------------------------------------------------------------------
%Determine a list of gage names, get gage indices, set gageFieldNames
gages = unique(gauge_flow.loc_id);
[gageFieldNames,gageInd] = deal(cell(size(gages)));
for m = 1:length(gages)
    gageInd{m} = strcmpi(gauge_flow.loc_id,gages{m});
    gageFieldNames{m} = [gauge_flow.set_name{m},'_',gages{m}];
end

%Load the grids
loadGrids = load_grid_params(inputObs,'grids_obs');

%Save only the data portion of these
for m = 1:length(gages)
    gageInd = find(strcmpi(gauge_flow.loc_id,gages{m}),1,'first');
    [~,gridName,~] = fileparts(gauge_flow.grid{gageInd});
    obsGrids.(gageFieldNames{m}) = loadGrids.grids_obs.(gridName).data;
end
clear loadGrids

%Get model headers
[surfIbound,surfaceHeader] = import_grid(surfaceGrid,'header');
modflowHeader = import_grid(modflowGrid,'header');

%Get domain mask
surfaceMask = surfIbound==1;

%Resample these to ILHM resolution and trim to model domain
for m = 1:length(gageFieldNames)
    [obsGrids.(gageFieldNames{m}),mask] = cell_statistics(surfaceHeader,modflowHeader,obsGrids.(gageFieldNames{m}),'majority','majority');
    obsGrids.(gageFieldNames{m}) = trim_domain(surfaceMask,obsGrids.(gageFieldNames{m}));
end
%%
%--------------------------------------------------------------------------
%Load the precipitation data
%--------------------------------------------------------------------------
name = 'precip';
% namespace = 'EBCW';
namespace = 'model';
sumDatasets = {'rain','snow'};
type = 'grid';
dataType = 'plus';

%Load the interval table
interval = h5dataread(inputOUTF,['/',type,'/',namespace,'/','interval']);
startIndex = find(dateRange(1) >= interval(:,1) & dateRange(1) <= interval(:,2));
endIndex = find(dateRange(2) >= interval(:,1) & dateRange(2) <= interval(:,2));
numGrids = endIndex - startIndex + 1;

%Get grid size
gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{1},'/',dataType],false);

%Loop through datasets
thisGrid = zeros(numGrids,gridSize(2));
for m = 1:length(sumDatasets)
    %Get grid size
    gridSize = h5varget(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],false);
    %Load the data
    tempData = h5dataread(inputOUTF,['/',type,'/',namespace,'/datasets/',sumDatasets{m},'/',dataType],true,...
            [startIndex - 1,0],[numGrids,gridSize(2)],[1,1]);
    thisGrid = thisGrid + tempData;
end
%Calculate yearly mean precipitation
%Modify intervals slightly to account for start and end periods
modInterval = interval(startIndex:endIndex,1);
modInterval(1) = dateRange(1);
modInterval(end) = dateRange(2);
[years,yearlyPrecip] = time_series_aggregate(modInterval,thisGrid,'water years','sum');
clear tempData thisGrid


%Get the average catchment precipitation for each gage while that gage was
%recording data
meanPrecip = zeros(length(gageFieldNames),1);
yearlyPrecipGage = cell(length(gageFieldNames),1);
for m = 1:length(gageFieldNames)
    startDate = max(min(gauge_flow.time_start(gageInd{m})),dateRange(1));
    endDate = min(max(gauge_flow.time_start(gageInd{m})),dateRange(2));

    %Determine which years to sum
    thisGageYears = ismember_dates(years,(startDate:1:endDate)',1,false);

    %Logical array from obsGrids
    thisGageInd = logical(obsGrids.(gageFieldNames{m})>0);

    %Extract precip for this gage, for those years
    meanPrecip(m) = mean(mean(yearlyPrecip(find(thisGageYears),find(thisGageInd)))); %#ok<FNDSB>
    yearlyPrecipMean = nanmean(yearlyPrecip(:,thisGageInd),2);
    yearlyPrecipGage{m} = yearlyPrecipMean(thisGageYears);
end

%%
%--------------------------------------------------------------------------
%Table of total flux error and efficiency
%--------------------------------------------------------------------------
selGages = {'Bear_Creek_near_Muskegon','Clam_River_at_Vogel_Center','Liitle_Muskegon_River_near_Oak_Grove',...
    'Muskegon_River_at_Evart','Muskegon_River_at_Newaygo','Muskegon_River_near_Croton'};
gageAreas = unit_conversions([43,629,894,3711,6086,5991],'km2','m2');
gages = fieldnames(gauge_flow);
% selGages = {'BOARDMAN_R','JORDAN_RIVE','PLATTE_RIVE'};
% gageAreas = unit_conversions([141,68,125],'mi2','m2');
table = zeros(length(selGages),9); %first column eff, second column err (avg. annual) in cm
colHeaders = {'log E','E','annual flow error in mm','annual flow in mm',...
    'annual flow error in %','std of flow error in %','annual precip in mm',...
    'annual flow error in % of precip','std of error in % of precip'};

for n = 1:length(selGages)
    m = find(ismember(gages,selGages{n}));
    startDate = max(min(gauge_flow.(gages{m}).time_start),dateRange(1));
    endDate = min(max(gauge_flow.(gages{m}).time_start),dateRange(2));
    startInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,startDate,3,false));
    endInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,endDate,3,false));
    if ~(isempty(startInd) || isempty(endInd))
        %Calculate some inputs
        [months,monthlyVals] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            [gauge_flow.(gages{m}).sim(startInd:endInd),gauge_flow.(gages{m}).obs(startInd:endInd)],'months','sum');
        flowDiff = unit_conversions(gauge_flow.(gages{m}).sim(startInd:endInd) - ...
            gauge_flow.(gages{m}).obs(startInd:endInd),'ps','pday');
        [years,yearlyAvgDiff] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            flowDiff,'water years','sum');
        [years,yearlyAvgGage] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            unit_conversions(gauge_flow.(gages{m}).obs(startInd:endInd),'ps','pday'),'water years','sum');

%         table(n,1) = nash_sutcliffe(gauge_flow.(gages{m}).sim(startInd:endInd),...
%             gauge_flow.(gages{m}).obs(startInd:endInd),'log'); %daily log E
%         table(n,2) = nash_sutcliffe(gauge_flow.(gages{m}).sim(startInd:endInd),...
%             gauge_flow.(gages{m}).obs(startInd:endInd)); %daily E

        table(n,1) = nash_sutcliffe(monthlyVals(:,1),monthlyVals(:,2),'log'); %monthly log E
        table(n,2) = nash_sutcliffe(monthlyVals(:,1),monthlyVals(:,2)); %monthly E
        table(n,3) = nanmean(yearlyAvgDiff)/gageAreas(n)*1000; %annual flow error in mm
        table(n,4) = nanmean(yearlyAvgGage)/gageAreas(n)*1000; %annual flow in mm
        table(n,5) = table(n,3) / table(n,4) * 100; %annual flow error in %
        table(n,6) = nanstd(yearlyAvgDiff./yearlyAvgGage) * 100;
        table(n,7) = nanmean(yearlyPrecipGage{m})*1000; %annual precip in mm
        table(n,8) = table(n,3) / table(n,7) * 100; %annual flow error in % of precip
%         table(n,9) = nanstd(yearlyAvgDiff/gageAreas(n)./yearlyPrecipGage{m}) * 100;
    end
end
%%
%--------------------------------------------------------------------------
%Create the plots of simulated and observed hydrographs
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,1120,840])

gagesSel = {'04126740','04126970','04127800'};
gageSelNames = cell(size(gagesSel));
for m = 1:length(gagesSel)
    gageSelNames{m} = strrep(gauge_flow.loc_name{find(gageInd{m},1,'first')},'_',' ');
end
gageTicks = {(0:2:10),(0:2:10),(0:2:10)};
xLimits = [dateRange(1),dateRange(2)];

for m = length(gagesSel):-1:1
    subplot(length(gagesSel),1,m,'Position',[0.1,(0.9-0.8/length(gagesSel))-(m-1)/length(gagesSel)*0.8,0.8,0.8/length(gagesSel)])
    hold on
    %Extract these for convenience
    thisTime = gauge_flow.time_start(gageInd{m});
    thisSim = gauge_flow.sim(gageInd{m});
    thisObs = gauge_flow.obs(gageInd{m});
    %Determine some dates limits
    startDate = max(min(thisTime),dateRange(1));
    endDate = min(max(thisTime),dateRange(2));
    startInd = find(ismember_dates(thisTime,startDate,3,true));
    endInd = find(ismember_dates(thisTime,endDate,3,true));
    %Calculate monthly
    [months,monthlyVals] = time_series_aggregate(thisTime(startInd:endInd),...
            [thisSim(startInd:endInd),thisObs(startInd:endInd)],'months','mean');
    if endInd ~= startInd
        plot(months,monthlyVals(:,2),'k','LineWidth',0.5)
        plot(months,monthlyVals(:,1),'r','LineWidth',1)
        box on
        set(gca,'XLim',xLimits)
        thisTitle = title(gageSelNames{m},'FontSize',14);
        thisPos = get(thisTitle,'Position');
        set(thisTitle,'Position',[thisPos(1),thisPos(2)*0.9,thisPos(3)]);
%         set(thisTitle,'Position',[
        datetick('x','yyyy','keeplimits')
%         set(gca,'FontSize',18);
        set(gca,'YTick',gageTicks{m})
        set(gca,'YLim',[gageTicks{m}(1),gageTicks{m}(end)]);
    end
    if rem(m,2)==0
        set(gca,'YAxisLocation','right');
    end
    if m == 1
        legend({'Gage','Sim'},'Location','NorthEast')
    end
    if m == 2
        ylabel('Stream Discharge (m^3/s)','FontSize',20);
    end
    if m < length(gages)
        set(gca,'XTickLabel',{});
    end
end
% %%
% %Plot a second set of gages
% figure
% set(gcf,'Position',[0,0,1120,840])
%
% gages = {'Muskegon_River_at_Newaygo','Muskegon_River_near_Croton','Liitle_Muskegon_River_near_Oak_Grove'};
% gageNames = {'Muskegon at Newaygo','Muskegon near Croton','Little Muskegon near Oak Grove'};
% gageTicks = {(0:50:300),(0:50:300),(0:10:60)};
% for m = 1:length(gages)
%     subplot(length(gages),1,m)
%     hold on
%     startDate = max(min(gauge_flow.(gages{m}).time_start),dateRange(1));
%     endDate = min(max(gauge_flow.(gages{m}).time_start),dateRange(2));
%     startInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,startDate,3,true));
%     endInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,endDate,3,true));
%     if endInd ~= startInd
%         plot(gauge_flow.(gages{m}).time_start,gauge_flow.(gages{m}).obs,'k','LineWidth',0.5)
%         plot(gauge_flow.(gages{m}).time_start,gauge_flow.(gages{m}).sim,'r','LineWidth',1)
%         box on
%         set(gca,'XLim',[min(gauge_flow.(gages{m}).time_start),max(gauge_flow.(gages{m}).time_start)])
%         title(gageNames{m},'FontSize',20);
%         datetick('x','yyyy','keeplimits')
%         set(gca,'FontSize',18);
%         set(gca,'YTick',gageTicks{m})
%         set(gca,'YLim',[gageTicks{m}(1),gageTicks{m}(end)]);
%     end
%     if m == 2
%         legend({'Gage','Sim'})
%         ylabel('Stream Discharge (m^3/s)','FontSize',20);
%     end
% end
%
% %Do a third figure, assortment
% figure
% set(gcf,'Position',[0,0,1120,840])
%
% gages = {'Clam_River_at_Vogel_Center','Muskegon_River_at_Evart','Muskegon_River_near_Croton'};
% gageNames = {'04121300','04121500','04121970'};
% gageTicks = {(0:5:25),(0:50:200),(0:50:250)};
%
% thisDateRange = datenum({'10/1/2003','9/31/2007'});
% for m = 1:length(gages)
%     subplot(length(gages),1,m)
%     hold on
%     startDate = max(min(gauge_flow.(gages{m}).time_start),thisDateRange(1));
%     endDate = min(max(gauge_flow.(gages{m}).time_start),thisDateRange(2));
%     startInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,startDate,3,true));
%     endInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,endDate,3,true));
%     if endInd ~= startInd
%         plot(gauge_flow.(gages{m}).time_start(startInd:endInd),gauge_flow.(gages{m}).obs(startInd:endInd),'k','LineWidth',0.5)
%         plot(gauge_flow.(gages{m}).time_start(startInd:endInd),gauge_flow.(gages{m}).sim(startInd:endInd),'r','LineWidth',1)
%         box on
%         set(gca,'XLim',thisDateRange)
%         title(gageNames{m},'FontSize',20);
%         datetick('x','yyyy','keeplimits')
%         set(gca,'FontSize',18);
%         set(gca,'YTick',gageTicks{m})
%         set(gca,'YLim',[gageTicks{m}(1),gageTicks{m}(end)]);
%     end
%     if m == 2
%         legend({'Gage','Sim'})
%         ylabel('Stream Discharge (m^3/s)','FontSize',20);
%     end
% end
%%
%--------------------------------------------------------------------------
%Create the plots of sim vs. obs
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,1120,840])

%Plot flow residuals
gageName = 'Muskegon_River_at_Evart';
subplot(2,2,1)
startDate = max(min(gauge_flow.(gageName).time_start),dateRange(1));
endDate = min(max(gauge_flow.(gageName).time_start),dateRange(2));
startInd = find(ismember_dates(gauge_flow.(gageName).time_start,startDate,3,true));
endInd = find(ismember_dates(gauge_flow.(gageName).time_start,endDate,3,true));
plot(gauge_flow.(gageName).obs(startInd:endInd),(gauge_flow.(gageName).sim(startInd:endInd)...
    - gauge_flow.(gageName).obs(startInd:endInd))./gauge_flow.(gageName).obs(startInd:endInd)*100,'.','MarkerSize',2,'Color',[0.5,0.5,0.5]);
line([0,300],[0,0],'Color',[0,0,0])
ylabel('Unbiased Residual (%)','FontSize',20);
xlabel('Observed Discharge (m^3/s)','FontSize',20);
set(gca,'FontSize',18)

%Plot the sim/obs flows, splitting the sets
subplot(2,2,2)
hold on
sets = count_unique(flow.grid);
symbols = {'ko','r^','bs'};
for m = 1:length(sets)
    setInd = ismember(flow.grid,sets{m});
    plot(flow.obs(setInd),flow.base(setInd),symbols{m},'MarkerSize',5)
end
legend(sets)
line([0.001,10],[0.001,10],'Color',[0,0,0])
set(gca,'XLim',[0.001,10]);
set(gca,'YLim',[0.001,10]);
set(gca,'YTick',[0.001,0.01,0.1,1,10]);
set(gca,'XTick',[0.001,0.01,0.1,1,10]);
set(gca,'YScale','log');
set(gca,'XScale','log');
set(gca,'FontSize',18);
box on
ylabel('Simulated Baseflow (m^3/s)','FontSize',20);
xlabel('Observed Discharge (m^3/s)','FontSize',20);

%Plot total groundwater sim vs. obs head
subplot(2,2,3)
plot(head.obs,head.sim,'.','Color',[0.5,0.5,0.5],'MarkerSize',2);
line([160,425],[160,425],'Color',[0,0,0]);
set(gca,'XLim',[160,425]);
set(gca,'YLim',[160,425]);
set(gca,'FontSize',18);
ylabel('Simulated Head (m)','FontSize',20)
xlabel('Observed Head (m)','FontSize',20);

%Plot annual residual median and +/- 2 std
subplot(2,2,4)
years = (1980:1:2007);
obsDateVec = datevec(head.time_start);
[residMed,residStd] = deal(zeros(size(years)));
for m = 1:length(years)
    yearInd = obsDateVec(:,1)==years(m);
    thisResid = head.sim(yearInd) - head.obs(yearInd);
    residMed(m) = nanmedian(thisResid);
    residStd(m) = nanstd(thisResid);
end
plot(years,residMed,'Color',[0,0,0],'LineWidth',2);
patch([years,fliplr(years)],[residMed + (residStd),fliplr(residMed-(residStd))],[0.5,0.5,0.5])
line([years(1),years(end)],[0,0],'Color',[0,0,0]);
set(gca,'XLim',[years(1),years(end)]);
set(gca,'XTick',(1980:5:2005));
set(gca,'FontSize',18);
ylabel('Head Residual (m)','FontSize',20);
xlabel('Year','FontSize',20);

%%
%--------------------------------------------------------------------------
%Create the plots of sim vs. obs heads
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,1120,420])

%Plot total groundwater sim vs. obs head
subplot(1,2,1)
plot(head.obs,head.sim,'.','Color',[0.5,0.5,0.5],'MarkerSize',2);
line([160,425],[160,425],'Color',[0,0,0]);
set(gca,'XLim',[160,425]);
set(gca,'YLim',[160,425]);
set(gca,'FontSize',18);
ylabel('Simulated Head (m)','FontSize',20)
xlabel('Observed Head (m)','FontSize',20);

%Plot annual residual median and +/- 2 std
subplot(1,2,2)
years = (2000:1:2014);
obsDateVec = datevec(head.time_start);
[residMed,residStd] = deal(zeros(size(years)));
for m = 1:length(years)
    yearInd = obsDateVec(:,1)==years(m);
    if sum(yearInd) > 0
        thisResid = head.sim(yearInd) - head.obs(yearInd);
        residMed(m) = nanmedian(thisResid);
        residStd(m) = nanstd(thisResid);
    end
end
plot(years,residMed,'Color',[0,0,0],'LineWidth',2);
patch([years,fliplr(years)],[residMed + (residStd),fliplr(residMed-(residStd))],[0.5,0.5,0.5])
line([years(1),years(end)],[0,0],'Color',[0,0,0]);
set(gca,'XLim',[years(1),years(end)]);
set(gca,'XTick',(1980:4:2008));
set(gca,'FontSize',18);
ylabel('Head Residual (m)','FontSize',20);
xlabel('Year','FontSize',20);

%%
%--------------------------------------------------------------------------
%Plot annual model efficiency (log)
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,560,420])

gages = {'Bear_Creek_near_Muskegon','Clam_River_at_Vogel_Center','Liitle_Muskegon_River_near_Oak_Grove',...
    'Muskegon_River_at_Evart','Muskegon_River_near_Croton','Muskegon_River_at_Newaygo'};
gageNames = {'Bear','Clam','Little MR','MR Evart','MR Croton','MR Newaygo'};
colors = {'r','b','g','k','m','c'};
effOpt = 'log';
hold on
for m = 1:length(gages)
    [timeSplit,simSplit] = time_series_aggregate(gauge_flow.(gages{m}).time_start,gauge_flow.(gages{m}).sim,'water years','split');
    [timeSplit,obsSplit] = time_series_aggregate(gauge_flow.(gages{m}).time_start,gauge_flow.(gages{m}).obs,'water years','split');

    thisEff = zeros(length(simSplit)-2,1);
    for n = 2:length(simSplit)-1
        thisEff(n-1) = nash_sutcliffe(simSplit{n},obsSplit{n},effOpt);
    end
    startYear = datevec(timeSplit{2}(1));
    endYear = datevec(timeSplit{end-1}(end));
    waterYears = (startYear(1)+1:1:endYear(1))';
    plot(waterYears,thisEff,colors{m},'LineWidth',0.75)
end
line([1981,2007],[0,0],'Color',[0,0,0]);
box on
set(gca,'YLim',[-2,1]);
set(gca,'XLim',[1981,2007]);
set(gca,'FontSize',18);
ylabel('Log(Efficiency)','FontSize',20);
xlabel('Water Year','FontSize',20);
legend(gageNames)
%%
%--------------------------------------------------------------------------
%Plot annual and monthly model error (cm)
%--------------------------------------------------------------------------
figure
set(gcf,'Position',[0,0,560,840])

gages = {'Bear_Creek_near_Muskegon','Clam_River_at_Vogel_Center','Liitle_Muskegon_River_near_Oak_Grove',...
    'Muskegon_River_at_Evart','Muskegon_River_near_Croton','Muskegon_River_at_Newaygo'};
gageNames = {'Bear','Clam','Little MR','MR Evart','MR Croton','MR Newaygo'};
gageAreas = unit_conversions([43,629,894,3711,5991,6086],'km2','m2');
colors = {'r','b','g','k','m','c'};

%First annual
subplot(2,1,1)
hold on
for m = 1:length(gages)
    startDate = max(min(gauge_flow.(gages{m}).time_start),dateRange(1));
    endDate = min(max(gauge_flow.(gages{m}).time_start),dateRange(2));
    startInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,startDate,3,false));
    endInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,endDate,3,false));

    if endInd ~= startInd
        [timeSplit,simSplit] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            gauge_flow.(gages{m}).sim(startInd:endInd),'water years','split');
        [timeSplit,obsSplit] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            gauge_flow.(gages{m}).obs(startInd:endInd),'water years','split');

        %Remove partial water years
        if length(timeSplit{1})<365
            timeSplit(1) = [];
            simSplit(1) = [];
            obsSplit(1) = [];
        end
        if length(timeSplit{end})<365
            timeSplit(end) = [];
            simSplit(end) = [];
            obsSplit(end) = [];
        end

        thisErr = zeros(length(simSplit),1);
        for n = 1:length(simSplit)
            thisErr(n) = unit_conversions(nansum(simSplit{n} - obsSplit{n}),'ps','pday')/gageAreas(m);
        end
        startYear = datevec(timeSplit{1}(1));
        endYear = datevec(timeSplit{end}(end));
        waterYears = (startYear(1)+1:1:endYear(1))';
        plot(waterYears,thisErr*100,colors{m},'LineWidth',0.75)
    end
end
line([1980,2007],[0,0],'Color',[0,0,0]);
box on
set(gca,'XLim',[1980,2007]);
set(gca,'YLim',[-30,30]);
set(gca,'FontSize',18);
ylabel('Annual Error (cm)','FontSize',20);
xlabel('Water Year','FontSize',20);

%Then, monthly average
subplot(2,1,2)
hold on
for m = 1:length(gages)
    startDate = max(min(gauge_flow.(gages{m}).time_start),dateRange(1));
    endDate = min(max(gauge_flow.(gages{m}).time_start),dateRange(2));
    startInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,startDate,3,true));
    endInd = find(ismember_dates(gauge_flow.(gages{m}).time_start,endDate,3,true));

    if endInd ~= startInd
        [timeSplit,simSplit] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            gauge_flow.(gages{m}).sim(startInd:endInd),'months','split');
        [timeSplit,obsSplit] = time_series_aggregate(gauge_flow.(gages{m}).time_start(startInd:endInd),...
            gauge_flow.(gages{m}).obs(startInd:endInd),'months','split');

        [thisErr,thisMonth] = deal(zeros(length(simSplit),1));
        for n = 1:length(simSplit)
            thisMonth(n) = timeSplit{n}(1);
            thisErr(n) = unit_conversions(nansum(simSplit{n}) - nansum(obsSplit{n}),'ps','pday')/gageAreas(m);
        end

        %Now calculate cross-aggregation
        [months,monthlyAvg] = time_series_aggregate(thisMonth,thisErr*100,'months','sum','years','mean');

        %Plot
        plot(months,monthlyAvg,colors{m},'LineWidth',0.75);
    end
end
line([1,12],[0,0],'Color',[0,0,0]);
box on
set(gca,'XLim',[1,12])
set(gca,'XTick',(1:12))
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',18);
ylabel('Average Monthly Error (cm)','FontSize',20);
xlabel('Calendar Months','FontSize',20);
legend(gageNames)
