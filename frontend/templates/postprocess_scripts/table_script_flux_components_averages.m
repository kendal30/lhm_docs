%This script creates annual ET maps for the entire model

%--------------------------------------------------------------------------
%Specify Inputs and Outputs
%--------------------------------------------------------------------------
inputDir = 'D:\Users\Anthony\Modeling\ILHM\Output\test-v1\fullGrid13';
inputFilename = 'OUTF_test-v1_fullGrid13_20090409.h5';
inputFile = [inputDir,filesep,inputFilename];

inputFilename2 = 'OUTS_test-v1_fullGrid13_20090409.mat';
inputDir2 = 'D:\Users\Anthony\Modeling\ILHM\Output\test-v1\fullGrid13';
inputFile2 = [inputDir2,filesep,inputFilename2];

type = 'grid';
namespace = 'expanded';
dataType = 'plus';

allETDatasets = {'evap_canopy','evap_depression','evap_ice_pack','evap_soil',...
    'evap_snow_upland','evap_wetland','transp_upland','transp_wetland'};
allETRescale = {'all','upland','wetland','upland',...
    'upland','wetland','upland','wetland'};
    
allRunDatasets = {'precip_excess_upland','precip_excess_wetland'};
allRunRescale = {'upland','wetland'};

uplandETDatasets = {'evap_canopy','evap_soil','evap_snow_upland','transp_upland'};
uplandOtherDatasets = {'deep_percolation_upland','precip_excess_upland'};

precipDatasets = {'precip'};

inputStatic = 'D:\Users\Anthony\Modeling\ILHM\Input\STAT_MRW-4lay-soil_12_20090315.mat';

outputDir = 'D:\Users\Anthony\Graphics\5_05_09_Recalc_Chap_5';
outputFilename = 'flux_components_table.mat';
outputFile = [outputDir,filesep,outputFilename];

%%
%--------------------------------------------------------------------------
%Load lookup table and the interval table, and the MRW grid
%--------------------------------------------------------------------------
%Import the lookup table
lookup = h5dataread(inputFile,['/',type,'/',namespace,'/','lookup']);
%Create a mask for areas not in the active grid
mask = lookup == 0;
lookup(mask) = 1;

%Load the interval table
interval = h5dataread(inputFile,['/',type,'/',namespace,'/','interval']);

%Load the MRW grid, and trim to model domain
loadData = load(inputStatic,'mrw_grid');
mrwGrid = trim_domain(double(~mask),loadData.mrw_grid.data);
mrwGrid(isnan(mrwGrid))=0;
mrwGrid = logical(mrwGrid);
mrwFind = find(mrwGrid);

%--------------------------------------------------------------------------
%Load the rescale grids
%--------------------------------------------------------------------------
rescaleGrids = load(inputStatic,'water_fraction','stream_fraction');
rescaleGrids.stream_fraction.data = min(rescaleGrids.stream_fraction.data, 1-rescaleGrids.water_fraction.data);
wetFrac = trim_domain(~mask,rescaleGrids.water_fraction.data + rescaleGrids.stream_fraction.data)';
upFrac = 1 - wetFrac;
clear rescaleGrids
rescaleGrids = struct('upland',upFrac,'wetland',wetFrac,'all',upFrac+wetFrac);

%--------------------------------------------------------------------------
%Load geolocation information
%--------------------------------------------------------------------------
bot = h5dataread(inputFile,'/geolocation/bot');
cellsize = h5dataread(inputFile,'/geolocation/cellsize');
left = h5dataread(inputFile,'/geolocation/left');
numCol = h5dataread(inputFile,'/geolocation/num_col');
numRow = h5dataread(inputFile,'/geolocation/num_row');

%Build the header for asciiwrite
header = struct('cols',numCol,'rows',numRow,'left',left,'bottom',bot,'cellsize',cellsize,'noData',-9999);

%%
%--------------------------------------------------------------------------
%Loop through datasets for all data
%--------------------------------------------------------------------------
%Get grid size
gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{1},'/',dataType],false);
zerosGrid = zeros(gridSize);

%Create output
allData = struct();

%Get the precip data
thisData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',precipDatasets{1},'/',dataType],true,...
    [0,0],[gridSize(1),gridSize(2)],[1,1]);
%Generate the table data
[years,yearlyData] = time_series_aggregate(interval(:,1),thisData(:,mrwFind),'water years','sum');
yearlyData = nanmean(yearlyData(2:end-1,:),2);
precipYearly = yearlyData;
allData.precip.mean = nanmean(yearlyData(:));
allData.precip.std = nanstd(yearlyData(:));

etYearly = zeros(size(yearlyData,1),1);
%All ET
for m = 1:length(allETDatasets)
    %Initialize the dataset
    thisData = zerosGrid;
    
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],false);
    
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
    
    %Rescale to the domain of the dataset
    thisData = thisData.* repmat(rescaleGrids.(allETRescale{m}),gridSize(1),1);
    
    %Generate the table data
    [years,yearlyData] = time_series_aggregate(interval(:,1),thisData(:,mrwFind),'water years','sum');
    yearlyData = nanmean(yearlyData(2:end-1,:),2);
    etYearly = etYearly + yearlyData;
    allData.total.(allETDatasets{m}).mean = nanmean(yearlyData(:));
    allData.total.(allETDatasets{m}).std = nanstd(yearlyData(:));
end

%All Run
runYearly = zeros(size(yearlyData,1),1);
for m = 1:length(allRunDatasets)
    %Initialize the dataset
    thisData = zerosGrid;
    
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',allRunDatasets{m},'/',dataType],false);
    
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allRunDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',allRunDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
    
    %Rescale to the domain of the dataset
    thisData = thisData.* repmat(rescaleGrids.(allRunRescale{m}),gridSize(1),1);
    
    %Generate the table data
    [years,yearlyData] = time_series_aggregate(interval(:,1),thisData(:,mrwFind),'water years','sum');
    yearlyData = nanmean(yearlyData(2:end-1,:),2);
    runYearly = runYearly + yearlyData;
    allData.total.(allRunDatasets{m}).mean = nanmean(yearlyData(:));
    allData.total.(allRunDatasets{m}).std = nanstd(yearlyData(:));
end


%Special handling of baseflow
%Get the groundwater part
load(inputFile2,'postDischargeMonthly','postEvapMonthly');
postDischargeMonthly = double(postDischargeMonthly(1:336,:))/1e6; %to rescale from int32 * 1e6
postEvapMonthly = double(postEvapMonthly(1:336,:))/1e6; %to rescale from int32 * 1e6
thisData = postDischargeMonthly - postEvapMonthly; %to remove evaporated water from groundwater discharge
clear postDischargeMonthly postEvapMonthly

%Generate the table data
[years,yearlyData] = time_series_aggregate(interval(:,1),thisData(:,mrwFind),'water years','sum');
yearlyData = nanmean(yearlyData(2:end-1,:),2);
runYearly = runYearly + yearlyData;
allData.total.baseflow.mean = nanmean(yearlyData(:));
allData.total.baseflow.std = nanstd(yearlyData(:));

storageYearly = precipYearly - etYearly - runYearly;
allData.total.storage.mean = nanmean(storageYearly);
allData.total.storage.std = nanstd(storageYearly);

allData.total.et.mean = nanmean(etYearly);
allData.total.et.std = nanstd(etYearly);
allData.total.run.mean = nanmean(runYearly);
allData.total.run.std = nanstd(runYearly);

%% Now Uplands
%Here, exclude all areas with 0 upland fraction
uplandFind = find(mrwGrid & (rescaleGrids.upland > 0)');

%Upland ET
uplandETYearly = zeros(size(yearlyData,1),1);
for m = 1:length(uplandETDatasets)
    %Initialize the dataset
    thisData = zerosGrid;
    
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],false);
    
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandETDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
        
    %Generate the table data
    [years,yearlyData] = time_series_aggregate(interval(:,1),thisData(:,uplandFind),'water years','sum');
    yearlyData = nanmean(yearlyData(2:end-1,:),2);
    uplandETYearly = uplandETYearly + yearlyData;
    allData.upland.(uplandETDatasets{m}).mean = nanmean(yearlyData(:));
    allData.upland.(uplandETDatasets{m}).std = nanstd(yearlyData(:));
    allData.upland.(uplandETDatasets{m}).cv = nanstd(yearlyData(:))/nanmean(yearlyData(:));
end

allData.upland.et.mean = nanmean(uplandETYearly);
allData.upland.et.std = nanstd(uplandETYearly);
allData.upland.et.cv = nanstd(uplandETYearly)/nanmean(uplandETYearly);

%Upland Other
for m = 1:length(uplandOtherDatasets)
    %Initialize the dataset
    thisData = zerosGrid;
    
    %Get grid size
    gridSize = h5varget(inputFile,['/',type,'/',namespace,'/datasets/',uplandOtherDatasets{m},'/',dataType],false);
    
    %Load monthly data
    if length(gridSize) == 2
        tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandOtherDatasets{m},'/',dataType],true,...
            [0,0],[gridSize(1),gridSize(2)],[1,1]);
        thisData = thisData + tempData;
    else
        for n = 1:gridSize(3) %Loop through the third dimension
            tempData = h5dataread(inputFile,['/',type,'/',namespace,'/datasets/',uplandOtherDatasets{m},'/',dataType],true,...
                [0,0,n-1],[gridSize(1),gridSize(2),1],[1,1,1]);
            thisData = thisData + tempData;
        end
    end
        
    %Generate the table data
    [years,yearlyData] = time_series_aggregate(interval(:,1),thisData(:,uplandFind),'water years','sum');
    yearlyData = nanmean(yearlyData(2:end-1,:),2);
    allData.upland.(uplandOtherDatasets{m}).mean = nanmean(yearlyData(:));
    allData.upland.(uplandOtherDatasets{m}).std = nanstd(yearlyData(:));
    allData.upland.(uplandOtherDatasets{m}).cv = nanstd(yearlyData(:))/nanmean(yearlyData(:));
end
%%
%--------------------------------------------------------------------------
%Output the data file
%--------------------------------------------------------------------------
save(outputFile,'-struct','allData');

%%
%--------------------------------------------------------------------------
%Create the monthly plot - annual calendar
%--------------------------------------------------------------------------
figure
% colormap('gray')
% colormap(flipud(colormap))
colormap('jet')

area(allData.Canopy.months,[allData.Wetland_Evap.monthlyAvg,...
    allData.Snow_Sub.monthlyAvg+allData.Ice_Sub.monthlyAvg,...
    allData.Canopy.monthlyAvg,...
    allData.Soil.monthlyAvg+allData.Depression.monthlyAvg,...
    allData.Wetland_Transp.monthlyAvg,allData.Upland_Transp.monthlyAvg]*100)

%Set properties
set(gca,'XLim',[1,12]);
set(gca,'YLim',[0,10]);
set(gca,'XTick',(1:12));
set(gca,'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'});
set(gca,'FontSize',18)
ylabel(gca,'Monthly Flux (cm/m^2)','FontSize',20);
xlabel(gca,'Calendar Month','FontSize',20);
legend({'Open Water Evap','Sublimation','Canopy Evap','Soil Evap','Wetland Transp','Upland Transp'})
set(gca,'Box','on')

