function h = value_shaded_matrix(values,xLabels,yLabels,numFormat,colorLabel,inputCMap)
%VALUE_SHADED_MATRIX  Create a value-shaded matrix.
%   h = value_shaded_matrix(values,xLabels,yLabels,inputCMap)
%
%   Descriptions of Input Variables:
%   values: an MxN array containing the values of the matrix
%   xLabels: labels of the rows of the matrix, in descending order
%   yLabels: labels of the columns of the matrix, in ascending order
%   numFormat: number format for the printed values, see num2str, if not
%       provided, will be %4.2f
%   colorLabel: labels the colorbar, if isempty, colorbar will not be
%       placed on the figure
%   inputCMap: optionally, this specifies the input colormap to use for
%       shading the cells.  Either a string can be used, indicating a built-in
%       MATLAB colormap, or an array specifying a custom colormap can be used.
%       Then, the size must be OX3, where O is the number of values in the colormap.
%       If this variable is not specified, the default colormap will be chosen.  In 
%       either case, the shading will be normalized to the maximum and 
%       minimum values within the 'values' array.
%
%   Descriptions of Output Variables:
%   h: object handle of the output figure.
%
%   Example(s):
%   >> value_shaded_matrix
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-05
% Copyright 2008 Michigan State University.

%Process the inputs
if nargin < 4
    numFormat = '%4.2f';
end
if nargin < 5
    colorLabel = '';
end
if nargin < 6
    inputCMap = '';
end
%Using the input values matrix, build the cell vertex matrices for patch
[numRow,numCol] = size(values);
numVals = numRow*numCol;
[patchX,patchY] = deal(zeros(4,numVals));

yScale = 1;
xScale = 2.3;

%Vertex order will be clockwise, so Top-Left: 1, Top-Right:2, Bottom-Right: 3,
%Bottom-Left:4
patchX(1,:) = repmat((0:1:numCol-1),1,numRow);
patchX(4,:) = patchX(1,:); 
patchX(2,:) = repmat((1:1:numCol),1,numRow);
patchX(3,:) = patchX(2,:);
patchX = patchX * xScale;

patchY(1,:) = reshape(repmat((0:-1:-(numRow-1))',1,numCol)',1,numVals);
patchY(2,:) = patchY(1,:);
patchY(3,:) = reshape(repmat((-1:-1:-numRow)',1,numCol)',1,numVals);
patchY(4,:) = patchY(3,:);
patchY = patchY * yScale;

%Create the new figure, and format it properly for display
h = figure;

if ischar(inputCMap)
    if ~isempty(inputCMap)
        colormap(inputCMap);
    end
    cMap = get(h,'Colormap');
else
    cMap = inputCMap;
end
alphaFactor = 0.75;
cMap = cMap * (alphaFactor) + ones(size(cMap)) * (1-alphaFactor);
set(h,'Colormap',cMap);

axis equal
axis([0,numCol*xScale,-numRow*yScale,0])
set(gca,'XTick',(0.5:1:(numCol - 0.5))*xScale);
set(gca,'YTick',fliplr((-0.5:-1:-(numRow - 0.5)))*yScale);
set(gca,'XAxisLocation','top');

%Build the colormap for the patches
minVal = min(min(values));
valuesNorm = values - minVal;
maxVal = max(max(valuesNorm));
valuesNorm = valuesNorm / maxVal;
valuesDiscrete = round(valuesNorm*(size(cMap,1)-1))+1;
cIndices = reshape(valuesDiscrete',1,numVals);
cValues = cMap(cIndices,:);

%Plot the patch object
patch(patchX,patchY,cIndices);

%Plot the values themselves within the cells
for m=1:numRow
    for n=1:numCol
        %Determine if the color needs to be inverted based on the darknes
        %off the colormap
        totalC = sum(cValues(n+(m-1)*numCol,:));
        if totalC * alphaFactor <= 0.8
            fontC = 'white';
        else
            fontC = 'black';
        end
        text((n - 0.75)*xScale, -(m - 0.5)*yScale,num2str(values(m,n),numFormat),'FontSize',12,'Color',fontC);
    end
end

%Label the axes
set(gca,'XTickLabel',xLabels);
set(gca,'YTickLabel',flipud(yLabels'));
set(gca,'FontSize',13,'FontWeight','bold');
set(gca,'TickLength',[0,0]);

%Insert the colorbar, also adjust the colormap to account for alpha

if ~isempty(colorLabel)
    g = colorbar;
    set(g,'Position',[0.85,0.25,0.035,0.5]);
    set(g,'YTick',(1:((length(cMap)-1)/4):length(cMap)));
    set(g,'YTickLabel',num2str((1:((length(cMap)-1)/4):length(cMap))'/length(cMap)*maxVal+minVal,'%4.2f'));
    set(g,'FontSize',12);
    set(g,'XAxisLocation','top');
    set(g,'TickLength',[0,0.1]);
    set(g,'XTick',0.0125,'XTickLabel',colorLabel);
end