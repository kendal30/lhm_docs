%This script just calculates a data table of various water budget
%components, must be run after the requisite plot scripts

inputDir = 'D:\Users\Anthony\Graphics\2_19_09_Thesis_Chap_5';
inputFile1 = 'monthly_ET_components_data.mat';
inputFile2 = 'monthly_ET_R_data.mat';

datasets = {'precip','allET','allDischarge','uplandET','uplandR','uplandRun',...
    'Canopy','Depression','Ice_Sub','Soil','Snow_Sub','Wetland_Evap','Upland_Transp','Wetland_Transp'};

%Load the data
loadData1 = load([inputDir,filesep,inputFile1]);
loadData2 = load([inputDir,filesep,inputFile2]);
allData = merge_struct(loadData1,loadData2);

%Build the table
table = zeros(length(datasets),2);
for m = 1:length(datasets)
    table(m,1) = mean(allData.(datasets{m}).yearlyAvg);
    table(m,2) = std(allData.(datasets{m}).yearlyAvg);
end