function obj = optim_wrapper(paramsOrInit)
% This wrapper function is called both to initialize the optimization, and
% during the optimization loop. It recognizes the calling context based on
% whether it is passed a structured array input or a list of parameters
    persistent workspace

    if isstruct(paramsOrInit)
        workspace = paramsOrInit;
    else
        update_modflow(paramsOrInit,workspace.groundwater);
        obsTable = run_modflow(workspace);
        obj = mae(obsTable);
        
        % Write the output
        fid = fopen(workspace.outFile,'at');
        fprintf(fid,['\n',repmat('%.10f,',1,length(paramsOrInit)),'%f'],[paramsOrInit,obj]);
        fclose(fid);
    end
end


function update_modflow(alphas,groundwater)
% Map alphas to mult array
startHK = 1;
if isfield(groundwater.mult,'hk')
    numHK = length(groundwater.mult.hk.curr);
    groundwater.mult.hk.curr = alphas(startHK:startHK+numHK-1);
    startVANI = numHK + startHK;
    numCheck = startHK + numHK - 1;
end
if isfield(groundwater.mult,'vani')
    numVANI = length(groundwater.mult.vani.curr);
    groundwater.mult.vani.curr = alphas(startVANI:startVANI+numVANI-1);
    numCheck = numVANI + startVANI - 1;
end
assert(numCheck==length(alphas),'The number of optimized parameters does not match parameter mapping');

% Update the appropriate package file
if groundwater.package.upw.active
    packageFunc = str2func(groundwater.package.upw.initFunc);
    package = groundwater.package.upw;
elseif groundwater.package.lpf.active
    packageFunc = str2func(groundwater.package.lpf.initFunc);
    package = groundwater.package.lpf;
end
packageFunc('.',package,groundwater.structure,...
    groundwater.params,groundwater.constants,groundwater.mult,groundwater.grids);
end


function obsTable = run_modflow(workspace)
% Run the unsaturated zone module
unsaturated_zone_module(workspace.state,workspace.structure,workspace.groundwater);

% If the EVT file is absent, run that as well (not optim-dependent)
if ~exist([workspace.groundwater.structure.model_name,'_evt.dat'],'file')
    evt_binary_write(workspace.structure,workspace.groundwater);
end

% Run modflow
system([workspace.groundwater.structure.modflow_exec ' ' workspace.groundwater.package.nam.name]);

% Read in the heads
obsStruct = obs_heads_read(workspace.structure,workspace.groundwater);
obsTable = obsStruct.head;
end

function obj = mae(obsTable)
% Calculate the mean absolute error
obj = mean(abs(obsTable.sim - obsTable.obs),'omitnan');
end