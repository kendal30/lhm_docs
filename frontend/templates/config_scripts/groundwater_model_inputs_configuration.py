# -*- coding: utf-8 -*-
"""
Configuration script for 
Created on Sat Feb 16 09:55:55 2019

@author: kendal30
"""
import os

#Specify the path to the model input geodatabase
inputDB = r'F:\Users\Anthony\Data\GIS_Data\Active\Kalamazoo_River_Submodel\Saugatuck_Model_Boundaries.gdb'


#Specify a geodatabase that will be used for scratch workspace
scratchDB = r'F:\Users\Anthony\Modeling\ILHM\Prep\2019\Saugatuck\Import_Working\modflow_scratch_2.gdb'

#Specify the directory where outputs will be written
outDir = r'F:\Users\Anthony\Modeling\ILHM\Prep\2019\Saugatuck\Model_Layers\Groundwater'

#Specify where to find the frontend addpath script    
scriptAddpath = 'addpath_frontend.py'
pathFrontend = r'S:\Users\kendal30\code\lhm\frontend'

#Add helper files to the path
thisDir = os.curdir
os.chdir(pathFrontend)
exec(open(os.path.join(pathFrontend,scriptAddpath)).read())
os.chdir(thisDir)
    
    
#------------------------------------------------------------------------------
#Specify input 'sources' and 'groundwater' dictionaries
#------------------------------------------------------------------------------
"""fieldVal: from which IBOUND values will be obtained, fieldLay: layer to which those values will be written
If not all layers are specified then the last layer will be copied to all lower layers
If a feature should be applied to all layers, use -1 for the field value

Notes on using 'method':'grid', you grids should be specified in layer order, any layers unspecified will
use the previous layer. So, for instance if you specify 5 inputs in your ibound features
configuration for a 5 layer model, then it is presumed this list is in layer order, if 
you only specify 1 input, this will be copied to all layers, if you specify 3, then layers 1-3
will have unique ibound grids, while 4 and 5 will have copies of layer 3"""

#Input data sources
sources = dict()
sources['bottom'] = {'method':'grid','fieldVal':'','fieldLay':''}
sources['bottom']['data'] = [r'F:\Users\Anthony\Data\GIS_Data\Active\Michigan\Model_Bottom\bedrock_rekrig_usgs_lusch_m.img']

sources['startHeads'] = {'method':'grid','fieldVal':'','fieldLay':''} #for now ony a single layer is supported for startHeads
sources['startHeads']['data'] = [r'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model\LP_model_prep.gdb\prep_dem_raw']

sources['boundaryHeads'] = {'method':'feature','fieldVal':'const_head','fieldLay':'IBOUND_lay'} #for now ony a single layer is supported for boundaryHeads
sources['boundaryHeads']['data'] = [r'F:\Users\Anthony\Data\GIS_Data\Active\Kalamazoo_River_Submodel\Saugatuck_Model_Boundaries.gdb\specified_head_boundaries']

sources['top'] = {'method':'grid','fieldVal':'','fieldLay':''} 
sources['top']['data'] = [r'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model\LP_model_prep.gdb\prep_dem_raw']

sources['ibound'] = {'method':'feature','fieldVal':'IBOUND_val','fieldLay':'IBOUND_lay','data':''}
sources['ibound']['data'] = ['model_boundary_refined','specified_head_boundaries'] #subsequent features overwrite values of earlier ones
sources['ibound']['data'] = [os.path.join(inputDB,thisFeature) for thisFeature in sources['ibound']['data']]

sources['horizontal_conductivity'] = {'method':'grid','fieldVal':'','fieldLay':''}
sources['horizontal_conductivity']['data'] = [r'F:\Users\Anthony\Data\GIS_Data\Active\Michigan\Conductivities\Qgeo_HK_md_limit_water_100.tif']
sources['vertical_anisotropy'] = {'method':'constant','fieldVal':'','fieldLay':'','data':2}
sources['specific_yield'] = {'method':'constant','fieldVal':'','fieldLay':'','data':0.25}
sources['specific_storage'] = {'method':'constant','fieldVal':'','fieldLay':'','data':1e-4}

#Specify an EVT extinction depth
sources['evt_exctinction_depth'] = {'method':'constant','fieldVal':'','fieldLay':'','data':3}

#Specify DRN and RIV input sources
sources['drn_bed_thickness'] = {'method':'constant','fieldVal':'','fieldLay':'','data':1}
sources['drn_bed_conductivity'] = {'method':'grid','fieldVal':'','fieldLay':''} 
sources['drn_bed_conductivity']['data'] = sources['horizontal_conductivity']['data']
sources['drn_exclusion_mask'] = {'method':'feature','fieldVal':'Drn_exclude','fieldLay':'IBOUND_lay'}
sources['drn_exclusion_mask']['data'] = [r'F:\Users\Anthony\Data\GIS_Data\Active\Kalamazoo_River_Submodel\Saugatuck_Model_Boundaries.gdb\drn_exclusion_area',\
       r'F:\Users\Anthony\Data\GIS_Data\Active\Kalamazoo_River_Submodel\Saugatuck_Model_Boundaries.gdb\specified_head_boundaries']
sources['riv_bed_thickness'] = {'method':'constant','fieldVal':'','fieldLay':'','data':1}
sources['riv_bottom_depth'] = {'method':'feature','fieldVal':'Riv_bot_depth','fieldLay':'Riv_lay'}
sources['riv_bottom_depth']['data'] = [r'F:\Users\Anthony\Data\GIS_Data\Active\Kalamazoo_River_Submodel\Saugatuck_Model_Boundaries.gdb\lake_kalamazoo_river_cells']
sources['riv_bed_conductivity'] = {'method':'grid','fieldVal':'','fieldLay':''} 
sources['riv_bed_conductivity']['data'] = sources['horizontal_conductivity']['data']

#Only need to enter these if you are not using the DEM to set elevations
sources['drn_elevation'] = {'method':'feature','fieldVal':'','fieldLay':''}
sources['drn_elevation']['data'] = []
sources['riv_elevation'] = {'method':'feature','fieldVal':'Riv_surf_elev','fieldLay':'Riv_lay'}
sources['riv_elevation']['data'] = sources['riv_bottom_depth']['data']

#Specify the 'groundwater' dictionary
groundwater = dict()
#IBOUND parameters
groundwater['numLay'] = 6 #Specify the total number of MODFLOW layers
groundwater['cellsize'] = 80 #either a number, or a complete path to a reference grid (which will also be used for snapping)

#Layering parameters
groundwater['numLay'] = 6 #Specify the number of layers in the MODFLOW model
groundwater['minLayThick'] = [1,1,1,1,1,1] #Either a single scalar, or a list of length equal to numLay
groundwater['maxLayThick'] = [5,5,5,10,20,40] #specify if some layers should have a maximum thickness, set 0 otherwise
#i.e. the thickness beyond the equalThickThresh will be divided among the layers that have no maximum thickness specified
groundwater['equalThickThresh'] = 5

# This section is to add a deep bedrock layer
groundwater['useDeepBedrock'] = True
groundwater['deepBedrockThick'] = 100

#Starting heads parameters
groundwater['minStartHeads'] = 176.0 #Specify the minimum start heads value, as a manual threshold on interpolated start heads, specify "False" if not using this condition
groundwater['minLaySatConstHead'] = False #Specify the minimum saturation in the first layer for constant head cells, specify "False" if not using this condition

#Aquifer properties parameters
groundwater['lakeConductivity'] = 100 #specify the conductivity assigned to cells that are within lakes

#Specify whether to use certain packages
groundwater['useEVT'] = True
groundwater['useDRN'] = True
groundwater['useRIV'] = False

#Specify parameters for internal boundary conditions
groundwater['drnElevDEM'] = True#if true, this will use the DEM to set drains, using the minimum finer-scale value, otherwise will use the input source feature
groundwater['drnBuffCells'] = 1 #number of cells to buffer away from a specified head cell
groundwater['drnBuffVal'] = 1 #value to buffer around in the exclusion mask array
groundwater['drnLayBotDiff'] = 1.1  #Specify the minimum allowed depth (in model units) to the bottom of the first layer from the bottom of the drain
groundwater['drnMinElev'] = 176.0 #Specify as a numeric value if desired, otherwise set to False

groundwater['rivElevDEM'] = False #if true, this will use the DEM to set river elevations, just like for drns, otherwise will use the input source feature
groundwater['rivLayBotDiff'] = 1.1  #Specify the minimum allowed depth (in model units) to the bottom of the first layer from the bottom of the drain
groundwater['rivMinElev'] = 176.0 #Specify as a numeric value if desired, otherwise set to False, will only be used if 'rivElevDEM' is True

addArgs = dict()
#------------------------------------------------------------------------------
#Custom 'modflow_ibound_prep' inputs
#------------------------------------------------------------------------------
#Specify the feature to match the extent and coordinate system to
addArgs['inMatchExtent'] = os.path.join(inputDB,'model_boundary_initial')

#Specify output names
addArgs['nameOutIbound'] = 'ibound_no_marina_lay'

#These are needed for later functions
addArgs['iboundName'] = addArgs['nameOutIbound']
addArgs['iboundDir'] = outDir
#------------------------------------------------------------------------------
#Custom 'modflow_dem_dependent' inputs
#------------------------------------------------------------------------------
#Specify output names
addArgs['nameOutBot'] = 'bottom_lay'
addArgs['nameOutTop'] = 'model_top'
addArgs['nameOutStart'] = 'start_heads_no_marina_lay'

#------------------------------------------------------------------------------
#Custom 'modflow_aquifer_properties_prep' inputs
#------------------------------------------------------------------------------
addArgs['nameOutSpecYield'] = 'spec_yield_lay'
addArgs['nameOutSpecStor'] = 'spec_stor_lay'
addArgs['nameOutHorizCond'] = 'hk_lay'
addArgs['nameOutVertAni'] = 'vani_lay'

#Input path for lake K
lhmPrepDB = r'F:\Users\Anthony\Modeling\ILHM\Prep\2014\LP_Model\LP_model_prep.gdb'
addArgs['inputWetDepth'] = os.path.join(lhmPrepDB,'prep_wetland_depth')

#------------------------------------------------------------------------------
#Custom 'modflow_aquifer_properties_prep' inputs
#------------------------------------------------------------------------------
#Some input paths
addArgs['inputDEM'] = os.path.join(lhmPrepDB,r'prep_dem_raw')
addArgs['inputDEMFill'] = os.path.join(lhmPrepDB,'prep_dem_fill')
addArgs['inputIntDrain'] = os.path.join(lhmPrepDB,'prep_intern_drain_grid')
addArgs['inputStrFrac'] = os.path.join(lhmPrepDB,'prep_stream_fraction')
addArgs['inputWetPres'] = os.path.join(lhmPrepDB,'prep_wetland_present')
addArgs['inputWetComp'] = os.path.join(lhmPrepDB,'prep_wetland_complex')

#Output Names
addArgs['nameOutETDepth'] = 'et_extinct_depth'
addArgs['nameOutETSurface'] = 'et_surf'
addArgs['nameOutDrnArea'] = 'drains_area'
addArgs['nameOutDrnK'] = 'drains_K'
addArgs['nameOutDrnElev'] = 'drains_elev'
addArgs['nameOutDrnCond'] = 'drains_cond'
addArgs['nameOutDrnPresence'] = 'drains'
addArgs['nameOutRivElev'] = 'rivers_elev'
addArgs['nameOutRivCond'] = 'rivers_cond'
addArgs['nameOutRivBot'] = 'rivers_bot'
addArgs['nameOutRivPresence'] = 'rivers'
    