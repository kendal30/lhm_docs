%Jobs list
jobsInitFiles = {'G:\Modeling\ILHM\Output\AGU_2012\2-2050-A2\INIT_AGU_2012_2-2050-A2_20121128.mat',...
    'G:\Modeling\ILHM\Output\AGU_2012\2-2095-A2\INIT_AGU_2012_2-2095-A2_20121128.mat'};
%Input dir
inputDir = 'G:\Modeling\ILHM\Input';
%Cpu Threshold
cpuThreshold = 75;

addpath(pwd)
currJob = 0;
numJobs = length(jobsInitFiles);
while currJob < numJobs
    %Check to see what resource usage was over the last minute
    tic
    avgLoad = 0;
    numChecks = 0;
    while toc < 60
        [~,cpuLoad] = system('wmic cpu get loadpercentage');
        numChecks = numChecks + 1;
        cpuLoad = regexp(cpuLoad,'[0-9]*','match');
        avgLoad = avgLoad + (str2double(cpuLoad{1}) + str2double(cpuLoad{2}))/2;
        pause(3); %pause for three seconds
    end
    avgLoad = avgLoad / numChecks;
    
    %If the resource load is less than the cpu threshold, launch the next
    %job
    if avgLoad<cpuThreshold
        currJob = currJob + 1;
        [currPath,~,~] = fileparts(jobsInitFiles{currJob});
        cd(currPath); %Change to the base ILHM directory if not already there
        disp(['Launching ',jobsInitFiles{currJob}]);
        initiateStr = ['!matlab -nosplash -minimize -r "ilhm_start(''',inputDir,''')"&'];
        eval(initiateStr);
        pause(60*3) %pause for 3 minutes to wait for full loading
    else
        disp([datestr(now),': System usage is ',num2str(avgLoad),', > ',num2str(cpuThreshold),'% CPU usage'])
        pause(60*10) %pause for 10 minutes to wait for an opening
    end
end

%Quit this instance of MATLAB
exit