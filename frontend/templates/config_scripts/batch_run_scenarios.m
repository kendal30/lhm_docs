optionUnzip = false;

%Specify the init filenames
initFiles = {'CHP_CHP-new-grid_2008-2014-annual-irrig-tech-1_20171205.zip',...
    'CHP_CHP-new-grid_2008-2014-annual-irrig-tech-2_20171205.zip',...
    'CHP_CHP-new-grid_2008-2014-annual-irrig-tech-3_20171205.zip',...
    'CHP_CHP-new-grid_2008-2014-annual-irrig-tech-4_20171205.zip',...
    'CHP_CHP-new-grid_2008-2014-annual-irrig-tech-5_20171205.zip',...
    'CHP_CHP-new-grid_2008-2014-annual-irrig-tech-7-6cm_20171205.zip'};

%Specify several folders
dirInit = 'F:\Users\kendal30\Modeling\LHM\Init';
dirOutput = 'F:\Users\kendal30\Modeling\LHM\Output';
dirInput = 'F:\Users\kendal30\Modeling\LHM\Input';

%Loop through files, unzip them, then cd to that directory 
dirOrig = pwd;
for m = 1:length(initFiles)
    thisFile = initFiles{m};
    
    if optionUnzip
        %Unzip the init files
        unzip([dirInit,filesep,thisFile],dirOutput);
    end
    
    %Parse the filename
    modelParts = split(thisFile,'_');
    
    %Change directory and then run
    cd([dirOutput,filesep,modelParts{2},filesep,modelParts{3}]);
    eval(['!matlab -nosplash -r "lhm_start(''', dirInput, ''')"&']);
end
cd(dirOrig)