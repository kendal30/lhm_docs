function [pathConfig,pathFrontend] = initialize_notebook(nameNotebook)
%more off % not supported in the new official Jupyter capability in MATLAB

% Define constants for use below
configNotebook = 'notebook.yml';
scriptAddpath = 'addpath_frontend.m';

% Set up the paths
pathConfig = pwd; %path where this notebook is run

% Get the frontend location
fid = fopen(configNotebook,'rt');
lineCfg = fgetl(fid);
while lineCfg > 0
    lineCfg = strtrim(lineCfg);
    if length(lineCfg) > 12
        if strcmpi(lineCfg(1:12),'pathFrontend')
            [~,pathFrontend] = strtok(lineCfg,':');
            pathFrontend = strtrim(pathFrontend);
            if strcmpi(pathFrontend(1),':')
                pathFrontend(1)=[];
            end
            pathFrontend = strtrim(pathFrontend);
        end
    end
    lineCfg = fgetl(fid);
end
fclose(fid);

%Addpath to the LHM Frontend
run([pathFrontend,filesep,scriptAddpath]);
fprintf('Paths are set for the %s notebook',nameNotebook);