#Define constants for use below
configNotebook <- 'notebook.yml'
scriptAddpath <- 'addpath_frontend.R'

#Set up the paths
pathConfig <- getwd()

#Read the path to the frontend from the notebook.cfg file
library(yaml)
config <- read_yaml(configNotebook)

#Get the frontend location
pathFrontend <- config$frontend$pathFrontend

#Add the LHM frontend paths to the system path
setwd(pathFrontend)
source(scriptAddpath)
setwd(pathConfig)

#Get the regionName
regionName <- basename(pathConfig)

#Now, get the location of the model prep directory
library(stringr)
if (str_sub(dirname(pathConfig),-6,-1) != 'Config') {
    optionalSubDir <- basename(dirname(pathConfig))
    pathModeling <- dirname(dirname(dirname(pathConfig))) #must go up three levels
    pathPrep <- paste0(pathModeling,'/Prep/',optionalSubDir,'/',regionName)
} else {
    optionalSubDir <- FALSE
    pathModeling <- dirname(dirname(pathConfig)) #must go up two levels
    pathPrep <- paste0(pathModeling,'/Prep/',regionName)
}
pathLAI <- paste0(pathPrep,'/LAI_Clipped')

#Print this:
print('Paths are set')