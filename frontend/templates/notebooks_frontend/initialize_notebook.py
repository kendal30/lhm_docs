def initialize_notebook(notebook, subModel=False):
    import os, yaml, sys, glob
    from IPython.display import display, HTML

    # Define constants for use below
    configNotebook = 'notebook.yml'

    # Set up the paths
    pathConfig = os.getcwd() #path where this notebook is run

    # Read in the path to the frontend from the notebook.yml file
    configDict = yaml.safe_load(open(os.path.join(pathConfig,configNotebook)))
    pathFrontend = configDict['frontend']['pathFrontend']

    # Add the LHM frontend paths to the system pat
    sys.path.append(pathFrontend) #add the current directory

    #Add the preparation and import folders recursively so those scripts can be called with arguments
    subDirList = ['preparation','import']
    for subDir in subDirList:
        thisDirs = glob.glob(pathFrontend+'\\'+subDir+'\\**\\',recursive=True)
        [sys.path.append(thisDir) for thisDir in thisDirs]

    #Add the helper folder non-recursively
    dirList = ['helper']
    for thisDir in dirList:
        sys.path.append(os.path.join(pathFrontend,thisDir))

    # Test to see if this is the top-level Frontend directory
    if os.path.normpath(configDict['frontend']['pathModel']).lower() != os.path.normpath(pathConfig).lower():
        if not subModel:
            # Get the regionName
            (thisPath,regionName) = os.path.split(pathConfig)
            regionPath = pathConfig
        else:
            # Get the regionName and submodelName
            (thisPath,submodelName) = os.path.split(pathConfig)
            regionPath = os.path.split(thisPath)[0]
            regionName = os.path.split(regionPath)[1]
            thisPath = os.path.split(regionPath)[0]

        # Determine if there is an optional subdirectory
        if os.path.split(thisPath)[1]!='Config':
            optionalSubDir = os.path.split(thisPath)[1]
        else:
            optionalSubDir = False

        # Get the input database full path
        inputDB = os.path.join(regionPath.replace('\\config\\','\\prep\\').replace('\\Config\\','\\Prep\\'),regionName+'_model_prep.gdb')

        # Print this:
        if not subModel:
            display(HTML('This is the <strong>%s</strong> notebook for the <strong>%s</strong> region'%\
                     (notebook,regionName)))
        else:
            display(HTML(('This is the <strong>%s</strong> notebook for the <strong>%s</strong> region '+\
                    'submodel <strong>%s</strong>')%(notebook,regionName,submodelName)))

        # Return
        if not subModel:
            return inputDB, pathConfig, regionName, optionalSubDir
        else:
            return inputDB, pathConfig, regionName, submodelName, optionalSubDir
    else:
        display(HTML('The <strong>%s</strong> notebook is initialized'%(notebook)))

        return pathConfig #returns the model top-level directory
