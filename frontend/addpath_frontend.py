def addpath_frontend(dirFrontend):
    import sys, os, glob

    sys.path.append(dirFrontend) #add the current directory

    #Add the preparation and import folders recursively so those scripts can be called with arguments
    subDirList = ['preparation','import']
    for subDir in subDirList:
        thisDirs = glob.glob(dirFrontend+'\\'+subDir+'\\**\\',recursive=True)
        [sys.path.append(thisDir) for thisDir in thisDirs]

    #Add the helper folder non-recursively
    dirList = ['helper']
    for thisDir in dirList:
        sys.path.append(os.path.join(dirFrontend,thisDir))
