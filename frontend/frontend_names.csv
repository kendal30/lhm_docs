# The names listed below are those used within LHM's Frontend. The variable name is the first column 
# the second column is the folder filename extension or field name used.
# This file is read by lhm_frontend.names_frontend

# LHM-standard directories
dirWorking, Import_Working
dirWaterUse, Water_Use
dirLanduse, LU_Clipped_Reclass
dirLAI, LAI_Clipped
dirLAIBackup, LAI_Clipped_Backup
dirObsPrep, Observations_Prep
dirModelLayers, Model_Layers
dirSubmodelLayers, Submodel_Layers

dirObsET, ET
dirObsSnowCover, Snow_Cover

dirSubLanduse, Landuse
dirRoughness, Roughness
dirFlowtimes, Flowtimes
dirImperv, Impervious

dirIrrigTech, Irrigation_technology
dirIrrigLim, Irrigation_limits
dirIrrigFrac, Irrigation_gw_frac
dirIrrigLeak, Irrigation_canal_leakage
dirIrrigEvap, Irrigation_canal_evap
dirIrrigPODID, Irrigation_sw_POD_id
dirIrrigPODShed, Irrigation_sw_POD_shed
dirIrrigActive, Irrigation_active

dirObs, Observations
dirSurface, Surface
dirGndwater, Groundwater

# Names of the working geodatabases
gdbWetlands, NWI_merge_parse.gdb
gdbHydro, NHD_merge.gdb
gdbSoils, lhm_soils.gdb

# Properties of the notebook config file
nbFile, notebook.yml
nbSectionFrontend, frontend
nbFrontend, pathFrontend
nbCode, pathCode
nbModel, pathModel

# Properties of the sources config file
srcFile, sources.yml
srcSectionModel, model
srcSectionSurface, surface
srcSectionGndwater, groundwater
srcSectionWaterUse, water_use
srcSectionObs, observations

srcRegion, region_name
srcSubmodel, submodel_name
srcBdry, boundary
srcBdryBuff, boundary_buffered
srcStartHeads, starting_heads
srcDEM, dem
srcStates, states
srcWetlands, wetlands
srcWetlandsRaw, wetlands_raw
srcLanduse, landuse
srcImperv, impervious
srcIncorp, incorporated_boundaries
srcRoads, roads
srcSoils, soils
srcSoilsRaw, soils_raw

srcFlowaccBasins, flowaccum_basins
srcHydroPolys, hydro_polys
srcHydroLines, hydro_lines
srcHydroIrrigCanal, hydro_irrig_canals
srcHydroDrainDitch, hydro_drain_ditches
srcHydroReservoir, hydro_reservoirs
srcHydroRaw, hydro_raw
srcHydroGeom, hydro_geometry
srcHydroFlow, hydro_flow

srcLakesDepth, lakes_depth
srcWetlandsDepth, wetlands_depth

srcIrrigTech, irrigation_technology
srcIrrigLimit, irrigation_limits
srcIrrigFrac, irrigation_gw_frac
srcIrrigLeak, irrigation_canal_leakage
srcIrrigEvap, irrigation_canal_evap
srcIrrigPOD, irrigation_sw_POD_id
srcIrrigShed, irrigation_sw_POD_shed
srcIrrigActive, irrigation_active

srcTop, top
srcBottom, bottom
srcIbound, ibound
srcGndStartHeads, gnd_start_heads
srcHorizCond, horizontal_conductivity
srcVertAni, vertical_anisotropy
srcVertCond, vertical_conductivity
srcSpecYield, specific_yield
srcSpecStor, specific_storage
srcHorizCondDeep, deep_horizontal_conductivity
srcVertAniDeep, deep_vertical_anisotropy
srcSpecYieldDeep, deep_specific_yield
srcSpecStorDeep, deep_specific_storage
srcBdryHeads, constant_heads
srcEvtExtDepth, evt_exctinction_depth

srcDrnPres, drn_presence
srcDrnElev, drn_elevation
srcDrnBedK, drn_bed_conductivity
srcDrnBedCond, drn_bed_conductance
srcDrnBedThick, drn_bed_thickness
srcDrnWidth, drn_width
srcDrnExcMask, drn_exclusion_mask

srcRivPres, riv_presence
srcRivElev, riv_elevation
srcRivDepth, riv_bottom_depth
srcRivBedK, riv_bed_conductivity
srcRivBedCond, riv_bed_conductance
srcRivBedThick, riv_bed_thickness
srcRivExcMask, riv_exclusion_mask

srcGhbPres, ghb_presence
srcGhbElev, ghb_elevation
srcGhbCond, ghb_conductance
srcGhbExcMask, ghb_exclusion_mask

srcChdPres, chd_presence
srcChdElev, chd_elevation

# Preparation names
prepBdry, boundary_surface
prepBdryBuff, boundary_surface_buffer
prepStartHeads, prep_start_heads
prepInterpWells, prep_interpolation_wells

prepDEMRaw, prep_dem_raw
prepDEMRawDir, prep_dem_raw_flowdir
prepDEMAspect, prep_dem_aspect
prepDEMFill, prep_dem_fill
prepDEMFillAccum, prep_dem_fill_accum
prepDEMFillDir, prep_dem_fill_flowdir
prepDEMFillSlope, prep_dem_fill_slope
prepDEMWet, prep_dem_wetland
prepDEMUp, prep_dem_upland

prepInternDrain, prep_intern_drain_grid
prepInternBasin, prep_intern_drain_basin_id
prepInternSink, prep_intern_drain_sink_id

prepSelGauges, prep_select_gauges
prepUserGauges, prep_user_gauges
prepCombGauges, prep_combine_gauges

prepWetDepth, prep_wetland_depth
prepWetPres, prep_wetland_present
prepWetComp, prep_wetland_complex

prepConnBuff, prep_hydro_conn_buff_poly
prepConnLines, prep_hydro_lines_conn
prepConnPolys, prep_hydro_polys_conn

prepStrFrac, prep_stream_fraction
prepStrDepth, prep_stream_depth
prepStrWidth, prep_stream_width
prepStrRadius, prep_stream_radius
prepStrLength, prep_stream_length
prepStrVel, prep_stream_velocity
prepStrPres, prep_stream_present
prepStrSin, prep_stream_sinuosity
prepStrElev, prep_stream_elev

prepRunWsheds, prep_runoff_watersheds
prepRunDSWsheds, prep_runoff_downstream_watersheds
prepRunGauges, prep_runoff_gauges
prepRunSubTime, prep_runoff_subsurface_flowtimes
prepRunOverLength, prep_runoff_overland_flowlength
prepRunOverSlope, prep_runoff_overland_flowslope

# Specify standard feature field names
fieldGaugeIDNum, Site_Num

# Name prefixes
prefixLanduse,lhm_reclass_
prefixMannings, mannings_
prefixFlowtime, flowtime_
prefixImperv, clip_
prefixImpervType, impervtype_
prefixPercent, impervpercent_

# General properties
outExtRaster, .tif
outExtTable, .csv
outExtFeature, .shp

# Model surface grid names
modSurfGrid, model_grid
modSurfLatGrid, latitude
modSurfLonGrid, longitude

modSurfRunWshed, runoff_watershed
modSurfRunDSWshed, runoff_downstream_watershed
modSurfRunGauges, runoff_gauges
modSurfRunOverFlowlen, runoff_overland_flowlength
modSurfRunOverFlowslope, runoff_overland_flowslope
modSurfRunIntBasin, runoff_internal_basin
modSurfRunIntSink, runoff_internal_sink
modSubRunTime, runoff_subsurface_flowtimes
modSurfStrVel, stream_velocity
modSurfStrDepth, stream_bottom_depth
modSurfStrFrac, fraction_stream


# Model groundwater grid names
modGndBot, bottom
modGndTop, model_top
modGndStart, start_heads
modGndIbound, ibound
modGndIboundFeature, ibound_grid.shp

modGndSpecYield, spec_yield
modGndSpecStor, spec_stor
modGndHorizCond, hk
modGndVertAni, vani
modGndVertCond, vk

modGndETDepth, et_extinct_depth
modGndETSurface, et_surf

modGndDrnPresence, drains
modGndDrnElev, drains_elev
modGndDrnCond, drains_cond

modGndRivPresence, rivers
modGndRivElev, rivers_elev
modGndRivCond, rivers_cond
modGndRivBot, rivers_bot

modGndGhbPres, ghb
modGndGhbElev, ghb_elev
modGndGhbCond, ghb_cond

modGndChdPres, chd
modGndChdElev, chd_elev

# Set observation names
obsCsvUSGS, usgs_gauge_flow.csv
obsCsvUser, user_gauge_flow.csv
obsCsvDrinking, drinking_water_wells.csv

obsFieldPoints, point
obsFieldStations, station
obsFieldZones, zone
obsFieldGrids, grid

obsFeatDrinking, obs_wells_cleaned

obsWshedsUSGS, obs_watersheds_USGS
obsWshedsUSER, obs_watersheds_USER