"""
This module is used to download and process MODIS products using LHM.
"""
import lhm_frontend

'''
--------------------------------------------------------------------------------
MODIS Data Products
--------------------------------------------------------------------------------
'''
def select_products(config):
    """This function provides the necessary data source structures for GEE, given a datatype
    and date range. It provides best-available products (as opposed to consistent ones).
    """

    import pandas, datetime, numpy

    # Create a table in Pandas for each dataset
    dfLai1 = pandas.DataFrame({'type':'Lai','product':'MOD15A2H','source':'MODIS/006/MOD15A2H',\
                                    'band':'Lai_500m','scale':500,'CRS':'EPSG:4326',\
                                    'units':'','multiplier':0.1,\
                                    'year':numpy.arange(2000,2002+1),\
                                    'startDate':pandas.to_datetime('2000-02-01'),\
                                    'endDate':pandas.to_datetime('2002-07-03')})
    dfLai2 = pandas.DataFrame({'type':'Lai','product':'MCD15A3H','source':'MODIS/006/MCD15A3H',\
                                    'band':'Lai','scale':500,'CRS':'EPSG:4326',\
                                    'units':'','multiplier':0.1,\
                                    'year':numpy.arange(2002,datetime.datetime.today().year+1),\
                                    'startDate':pandas.to_datetime('2002-07-04'),\
                                    'endDate':pandas.to_datetime(datetime.datetime.today().strftime('%Y-%m-%d'))})

    # Snow cover
    dfSnowCover = pandas.DataFrame({'type':'Snow Cover','product':'MOD10A1','source':'MODIS/006/MOD10A1',\
                                    'band':'NDSI_Snow_Cover','scale':500,'CRS':'EPSG:4326',\
                                    'units':'','multiplier':1,\
                                    'year':numpy.arange(2000,datetime.datetime.today().year+1),\
                                    'startDate':pandas.to_datetime('2000-02-24'),\
                                    'endDate':pandas.to_datetime(datetime.datetime.today().strftime('%Y-%m-%d'))})
    
    # ET
    dfET = pandas.DataFrame({'type':'ET','product':'MOD16A2','source':'MODIS/006/MOD16A2',\
                                    'band':'ET','scale':500,'CRS':'EPSG:4326',\
                                    'units':'kg/m2/8-day','multiplier':0.1,\
                                    'year':numpy.arange(2001,datetime.datetime.today().year+1),\
                                    'startDate':pandas.to_datetime('2001-01-01'),\
                                    'endDate':pandas.to_datetime(datetime.datetime.today().strftime('%Y-%m-%d'))})

    # Be sure to add the new dataset in this list
    dfProducts = pandas.concat([dfLai1,dfLai2,dfSnowCover,dfET],axis=0,ignore_index=True)

    # Alter the date column in order to set dates within bounds
    dfProducts['startDate'] = numpy.maximum(dfProducts['startDate'],pandas.to_datetime(dfProducts['year'],format='%Y'))
    dfProducts['endDate'] = numpy.minimum(dfProducts['endDate'],pandas.to_datetime(dfProducts['year'].astype(str)+'-12-31',format='%Y-%m-%d'))


    # Now, select the product and date range requested
    selStart = pandas.to_datetime(config['startDate'])
    selEnd = pandas.to_datetime(config['endDate'])
    indDate = numpy.logical_and(dfProducts['startDate']<=selEnd,dfProducts['endDate']>=selStart)
    dfSelect = dfProducts.loc[numpy.logical_and(indDate,dfProducts['type'].str.lower()==config['type'].lower())].copy()

    # Modify the start and end dates as needed to match our selection
    dfSelect['startDate'] = numpy.maximum(dfSelect['startDate'],selStart.to_datetime64())
    dfSelect['endDate'] = numpy.minimum(dfSelect['endDate'],selEnd.to_datetime64())

    # Sort by start date
    dfSelect = dfSelect.sort_values(by='startDate',axis=0)

    # Reset the index
    dfSelect.reset_index(inplace=True, drop=True)
    return dfSelect

'''
--------------------------------------------------------------------------------
Google Earth Engine routines
--------------------------------------------------------------------------------
'''
def ee_authenticate():
    """This function will interact with the user to get them set up with google earth engine"""
    import ee
    ee.Authenticate()

def ee_fetch_modis(inputDB, pathConfig, config=dict(), lhm=True):
    """This function uses Google Earth Engine to retrieve MODIS data

    The input config file is a dictionary that needs fields 'type', 'startDate', 
    'endDate', and 'outDir'. For lhm=True, 'outDir' is treated as a subdirectory
    of the main prep folder.

    Options for 'type' are currently 'lai', 'et', and 'snow depth' (capitalization ignored).
    Both the start and end dates should be formatted like %Y-%m-%d

    It is designed to be run within LHM, but should be usable outside of it. Just be
    sure to include:
      - a field 'box' with a string as the feature name to use, or a
        list of format [left, right, top, bot] in Lat/Lon coordinates.
    
    You can use 'optClip' to specify a clipping feature, if you'd like.

    'numThreads' gives you the ability to customize the number of download threads. Default is 3.

    """

    import ee, re, arcpy, numpy, requests, zipfile, os, shutil, glob, concurrent, pandas, time
    from tqdm.notebook import tqdm

    # Handle configuration, include defaults, override with user-specific values
    userConfig = config
    config = {'optClip':False,'numThreads':3}
    for key in userConfig.keys():
        config[key] = userConfig[key]

    # Get the dfProducts dataframe for the specified data type and time range
    dfProduct = select_products(config)

    # Fix the coordinates
    dataCRS = dfProduct.iloc[0]['CRS']

    if lhm:
        # Get standard names
        [outExt,inputBdryName] = lhm_frontend.names_frontend(['outExtRaster','prepBdryBuff'])

        # Standardized inputs
        inBoxFeature = inputDB + '/' + inputBdryName

        # For LHM, the output directory is a subdirectory of the main preep folder
        outDir = os.path.join(os.path.split(inputDB)[0],config['outDir'])

        # Make the directory(ies) if not already there
        if not os.path.exists(outDir):
            os.makedirs(outDir)
    else:
        # Process the box
        if isinstance(config['box'],str):
            inBoxFeature = config['box']
        elif isinstance(config['box'],list):
            # Request region (rectangular in lat/lon)
            reqLeft = config['box'][0]
            reqRight = config['box'][1]
            reqTop = config['box'][2]
            reqBot = config['box'][3]

        # Get the output directory
        outDir = config['outDir']

        # Output extension
        outExt = '.tif'

    # Specify temporary names
    tempFilename = 'temp_tile_'
    tempBoxFeature = 'temp_box_feature.shp'
    tempClipRaster = 'temp_clip_raster.tif'

    #------------------------------------------------------------------------------
    # Set up the environment
    #------------------------------------------------------------------------------
    # Initialize Earth Engine
    ee.Initialize()

    # Set up the simultaneous processing executor
    executor = concurrent.futures.ThreadPoolExecutor(max_workers = config['numThreads'])

    # Set up the ArcPy environment
    arcpy.env.workspace = outDir
    arcpy.env.compression = 'LZ77'
    arcpy.env.overwriteOutput = True

    #------------------------------------------------------------------------------
    # Helper Functions
    #------------------------------------------------------------------------------
    def download_file(r,URI):
        with open(URI, 'wb') as fd:
            for chunk in r.iter_content(chunk_size=128):
                fd.write(chunk)

    def fetch_URL(image,scale,CRS,box):
        argDict = {'crs':CRS,'region':box}
        if scale:
            argDict['scale'] = scale
        path = image.getDownloadUrl(argDict)
        return path

    #------------------------------------------------------------------------------
    # Set up processing
    #------------------------------------------------------------------------------
    # Set up the bounding box feature
    # Extract the boundary of the clip object
    if inBoxFeature:
        # Reproject to match GEE coordinate system
        sr = arcpy.SpatialReference(int(re.findall('EPSG:([0-9]+)',dataCRS)[0]))
        result = arcpy.Project_management(inBoxFeature,tempBoxFeature, sr)

        # Get the box
        reqBox = arcpy.Describe(tempBoxFeature).extent
        reqLeft = reqBox.XMin
        reqRight = reqBox.XMax
        reqTop = reqBox.YMax
        reqBot = reqBox.YMin

    # Create the clip polygon for GEE
    clipBox = ee.Geometry.Polygon([[reqLeft, reqBot], [reqRight, reqBot], \
                                    [reqRight, reqTop], [reqLeft, reqTop], [reqLeft, reqBot]]);

    # Define the map function
    def clip_img(img):
        return img.clip(clipBox)

    firstYear = True
    for (thisInd,thisYear) in enumerate(tqdm(dfProduct['year'].values,desc='Downloading Years')):
        # Get variables from the dfProduct dataframe
        startDate = dfProduct.loc[thisInd,'startDate'].strftime('%Y-%m-%d')
        endDate = dfProduct.loc[thisInd,'endDate'].strftime('%Y-%m-%d')
        dataProduct = dfProduct.loc[thisInd,'product']
        dataSource = dfProduct.loc[thisInd,'source']
        dataBand = dfProduct.loc[thisInd,'band']
        dataScale = int(dfProduct.loc[thisInd,'scale'])

        # Initialize the image object
        thisCollection = ee.ImageCollection(dataSource).select(dataBand).filterDate(startDate, endDate).map(clip_img);

        # Create the output directory if it doesn't already exist
        thisOutDir = os.path.join(outDir,str(thisYear))
        if not os.path.exists(thisOutDir):
            os.mkdir(thisOutDir)

        # Convert to a list
        thisList = thisCollection.toList(100)
        numImg = thisList.length().getInfo()

        #--------------------------------------------------------------------------
        # Determine how many tiles we need to divide our request into
        if firstYear:
            try:
                fetch_URL(ee.Image(thisList.get(0)),dataScale,dataCRS,clipBox)
                # This worked without errors, we need only 1 download tile
                numTiles = 1
            except ee.ee_exception.EEException as e:
                # Get the error message
                message = e.args[0]

                # Parse the error to get numbers
                messageNums = re.findall('[0-9]+',message)
                reqSize = float(messageNums[0])
                maxSize = float(messageNums[1])

                # Determine the number of tiles we'll need, make them nowhere near the maximum size
                numTiles = int(numpy.ceil(reqSize/maxSize)*2) # multiply by 2 to make smaller tiles, too many download errors with big tiles

            # Set to false so we don't revisit
            firstYear = False

            # Create tiles, very slightly overlap in order to gaurantee good mosaic
            tileEdges = numpy.concatenate((numpy.arange(reqLeft,reqRight,(reqRight-reqLeft)/numTiles),[reqRight]),axis=0)
            tileEdgeLeft = numpy.concatenate(([tileEdges[0]],tileEdges[1:numTiles]-0.001),axis=0)
            tileEdgeRight = numpy.concatenate((tileEdges[1:numTiles]+0.001,[tileEdges[numTiles]]),axis=0)

            # Create the list of clip polygons
            clipBoxes = [ee.Geometry.Polygon([[tileEdgeLeft[n], reqBot], [tileEdgeRight[n], reqBot],\
                                              [tileEdgeRight[n], reqTop], [tileEdgeLeft[n], reqTop], \
                                              [tileEdgeLeft[n], reqBot]]) for n in range(0,numTiles)]

        #--------------------------------------------------------------------------
        # Now, iterate over the list, getting download URLs
        index = pandas.MultiIndex.from_product([list(range(0,numImg)),list(range(0,numTiles))],names=['img','tile'])
        dfThis = pandas.DataFrame(columns=['url','date'],index=index)
        for m in range(0,numImg):
            thisImg = ee.Image(thisList.get(m))
            for n in range(0,numTiles):
                dfThis.loc[(m,n),'url'] = fetch_URL(thisImg,dataScale,dataCRS,clipBoxes[n])
                dfThis.loc[(m,n),'date'] = thisImg.getInfo()['properties']['system:index']
        dfThis.reset_index(inplace=True)

        #--------------------------------------------------------------------------
        # Define the worker thread that will fetch our data
        def worker(numImg, prevTries = 0):
            # Subset the dataframe of URLs for this year
            dfThisImg = dfThis.loc[dfThis['img']==numImg,:].copy()

            # Check to see if the output local path already exists, if not, continue
            finalName = '%s.%s.%s.%dm%s'%(dataProduct,dfThisImg['date'].iloc[0],dataBand,dataScale,outExt)
            finalRaster = os.path.join(thisOutDir,finalName)

            mosaicList = [] # keep track of rasters to mosaic
            dirList = [] # keep track of tiles to delete
            for n in range(0,numTiles):
                thisURL = dfThisImg['url'].iloc[n]

                # Download file if it's not already there
                if not os.path.exists(finalRaster):
                    thisDir = os.path.join(outDir,'img_%d_tile_%d'%(numImg,n))
                    dirList.append(thisDir) # keep track of these, will clean up in a moment

                    # Try to get the remote resource, try a few times
                    proceed = False
                    tries = 0
                    while not proceed:
                        # Get the remote resource
                        rFile = requests.get(thisURL)
                        # Determine if success
                        if rFile.status_code==200:
                            proceed = True
                        elif tries < 10:
                            time.sleep(1) # pause for 1 second
                            tries += 1
                        else:
                            return False # Break the function, downloaded didn't succeed

                    # Download the data, store locally
                    if rFile.status_code==200:
                        # Make the directory and download the file
                        os.mkdir(thisDir)
                        fileURI = os.path.join(thisDir,tempFilename+str(n)+'.zip')
                        download_file(rFile,fileURI)

                        # Unzip the file
                        thisZipObj = zipfile.ZipFile(fileURI)
                        thisZipObj.extractall(thisDir)
                        thisZipObj.close()

                        # Get the raster in this directory
                        thisRaster = glob.glob(os.path.join(thisDir,'*%s'%outExt))[0]
                        if n == 0:
                            baseRaster = thisRaster
                        else:
                            mosaicList.append(thisRaster)
                else:
                    return True # already done

            # Copy the base raster to the final location
            result = arcpy.CopyRaster_management(baseRaster,finalRaster)

            # If need be, mosaic tiles
            if numTiles > 1:
                print(mosaicList)
                print(finalRaster)
                result = arcpy.Mosaic_management(mosaicList,finalRaster)

            # Remove the working directories
            [shutil.rmtree(rmDir) for rmDir in dirList]

            # Finally, clip the feature if desired
            if config['optClip']:
                thisClipRaster = 'img_%s_'%numImg + tempClipRaster
                result = arcpy.Rename_management(finalRaster,thisClipRaster)
                result = arcpy.Clip_management(in_raster=os.path.join(thisOutDir,thisClipRaster),\
                                               out_raster=finalRaster,\
                                               in_template_dataset=os.path.join(outDir,tempBoxFeature),\
                                               clipping_geometry='ClippingGeometry',\
                                               maintain_clipping_extent='NO_MAINTAIN_EXTENT')
                result = arcpy.Delete_management(os.path.join(thisOutDir,thisClipRaster))

            # Return final file if downloaded
            return finalName


        #--------------------------------------------------------------------------
        # Execute all download threads,
        threads = [executor.submit(worker, jobNum) for jobNum in range(0,numImg)]
        concurrent.futures.wait(threads)


    # Shutdown the executor
    executor.shutdown()

    # Clean up temp files
    result = arcpy.Delete_management(tempBoxFeature)

    if lhm:
        # Reset the working directory
        os.chdir(pathConfig)
        
        # Check to see if we need to reproject
        batch_reproject(inputDB,outDir)

        # Log the result
        lhm_frontend.log_lhm_output('MODIS %s'%dfProduct['type'][0], outDir, inputDB)


'''
--------------------------------------------------------------------------------
Batch Raster Operations
--------------------------------------------------------------------------------
'''
def batch_reproject(inputDB,subDirIn):
    """This script re-projects LAI data to the model projection"""
    import arcpy, os, tqdm
    from tqdm.notebook import tqdm

    # Get standard names
    [inputBdryName] = lhm_frontend.names_frontend('prepBdryBuff')
    inputBdry = inputDB + '/' + inputBdryName

    # Set up the environment
    arcpy.env.extent = inputBdry
    arcpy.env.outputCoordinateSystem = inputBdry
    arcpy.env.cellSize = ''
    arcpy.env.snapRaster = ''
    arcpy.env.pyramids = ''
    
    # Check out a spatial analyst extension license
    arcpy.CheckOutExtension("Spatial")

    # Determine directories to use
    inputDir = os.path.join(os.path.split(inputDB)[0],subDirIn)

    # Use the walk function to catalog rasters
    rasters = []
    walk = arcpy.da.Walk(inputDir,datatype="RasterDataset")
    for dirpath,dirname,filenames in walk:
        for filename in filenames:
            rasters.append(os.path.join(dirpath,filename))

    # Now loop through and reprocess
    for raster in tqdm(rasters,desc='Reprojecting'):
        # Copy the original raster to get it out of the way
        [origPath,origNameExt] = os.path.split(raster)
        [origName,origExt] = os.path.splitext(origNameExt)
        origRaster = os.path.join(origPath,origName+'_orig'+origExt)
        arcpy.Rename_management(raster,origRaster)

        # Now, use apply environment to reproject
        # print(origNameExt)
        thisApply = arcpy.sa.ApplyEnvironment(origRaster)
        thisApply.save(raster)

        # Delete the origRaster
        arcpy.Delete_management(origRaster)

        # Delete the raster object
        del thisApply

'''
--------------------------------------------------------------------------------
Batch Fill NaN
--------------------------------------------------------------------------------
'''
def batch_fill_nan_LAI(inputDB):
    """This script takes care of NaNs in LAI data, which create localized unrealistic recharge
    values in LHM."""

    import arcpy, os
    from tqdm.notebook import tqdm

    # Standardized inputs
    [inputBdryName,subDirLAI,subDirLAIBackup] = lhm_frontend.names_frontend(['prepBdryBuff','dirLAI','dirLAIBackup'])
    inputBdry = inputDB + '/' + inputBdryName

    # Set up the environment
    arcpy.env.extent = inputBdry
    arcpy.env.outputCoordinateSystem = inputBdry
    arcpy.env.cellSize = ''
    arcpy.env.snapRaster = ''
    arcpy.env.pyramids = ''
    arcpy.env.compression = 'LZ77'
    
    # Check out a spatial analyst extension license
    arcpy.CheckOutExtension("Spatial")

    # Determine directories to use
    inputDir = os.path.join(os.path.split(inputDB)[0],subDirLAI)
    backupDir = os.path.join(os.path.split(inputDir)[0],subDirLAIBackup)

    # Use the walk function to catalog rasters
    rasters = []
    walk = arcpy.da.Walk(inputDir,datatype="RasterDataset")
    for dirpath,dirname,filenames in walk:
        for filename in filenames:
            rasters.append(os.path.join(dirpath,filename))

    # Create the output backup directory, and check to see if rasters already exist there,
    # if so, assume that they have already been processed and remove those rasters from
    # the list to process below
    rasters = [thisRaster for thisRaster in rasters if not os.path.exists(os.path.join(backupDir,os.path.split(thisRaster)[1]))]

    # Now loop through and reprocess
    for raster in tqdm(rasters):
        # Copy the original raster to get it out of the way
        [origPath,origNameExt] = os.path.split(raster)
        [origName,origExt] = os.path.splitext(origNameExt)
        origRaster = os.path.join(origPath,origName+'_orig'+origExt)
        arcpy.Rename_management(raster,origRaster)

        origRasterObject = arcpy.Raster(origRaster)

        # Set value = 254 in LAI dataset to Null, to create mask for Nibble
        rasterMask = arcpy.sa.Con(origRasterObject < 249, origRasterObject)

        # Perform Nibble operation to replace null values with nearest neighbors
        rasterNibble = arcpy.sa.Nibble(origRasterObject, rasterMask, "ALL_VALUES")

        # Save nibbled raster
        rasterNibble.save(raster)

        # Save original raster as backup
        arcpy.Copy_management(origRaster,os.path.join(backupDir,origNameExt))

        # Clean up
        arcpy.Delete_management(origRaster)
        del rasterNibble, rasterMask, origRasterObject
