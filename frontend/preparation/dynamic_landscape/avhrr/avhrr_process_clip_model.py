#This script extracts LAI values for the specified boundary polygon, and writes them out in the coordinate system
#of the polygon, rather than the grids.

#Import modules
import arcgisscripting, os, tarfile, numpy
from shutil import copyfile
from glob import glob

#Specify constants
band = 6
numCols = 4587
numRows = 2889
skip = numCols * numRows * (band - 1)

#Static Paths
gridsLoc = "C:\\Users\\Anthony\\Raw_Data\\AVHRR"
hdrTemplate = "single_band_template.hdr"

prjTemplate = "georeference_template.aux.xml"

boundLoc = 'C:\\Users\\Chris\\ILHM_Inputs\\Boundary'
boundName = 'jordan_data_clip.shp'

outputLocBase = 'C:\\Users\\Chris\\ILHM_Inputs\\NDVI\\AVHRR_Clipped'
stagingLoc = 'C:\\Users\\Chris\\ILHM_Inputs\\NDVI\\StagingLoc'

#Determine full paths to a few components
hdrLoc = os.path.join(gridsLoc,hdrTemplate)
prjLoc = os.path.join(gridsLoc,prjTemplate)
boundShape = os.path.join(boundLoc, boundName)

#Get a complete directory listing of .zip files in the input directory
grids = glob(os.path.join(gridsLoc,'*.gz'))

#Create the geoprocessor object
gp = arcgisscripting.create()
#Check out the spatial analyst extension
gp.CheckOutExtension("Spatial")
#Set the output coordinate system to match the boundary polygon
gp.OutputCoordinateSystem = boundShape

for grid in grids:
    (inPath, inName) = os.path.split(grid)

    #Determine the year of the output
    inYear = inName[2:4]
    if inYear[0] in ['8','9']:
        inYear = '19' + inYear
    else:
        inYear = '20' + inYear
    outputLoc = os.path.join(outputLocBase,inYear)

    #Check to see if a directory for the year has already been created, if not, create it
    if not os.path.exists(outputLoc):
        os.mkdir(outputLoc)

    #Create the output grid name
    currFile = os.path.splitext(os.path.splitext(inName)[0])[0] #there are two file extensions
    outputGrid = os.path.join(outputLoc,currFile)

    #Check to see if the output grid already exists, if not, then continue
    if not os.path.exists(outputGrid):
        #Extract the .img file from the .tar.gz file
        tempTar = tarfile.open(grid,"r:gz")
        tempTar.extract(currFile+".img", stagingLoc)
        tempTar.close()

        #Open the .img file, and read in the Nth band
        fIn = open(os.path.join(stagingLoc,currFile+".img"))
        fIn.seek(skip,0)
        tempImg = numpy.fromfile(file = fIn, dtype = numpy.uint8, count = numRows*numCols)
        fIn.close()

        #Write out the .img file as a .bil file
        inputGrid = os.path.join(stagingLoc,currFile+".bil")
        fOut = open(inputGrid,'w')
        tempImg.tofile(fOut)
        fOut.close()

        #Copy the template .hdr file to a new .hdr file, also the .aux.xml file for projection
        copyfile(hdrLoc,os.path.join(stagingLoc,currFile+".hdr"))
        copyfile(prjLoc,os.path.join(stagingLoc,currFile+".bil.aux.xml"))

        #Clip this BIL grid using the boundary shapefile
        try:
            gp.ExtractByMask_sa(inputGrid,boundShape,outputGrid)
        except:
            gp.AddWarning(gp.GetMessages(2))
            print gp.GetMessages(2)

        #Clean the staging location directory
        cleanList = os.listdir(stagingLoc)
        for clean in cleanList:
            try:
                os.remove(os.path.join(stagingLoc,clean))
            except:
                pass #just move on, and try to delete on the next go-round
