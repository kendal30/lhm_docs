#This script extracts NDVI values for the specified boundary polygon, and writes them out in the coordinate system
#of the polygon, rather than the grids.  It is intended to operate on NDVI data from the AVHRR satellite
#processed by the GIMMS technique

#Import modules
import arcgisscripting, os, string, gzip, numpy

#Static Paths
gridsPath = 'C:\\Users\\Anthony\\Raw_Data\\GIMMS'
gridBaseName = 'data.tif.gz'

boundShape = "C:\\Users\\Chris\\ILHM_Inputs\\Boundary\\jordan_data_clip.shp"

outputLocBase = "C:\\Users\\Chris\\ILHM_Inputs\\NDVI\\GIMMS_Clipped"
stagingLoc = "C:\\Users\\Chris\\ILHM_Inputs\\NDVI\\StagingLoc"

#Build a complete list of input directories
#Get a complete listing of the appropriate .gz files in the input directory
grids = []
[grids.append(os.path.join(root,file)) for root,dirs,files in os.walk(gridsPath) \
    for file in files if file.endswith(gridBaseName)]

#Create the geoprocessor object
gp = arcgisscripting.create()
#Check out the spatial analyst extension
gp.CheckOutExtension("Spatial")
#Set the output coordinate system to match the boundary polygon
gp.OutputCoordinateSystem = boundShape

for grid in grids:
    (inPath, inName) = os.path.split(grid)

    #Determine the year of the output
    inYear = inName[0:2]
    if inYear[0] in ['8','9']:
        inYear = '19' + inYear
    else:
        inYear = '20' + inYear
    outputLoc = os.path.join(outputLocBase,inYear)

    #Check to see if a directory for the year has already been created, if not, create it
    if not os.path.exists(outputLoc):
        os.mkdir(outputLoc)

    #Create the output grid name
    nameComponents = inName.split('.')
    outputGrid = os.path.join(outputLoc,nameComponents[0]) #only want the date
    currImg = os.path.splitext(inName)[0] #just the .tif file

    #Check to see if the output grid already exists, if not, then continue
    if not os.path.exists(outputGrid):
        #Extract the .tif file from the .gz file
        gzFile = gzip.GzipFile(grid)
        content = gzFile.read()
        gzFile.close()

        #Write the .tif file to the staging location
        currFile = os.path.join(stagingLoc,currImg)
        outFile = open(currFile,'wb')
        outFile.write(content)
        outFile.close()

        #Clip this .tif using the boundary shapefile
        try:
            gp.ExtractByMask_sa(currFile,boundShape,outputGrid)
        except:
            gp.AddWarning(gp.GetMessages(2))
            print gp.GetMessages(2)

        #Clean the staging location directory
        cleanList = os.listdir(stagingLoc)
        for clean in cleanList:
            try:
                os.remove(os.path.join(stagingLoc,clean))
            except:
                pass #just move on, and try to delete on the next go-round
