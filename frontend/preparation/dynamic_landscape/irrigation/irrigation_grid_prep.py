def irrigation_grid_prep(inputDB, pathConfig, params=dict(), printResult=True):
    """Irrigation grid preparation routine. This uses standard source data structures
    and creates all the input grids necessary for the model to run, given specified inputs.

    Params are:
        cellSize: cellsize in meters, default is 25
    """

    # Import  modules
    import arcpy, os
    import lhm_frontend, raster
    create_surface_raster = raster.create_surface_raster
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Standardized/Advanced Inputs
    #-------------------------------------------------------------------------------
    [rowTech,rowLimit,rowFrac,rowLeak,\
        rowEvap,rowPOD,rowShed,rowActive,dirBase,dirTech,dirLimit,dirFrac,dirLeak,dirEvap,\
        dirPOD,dirShed,dirActive,outExtension] = lhm_frontend.names_frontend([\
        'srcIrrigTech','srcIrrigLimit',\
        'srcIrrigFrac','srcIrrigLeak','srcIrrigEvap','srcIrrigPOD','srcIrrigShed','srcIrrigActive',\
        'dirWaterUse','dirIrrigTech','dirIrrigLim','dirIrrigFrac','dirIrrigLeak',\
        'dirIrrigEvap','dirIrrigPODID','dirIrrigPODShed','dirIrrigActive','outExtRaster'])

    # Get the model input locations
    (inputSources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig,section='WaterUse')

    # Specify names for constant rasters
    nameConstTech = 'tech_'
    nameConstLimit = 'limit_'
    nameConstFrac = 'gwFrac_'
    nameConstLeak = 'leakFrac_'
    nameConstEvap = 'evapFrac_'
    nameConstPOD = 'POD_'
    nameConstShed = 'PODshed_'
    nameConstActive = 'active_'

    # Specify prefixes
    prefix = 'clip_'

    # Specify the output directory
    outputDir = os.path.join(os.path.split(inputDB)[0],dirBase)

    datasets = {rowTech:{'dir':dirTech,'nameConst':nameConstTech},\
                rowLimit:{'dir':dirLimit,'nameConst':nameConstLimit},\
                rowFrac:{'dir':dirFrac,'nameConst':nameConstFrac},\
                rowLeak:{'dir':dirLeak,'nameConst':nameConstLeak},\
                rowEvap:{'dir':dirEvap,'nameConst':nameConstEvap},\
                rowPOD:{'dir':dirPOD,'nameConst':nameConstPOD},\
                rowShed:{'dir':dirShed,'nameConst':nameConstShed},\
                rowActive:{'dir':dirActive,'nameConst':nameConstActive}}


    # Specify output cellsize
    paramDefaults = {'cellSize':25}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Set up the environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],outputDir,inputBdry)

    #-------------------------------------------------------------------------------
    # Helper Functions
    #-------------------------------------------------------------------------------
    def process_dataset(source,outDir,namePrefix,nameConst=None):
        if source['method'] in ['features','grid']:
            listData = source['data']
            for thisData in listData:
                # Get the name of this irrigation grid
                if source['method'] == 'grid' and source['fieldVal'] != '':
                    strVal = '%s'%source['fieldVal']
                    outFile = os.path.join(outDir,namePrefix+nameConst+strVal+outExtension)
                else:
                    nameData = os.path.splitext(os.path.split(thisData)[1])[0]
                    outFile = os.path.join(outDir,namePrefix+nameData+outExtension)

                if not arcpy.Exists(outFile):
                    # Get this source
                    thisSource = {'method':source['method'], 'fieldVal':source['fieldVal'],\
                        'data':thisData}

                    # Run the array creation routine
                    create_surface_raster(thisSource,outFile)

        elif source['method'] in ['constant']:
            strVal = '%s'%source['data']
            outFile = os.path.join(outDir,nameConst+strVal+outExtension)

            # Run the array creation routine
            create_surface_raster(source,outFile)

    #-------------------------------------------------------------------------------
    # Do the processing
    #-------------------------------------------------------------------------------
    for thisDataset in datasets.keys():
        if thisDataset in inputSources.keys():
            thisInputSource = inputSources[thisDataset]
            thisOutDir = datasets[thisDataset]['dir']
            thisConstName = datasets[thisDataset]['nameConst']
            process_dataset(thisInputSource,thisOutDir,prefix,thisConstName)

            # Log this
            lhm_frontend.log_lhm_output(thisDataset,thisOutDir,inputDB,printResult)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Finished processing irrigation datasets for the <strong>%s</strong> region'%regionName))
