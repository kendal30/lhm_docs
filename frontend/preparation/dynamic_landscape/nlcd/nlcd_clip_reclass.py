def nlcd_clip_reclass(inputDB, pathConfig, printResult=True):
    # This script clips land use and impervious grids from NLCD to a model boundary grid,
    # it then reclassifies the NLCD cells into LHM classes, and manning's roughness grids
    # modified from an earlier script written by Sherry Martin

    # Import system modules
    import arcpy, os
    import lhm_frontend
    from compression import compress_file_raster
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Standardized/Advanced Inputs
    #-------------------------------------------------------------------------------
    # Model geodatabase containing feature file named 'boundary_surface_buffer'
    [configNotebook,frontendSection,rowFrontend,rowLU,rowImperv,\
        dirLU,subdirLU,subdirRough,subdirImperv,prefixLU,prefixMannings,prefixImperv,outExt] = \
        lhm_frontend.names_frontend(['nbFile','nbSectionFrontend','nbFrontend',\
        'srcLanduse','srcImperv','dirLanduse','dirSubLanduse','dirRoughness','dirImperv','prefixLanduse',\
        'prefixMannings','prefixImperv','outExtRaster'])

    # Get the standard inputs
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Specify the output directory
    outputDir = os.path.join(os.path.split(inputDB)[0],dirLU)
    outDirLU = os.path.join(outputDir,subdirLU)
    outDirRough = os.path.join(outputDir,subdirRough)
    outDirImperv = os.path.join(outputDir,subdirImperv)

    # Specify NLCD-specific stuff tables
    tableLU = 'reclass_nlcd_ilhm.txt'
    tableMannings = 'reclass_nlcd_mannings.txt'
    floatFactor = 1000 # to convert from integer to float
    tableSubDir = '/preparation/dynamic_landscape/nlcd/' # relative to the frontend LHM directory

    #-------------------------------------------------------------------------------
    # Set up the environment
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment('',outputDir,inputBdry)

    # Get notebook paths
    notebookDict = lhm_frontend.read_config_dict(os.path.join(pathConfig,configNotebook),frontendSection)
    pathFrontend = notebookDict[rowFrontend]

    # Split the list of LU and impervious grids, if needed
    listLU = inputPaths[rowLU]
    listImperv = inputPaths[rowImperv]

    # Add the path to the reclass tables
    tableLU = pathFrontend+tableSubDir+tableLU
    tableMannings = pathFrontend+tableSubDir+tableMannings

    #-------------------------------------------------------------------------------
    # Do the processing
    #-------------------------------------------------------------------------------
    # First, process LU
    for LU in listLU:
        # Get the name of this LU grid
        nameLU = os.path.splitext(os.path.split(LU)[1])[0]

        # Set the environment settings
        arcpy.env.cellSize = arcpy.Describe(LU).MeanCellHeight

        # Execute ExtractByMask
        tempClip = arcpy.sa.ExtractByMask(LU,inputBdry)

        # Reclassify
        thisReclassLU = arcpy.sa.ReclassByTable(tempClip,tableLU,'FROM','TO','OUT')
        thisReclassMannings = arcpy.sa.ReclassByTable(tempClip,tableMannings,'FROM','TO','OUT')

        # Convert mannings to float, and divide by floatFactor
        thisFloatMannings = arcpy.sa.Float(thisReclassMannings)/floatFactor

        # Save
        thisReclassLU.save(os.path.join(outDirLU,prefixLU+nameLU+outExt))
        thisFloatMannings.save(os.path.join(outDirRough,prefixMannings+nameLU+outExt))

        # Log this
        lhm_frontend.log_lhm_output(prefixLU+nameLU+outExt, outDirLU, inputDB)
        lhm_frontend.log_lhm_output(prefixMannings+nameLU+outExt, outDirRough, inputDB)

        # Cleanup
        del thisReclassLU, thisReclassMannings, thisFloatMannings, tempClip
        compress_file_raster(os.path.join(outDirLU,prefixLU+nameLU+outExt))
        compress_file_raster(os.path.join(outDirRough,prefixMannings+nameLU+outExt))

    # Then, process impervious
    for imperv in listImperv:
        # Get the name of this impervious grid
        nameImperv = os.path.splitext(os.path.split(imperv)[1])[0]

        # Set the environment settings
        arcpy.env.cellSize = arcpy.Describe(imperv).MeanCellHeight

        # Execute ExtractByMask
        tempClip = arcpy.sa.ExtractByMask(imperv,inputBdry)

        # Save
        tempClip.save(os.path.join(outDirImperv,prefixImperv+nameImperv+outExt))

        # Log this
        lhm_frontend.log_lhm_output(prefixImperv+nameImperv+outExt, outDirImperv, inputDB)

        # Cleanup
        del tempClip
        compress_file_raster(os.path.join(outDirImperv,prefixImperv+nameImperv+outExt))

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('NLCD Landuse and Impervious data have finished processing for the <strong>%s</strong> region'%regionName))
