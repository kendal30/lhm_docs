import os, lhm_frontend

'''
------------------------------------------------------------------------------
 Channel routing
------------------------------------------------------------------------------
'''
def overland_channel_routing(inputDB, pathConfig, params=dict(), printResult=True):
    """This script creates all grids necessary for LHM runoff calculations.

    Params are:
        slopeMinVal: minimum value of slope used for runoff calculations (m/m), default is 0.0001
        roughMinVal: minimum roughness value allowed, default is 0.025
        lengthMinVal: minimum overland flow length, in projection units, default is 10
        sepFlowtimes: True/False, specifying whether output runoff flowtimes are to downstream gauge (False), or 
            for upland cells to the stream, and for the stream to the downstream gauge (True), default is False
        maxProcesses: the maximum number of processes to use for tiled flowlength calculations, the best value is an
            integer divisor of your number of tiles, default is 3
        tileBufferCells: the number of cells to buffer the individual flowaccumualtion tiles, default is 100
    """

    # Import modules
    import numpy
    import compression
    from raster import create_surface_raster, read_raster, write_raster
    from gis import tile_flowlength
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Script defaults, shouldn't need to change these
    #-------------------------------------------------------------------------------
    (prefixMannings,prefixFlowtime,nameCombine,fieldGaugeID,\
        dirLanduse,dirRoughness,dirFlowtimes,
        nameFlowdir,nameFlowslope,nameRadius,nameStreams,nameSinuosity,\
        outWatersheds,outDSWatersheds,outGaugeRaster,outOverFlowlength,outOverFlowslope,
        rowFlowaccTiles) = \
        lhm_frontend.names_frontend(['prefixMannings','prefixFlowtime',\
        'prepCombGauges','fieldGaugeIDNum','dirLanduse','dirRoughness','dirFlowtimes',\
        'prepDEMFillDir','prepDEMFillSlope','prepStrRadius','prepStrPres','prepStrSin',\
        'prepRunWsheds','prepRunDSWsheds','prepRunGauges','prepRunOverLength','prepRunOverSlope',\
        'srcFlowaccBasins'])

    # Get the standard input locations
    (inputSources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Get the full paths for these
    inputBase = inputDB + '/%s'
    inFlowdir = inputBase%nameFlowdir
    inFlowslope = inputBase%nameFlowslope
    inRadius = inputBase%nameRadius
    inStreams = inputBase%nameStreams
    inSinuosity = inputBase%nameSinuosity
    inGaugeFeature = inputBase%nameCombine

    # Parse the input params structure
    paramDefaults = {'sepFlowtimes':False, 'slopeMinVal':0.0001, 'roughMinVal':0.025, \
        'lengthMinVal':10, 'maxProcesses':3, 'tileBufferCells':100}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Specify temporary names
    tempRoughRescale = 'temp_roughscale'
    tempFlowtimeStr = 'temp_flowtime_str'
    tempFocalMax = 'temp_focal_max'
    tempFlowtime = 'temp_flowtime'
    tempFlowlength = 'temp_flowlength'
    tempWatersheds = 'temp_watersheds'
    tempFlowaccTiles = 'temp_flowacc_tiles'
    tempTilesClip = 'temp_tiles_clip'
    tempFlowdirGauge = 'temp_flowdir_gauge'
    tempFlowdirStr = 'temp_flowdir_str'
    tempFlowdirStrGauge = 'temp_flowdir_str_gauge'
    tempCost = 'temp_cost'
    tempUpCost = 'temp_upland_cost'
    tempStrCost = 'temp_stream_cost'
    tempWeightSlope = 'temp_weight_slope'
    tempWeightDist = 'temp_weight_dist'

    #-------------------------------------------------------------------------------
    # Set up the geoprocessing environment and get layer names
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment(inFlowdir,inputDB,inputBdry)

    # Get the cellSize
    cellSize = float(arcpy.env.cellSize)
    cellDistAvg = cellSize * 1.20711

    # Full path to roughness grid
    inDirRoughness = os.path.join(os.path.split(inputDB)[0], dirLanduse+'/'+dirRoughness)

    # Specify the output directory for the flowtimes
    outDirFlowtimes = os.path.join(os.path.split(inputDB)[0], dirLanduse+'/'+dirFlowtimes)

    #%% Processing steps
    #-------------------------------------------------------------------------------
    # Pre-prep for all steps
    #-------------------------------------------------------------------------------
    # Prepare the gauges and their watersheds
    # Convert the gauge points to a raster
    arcpy.PointToRaster_conversion(inGaugeFeature,fieldGaugeID,outGaugeRaster,'MAXIMUM','NONE',cellSize)

    # Import these rasters
    rasterStreams = arcpy.Raster(inStreams)
    rasterGauge = arcpy.Raster(outGaugeRaster)

    # Read in the flowaccumulation tiles grid
    create_surface_raster(inputSources[rowFlowaccTiles],tempFlowaccTiles)
    
    # Clip to the surface boundary
    rasterTiles = arcpy.sa.ExtractByMask(tempFlowaccTiles,inputBdry)

    # Convert to integer
    rasterTilesInt = arcpy.sa.Int(rasterTiles)
    rasterTilesInt.save(tempTilesClip)

    # Build the raster attribute table for the tiles
    arcpy.BuildRasterAttributeTable_management(tempTilesClip)

    #------------------------------------------------------------------------------
    # Process flowtimes
    #------------------------------------------------------------------------------
    arcpy.env.workspace = inDirRoughness
    roughnessGrids = arcpy.ListRasters(wild_card = prefixMannings+'*')
    arcpy.env.workspace = inputDB

    calcOnce = False # track whether arrays that need to be calculated only once have been
    for (ind,roughnessGrid) in enumerate(roughnessGrids):
        # Get this roughness grid and the output flowtime name
        roughness = os.path.join(inDirRoughness,roughnessGrid)
        outFlowtime = prefixFlowtime  + roughnessGrid

        # Check to see if the output grid already exists
        if not arcpy.Exists(os.path.join(outDirFlowtimes,outFlowtime)):
            calcFlowtime = True
        else:
            calcFlowtime = False
            print('{0} already exists \n\t delete and rerun if you want to re-calculate it'.format(outFlowtime))

        # Calculate once here for streams, independent of whether this flowtime should be calculated or not
        if calcFlowtime and not calcOnce:
            # Prepare flowdirection raster with gauges, calculate flowtimes
            # Imprint the gauge values on the flowdirection raster as NoData
            rasterFlowdirGauge = arcpy.sa.Con(arcpy.sa.IsNull(outGaugeRaster),inFlowdir)
            rasterFlowdirGauge.save(tempFlowdirGauge)

            # Prepare this here, needed for sepFlowtimes, but also for the MUSLE down below
            # Imprint streams as no data on the flowdir array
            rasterFlowdirStr = arcpy.sa.Con(rasterStreams==0,inFlowdir)
            rasterFlowdirStr.save(tempFlowdirStr)

            # Import these rasters
            rasterRadius = arcpy.Raster(inRadius)
            rasterSinuosity = arcpy.Raster(inSinuosity)
            
            # Here, set all cells except streams to noData
            if params['sepFlowtimes']:
                rasterFlowdirStrGauge = arcpy.sa.Con(rasterStreams==1,rasterFlowdirGauge)
                rasterFlowdirStrGauge.save(tempFlowdirStrGauge)
            else:
                arcpy.CopyRaster_management(tempFlowdirGauge,tempFlowdirStrGauge)
                rasterFlowdirStrGauge = arcpy.Raster(tempFlowdirStrGauge)

            # Make sure that there are no zero slope values, and convert to m/m slope
            rasterSlope = arcpy.Raster(inFlowslope)
            rasterSlopeMin = arcpy.sa.Con(rasterSlope/100<params['slopeMinVal'], params['slopeMinVal'], rasterSlope/100)

            # Rescale roughness grid to match DEM
            arcpy.Resample_management(roughness,tempRoughRescale,cellSize,'BILINEAR')

            # Make sure there are no zero roughness values
            rasterRough = arcpy.Raster(tempRoughRescale)
            rasterRoughMin = arcpy.sa.Con(rasterRough>=params['roughMinVal'], rasterRough, params['roughMinVal'])

            # Only do more calculations if I haven't already calculated these--useful for restarting big runs
            if not arcpy.Exists(tempFlowtimeStr):
                # Calculate the flow cost in streams (See LNW for equation)
                rasterStrCost = rasterRoughMin * (rasterRadius**-0.6666667) * (rasterSlopeMin**-0.5) * rasterSinuosity
                rasterStrCost.save(tempStrCost)

                # Get the in-stream cost only, 0 elsewhere
                rasterCost = arcpy.sa.Con(rasterStreams==0,0,rasterStrCost)
                rasterCost.save(tempCost)

                # Calculate the flowtime from stream cells to the downstream gauge
                # Only calculate this once, doesn't change with roughness grids
                rasterFlowtimeStrOnly = tile_flowlength(arcpy,params,rasterFlowdirStrGauge,rasterTilesInt,rasterCost)
                rasterFlowtimeStrOnly.save(tempFlowtimeStr)

                # Clean up
                del rasterCost, rasterStrCost
            else:
                rasterFlowtimeStrOnly = arcpy.Raster(tempFlowtimeStr)
            
            if not arcpy.Exists(tempFocalMax):
                # Assign 0 values to nulls
                rasterFlowTimeStr01 = arcpy.sa.Con(arcpy.sa.IsNull(rasterFlowtimeStrOnly),0,rasterFlowtimeStrOnly)

                # # Relating gauges to next downstream gauge
                # Calculate focalmax on the streams to pick flowtime to downstream gauge
                rasterFocalMax = arcpy.sa.FocalStatistics(rasterFlowTimeStr01,'RECTANGLE 3 3 CELL','MAXIMUM','DATA')
                rasterFocalMax.save(tempFocalMax)

                # Clean up
                del rasterFlowTimeStr01
            else:
                rasterFocalMax = arcpy.Raster(tempFocalMax)

            calcOnce = True # make sure we don't do this step again

        if calcFlowtime:
            if ind > 0:
                # Rescale roughness grid to match DEM
                arcpy.Resample_management(roughness,tempRoughRescale,cellSize,'BILINEAR')

                # Make sure there are no zero roughness values
                rasterRough = arcpy.Raster(tempRoughRescale)
                rasterRoughMin = arcpy.sa.Con(rasterRough>=params['roughMinVal'], rasterRough, params['roughMinVal'])

            # Calculate the flow cost in uplands (See LNW for equation)
            rasterUpCost = 78.4462 * (cellSize**-0.533) * (rasterRoughMin**0.467) * (rasterSlopeMin**-0.2335)
            rasterUpCost.save(tempUpCost)

            # Calculate flowtime from upland cells to the stream <-- this updates for each LU map
            rasterFlowtimeUpOnly = tile_flowlength(arcpy,params,rasterFlowdirStr,rasterTilesInt,rasterUpCost)

            # Now add upland and wetland together
            if params['sepFlowtimes']:
                rasterFlowtime = arcpy.sa.Con(arcpy.sa.IsNull(rasterFlowtimeUpOnly),rasterFlowtimeStrOnly,rasterFlowtimeUpOnly)
            else:
                rasterFlowtime = arcpy.sa.Con(arcpy.sa.IsNull(rasterFlowtimeUpOnly),rasterFlowtimeStrOnly,rasterFlowtimeUpOnly+rasterFlowtimeStrOnly)

            # Add the focalmax for each gauge to the flowtime grid
            rasterFlowtimeGauge = arcpy.sa.Con(arcpy.sa.IsNull(rasterGauge),rasterFlowtime,\
                arcpy.sa.Con(rasterGauge>0,rasterFocalMax,rasterFlowtime))

            # Now, convert to a 32 bit integer to reduce the size of this output
            arcpy.CopyRaster_management(rasterFlowtimeGauge,os.path.join(outDirFlowtimes,outFlowtime),pixel_type='32_BIT_UNSIGNED')

            # Finally, compress the raster
            compression.compress_file_raster(os.path.join(outDirFlowtimes,outFlowtime))

            # Log this output
            lhm_frontend.log_lhm_output(outFlowtime,outDirFlowtimes,inputDB,printResult)

            # Clean up this loop
            del rasterRough, rasterRoughMin, rasterUpCost, rasterFlowtimeUpOnly, rasterFlowtimeGauge, rasterFlowtime 

    # Clean up this section
    if calcOnce: # if this is false, then this section didn't do anything
        del rasterFlowdirGauge, rasterSinuosity, rasterRadius, rasterSlope, rasterSlopeMin, \
            rasterFlowdirStrGauge, rasterFlowdirStr, rasterFocalMax, rasterFlowtimeStrOnly

        # Delete the temporary files
        delTemps = [tempRoughRescale, tempFlowdirGauge, tempFlowdirStr, tempCost, tempUpCost, tempStrCost,\
            tempFocalMax, tempFlowdirStrGauge]
        [arcpy.Delete_management(delTemp) for delTemp in delTemps]

        # Clean up the upland flowtime and flowlength rasters
        [arcpy.Delete_management(delTemp) for delTemp in arcpy.ListRasters(tempFlowtime+'*')]
        [arcpy.Delete_management(delTemp) for delTemp in arcpy.ListRasters(tempFlowlength+'*')]

    #%%
    #------------------------------------------------------------------------------
    # Calculate watersheds and add gauge points to the downstream watershed
    #------------------------------------------------------------------------------
    # Calculate watersheds for the gauge points
    rasterWsheds = arcpy.sa.Watershed(inFlowdir,outGaugeRaster)
    rasterWsheds.save(tempWatersheds)

    # Import the flowdirection, watershed, and gauge rasters
    arrayGauge = read_raster(rasterGauge, noDataToValue=-1)
    arrayFlowdir = read_raster(inFlowdir, noDataToValue=-1)
    arrayWsheds = read_raster(rasterWsheds)
    arrayWshedsOut = arrayWsheds.copy()

    # For each gauge, identify the downstream watershed
    indGauges = numpy.argwhere(arrayGauge>0)
    for thisGaugeInd in indGauges:
        thisFlowdir = arrayFlowdir[thisGaugeInd[0],thisGaugeInd[1]]

        # Determine the index of this gauge's watershed
        if thisFlowdir == 1:
            indAdd = [0,1]
        elif thisFlowdir == 2:
            indAdd = [1,1]
        elif thisFlowdir == 4:
            indAdd = [1,0]
        elif thisFlowdir == 8:
            indAdd = [1,-1]
        elif thisFlowdir == 16:
            indAdd = [0,-1]
        elif thisFlowdir == 32:
            indAdd = [-1,-1]
        elif thisFlowdir == 64:
            indAdd = [-1,0]
        elif thisFlowdir == 128:
            indAdd = [-1,1]
        else: #current gauge not in valid flowdir cell
            continue
        indWshed = numpy.add(thisGaugeInd,indAdd)

        # Extract that value, and write to the arrayWsheds
        thisWshed = arrayWsheds[indWshed[0],indWshed[1]]
        arrayWshedsOut[thisGaugeInd[0],thisGaugeInd[1]] = thisWshed

    # Write the watersheds array back to disk
    arrayWshedsOut[arrayWshedsOut==-1] = 0
    write_raster(arrayWshedsOut,outWatersheds,rasterWsheds,nanVal=0)
    rasterWshedsOut = arcpy.Raster(outWatersheds)

    # Log this output
    lhm_frontend.log_lhm_output(outWatersheds,arcpy.env.workspace,inputDB,printResult)

    # Create an array that has the downstream watershed defined only in cells with runoff gauges
    rasterDSWshedsOut = arcpy.sa.Con(~arcpy.sa.IsNull(rasterGauge),rasterWshedsOut)
    rasterDSWshedsOut.save(outDSWatersheds)

    # Log this output
    lhm_frontend.log_lhm_output(outDSWatersheds,arcpy.env.workspace,inputDB,printResult)

    # Clean up this section
    del arrayGauge, arrayFlowdir, arrayWsheds, arrayWshedsOut
    del rasterDSWshedsOut, rasterWshedsOut, rasterWsheds, rasterGauge
    arcpy.Delete_management(tempWatersheds)

    #-------------------------------------------------------------------------------
    # Add-on calculations for the MUSLE sedimentation module
    #-------------------------------------------------------------------------------
    rasterStreams = arcpy.Raster(inStreams)

    # Imprint streams as no data on the flowdir array
    rasterFlowdirStr = arcpy.sa.Con(rasterStreams==0,inFlowdir)
    rasterFlowdirStr.save(tempFlowdirStr)

    # Calculate non-weighted flow length to streams
    rasterFlowlength = tile_flowlength(arcpy,params,rasterFlowdirStr,rasterTilesInt)
    #rasterFlowlength = arcpy.sa.FlowLength(rasterFlowdirStr,'DOWNSTREAM')

    # Fix this to a minimum value, and remove nodata
    rasterLengthMin = arcpy.sa.Con(arcpy.sa.IsNull(rasterFlowlength),params['lengthMinVal'],\
        arcpy.sa.Con(rasterFlowlength<params['lengthMinVal'],params['lengthMinVal'],rasterFlowlength))
    rasterLengthMin.save(outOverFlowlength)

    # Log this output
    lhm_frontend.log_lhm_output(outOverFlowlength,arcpy.env.workspace,inputDB,printResult)

    # First, calculate the total slope along the path (this is of course ridiculous, but it will make sense in a minute)
    rasterInSlope = arcpy.Raster(inFlowslope)
    rasterWeightSlope = rasterInSlope/cellDistAvg
    rasterWeightSlope.save(tempWeightSlope)
    rasterWeightDist = arcpy.sa.CreateConstantRaster(1/cellDistAvg,'FLOAT')
    rasterWeightDist.save(tempWeightDist)
    rasterTotalSlope = tile_flowlength(arcpy,params,rasterFlowdirStr,rasterTilesInt,rasterWeightSlope)
    #rasterTotalSlope = arcpy.sa.FlowLength(rasterFlowdirStr,'DOWNSTREAM',rasterWeightSlope)
    rasterTotalCells = tile_flowlength(arcpy,params,rasterFlowdirStr,rasterTilesInt,rasterWeightDist)
    #rasterTotalCells = arcpy.sa.FlowLength(rasterFlowdirStr,'DOWNSTREAM',rasterWeightDist)

    # Calculate the weighting for the flow slope calculation
    rasterFlowslope = rasterTotalSlope / rasterTotalCells

    # Fix this to a minimum value, and remove nodata
    rasterFlowslopeMin = arcpy.sa.Con(arcpy.sa.IsNull(rasterFlowslope),params['slopeMinVal'],\
        arcpy.sa.Con(rasterFlowslope<params['slopeMinVal'],params['slopeMinVal'],rasterFlowslope))
    rasterFlowslopeMin.save(outOverFlowslope)

    # Log this output
    lhm_frontend.log_lhm_output(outOverFlowslope,arcpy.env.workspace,inputDB,printResult)

    # Clean up after this section
    # Delete temporary raster objects
    del rasterFlowdirStr, rasterFlowlength, rasterFlowslope, rasterFlowslopeMin, rasterInSlope, \
        rasterLengthMin, rasterTotalCells, rasterTotalSlope, rasterWeightDist, rasterWeightSlope,

    # Delete temporary files    
    arcpy.Delete_management(tempWeightSlope)
    arcpy.Delete_management(tempWeightDist)
    
    # Clean up the flowtime rasters
    [arcpy.Delete_management(delTemp) for delTemp in arcpy.ListRasters(tempFlowtime+'*')]
    [arcpy.Delete_management(delTemp) for delTemp in arcpy.ListRasters(tempFlowlength+'*')]

    # Clean up from all three sections
    arcpy.Delete_management(tempFlowaccTiles)
    arcpy.Delete_management(tempTilesClip)
    del rasterStreams, rasterTilesInt

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Overland and channel routing are prepared for the <strong>%s</strong> region'%regionName))


'''
------------------------------------------------------------------------------
Prepare impermeable surface routing
------------------------------------------------------------------------------
'''
def impermeable_surfaces_routing(inputDB, pathConfig, printResult=True):
    # This script creates a map of urban impermeable surface runoff types for ILHM
    # The categories are 1: upland run-on (concentration), 2) wetland run-on/storage,
    # and 3) overland flow (storm drains)

    # Import system modules
    if printResult:
        from IPython.display import display, HTML
    #-------------------------------------------------------------------------------
    # Define inputs
    #-------------------------------------------------------------------------------
    (rowLU,rowImperv,rowIncorp,rowRoads,dirLanduse,dirImperv,gdbWetlands,prefixImperv,prefixType,prefixPercent,outExt) = \
        lhm_frontend.names_frontend([\
        'srcLanduse','srcImperv','srcIncorp','srcRoads','dirLanduse','dirImperv','gdbWetlands',\
        'prefixImperv','prefixImpervType','prefixPercent','outExtRaster'])

    # Get the model input locations
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Other standardized paths
    outDir = os.path.join(os.path.split(inputDB)[0], dirLanduse+'/'+dirImperv)
    inputNWI = os.path.join(os.path.split(inputDB)[0],gdbWetlands)

    # Specify the class(es) of landuse relevant to each type
    devCodes = {'low':[21,22],'high':[23,24]}
    impervNull = 127

    # Specify distances for nearby objects, in grid projection coords
    buffDist = {'roads':5, 'wetlands':10, 'retention':100}

    # Specify grid or feature inputs
    inputTypes = {'impervious':'raster', 'incorporated':'feature', 'roads':'feature', \
        'wetlands':'feature', 'landuse':'raster', 'retention':'feature'}

    # Internal processing names, shouldn't need to change these
    clipNamesOnce = {'incorporated':'tempclipincorp', 'roads':'tempcliproad'}
    clipNamesEach = {'landuse':'tempcliplu'}
    clipNames = clipNamesOnce.copy()
    clipNames.update(clipNamesEach)
    buffNames = {'roads':'tempbuffroad', 'wetlands':'tempbuffwet', 'retention':'tempbuffreten'}
    presNames = {'impervious':'temppresimperv', 'incorporated':'temppresincorp', 'roads':'temppresroad', \
        'wetlands':'temppreswet', 'landuse':{'low':'temppreslandlow','high':'temppreslandhigh'}, 'retention':'temppresreten'}
    presField = 'present'

    #-------------------------------------------------------------------------------
    # Set up the environment, get inputs from the DB
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment('',inputDB,inputBdry)

    # Split the list of LU and impervious grids, if needed
    listLU = inputPaths[rowLU]
    listImperv = inputPaths[rowImperv]

    # Specify the input datasets, impervious values are percent 0 - 100
    # For now, incorporated, roads, wetlands, and retention should be shapefiles
    inputs = {'incorporated':inputPaths[rowIncorp],'roads':inputPaths[rowRoads],\
        'wetlands':inputNWI+'/nwi_processed',\
        'retention':inputNWI+'/nwi_processed' }

    #-------------------------------------------------------------------------------
    # Clip input files to boundary, or copy to the working directory with new name
    #-------------------------------------------------------------------------------
    for thisInput in clipNamesOnce.keys():
        if inputTypes[thisInput]=='feature':
            arcpy.Clip_analysis(inputs[thisInput],inputBdry,clipNames[thisInput])
        elif inputTypes[thisInput]=='raster': # This is a raster
            arcpy.env.cellSize = arcpy.Describe(inputs[thisInput]).MeanCellHeight
            rasterExtract = arcpy.sa.ExtractByMask(inputs[thisInput],inputBdry)
            rasterExtract.save(clipNames[thisInput])
            del rasterExtract

    #-------------------------------------------------------------------------------
    # Create buffered files
    #-------------------------------------------------------------------------------
    for thisBuff in buffNames.keys():
        if thisBuff in clipNames.keys():
            buffInput = clipNames[thisBuff]
        else:
            buffInput = inputs[thisBuff]
        arcpy.Buffer_analysis(buffInput,buffNames[thisBuff],buffDist[thisBuff])

    #-------------------------------------------------------------------------------
    # Loop through Impervious grids
    #-------------------------------------------------------------------------------
    for thisImperv in listImperv:
        # Determine which LU grid to use, for now, just select the central landuse out of the list
        thisLU = listLU[int(round(len(listLU)/2))]

        # Get the base name
        nameImperv = os.path.splitext(os.path.split(thisImperv)[1])[0]

        # Update the inputs array
        inputs['landuse'] = thisLU
        inputs['impervious'] = os.path.join(outDir,prefixImperv+nameImperv+outExt)

        # Determine the output grid cellsize, assumes all at the same cellsize
        matchRes = inputs['impervious']
        outSize = arcpy.Describe(matchRes).MeanCellHeight
        arcpy.env.cellSize = outSize

        # Clip landuse grids, here we need the original LU, because we need access to level 2 urban not used by LHM
        for thisInput in clipNamesEach.keys():
            if inputTypes[thisInput]=='feature':
                arcpy.Clip_analysis(inputs[thisInput],inputBdry,clipNames[thisInput])
            elif inputTypes[thisInput]=='raster': # This is a raster
                rasterExtract = arcpy.sa.ExtractByMask(inputs[thisInput],inputBdry)
                rasterExtract.save(clipNames[thisInput])
                del rasterExtract

        # Update the inputs array
        inputs['landuse'] = clipNames['landuse']

        #---------------------------------------------------------------------------
        # Convert 0/1 grids for presence or absence of all conditions
        #---------------------------------------------------------------------------
        for thisPres in presNames.keys():
            if thisPres not in ['landuse','impervious']:
                if thisPres in buffNames:
                    tempName = buffNames[thisPres]
                else:
                    tempName = clipNames[thisPres]

                # Add the presence/absence field
                arcpy.AddField_management(tempName,presField,'SHORT')

                # Calculate the value as 1 for all objects
                arcpy.CalculateField_management(tempName,presField,'1','PYTHON')

                # Export this shapefile as a grid
                arcpy.FeatureToRaster_conversion(tempName,presField,'tempPres',outSize)

                # Convert null values to 0
                rasterPres = arcpy.sa.Con(arcpy.sa.IsNull('tempPres'),0,'tempPres')
                rasterPres.save(presNames[thisPres])

                # Clean up
                del rasterPres
                arcpy.Delete_management('tempPres')

            else: # now for impervious and landuse
                if inputTypes[thisPres] == 'raster':
                    rasterInput = arcpy.Raster(inputs[thisPres])
                    # Check to see if this is the land use case
                    if thisPres=='landuse':
                        for thisLand in presNames['landuse']: # for both high and low res cases
                            # Start with no matches to the LU class
                            rasterPres = arcpy.sa.CreateConstantRaster(0)
                            for thisVal in devCodes[thisLand]:
                                rasterPresLast = rasterPres
                                rasterPres = arcpy.sa.Con(rasterInput==thisVal,1,0) + rasterPresLast
                            # Save out the presence raster
                            rasterPres.save(presNames[thisPres][thisLand])
                        del rasterPresLast, rasterPres
                    elif thisPres=='impervious':
                        rasterPres = arcpy.sa.Con(rasterInput>0,arcpy.sa.Con(rasterInput<impervNull,1,0),0)
                        rasterPres.save(presNames[thisPres])
                        del rasterPres

        #---------------------------------------------------------------------------
        # Create all test conditions
        #---------------------------------------------------------------------------
        # Build raster objects
        rasterIncorp = arcpy.Raster(presNames['incorporated'])
        rasterRoads = arcpy.Raster(presNames['roads'])
        rasterWetlands = arcpy.sa.Raster(presNames['wetlands'])
        rasterRetention = arcpy.sa.Raster(presNames['retention'])
        rasterImperv = arcpy.Raster(presNames['impervious'])
        rasterUrbLD = arcpy.Raster(presNames['landuse']['low'])
        rasterUrbHD = arcpy.Raster(presNames['landuse']['high'])

        # Test for upland run-on, output
        rasterTest1 = rasterImperv * arcpy.sa.Con(rasterIncorp==0,1,0)
        rasterTest2 = rasterImperv * rasterIncorp * rasterUrbLD * arcpy.sa.Con(rasterRoads==0,1,0)
        rasterTestUpland = (rasterTest1 + rasterTest2) * arcpy.sa.Con(rasterWetlands==0,1,0)

        # Test for wetland run-on/storage
        rasterTest3 = (rasterTest1 + rasterTest2) * rasterWetlands
        rasterTest4 = rasterImperv * rasterIncorp * rasterUrbHD * \
            arcpy.sa.Con(rasterRoads==0,1,0) * arcpy.sa.Con(rasterUrbLD==0,1,0)
        rasterTestWetland = rasterTest3 + rasterTest4 * rasterRetention

        # Test for overland flow runoff
        rasterTest5 = rasterImperv * rasterIncorp * rasterRoads
        rasterTest6 = rasterTest4 * arcpy.sa.Con(rasterRetention==0,1,0)
        rasterTestRunoff = rasterTest5 + rasterTest6

        #---------------------------------------------------------------------------
        # Create output grids
        #---------------------------------------------------------------------------
        # Set up the output names
        outNameType = prefixType+nameImperv+outExt
        outPathType = os.path.join(outDir,outNameType)
        outNameImperv = prefixPercent+nameImperv+outExt
        outPathImperv = os.path.join(outDir,outNameImperv)

        # Create the output type raster
        rasterOutType = (rasterTestUpland * 1) + (rasterTestWetland * 2) + (rasterTestRunoff * 3)
        rasterOutType.save(outPathType)

        # Log this output
        lhm_frontend.log_lhm_output(outNameType,arcpy.env.workspace,inputDB,printResult)

        # Create the output impervious percent raster, with 0 instead of null value
        rasterOutImperv = arcpy.sa.Con(rasterImperv==1,inputs['impervious'],0)
        rasterOutImperv.save(outPathImperv)

        # Log this output
        lhm_frontend.log_lhm_output(outNameImperv,arcpy.env.workspace,inputDB,printResult)

        #---------------------------------------------------------------------------
        # Clean up
        #---------------------------------------------------------------------------
        # Delete non-raster temporary objects
        for thisPres in presNames:
            if thisPres=='landuse':
                for thisLand in presNames[thisPres]:
                    arcpy.Delete_management(presNames[thisPres][thisLand])
            else:
                arcpy.Delete_management(presNames[thisPres])

        # Delete the raster objects
        del rasterTest1,rasterTest2,rasterTest3,rasterTest4,rasterTest5,rasterTest6,\
            rasterTestRunoff,rasterTestUpland,rasterTestWetland,rasterOutImperv,rasterOutType,\
            rasterImperv,rasterUrbLD,rasterUrbHD,rasterIncorp,rasterRoads,rasterWetlands,rasterRetention

    # Delete clips/buffers used for all LU years
    for thisClip in clipNames:
        arcpy.Delete_management(clipNames[thisClip])
    for thisBuff in buffNames:
        arcpy.Delete_management(buffNames[thisBuff])

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Impermeable surfaces routing is prepared for the <strong>%s</strong> region'%regionName))


'''
------------------------------------------------------------------------------
Finish preparing stream gauges
------------------------------------------------------------------------------
'''
def snap_combine_gauges(inputDB, pathConfig, params=dict(), printResult=True):
    """This function combines user/custom network gauges with the USGS stream gauges used
    so far to define the stream network geometry, and limits the USGS gauges to those
    used by the geometry creation regime as well as those (optionally) manually refined
    by the user after USGS gauge select

    Params are:
        errorAccept: fraction, default is 20% error between stated watershed and reported area, USGS only
        snapDistance: m, maximum distance to snap gauge points to flowaccumulation grid, maximum is 400
    """
    import gis
    if printResult:
        import os
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # standardized/advanced settings
    #-------------------------------------------------------------------------------
    (inputBdryName,fieldGaugeLHM,inFlowaccum,inUserGauges,inGaugesSelect,outGauges) = \
        lhm_frontend.names_frontend(['prepBdryBuff','fieldGaugeIDNum','prepDEMFillAccum',\
        'prepUserGauges','prepSelGauges','prepCombGauges'])

    inputBdry = inputDB + '/' + inputBdryName

    # Parse the input params structure
    paramDefaults = {'errorAccept':0.2, 'snapDistance':400}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Specify USGS field names to rename
    fieldsRename = {'site_no':'loc_id','station_nm':'loc_name'}

    # Standard field names from the USGS and user gauge prep
    fieldGaugeID = 'loc_id'
    fieldGaugeArea0 = 'drain_area_va'
    fieldGaugeArea = 'drain_area'
    fieldGaugeAgency = 'agency_cd'
    fieldGaugeName = 'loc_name'
    fieldBeginDate = 'begin_date'
    fieldEndDate = 'end_date'
    fieldErrorArea = 'error_area'

    # Fields to keep from combined gauges file, others will be deleted
    fieldsKeep = [fieldGaugeAgency,fieldGaugeID,fieldGaugeArea,fieldGaugeName,fieldGaugeLHM,\
                  fieldBeginDate,fieldEndDate]

    # Specify temporary names
    tempLoopFeature = 'temp_loop_feature'
    tempSnapFeature = 'temp_snap_feature'
    tempErrorFeature = 'temp_error_feature'

    tempLayer = 'temp_layer'

    # Set environment settings
    arcpy = lhm_frontend.set_environment('',inputDB,inputBdry)

    #-------------------------------------------------------------------------------
    # Prepare USGS gauges
    #-------------------------------------------------------------------------------
    # Copy to the output, and delete unnecessary fields
    arcpy.CopyFeatures_management(inGaugesSelect,outGauges)

    # Add a numeric ID field
    fieldsList = [f.name for f in arcpy.ListFields(outGauges)]
    if fieldGaugeLHM not in fieldsList:
        arcpy.AddField_management(outGauges,fieldGaugeLHM,'LONG')
        arcpy.CalculateField_management(outGauges,fieldGaugeLHM,'!ObjectID!')

    # Rename site ID and name fields
    for fieldRename in fieldsRename.keys():
        arcpy.AlterField_management(outGauges,fieldRename,fieldsRename[fieldRename],fieldsRename[fieldRename])

    # Rename the drainage area field
    if fieldGaugeArea not in fieldsList:
        arcpy.AddField_management(outGauges,fieldGaugeArea,'FLOAT')
        arcpy.CalculateField_management(outGauges,fieldGaugeArea,'!%s!'%fieldGaugeArea0)

    listFields = arcpy.ListFields(outGauges)
    delFields = [field.name for field in listFields if field.name not in fieldsKeep]
    [arcpy.DeleteField_management(outGauges,field) for field in delFields[2:]] # skip OBJECTID and Shape

    #-------------------------------------------------------------------------------
    # Add in the user-created gauges, if present
    #-------------------------------------------------------------------------------
    if arcpy.Exists(inUserGauges):
        # Make a copy of this feature
        arcpy.Copy_management(inUserGauges,tempLoopFeature)

        # Get maximum value of fieldGaugeID in outGauges
        rows = arcpy.SearchCursor(outGauges)
        maxID = 0
        for row in rows:
            thisID = row.getValue(fieldGaugeLHM)
            if thisID > maxID:
                maxID = thisID
        del row, rows

        # Recalculate fieldGaugeID in this inputFeature to start at end of maximum
        arcpy.CalculateField_management(tempLoopFeature,fieldGaugeLHM,'!'+fieldGaugeLHM+'! + '+str(maxID),'PYTHON')

        # Clean out other fields no longer needed
        listFields = arcpy.ListFields(tempLoopFeature)
        delFields = [field.name for field in listFields if field.name not in fieldsKeep]
        [arcpy.DeleteField_management(tempLoopFeature,field) for field in delFields[2:]] # skip OBJECTID and Shape

        # Merge features
        arcpy.Append_management(tempLoopFeature,outGauges)

        # Clean up
        arcpy.Delete_management(tempLoopFeature)

    #-------------------------------------------------------------------------------
    # Snap gauges and compute error in drainge basin area
    #-------------------------------------------------------------------------------
    # Rename
    if arcpy.Exists(tempSnapFeature):
        arcpy.Delete_management(tempSnapFeature)
    arcpy.Rename_management(outGauges,tempSnapFeature)

    # Run the snap pour points tool
    gis.snap_points_flowaccum_feature(tempSnapFeature,outGauges,inFlowaccum,params['snapDistance'],fieldGaugeLHM)

    # Extract flowaccumulation values to points
    gis.raster_vals_to_points(outGauges,inFlowaccum,'flowaccum')

    # Compute error area
    rasterFlowaccum = arcpy.Raster(inFlowaccum)
    cellSize = rasterFlowaccum.meanCellHeight
    arcpy.AddField_management(outGauges,fieldErrorArea,'FLOAT')
    arcpy.CalculateField_management(outGauges,fieldErrorArea,'(!flowaccum!*float(%f)**2 - !%s!*1000**2)/(!%s!*1000**2)'%\
                                    (cellSize,fieldGaugeArea,fieldGaugeArea))
    arcpy.DeleteField_management(outGauges,'flowaccum')

    # Replace null values with 0
    codeBlock = """def float_handle_empty(inVal):
                if inVal in (u' ', ' ' ,u'', ''):
                    return 0
                elif inVal is None:
                    return 0
                else:
                   return float(inVal)"""

    calcExpression = 'float_handle_empty(!%s!)'%(fieldErrorArea)
    arcpy.CalculateField_management(outGauges,fieldErrorArea,calcExpression,'PYTHON',codeBlock)

    # Add X and Y fields
    arcpy.AddXY_management(outGauges)

    #-------------------------------------------------------------------------------
    # Delete features that do not meet error threshold criteria
    #-------------------------------------------------------------------------------
    # Rename
    if arcpy.Exists(tempErrorFeature):
        arcpy.Delete_management(tempErrorFeature)
    arcpy.Rename_management(outGauges,tempErrorFeature)

    # Create a selection layer
    arcpy.MakeFeatureLayer_management(tempErrorFeature,tempLayer)

    # Select all gauges that have error inside the threshold
    arcpy.SelectLayerByAttribute_management(tempLayer,'NEW_SELECTION','(\"%s\">-%f)'%(fieldErrorArea,params['errorAccept']))
    arcpy.SelectLayerByAttribute_management(tempLayer,'REMOVE_FROM_SELECTION','(\"%s\">%f)'%(fieldErrorArea,params['errorAccept']))

    # Add USER gauges
    arcpy.SelectLayerByAttribute_management(tempLayer,'ADD_TO_SELECTION','(\"%s\" LIKE \'%%%s%%\')'%(fieldGaugeAgency,'USER'))

    # Export to a new feature
    arcpy.CopyFeatures_management(tempLayer,outGauges)

    # Log this output
    lhm_frontend.log_lhm_output(outGauges,arcpy.env.workspace,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    # delete rasters
    del rasterFlowaccum

    # Delete temporary layers and rasters/features
    arcpy.Delete_management(tempLayer)
    arcpy.Delete_management(tempSnapFeature)
    arcpy.Delete_management(tempErrorFeature)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('USGS and user gauges combined for the <strong>%s</strong> region'%regionName))
