'''
--------------------------------------------------------------------------------
Merge state databases, parse nwi codes
--------------------------------------------------------------------------------
'''
def nwi_merge_parse(inputDB, pathConfig, params=dict(), printResult=True):
    '''This function merges NWI wetlands and clips them to the area of interest, it then does
    other model-releavant processing including parsing the NWI codes
    
    Params are:
        cellSize: output cellsize of zonal statitistics operations in meters, default is 10
        complexElevTol: tolerance in elevation difference to identify wetland complexes, default is 1.5
        statesDone: list of states already done
        statesOmit: list of states to omit'''

    import arcpy, os, re
    from tqdm.auto import tqdm # tqdm is a progress bar module
    import lhm_frontend
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Advanced/Standardized Inputs
    #-------------------------------------------------------------------------------
    (configFile,configSection,rowSourceRaw,rowSource,rowStates,outputDBName) = \
        lhm_frontend.names_frontend(['srcFile','srcSectionSurface','srcWetlandsRaw','srcWetlands',\
        'srcStates','gdbWetlands'])
    
    # Get the model input locations
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Specify the output directory
    outputDir = os.path.split(inputDB)[0]
    outputDB = os.path.join(outputDir,outputDBName)

    # Temporary names (shouldn't need to change these)
    tempStates = 'temp_states'
    tempClip = 'temp_clip'
    tempMerge = 'temp_merge'
    tempDissolve = 'temp_dissolve_wetlands'
    tempComplex = 'temp_dissolve_complex'
    tempJoined = 'temp_joined_elevs'
    tempJoinedCalc = 'temp_joined_calc'
    tempZonal = 'temp_zonal'
    tempSample = 'temp_sample'
    tempSamplePoints = 'temp_sample_points'
    tempLayerElev = 'temp_layer_elev'
    tempLayerOut = 'temp_layer_out'

    # Handle specifiable parameters
    paramDefaults = {'cellSize':10, 'complexElevTol':1.5,'statesDone':[],'statesOmit':[]}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Specify outputNames
    outProcessed = 'nwi_processed'
    outSplitSystems = ['L','R','P']
    outSplitNames = ['nwi_lacustrine','nwi_riverine','nwi_palustrine']

    # Specify settings for wetland IDs and complexes
    fieldWetlandID = 'Wetland_ID'
    fieldWetlandElev = 'Wetland_Elev'
    fieldWetlandElevInt = 'Wetland_Elev_Int'
    fieldComplexID = 'Complex_ID'
    fieldsKeep = ['OBJECTID','SHAPE',tempDissolve+'_ATTRIBUTE',tempDissolve+'_FIRST_WETLAND_TYPE',\
        tempDissolve+'_Wetland_ID','Wetland_Elev','Complex_ID','SHAPE_Length','SHAPE_Area']
    fieldsRenameOrig = [tempDissolve+'_ATTRIBUTE',tempDissolve+'_FIRST_WETLAND_TYPE',\
        tempDissolve+'_Wetland_ID']
    fieldsRenameFinal = ['ATTRIBUTE','WETLAND_TYPE','Wetland_ID']
    fieldsRenameTypes = ['TEXT','TEXT','LONG']
    fieldsRenameLength = ['25','255','']

    # Wetland code classification and parsing
    regExpStr = '(?P<System>[A-Z]{1})(?P<Subsystem>[1-5]{0,1})'+\
        '(?P<Class_Subclass>([A-Z]{2}(?!/)[1-9]{0,1}(?!/))|([A-Z]{2}[1-9]{0,1}[/]{1}[A-Z]{0,2}[1-9]{0,1})|[Ff]{1}|[h]{0,1})'+\
        '(?P<Water_Regime>[A-Z]{0,1})(?P<Special_Modifiers>[a-z1-9]{0,3})'
    regExp = re.compile(regExpStr) # use the compiled regular expression down below
    classFields = ['System','Subsystem','Class_Subclass','Water_Regime','Special_Modifiers']
    classFieldTypes = ['TEXT','TEXT','TEXT','TEXT','TEXT']
    classFieldLengths = ['1','1','7','1','3']
    classFieldOrig = 'ATTRIBUTE'

    #-------------------------------------------------------------------------------
    # Set up the processing environment, create outputDB if needed
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment(params['cellSize'],'',inputBdry)

    # Create the output geodatabase, if needed
    if not os.path.exists(outputDB):
        arcpy.CreateFileGDB_management(outputDir,outputDBName)
    arcpy.env.workspace = outputDB

    # NWI directory
    inDirNWI = inputPaths[rowSourceRaw]['dir']
    baseDBName = inputPaths[rowSourceRaw]['baseNameDB']
    baseDatasetName = inputPaths[rowSourceRaw]['baseNameDataset']

    # TIGER states (for state IDs)
    inTigerStates = inputPaths[rowStates]['feature']
    fieldState = inputPaths[rowStates]['field']

    #-------------------------------------------------------------------------------
    # Determine which states to merge
    #-------------------------------------------------------------------------------
    # Intersect the overlay of states with the project mask
    arcpy.PairwiseClip_analysis(inTigerStates,inputBdry,tempStates)

    # Bring in these features and make a list of unique states
    rows = arcpy.SearchCursor(arcpy.env.workspace+'/'+tempStates)
    states = []
    for row in rows:
        thisState = row.getValue(fieldState)
        if not thisState in states:
            states.append(thisState)
    del row, rows

    # Remove already completed states, and omitted states
    states = [state for state in states if state not in params['statesOmit']]

    # Clean up
    arcpy.Delete_management(tempStates)

    #-------------------------------------------------------------------------------
    # Clip the NWI layers then merge
    #-------------------------------------------------------------------------------
    # Second, loop through the states, clip them, and append to a final feature
    for state in tqdm(states,desc='Clipping and Merging NWI Layers'):
        if state not in params['statesDone']:
            thisInd = states.index(state) # to determine if this is the first pass through

            # Get the state-specific feature names
            thisDB = os.path.join(inDirNWI,state + baseDBName)
            thisFeature = thisDB + '/' + state + baseDatasetName

            # Clip the NWI features for this state
            result = arcpy.PairwiseClip_analysis(thisFeature,inputBdry,tempClip)

            # If this is the first pass through, copy to output dataset, otherwise append
            if thisInd == 0:
                arcpy.CopyFeatures_management(tempClip,tempMerge)

                # Get a list of fields that we will keep -- not all NWI layers have the same 
                fieldsMerge = [f.name.lower() for f in arcpy.ListFields(tempMerge)]
            else:
                # Get a list of fields in this state
                fieldsClip = [f.name.lower() for f in arcpy.ListFields(tempClip)]

                # Delete fields if not present in the merge
                for field in fieldsClip:
                    if field not in fieldsMerge:
                        arcpy.DeleteField_management(tempClip,field)

                # Now merge        
                arcpy.Append_management(tempClip,tempMerge)

            # Clean up
            arcpy.Delete_management(tempClip)

        # Print the status
        print(' - %s completed'%state)


    #-------------------------------------------------------------------------------
    # Process the layers further
    #-------------------------------------------------------------------------------
    # Assign wetland IDs-------------------------------------------------------------
    # First, need contiguous wetlands with identical class as a single feature
    arcpy.RepairGeometry_management(tempMerge)
    arcpy.Dissolve_management(tempMerge,tempDissolve,classFieldOrig,[['WETLAND_TYPE','FIRST']],'SINGLE_PART','DISSOLVE_LINES')
    arcpy.Delete_management(tempMerge)

    # Now, add a field for wetland ID and calculate, use an autonumber field
    arcpy.AddField_management(tempDissolve,fieldWetlandID,'LONG')
    rows = arcpy.UpdateCursor(tempDissolve)
    currVal = 0
    for row in rows:
        currVal += 1
        row.setValue(fieldWetlandID,currVal)
        rows.updateRow(row)
    del row, rows

    # Identify wetland elevations----------------------------------------------------
    # Extract elevations for each polygon, first use zonal statistics
    arcpy.sa.ZonalStatisticsAsTable(tempDissolve,fieldWetlandID,inputPaths['dem'],tempZonal,\
        'DATA','MINIMUM')

    # Make a feature layer from the feature and a table view from the table
    arcpy.MakeFeatureLayer_management(tempDissolve,tempLayerElev)

    # Then, join the zonal statistics table to the feature layer
    arcpy.AddJoin_management(tempLayerElev,fieldWetlandID,tempZonal,fieldWetlandID)

    # Identify features missing values
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'NEW_SELECTION',tempZonal+'.'+'MIN Is NULL')

    # Convert to points
    arcpy.FeatureToPoint_management(tempLayerElev,tempSamplePoints)

    # Sample
    arcpy.sa.ExtractValuesToPoints(tempSamplePoints,inputPaths['dem'],tempSample)

    # Clear selection and join sampled values back to feature layer, setting the null values in each elevation field to 0
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'CLEAR_SELECTION')
    arcpy.AddJoin_management(tempLayerElev,fieldWetlandID,tempSample,tempDissolve+'_'+fieldWetlandID)

    # Export this to a feature
    arcpy.CopyFeatures_management(tempLayerElev,tempJoined)
    arcpy.Delete_management(tempLayerElev)

    # Set null values to 0
    arcpy.MakeFeatureLayer_management(tempJoined,tempLayerElev)
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'NEW_SELECTION',tempZonal+'_'+'MIN Is NULL')
    arcpy.CalculateField_management(tempLayerElev,tempZonal+'_MIN',0,'PYTHON') # set null values to 0
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'SWITCH_SELECTION') # will pick rows where zonal worked
    arcpy.CalculateField_management(tempLayerElev,tempSample+'_RASTERVALU',0,'PYTHON')
    arcpy.Delete_management(tempLayerElev)

    # Calculate single output wetland elevation field
    arcpy.AddField_management(tempJoined,fieldWetlandElev,'FLOAT')
    arcpy.MakeFeatureLayer_management(tempJoined,tempLayerElev)
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'NEW_SELECTION',tempZonal+'_MIN > '+tempSample+'_RASTERVALU')
    arcpy.CalculateField_management(tempLayerElev,fieldWetlandElev,'!'+tempZonal+'_MIN!','PYTHON')
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'SWITCH_SELECTION')
    arcpy.CalculateField_management(tempLayerElev,fieldWetlandElev,'!'+tempSample+'_RASTERVALU!','PYTHON')
    arcpy.SelectLayerByAttribute_management(tempLayerElev,'CLEAR_SELECTION')
    arcpy.CopyFeatures_management(tempLayerElev,tempJoinedCalc)

    # Clean up from these steps
    arcpy.Delete_management(tempLayerElev)
    arcpy.Delete_management(tempJoined)
    arcpy.Delete_management(tempDissolve)
    arcpy.Delete_management(tempSample)
    arcpy.Delete_management(tempSamplePoints)
    arcpy.Delete_management(tempZonal)

    # Assign complex IDs-------------------------------------------------------------
    # Convert elevation to integer, based on threshold
    arcpy.AddField_management(tempJoinedCalc,fieldWetlandElevInt,'LONG')
    arcpy.CalculateField_management(tempJoinedCalc,fieldWetlandElevInt,'int(!'+fieldWetlandElev+'!/'+str(params['complexElevTol'])+')','PYTHON')

    # Dissolve based on integer elevation field
    arcpy.Dissolve_management(tempJoinedCalc,tempComplex,fieldWetlandElevInt,'','SINGLE_PART','')

    # Calculate wetland complex ID, use an autonumber field
    arcpy.AddField_management(tempComplex,fieldComplexID,'LONG')
    rows = arcpy.UpdateCursor(tempComplex)
    currVal = 0
    for row in rows:
        currVal += 1
        row.setValue(fieldComplexID,currVal)
        rows.updateRow(row)
    del row, rows

    # Spatial join with complex ID feature
    arcpy.SpatialJoin_analysis(tempJoinedCalc,tempComplex,outProcessed)
    arcpy.Delete_management(tempJoinedCalc)
    arcpy.Delete_management(tempComplex)

    # Remove unwanted fieldnames, match case-insensitive
    fieldsKeep = [field.lower() for field in fieldsKeep]
    fieldList = [field.name.lower() for field in arcpy.ListFields(outProcessed)]
    dropFields = [field for field in fieldList if field not in fieldsKeep]
    arcpy.DeleteField_management(outProcessed,dropFields)

    # Rename a few fields that were modified in joins
    for field in fieldsRenameOrig:
        thisInd = fieldsRenameOrig.index(field)
        arcpy.AddField_management(outProcessed,fieldsRenameFinal[thisInd],fieldsRenameTypes[thisInd],'','',fieldsRenameLength[thisInd])
        arcpy.CalculateField_management(outProcessed,fieldsRenameFinal[thisInd],'!'+field+'!','PYTHON')
    arcpy.DeleteField_management(outProcessed,fieldsRenameOrig)

    # Parse the codes----------------------------------------------------------------
    # Add fields
    for field in classFields:
        thisInd = classFields.index(field)
        arcpy.AddField_management(outProcessed,field,classFieldTypes[thisInd],'','',classFieldLengths[thisInd])

    # Loop through the table and populate the fields
    rows = arcpy.UpdateCursor(outProcessed)
    for row in rows:
        classes = regExp.match(row.getValue(classFieldOrig))
        if classes == None:
            pass # incorporated this exception in the regular expression above, shouldn't need any longer
    # #        if row.getValue(classFieldOrig) == 'PF': # assume that PF should be really Pf
    # #            classes = regExp.match('Pf')
        if not classes == None: # there might still be some unparseable fields
            for field in classFields:
                row.setValue(field,classes.group(field))
            rows.updateRow(row)
        else:
            print('OBJECTID %d has unparseable ATTRIBUTE'%(row.getValue('OBJECTID')))
    del row, rows

    # Log this
    lhm_frontend.log_lhm_output(outProcessed, outputDB, inputDB)

    # Split out and save-------------------------------------------------------------
    # Split the features into lacustrine, palustrine, riverine
    arcpy.MakeFeatureLayer_management(outProcessed,tempLayerOut)
    for outSystem in outSplitSystems:
        thisInd = outSplitSystems.index(outSystem)
        arcpy.SelectLayerByAttribute_management(tempLayerOut,'NEW_SELECTION','System = \''+outSystem+'\'')
        arcpy.CopyFeatures_management(tempLayerOut,outSplitNames[thisInd])

        # Log this
        lhm_frontend.log_lhm_output(outSplitNames[thisInd], outputDB, inputDB)
    arcpy.Delete_management(tempLayerOut)

    #-------------------------------------------------------------------------------
    # Finish up
    #-------------------------------------------------------------------------------
    # Update the config file with the location of the processed NWI data
    lhm_frontend.update_config_sources(os.path.join(pathConfig,configFile),configSection,rowSource,outputDB)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('NWI wetlands have finished processing for the <strong>%s</strong> region'%regionName))


'''
--------------------------------------------------------------------------------
Compute average depths for NWI wetlands
--------------------------------------------------------------------------------
'''
def nwi_depths(inputDB, pathConfig, params=dict(), printResult=True):
    '''This script process the NWI dataset and creates depth estimates for each wetland.
    These will later be merged with lake depths to produce a more comprehensive lake/wetland
    depth map.'''

    import arcpy, os
    import lhm_frontend
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Advanced/Standardized Inputs
    #-------------------------------------------------------------------------------
    # Get standard names
    (rowSource) = lhm_frontend.names_frontend(['srcWetlands'])

    # Get the model input locations
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)
    outputDB = inputPaths[rowSource]

    # NWI-specific fields
    # Specify the name of the NWI dataset to us
    nwiName = 'nwi_palustrine'
    # Specify outputNames
    outDepth = 'nwi_palustrine_depth'
    # Specify information about the new depths field
    fieldDepth = 'depth'
    typeDepth = 'float'
    # Specify the depth table for the water regime modifier types
    fieldModifier = 'Water_Regime'

    if not 'depthTable' in params.keys():
        depthTable = {'':0, 'U':0, 'A':-1.0, 'B':0, \
            'C':-0.8, 'E':-0.5, 'F':0.25, \
            'G':1.1, 'H':2, 'J':-1.5, 'K':1,\
            'L':1.5, 'M':1, 'N':0.7, 'P':0.3, \
            'S':-1.0, 'R':-0.8, 'T':0.25, 'V':2}
    else:
        depthTable = params['depthTable']
    # see class names at: http://www.fws.gov/wetlands/Documents/Wetlands-and-Deepwater-Habitats-Classification-chart.pdf

    # Set up the processing environment
    arcpy = lhm_frontend.set_environment('',outputDB,inputBdry)


    #-------------------------------------------------------------------------------
    # Do the processing
    #-------------------------------------------------------------------------------
    # Copy the dataset into its new name
    arcpy.CopyFeatures_management(nwiName,outDepth)

    # Add a field
    arcpy.AddField_management(outDepth,fieldDepth,typeDepth)

    # Calculate field values
    rows = arcpy.UpdateCursor(outDepth)

    for row in rows:
        thisModifier = row.getValue(fieldModifier)
        row.setValue(fieldDepth,depthTable[thisModifier])
        rows.updateRow(row)
    del row, rows

    # Log this
    lhm_frontend.log_lhm_output(outDepth, outputDB, inputDB)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Depths for NWI wetlands have been calculated for the <strong>%s</strong> region'%regionName))
