'''
--------------------------------------------------------------------------------
Define externally-drained features
--------------------------------------------------------------------------------
'''
def externally_drained_features(inputDB, pathConfig, params=dict(), printResult=True):
    """This script creates a joined set of polygons to use for determining externally-connected
    drainage (or significant internally-connected drainage, if need be)
    
    Params are:
        cellSize: must be in model units, default 10
        expandNum: needs to be an integer, number of cells used to identify connected features, default 4
        selectTolerance, distance to select features that intersect model boundary, default 100
    """

    import arcpy, os
    import lhm_frontend
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Script defaults, shouldn't need to change these
    #-------------------------------------------------------------------------------
    # Get the input names
    (inputBdryNoBuffer,rowPolys,rowLines,outSelectPoly,outLines,outPolys) = lhm_frontend.names_frontend(\
        ['prepBdry','srcHydroPolys','srcHydroLines','prepConnBuff','prepConnLines','prepConnPolys'])

    # Get the model standard input locations
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Handle default params, params are
    paramDefaults = {'cellSize':10, 'expandNum':4, 'selectTolerance':100}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Internally-used poly names
    tempHydroPolys = 'temp_hydro_polys'
    tempHydroLines = 'temp_hydro_lines'
    tempHydroGridPolys = 'temp_hydro_grid_polys'
    tempHydroGridLines = 'temp_hydro_grid_lines'
    tempMergeTypes = 'temp_hydro_types_merge'
    tempDissolvePoly = 'temp_hydro_poly_dissolve'
    tempBdryLine = 'temp_bdry_line'

    # Internally-used layer names
    layerDissolve = 'layer_dissolve'
    layerBdry = 'layer_boundary'
    layerLines = 'layer_lines'
    layerPolys = 'layer_polys'
    layerBuffs = 'layer_buffers'

    # Set up the arcpy environment
    arcpy = lhm_frontend.set_environment('',inputDB,inputBdry)

    #-------------------------------------------------------------------------------
    # Identify connected features
    #-------------------------------------------------------------------------------
    # Copy features to temporary version
    arcpy.CopyFeatures_management(inputPaths[rowLines],tempHydroLines)
    arcpy.CopyFeatures_management(inputPaths[rowPolys],tempHydroPolys)

    # Then, add a field and calculate its value to zero for dissolving
    arcpy.AddField_management(tempHydroPolys,'Dissolve','SHORT')
    arcpy.CalculateField_management(tempHydroPolys,'Dissolve',1,'PYTHON')
    arcpy.AddField_management(tempHydroLines,'Dissolve','SHORT')
    arcpy.CalculateField_management(tempHydroLines,'Dissolve',1,'PYTHON')

    # Next,convert each to raster with that feature
    arcpy.PolygonToRaster_conversion(tempHydroPolys,'Dissolve',tempHydroGridPolys,'','',params['cellSize'])
    arcpy.PolylineToRaster_conversion(tempHydroLines,'Dissolve',tempHydroGridLines,'','',params['cellSize'])

    # Merge the layers
    rasterMerge = arcpy.sa.Con(arcpy.sa.IsNull(tempHydroGridPolys),arcpy.sa.Con(arcpy.sa.IsNull(tempHydroGridLines),0,1),1)
    rasterMerge.save(tempMergeTypes)

    # Expand the zones
    rasterExpand = arcpy.sa.Expand(rasterMerge,params['expandNum'],1)

    # Remove the zero values
    rasterNoZeros = arcpy.sa.Con(rasterExpand==1,1)

    # Now, use region group to identify connected layers
    rasterGroups = arcpy.sa.RegionGroup(rasterNoZeros,'EIGHT')

    # Convert these to a polygon, first need an integer grid
    rasterGroupsCount = arcpy.sa.Int(arcpy.sa.Lookup(rasterGroups,'Count'))
    arcpy.RasterToPolygon_conversion(rasterGroupsCount,tempDissolvePoly,'NO_SIMPLIFY')

    # Convert the boundary to a polyline feature
    arcpy.PolygonToLine_management(inputBdryNoBuffer,tempBdryLine)

    # Make feature layers for selection
    arcpy.MakeFeatureLayer_management(tempDissolvePoly,layerDissolve)
    arcpy.MakeFeatureLayer_management(tempBdryLine,layerBdry)

    # Select those groups that intersect the model boundary
    arcpy.SelectLayerByLocation_management(layerDissolve,'INTERSECT',layerBdry,params['selectTolerance'])

    # Copy these to a new output layer
    arcpy.CopyFeatures_management(layerDissolve,outSelectPoly)

    # Log this output
    lhm_frontend.log_lhm_output(outSelectPoly,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Create selections of the hydrolines and hydropolys that fall within these features
    #-------------------------------------------------------------------------------
    # First, make feature layers
    arcpy.MakeFeatureLayer_management(inputPaths[rowLines],layerLines)
    arcpy.MakeFeatureLayer_management(inputPaths[rowPolys],layerPolys)
    arcpy.MakeFeatureLayer_management(outSelectPoly,layerBuffs)

    # Now, select by location
    arcpy.SelectLayerByLocation_management(layerLines,'INTERSECT',layerBuffs)
    arcpy.SelectLayerByLocation_management(layerPolys,'INTERSECT',layerBuffs)

    # Copy these to new, final features
    arcpy.CopyFeatures_management(layerLines,outLines)
    arcpy.CopyFeatures_management(layerPolys,outPolys)

    # Log these outputs
    lhm_frontend.log_lhm_output(outLines,inputDB,inputDB,printResult)
    lhm_frontend.log_lhm_output(outPolys,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    # Delete temporary raster objects
    del rasterExpand, rasterGroups, rasterGroupsCount, rasterMerge, rasterNoZeros

    # first list variables/objects/files to delete
    delLayers = [thisVar for thisVar in dir() if thisVar[0:5]=='layer']
    delTemps = [thisVar for thisVar in dir() if thisVar[0:4]=='temp']

    # Layers
    for delLayer in delLayers:
        arcpy.Delete_management(eval(delLayer))

    # Delete temporary rasters and features
    for delTemp in delTemps:
        arcpy.Delete_management(eval(delTemp))

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Externally-drained features finished processing for the <strong>%s</strong> region'%regionName))

'''
--------------------------------------------------------------------------------
Define internally-drained areas
--------------------------------------------------------------------------------
'''
def internally_drained_areas(inputDB, pathConfig, params=dict(), printResult=True):
    """This functions calculates internally-drained areas using a DEM, and externally-drained features
    
    Params are:
        sinkDepth: Minimum depth for a sink to be considered valid, in DEM units, default is 0.25 
        connDist: Search distance for connected features, in projection units, default is 30
        minSizeExtern: Minimum size of a valid externally-drained region, in projection units squared, default is 7e6  
        minSizeIntern: Minimum size of a valid internally-drained region, in projection units squared, default is 2.5e5
    """ 

    import arcpy
    import lhm_frontend

    if printResult:
        import os
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Script defaults, shouldn't need to change these
    #-------------------------------------------------------------------------------
    # Get the input names
    (inBdryName,inDEMName,inFillName,inFdirName,inConnWetName,inConnStrName,inConnBuffName,\
        outInternGrid,outBasinID,outSinkID) = lhm_frontend.names_frontend(\
        ['prepBdryBuff','prepDEMRaw','prepDEMFill','prepDEMRawDir','prepConnLines','prepConnLines','prepConnBuff',\
        'prepInternDrain','prepInternBasin','prepInternSink'])

    # Specify input names
    inDEM = inputDB + '/' + inDEMName
    inFill = inputDB + '/' + inFillName
    inFdir = inputDB + '/' + inFdirName
    inConnWet = inputDB + '/' + inConnWetName
    inConnStr = inputDB + '/' + inConnStrName
    inBuffHydro = inputDB + '/' + inConnBuffName
    inMask = inputDB + '/' + inBdryName

    # Handle default params, params are
    paramDefaults = {'sinkDepth':10, 'connDist':4, 'minSizeExtern':100, 'minSizeIntern':2.5e5}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Specify temporary names
    tempSinkGrid = 'temp_sinks_grid'
    tempSinkPoly = 'temp_sinks_polys'
    tempSinkGridDis = 'temp_sinks_disconn_grid'
    tempSinkPolyDis = 'temp_sinks_disconn_polys'
    tempInternGrid = 'temp_intern_grid'
    tempInternFillGrid = 'temp_intern_fill_grid'
    tempBuffHydro = 'temp_buff_hydro'
    layerSinks = 'layer_sinks'

    # Set up the arcpy environment
    arcpy = lhm_frontend.set_environment(inDEM,inputDB,inDEM)

    #-------------------------------------------------------------------------------
    # Do the calculations
    #-------------------------------------------------------------------------------
    # Create a mask of sinks
    rasterFill = arcpy.Raster(inFill)
    rasterDEM = arcpy.Raster(inDEM)
    rasterSinks = arcpy.sa.Con(rasterFill  - rasterDEM > params['sinkDepth'], 1)
    rasterSinks.save(tempSinkGrid)

    # Convert sinks to polygons
    arcpy.RasterToPolygon_conversion(tempSinkGrid,tempSinkPoly,'NO_SIMPLIFY')

    # Create a feature layer
    arcpy.MakeFeatureLayer_management(tempSinkPoly,layerSinks)

    # Select by location
    arcpy.SelectLayerByLocation_management(layerSinks,'WITHIN_A_DISTANCE',inConnStr,params['connDist'],'NEW_SELECTION')
    arcpy.SelectLayerByLocation_management(layerSinks,'WITHIN_A_DISTANCE',inConnWet,params['connDist'],'ADD_TO_SELECTION')
    arcpy.SelectLayerByLocation_management(layerSinks,'','','','SWITCH_SELECTION')

    # Copy layer to features
    arcpy.CopyFeatures_management(layerSinks,tempSinkPolyDis)

    # Determine output cellsize (based on input raster)
    outSize = arcpy.Describe(inDEM).MeanCellHeight

    # Convert to raster
    arcpy.FeatureToRaster_conversion(tempSinkPolyDis,'GRIDCODE',tempSinkGridDis,outSize)

    # Determine the watershed of the sinks
    rasterIntern = arcpy.sa.Watershed(inFdir,tempSinkGridDis)
    rasterIntern.save(tempInternGrid)

    # Replace NULL values with 0
    rasterInternNullTo0 = arcpy.sa.Con(arcpy.sa.IsNull(rasterIntern),0,rasterIntern)

    # Group regions
    rasterRegion = arcpy.sa.RegionGroup(rasterInternNullTo0,'EIGHT')

    # Extract all small non-internally drained regions
    whereClause = 'COUNT < ' + str(int(params['minSizeExtern'] / outSize / outSize)) + ' AND LINK = 0'
    rasterExtract = arcpy.sa.ExtractByAttributes(rasterRegion,whereClause)

    # Replace these values with 1 to indicate it is actually internally drained
    rasterIntern2 = arcpy.sa.Con(arcpy.sa.IsNull(rasterExtract),rasterInternNullTo0,1)

    # Group regions again
    rasterRegion2 = arcpy.sa.RegionGroup(rasterIntern2,'EIGHT')

    # Now, extract all small internally-drained regions
    whereClause = 'COUNT < ' + str(int(params['minSizeIntern'] / outSize / outSize)) + ' AND LINK = 1'
    rasterExtract2 = arcpy.sa.ExtractByAttributes(rasterRegion2,whereClause)

    # Create a filled internally-drained raster from this
    rasterInternFill = arcpy.sa.Con(arcpy.sa.BooleanAnd(arcpy.sa.IsNull(rasterExtract2),rasterIntern2==1),1,0)
    # rasterInternFill = arcpy.sa.Con(arcpy.sa.BooleanAnd(arcpy.sa.IsNull(rasterExtract2),arcpy.sa.BooleanAnd(rasterIntern2,1)),1)
    # mapExpression = 'con( isnull( ' + tempExtract2 + ' ) and ( ' + tempIntern2 + ' == 1 ) , 1 )'
    rasterInternFillExtract = arcpy.sa.ExtractByMask(rasterInternFill,inMask)
    rasterInternFillExtract.save(tempInternFillGrid)

    # Clip out areas in the tempInternFillGrid that fall inside this buffered connected hydro
    arcpy.FeatureToRaster_conversion(inBuffHydro,'GRIDCODE',tempBuffHydro,arcpy.env.cellSize)
    rasterFinal = arcpy.sa.Con(arcpy.sa.IsNull(tempBuffHydro),rasterInternFillExtract,0)
    rasterFinal.save(outInternGrid)

    # Log this output
    lhm_frontend.log_lhm_output(outInternGrid,inputDB,inputDB,printResult)

    # Create the internally drained sink ID raster
    rasterSinksFinal = arcpy.sa.Con(rasterFinal > 0, rasterSinks)
    rasterSinkID = arcpy.sa.RegionGroup(rasterSinksFinal,'EIGHT')
    rasterSinkID.save(outSinkID)

    # Log this output
    lhm_frontend.log_lhm_output(outSinkID,inputDB,inputDB,printResult)

    # Mask the flowdirection raster, run watersheds on the final sinks raster
    rasterFlowdirMask = arcpy.sa.Con(rasterFinal > 0, inFdir)
    rasterSinksWsheds = arcpy.sa.Watershed(rasterFlowdirMask,rasterSinkID)

    # Finally, will need to fill gaps in these that are truly internally drained, just
    # not meeting the sinks depth criterion, so assign them a value of 0 to indicate
    # that the runoff stays in the cell
    rasterSinksWshedsFill = arcpy.sa.Con(arcpy.sa.IsNull(rasterSinksWsheds),\
        arcpy.sa.Con(rasterFinal > 0,0),rasterSinksWsheds)
    rasterSinksWshedsFill.save(outBasinID)

    # Log this output
    lhm_frontend.log_lhm_output(outBasinID,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    # Delete temporary raster objects
    del rasterDEM, rasterExtract, rasterExtract2, rasterFill, rasterFinal, rasterFlowdirMask, \
        rasterIntern, rasterIntern2, rasterInternFill, rasterInternFillExtract, rasterInternNullTo0, rasterRegion, rasterRegion2, \
        rasterSinkID, rasterSinks, rasterSinksFinal, rasterSinksWsheds, rasterSinksWshedsFill

    # first list variables/objects/files to delete
    delLayers = [thisVar for thisVar in dir() if thisVar[0:5]=='layer']
    delTemps = [thisVar for thisVar in dir() if thisVar[0:4]=='temp']

    # Layers
    for delLayer in delLayers:
        arcpy.Delete_management(eval(delLayer))

    # Delete temporary rasters and features
    for delTemp in delTemps:
        arcpy.Delete_management(eval(delTemp))

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Internally-drained areas finished processing for the <strong>%s</strong> region'%regionName))
