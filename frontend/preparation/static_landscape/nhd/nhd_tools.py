def nhd_extract(inputDB,pathConfig,printResult=True):
    # This function merges NHD data for use by LHM

    import arcpy, os
    import lhm_frontend
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Specify inputs and outputs
    #-------------------------------------------------------------------------------
    (configFile,configSection,rowSourceRaw,rowLines,rowPolys,rowCanals,rowDitches,\
        rowReservoirs) = lhm_frontend.names_frontend(['srcFile','srcSectionSurface',\
        'srcHydroRaw','srcHydroLines','srcHydroPolys','srcHydroIrrigCanal',\
        'srcHydroDrainDitch','srcHydroReservoir'])

    # Get the model input locations
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Specify the config file information, corresponds to the outFeatures list
    rowSources = [rowLines,rowPolys]

    # Specify the fields to extract information from
    dictExtract = {'canals':{'inFeature':'NHDFlowline','field':'FType','fieldVal':336,\
        'rowConfig':rowCanals,'outName':'NHDCanals'},'ditches':{'inFeature':'NHDFlowline',\
        'field':'FType','fieldVal':336,'rowConfig':rowDitches,'outName':'NHDDitches'},\
        'reservoirs':{'inFeature':'NHDWaterbody','field':'FType','fieldVal':436,\
        'rowConfig':rowReservoirs,'outName':'NHDReservoirs'}}

    # Specify the output directory
    outputDir = os.path.split(inputDB)[0]
    outputDBName = 'NHD_merge.gdb'
    outputDB = os.path.join(outputDir,outputDBName)

    # Specify outputNames
    outDataset = 'Hydrography'
    outFeatures = ['NHDFlowline','NHDWaterbody']

    # Specify temporary names
    tempSelectLayer = 'temp_select_layer'

    #-------------------------------------------------------------------------------
    # Set up the processing environment, create outputDB if needed
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment('','',inputBdry)
    arcpy.env.outputZFlag = 'Disabled'
    arcpy.env.outputMFlag = 'Disabled'

    # Create the output geodatabase, if needed
    if not os.path.exists(outputDB):
        arcpy.CreateFileGDB_management(outputDir,outputDBName)
    arcpy.env.workspace = outputDB

    # Create the output feature datasets
    arcpy.CreateFeatureDataset_management(outputDB,outDataset)

    # NHD directory
    inDBnhd = inputPaths[rowSourceRaw]

    #-------------------------------------------------------------------------------
    # Clip the datasets and copy to new DB
    #-------------------------------------------------------------------------------
    for thisFeature in outFeatures:
        clipIn = inDBnhd + '/' + outDataset + '/' + thisFeature
        clipOut = outputDB + '/' + outDataset + '/' + thisFeature
        arcpy.Clip_analysis(clipIn,inputBdry,clipOut)

    #-------------------------------------------------------------------------------
    # Extract processed features
    #-------------------------------------------------------------------------------
    for thisExtract in dictExtract.values():
        # Make a selection layer
        inFeature = outputDB + '/' + outDataset + '/' + thisExtract['inFeature']
        whereClause = '"%s" = %s'%(thisExtract['field'],thisExtract['fieldVal'])
        arcpy.MakeFeatureLayer_management(inFeature,tempSelectLayer,whereClause)

        # Copy features to new location
        outFeature = outputDB + '/' + outDataset + '/' + thisExtract['outName']
        arcpy.CopyFeatures_management(tempSelectLayer,outFeature)

        # Log this
        lhm_frontend.log_lhm_output(outDataset + '/' + thisExtract['outName'], outputDB, inputDB)

        # Clean up
        arcpy.Delete_management(tempSelectLayer)

    #-------------------------------------------------------------------------------
    # Finish up
    #-------------------------------------------------------------------------------
    # Update the config file with the location of the processed NHD data
    for (thisSourceInd,rowSource) in enumerate(rowSources):
        lhm_frontend.update_config_sources(os.path.join(pathConfig,configFile),\
                                           configSection,rowSource,\
                                           outputDB+'/'+outDataset+'/'+outFeatures[thisSourceInd])
    for thisExtract in dictExtract.values():
        lhm_frontend.update_config_sources(os.path.join(pathConfig,configFile),\
                                           configSection,thisExtract['rowConfig'],\
                                           outputDB+'/'+outDataset+'/'+thisExtract['outName'])

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('NHD hydrography has finished processing for the <strong>%s</strong> region'%regionName))
