def prep_streams(inputDB, pathConfig, params=dict(), printResult=True):
    """This function prepares stream inputs for LHM using geometries derived
    from USGS gauges at median flow

    Params are:
        accumThreshold: either 'flow' or 'count', means of determining the threshold for indicating a stream
            default is 'flow'
        accumCount: if using accumThreshold='count', the number of cells to use as a stream threshold, default is 3000
        minFlow: if using accumThreshold='flow', the value in m^3/s to use to indicate a stream, default is 0.007 
            (about 0.2 cfs)
        flowFactor: a discharge factor to increase discharge above the low flows to more accurately
            reflect mean flow through the system, default is 1
    """

    import arcpy, os, math, numpy
    import lhm_frontend
    from gis import iterative_spatial_outlier_removal
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Script defaults, shouldn't need to change these
    #-------------------------------------------------------------------------------
    (inputBdryNoBuffName,rowGeom,rowGauge,rowLines,\
        nameWetPres,nameDemFillAccum,nameDemRaw,outDepth,outWidth,outRadius,outVelocity,\
        outLength,outStreams,outSinuosity,outFrac,outElev) = \
        lhm_frontend.names_frontend(['prepBdry','srcHydroGeom','srcHydroFlow','srcHydroLines',\
        'prepWetPres','prepDEMFillAccum','prepDEMRaw',\
        'prepStrDepth','prepStrWidth','prepStrRadius','prepStrVel',\
        'prepStrLength','prepStrPres','prepStrSin','prepStrFrac','prepStrElev'])    

    # Get the model input locations
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Specify standard input locations
    inFlowaccum = inputDB + '/' + nameDemFillAccum
    inRawDEM = inputDB + '/' + nameDemRaw
    wetPres = inputDB + '/' + nameWetPres

    # Parse the input params structure
    paramDefaults = {'accumThreshold':'flow', 'accumCount':3000, 'minFlow':0.007, 'flowFactor':1}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Specify params for the spatial outlier removal
    paramsOutlier = dict()
    paramsOutlier['neighbSize'] = 300*1000 # Specify size of neighborhood to examine outliers, in model units
    paramsOutlier['nsigmaThresh'] = 3 # Number of standard deviations above which to exclude points
    paramsOutlier['nsigmaLargeDevThresh'] = 1 # this is the threshold for nsigma under large std conditions
    paramsOutlier['largeDevThresh'] = 250 # in output units (meters), this is the threshold for neighborhoods with a large standard deviation
    paramsOutlier['nIter'] = 1 # Specify the number of iterations
    paramsOutlier['fieldData'] = 'basinYieldmmpyr'

    # Specify the flow field name in the feature
    fieldFlow = 'Q50'
    fieldBasinArea = 'Basin_Area'

    # Specify sinuosity parameters
    lineDistSin = 50 # in map units
    cellSizeSin = 1000 # in map units
    fieldFishnetID = 'Fish_ID'
    fieldSinuosity = 'Sinuosity'

    # Temporary features
    tempGaugeClip = 'temp_gauge_clip'
    tempGaugeArea = 'temp_gauge_area'
    tempGaugeBY = 'temp_gauge_basin_yield'
    tempGaugeWeight = 'temp_gauge_weight'
    tempBasinYield = 'temp_basin_yield'
    tempDisch = 'temp_discharge'
    tempD_C1 = 'tempGeom_DC1'
    tempD_C2 = 'tempGeom_DC2'
    tempW_C1 = 'tempGeom_WC1'
    tempW_C2 = 'tempGeom_WC2'
    tempR_C1 = 'tempGeom_RC1'
    tempR_C2 = 'tempGeom_RC2'
    tempV_C1 = 'tempGeom_VC1'
    tempV_C2 = 'tempGeom_VC2'
    tempMemPoint = 'temp_mem_point'
    tempSplitPoint = 'temp_split_point'
    tempFishnet = 'temp_fishnet'
    tempJoin = 'temp_join'
    tempPres = 'temp_wet_pres'
    
    tempLayer = 'temp_feature_layer'

    #-------------------------------------------------------------------------------
    # Set up the geoprocessing environment and get layer names, import helper functions
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment(inFlowaccum,inputDB,inputBdry)

    # Get the cellSize and numCells
    inDescribe = arcpy.Describe(inFlowaccum)
    cellSize = inDescribe.MeanCellHeight
    numCells = inDescribe.Height * inDescribe.Width

    #-------------------------------------------------------------------------------
    # Calculate streams grid
    #-------------------------------------------------------------------------------
    # Clip the Q50 values to the boundary, not the buffered one here!
    arcpy.Clip_analysis(inputPaths[rowGauge],inputBdryNoBuffName,tempGaugeClip)

    # Create a selection layer to exclude features with a basin area of 0
    whereStr = '\"%s\" > 0'%fieldBasinArea
    arcpy.MakeFeatureLayer_management(tempGaugeClip,tempLayer,whereStr)
    arcpy.CopyFeatures_management(tempLayer,tempGaugeArea)
    
    # Add a field for basin yield calculation
    arcpy.AddField_management(tempGaugeArea,'basinYieldNative','FLOAT')
    arcpy.AddField_management(tempGaugeArea,'basinYieldmmpyr','FLOAT')
    arcpy.AddField_management(tempGaugeArea,'basinYieldDisch','FLOAT')
    
    # Calculate its value in native units (m^3/s/km^2)
    calcStr = '!%s! / !%s!'%(fieldFlow,fieldBasinArea)
    arcpy.CalculateField_management(tempGaugeArea,'basinYieldNative',calcStr,'PYTHON')

    # Convert to units of mm/yr
    convFac = 86400 * 365.25 / (1000**2) * 1000
    calcStr = '!basinYieldNative! * %s'%str(convFac)
    arcpy.CalculateField_management(tempGaugeArea,'basinYieldmmpyr',calcStr,'PYTHON')

    # Calculate in units of m^3/s/cell (same as the geometry relationships)
    convFac = 1 / (1000**2) * (cellSize**2)
    calcStr = '!basinYieldNative! * %s'%str(convFac)
    arcpy.CalculateField_management(tempGaugeArea,'basinYieldDisch',calcStr,'PYTHON')

    # Now remove outliers spatially
    iterative_spatial_outlier_removal(tempGaugeArea,tempGaugeBY,paramsOutlier)

    # Export the selected features to a raster, using the basinYieldDisch field
    arcpy.FeatureToRaster_conversion(tempGaugeBY,'basinYieldDisch',tempBasinYield,cellSize)
    rasterGaugeWeight = arcpy.Raster(tempBasinYield)

    # Create the mask grid for nibble using map algebra
    rasterMaskNibble = arcpy.sa.SetNull(arcpy.sa.Con(arcpy.sa.IsNull(rasterGaugeWeight),0,1)==0,1)

    # Need an integer weight grid with no nan values
    rasterGaugeWeightInt = arcpy.sa.Int(arcpy.sa.Con(arcpy.sa.IsNull(rasterGaugeWeight),0,rasterGaugeWeight) * numCells)

    # Create discharge weight grid using nibble
    rasterDischWeightInt = arcpy.sa.Nibble(rasterGaugeWeightInt,rasterMaskNibble)
    rasterDischWeightInt.save(tempGaugeWeight)

    # Convert back to a float grid
    rasterDischWeight = arcpy.sa.Float(rasterDischWeightInt) / numCells

    # Multiply the weight grid by the flowaccumulation grid
    rasterDischAccum = inFlowaccum * rasterDischWeight

    # Multiply the discharge accumulation grid by the flow factor
    rasterDischScaled = rasterDischAccum * params['flowFactor']

    if params['accumThreshold'] == 'flow':
        # Create the streams grid
        rasterStreams = arcpy.sa.Con(rasterDischScaled > params['minFlow'], 1, 0)
        rasterStreams.save(outStreams)

        # Log this output
        lhm_frontend.log_lhm_output(outStreams,inputDB,inputDB,printResult)
        
    elif params['accumThreshold'] == 'count':
        # Just use a count of cells in the flow accumulation raster to define streams
        rasterAccum = arcpy.Raster(inFlowaccum)
        rasterStreams = arcpy.sa.Con(rasterAccum > params['accumCount'], 1, 0)
        rasterStreams.save(outStreams)

        # Log this output
        lhm_frontend.log_lhm_output(outStreams,inputDB,inputDB,printResult)

    # Clip the accumulation grid to the streams grid
    rasterStreamNoData = arcpy.sa.Con(rasterStreams==1,rasterStreams)
    rasterDischClip = rasterStreamNoData * rasterDischScaled
    rasterDischClip.save(tempDisch)
    #-------------------------------------------------------------------------------
    # Calculate geometries
    #-------------------------------------------------------------------------------
    # Calculate the depths grid
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'D_C1',tempD_C1,cellSize)
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'D_C2',tempD_C2,cellSize)
    rasterDepth = tempD_C1 * (rasterDischClip ** tempD_C2)
    rasterDepth.save(outDepth)

    # Log this output
    lhm_frontend.log_lhm_output(outDepth,inputDB,inputDB,printResult)

    # Calculate the widths grid
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'W_C1',tempW_C1,cellSize)
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'W_C2',tempW_C2,cellSize)
    rasterWidth = tempW_C1 * (rasterDischClip ** tempW_C2)
    rasterWidth.save(outWidth)

    # Log this output
    lhm_frontend.log_lhm_output(outWidth,inputDB,inputDB,printResult)

    # Calculate the hydraulic radius grid
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'R_C1',tempR_C1,cellSize)
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'R_C2',tempR_C2,cellSize)
    rasterRadius = tempR_C1 * (rasterDischClip ** tempR_C2)
    rasterRadius.save(outRadius)

    # Log this output
    lhm_frontend.log_lhm_output(outRadius,inputDB,inputDB,printResult)

    # Calculate the average channel velocity grid
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'V_C1',tempV_C1,cellSize)
    arcpy.FeatureToRaster_conversion(inputPaths[rowGeom],'V_C2',tempV_C2,cellSize)
    rasterVelocity = tempV_C1 * (rasterDischClip ** tempV_C2)
    rasterVelocity.save(outVelocity)

    # Log this output
    lhm_frontend.log_lhm_output(outVelocity,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Extract elevation of streams
    #-------------------------------------------------------------------------------
    rasterDEM = arcpy.Raster(inRawDEM)
    rasterStreamElev = arcpy.sa.Con(rasterStreams==1,rasterDEM)
    rasterStreamElev.save(outElev)

    # Log this output
    lhm_frontend.log_lhm_output(outElev,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Sinuosity and Stream Length Calculation
    #-------------------------------------------------------------------------------
    # Modified from create_points_on_polylines by Ian Broad, www.ianbroad.com
    # Create a new feature class in memory
    mem_point = arcpy.CreateFeatureclass_management("in_memory", tempMemPoint, "POINT", "", "DISABLED", "DISABLED", inputPaths[rowLines])
    arcpy.AddField_management(mem_point, "LineOID", "TEXT")
    arcpy.AddField_management(mem_point, "Distance", "FLOAT")
    arcpy.AddField_management(mem_point, fieldSinuosity, "FLOAT")

    # Count the number of features in the input line feature
    # result = arcpy.GetCount_management(inputPaths[rowLines])
    # features = int(result.getOutput(0))

    # Fields to search
    fields = ["SHAPE@", "OID@"]

    # Do the work of creating the points
    with arcpy.da.SearchCursor(inputPaths[rowLines], (fields)) as search:
        with arcpy.da.InsertCursor(mem_point, ("SHAPE@", "LineOID", "Distance", fieldSinuosity)) as insert:
            for row in search:
                line_geom = row[0]
                length = float(line_geom.length)
                count = lineDistSin
                oid = str(row[1])

                # Get the x,y coordinates of the first point
                firstPoint = line_geom.firstPoint
                lastCoords = [firstPoint.X,firstPoint.Y]

                while count <= length:
                    # Get this point
                    point = line_geom.positionAlongLine(count, False)
                    # Get the x,y of this point
                    thisCoords = [point.centroid.X,point.centroid.Y]
                    # Calculate distance and sinuosity
                    thisDist = math.sqrt((thisCoords[0]-lastCoords[0])**2 + (thisCoords[1]-lastCoords[1])**2)
                    thisSin = lineDistSin / thisDist
                    # Insert this point
                    insert.insertRow((point, oid, count, thisSin))
                    # Increment counter, rest lastCoords
                    count += lineDistSin
                    lastCoords = thisCoords

    # Copy these points to a temporary layer
    arcpy.CopyFeatures_management(mem_point,tempSplitPoint)

    # Clean up
    arcpy.Delete_management(mem_point)

    # Create a fishnet at the required resolution
    arcpy.CreateFishnet_management(tempFishnet,str(inDescribe.extent.XMin)+' '+str(inDescribe.extent.YMin),\
        str(inDescribe.extent.XMin)+' '+str(inDescribe.extent.YMin+1000),\
        str(cellSizeSin),str(cellSizeSin),'0','0',\
        str(inDescribe.extent.XMax)+' '+str(inDescribe.extent.YMax),'NO_LABELS','','POLYGON')

    # Add an ID field and calculate its value
    arcpy.AddField_management(tempFishnet,fieldFishnetID,'LONG')
    arcpy.CalculateField_management(tempFishnet,fieldFishnetID,"!OID!+1",'PYTHON')

    # Spatial join the fishnet to the points
    arcpy.SpatialJoin_analysis(tempSplitPoint,tempFishnet,tempJoin)

    # Read in this table directly
    arrayID = arcpy.da.FeatureClassToNumPyArray(tempJoin,fieldFishnetID)
    arraySin = arcpy.da.FeatureClassToNumPyArray(tempJoin,fieldSinuosity)

    # Sinuosity calculations
    uniqueIDs = numpy.unique(arrayID)
    avgSin = numpy.zeros([numpy.shape(uniqueIDs)[0]],dtype=[(fieldFishnetID,'<i4'),(fieldSinuosity,'<f4')])
    count = 0
    for uniqueID in uniqueIDs[fieldFishnetID]:
        thisInd = arrayID[fieldFishnetID] == uniqueID
        avgSin[fieldFishnetID][count] = uniqueID
        avgSin[fieldSinuosity][count] = numpy.mean(arraySin[fieldSinuosity][thisInd])
        count += 1

    # Create a field in tempFishnet, calculate value as 1
    arcpy.AddField_management(tempFishnet,fieldSinuosity,'FLOAT')
    arcpy.CalculateField_management(tempFishnet,fieldSinuosity,'1','Python')

    # Make sure no calculated avgSin < 1
    testInd = avgSin[fieldSinuosity] < 1
    avgSin[fieldSinuosity][testInd] = 1

    # Add this back to the fishnets directly, updating values
    arcpy.da.ExtendTable(tempFishnet,fieldFishnetID,avgSin,fieldFishnetID,False)

    # Convert this fishnet to a grid
    arcpy.FeatureToRaster_conversion(tempFishnet,fieldSinuosity,outSinuosity,cellSize)

    # Calculate the sinuous length, assume random orientation length factor (1.20711)
    rasterSinuos = arcpy.Raster(outSinuosity)
    rasterLength = rasterSinuos * rasterStreamNoData * cellSize * 1.20711
    rasterLength.save(outLength)

    # Log these outputs
    lhm_frontend.log_lhm_output(outSinuosity,inputDB,inputDB,printResult)
    lhm_frontend.log_lhm_output(outLength,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Stream Fraction
    #-------------------------------------------------------------------------------
    # First, resample the wetPres grid to a temp grid to match resolution of streams
    arcpy.Resample_management(wetPres, tempPres, cellSize, "BILINEAR")

    # Create a streams grid that does not overlap the wetlands
    rasterWetFrac = arcpy.Raster(tempPres)
    rasterNoOverlap = arcpy.sa.Con(rasterWetFrac==0,rasterStreams,0)

    # Now, calculate the fraction of the cell covered by the stream
    rasterStrFrac = rasterNoOverlap * rasterLength * rasterWidth / (cellSize**2)
    rasterStrFrac.save(outFrac)

    # Log this output
    lhm_frontend.log_lhm_output(outFrac,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    # Delete temporary raster objects
    del	rasterDepth, rasterLength, rasterNoOverlap, rasterRadius, rasterSinuos, rasterStreams, \
        rasterVelocity, rasterWetFrac, rasterWidth, rasterStreamNoData, rasterStrFrac, rasterDEM, \
        rasterStreamElev

    if params['accumThreshold'] == 'flow':
    	del rasterDischAccum, rasterDischClip, rasterDischScaled, rasterDischWeight, \
    	    rasterDischWeightInt, rasterGaugeWeight, rasterGaugeWeightInt, rasterMaskNibble
    elif params['accumThreshold'] == 'count':
    	del rasterAccum

    # first list variables/objects/files to delete
    # delLayers = [thisVar for thisVar in dir() if thisVar[0:5]=='layer']
    delTemps = [thisVar for thisVar in dir() if thisVar[0:4]=='temp']

    # Delete temporary rasters and features
    for delTemp in delTemps:
        arcpy.Delete_management(eval(delTemp))

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Streams are prepared for the <strong>%s</strong> region'%regionName))
