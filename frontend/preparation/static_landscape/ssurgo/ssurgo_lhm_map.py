def ssurgo_lhm_map(inputDB, pathConfig, params=dict(), printResult=True, reloadData=False):
    #%%
    # Available params are:
    #   'layerTop' and 'layerBot' which must be both specified, and are lists of depths to process layers for
    #   'datasets' which is a list of datasets to write out, check the code below <-- not implemented yet
    # 
    # Import modules
    import arcpy, os, numpy, pandas, re
    from tqdm.auto import tqdm # tqdm is a progress bar module
    if printResult:
        from IPython.display import display, HTML

    # Ignore numpy warnings in this module
    numpy.seterr(divide='ignore', invalid='ignore')

    # Ignore the pandas setting with copy warning
    pandas.options.mode.chained_assignment = None

    stdInput = True # set to false if using custom inputs, specify below

    params
    # Get the layering from params
    if 'layerTop' not in params.keys():
        layerTop = numpy.array([0,20,50,100],float) # in cm
    else:
        layerTop = numpy.array(params['layerTop'],float)
    if 'layerBot' not in params.keys():
        layerBot = numpy.array([20,50,100,300],float)
    else:
        layerBot = numpy.array(params['layerBot'],float)
    layerThick = layerBot - layerTop
    numLay = len(layerTop)

    #-------------------------------------------------------------------------------
    # Advanced/Standardized Inputs
    #-------------------------------------------------------------------------------
    if stdInput:
        import lhm_frontend

        # Get the standard variables
        [configFileNotebook,configFileSources,configSectionNotebook,configSectionSources,rowFrontend,\
            rowSource,rowSourceRaw,outDBName,subDirWorking] = \
            lhm_frontend.names_frontend(['nbFile','srcFile','nbSectionFrontend','srcSectionSurface','nbFrontend',\
            'srcSoils','srcSoilsRaw','gdbSoils','dirWorking'])

        # Get the model standard input locations
        (inputSources,featBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

        # Output Directory
        outDir = os.path.split(inputDB)[0]

        # Get the model input locations
        inputPaths = lhm_frontend.read_config_dict(os.path.join(pathConfig,configFileNotebook),configSectionNotebook)

        # Get these from the inputDB
        frontendDir = inputPaths[rowFrontend]
        featBdry = os.path.join(inputDB,featBdry)
        dirSoilData = os.path.join(frontendDir,'preparation\\static_landscape\\ssurgo')

        # Specify options
        optNibble = True
        buildPyramids = 'PYRAMIDS'

    else:
    #    featBdry = 'S:\\Data\\GIS_Data\\Derived\\LP_Model\\Counties\\four_counties_sw_mi_soils_testing.shp'
        featBdry = 'S:\\Data\\GIS_Data\\Derived\\Nationwide\\Boundaries\\CONUS_states_dissolve_albers.shp'
        outDir = 'F:\\Users\\Anthony\\Processing\\Nationwide_SSURGO'
        subDirWorking = '';
    #    outDBName = 'nationwide_gssurgo_testing.gdb'
        outDBName = 'nationwide_gssurgo.gdb'

        # Specify options
        optNibble = False
        buildPyramids = 'NONE'

    #------------------------------------------------------------------------------
    # New Stuff
    #------------------------------------------------------------------------------
    # Specify databases
    gssurgoDB = inputSources[rowSourceRaw]
    inMukey = 'MapunitRaster_30m'

    # Fields for pandas import
    fieldsComponent = ['mukey','cokey','comppct_r']
    fieldsChtexturegrp = ['chkey','chtgkey','texdesc_255'] # texdesc_255 is a new field that this script makes
    fieldsChorizon = ['chkey','cokey','hzdept_r','hzdepb_r','sandtotal_r','claytotal_r','fraggt10_r','frag3to10_r','om_r','dbovendry_r',]
    fieldsChtexture = ['texcl','chtgkey','chtkey']

    # Specify fields and averaging types for standardized layering
    layersFieldsComp = ['sandtotal_r','claytotal_r','fraggt10_r','frag3to10_r','om_r','dbovendry_r']
    layersFieldsProp = ['theta_sat','theta_resid','alpha','n','ksat','k0',\
                        'L','infil_cap','wilt_pt','dry_albedo','field_cap','bub_press',\
                        'pore_size','Class_ID','ksat_h']
    layersAvgTypeProp = ['arithmetic','arithmetic','arithmetic','arithmetic','harmonic','harmonic',\
                         'arithmetic','arithmetic','arithmetic','first','arithmetic','arithmetic',\
                         'arithmetic','mode','arithmetic']

    # Specify output names, fields and types
    outFieldNames = ['mukey', 'laynum', 'laytop', 'laybot'] + layersFieldsComp + layersFieldsProp
    outNullValue = -9999
    outMukey = 'mukey'
    outFieldTypes = ['int64','int8','int16','int16'] + ['float64','float64','float64','float64','float64','float64'] + \
        ['float64','float64','float64','float64','float64','float64','float64','float64','float64','float64',\
        'float64','float64','float64','int8'] + ['float64']

    # Specify map fields
    mapFieldNames = ['laytop','laybot'] + layersFieldsComp + layersFieldsProp

    # Specify table names
    tableMapUnitProps = 'mapunit_layer_properties'

    # Assign temporary names created when exporting
    tempMukey = 'tempMukey'
    tempRaster = 'tempRaster'
    tempTable = 'tempTable'
    tempMask = 'tempMask'
    tempJoin = 'tempJoin'
    tempBdryFeat = 'tempBdryFeat'
    tempBdryRaster = 'tempBdry'

    #-------------------------------------------------------------------------------
    # Prepare the environment settings
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment(os.path.join(gssurgoDB,inMukey),'',featBdry)
    arcpy.env.pyramid = buildPyramids # update based on preferences

    # Create the output geodatabase
    outDB = outDir + '/' + outDBName
    if not os.path.exists(outDB):
        arcpy.CreateFileGDB_management(outDir,outDBName)
    arcpy.env.workspace = outDB

    # Prepare the temporary datastore
    if len(subDirWorking)>0:
        outDirStore = os.path.join(outDir,subDirWorking)
    else:
        outDirStore = outDir
    outFileStore = os.path.join(outDirStore,'temp.h5')
    if not reloadData:
        if os.path.exists(outFileStore):
            os.remove(outFileStore)

    #-------------------------------------------------------------------------------
    # Helper functions
    #-------------------------------------------------------------------------------
    def safe_delete(obj):
        try:
            arcpy.Delete_management(obj)
        except:
            print('Manually delete %s, it failed to delete'%(obj))

    #%%------------------------------------------------------------------------------
    # Determine extent and mukeys of region to process
    #------------------------------------------------------------------------------
    if not reloadData:
        print('Extracting analysis region from gSSURGO dataset')

        # Extract by mask
        rasterMukey = arcpy.sa.ExtractByMask(os.path.join(gssurgoDB,inMukey),featBdry)
        rasterMukey.save(outMukey)
        arcpy.BuildRasterAttributeTable_management(outMukey, "Overwrite")

        # Pull in the list of mukeys to select against
        dfMukey= pandas.DataFrame(arcpy.da.TableToNumPyArray(os.path.join(outDB,outMukey),['VALUE']))
        dfMukey = dfMukey.rename(columns={'VALUE':'mukey'})
        dfMukey['mukey'] = dfMukey['mukey'].astype(numpy.dtype('U50'))

    #%% ---------------------------------------------------------------------------
    # Prepare the data
    #------------------------------------------------------------------------------
    if not reloadData:
        print('Extracting data from the databases')
        ## Bring in the textural properties from ROSETTA       
        dfPctToClass = pandas.read_csv(os.path.join(dirSoilData,'percentages_to_class.csv'))
        dfTexclToClass = pandas.read_csv(os.path.join(dirSoilData,'texcl_to_class.csv'))
        dfClassNameToID = pandas.read_csv(os.path.join(dirSoilData,'class_to_properties.csv'))
        dfClassNameToID = dfClassNameToID[['Class_ID','Class_name']]
        dfClassToPct = pandas.read_csv(os.path.join(dirSoilData,'class_to_percentages.csv'))
        dfClassToProp = pandas.read_csv(os.path.join(dirSoilData,'class_to_properties.csv'))
        dfPctToProp = pandas.read_csv(os.path.join(dirSoilData,'percentages_to_properties.csv'))

        ## Bring in the SSURGO tables from the geodatabase
        arcpy.env.workspace = gssurgoDB
        dfComponent = pandas.DataFrame(arcpy.da.TableToNumPyArray('component',field_names=fieldsComponent,null_value='-9999'))
        dfChorizon = pandas.DataFrame(arcpy.da.TableToNumPyArray('chorizon',field_names=fieldsChorizon,null_value='-9999'))
        dfChtexture = pandas.DataFrame(arcpy.da.TableToNumPyArray('chtexture',field_names=fieldsChtexture,null_value='-9999'))
        # For chtexturegrp, add a length 255 version of texdesc
        fieldsExist = [field.name for field in arcpy.ListFields('chtexturegrp')]
        if 'texdesc_255' not in fieldsExist:
            result = arcpy.AddField_management('chtexturegrp','texdesc_255',field_type='TEXT',field_length='255')
            result = arcpy.CalculateField_management('chtexturegrp','texdesc_255','!texdesc!','PYTHON_9.3')
        dfChtexturegrp = pandas.DataFrame(arcpy.da.TableToNumPyArray('chtexturegrp',field_names=fieldsChtexturegrp,null_value='-9999'))
        dfChtexturegrp = dfChtexturegrp.rename(columns={'texdesc_255':'texdesc'})

        ## Subselect only MUKEYs in active region, and trim the two texture tables to only use the first value for each key
        dfComponent = dfComponent.loc[dfComponent['mukey'].isin(dfMukey['mukey']),:].copy()
        dfChorizon = dfChorizon.loc[dfChorizon['cokey'].isin(dfComponent['cokey']),:].copy()
        dfChorizon = dfChorizon.drop_duplicates() # these are remanent from the state-by-state merging I think
        dfChtexturegrp = dfChtexturegrp.loc[dfChtexturegrp['chkey'].isin(dfChorizon['chkey']),:].copy()
        dfChtexturegrp = dfChtexturegrp.groupby(['chkey']).first().reset_index()
        dfChtexture = dfChtexture.loc[dfChtexture['chtgkey'].isin(dfChtexturegrp['chtgkey']),:].copy()
        dfChtexture = dfChtexture.groupby(['chtgkey']).first().reset_index()

        #%% ---------------------------------------------------------------------------
        # Create horizon composition and properties tables
        #------------------------------------------------------------------------------
        print('Calculating composition and properties')
        ## Create horizon_composition and horizon_properties tables
        dfHorizonComp = dfChorizon.copy()
        dfHorizonComp = dfHorizonComp.merge(dfChtexturegrp,on='chkey',how='left')
        dfHorizonComp = dfHorizonComp.merge(dfChtexture,on='chtgkey',how='left')

        ## Process the different textural classification methods
        # Assign Class_ID from texcl_to_class
        dfHorizonComp.loc[:,'Class_ID'] = pandas.DataFrame(dfHorizonComp.loc[:,'texcl']).copy().\
            merge(dfTexclToClass.loc[:,['texcl','Class_ID']],on='texcl',how='left').\
            drop(['texcl'],axis=1).values

        ## Big Step: Need to assign Class_ID with nothing other than texdesc to go by
        # Assign nan Class_ID from texdesc_to_class, use regular expressions to parse
        testNan = numpy.isnan(dfHorizonComp.loc[:,'Class_ID'])
        replaceStrings = {'marly material':'muck','peat':'muck','plant material':'muck','herbaceous material':'muck',\
                          'coprogenous material':'silty clay loam','coprogenous earth':'silty clay loam','diatomaceous earth':'silty clay loam',\
                          'diatomaceous material':'silty clay loam','hm':'muck','dia':'muck',\
                          'cemented material':'bedrock','duripan':'bedrock','petrocalcic':'bedrock',\
                          'cemented':'bedrock','indurated':'bedrock','permafrost':'bedrock','permanently frozen material':'bedrock',\
                          'permanently frozen water':'water','-9999':'none','variable':'sandy clay loam'}
        replaceDuplicates = {'bedrock bedrock':'bedrock','muck muck':'muck'}
        replaceWords = {'sand':{'findString':r'\bs\b','replaceString':'sand'},'gravel':{'findString':r'\bg\b','replaceString':'gravel'},\
                        'muck':{'findString':r'\bmarl\b','replaceString':'muck'}}
        regExpStr = r'(sandy?|clay|silty?|loamy?|bedrock|\bmuck\b|water)'
        regExp = re.compile(regExpStr)
        testGroup = dfHorizonComp.loc[testNan,'texdesc'].fillna('none').values
        # Return only values before "to" or ","
        testGroup = [re.split(',|to',thisString)[0].strip() for thisString in testGroup] # pick the first
        # Replace words with synonyms
        for string in replaceStrings:
            testGroup = [re.sub(string,replaceStrings[string],thisString.lower()) for thisString in testGroup]
        for word in replaceWords:
            testGroup = [re.sub(replaceWords[word]['findString'],replaceWords[word]['replaceString'],thisString.lower()) for thisString in testGroup]
        # Now match
        arrayTexdescToClass = [' '.join(regExp.findall(thisDesc)) for thisDesc in testGroup]
        # For various reasons, some duplicates
        for dup in replaceDuplicates:
            arrayTexdescToClass = [thisString.replace(dup,replaceDuplicates[dup]) for thisString in arrayTexdescToClass]
        # Now join
        dfHorizonComp.loc[testNan,'Class_ID'] = pandas.DataFrame(arrayTexdescToClass,columns=['Class_name']).\
            merge(dfClassNameToID,on='Class_name',how='left').drop('Class_name',axis=1).values


        ## Now, we need to do a second set of matching for gravel types
        replaceStrings = {'boulders?y?':'gravel','cobbl(es)?y?':'gravel','artifacts':'gravel','ston(es)?y?':'gravel','paragravel':'gravel',\
                          'cinders':'gravel','channers?y?':'gravel','parachanners':'gravel','gravelly':'gravel',\
                          'pumiceous':'gravel','flagstones':'gravel','fragmental material':'gravel','flags':'gravel'}
        replaceDuplicates = {'gravel gravel':'gravel'}
        regExpStr = r'(gravel)'
        regExp = re.compile(regExpStr)
        testNan = numpy.isnan(dfHorizonComp.loc[:,'Class_ID'])
        testGroup = dfHorizonComp.loc[testNan,'texdesc'].fillna('none').values
        # Return only values before "to" or ","
        testGroup = [re.split(',|to',thisString)[0].strip() for thisString in testGroup] # pick the first
        # Replace words with synonyms
        for string in replaceStrings:
            testGroup = [re.sub(string,replaceStrings[string],thisString.lower()) for thisString in testGroup]
        for word in replaceWords:
            testGroup = [re.sub(replaceWords[word]['findString'],replaceWords[word]['replaceString'],thisString.lower()) for thisString in testGroup]
        # Now match
        arrayTexdescToClass = [' '.join(regExp.findall(thisDesc)) for thisDesc in testGroup]
        # For various reasons, some duplicates
        for dup in replaceDuplicates:
            arrayTexdescToClass = [thisString.replace(dup,replaceDuplicates[dup]) for thisString in arrayTexdescToClass]
        # Change the gravel to sand for the purposes of our further processing, hydraulically the most similar
        arrayTexdescToClass = [thisString.replace('gravel','sand') for thisString in arrayTexdescToClass]
        # Now join
        dfHorizonComp.loc[testNan,'Class_ID'] = pandas.DataFrame(arrayTexdescToClass,columns=['Class_name']).\
            merge(dfClassNameToID,on='Class_name',how='left').drop('Class_name',axis=1).values

        ## Process the Percentage_ID from %sand, %clay
        # Make sand and clay dataframes for this analysis
        dfSandClay = dfHorizonComp[['chkey','sandtotal_r','claytotal_r']]
        dfPercentID = dfPctToClass[['Percentage_ID','percentSand','percentClay']]

        # Round down the %sand and # clay
        dfSandClay.loc[:,'sandtotal_r'] = numpy.floor(dfSandClay.loc[:,'sandtotal_r'].copy())
        dfSandClay.loc[:,'claytotal_r'] = numpy.floor(dfSandClay.loc[:,'claytotal_r'].copy())
        dfSandClay = dfSandClay.fillna(-9999)
        soilSumDiff = numpy.sum(dfSandClay.loc[:,['sandtotal_r','claytotal_r']].values,axis=1) - 100
        testHigh = soilSumDiff > 0
        dfSandClay.loc[testHigh,'sandtotal_r'] = numpy.floor(dfSandClay.loc[testHigh,'sandtotal_r'].copy() - soilSumDiff[testHigh]/2)
        dfSandClay.loc[testHigh,'claytotal_r'] = numpy.floor(dfSandClay.loc[testHigh,'claytotal_r'].copy() - soilSumDiff[testHigh]/2)

        # Join to the dfPercentID table, will still be NaN in Percentage_ID because of missing sandtotal_r and/or claytotal_r
        dfSandClay = dfSandClay.merge(dfPercentID,right_on=['percentSand','percentClay'],left_on=['sandtotal_r','claytotal_r'],how='left')

        # Add to the dfHorizonComposition
        dfHorizonComp = dfHorizonComp.merge(dfSandClay[['Percentage_ID','chkey']],on='chkey',how='left')

        ## If %sand and %clay are missing from the database, then update with values based solely on Class_ID
        # Update the Percentage_ID from the Class_ID if Percentage_ID is NaN
        testNan = dfHorizonComp['Percentage_ID'].isnull()
        dfHorizonComp.loc[testNan,'Percentage_ID'] = pandas.DataFrame(dfHorizonComp.loc[testNan,['Class_ID']]).copy().merge(\
                         dfClassToPct,on='Class_ID',how='left')['Percentage_ID'].values

        # Assign %sand and %clay using the Percentage_ID for these features
        dfSandClayUpdate = dfHorizonComp.loc[testNan,['chkey','Percentage_ID']].merge(dfPercentID,on=['Percentage_ID'],how='left')
        dfSandClayUpdate = dfSandClayUpdate.rename(columns={'percentSand':'sandtotal_r','percentClay':'claytotal_r'})
        dfHorizonComp = dfHorizonComp.set_index('chkey')
        dfSandClayUpdate = dfSandClayUpdate.set_index('chkey')
        dfHorizonComp.update(dfSandClayUpdate)
        dfHorizonComp = dfHorizonComp.reset_index()

        # Assign Class_ID_percent from Percentage_ID
        dfHorizonComp.loc[:,'Class_ID_percent'] = pandas.DataFrame(dfHorizonComp.loc[:,'Percentage_ID']).\
            merge(dfPctToClass.loc[:,['Percentage_ID','Class_ID']],on='Percentage_ID',how='left').\
            drop(['Percentage_ID'],axis=1).values

        ## Harmonize the two methods for getting percentages
        testNotNan = numpy.logical_and(numpy.logical_not(dfHorizonComp['Class_ID_percent'].isnull()),\
                                       numpy.logical_not(dfHorizonComp['Class_ID'].isnull()))
        testTexture = dfHorizonComp['Class_ID']<13 # don't include muck, bedrock, or water
        testNotEqual = numpy.logical_not(dfHorizonComp['Class_ID_percent'].values==dfHorizonComp['Class_ID'].values)
        testHarmonize = numpy.logical_and(testNotEqual,numpy.logical_and(testNotNan,testTexture))
        dfHarmonize = dfHorizonComp.loc[testHarmonize,['chkey','sandtotal_r','claytotal_r','Class_ID']].merge(dfClassToPct.merge(\
                                       dfPctToClass[['Percentage_ID','percentSand','percentClay']],on='Percentage_ID',how='left'),\
                                       on='Class_ID',how='inner')

        # Average the two methods
        dfHarmonize['sandavg'] = numpy.floor(numpy.nanmean([dfHarmonize['sandtotal_r'],dfHarmonize['percentSand']],axis=0))
        dfHarmonize['clayavg'] = numpy.floor(numpy.nanmean([dfHarmonize['claytotal_r'],dfHarmonize['percentClay']],axis=0))
        soilSumDiff = numpy.sum(dfHarmonize.loc[:,['sandavg','clayavg']].values,axis=1) - 100
        testHigh = soilSumDiff > 0
        dfHarmonize.loc[testHigh,'sandavg'] = numpy.floor(dfHarmonize.loc[testHigh,'sandavg'].copy() - soilSumDiff[testHigh]/2)
        dfHarmonize.loc[testHigh,'clayavg'] = numpy.floor(dfHarmonize.loc[testHigh,'clayavg'].copy() - soilSumDiff[testHigh]/2)
        dfHarmonize = dfHarmonize.drop(['sandtotal_r','claytotal_r','percentSand','percentClay','Class_ID','Percentage_ID'],axis=1)
        dfHarmonize = dfHarmonize.merge(dfPercentID,right_on=['percentSand','percentClay'],left_on=['sandavg','clayavg'],how='left')

        # Update the Class_ID in dfHarmonize
        dfHarmonize['Class_ID'] = dfHarmonize[['Percentage_ID']].merge(\
                   dfPctToClass[['Percentage_ID','Class_ID']],on='Percentage_ID',how='left').\
                   drop(['Percentage_ID'],axis=1).values

        # Prepare to update the dfHorizonComp
        dfHarmonize = dfHarmonize.drop(['percentSand','percentClay'],axis=1)
        dfHarmonize = dfHarmonize.rename(columns={'sandavg':'sandtotal_r','clayavg':'claytotal_r'})

        # Update the values of dfHorizonComp, updates Percentage_ID, sandtotal_r, claytotal_r
        dfHorizonComp = dfHorizonComp.set_index('chkey')
        dfHorizonComp.update(dfHarmonize.set_index('chkey'))
        dfHorizonComp = dfHorizonComp.reset_index()

        # Finally, set any remaining Class_ID values to water (15), because these lack any textural information and will be omitted later
        dfHorizonComp['Class_ID'] = dfHorizonComp['Class_ID'].fillna(15)

        ## Next, create the horizon properties
        # Create the horizon properties dataframe
        dfHorizonProp = dfHorizonComp[['chkey','Percentage_ID','Class_ID']].merge(dfPctToProp,on='Percentage_ID',how='left')
        dfHorizonProp = dfHorizonProp.merge(dfClassToProp,on='Class_ID',how='inner')

        # Reset -9999 to NaN in both dataframes
        testNan = dfHorizonProp==-9999 # these were set at -9999 somewhere previously
        dfHorizonProp[testNan] = numpy.NaN
        testNan = dfHorizonComp==-9999 # these were set at -9999 somewhere previously
        dfHorizonComp[testNan] = numpy.NaN

        # Calculate DUL (field capacity) and LL (wilting point)----------------------
        # This will average the two values, (From Ritchie et al. 1999) and from (FILL IN REF)
        # It also constrains to lie between theta_sat and theta_resid
        dfHorizonAll = dfHorizonProp.merge(dfHorizonComp,on='chkey',how='inner')
        dfTest = dfHorizonAll[['chkey','field_cap','wilt_pt','sandtotal_r','claytotal_r','dbovendry_r']]
        testCalc = numpy.logical_not(dfTest.drop(['chkey'],axis=1).isnull()).any(axis=1)
        dfTest = pandas.DataFrame(dfTest.loc[testCalc,['chkey']])
        dfHorizonCalc = dfHorizonAll.merge(dfTest,on='chkey',how='inner')[['chkey','sandtotal_r','claytotal_r',\
                                          'dbovendry_r','theta_sat','theta_resid','wilt_pt','field_cap']]
        del dfHorizonAll # clean this big one out of memory

        # This uses Ritchie et al. 1999, modified to limit values at 0.3 by mass, from Figure 1 of their paper
        dfHorizonCalc['fieldCapMass'] = (0.186 * (dfHorizonCalc['sandtotal_r']/dfHorizonCalc['claytotal_r']) ** -0.141)
        testLimit = dfHorizonCalc['fieldCapMass']>0.3
        dfHorizonCalc.loc[testLimit,'fieldCapMass'] = 0.3
        dfHorizonCalc['fieldCapRitchie'] = dfHorizonCalc['fieldCapMass'] * dfHorizonCalc['dbovendry_r']
        dfHorizonCalc['wiltPtRitchie'] =  0.132 - 2.5e-6 * numpy.exp(0.105 * dfHorizonCalc['sandtotal_r'])

        # Now, average with prior estimate from textural class
        dfHorizonCalc['fieldCapAvg'] = numpy.mean([dfHorizonCalc.loc[:,'field_cap'],dfHorizonCalc.loc[:,'fieldCapRitchie']],axis=0)
        dfHorizonCalc['wiltPtAvg'] = numpy.mean([dfHorizonCalc.loc[:,'wilt_pt'],dfHorizonCalc.loc[:,'wiltPtRitchie']],axis=0)

        # Limit to range in the original table values
        testLim = dfHorizonCalc['fieldCapAvg'] > dfHorizonCalc['theta_sat']
        dfHorizonCalc.loc[testLim,'fieldCapAvg'] = dfHorizonCalc.loc[testLim,'field_cap']
        testLim = dfHorizonCalc['wiltPtAvg'] < dfHorizonCalc['theta_resid']
        dfHorizonCalc.loc[testLim,'wiltPtAvg'] = dfHorizonCalc.loc[testLim,'wilt_pt']

        # Update the original table with these values
        dfHorizonCalc = dfHorizonCalc.drop(['sandtotal_r','claytotal_r','dbovendry_r','theta_sat','theta_resid',\
                                            'wilt_pt','field_cap','fieldCapMass','fieldCapRitchie','wiltPtRitchie'],axis=1)
        dfHorizonCalc = dfHorizonCalc.rename(columns={'fieldCapAvg':'field_cap','wiltPtAvg':'wilt_pt'})
        dfHorizonProp = dfHorizonProp.set_index('chkey')
        dfHorizonProp.update(dfHorizonCalc.set_index('chkey'))
        dfHorizonProp = dfHorizonProp.reset_index()


        #%%------------------------------------------------------------------------------
        # Calculate weighted averages for horizon properties within standard layers
        #------------------------------------------------------------------------------
        # First, set the bottom depth, if empty, to the bottom of the lowest layer, and the top to 0
        testBotNull = numpy.isnan(dfHorizonComp['hzdepb_r'])
        dfHorizonComp.loc[testBotNull,'hzdepb_r'] = layerBot[-1]
        testTopNull = numpy.isnan(dfHorizonComp['hzdept_r'])
        dfHorizonComp.loc[testTopNull,'hzdept_r'] = 0

        # Next, set the bottom depth of bedrock type with 0 as the bottom depth to the bottom of the lowest layer
        testBedBot0 = numpy.logical_and(dfHorizonComp['Class_ID']==13,dfHorizonComp['hzdepb_r']==0)
        dfHorizonComp.loc[testBedBot0,'hzdepb_r'] = layerBot[-1]

        # Want only the non-water rows, and a single dataframe
        testNotWater = numpy.logical_not(dfHorizonComp['Class_ID']==15)
        dfHorizonWeight = dfHorizonComp.loc[testNotWater,:].merge(dfHorizonProp.drop(['Percentage_ID','Class_ID'],axis=1),how='left',on='chkey')

        # Clean up
        del dfChtexturegrp, dfHorizonCalc, dfHorizonProp, dfChorizon, dfChtexture

        ## Loop through the components, reweighting to standard layers
        print('Averaging properties across standard layers')
        grouped = dfHorizonWeight.groupby('cokey')
        numComponents = len(grouped)

        # Loop through the components, filling in a 3-D array
        layersOutComp = numpy.zeros([numLay,len(layersFieldsComp),numComponents],float)
        layersOutProp = numpy.zeros([numLay,len(layersFieldsProp),numComponents],float)
        layersOutCokey = numpy.empty([numComponents],numpy.object_)

        m=0 # keep an index
        for cokey, group in tqdm(grouped):
            layersOutCokey[m] = cokey
            # For convenience
            thisTop = group['hzdept_r'].values.astype(float)
            thisBot = group['hzdepb_r'].values.astype(float)

            # Extend the top of the shallowest horizon, and the bottom of the edges of the profile
            firstLay = numpy.flatnonzero(thisTop==numpy.min(thisTop))[0] # only return one value in case of multiple
            thisTop[firstLay] = 0
            lastLay = numpy.flatnonzero(thisBot==numpy.max(thisBot))[0] # only return one value in case of multiple
            thisBot[lastLay] = numpy.max(layerBot)

            # Look for gaps in the horizons
            topSorted = numpy.sort(thisTop)
            topSortedIndex = numpy.argsort(thisTop)
            botSorted = numpy.sort(thisBot)
            botSortedIndex = numpy.argsort(thisBot)
            diffLay = numpy.zeros(topSorted.shape,float)
            diffLay[:-1] = topSorted[1:] - botSorted[0:-1]

            # Fill downward
            test = diffLay > 0
            if test.any:
                botSorted[test] = botSorted[test] + diffLay[test]
                thisBot[botSortedIndex] = botSorted

            # Calculate the weighting for this component
            thisWeight = numpy.zeros([len(group),numLay],float)
            for n in range(0,numLay):
                topDiff = (thisTop - layerTop[n])
                botDiff = (thisBot - layerBot[n])
                thisWeight[:,n] = ((layerThick[n] - (topDiff > 0) * topDiff + (botDiff < 0) * botDiff) / layerThick[n])
            thisWeight[thisWeight<0] = 0

            # First, do the composition table, this one can allow NaN values if no data are
            # defined in the new standardized layer
            for thisField in layersFieldsComp:
                indField = layersFieldsComp.index(thisField)
                # If composition is NaN, assign this to weight as well
                thisNan = group[thisField].isnull()
                thisWeightComp = thisWeight.copy()
                thisWeightComp[thisNan,:] = 0

                # Re-weight, this will also apply NaNs to complete empty layers, which is okay
                thisWeightComp = thisWeightComp / numpy.tile(numpy.reshape(numpy.sum(thisWeightComp,0),[1,thisWeightComp.shape[1]]),[thisWeightComp.shape[0],1])

                # Get the values for this composition type, change NaN to 0
                thisValues = group[thisField].fillna(0).values

                # Multiply weight grid by this composition, and sum to get this weighted values
                thisWeighted = numpy.sum(thisWeightComp * numpy.tile(numpy.reshape(thisValues,[thisValues.shape[0],1]),[1,numLay]),0)

                # Store
                layersOutComp[:,indField,m] = thisWeighted

            # Reweight to a sum of 1
            thisWeightProp = thisWeight / numpy.tile(numpy.reshape(numpy.sum(thisWeight,0),[1,thisWeight.shape[1]]),[thisWeight.shape[0],1])

            # Do the properties table
            for thisField in layersFieldsProp:
                indField = layersFieldsProp.index(thisField)
                # Get the values for this property type
                thisValues = group[thisField].values

                # Check for which type of averaging and handle differently
                # Check the averaging type and handle differently
                if layersAvgTypeProp[indField] == 'arithmetic':
                    thisWeighted = numpy.sum(thisWeightProp * numpy.tile(numpy.reshape(thisValues,[thisValues.shape[0],1]),[1,numLay]),0)
                elif layersAvgTypeProp[indField] == 'harmonic':
                    thisWeighted = 1 / numpy.sum(thisWeightProp / numpy.tile(numpy.reshape(thisValues,[thisValues.shape[0],1]),[1,numLay]),0)
                elif layersAvgTypeProp[indField] == 'first':
                    # Fill with shallowest layer value (identified above)
                    thisWeighted = numpy.tile(thisValues[firstLay],numLay) # all layers get the same value
                elif layersAvgTypeProp[indField] == 'mode':
                    # Get unique values
                    uniqueValues = numpy.unique(thisValues)
                    # Create empty output array, and count array
                    thisWeighted = numpy.zeros([numLay],float)
                    thisSum = numpy.zeros([numLay],float)
                    # For each unique value, sum its weight within each layer, store maximum
                    for uniqueValue in uniqueValues:
                        innerSum = numpy.sum(thisWeightProp[thisValues==uniqueValue,:],0)
                        test = innerSum > thisSum
                        thisWeighted[test] = uniqueValue
                        thisSum[test] = innerSum[test]

                # Store
                layersOutProp[:,indField,m] = thisWeighted
            # Increment the index
            m+=1

        # Store these in a dataframe
        layersOutComp = layersOutComp.transpose([0,2,1]).reshape([numComponents*numLay,len(layersFieldsComp)],order='F')
        layersOutProp = layersOutProp.transpose([0,2,1]).reshape([numComponents*numLay,len(layersFieldsProp)],order='F')
        layersOutNum = numpy.tile(numpy.arange(1,numLay+1),numComponents).reshape([numComponents*numLay,1])
        layersOutCokey = numpy.tile(layersOutCokey.reshape([1,numComponents]),[numLay,1]).reshape([numComponents*numLay,1],order='F')
        layersOutTop = numpy.tile(layerTop,numComponents).reshape([numComponents*numLay,1])
        layersOutBot = numpy.tile(layerBot,numComponents).reshape([numComponents*numLay,1])
        dfLayerWeighted = pandas.DataFrame(numpy.concatenate([layersOutComp,layersOutProp,layersOutCokey,layersOutNum,\
            layersOutTop,layersOutBot],axis=1),columns=layersFieldsComp+layersFieldsProp+\
            ['cokey','laynum','hzdept_r','hzdepb_r'])

        # Clean up
        del layersOutComp, layersOutProp, layersOutNum, layersOutCokey, layersOutTop, layersOutBot
        del dfHorizonWeight

        # Save out
        store = pandas.HDFStore(outFileStore)
        store['dfComponent'] = dfComponent
        store['dfLayerWeighted'] = dfLayerWeighted
        store.close()

    #%%------------------------------------------------------------------------------
    # Calculate weighted averages for horizon properties across components within mapunits
    #------------------------------------------------------------------------------
    if reloadData:
        store = pandas.HDFStore(outFileStore)
        dfComponent = store['dfComponent']
        dfLayerWeighted = store['dfLayerWeighted']
        store.close()

    ## Loop through the mapunits, averaging across components within standard layers
    print('Averaging properties across components')
    # Merge with components, drop duplicates
    dfLayerWeightedCalc = dfLayerWeighted.merge(dfComponent,on='cokey',how='inner').drop_duplicates()
    grouped = dfLayerWeightedCalc.groupby('mukey')
    numMapunits = len(grouped)

    # Loop through the unique mukeys, filling in the mapunitsOutProp and mapunitsOutComp tables
    mapunitsOutComp = numpy.zeros([numLay,len(layersFieldsComp),numMapunits],float) + numpy.NaN
    mapunitsOutProp = numpy.zeros([numLay,len(layersFieldsProp),numMapunits],float) + numpy.NaN
    mapunitsOutMukey = numpy.empty([numMapunits],numpy.object_)

    m = 0 # set an index
    for mukey, group in tqdm(grouped):
        mapunitsOutMukey[m] = mukey

        # Extract thisPercent
        thisPercent = (group[['cokey','comppct_r']].groupby('cokey').first()['comppct_r'].values.astype(float))

        # Check if sum of thisPercent is 0, set all to 100 if so, will reweight next
        if numpy.sum(thisPercent)==0:
            thisPercent[:] = 100

        # Re-weight the percentages, and convert the fractions
        thisPercent = thisPercent / numpy.sum(thisPercent)

        # Extract these
        groupedCokey = group.groupby('cokey')
        numCokey = len(groupedCokey)

        thisComp = numpy.zeros([numLay,len(layersFieldsComp),numCokey],float)
        thisProp = numpy.zeros([numLay,len(layersFieldsProp),numCokey],float)

        n=0
        for cokey, groupCokey in groupedCokey:
            thisComp[:,:,n] = groupCokey.loc[:,layersFieldsComp]
            thisProp[:,:,n] = groupCokey.loc[:,layersFieldsProp]
            n+=1

        # If there is only a single component, then don't average
        if thisPercent.shape[0]==1:
            # Store
            mapunitsOutComp[:,:,m] = thisComp[:,:,0]
            mapunitsOutProp[:,:,m] = thisProp[:,:,0]
        else:
            # Get the weights, really just for clarity here
            thisPercentComp = numpy.tile(numpy.tile(numpy.reshape(thisPercent,[1,1,thisPercent.shape[0]]),[1,len(layersFieldsComp),1]),[numLay,1,1])
            thisPercentProp = numpy.tile(numpy.tile(numpy.reshape(thisPercent,[1,1,thisPercent.shape[0]]),[1,len(layersFieldsProp),1]),[numLay,1,1])

            # All composition quantities are arithmetic averages
            mapunitsOutComp[:,:,m] = numpy.sum(thisComp * thisPercentComp,2)

            # Properties quantities may not be arithmetic averages (Class_ID, for example)
            # First, do a bunch of the fields all at once
            indMean = numpy.in1d(layersAvgTypeProp,['arithmetic','harmonic','first'])
            thisValues = thisProp[:,indMean,:]
            thisWeight = thisPercentProp[:,indMean,:]
            mapunitsOutProp[:,indMean,m] = numpy.sum(thisValues * thisWeight,2)

            # Second, do the 'mode'
            indMode = numpy.logical_not(indMean)
            for indField in numpy.flatnonzero(indMode):
                for n in range(0,numLay):
                    thisValues = thisProp[n,indField,:].flatten()
                    thisWeight = thisPercentProp[n,indField,:].flatten()
                    uniqueValues,uniqueCount = numpy.unique(thisValues,return_counts=True)
                    uniqueValues = numpy.int64(uniqueValues).flatten()
                    weightedCount = numpy.bincount(numpy.int64(thisValues),thisWeight)[uniqueValues]
                    mapunitsOutProp[n,indField,m] = uniqueValues[numpy.argwhere(weightedCount==numpy.max(weightedCount))[0]]

        # Increment the index
        m+=1

    # Store these in a dataframe
    mapunitsOutComp = mapunitsOutComp.transpose([0,2,1]).reshape([numMapunits*numLay,len(layersFieldsComp)],order='F')
    mapunitsOutProp = mapunitsOutProp.transpose([0,2,1]).reshape([numMapunits*numLay,len(layersFieldsProp)],order='F')
    mapunitsOutNum = numpy.tile(numpy.arange(1,numLay+1),numMapunits).reshape([numMapunits*numLay,1])
    mapunitsOutMukey = numpy.tile(mapunitsOutMukey.reshape([1,numMapunits]),[numLay,1]).reshape([numMapunits*numLay,1],order='F')
    mapunitsOutTop = numpy.tile(layerTop,numMapunits).reshape([numMapunits*numLay,1])
    mapunitsOutBot = numpy.tile(layerBot,numMapunits).reshape([numMapunits*numLay,1])
    dfCompWeighted = pandas.DataFrame(numpy.concatenate([mapunitsOutComp,mapunitsOutProp,mapunitsOutMukey,mapunitsOutNum,\
        mapunitsOutTop,mapunitsOutBot],axis=1),columns=layersFieldsComp+layersFieldsProp+\
        ['mukey','laynum','laytop','laybot'])

    # Clean up
    del mapunitsOutComp, mapunitsOutProp, mapunitsOutNum, mapunitsOutMukey, mapunitsOutTop, mapunitsOutBot
    del dfLayerWeighted

    #%%------------------------------------------------------------------------------
    # Finally, do the geospatial processing
    #------------------------------------------------------------------------------
    arcpy.env.workspace = outDB
    arcpy.env.scratchWorkspace = outDB
    
    # Need to change formatting to string types if object
    for outField in outFieldNames:
        indField = outFieldNames.index(outField)
        dfCompWeighted[[outField]] = dfCompWeighted[[outField]].astype(outFieldTypes[indField])

    # Check to make sure the table we need to write doesn't already exist
    arcpy.Delete_management(os.path.join(outDB,tableMapUnitProps))

    # Write out the table
    recOut = dfCompWeighted.fillna(outNullValue).to_records()
    arrayOut = numpy.array(recOut,dtype=recOut.dtype)
    pathTable = os.path.join(outDB,tableMapUnitProps)
    if arcpy.Exists(pathTable):
        arcpy.Delete_management(pathTable)
    arcpy.da.NumPyArrayToTable(arrayOut,pathTable)

    # Remove polygons with no tabular data---------------------------------------
    print('Removing mapunits with no tabular data')

    # Make a table view for the first layer
    arcpy.MakeQueryTable_management(outDB+'/'+tableMapUnitProps,tempTable,where_clause='"laynum" = 1')

    # Add the join to the raster layer
    joinedMukey = arcpy.AddJoin_management(outMukey,'Value',tempTable,'mukey')
    arcpy.CopyRaster_management(joinedMukey,tempJoin)

    # Convert null vales in Class_ID to outNullValue
    calcExpression = '''def checkNull(fieldVal):
        if fieldVal == None:
            return -9999
        else:
            return fieldVal'''
    arcpy.CalculateField_management(tempJoin,'Class_ID','checkNull(!Class_ID!)','PYTHON',calcExpression)

    # If we are nibbling away missing areas
    if optNibble:
        # Lookup just the Class_ID field for the first layer for Nibbling purposes, replace NoData values
        arcpy.CalculateStatistics_management(tempJoin)
        rasterClass = arcpy.sa.Lookup(tempJoin,'Class_ID')
        rasterNoData = arcpy.sa.Con(rasterClass!=outNullValue,rasterClass)

        # Create a raster from the boundary 
        arcpy.CopyFeatures_management(featBdry,tempBdryFeat)
        arcpy.AddField_management(tempBdryFeat,'grid','short')
        arcpy.CalculateField_management(tempBdryFeat,'grid','1')
        arcpy.PolygonToRaster_conversion(tempBdryFeat,'grid',tempBdryRaster,cellsize=arcpy.env.cellSize)
        rasterBoundary = arcpy.Raster(tempBdryRaster)
        
        # Create the nibble mask using the boundary and the nodata mukey this to limit the extent of the nibble
        rasterNibbleMask = arcpy.sa.Con(arcpy.sa.IsNull(rasterBoundary),-1,rasterNoData) # this will make the area outside the extent 1s, no nibbling there
        rasterNibbleMask.save(tempMask)

        # Do the nibble
        rasterNibbleMukey = arcpy.sa.Nibble(outMukey,rasterNibbleMask,'DATA_ONLY','PROCESS_NODATA')

        # Save the nibbled raster to tempMukey
        rasterNibbleMukey.save(tempMukey)
        arcpy.BuildRasterAttributeTable_management(tempMukey, "Overwrite")

        # Clean up
        del rasterNibbleMukey, rasterNibbleMask, rasterClass, rasterNoData, rasterBoundary
        arcpy.Delete_management(tempJoin)
        arcpy.Delete_management(tempBdryFeat)
        arcpy.Delete_management(tempBdryRaster)
    else:
        rasterMukeyJoin = arcpy.sa.Lookup(tempJoin,'VALUE')
        rasterMukeyJoin.save(tempMukey)
        arcpy.BuildRasterAttributeTable_management(tempMukey, "Overwrite")
        del rasterMukeyJoin

    # Clean up
    safe_delete(joinedMukey)

    # Now, process each layer and export to the geodatabase----------------------
    print('Creating output maps')
    for m in range(0,numLay):
        # Make a table view for the table
        arcpy.MakeQueryTable_management(outDB+'/'+tableMapUnitProps,tempTable, where_clause='"laynum" = ' + str(m+1))

        # Add the join to the raster layer
        joinedMukey = arcpy.AddJoin_management(tempMukey,'VALUE',tempTable,'mukey')
        arcpy.CopyRaster_management(joinedMukey,tempJoin)

        for field in tqdm(mapFieldNames,desc='Output Lay %d'%(m+1)):
            # Get the output name
            outName = os.path.join(outDir,outDBName)+'/'+field+'_lay'+str(m+1)
            
            # Bring in just the desired field
            rasterPrep = arcpy.sa.Lookup(tempJoin,field)

            # Replace the NoData value with NoData, and save
            rasterNoData = arcpy.sa.Con(rasterPrep!=outNullValue,rasterPrep)

            # Use copy raster to enforce compression settings
            arcpy.CopyRaster_management(rasterNoData,outName) # Save this raster to the geodatabase
            
            # Log this
            if stdInput:
                lhm_frontend.log_lhm_output(outName, os.path.join(outDir,outDBName), inputDB, printResult=False)

            # Clean up
            del rasterPrep, rasterNoData

        # Remove this join from the layer
        safe_delete(joinedMukey)
        safe_delete(tempTable)
        safe_delete(tempJoin)

    # Clean up
    safe_delete(tempMukey)
    safe_delete(tempMask)
    if not reloadData:
        del rasterMukey
    os.remove(outFileStore)

    # Update the config file with the location of the processed soils data
    if stdInput:
        lhm_frontend.update_config_sources(os.path.join(pathConfig,configFileSources),configSectionSources,rowSource,outDB)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Soils have finished processing for the <strong>%s</strong> region'%regionName))
