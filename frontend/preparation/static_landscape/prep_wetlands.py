# Module imports
import os
import lhm_frontend

'''
--------------------------------------------------------------------------------
Module names
--------------------------------------------------------------------------------
'''
def names_module(reqList):
    """These inputs are used only within the module below
    """

    names = dict()
    names['featureLacustrine'] = 'nwi_lacustrine'
    names['featurePalustrine'] = 'nwi_palustrine'
    names['featurePalustrineDepth'] = 'nwi_palustrine_depth'
    names['fieldDepth'] = 'depth'

    # Return what is requested
    if reqList is None:
        return names
    elif isinstance(reqList,list):
        if len(reqList) > 1:
            return [names[thisReq] for thisReq in reqList]
        else:
            return names[reqList[0]]
    elif isinstance(reqList,str):
        return names[reqList]

'''
--------------------------------------------------------------------------------
Main module function
--------------------------------------------------------------------------------
'''
def prep_wetlands(inputDB, pathConfig, params=dict(), runOnly=False, printResult=True):
    """This function creates output grids of wetland presence, depth, and assigns
    a wetland complex ID. It also creates a pair of masked DEMs, one for areas with
    wetlands present, the other for upland areas without wetlands. Thse will later
    be aggregated to model resolution to produce the wetland and upland elevation
    rasters.

    Params are:
        avgAggFactor: aggregaton factor for assigning average depths to features, default is 400
        elevThresh: elevation separation between adjacent wetlands that define separate complexes, default is 2 m
        elevRegionAggFactor: factor to aggregate elevations for wetland complex region groups, default is 40

    To facilitate running only a single part of the code, you can specify an input runOnly
    which will run only a part of the code, options are either False, unspecified, 'presence',
    'elevations', 'complexes', or 'depths'. runOnly mode can only do a single piece
    at a time.
    """

    # Import system modules
    if printResult:
        from IPython.display import display, HTML
        
        regionName = os.path.split(pathConfig)[1]

    # Get standard inputs, use the DEM to match cellsize
    inputDEM = lhm_frontend.names_frontend(['prepDEMRaw'])

    # Parse the input params structure
    paramDefaults = {'avgAggFactor':400, 'elevThresh':2, 'elevRegionAggFactor':40}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Set up the geoprocessing environment and get sources
    (sources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB, pathConfig)
    arcpy = lhm_frontend.set_environment(inputDEM, inputDB, inputBdry)

    # Call each piece of the model
    if (not runOnly) or runOnly=='presence':
        outPres = presence_absence(params, arcpy, sources)
        
        # Log this output
        lhm_frontend.log_lhm_output(outPres,inputDB,inputDB,printResult)

        if printResult:
            display(HTML('Wetlands presence/absence is prepared for the <strong>%s</strong> region'%regionName))

    if (not runOnly) or runOnly=='elevations':
        (outWetElev,outUpElev) = elevations(params, arcpy, sources)

        # Log these outputs
        lhm_frontend.log_lhm_output(outWetElev,inputDB,inputDB,printResult)
        lhm_frontend.log_lhm_output(outUpElev,inputDB,inputDB,printResult)

        if printResult:
            display(HTML('Wetlands elevations are prepared for the <strong>%s</strong> region'%regionName))

    if (not runOnly) or runOnly=='complexes':
        outComplex = complexes(params, arcpy, sources)

        # Log this output
        lhm_frontend.log_lhm_output(outComplex,inputDB,inputDB,printResult)

        if printResult:
            display(HTML('Wetlands complexes are prepared for the <strong>%s</strong> region'%regionName))

    if (not runOnly) or runOnly=='depths':
        outDepths = depths(params, arcpy, sources)

        # Log this output
        lhm_frontend.log_lhm_output(outDepths,inputDB,inputDB,printResult)

        if printResult:
            display(HTML('Wetlands depths are prepared for the <strong>%s</strong> region'%regionName))
        

'''
--------------------------------------------------------------------------------
Presence/Absence
--------------------------------------------------------------------------------
'''
def presence_absence(params, arcpy, sources):
    """This function creates a 1/0, presence/absence grid"""

    # Get LHM standard names
    (rowWetlands,outCompPres) = lhm_frontend.names_frontend(['srcWetlands','prepWetPres'])

    # Get module names
    (featureLacustrine,featurePalustrine) = names_module(['featureLacustrine','featurePalustrine'])

    # Temporary names, this function only
    tempInSystem = 'temp_input_system'
    tempDataSystem = 'temp_data_system'
    tempPresSystem = 'temp_pres_system'
    tempPresLac = 'temp_pres_lacustrine'
    tempPresComp = 'temp_pres_composite'

    # Set up the environment
    cellSize = arcpy.env.cellSize

    #--------------------------------------------------------------------------------
    # Calculations
    #--------------------------------------------------------------------------------
    # Create dictionary for presence/absence, currently only supports NWI
    features = dict()
    features['lacustrine'] =  {'method':'feature', 'fieldVal':'',\
        'data':sources[rowWetlands] + '/' + featureLacustrine}
    features['palustrine'] =  {'method':'feature', 'fieldVal':'',\
        'data':sources[rowWetlands] + '/' + featurePalustrine}

    # Now a list of systems to composite
    listSystems = ['lacustrine','palustrine']

    ## Create a composite presence/absence grid
    rasterPres = None
    for system in listSystems:
        # Test if there are gridded features for the complex calculation
        testGrid = features[system]['method'] == 'grid'

        # Copy to a grid, if not already one
        if not testGrid:
            # Copy input features to temporary features
            arcpy.CopyFeatures_management(features[system]['data'],tempInSystem)

            # Add a field and calculate its value for the grid conversion
            arcpy.AddField_management(tempInSystem,'GRID','FLOAT')
            arcpy.CalculateField_management(tempInSystem,'GRID',1,'PYTHON')

            # Write out a presence grid
            arcpy.FeatureToRaster_conversion(tempInSystem,'GRID',tempDataSystem,cellSize)

            # Get the raster object
            rasterSystem = arcpy.Raster(tempDataSystem)

            # Clean up
            arcpy.Delete_management(tempInSystem)

        else:
            rasterSystem = arcpy.Raster(features[system]['data'])

        # Resample the grid to our resolution with ApplyEnvironment
        rasterSystem = arcpy.sa.ApplyEnvironment(rasterSystem)
        rasterSystem.save(tempPresSystem)

        # Convert the raster to 0/1
        rasterThisPres = arcpy.sa.Con(arcpy.sa.IsNull(rasterSystem),0,\
                                      arcpy.sa.Con(rasterSystem>0,1,0))

        # If this is the first grid, keep it, otherwise add
        if rasterPres is None:
            rasterThisPres.save(tempPresLac)
            del rasterThisPres #delete here for clarity
            rasterPres = arcpy.Raster(tempPresLac)
        else:
            rasterCompPres = arcpy.sa.Con(rasterPres>rasterThisPres,rasterPres,\
                rasterThisPres)
            rasterCompPres.save(tempPresComp)
            del rasterThisPres, rasterCompPres
            rasterPres = arcpy.Raster(tempPresComp)

        # Clean up after this step
        del rasterSystem

    # Save out the pres/absence grid
    arcpy.CopyRaster_management(rasterPres,outCompPres)

    # Clean up after this step
    del rasterPres

    if arcpy.Exists(tempDataSystem):
        arcpy.Delete_management(tempDataSystem)
    arcpy.Delete_management(tempPresSystem)
    arcpy.Delete_management(tempPresLac)
    arcpy.Delete_management(tempPresComp)

    return outCompPres


'''
--------------------------------------------------------------------------------
Wetland and upland elevations
--------------------------------------------------------------------------------
'''
def elevations(params, arcpy, sources):
    """This function assigns masked elevations to wetland and upland areas for later aggregation"""

    # Get LHM standard names
    (inputDEM,prepWetPres,outWetElev,outUpElev) = lhm_frontend.names_frontend(\
        ['prepDEMRaw','prepWetPres','prepDEMWet','prepDEMUp'])

    # Create a wetland and uplands DEM elevation
    rasterPres = arcpy.Raster(prepWetPres)
    rasterDEM = arcpy.Raster(inputDEM)
    rasterDEMWet = arcpy.sa.Con(rasterPres==1,rasterDEM)
    rasterDEMUp = arcpy.sa.Con(rasterPres==0,rasterDEM)
    rasterDEMWet.save(outWetElev)
    rasterDEMUp.save(outUpElev)

    # Clean up after this step
    del rasterDEMWet, rasterDEMUp, rasterPres, rasterDEM

    return outWetElev, outUpElev

'''
--------------------------------------------------------------------------------
Wetland complexes
--------------------------------------------------------------------------------
'''
def complexes(params, arcpy, sources):
    """This function creates a grid of wetland complexes, with each assigned a unique integer"""

    # Get LHM standard names
    (inputDEM,prepWetPres,outComplex) = lhm_frontend.names_frontend(['prepDEMRaw','prepWetPres','prepWetComp'])

    # Parse the params
    elevThresh = params['elevThresh']
    elevRegionAggFactor = params['elevRegionAggFactor']

    # Temporary names, this function only
    tempPresComp = 'temp_pres_complex'    
    tempPresPolys = 'temp_pres_polys'
    tempElevPolys = 'temp_elev_polys'
    tempComplexPolys = 'temp_complex_polys'
    tempComplexRaster = 'temp_complex_raster'
    tempElevComplex = 'temp_elev_complex'

    # Parse the cellsize
    cellSize = arcpy.env.cellSize

    #--------------------------------------------------------------------------------
    # Calculations
    #--------------------------------------------------------------------------------
    # Write a presence grid with NoData
    rasterPres = arcpy.Raster(prepWetPres)
    rasterPresNull = arcpy.sa.Con(rasterPres>0,1)
    rasterPresNull.save(tempPresComp)

    # Now, create a classified DEM to make regions out of
    rasterDEM = arcpy.Raster(inputDEM)
    rasterElevGroup = arcpy.sa.Int(rasterDEM / elevThresh)
    rasterElevGroup.save(tempElevComplex)

    # Aggregate this to a coarser resolution,
    rasterElevAgg = arcpy.sa.Aggregate(rasterElevGroup, elevRegionAggFactor, 'MEDIAN', ignore_nodata='DATA')
    rasterElevAgg = arcpy.sa.Int(rasterElevAgg)

    # Convert the coarse elevation grid and the wetlands grid to polygons
    arcpy.RasterToPolygon_conversion(tempPresComp,tempPresPolys,'NO_SIMPLIFY')
    arcpy.RasterToPolygon_conversion(rasterElevAgg,tempElevPolys,'NO_SIMPLIFY')

    # Now, intersect the two polygon sets, and compute a unique ID
    arcpy.Intersect_analysis([tempPresPolys,tempElevPolys], tempComplexPolys)
    arcpy.AddField_management(tempComplexPolys, 'Complex_ID', 'LONG')
    arcpy.CalculateField_management(tempComplexPolys, 'Complex_ID', '!OBJECTID!')

    # Convert this back to a raster
    arcpy.FeatureToRaster_conversion(tempComplexPolys, 'Complex_ID', tempComplexRaster, cellSize)
    rasterComplex = arcpy.sa.Con(arcpy.sa.IsNull(tempComplexRaster),0,tempComplexRaster)
    rasterComplex = arcpy.sa.ApplyEnvironment(rasterComplex)
    arcpy.CopyRaster_management(rasterComplex,outComplex)

    # Clean up after this step
    arcpy.Delete_management(rasterComplex)
    del rasterDEM, rasterElevGroup, rasterElevAgg, rasterComplex, rasterPres, rasterPresNull

    # Now delete temporary rasters
    delTemps = [tempPresComp, tempPresPolys, tempElevPolys, \
        tempComplexPolys, tempComplexRaster, tempElevComplex]
    [arcpy.Delete_management(delTemp) for delTemp in delTemps]

    return outComplex

'''
--------------------------------------------------------------------------------
Wetland depths
--------------------------------------------------------------------------------
'''
def depths(params, arcpy, sources):
    """This function creates a grid of wetland depths, with noData outside of wetlands"""

    from raster import create_surface_raster

    # Get LHM standard names
    (rowDepthsLac,rowDepthsPal,rowWetlands,prepWetPres,outCompDepth) = \
        lhm_frontend.names_frontend(['srcLakesDepth','srcWetlandsDepth','srcWetlands','prepWetPres','prepWetDepth'])

    # Get module names
    (featureLacustrine,featurePalustrineDepth,fieldDepth) = names_module([\
        'featureLacustrine','featurePalustrineDepth','fieldDepth'])

    # Temporary names
    tempDepLac = 'temp_depth_lac'
    tempDepPal = 'temp_depth_pal'

    # Set up the environment
    cellSize = arcpy.env.cellSize
    avgAggFactor = params['avgAggFactor']

    #--------------------------------------------------------------------------------
    # Calculations
    #--------------------------------------------------------------------------------
    # Create dictionary for depths
    featuresDepth = dict()
    if sources[rowDepthsLac]['data'] != '':
        featuresDepth['lacustrine'] = sources[rowDepthsLac]
    else: # sets a cosntant depth for the lake features
        featuresDepth['palustrine'] = {'method':'feature', 'fieldVal':sources[rowDepthsLac]['fieldVal'], \
            'data':sources[rowWetlands] + '/' + featureLacustrine}
    if sources[rowDepthsPal]['data'] != '':
        featuresDepth['palustrine'] = sources[rowDepthsPal]
    else: # this is the default behavior, uses the nwi_prep routine
        featuresDepth['palustrine'] = {'method':'feature', 'fieldVal':fieldDepth, \
            'data':sources[rowWetlands] + '/' + featurePalustrineDepth}

    # Create the depths grids, will have NaNs
    create_surface_raster(featuresDepth['lacustrine'],tempDepLac)
    create_surface_raster(featuresDepth['palustrine'],tempDepPal)

    # Can end up with a situation where the rasters aren't quite right
    rasterLacDepth = arcpy.sa.ApplyEnvironment(arcpy.Raster(tempDepLac))
    rasterPalDepth = arcpy.sa.ApplyEnvironment(arcpy.Raster(tempDepPal))

    # Create the composite grid for depths, will have NaNs
    rasterCompDepth = arcpy.sa.Con(arcpy.sa.IsNull(rasterLacDepth), rasterPalDepth, rasterLacDepth)

    # Some riverine and lacustrine features may not yet have depths assigned, will take care of that here
    # note, riverine features will have depths overwritten by the streams within the model, so just
    # need to handle lacustrine features

    # First, will need to generate a fill raster, which I am doing via aggregation and then nibbling (nearest neighbor fill)
    # I will do this at the coarse scale to be fast, nibble can be a very slow operation!
    rasterLacDepthCoarse = arcpy.sa.Aggregate(tempDepLac,avgAggFactor,'MEAN',ignore_nodata='DATA')
    arcpy.env.cellSize = rasterLacDepthCoarse
    rasterNibbleVals = arcpy.sa.Int(arcpy.sa.Con(arcpy.sa.IsNull(rasterLacDepthCoarse),0,rasterLacDepthCoarse) * 1000)
    rasterNibbleMask = arcpy.sa.Int(rasterLacDepthCoarse)
    rasterLacDepthFill = arcpy.sa.Nibble(rasterNibbleVals,rasterNibbleMask,'DATA_ONLY')
    rasterLacDepthFill = arcpy.sa.Float(rasterLacDepthFill) / 1000
    arcpy.env.cellSize = cellSize # back to the original environment settings
    rasterLacDepthFill = arcpy.sa.ApplyEnvironment(rasterLacDepthFill) # gets it back to the necessary grid dimensions

    # Then, use that fill raster to update the composite depths grid
    rasterPres = arcpy.Raster(prepWetPres)
    rasterCompDepthFill = arcpy.sa.Con(arcpy.sa.BooleanAnd(arcpy.sa.IsNull(rasterCompDepth),\
                                        rasterPres>0),rasterLacDepthFill, rasterCompDepth)
    rasterCompDepthFill.save(outCompDepth)

    # Clean up 
    # Delete temporary raster objects
    del rasterLacDepth, rasterPalDepth, rasterPres, \
        rasterLacDepthCoarse, rasterNibbleVals, rasterNibbleMask, rasterLacDepthFill,\
        rasterCompDepthFill, rasterCompDepth

    # Delete temporary rasters
    arcpy.Delete_management(tempDepLac)
    arcpy.Delete_management(tempDepPal)

    return outCompDepth