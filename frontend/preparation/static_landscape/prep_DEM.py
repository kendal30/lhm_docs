def prep_DEM(inputDB, pathConfig, printResult=True):
    """This function prepares a new DEM for use, exporting the DEM
    to a desired coordinate system, generating a filled DEM, and flowdirection and
    slope grids at the DEM resolution"""

    import arcpy, os
    import lhm_frontend
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Get defaults, set up the environment
    #-------------------------------------------------------------------------------
    # Get the input names
    (outRawName,outAspectName,outFdirName,outFillName,outFillFdirName,outFillSlopeName,\
        outFillAccumName,configFile,configSection,rowDEM,rowGroundwaterTop) = lhm_frontend.names_frontend(\
            ['prepDEMRaw','prepDEMAspect','prepDEMRawDir','prepDEMFill','prepDEMFillDir','prepDEMFillSlope',\
                'prepDEMFillAccum','srcFile','srcSectionSurface','srcDEM','srcTop'])

    # Get the standard input locations
    (inputSources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Set up the environment
    arcpy = lhm_frontend.set_environment('',inputDB,inputBdry)

    #-------------------------------------------------------------------------------
    # Extraction
    #-------------------------------------------------------------------------------
    # Extract the raster
    rasterExtract = arcpy.sa.ExtractByMask(inputSources[rowDEM],inputBdry)
    rasterExtract.save(outRawName)

    # Log this output
    lhm_frontend.log_lhm_output(outRawName,inputDB,inputDB,printResult)

    # Calculate the DEM aspect on the raw grid
    rasterAspect = arcpy.sa.Aspect(rasterExtract)
    rasterAspect.save(outAspectName)

    # Log this output
    lhm_frontend.log_lhm_output(outAspectName,inputDB,inputDB,printResult)

    # Get the flowdirection of the raw DEM
    rasterFlowdirRaw = arcpy.sa.FlowDirection(rasterExtract)
    rasterFlowdirRaw.save(outFdirName)

    # Log this output
    lhm_frontend.log_lhm_output(outFdirName,inputDB,inputDB,printResult)

    # Create the filled DEM
    rasterFill = arcpy.sa.Fill(rasterExtract)
    rasterFill.save(outFillName)

    # Log this output
    lhm_frontend.log_lhm_output(outFillName,inputDB,inputDB,printResult)

    # Get the flowdirection and slope of the filled DEM
    rasterFlowdir = arcpy.sa.FlowDirection(rasterFill,'NORMAL',outFillSlopeName)
    rasterFlowdir.save(outFillFdirName)

    # Log this output
    lhm_frontend.log_lhm_output(outFillFdirName,inputDB,inputDB,printResult)
    lhm_frontend.log_lhm_output(outFillSlopeName,inputDB,inputDB,printResult)

    # Calculate the flowaccumulation grid of this DEM
    rasterAccum = arcpy.sa.FlowAccumulation(rasterFlowdir)
    rasterAccum.save(outFillAccumName)

    # Log this output
    lhm_frontend.log_lhm_output(outFillAccumName,inputDB,inputDB,printResult)

    #-------------------------------------------------------------------------------
    # Write out the groundwater model top source
    #-------------------------------------------------------------------------------
    # Update the config file with the location of the raw DEM
    if rowGroundwaterTop not in inputSources.keys():
        lhm_frontend.update_config_sources(os.path.join(pathConfig,configFile),configSection,\
                                           rowGroundwaterTop,os.path.join(inputDB, outRawName))

    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    del rasterAccum, rasterAspect, rasterExtract, rasterFill, rasterFlowdir, rasterFlowdirRaw

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('The DEM is processed for the <strong>%s</strong> region'%regionName))
