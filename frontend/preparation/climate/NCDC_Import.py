#This script imports NCDC data into a MySQL database
#NOTES:
#Search and replace ',,,' with ',C,'

import MySQLdb

#Specify the input file
inFile = 'C:\\Users\\Anthony\\Data\\Other_Downloaded\\NCDC\\Statewide_Through_early_2010\\8814364531277dat.txt'
delimeter = ','
headerlines = 2

#Specify the database connection information
dbInfo = {'host':'192.168.0.55','user':'ILHM','pass':'dataaccess','db':'ilhm_inputs'}

#Specify the MySQL column names from the text file
colNames = ['COOPID','CD','ELEM','UN','YEAR','MO','DA','TIME01','HOUR01','FLAG01','QUAL01',\
    'TIME02','HOUR02','FLAG02','QUAL02','TIME03','HOUR03','FLAG03','QUAL03',\
    'TIME04','HOUR04','FLAG04','QUAL04','TIME05','HOUR05','FLAG05','QUAL05',\
    'TIME06','HOUR06','FLAG06','QUAL06','TIME07','HOUR07','FLAG07','QUAL07',\
    'TIME08','HOUR08','FLAG08','QUAL08','TIME09','HOUR09','FLAG09','QUAL09',\
    'TIME10','HOUR10','FLAG10','QUAL10','TIME11','HOUR11','FLAG11','QUAL11',\
    'TIME12','HOUR12','FLAG12','QUAL12','TIME13','HOUR13','FLAG13','QUAL13',\
    'TIME14','HOUR14','FLAG14','QUAL14','TIME15','HOUR15','FLAG15','QUAL15',\
    'TIME16','HOUR16','FLAG16','QUAL16','TIME17','HOUR17','FLAG17','QUAL17',\
    'TIME18','HOUR18','FLAG18','QUAL18','TIME19','HOUR19','FLAG19','QUAL19',\
    'TIME20','HOUR20','FLAG20','QUAL20','TIME21','HOUR21','FLAG21','QUAL21',\
    'TIME22','HOUR22','FLAG22','QUAL22','TIME23','HOUR23','FLAG23','QUAL23',\
    'TIME24','HOUR24','FLAG24','QUAL24','TIME25','TOTAL','TOTFLAG','TOTQUAL']
colStr = ','.join(colNames)
formStr = '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s'

colTypes = ['INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)',\
    'INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)','INTEGER','INTEGER','VARCHAR(255)','VARCHAR(255)']

#Specify the output table
outTable = 'hourly_precip_data'

#Specify the subset output table, and columns to match
##subTable = 'watershed_precip_stations'
##subOutTable = 'watershed_precip_data'
##outTableCol = 'COOPID'
##subTableCol = 'COOP_ID'

#Open the mysql connection
dbConn = MySQLdb.connect(host=dbInfo['host'], user=dbInfo['user'], passwd=dbInfo['pass'], db=dbInfo['db'])
cursor = dbConn.cursor()

#Create the MySQL table if it does not exist
statement = 'CREATE TABLE IF NOT EXISTS ' + outTable + '('
for m in range(0,len(colNames)):
    statement = statement + colNames[m] + ' ' + colTypes[m] + ', '
statement = statement[0:-2] + ');'
cursor.execute(statement)

#Open the text file
fileConn = open(inFile,'rU')

#Skip headerlines
for m in range(0,headerlines):
    line = fileConn.readline()

#Read in the text file and write it to the DB
while 1:
    line = fileConn.readline()[0:-2] #omit the line ending character \n
    if line == '':
        break
    #Split the line using the delimeter
    lineSplit = line.split(delimeter)

    #Check to see if this line is already in the database


    #Write this line to the MySQL database
    statement = 'INSERT INTO ' + outTable + ' (' + colStr + ') VALUES (' + formStr + ')'
    cursor.execute(statement,lineSplit)

#Commit those changes
dbConn.commit()

#Now, select a subset of the rows for the watershed
##cursor.execute('DROP TABLE IF EXISTS ' + subOutTable)
##cursor.execute('CREATE TABLE ' + subOutTable + ' AS SELECT ' + outTable + '.* FROM ' +\
##    outTable + ',' + subTable + ' WHERE ' + outTable + '.' + outTableCol + ' = ' +\
##    subTable + '.' + subTableCol)
##dbConn.commit()

#Close the connections
fileConn.close()
dbConn.close()