#This script extracts LAI values for the specified boundary polygon, and writes them out in the coordinate system
#of the polygon, rather than the grids.

#Import modules
import arcgisscripting, os, shutil

#Change the year
years = ['2004','2005']
months = ['01','02','03','04','05','06','07','08','09','10','11','12']
#Static Paths
gridsLocBase = 'F:\\GIS_Data\\NEXRAD_Data'
boundLoc = 'D:\\Users\\sherry\\NEXRAD\\EBCW'
boundName = 'BCextended_model_boundary_10kBuffer_dissolve.shp' #'buffered_boundary_final.shp'
boundShape = os.path.join(boundLoc, boundName)
outputLocBase = 'D:\\Users\\sherry\\NEXRAD\\EBCW'
##tempProcessingDir = 'F:\\Users\\sherry\\NEXRAD\\temp'

#Create the geoprocessor object
gp = arcgisscripting.create(9.3)
#Check out the spatial analyst extension
gp.CheckOutExtension("Spatial")
#Set the output coordinate system to match the boundary polygon
gp.OutputCoordinateSystem = boundShape

for year in years:
    gridsLocYear = os.path.join(gridsLocBase, year)
    outputLocYear = os.path.join(outputLocBase,year)
    if not os.path.exists(outputLocYear):
        os.mkdir(outputLocYear,777)
    for month in months:
        count512 = 1 #this will keep track of the number of grids in the output directory,
        #for some reason there is a limit of 512, start at one to account for the "info" folder

        #Current directories
        gridsLoc = os.path.join(gridsLocYear,year + month + '.tar')
##        processLoc = os.path.join(tempProcessingDir,year + month + '.tar')
        outputLoc = os.path.join(outputLocYear,month)

        #Copy current month grids to temp processing directory
##        if not os.path.exists(processLoc):
##            shutil.copytree(gridsLoc,processLoc)
##        gridsLoc = processLoc

        #Make the output location if it doesn't already exist
        if not os.path.exists(outputLoc):
            os.mkdir(outputLoc,777)

        #List all of the rasters in the current grid location
        gp.workspace = gridsLoc
        grids = gp.ListRasters("","GRID")

        #Loop through the rasters
        for grid in grids:
            if count512 == 512:
                outputLoc = outputLoc + 'a'
                count512 = 1
            if not os.path.exists(outputLoc):
                os.mkdir(outputLoc,777)
            outputGrid = os.path.join(outputLoc,grid)
            count512 = count512 + 1
            if os.path.getsize(os.path.join(gridsLoc,os.path.join(grid,'w001001.adf'))) > 0: #some grids are empty
                if not os.path.exists(outputGrid): #this is for restarting purposes
                    print(outputGrid)
                    gp.ExtractByMask_sa(grid,boundShape,outputGrid)