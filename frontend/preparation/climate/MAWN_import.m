inputDir = 'C:\Users\Anthony\Data\Other_Downloaded\MAWN\Traverse';
tempDir = 'C:\Users\Anthony\Data\Other_Downloaded\MAWN\Traverse\temp'; %only used if input is .tar.gz files, must be empty
inputFiles = {'arlene.tar.gz','bearlake.tar.gz','benzonia.tar.gz','eastleland.tar.gz',...
    'elkrapids.tar.gz','hawks.tar.gz','kewadin.tar.gz','mcbain.tar.gz','northport.tar.gz',...
    'nwmhrs.tar.gz','oldmission.tar.gz'};

%Mapping Information
mapIds = {'arl','blk','bnz','eld','elk','haw','kwd','mcb','nth','nwm','old'};
mapHeaders = struct('air_temp','atmp','leaf_wet_canopy','leaf1','soil_moist_10in','mstr1',...
    'rel_hum','relh','soil_temp_2in','soil0','solar_rad','srad','wind_dir','wdir',...
    'leaf_wet_mount','leaf0','soil_moist_4in','mstr0','precip','pcpn','soil_temp_4in','soil1',...
    'wind_avg','wspd','wind_max','wspd_max');

mapNoData = -999;

%MySQL table information
db = struct('host','192.168.0.55','user','ILHM','pass','dataaccess','db','ilhm_inputs');
db.stationTable.name = 'mawn_stations_traverse';
db.stationTable.cols = {'ID','MAWN_ID'};
db.dataTable.name = 'mawn_data_traverse';
db.dataTable.cols = {'ID','MAWN_ID','date'};
db.dataTable.vecs = {'ID','strID','date'};

%Open a MySQL session
myopen(db.host,db.user,db.pass);
dbopen(db.db);

%Get the maximum row ID
queryOutput = tbread(db.dataTable.name,db.dataTable.vecs(1),db.dataTable.cols(1));
currMaxId = max(queryOutput.ID);

for m = 1:length(inputFiles)
    %First, check to see if the input file is a .tar.gz file, if so,
    %extract it, and then loop through those files,
    %assigning each one to inputFiles{m}
    if strcmpi(inputFiles{m}(end-6:end),'.tar.gz')
        tempFiles = true;
        thisInputDir = tempDir;
        gunzip([inputDir,filesep,inputFiles{m}],tempDir);
        tarfile = dir([tempDir,filesep,'*.tar']);
        untar([tempDir,filesep,tarfile(1).name],tempDir);
        delete([tempDir,filesep,tarfile(1).name]);
        dirListCsv = dir([tempDir,filesep,'*.csv']);
        csvFiles = cell(length(dirListCsv),1);
        for n = 1:length(dirListCsv)
            csvFiles{n} = dirListCsv(n).name;
        end
    else
        thisInputDir = inputDir;
        tempFiles = false;
        csvFiles = {inputFiles{m}}; %#ok<CCAT1>
    end
    
    for n = 1:length(csvFiles)
        %Read in the input data into a structured array
        [inputDates,inputData,inputDataBulk] = mawn_file_parse([thisInputDir,filesep,csvFiles{n}]);
        
        %Now, delete the file if no longer needed
        if tempFiles
            delete([thisInputDir,filesep,csvFiles{n}]);
        end
        
        %Transform from EDT to EST
        inputDates = daylight_savings_correction(inputDates);
        
        %Handle the precipitation column appropriately (don't want NaN values
        %where 0 is the right number). Assume that as long as the station is
        %working, the precipitation sensor is also working.
        test = all(isnan(inputDataBulk),2);
        inputData.(mapHeaders.precip)(~test) = 0;
        
        %Query the database for the current station to see what dates are
        %already loaded
        whereStr = ['WHERE `MAWN_ID` LIKE "',mapIds{m},'"'];
        queryOutput = tbread(db.dataTable.name,db.dataTable.vecs,db.dataTable.cols,whereStr);
        
        %Find the start and end dates of the sequence
        if ~isempty(queryOutput.date)
            queryDates = datenum(queryOutput.date,'yyyy-mm-dd HH:MM:SS');
            minDate = min(queryDates);
            maxDate = max(queryDates);
            
            %Check to see if any of the input dates do not already match values
            %in the table
            newDates = ~ismember(inputDates,queryDates);
        else
            newDates = true(size(inputDates));
        end
        
        if any(newDates)
            %Now, construct the output structured array
            output.ID = (currMaxId+1:1:currMaxId+sum(newDates))';
            output.MAWN_ID = cellstr(repmat(mapIds{m},[sum(newDates),1]));
            output.date = cellstr(datestr(inputDates(newDates),'yyyy-mm-dd HH:MM:SS'));
            dataFields = fieldnames(mapHeaders);
            for n = 1:length(dataFields)
                output.(dataFields{n}) = inputData.(mapHeaders.(dataFields{n}))(newDates);
            end
            
            %Write the data to the data table
            tbwrite(db.dataTable.name,output,{},3);
            
            %Update the maximum ID for the next table
            currMaxId = max(output.ID);
        end
        
        
    end
end

myclose;