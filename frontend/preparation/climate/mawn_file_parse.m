function [outputDates,outputData,inputData] = mawn_file_parse(inputFile)

%Specify some program specifics
headerMatchStr = 'date,';
footerMatchStr = 'Variable Ids:';
inputDateform = 'mm/dd/yyyy HH:MM';
delimiter = ',';
dateCol = 1;
timeCol = 2;

%Open the file and read it in
fileText = fileread(inputFile);

%Split the file into lines
fileLines = regexp(fileText,'\n','split');

%First, find the start of the data
test = false;
headerlines = 0;
currLine = 0;
while ~test
    headerlines = headerlines + 1;
    currLine = currLine + 1;
    if length(fileLines{currLine}) >= length(headerMatchStr)
        test = strcmpi(fileLines{currLine}(1:length(headerMatchStr)),headerMatchStr);   
    end
end

%Parse the header into column names
inputHeaders = regexp(fileLines{currLine},',','split');
if dateCol ~= timeCol, numDateTimeCols = 2; else numDateTimeCols = 1; end
numDataCols = length(inputHeaders) - numDateTimeCols;

%Now, find the end of the data, denoted by a particular string
test = false;
datalines = 0;
while ~test
    datalines = datalines + 1;
    currLine = currLine + 1;
    if length(fileLines{currLine}) >= length(footerMatchStr)
        test = strcmpi(fileLines{currLine}(1:length(footerMatchStr)),footerMatchStr);
        if test, datalines = datalines - 2;end %There's a blank line before the footer
    end
end

%Build the input data format
inputFormat = [repmat('%s ',1,numDateTimeCols),repmat('%f ',1,numDataCols)];
inputFormat = [inputFormat(1:end-1),'\n'];

%Now, read in the data
inputDataRead = textscan(fileText,inputFormat,datalines,'Delimiter',delimiter,'CollectOutput',true,'Headerlines',headerlines);
inputData = inputDataRead{2};
inputDateString = cell(size(inputDataRead{1},1),1);
for m = 1:length(inputDateString)
    inputDateString{m} = [inputDataRead{1}{m,1},' ',inputDataRead{1}{m,2}];
end
outputDates = datenum(inputDateString,inputDateform);

%Parse the data into a structured array
outputData = struct();
for m = 1:numDataCols
    outputData.(inputHeaders{numDateTimeCols+m}) = inputData(:,m);
end