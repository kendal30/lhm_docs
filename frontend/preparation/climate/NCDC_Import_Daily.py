#This script imports NCDC data into a MySQL database
#NOTES:
#Search and replace ',,,' with ',C,'

import MySQLdb

#Specify the input file
inFiles = ['C:\\Users\\Anthony\\Data\\Other_Downloaded\\NCDC\\Daily_Statewide\\mid_2008_through_mid_2010\\4843974531315dat.txt']
delimeter = ','
headerlines = 2

#Specify the database connection information
dbInfo = {'host':'192.168.0.55','user':'ILHM','pass':'dataaccess','db':'ilhm_inputs'}

#Specify the MySQL column names from the text file
colNames = ['DSET','COOPID','WBNID','CD','ELEM','UN','YEARMO','DAHR01','DAY01','FLAG01','QUAL01',\
    'DAHR02','DAY02','FLAG02','QUAL02','DAHR03','DAY03','FLAG03','QUAL03',\
    'DAHR04','DAY04','FLAG04','QUAL04','DAHR05','DAY05','FLAG05','QUAL05',\
    'DAHR06','DAY06','FLAG06','QUAL06','DAHR07','DAY07','FLAG07','QUAL07',\
    'DAHR08','DAY08','FLAG08','QUAL08','DAHR09','DAY09','FLAG09','QUAL09',\
    'DAHR10','DAY10','FLAG10','QUAL10','DAHR11','DAY11','FLAG11','QUAL11',\
    'DAHR12','DAY12','FLAG12','QUAL12','DAHR13','DAY13','FLAG13','QUAL13',\
    'DAHR14','DAY14','FLAG14','QUAL14','DAHR15','DAY15','FLAG15','QUAL15',\
    'DAHR16','DAY16','FLAG16','QUAL16','DAHR17','DAY17','FLAG17','QUAL17',\
    'DAHR18','DAY18','FLAG18','QUAL18','DAHR19','DAY19','FLAG19','QUAL19',\
    'DAHR20','DAY20','FLAG20','QUAL20','DAHR21','DAY21','FLAG21','QUAL21',\
    'DAHR22','DAY22','FLAG22','QUAL22','DAHR23','DAY23','FLAG23','QUAL23',\
    'DAHR24','DAY24','FLAG24','QUAL24','DAHR25','DAY25','FLAG25','QUAL25',\
    'DAHR26','DAY26','FLAG26','QUAL26','DAHR27','DAY27','FLAG27','QUAL27',\
    'DAHR28','DAY28','FLAG28','QUAL28','DAHR29','DAY29','FLAG29','QUAL29',\
    'DAHR30','DAY30','FLAG30','QUAL30','DAHR31','DAY31','FLAG31','QUAL31']
colStr = ','.join(colNames)
formStr = '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s,'\
    '%s,%s,%s,%s,%s,%s,%s,%s'

colTypes = ['INTEGER','INTEGER','INTEGER','INTEGER','VARCHAR(4)','VARCHAR(2)','INTEGER','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)',\
    'INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)','INTEGER','INTEGER','VARCHAR(1)','VARCHAR(1)']

#Specify the output table
outTable = 'daily_precip_data'

#Specify the subset output table, and columns to match
subTable = 'watershed_daily_precip_stations'
subOutTable = 'watershed_daily_precip_data'
outTableCol = 'COOPID'
subTableCol = 'COOPID'

#Open the mysql connection
dbConn = MySQLdb.connect(host=dbInfo['host'], user=dbInfo['user'], passwd=dbInfo['pass'], db=dbInfo['db'])
cursor = dbConn.cursor()

#Create the MySQL table if it does not exist
statement = 'CREATE TABLE IF NOT EXISTS ' + outTable + '('
for m in range(0,len(colNames)):
    statement = statement + colNames[m] + ' ' + colTypes[m] + ', '
statement = statement[0:-2] + ');'
cursor.execute(statement)

#Define a lambda function for properly handling empty columns
def empty(val):
    if val=='':
        return None
    else:
        return val

#Loop through the text files
for inFile in inFiles:
    #Open the text file
    fileConn = open(inFile,'rU')

    #Skip headerlines
    for m in range(0,headerlines):
        line = fileConn.readline()

    #Read in the text file and write it to the DB
    while 1:
        line = fileConn.readline()[0:-1] #omit the \n line feed character
        if line == '':
            break

        #Split the line using the delimeter
        lineSplit = line.split(delimeter)

        #Replace empty list items with None
        lineSplit = [empty(item) for item in lineSplit]

        #Write this line to the MySQL database
        statement = 'INSERT INTO ' + outTable + ' (' + colStr + ') VALUES (' + formStr + ')'
        cursor.execute(statement,lineSplit)

    #Commit those changes
    dbConn.commit()

#Now, select a subset of the rows for the watershed
cursor.execute('DROP TABLE IF EXISTS ' + subOutTable)
cursor.execute('CREATE TABLE ' + subOutTable + ' AS SELECT ' + outTable + '.* FROM ' +\
    outTable + ',' + subTable + ' WHERE ' + outTable + '.' + outTableCol + ' = ' +\
    subTable + '.' + subTableCol)
test = 'CREATE TABLE ' + subOutTable + ' AS SELECT ' + outTable + '.* FROM ' +\
    outTable + ',' + subTable + ' WHERE ' + outTable + '.' + outTableCol + ' = ' +\
    subTable + '.' + subTableCol
dbConn.commit()

#Close the connections
fileConn.close()
dbConn.close()