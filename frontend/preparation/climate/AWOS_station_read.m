%this function reads in AWOS data from a formatted text file and returns a
%MATLAB structured array with fields declared below
%make sure in the original file that no column entries are missing, this
%may be difficult to do, so check through the output thoroughly!


mysql_host='localhost';
mysql_database='ilhm_inputs';
mysql_user='ILHM';
mysql_password='dataaccess';


data_path='F:\Users\Anthony\Data\Other_Downloaded\andreesen\mkgwtr';
filenames={...%'big_rapids_9906.txt','cadillac_9306.txt','gaylord_9906.txt','grayling_0406.txt',...
    'houghton_lake_8006.txt','ludington_0106.txt','manistee_8806.txt','muskegon_8006.txt'};
matlab_data_file='awos_stations.mat';



%declare data format string
format='%6s %4s %12s %3s %3s %3s %3s %3s %1s %1s %1s %4s %2s %2s %2s %2s %1s %4s %4s %6s %5s %6s %3s %3s %5s %5s %5s %2s';

%declare field names
fields={'usaf_call','wban_id','date','wind_az','wind_spd','wind_gus','ceiling',...
    'sky_cov','low_cloud','mid_cloud','high_cloud','vis','pres_weath_1','pres_weath_2',...
    'pres_weath_3','pres_weath_4','past_weath','temp','dewp','sea_level_press','altimeter',...
    'station_press','max_temp','min_temp','precip_6','precip_12','precip_24','snow_depth'};

%declare input field types
int_cols=[1,4,9,10,11,13,14,15,16,17];
float_cols=[5,6,7,12,18,19,20,21,22,23,24,25,26,27,28];
str_cols=[2,3,8];

%declare MYSQL columns and table name
mysql_table='awos_data';
mysql_columns={'usaf_call','wban_id','date','wind_az','wind_spd','wind_gus','ceiling',...
    'sky_cov','low_cloud','mid_cloud','high_cloud','vis','pres_weath_1','pres_weath_2',...
    'pres_weath_3','pres_weath_4','past_weath','temp','dewp','sea_level_press','altimeter',...
    'station_press','max_temp','min_temp','precip_6','precip_12','precip_24','snow_depth'};
mysql_coltypes={'mediumint','varchar(4)','datetime','smallint','float','float','float','varchar(4)',...
    'tinyint','tinyint','tinyint','float','tinyint','tinyint','tinyint','tinyint','tinyint',...
    'float','float','float','float','float','float','float','float','float','float','float'};

%prepare variables used herein
num_cols=length(fields);
cur_dir=cd;




%initialize data file
cd(data_path);
eval_string=['save ',matlab_data_file,' matlab_data_file'];
eval(eval_string);

%open the mysql database
myopen(mysql_host,mysql_user,mysql_password);
dbopen(mysql_database);

%create a table for the data
%tbadd(mysql_table,mysql_columns,mysql_coltypes);

%% loop through the data files
for g=1:length(filenames)
    filename=char(filenames{g});
    matlab_variable=filename(1:end-4);

    %% read in the data
    fid=fopen(filename);
    data=textscan(fid,format,'headerlines',1);
    fclose(fid);

    %% clean up the data
    h=waitbar(0,'Cleaning the data');
    for i=1:num_cols
        waitbar(i/num_cols,h);
        for j=1:6 %replace the stars with NaN
            stars=strmatch(repmat('*',7-j),data{i});
            if ~isempty(stars)
                for k=1:length(stars)
                    data{i}{stars(k)}='NaN';
                end
            end
        end
        if ismember(i,(25:28)) %remove the 'T' for trace
            trace_flag=strmatch('.00T',data{i});
            if ~isempty(trace_flag)
                for k=1:length(trace_flag)
                    data{i}{trace_flag(k)}='.00';
                end
            end
        end
    end
    if ishandle(h); close(h); end

    %% convert the data into appropriate types

    h=waitbar(0,'Converting Datatypes');
    index=length(str_cols);
    for i=1:length(int_cols)
        index=index+1;
        waitbar(index/num_cols,h);
        temp_data=int32(str2num(char(data{int_cols(i)}))); %#ok<ST2NM>
        if isempty(temp_data)
            temp_data=zeros(size(data{int_cols(i)}))*NaN;
        end
        data{int_cols(i)}=temp_data;
    end
    for i=1:length(float_cols)
        index=index+1;
        waitbar(index/num_cols,h);
        temp_data=str2num(char(data{float_cols(i)})); %#ok<ST2NM>
        if isempty(temp_data)
            temp_data=zeros(size(data{int_cols(i)}))*NaN;
        end
        data{float_cols(i)}=temp_data;
    end
    if ishandle(h); close(h); end

    %% convert units to SI
    %date to MATLAB format and convert from GMT to EST
    data{3}=datenum(data{3},'yyyymmddHHMM');

    %windspeeds from knots to m/s
    for i=5:6
        data{i}=unit_conversions(data{i},'kts');
    end

    %ceiling to meters from hundreds of feet
    temp_data=data{7};
    temp_data(temp_data==722)=Inf;
    temp_data=unit_conversions(temp_data*100,'ft');
    data{7}=temp_data;

    %visibility from statute miles to meters
    data{12}=unit_conversions(data{12},'mi');

    %temperatures from F to C
    for i=18:19
        data{i}=unit_conversions(data{i},'F');
    end
    for i=23:24
        data{i}=unit_conversions(data{i},'F');
    end

    %pressures from millibars/inches to Pa
    data{20}=unit_conversions(data{20},'mbar');
    data{21}=unit_conversions(data{21},'inHg');
    data{22}=unit_conversions(data{22},'mbar');

    %precip and snow depth from inches to meters
    for i=25:28
        data{i}=unit_conversions(data{i},'in');
    end

    %% convert the cell array to a structured array
    for i=1:num_cols
        data_struct.(fields{i})=data{i};
    end


    eval_string_1=[matlab_variable,'=data_struct;'];
%     load(matlab_data_file,matlab_variable);
    eval_string_2=['save ',matlab_data_file,' ',matlab_variable,' -append'];
    eval_string_3=['clear ',matlab_variable];
    eval_string_4=[matlab_variable,'.date=cellstr(datestr(',matlab_variable,'.date,31));'];
    eval_string_5=['tbwrite(mysql_table,',matlab_variable,',mysql_columns,3);'];


    eval(eval_string_1);
    eval(eval_string_2);
    clear data data_struct
    eval(eval_string_4);
    eval(eval_string_5);

    eval(eval_string_3);
end
cd(cur_dir);
