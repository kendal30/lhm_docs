inputDir = 'C:\Users\Anthony\Project_Files\2010\Bay_Harbor_Consulting\Compiled_Data';
inputFiles = {'WeatherStationData_complete.csv'};
inputHeaders = {'TIMESTAMP','Batt_Volt_Min','Batt_Volt_Avg','PTemp_C_Avg','Temp_F_Avg','BP_inHg_Avg','SlrkW_kW_m2_Avg','AirTF_Avg',...
    'WS_mph_Avg','WS_mph_S_WVT','WindDir_D1_WVT_deg','WindDir_SD1_WVT_deg','RH_percent','Rain_in_Tot'};
inputFormat = '%s %f %f %f %f %f %f %f %f %f %f %f %f %f';
inputDelimiter = ',';
inputHeaderlines = 1;
inputDateform = 'mm/dd/yy HH:MM';

%Mapping Information
mapIds = {'bay_harbor'};
mapHeaders = {'','','','','','pressure','solar_rad','air_temp',...
    'wind_avg','','wind_dir','','rel_hum','precip'};
mapNoData = -999;

%MySQL table information
db = struct('host','192.168.0.55','user','ILHM','pass','dataaccess','db','ilhm_inputs');
db.stationTable.name = 'bay_harbor_station';
db.stationTable.cols = {'ID','stationID','start_date','end_date'};
db.dataTable.name = 'bay_harbor_data';
db.dataTable.cols = {'ID','stationID','datetime'};
db.dataTable.vecs = {'ID','stationID','datetime'};

%Open a MySQL session
myopen(db.host,db.user,db.pass);
dbopen(db.db);

%Get the maximum row ID
queryOutput = tbread(db.dataTable.name,db.dataTable.vecs(1),db.dataTable.cols(1));
if ~isempty(queryOutput.ID)
    currMaxId = max(queryOutput.ID);
else
    currMaxId = 0;
end

for m = 1:length(inputFiles)
    %Read in the input data into a structured array
    inputData = textread_struct(inputHeaders,[inputDir,'\',inputFiles{m}],inputFormat,'headerlines',inputHeaderlines,'delimiter',inputDelimiter);

    %Convert NoData values
    for n = 2:length(inputHeaders)
        testNoData = (inputData.(inputHeaders{n}) == mapNoData);
        inputData.(inputHeaders{n})(testNoData) = NaN;
    end
    
    %Parse the input date
    inputDates = datenum(inputData.TIMESTAMP,inputDateform);

    %Transform from EDT to EST
    inputDates = daylight_savings_correction(inputDates);
    
    %Transform units
    inputData.BP_inHg_Avg = unit_conversions(inputData.BP_inHg_Avg,'inHg','Pa');
    inputData.SlrkW_kW_m2_Avg = inputData.SlrkW_kW_m2_Avg * 1000;
    inputData.AirTF_Avg = unit_conversions(inputData.AirTF_Avg,'F','C');
    inputData.WS_mph_Avg = unit_conversions(inputData.WS_mph_Avg,'miph','mps');
    inputData.Rain_in_Tot = unit_conversions(inputData.Rain_in_Tot,'in','m');
    
    %Do some manual qa/qc at this step
    test = inputData.BP_inHg_Avg < 0.97 * 10^5;
    inputData.BP_inHg_Avg(test) = NaN;
    
    %Query the database for the current station to see what dates are
    %already loaded
    whereStr = ['WHERE `stationID` LIKE "',mapIds{m},'"'];
    queryOutput = tbread(db.dataTable.name,db.dataTable.vecs,db.dataTable.cols,whereStr);
    
    %Find the start and end dates of the sequence
    if ~isempty(queryOutput.datetime)
        queryDates = datenum(queryOutput.datetime,'yyyy-mm-dd HH:MM:SS');
        minDate = min(queryDates);
        maxDate = max(queryDates);
        
        %Check to see if any of the input dates do not already match values
        %in the table
        newDates = ~ismember(inputDates,queryDates);
    else
        newDates = true(size(inputDates));
    end
    
    %Now, construct the output structured array
    output.ID = (currMaxId+1:1:currMaxId+sum(newDates))';
    output.stationID = cellstr(repmat(mapIds{m},[sum(newDates),1]));
    output.datetime = cellstr(datestr(inputDates(newDates),'yyyy-mm-dd HH:MM:SS'));
    for n = 3:length(mapHeaders)
        if ~isempty(mapHeaders{n})
            output.(mapHeaders{n}) = inputData.(inputHeaders{n})(newDates);
        end
    end
    
    %Write the data to the data table
    tbwrite(db.dataTable.name,output,{},3);  
    
    %Update the maximum ID for the next table
    currMaxId = max(output.ID);
end

myclose;