def ftp_fetch(remoteAddress,remoteBaseDir,localBaseDir,stationList,remoteDirList):
    def handle_download(block):
        localDisk.write(block)

    #Imports
    from ftplib import FTP
    import os

    #Login to the server, navigate to the base directory
    ftp = FTP(remoteAddress)
    ftp.login()
    ftp.cwd(remoteBaseDir)

    #Initialize the file list
    fileList = []

    #Loop through the station and directory lists, looking for relevant files, and downloading
    #them to the local repository
    for remoteDir in remoteDirList:
        #Determine the local Dir
        localDir = os.path.join(localBaseDir,remoteDir)

        #Navigate to the remote directory
        ftp.cwd(remoteDir)

        #Get the complete directory list, try multiple times in case of failure
        notList = True
        while notList:
            try:
                remoteDirList = ftp.nlst()
                notList = False
            except:
                notList = True

        #Check to see if the local directory exists, create it if not
        if not os.path.exists(localDir):
            os.mkdir(localDir)

        for station in stationList:
            #Check to see if any files on the server exist that correspond to this station
            stationCheck = re.compile(station)
            remoteFiles = [station for station in remoteDirList if stationCheck.match(station)]

            #Loop through these files
            for remoteFile in remoteFiles:
                #Determine the local file name
                localFile = os.path.join(localDir,remoteFile)
                if os.path.exists(localFile):
                    notFetched = False
                else:
                    notFetched = True
                    fileList.append(localFile)

                while notFetched:
                    #First, open the file locally
                    localDisk = open(localFile,'wb')
                    try:
                        #Download the file
                        ftp.retrbinary('RETR '+ remoteFile, handle_download)

                        #Move on to next file
                        notFetched = False
                    except:
                        notFetched = True

                    #Close the file locally when complete
                    localDisk.close()

        #Navigate back up one directory
        ftp.cwd('..')

    #Finally, close the FTP connection
    ftp.close()

    #Return a list of files downloaded
    return fileList

def read_file(inFile,headerlines,delimeter):
    def empty(val):
        if val=='':
            return None
        else:
            return val
    #Open the text file
    fileConn = open(inFile,'rU')

    #Skip headerlines
    for m in range(0,headerlines):
        line = fileConn.readline()

    #Read in the text file and write it to a nested list
    fileLines = []
    while 1:
        line = fileConn.readline()[0:-1] #omit the \n line feed character
        if line == '':
            break

        #Split the line using the delimeter
        lineSplit = re.split(delimeter,line)

        #Replace empty list items with None
        lineSplit = [empty(item) for item in lineSplit]

        #Append this list
        fileLines.append(lineSplit)

    return fileLines

def replace_stars(inFile):
    from numpy import nan
    def replace_func(item,test):
        if test:
            item = nan
        else:
            item = item
        return item

    import re

    #Compile the regular expression
    starCheck = re.compile('\*')

    outFile = [[replace_func(column,starCheck.match(column)) for column in line] for line in inFile]
    return outFile

#Begin main code
#Specify these values
startDate = '1892-01-01'
endDate = '2009-12-31'

stationsInventory = 'C:/Users/Anthony/Data/Other_Downloaded/NCDC/Surface_Hourly/us_mi_surface_stations_inventory.dbf'
inventoryFields = {'USAF':'USAF','WBAN':'WBAN'}

remoteAddress = 'ftp.ncdc.noaa.gov'
remoteBaseDir = '/pub/data/noaa'

tempDir = 'c:/users/anthony/temp'
localBaseDir = 'C:/Users/Anthony/Data/Other_Downloaded/NCDC/Surface_Hourly'

reformatExec = 'C:/Users/Anthony/Data/Other_Downloaded/NCDC/Surface_Hourly/ishapp2.exe'
rawExt = '.raw'
procExt = '.txt'
headerlines = 1
delimeter = '[ ]*'

inDateFormat = '%Y-%m-%d'
procDateFormat = '%Y%m%d%H%M'
dbDateFormat = '%Y-%m-%d %H:%M:%S'

dbInfo = {'host':'192.168.0.55','user':'ILHM','pass':'dataaccess','db':'ilhm_inputs'}
dataTable = 'ncdcSurfaceHourly'
colNames = ['usaf_wban','usaf_call','wban_id','date','wind_az','wind_spd','wind_gus','ceiling',\
    'sky_cov','low_cloud','mid_cloud','high_cloud','vis','pres_weath_1','pres_weath_2',\
    'pres_weath_3','pres_weath_4','past_weath','temp','dewp','sea_level_press','altimeter',\
    'station_press','max_temp','min_temp','precip_6','precip_12','precip_24','snow_depth']
colTypesData = ['varchar(12)','mediumint','varchar(4)','datetime','smallint','float','float','float','varchar(4)',\
    'tinyint','tinyint','tinyint','float','tinyint','tinyint','tinyint','tinyint','tinyint',\
    'float','float','float','float','float','float','float','float','float','float','float']
colStrData = ','.join(colNames)
formStrData =

stationsTable = 'ncdcSurfaceHourly_stations'
stationsInventoryCols = ['USAF','WBAN','start_date','end_date']

#Import modules
import MySQLdb, gzip, os, subprocess
from dbfpy import dbf
from datetime import datetime
from shutil import copyfile
from numpy import nan, inf, isnan, isinf

#Open database connection
dbConn = MySQLdb.connect(host=dbInfo['host'], user=dbInfo['user'], passwd=dbInfo['pass'], db=dbInfo['db'])
cursor = dbConn.cursor()

#Read in the list of stations to check


#Create the data table if it does not exist
statement = 'CREATE TABLE IF NOT EXISTS ' + dataTable + '('
for m in range(0,len(colNames)):
    statement = statement + colNames[m] + ' ' + colTypes[m] + ', '
statement = statement[0:-2] + ');'
cursor.execute(statement)




#Read in the station table, determine start and end dates for each station
#Update the station table
#Query the maximum and minimum dates for each station
'SELECT MAX(date) FROM data GROUP BY station'

queryString = 'SELECT ' + ', '.join(stationsInventoryCols) + ' FROM ' + stationsTable
dbConn.query(queryString)
result = dbConn.store_result() #only use store_result on smaller queries, it fetches the entire query result, use_result() just makes it available
stationsInventory = dict()
row = result.fetch_row()
while row:
    stationsInventory['%s-%s' % (row[0][0],row[0][1])] = {'start':row[0][2],'end':row[0][3]}
    row = result.fetch_row()

#Create stationList string for use
stationList = stationsInventory.keys()

#Build the list of years
startTime = datetime.strptime(startDate,inDateFormat)
endTime = datetime.strptime(endDate,inDateFormat)
yearRange = [str(year) for year in range(int(startTime.year),int(endTime.year)+1)]
remoteDirList = ['additional']
[remoteDirList.append(year) for year in yearRange if int(year) > 1900]

#Fetch the data from the ftp server
fileList = ftp_fetch(remoteAddress,remoteBaseDir,localBaseDir,stationList,remoteDirList)

#Set the startupinfo for subprocess to avoid launching a command window
startInfo= subprocess.STARTUPINFO()
startInfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

#Now, process each file individually
#Open the database cursor
cursor = dbConn.cursor()
for inFile in fileList[0:2]:
    #Get the output temporary filenames
    inFilename = os.path.split(inFile)[1]
    (inFileBase,inFileExt) = os.path.splitext(inFilename)
    outFileBase = os.path.join(tempDir,inFileBase)
    outFileRaw = outFileBase + rawExt
    outFileProc = outFileBase + procExt

    #Extract the data to a temporary directory, if zipped
    if inFileExt == '.gz':
        gzFile = gzip.GzipFile(inFile)
        content = gzFile.read()
        gzFile.close()

        #Write the raw file
        outFile = open(outFileRaw,'wb')
        outFile.write(content)
        outFile.close()
    else:
        copyfile(inFile,outFileRaw)

    #Execute the helper program, because of some limitation in the executable, I need
    #to use only the local name, and cwd to the temp directory
    spawnArgs = [reformatExec, os.path.split(outFileRaw)[1], os.path.split(outFileProc)[1]]
    subprocess.Popen(spawnArgs, startupinfo=startInfo, cwd=tempDir).wait()

    #Read in the data, return a list of lines as lists
    fileLines = read_file(outFileProc,headerlines,delimeter)

    #Convert stars in the data to nan (to allow math without errors)
    fileLines = replace_stars(fileLines)

    #Convert units of data, and parse date format, then load data into database
    for line in fileLines:
        #parse date format
        tempDate = datetime.strptime(line[2],procDateFormat)
        #check to see if this station is already in the database
        thisStation = '%s-%s' % (line[0],line[1])
        if thisStations in stationList:
            #check to see if date is already in the database
            test1 = (stationsInventory[thisStation]['start'] is None) and (stationsInventory[thisStation]['end'] is None) #dates not defined
            test2 = tempDate < stationsInventory[thisStation]['start']
            test3 = tempDate > stationsInventory[thisStation]['end']
        else: #go ahead and add data for new stations, this will be updated later
            test1 = test2 = test3 = True

        if test1 or test2 or test3:
            line[2] = tempDate.strftime(dbDateFormat)
            #convert windspeed from knots to m/s
            line[4] = 0.514444 * line[4]
            line[5] = 0.514444 * line[5]
            #convert ceiling to meters from hundreds of feet
            if line[6]==722: line[6] = float('inf')
            line[6] = line[6]*100*0.3048
            if isinf(line[6]): line[6] = 99999
            #visibility from statute miles to meters
            line[11] = line[11]*1609.34
            #temperatures from F to C
            line[17] = (line[17]-32)/1.8
            line[18] = (line[18]-32)/1.8
            line[22] = (line[22]-32)/1.8
            line[23] = (line[23]-32)/1.8
            #pressures from millibars/inchesHg to Pa
            line[19] = line[19] * 100 #mbar
            line[20] = line[20] * 3386.39 #inHg
            line[21] = line[21] * 100
            #precip and snow depth from inches to meters
            for m in range(24,28):
                line[m] = line[m] * 0.0254

            #Replace nan with none
            line = [None for item in line if isnan(item)]

            #Finally, add a composite usaf_wban field
            lineStore = line
            line = [thisStation]
            line.extend(lineStore)

            #Load line into the database
            statement = 'INSERT INTO ' + dataTable + ' (' + colStr + ') VALUES (' + formStr + ')'
            cursor.execute(statement,line)

    #Commit those changes
    dbConn.commit()

    #Clean up temporary directory
    os.remove(outFileRaw)
    os.remove(outFileProc)

#Close connections
inventory.close()
dbConn.close()

