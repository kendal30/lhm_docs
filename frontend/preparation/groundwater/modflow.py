#-*- coding: utf-8 -*-
'''This module contains all of the functions needed to produce inputs to MODFLOW in LHM
the four key modules are ibound_prep, dem_dependent_prep, aquifer_properties_prep,
and internal_bc_prep

For each function, call as:
    ibound_prep(inputDB, pathConfig, params, subModel=False),
either omitting the subModel=False, or specifying the subModel name as a string if using.

This can be used outside of LHM. In that case the call looks like:
    ibound_prep(inputDB=[], pathConfig=[], params=groundwater, lhm=False, altSources=[], altConfig=[])

In this case, the dictionary groundwater specifies the inputs as in config_sources.ipynb.
Use this as a template to create your own. You will need to import this module in order
to call the functions.

You will also need LHM's helper modules "raster" and "gis" in your path.
'''


'''
--------------------------------------------------------------------------------
Helper Functions
--------------------------------------------------------------------------------
'''
from xml.dom.expatbuilder import parseString


def names_module(reqList):
    """These inputs are used only within the module below. If you are using LHM, then 
    this module is not used, rather the names are pulled from lhm_frontend. This is used
    only for use outside LHM.
    """

    names = dict()
    names['modGndBot'] = 'bottom'
    names['modGndTop'] = 'model_top'
    names['modGndStart'] = 'start_heads'
    names['modGndIbound'] = 'ibound'
    names['modGndIboundFeature'] = 'ibound_grid.shp'

    names['modGndSpecYield'] = 'spec_yield'
    names['modGndSpecStor'] = 'spec_stor'
    names['modGndHorizCond'] = 'hk'
    names['modGndVertAni'] = 'vani'

    names['modGndETDepth'] = 'et_extinct_depth'
    names['modGndETSurface'] = 'et_surf'

    names['modGndDrnElev'] = 'drains_elev'
    names['modGndDrnCond'] = 'drains_cond'
    names['modGndDrnPresence'] = 'drn'

    names['modGndRivElev'] = 'rivers_elev'
    names['modGndRivCond'] = 'rivers_cond'
    names['modGndRivBot'] = 'rivers_bot'
    names['modGndRivPresence'] = 'riv'

    names['modGndGhbElev'] = 'ghb_elev'
    names['modGndGhbCond'] = 'ghb_cond'
    names['modGndGhbPresence'] = 'ghb'

    names['modGndChdElev'] = 'chd_elev'
    names['modGndChdPresence'] = 'chd'

    names['outExtRaster'] = '.tif'

    names['srcTop'] = 'top'
    names['srcBottom'] = 'bottom'
    names['srcIbound'] = 'ibound'
    names['srcBdryHeads'] = 'constant_heads'
    names['srcHorizCond'] = 'horizontal_conductivity'
    names['srcVertAni'] = 'vertical_anisotropy'
    names['srcSpecYield'] = 'specific_yield'
    names['srcSpecStor'] = 'specific_storage'
    names['srcHorizCondDeep'] = 'deep_horizontal_conductivity'
    names['srcVertAniDeep'] = 'deep_vertical_anisotropy'
    names['srcSpecYieldDeep'] = 'deep_specific_yield'
    names['srcSpecStorDeep'] = 'deep_specific_storage'
    names['srcEvtExtDepth'] = 'evt_exctinction_depth'
    names['srcRivElev'] = 'riv_elevation'
    names['srcRivDepth'] = 'riv_bottom_depth'
    names['srcDrnBedK'] = 'drn_bed_conductivity'
    names['srcDrnBedThick'] = 'drn_bed_thickness'
    names['srcRivBedK'] = 'riv_bed_conductivity'
    names['srcRivBedThick'] = 'riv_bed_thickness'
    names['srcRivExcMask'] = 'riv_exclusion_mask'
    names['srcDrnExcMask'] = 'drn_exclusion_mask'

    # Return what is requested
    if reqList is None:
        return names
    elif isinstance(reqList,list):
        if len(reqList) > 1:
            return [names[thisReq] for thisReq in reqList]
        else:
            return names[reqList[0]]
    elif isinstance(reqList,str):
        return names[reqList]


def read_layer(arcpy,inDir,inName,inLay,outExtRaster,readType,noDataToValue=None):
    """
    This function helps read in layered inputs, even if they have been collapsed.
    
    Options for readType are 'array' or 'raster'
    """
    import os
    from raster import read_raster
    
    # Build the initial name
    nameRaster = os.path.join(inDir,inName+'_lay%d'%inLay+outExtRaster)
    if not arcpy.Exists(nameRaster):
        nameRaster = os.path.join(inDir,inName+outExtRaster+'/Band_%d'%inLay)
    if arcpy.Exists(nameRaster):
        if readType=='array':
            return read_raster(nameRaster,noDataToValue)
        elif readType=='raster':
            return arcpy.Raster(nameRaster)
    else:
        raise ValueError('input %s must exist, create it first'%nameRaster)


'''
--------------------------------------------------------------------------------
ibound_prep
--------------------------------------------------------------------------------
'''
def ibound_prep(inputDB, pathConfig, params, subModel=False, lhm=True, altSources=[], altConfig=[], printResult=True):
    """This function creates IBOUND arrays for a MODFLOW model.
    
    If using LHM, it assumes that you want to start with the surface grid, and downsample an integer
    number of times. It will check that your input cellsize meets these criterion. If not, it will
    raise an error. You can override this behavior either by setting lhm = False, which will require
    you to specify altSources, or by setting params['surfaceGrid'] = False. Otherwise, params['surfaceGrid']
    is assumed True"""

    import os, pandas, numpy
    import raster, gis, lhm_frontend

    create_modflow_raster = raster.create_modflow_raster
    raster_vals_to_points = gis.raster_vals_to_points

    if lhm:
        import lhm_frontend
        log_lhm_output = lhm_frontend.log_lhm_output
        
        if printResult:
            from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Specify inputs and set environment
    #-------------------------------------------------------------------------------
    if lhm:
        (nameOutIbound, nameOutFeature, nameSurfGrid, outExtRaster,srcIbound) = lhm_frontend.names_frontend(\
                ['modGndIbound','modGndIboundFeature','modSurfGrid','outExtRaster','srcIbound'])

        (sources,inMatchExtent,outDir) = lhm_frontend.lhm_groundwater_inputs(inputDB,pathConfig,subModel)
        scratchDB = inputDB
    else:
        # Get sources and config
        sources = altSources
        inMatchExtent = altConfig['inMatchExtent']
        outDir = altConfig['outDir']
        scratchDB = altConfig['scratchDB']

        # Set names, use module standards
        (nameOutIbound, nameOutFeature, outExtRaster,srcIbound) = names_module(\
                ['modGndIbound','modGndIboundFeature','outExtRaster','srcIbound'])

    # Check inputs, set defaults if necessary
    if 'cellSize' not in params.keys():
        raise LookupError('"cellSize" must be set for the params dictionary')
    if 'surfaceGrid' not in params.keys():
        if lhm:
            if sources[srcIbound]['method'] == 'feature':
                params['surfaceGrid'] = True
            else:
                params['surfaceGrid'] = False
        else:
            params['surfaceGrid'] = False
    if 'iboundFeature' not in params.keys():
        params['iboundFeature'] = True

    # Specify temporary output names used only in this function
    tempResampSurf = 'tempResample_surfGrid'
    tempResamp01 = 'tempResample_01'
    tempFishnet = 'tempFishnet'
    tempFishnetPts = 'tempFishnet_label' # this is generated automatically by the CreateFishnet_management routine
    tempAttribs = 'tempFishnet_attribs'

    # Specify layer format string
    layStrForm = '_lay%d'

    # Environment settings
    arcpy = lhm_frontend.set_environment(params['cellSize'], scratchDB, inMatchExtent)

    #------------------------------------------------------------------------------
    # Create the model grid
    #------------------------------------------------------------------------------
    # If this is an LHM simulation, start by resampling the groundwater model 
    if params['surfaceGrid']:
        # Get the surface grid
        surfDir = os.path.join(os.path.split(outDir)[0],'Surface')
        rasterSurface = arcpy.Raster(os.path.join(surfDir,nameSurfGrid+outExtRaster))

        # Check that 'cellsize' is an integer fraction or mulitple of the model grid dimension
        cellsizeSurface = arcpy.Describe(rasterSurface).meanCellHeight
        test1 = params['cellSize']>cellsizeSurface and (params['cellSize']/cellsizeSurface).is_integer()
        test2 = params['cellSize']<=cellsizeSurface and (cellsizeSurface/params['cellSize']).is_integer()
        if test1 or test2:
            # Resample
            arcpy.Resample_management(rasterSurface,tempResampSurf,params['cellSize'])

            # Convert to 0/1 grid
            rasterResamp = arcpy.sa.Con(arcpy.sa.IsNull(tempResampSurf),0,1)
            rasterResamp.save(tempResamp01)
            
            # Set this as startRaster for the create_modflow_raster function
            startRaster = os.path.join(arcpy.env.workspace,tempResamp01)
        else:
            startRaster = False
    else:
        startRaster = False

    # Then, overlay supplied features (or use the input grid or constant value)
    thisOutPath = os.path.join(outDir,nameOutIbound+layStrForm+outExtRaster)
    create_modflow_raster(sources[srcIbound],range(1,params['numLay']+1),thisOutPath,\
        startRaster=startRaster,noDataToValue=0)
    
    # Log this output
    log_lhm_output(nameOutIbound+'_layN'+outExtRaster,outDir,inputDB,printResult)

    # Clean up
    if params['surfaceGrid']:
        del rasterResamp

    #------------------------------------------------------------------------------
    # Create a 2-D model grid in shapefile format, using the 1st layer ibound grid
    #------------------------------------------------------------------------------
    if params['iboundFeature']:
        # Get the properties of the first ibound layer grid for making the shapefile below
        outIboundFirstLay =  thisOutPath%(1)
        inDescribe = arcpy.Describe(outIboundFirstLay)

        # Create a fishnet at the required resolution
        # note that there is a discrepancy in the documentation (arcmap 10.7 and arcgis pro 1.4) but
        # the code below is correct, and width and height are switched
        arcpy.CreateFishnet_management(tempFishnet,str(inDescribe.extent.XMin)+' '+str(inDescribe.extent.YMin),\
            str(inDescribe.extent.XMin)+' '+str(inDescribe.extent.YMin+1000),\
            inDescribe.meanCellWidth,inDescribe.meanCellHeight,inDescribe.height,inDescribe.width,\
            str(inDescribe.extent.XMax)+' '+str(inDescribe.extent.YMax),'LABELS','','POLYGON')

        # Add a cellid field
        arcpy.AddField_management(tempFishnet,'cellid','long')
        arcpy.CalculateField_management(tempFishnet,'cellid','!OID!')

        # Extract the ibound values for the fishnet centroid points
        raster_vals_to_points(tempFishnetPts,outIboundFirstLay,srcIbound)

        # Add x and y coordinates to the fishnet label points
        arcpy.AddField_management(tempFishnetPts,'X','float')
        arcpy.AddField_management(tempFishnetPts,'Y','float')
        arcpy.CalculateField_management(tempFishnetPts,'X','!Shape.firstPoint.X!')
        arcpy.CalculateField_management(tempFishnetPts,'Y','!Shape.firstPoint.Y!')

        # Bring in the attribute table of the points so that we can calculate i,j
        dfPts = pandas.DataFrame(arcpy.da.TableToNumPyArray(tempFishnetPts,['OID@',srcIbound,'X','Y']))
        dfPts = dfPts.rename(columns={'OID@':'cellid'})

        # Calculate i and j, modflow origin is top-left
        originX = dfPts['X'].min()
        originY = dfPts['Y'].max()
        dfPts['row'] = (numpy.floor((originY - dfPts['Y']) / inDescribe.meanCellHeight) + 1).astype(int)
        dfPts['col'] = (numpy.floor((dfPts['X'] - originX)/ inDescribe.meanCellWidth) + 1).astype(int)

        # Export this table back into the geodatabase
        recOut = dfPts.to_records()
        arrayOut = numpy.array(recOut,dtype=recOut.dtype)
        pathTable = os.path.join(scratchDB,tempAttribs)
        if arcpy.Exists(pathTable):
            arcpy.Delete_management(pathTable)
        arcpy.da.NumPyArrayToTable(arrayOut,pathTable)

        # Add an index
        arcpy.AddIndex_management(tempAttribs,['cellid'],'cellid')

        # Join fields from the attribs table
        arcpy.JoinField_management(tempFishnet, 'cellid', tempAttribs, 'cellid', [srcIbound,'row','col','X','Y'])

        # Copy this to a permanent location
        outFishnet = os.path.join(outDir,nameOutFeature)
        arcpy.CopyFeatures_management(tempFishnet, outFishnet)

        # Log this output
        log_lhm_output(nameOutFeature,outDir,inputDB,printResult)
        
        # Clean up from this step
        [arcpy.Delete_management(thisTemp) for thisTemp in [tempFishnet, tempFishnetPts, tempAttribs]]

    # Print result:
    if lhm and printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('MODFLOW IBOUND is prepared for the <strong>%s</strong> region '%regionName))
        else:
            (thisPath,subModel) = os.path.split(pathConfig)
            regionName = os.path.split(os.path.split(thisPath)[0])[1]
            display(HTML('MODFLOW IBOUND is prepared for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))
    elif printResult:
        display(HTML('MODFLOW IBOUND is prepared'))
        
'''
--------------------------------------------------------------------------------
layers_start_heads
--------------------------------------------------------------------------------
'''
def layers_start_heads(inputDB, pathConfig, params, subModel=False, lhm=True, altSources=[], altConfig=[], printResult=True):
    """This function validates inputs for MODFLOW that are surface-elevation dependent, not including
    internal boundary conditions (drains, evapotranspiration) which are handled separately."""

    import os, numpy
    import raster

    create_modflow_raster = raster.create_modflow_raster
    write_raster = raster.write_raster
    read_raster = raster.read_raster

    if lhm:
        import lhm_frontend
        log_lhm_output = lhm_frontend.log_lhm_output
        
        if printResult:
            from IPython.display import display, HTML


    # Ignore numpy warnings
    numpy.seterr(divide='ignore', invalid='ignore')

    #-------------------------------------------------------------------------------
    # Specify inputs and set environment
    #-------------------------------------------------------------------------------
    if lhm:
        (nameOutBot,nameOutTop,nameOutStart,nameOutIbound,outExtRaster,srcTop,srcBottom,\
            srcStartHeads,srcGndStartHeads,srcBdryHeads) = \
            lhm_frontend.names_frontend(['modGndBot','modGndTop','modGndStart','modGndIbound','outExtRaster',\
            'srcTop','srcBottom','srcStartHeads','srcGndStartHeads','srcBdryHeads'])

        (sources,inMatchExtent,outDir) = lhm_frontend.lhm_groundwater_inputs(inputDB,pathConfig,subModel)
        scratchDB = inputDB
    else:
        # Get sources and config
        sources = altSources
        inMatchExtent = altConfig['inMatchExtent']
        outDir = altConfig['outDir']
        scratchDB = altConfig['scratchDB']

        # Set names, use module standards <-- in general these could differ from LHM
        (nameOutBot,nameOutTop,nameOutStart,nameOutIbound,outExtRaster,srcTop,srcBottom,\
            srcStartHeads,srcBdryHeads) = \
            names_module(['modGndBot','modGndTop','modGndStart','modGndIbound','outExtRaster',\
            'srcTop','srcBottom','srcStartHeads','srcBdryHeads'])

    # Specify temporary names
    tempTop = 'tempTop'
    tempThick = 'tempThick'
    tempBaseFix = 'tempBaseFix'
    tempBot = 'tempBot'
    tempStartHeads = 'tempStart'
    tempBdry = 'tempBdry'

    # Specify layer format string
    layStrForm = '_lay%d'

    # Environment settings
    arcpy = lhm_frontend.set_environment(params['cellSize'], scratchDB, inMatchExtent)

    # Get the number of layers into a local variable
    numLay = params['numLay']

    #------------------------------------------------------------------------------
    # Validate layer inputs and resample grids to model resolution
    #------------------------------------------------------------------------------
    # This also dynamically creates layers according to a set of rules specified
    # in the 'params' dictionary

    # First, create the model top grid
    create_modflow_raster(sources[srcTop],1,tempTop) # top only has a single layer
    rasterTop = arcpy.Raster(tempTop) # get this as a raster object

    # Handle the starting heads correctly, if specified in the groundwater model section, use those
    # otherwise, use the surface-model starting heads
    if srcGndStartHeads in sources:
        if sources[srcGndStartHeads] is not None:
            sources[srcStartHeads] = sources[srcGndStartHeads]

    # Correct the model top array, setting it equal to the boundary heads values if above them
    if params['useBdryHead']:
        create_modflow_raster(sources[srcBdryHeads],1,tempBdry) # get the 1st layer boundary heads
        rasterBdryHead = arcpy.Raster(tempBdry)

        # Read in the ibound array as a raster object
        rasterIbound = read_layer(arcpy,outDir,nameOutIbound,1,outExtRaster,'raster')
        rasterTopMin = arcpy.sa.Con(rasterBdryHead < rasterTop, rasterBdryHead, rasterTop)
        rasterTopFix = arcpy.sa.Con(rasterIbound==-1,rasterTopMin, rasterTop)
        thisOutPath = os.path.join(outDir,nameOutTop+outExtRaster)
        rasterTopFix.save(thisOutPath)

        # Clean up
        del rasterIbound, rasterTopMin, rasterTopFix
        arcpy.Delete_management(tempBdry)

        # Prepare for later use
        rasterTop = arcpy.Raster(thisOutPath)
        
    else:
        thisOutPath = os.path.join(outDir,nameOutTop+outExtRaster)
        rasterTop.save(thisOutPath)
        
    # Log this output
    log_lhm_output(nameOutTop+outExtRaster,outDir,inputDB,printResult)
    
    # Create temporary bottom grids in the scratch GDB, will modify them below
    thisOutPath = tempBot + layStrForm
    tempBotRasters = create_modflow_raster(sources[srcBottom],range(1,numLay+1),thisOutPath)

    # Nibble the bottom raster out to the edges of the model domain
    rasterBase = arcpy.Raster(tempBotRasters[-1])
    # Read in the ibound array as a raster object
    rasterIbound = read_layer(arcpy,outDir,nameOutIbound,numLay,outExtRaster,'raster')

    # Create the mask raster, if there are cells that are noData in rasterThis, they will be that in the mask too
    rasterMask = arcpy.sa.Con(rasterIbound!=0,rasterBase)
    # Now, nibble
    rasterNibble = arcpy.sa.Nibble(rasterBase,rasterMask)
    tempNibbleOut = os.path.join(outDir,'temp_'+nameOutBot + layStrForm%(numLay)+outExtRaster)
    rasterNibble.save(tempNibbleOut)

    # Create a thickness raster
    rasterThick = rasterTop - rasterNibble
    rasterThick.save(tempThick)

    # Determine the minimum total thickness allowed, adjust base downward if needed, save
    if isinstance(params['minLayThick'],list):
        if len(params['minLayThick'])>1:
            minThick = numpy.sum(params['minLayThick'])
        else:
            minThick = params['minLayThick'][0] * numLay
    else:
        minThick = params['minLayThick'] * numLay
    minThick = float(minThick)
    rasterBaseFix = arcpy.sa.Con(rasterThick < minThick, rasterTop - minThick, rasterNibble)
    rasterBaseFix.save(tempBaseFix)

    # Log output later once finished
    
    # Read this in for later
    arrayBaseFix = read_raster(tempBaseFix,noDataToValue=numpy.NaN)

    # Clean up the layers we don't need
    del rasterBase, rasterThick, rasterIbound, rasterMask, rasterBaseFix
    arcpy.Delete_management(tempBotRasters[-1])
    arcpy.Delete_management(tempNibbleOut)
    arcpy.Delete_management(tempThick)

    #------------------------------------------------------------------------------
    # Create the starting heads grids
    #------------------------------------------------------------------------------
    # Next, validate and resample the starting heads grid and constant heads grid
    # This will use a specified constant heads value to replace starting head values in constant head ibound cells
    for thisLay in range(0,numLay):
        # First, resample the grid to a temporary layer
        if thisLay+1 <= len(sources[srcStartHeads]['data']):
            create_modflow_raster(sources[srcStartHeads],thisLay+1,tempStartHeads)
            rasterStart = arcpy.Raster(tempStartHeads) # get this as a raster object, just for later output
            arrayStart = read_raster(tempStartHeads,noDataToValue=numpy.NaN)
        elif thisLay>0:
            prevLayerPath = os.path.join(outDir,nameOutStart + layStrForm%(thisLay) + outExtRaster)
            arrayStart = read_raster(prevLayerPath,noDataToValue=numpy.NaN)
            rasterStart = arcpy.Raster(prevLayerPath)

        # Now, assign a minimum value
        if params['minStartHeads']:
            arrayMinVal = numpy.where(arrayStart > params['minStartHeads'], arrayStart, params['minStartHeads'])
        else:
            arrayMinVal = arrayStart.copy()

        # Constrain to lie at least minThick above the base of the aquifer
        testFix = arrayMinVal < (arrayBaseFix + minThick)
        arrayMinVal[testFix] = arrayBaseFix[testFix] + minThick

        # Imprint boundary heads, if using
        if params['useBdryHead']:
            # Now, create a raster for this layer's boundaryHeads
            create_modflow_raster(sources[srcBdryHeads],thisLay+1,tempBdry)
            arrayInputBdry = read_raster(tempBdry,noDataToValue=numpy.NaN)

            # Bring in the ibound for this layer
            arrayIbound = read_layer(arcpy,outDir,nameOutIbound,(thisLay+1),outExtRaster,'array',0)
            
            # Create the constant head raster using this layer's ibound
            arrayConstHead = numpy.where(arrayIbound == -1, arrayInputBdry, arrayMinVal)

            # Clean up
            del arrayIbound, arrayInputBdry
        else:
            arrayConstHead = arrayMinVal

        # Update the arrayBaseFix as needed to lie below the constant heads
        if thisLay==0:
            testFix = arrayBaseFix > (arrayConstHead - minThick)
            arrayBaseFix[testFix] = arrayConstHead[testFix] - minThick

        # Save the last array
        thisOutPath = os.path.join(outDir,nameOutStart + layStrForm%(thisLay+1) + outExtRaster)
        write_raster(arrayConstHead,thisOutPath,rasterStart,numpy.NaN)
        
        # Clean up
        del rasterStart
        del arrayStart, arrayMinVal, arrayConstHead
        arcpy.Delete_management(tempStartHeads)
        arcpy.Delete_management(tempBdry)

    # Save the arrayBaseFix and clean up
    rasterTemplate = arcpy.Raster(tempTop)
    thisOutPath = os.path.join(outDir,nameOutBot + layStrForm%(numLay)+outExtRaster)
    write_raster(arrayBaseFix,thisOutPath,rasterTemplate,numpy.NaN)
    del rasterTemplate
    arcpy.Delete_management(tempTop)
    arcpy.Delete_management(tempBaseFix)

    
    # Log this output
    log_lhm_output(nameOutStart+'_layN'+outExtRaster,outDir,inputDB,printResult)
                
    # Next, create model layer bottoms if not already specified, otherwise, correct bottoms if already created
    if (len(sources[srcBottom]['data'])==1) and (numLay > 1):
        # Delete our temporary layer bottoms, we don't need these
        for thisLay in range(0,numLay-1):
            arcpy.Delete_management(tempBotRasters[thisLay])

        arrayTop = read_raster(rasterTop,noDataToValue=numpy.NaN)
        if params['layeringBelowWT'] == True:
            # Get the thickness of saturated aquifer to distribute in layers
            pathStartHeads = os.path.join(outDir,nameOutStart + layStrForm%(1) + outExtRaster)
            arrayStartHeads = read_raster(pathStartHeads,noDataToValue=numpy.NaN) + \
                params['minLaySatBelowWT']
            arrayPrevLayBot = numpy.minimum(arrayTop,arrayStartHeads)
        else:
            arrayPrevLayBot = arrayTop.copy()

        # Calculate the thickness of the aquifer to divide among the layers
        arrayThickFix = arrayPrevLayBot - arrayBaseFix

        # Calculate the amount of thickness to distribute dynamically above our equalThickThresh
        arrayExcess = arrayThickFix - params['equalThickThresh'] * numLay
        arrayDistrib = numpy.where(arrayExcess > 0, arrayExcess, 0)

        # Divide layer thicknesses equally for those cells below that thickness
        arrayUniformThick = numpy.where(arrayExcess<=0,arrayThickFix / numLay,params['equalThickThresh'])

        # Now loop through the layers, use the specified maximum layer thicknesses to
        # create model bottoms based on the difference between model top and bottom
        for thisLay in range(0,numLay-1):
            # Determine how many layers are remaining
            numLayRemain = numLay - thisLay

            # Attempt to evenly distribute remaining thickness
            arrayTryAddThick = arrayDistrib / numLayRemain

            # Check to see if that puts this layer over its limit, if there is a limit
            if params['maxLayThick'][thisLay] > 0:
                arrayTryThisThick = arrayUniformThick + arrayTryAddThick
                arrayThisExcess = arrayTryThisThick - params['maxLayThick'][thisLay]
                arrayAddThick = numpy.where(arrayThisExcess>0, arrayTryAddThick - arrayThisExcess, arrayTryAddThick)

                del arrayTryThisThick, arrayThisExcess
            else: # there is no limit to this layer's thickness
                arrayAddThick = arrayTryAddThick

            # Create the thickness for this layer
            arrayThisThick = arrayUniformThick + arrayAddThick

            # Calculate the elevation of the bottom of this layer
            arrayBot = arrayPrevLayBot - arrayThisThick

            # Subtract this arrayAddThick from the rasterDistrib
            arrayDistrib = arrayDistrib - arrayAddThick

            # Now, create the raster for this layer
            thisOutPath = os.path.join(outDir,nameOutBot + layStrForm%(thisLay+1) + outExtRaster)
            write_raster(arrayBot,thisOutPath,rasterTop,numpy.NaN)
        
            # Assign this for the next Loop
            arrayPrevLayBot = arrayBot.copy()

            # Clean up this layer
            del arrayTryAddThick , arrayAddThick, arrayThisThick

        # Clean up the other intermediate steps
        del arrayExcess, arrayDistrib, arrayUniformThick, arrayThickFix, arrayPrevLayBot, \
            arrayBot,  arrayStartHeads
    else:
        for thisLay in range(numLay-2,-1,-1): # already fixed the bottom layer
            
            # Handle either layer-specific or scalar values
            if isinstance(params['minLayThick'],list):
                if len(params['minLayThick'])>1:
                    minThickSurf= float(numpy.sum(params['minLayThick'][0:thisLay+1]))
                    minThickBelow = float(params['minLayThick'][thisLay+1])
                else:
                    minThickSurf = float((thisLay+1) * params['minLayThick'][0])
                    minThickBelow = float(params['minLayThick'][0])
            else:
                minThickSurf = float((thisLay+1) * params['minLayThick'])
                minThickBelow = float(params['minLayThick'])
                        
            # First, Nibble the values out to the edges
            # Get the IBOUND for this layer
            rasterIbound = read_layer(arcpy,outDir,nameOutIbound,(thisLay+1),outExtRaster,'raster')

            rasterThisBotNibble = arcpy.Raster(tempBotRasters[thisLay])
            thisTempOutPath = os.path.join(outDir,'temp_'+nameOutBot + layStrForm%(thisLay+1) + outExtRaster)
            # Create the mask raster, if there are cells that are noData in rasterThis, they will be that in the mask too
            rasterMask = arcpy.sa.Con(rasterIbound!=0,rasterThisBotNibble)
            # Now, nibble
            rasterNibble = arcpy.sa.Nibble(rasterThisBotNibble,rasterMask)
            rasterNibble.save(thisTempOutPath)

            # Next, check that the layer is adequately beneath the surface elevation
            rasterThisBot = arcpy.Raster(thisTempOutPath)
            rasterTopLimit = arcpy.sa.Con((rasterThisBot + minThickSurf)<= rasterTop, \
                rasterThisBot, rasterTop-minThickSurf)
            
            # Make sure that it is at least minLayThick above the layer below
            belowOutPath = os.path.join(outDir,nameOutBot + layStrForm%(thisLay+2) + outExtRaster)
            rasterLayBelow = arcpy.Raster(belowOutPath)
            rasterBotLimit = arcpy.sa.Con(rasterTopLimit >= (rasterLayBelow + minThickBelow),\
                rasterTopLimit, rasterLayBelow+minThickBelow)
            
            thisOutPath = os.path.join(outDir,nameOutBot + layStrForm%(thisLay+1) + outExtRaster)
            rasterBotLimit.save(thisOutPath)
            
            # Clean up
            del rasterLayBelow, rasterBotLimit, rasterThisBot, rasterTopLimit,rasterIbound, \
                rasterThisBotNibble, rasterMask, rasterNibble

            arcpy.Delete_management(tempBotRasters[thisLay])
            arcpy.Delete_management(thisTempOutPath)

    # Log this output
    log_lhm_output(nameOutBot+'_layN'+outExtRaster,outDir,inputDB,printResult)

    # Clean up
    del rasterTop

    #------------------------------------------------------------------------------
    # Add in deep bedrock
    #------------------------------------------------------------------------------
    # Handle the deep bedrock layering
    if params['useDeepBedrock']:
        # Update numLay
        thisLay = numLay + 1
        numLay = thisLay

        # Create starting heads layer, copy layer above
        rasterStartHeads = read_layer(arcpy,outDir,nameOutStart,(thisLay-1),outExtRaster,'raster')
        thisOutPath = os.path.join(outDir,nameOutStart+layStrForm%(thisLay)+outExtRaster)
        arcpy.CopyRaster_management(rasterStartHeads,thisOutPath)

        # Create ibound layer, copy layer above
        rasterIbound = read_layer(arcpy,outDir,nameOutIbound,(thisLay-1),outExtRaster,'raster')
        thisOutPath = os.path.join(outDir,nameOutIbound+layStrForm%(thisLay)+outExtRaster)
        arcpy.CopyRaster_management(rasterIbound,thisOutPath)

        # Create layer bottom
        rasterBot = read_layer(arcpy,outDir,nameOutIbound,(thisLay-1),outExtRaster,'raster')
        thisOutPath = os.path.join(outDir,nameOutBot+layStrForm%(thisLay)+outExtRaster)
        rasterBotDeep = rasterBot - params['deepBedrockThick']
        rasterBotDeep.save(thisOutPath)

        # Clean up
        del rasterIbound, rasterStartHeads, rasterBot, rasterBotDeep

    #------------------------------------------------------------------------------
    # Correct IBOUND array for model layer bottoms
    #------------------------------------------------------------------------------
    # If the model cell bottom is below the starting head value in this cell, switch the
    # ibound value from -1 to 1
    for thisLay in range(0,numLay):
        # Read in the ibound raster for this layer
        rasterIbound = read_layer(arcpy,outDir,nameOutIbound,(thisLay+1),outExtRaster,'raster')

        # Read in the model bottom for this layer
        rasterBot = read_layer(arcpy,outDir,nameOutBot,(thisLay+1),outExtRaster,'raster')

        # Read in the starting heads for this layer
        rasterStart = read_layer(arcpy,outDir,nameOutStart,(thisLay+1),outExtRaster,'raster')

        # Calculate the new output
        rasterIboundFix = arcpy.sa.Con(rasterStart <= (rasterBot + params['minLaySatBelowWT']),\
            arcpy.sa.Con(rasterIbound!=0,1,0),rasterIbound)
        rasterIboundNoNull = arcpy.sa.Con(arcpy.sa.IsNull(rasterIboundFix),0,rasterIboundFix)

        # Save this output
        thisOutPath = os.path.join(outDir,nameOutIbound + layStrForm%(thisLay+1) + outExtRaster)
        # Test to see if the output already exists
        testRename = False
        if arcpy.Exists(thisOutPath):
            origPath = thisOutPath
            thisOutPath = os.path.join(outDir,nameOutIbound + layStrForm%(thisLay+1) + '_new' + outExtRaster)
            currPath = thisOutPath
            testRename = True
        
        # Now save
        rasterIboundNoNull.save(thisOutPath)
        
        # Clean up
        del rasterIbound, rasterBot, rasterStart, rasterIboundFix

        # Rename and delete old if necessary
        if testRename:
            arcpy.Delete_management(origPath)
            arcpy.Rename_management(currPath,origPath)

    #------------------------------------------------------------------------------
    # Finish up
    #------------------------------------------------------------------------------
    # Print result:
    if lhm and printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('MODFLOW layering and start heads are prepared for the <strong>%s</strong> region '%regionName))
        else:
            (thisPath,subModel) = os.path.split(pathConfig)
            regionName = os.path.split(os.path.split(thisPath)[0])[1]
            display(HTML('MODFLOW layering and start heads are prepared for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))
    elif printResult:
        display(HTML('MODFLOW layering and start heads are prepared'))

'''
--------------------------------------------------------------------------------
aquifer_properties_prep
--------------------------------------------------------------------------------
'''
def aquifer_properties_prep(inputDB, pathConfig, params, subModel=False, lhm=True, altSources=[], altConfig=[], printResult=True):
    """This function creates aquifer property input layers for MODFLOW."""

    import os, numpy
    import gis, raster

    resample_coarse = gis.resample_coarse
    create_modflow_raster = raster.create_modflow_raster
    write_raster = raster.write_raster
    read_raster = raster.read_raster

    if lhm:
        import lhm_frontend
        log_lhm_output = lhm_frontend.log_lhm_output
        
        if printResult:
            from IPython.display import display, HTML

    # Ignore numpy warnings
    numpy.seterr(divide='ignore', invalid='ignore')

    #-------------------------------------------------------------------------------
    # Specify inputs and set environment
    #-------------------------------------------------------------------------------   
    if lhm:
        (sources,inMatchExtent,outDir) = lhm_frontend.lhm_groundwater_inputs(inputDB,pathConfig,subModel)
        scratchDB = inputDB

        (srcSpecYield,srcSpecStor,srcHorizCond,srcVertAni,srcVertCond,\
            srcSpecYieldDeep,srcSpecStorDeep,srcHorizCondDeep,srcVertAniDeep,\
            nameOutSpecYield,nameOutSpecStor,nameOutHorizCond,nameOutVertAni,nameOutVertCond,\
            nameOutIbound,nameOutBot,nameOutTop,outExtRaster) = lhm_frontend.names_frontend(\
            ['srcSpecYield','srcSpecStor','srcHorizCond','srcVertAni','srcVertCond',\
            'srcSpecYieldDeep','srcSpecStorDeep','srcHorizCondDeep','srcVertAniDeep',\
            'modGndSpecYield','modGndSpecStor','modGndHorizCond','modGndVertAni','modGndVertCond',\
            'modGndIbound','modGndBot','modGndTop','outExtRaster'])

        # Additional inputs
        inputWetDepth = os.path.join(inputDB,'prep_wetland_depth')
    else:
        # Get sources and config
        sources = altSources
        inMatchExtent = altConfig['inMatchExtent']
        outDir = altConfig['outDir']
        scratchDB = altConfig['scratchDB']

        # Additional inputs
        inputWetDepth = altConfig['inputWetDepth']

        # Set names, use module standards
        (srcSpecYield,srcSpecStor,srcHorizCond,srcVertAni,srcVertCond,\
            srcSpecYieldDeep,srcSpecStorDeep,srcHorizCondDeep,srcVertAniDeep,\
            nameOutSpecYield,nameOutSpecStor,nameOutHorizCond,nameOutVertAni,nameOutVertCond,nameOutStartHeads,\
            nameOutIbound,nameOutBot,nameOutTop,outExtRaster) = names_module(\
            ['srcSpecYield','srcSpecStor','srcHorizCond','srcVertAni','srcVertCond',\
            'srcSpecYieldDeep','srcSpecStorDeep','srcHorizCondDeep','srcVertAniDeep',\
            'modGndSpecYield','modGndSpecStor','modGndHorizCond','modGndVertAni','modGndVertCond','modGndStart',\
            'modGndIbound','modGndBot','modGndTop','outExtRaster'])

    # Specify temporary output names
    tempMask = 'tempMask'
    tempWetDepth = 'tempWetDepth'

    # Specify layer format string
    layStrForm = '_lay%d'

    # Environment settings
    arcpy = lhm_frontend.set_environment(params['cellSize'], scratchDB, inMatchExtent)

    #------------------------------------------------------------------------------
    # Do the processing for the basic layers
    #------------------------------------------------------------------------------
    # Specify output names
    rastersOut = {srcSpecStor:nameOutSpecStor, srcHorizCond:nameOutHorizCond}
    if params['useSpecYield']:
        rastersOut[srcSpecYield] = nameOutSpecYield
    if params['useVertCond']:
        rastersOut[srcVertCond] = nameOutVertCond
    else:
        rastersOut[srcVertAni] = nameOutVertAni

    # Fill in the data field
    numLay = params['numLay']
    for thisRaster in rastersOut.keys():
        for thisLay in range(0,numLay):
            # Create the raster from the input sources dictionary
            thisOutPath = os.path.join(outDir,'temp_'+rastersOut[thisRaster]+layStrForm%(thisLay+1)+outExtRaster)
            result = create_modflow_raster(sources[thisRaster],thisLay+1,thisOutPath)

            # Copy if not result, this will copy the previous layer if only the first (or less than all) were specified
            if (thisLay > 0) and (not result):
                lastOutPath = os.path.join(outDir,'temp_'+rastersOut[thisRaster]+layStrForm%(thisLay)+outExtRaster)
                arcpy.Copy_management(lastOutPath,thisOutPath)

    #------------------------------------------------------------------------------
    # Add in deep bedrock
    #------------------------------------------------------------------------------
    # Handle the deep bedrock layering
    if params['useDeepBedrock']:
        # Update numLay
        thisLay = numLay + 1
        numLay = thisLay

        # Create horizontal conductivity
        thisOutPath = os.path.join(outDir,'temp_'+nameOutHorizCond+layStrForm%(thisLay)+outExtRaster)
        create_modflow_raster(sources[srcHorizCondDeep],thisLay,thisOutPath)

        # Create anisotropy
        thisOutPath = os.path.join(outDir,'temp_'+nameOutVertAni+layStrForm%(thisLay)+outExtRaster)
        create_modflow_raster(sources[srcVertAniDeep],thisLay,thisOutPath)

        # Create specific storage
        thisOutPath = os.path.join(outDir,'temp_'+nameOutSpecStor+layStrForm%(thisLay)+outExtRaster)
        create_modflow_raster(sources[srcSpecStorDeep],thisLay,thisOutPath)

        # Create specific yield
        thisOutPath = os.path.join(outDir,'temp_'+nameOutSpecYield+layStrForm%(thisLay)+outExtRaster)
        create_modflow_raster(sources[srcSpecYieldDeep],thisLay,thisOutPath)

    #------------------------------------------------------------------------------
    # Nibble the values out to the edges, fill holes
    #------------------------------------------------------------------------------
    for thisLay in range(0,numLay):
        # Read in the ibound array as a raster object
        rasterIbound = read_layer(arcpy,outDir,nameOutIbound,(thisLay+1),outExtRaster,'raster')

        for thisRaster in rastersOut.keys():
            thisInPath = os.path.join(outDir,'temp_'+rastersOut[thisRaster]+layStrForm%(thisLay+1)+outExtRaster)
            rasterThis = arcpy.Raster(thisInPath)
            thisOutName = rastersOut[thisRaster]+layStrForm%(thisLay+1)+outExtRaster
            thisOutPath = os.path.join(outDir,thisOutName)

            # Create the mask raster, if there are cells that are noData in rasterThis, they will be that in the mask too
            rasterMask = arcpy.sa.Con(rasterIbound!=0,rasterThis)
            rasterMask.save(tempMask)

            # Now, nibble
            rasterNibble = arcpy.sa.Nibble(rasterThis,rasterMask)
            rasterNibble.save(thisOutPath)

            # Clean up
            del rasterThis, rasterMask, rasterNibble
            arcpy.Delete_management(thisInPath)

            # Log this output
            if lhm and thisLay==(numLay-1):
                log_lhm_output(rastersOut[thisRaster]+'_layN'+outExtRaster,outDir,inputDB,printResult)
        
        # Clean up after this step
        del rasterIbound
        arcpy.Delete_management(tempMask)

    #------------------------------------------------------------------------------
    # Add in the depth-dependent K and Specific Yield capability
    #------------------------------------------------------------------------------
    # Read this in now, may need it here, will need it later
    arrayModelTop = read_raster(os.path.join(outDir,nameOutTop+outExtRaster),\
        noDataToValue=numpy.NaN)

    # Read in the ibound array as a raster object
    nameIbound = os.path.join(outDir,nameOutIbound+'_lay1'+outExtRaster)
    if not arcpy.Exists(nameIbound):
        nameIbound = os.path.join(outDir,nameOutIbound+outExtRaster+'/Band_1')
    if arcpy.Exists(nameIbound):
        rasterIbound = arcpy.Raster(nameIbound)
    else:
        raise ValueError('ibound array %d must exist, create it first'%nameIbound)

    if params['useDepthDepCond']:
        for thisLay in range(0,numLay):
            # Read in model layers: bottoms and Ks
            arrayLayBot = read_layer(arcpy,outDir,nameOutBot,(thisLay+1),outExtRaster,'array',numpy.NaN)

            # Calculate the depth of the layer bottom rather than the elevation
            arrayDepthBot = arrayModelTop - arrayLayBot

            # Read in the conductivity distribution for this layer
            arrayLayK = read_raster(os.path.join(outDir,nameOutHorizCond+layStrForm%(thisLay+1)+outExtRaster),\
                noDataToValue=numpy.NaN)

            # For the first layer, the top is at a depth of 0
            if thisLay==0:
                arrayDepthTop = 0

            # Now, caculate the effective K for this layer
            arrayLayKAdj = arrayLayK / (-params['depthDepCondExp'] * (arrayDepthBot - arrayDepthTop)) * \
                        (numpy.exp(-params['depthDepCondExp'] * arrayDepthBot) - \
                        numpy.exp(-params['depthDepCondExp'] * arrayDepthTop))

            # Write K back out
            thisOutPath = os.path.join(outDir,nameOutHorizCond+layStrForm%(thisLay+1)+outExtRaster)
            write_raster(arrayLayKAdj,thisOutPath,rasterIbound,numpy.NaN)
            
            # Logged already above

            # Assign the current layer bottom as the top for the next layer
            arrayDepthTop = arrayLayBot.copy()

        # Clean up this step
        del arrayLayBot, arrayDepthBot, arrayDepthTop, arrayLayK, arrayLayKAdj


    if params['useDepthDepSpecYield'] and params['useSpecYield']:
        # Calculate effective exponent for specific Yield
        expSpecYield = params['depthDepCondExp']/params['depthDepSpecYieldExp']

        for thisLay in range(0,numLay):
            # Read in model layers: bottoms and SYs
            arrayLayBot = read_layer(arcpy,outDir,nameOutBot,(thisLay+1),outExtRaster,'array',numpy.NaN)

            # Calculate the depth of the layer bottom rather than the elevation
            arrayDepthBot = arrayModelTop - arrayLayBot

            # Read in the specific yield distribution for this layer
            arrayLaySY = read_raster(os.path.join(outDir,nameOutSpecYield+layStrForm%(thisLay+1)+outExtRaster),\
                noDataToValue=numpy.NaN)

            # For the first layer, the top is at a depth of 0
            if thisLay==0:
                arrayDepthTop = 0

            # Now, calculate effective SY for this layer
            arrayLaySYAdj = arrayLaySY / (-expSpecYield * (arrayDepthBot - arrayDepthTop)) * \
                        (numpy.exp(-expSpecYield * arrayDepthBot) - numpy.exp(-expSpecYield * arrayDepthTop))

            # Write SY back out
            thisOutPath = os.path.join(outDir,nameOutSpecYield+layStrForm%(thisLay+1)+outExtRaster)
            write_raster(arrayLaySYAdj,thisOutPath,rasterIbound,numpy.NaN)

            # Assign the current layer bottom as the top for the next layer
            arrayDepthTop = arrayLayBot.copy()

        # Clean up this step
        del arrayLayBot, arrayDepthBot, arrayDepthTop, arrayLaySY, arrayLaySYAdj

    

    #------------------------------------------------------------------------------
    # Add in the lake K overprint capability
    #------------------------------------------------------------------------------
    if params['useLakeCond']:
        # Determine the output grid cellsize
        outSize = float(arcpy.env.cellSize)
        outExtent = arcpy.env.extent
        outSnapRaster = arcpy.env.snapRaster

        # Resample the wetland depth to the coarse scale
        rasterWetDepth = resample_coarse(inputWetDepth,tempWetDepth,outExtent,outSize,'mean',outSnapRaster)
        arrayWetDepth = read_raster(rasterWetDepth,noDataToValue=0)

        # Calculate wetland depth bottom elevation from model top
        arrayWetBotElev = arrayModelTop - arrayWetDepth

        # Set the layer top
        arrayPrevLayBot = arrayModelTop
  
        # For each layer, check if layer bottom > wetland bottom elevation
        for thisLay in range(0,params['numLay']):
            # Read in model layers: bottoms and Ks
            arrayLayBot = read_layer(arcpy,outDir,nameOutBot,(thisLay+1),outExtRaster,'array',numpy.NaN)

            arrayLayK = read_raster(os.path.join(outDir,nameOutHorizCond+layStrForm%(thisLay+1)+outExtRaster),\
                noDataToValue=numpy.NaN)
            arrayLaySY = read_raster(os.path.join(outDir,nameOutSpecYield+layStrForm%(thisLay+1)+outExtRaster),\
                noDataToValue=numpy.NaN)
            if params['useVertCond']:
                arrayLayV = read_raster(os.path.join(outDir,nameOutVertCond+layStrForm%(thisLay+1)+outExtRaster),\
                    noDataToValue=numpy.NaN)

            # Get the fraction of this model layer that is within the lake
            fracNotLake = (arrayWetBotElev-arrayLayBot)/(arrayPrevLayBot - arrayLayBot)
            fracNotLake[fracNotLake<0] = 0
            fracNotLake[fracNotLake>1] = 1
            fracLake = 1 - fracNotLake
            
            # Set K and Sy
            arrayLayK = arrayLayK * fracNotLake + params['lakeConductivity'] * fracLake
            if params['useSpecYield']:
                arrayLaySY = arrayLaySY * fracNotLake + fracLake #Sy is 1 in lake water
            if params['useVertCond']:
                arrayLayV = arrayLayV * fracNotLake + params['lakeConductivity'] * fracLake

            # Write K and Sy back out
            thisOutPath = os.path.join(outDir,nameOutHorizCond+layStrForm%(thisLay+1)+outExtRaster)
            write_raster(arrayLayK,thisOutPath,rasterIbound,numpy.NaN)
            if params['useSpecYield']:
                thisOutPath = os.path.join(outDir,nameOutSpecYield+layStrForm%(thisLay+1)+outExtRaster)
                write_raster(arrayLaySY,thisOutPath,rasterIbound,numpy.NaN)
            if params['useVertCond']:
                thisOutPath = os.path.join(outDir,nameOutVertCond+layStrForm%(thisLay+1)+outExtRaster)
                write_raster(arrayLayV,thisOutPath,rasterIbound,numpy.NaN)

            # Update previous layer bottom
            arrayPrevLayBot = arrayLayBot.copy()

            # Clean up this step
            del arrayLayBot, arrayLayK
            if params['useSpecYield']:
                del arrayLaySY
            if params['useVertCond']:
                del arrayLayV

        # Clean up
        del rasterWetDepth
        del arrayWetDepth, arrayWetBotElev
        arcpy.Delete_management(tempWetDepth)

    # Clean up
    del rasterIbound
    del arrayModelTop

    # Print result:
    if lhm and printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('MODFLOW aquifer properties are prepared for the <strong>%s</strong> region '%regionName))
        else:
            (thisPath,subModel) = os.path.split(pathConfig)
            regionName = os.path.split(os.path.split(thisPath)[0])[1]
            display(HTML('MODFLOW aquifer properties are prepared for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))
    elif printResult:
        display(HTML('MODFLOW aquifer properties are prepared'))

'''
--------------------------------------------------------------------------------
internal_bc_prep
--------------------------------------------------------------------------------
'''
def internal_bc_prep(inputDB, pathConfig, params, subModel=False, lhm=True, altSources=[], altConfig=[], printResult=True):
    """This function prepares drain, river and evapotranspiration inputs for MODFLOW.

    Rivers are overprinted on drains, removing the drains from those cells.

    This tool creates evaporation surfaces and et extinction depths for internally-drained areas only,
    because in LHM the output to externally drained areas is then subject to in-stream ET processes."""

    import os, numpy
    import gis, raster, feature

    resample_coarse = gis.resample_coarse
    create_modflow_raster = raster.create_modflow_raster
    write_raster = raster.write_raster
    read_raster = raster.read_raster
    intersect_grid_feature = feature.intersect_grid_feature

    if lhm:
        import lhm_frontend
        log_lhm_output = lhm_frontend.log_lhm_output
        
        if printResult:
            from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Specify inputs and set environment
    #-------------------------------------------------------------------------------
    
    if lhm:
        (sources,inMatchExtent,outDir) = lhm_frontend.lhm_groundwater_inputs(inputDB,pathConfig,subModel)
        scratchDB = inputDB

        (nameInDEMRaw,nameInDEMFill,nameInDEMUpland,nameInInternDrain,\
            nameInWetDepth,nameInStrPres,nameInWetPres,nameInStrWidth,\
            nameInHorizCond,\
            nameOutETDepth,nameOutETSurface,\
            nameOutDrnPres,nameOutDrnElev,nameOutDrnCond,\
            nameOutRivPres,nameOutRivElev,nameOutRivBot,nameOutRivCond,\
            nameOutGhbPres,nameOutGhbElev,nameOutGhbCond,nameOutChdPres,nameOutChdElev,\
            nameOutIbound,nameOutIboundFeature,nameOutBot,nameOutTop,outExtRaster,outExtTable,\
            srcEvtSurf,srcEvtExtDepth,\
            srcRivPres,srcRivElev,srcRivBedK,srcRivBedCond,srcRivBedThick,srcRivDepth,srcRivExcMask,\
            srcDrnPres,srcDrnElev,srcDrnBedK,srcDrnBedCond,srcDrnBedThick,srcDrnExcMask,\
            srcGhbPres,srcGhbElev,srcGhbCond,srcGhbExcMask,\
            srcChdPres,srcChdElev) = \
            lhm_frontend.names_frontend(['prepDEMRaw','prepDEMFill','prepDEMUp','prepInternDrain',\
            'prepWetDepth','prepStrPres','prepWetPres','prepStrWidth',\
            'modGndHorizCond',\
            'modGndETDepth','modGndETSurface',\
            'modGndDrnPresence','modGndDrnElev','modGndDrnCond',\
            'modGndRivPresence','modGndRivElev','modGndRivBot','modGndRivCond',\
            'modGndGhbPres','modGndGhbElev','modGndGhbCond','modGndChdPres','modGndChdElev',\
            'modGndIbound','modGndIboundFeature','modGndBot','modGndTop','outExtRaster','outExtTable',\
            'srcEvtSurf','srcEvtExtDepth',\
            'srcRivPres','srcRivElev','srcRivBedK','srcRivBedCond','srcRivBedThick','srcRivDepth','srcRivExcMask',\
            'srcDrnPres','srcDrnElev','srcDrnBedK','srcDrnBedCond','srcDrnBedThick','srcDrnExcMask',\
            'srcGhbPres','srcGhbElev','srcGhbCond','srcGhbExcMask',\
            'srcChdPres','srcChdElev'])

        # Additional inputs
        inputDEM = os.path.join(inputDB,nameInDEMRaw)
        inputDEMFill = os.path.join(inputDB,nameInDEMFill)
        inputDEMUpland = os.path.join(inputDB,nameInDEMUpland)
        inputIntDrain = os.path.join(inputDB,nameInInternDrain)
        inputWetDepth = os.path.join(inputDB,nameInWetDepth)
        inputStrPres = os.path.join(inputDB,nameInStrPres)
        inputStrWidth = os.path.join(inputDB,nameInStrWidth)
        inputWetPres = os.path.join(inputDB,nameInWetPres)
    else:
        raise ValueError('Need to fix this')
        # Get sources and config
        sources = altSources
        inMatchExtent = altConfig['inMatchExtent']
        outDir = altConfig['outDir']
        scratchDB = altConfig['scratchDB']

        # Additional inputs
        inputDEM = altConfig['inputDEM']
        inputDEMFill = altConfig['inputDEMFill']
        inputDEMUpland = altConfig['inputDEMUpland']
        inputIntDrain = altConfig['inputIntDrain']
        inputWetDepth = altConfig['inputWetDepth']

        # Set names, use module standards
        (nameOutETDepth,nameOutETSurface,nameOutDrnArea,\
            nameOutDrnElev,nameOutDrnCond,\
            nameOutDrnPres,\
            nameOutRivElev,nameOutRivCond,nameOutRivBot,nameOutRivPres,\
            nameOutIbound,nameOutIboundFeature,nameOutBot,outExtRaster,\
            srcEvtExtDepth,srcRivElev,srcRivDepth,srcDrnBedK,srcDrnBedThick,\
            srcRivBedK,srcRivBedThick,srcRivExcMask,srcDrnExcMask) = \
            lhm_frontend.names_module(['modGndETDepth','modGndETSurface','modGndDrnArea',\
            'modGndDrnElev','modGndDrnCond',\
            'modGndDrnPresence',\
            'modGndRivElev','modGndRivCond','modGndRivBot','modGndRivPresence',\
            'modGndIbound','modGndIboundFeature','modGndBot','outExtRaster',\
            'srcEvtExtDepth','srcRivElev','srcRivDepth','srcDrnBedK','srcDrnBedThick',\
            'srcRivBedK','srcRivBedThick','srcRivExcMask','srcDrnExcMask'])

    # Specify temporary output names
    tempCoarseDEM = 'temp_coarse_dem'
    tempCoarseDEMFill = 'temp_coarse_dem_fill'
    tempCoarseDEMUpland = 'temp_coarse_dem_upland'
    tempCoarseIntDrain = 'temp_coarse_int_drain'
    tempCoarseWetDepth = 'temp_coarse_wet_depth'
    tempStrArea = 'temp_str_area'
    tempWetArea = 'temp_wet_area'
    tempDrnArea = 'temp_drn_area'
    tempDrnPres = 'temp_drn_pres'
    tempDrnPresRaster = 'temp_drn_pres_raster'
    tempDrnElev = 'temp_drn_elev'
    tempDrnCond = 'temp_drn_cond'
    tempDrnThick = 'temp_drn_thick'
    tempDrnExclude = 'temp_drn_exclude'
    tempRivPres = 'temp_river_pres'
    tempRivElev = 'temp_river_elev'
    tempRivPresRaster = 'temp_river_pres_raster'
    tempRivDepth = 'temp_river_depth'
    tempRivArea = 'temp_river_area'
    tempRivCond = 'temp_riv_cond'
    tempRivThick = 'temp_riv_thick'
    tempRivExclude = 'temp_riv_exclude'
    tempGhbPres = 'temp_ghb_pres'
    tempGhbPresRaster = 'temp_ghb_pres_raster'
    tempGhbElev = 'temp_ghb_elev'
    tempGhbCond = 'temp_ghb_cond'
    tempGhbArea = 'temp_ghb_area'
    tempGhbExclude = 'temp_ghb_exclude'

    tempKernel = 'temp_seep_kernel'
    tempFocalMin = 'temp_focal_min'

    # Specify drain set format string
    setStrForm = '_set%d'
    layStrForm = '_lay%d'

    # Environment settings
    arcpy = lhm_frontend.set_environment(params['cellSize'], scratchDB, inMatchExtent)

    # Update numLay for deep bedrock, if needed
    if params['useDeepBedrock']:
        numLay = params['numLay'] + 1
    else:
        numLay = params['numLay']

    #------------------------------------------------------------------------------
    # Resolve some options -- these can be pre-specified in prepare_model
    #------------------------------------------------------------------------------
    if 'evtSurfFeature' not in params.keys():
        if srcEvtSurf not in sources.keys():
            params['evtSurfFeature'] = False
        else:
            params['evtSurfFeature'] = True

    if 'drnElevFeature' not in params.keys():
        if srcDrnElev not in sources.keys():
            params['drnElevFeature'] = False
        else:
            params['drnElevFeature'] = True

    if 'drnBedCond' not in params.keys():
        if srcDrnBedCond not in sources.keys():
            params['drnBedCond'] = False
        else:
            params['drnBedCond'] = True

    if 'rivBedCond' not in params.keys():
        if srcDrnBedCond not in sources.keys():
            params['rivBedCond'] = False
        else:
            params['rivBedCond'] = True

    for bndCond in ['drn','riv','ghb']:
        if bndCond+'SpecCond' not in sources.keys():
            params[bndCond+'SpecCond'] = True

    #------------------------------------------------------------------------------
    # Internal helper functions
    #------------------------------------------------------------------------------
    def source_field_specified(sources,source,field):
        # The field might be absent, it might be empty, or it might be set as None
        sourceSpec = False
        if source in sources.keys():
            if field in sources[source].keys():
                if (sources[source][field] != '') and (sources[source][field] is not None):
                    sourceSpec = True
        return sourceSpec
    
    
    def read_table(tableDict, params):
        """Reads time-series MODFLOW specification tables, and interpolates if needed
        """

        import pandas, numpy

        # Specify field names to keep
        keepFields = ['presence_id','time_start','value']

        #-----------------------------------------------------------------------------------------------------
        # Helper functions
        #-----------------------------------------------------------------------------------------------------
        def table_field_specified(tableDict,field):
            # The field might be absent, it might be empty, or it might be set as None
            tableSpec = False
            if field in tableDict.keys():
                if (tableDict[field] != '') and (tableDict[field] is not None):
                    if isinstance(tableDict[field],dict):
                        if len(tableDict[field].keys)>0:
                            tableSpec = True
                    else:
                        tableSpec = True
            return tableSpec
    
        def field_mapping_date(dfIn,tableDict,fieldName):
            if table_field_specified(tableDict,fieldName):
                if isinstance(tableDict[fieldName],dict):
                    origField = tableDict[fieldName]['field']
                    format = tableDict[fieldName]['format']
                    # Rename the field and convert to datetime type            
                    dfIn[fieldName] = pandas.to_datetime(dfIn[origField],format=format)
                else:
                    raise TypeError('Field %s must be a dictionary'%fieldName)
            
            return dfIn

        def field_mapping_num(dfIn,tableDict,fieldName):
            if table_field_specified(tableDict,fieldName):
                if isinstance(tableDict[fieldName],dict):
                    origField = tableDict[fieldName]['field']
                    if origField in dfIn.columns:
                        if 'conv' in tableDict[fieldName].keys():
                            if tableDict[fieldName]['conv'] is not None:
                                convFactor = tableDict[fieldName]['conv']
                                dfIn[fieldName] = dfIn[origField] * convFactor
                                dfIn = dfIn.drop(columns=[origField])
                            else:
                                dfIn = dfIn.rename(columns={origField:fieldName}) 
                        else:
                            dfIn = dfIn.rename(columns={origField:fieldName})
                    else:
                        raise ValueError('Field %s not found in tableDict'%origField) 
                elif isinstance(tableDict[fieldName],str):
                    dfIn = dfIn.rename(columns={origField:fieldName})
                else:
                    raise TypeError('Field %s must be a dictionary or string'%fieldName)
            else:
                raise ValueError('Field %s not found in tableDict'%fieldName)

            return dfIn

        #-----------------------------------------------------------------------------------------------------
        # Processing
        #-----------------------------------------------------------------------------------------------------
        # Read the input table
        dfTable = pandas.read_csv(tableDict['path'])

        # Now, do the fields mapping
        dfTable = dfTable.rename(columns={tableDict['pres_id']:'presence_id'})
        dfTable['presence_id'] = dfTable['presence_id'].astype(int)
        dfTable = field_mapping_date(dfTable,tableDict,'time_start')
        dfTable = field_mapping_num(dfTable,tableDict,'value')

        # Handle the interpolation
        if table_field_specified(tableDict,'interp'):
            # Check for timeSeriesDateRange in params
            if 'timeSeriesDateRange' in params.keys():
                # Upsample frequency of data, interpolating linearly, between `time_start` values in our dataframe
                # Only interpolate the 'value' field, the 'pressence_id' field should be constant between time_start values
                dfTable = dfTable.set_index('time_start').groupby('presence_id').resample('D').interpolate(method='linear').reset_index()
     
            else:
                raise ValueError('timeSeriesDateRange must be specified in params if interpolation is used')

        # Finally, keep only our desired fields
        origCols = dfTable.columns
        [dfTable.drop(inplace=True,columns=thisCol) for thisCol in origCols if thisCol not in keepFields]

        return dfTable
    
    #------------------------------------------------------------------------------
    # Resample grids to model resolution, and intersect with model grid
    #------------------------------------------------------------------------------
    # Read in the ibound array as a raster object
    rasterIbound = read_layer(arcpy,outDir,nameOutIbound,1,outExtRaster,'raster')

    # Used by all three packages
    arrayLayBot = read_layer(arcpy,outDir,nameOutBot,1,outExtRaster,'array',numpy.NaN)
    arrayTop = read_raster(os.path.join(outDir,nameOutTop+outExtRaster),noDataToValue=numpy.NaN)

    # Determine the output grid cellsize
    outSize = float(arcpy.env.cellSize)
    outExtent = arcpy.env.extent
    outSnapRaster = arcpy.env.snapRaster

    if params['useEVT']:
        if not params['evtSurfFeature']:
            # compute from the minimum-upscaled DEM surface and the wetland depths
            # Read in the wetland depth
            rasterWetDepth = resample_coarse(inputWetDepth,tempCoarseWetDepth,outExtent,outSize,'mean',outSnapRaster)
            arrayWetDepth = read_raster(rasterWetDepth,noDataToValue=numpy.NaN)
        
    # Pre-processing for drains
    if params['useDRN']:
        # DRN area, only for automated drain presence/absence
        for lay in range(1,numLay+1):
            if source_field_specified(sources,srcDrnPres,'method') and (lay==1):
                # First, calculate stream area at the fine scale
                rasterStrPres = arcpy.Raster(inputStrPres)
                rasterStrWidth = arcpy.Raster(inputStrWidth)
                rasterStrArea = rasterStrPres * rasterStrWidth * rasterStrWidth.meanCellWidth #assume stream length in cell is 1

                # Resample to coarse 
                rasterStrAreaCoarse = resample_coarse(rasterStrArea,tempStrArea,outExtent,outSize,'sum',outSnapRaster)
                arrayStrArea = read_raster(rasterStrAreaCoarse,noDataToValue=numpy.NaN)

                # For wetlands, assume if they are present, then they cover the entire cell
                rasterWetPres = arcpy.Raster(inputWetPres)
                rasterWetArea = rasterWetPres * rasterWetPres.meanCellWidth**2

                # Resample to coarse
                rasterWetAreaCoarse = resample_coarse(rasterWetArea,tempWetArea,outExtent,outSize,'sum',outSnapRaster)
                arrayWetArea = read_raster(rasterWetAreaCoarse,noDataToValue=numpy.NaN)

                # Add together, limit to coarse cell area
                arrayDrnArea = numpy.minimum((arrayStrArea + arrayWetArea),outSize**2)

                # Write to temporary raster
                write_raster(arrayDrnArea, tempDrnArea+layStrForm%lay, rasterIbound, numpy.NaN)

                del rasterWetPres, rasterWetArea, rasterWetAreaCoarse, rasterStrPres, rasterStrWidth, rasterStrArea, rasterStrAreaCoarse
                del arrayStrArea, arrayWetArea, arrayDrnArea

            elif (sources[srcDrnPres]['method'] == 'feature') and params['drnSpecCond']:
                # if conductance is specified directly, and you have drain presence features, you don't need area
                # Create a temporary drain presence grid for this layer
                create_modflow_raster(sources[srcDrnPres], lay, tempDrnPresRaster,outputFeature=tempDrnPres)

                # Intersect drains feature with the model grid feature, export drain area (temporary raster)
                intersect_grid_feature(os.path.join(outDir,nameOutIboundFeature),tempDrnPres,addAttribs=[],\
                    rasterAttrib='Shape_area',rasterPath=tempDrnArea+layStrForm%lay,templateRaster=rasterIbound)

                # Clean up
                arcpy.Delete_management([tempDrnPres,tempDrnPresRaster])

            elif params['drnSpecCond']:
                # drain presence/absence is a grid, cannot compute area effectively without additional information
                raise ValueError('Drain presence/absence must be a feature if drain specific conductance is used.')

            else:
                # Set areas to zero for this layer
                arrayDrnArea = numpy.zeros(arrayTop.shape,dtype=numpy.float32)

                # Write to temporary raster
                write_raster(arrayDrnArea, tempDrnArea+layStrForm%lay, rasterIbound, numpy.NaN)

                # Clean up
                del arrayDrnArea 

        # DEM
        if (not params['drnElevFeature']) or params['useSurfaceDRN'] or (params['useEVT'] and not params['evtSurfFeature']):
            # DEM
            rasterDEM = resample_coarse(inputDEM,tempCoarseDEM,outExtent,outSize,'minimum',outSnapRaster)
            arrayDEM = read_raster(rasterDEM,noDataToValue=numpy.NaN)

            # Filled DEM
            rasterDEMFill = resample_coarse(inputDEMFill,tempCoarseDEMFill,outExtent,outSize,'minimum',outSnapRaster)
            arrayDEMFill = read_raster(rasterDEMFill,noDataToValue=numpy.NaN)

            # Clean up
            del rasterDEM, rasterDEMFill

        # Internally-drained area
        if not params['drnElevFeature']:
            # Internally drained area
            rasterIntDrain = resample_coarse(inputIntDrain,tempCoarseIntDrain,outExtent,outSize,'majority',outSnapRaster)
            arrayIntDrain = read_raster(rasterIntDrain,noDataToValue=0)
            
            # Clean up
            del rasterIntDrain

        # DEM for surface drains
        if params['useSurfaceDRN']:
            # Upland DEM
            rasterDEMUpland = resample_coarse(inputDEMUpland,tempCoarseDEMUpland,outExtent,outSize,'mean',outSnapRaster)
            arrayDEMUplandRaw = read_raster(rasterDEMUpland,noDataToValue=-999)

            # Limit to filled DEM elevation
            arrayDEMUplandFill = numpy.where(arrayDEMUplandRaw<arrayDEMFill,arrayDEMFill,arrayDEMUplandRaw)

            # Limit to model top
            arrayDEMUpland = numpy.where(arrayDEMUplandFill<arrayTop,arrayTop,arrayDEMUplandFill)

            # Clean up
            del rasterDEMUpland
            del arrayDEMUplandRaw, arrayDEMUplandFill

    if params['useRIV']:
        if params['rivSpecCond']:
            # if conductance is specified directly, you don't need area
            # Create a temporary drain presence grid for this layer
            create_modflow_raster(sources[srcRivPres], lay, tempRivPresRaster, outputFeature=tempRivPres)

            # Intersect drains feature with the model grid feature, export drain area (temporary raster)
            intersect_grid_feature(os.path.join(outDir,nameOutIboundFeature),tempRivPres,addAttribs=[],\
                rasterAttrib='Shape_area',rasterPath=tempRivArea+layStrForm%lay,templateRaster=rasterIbound)

            # Clean up
            arcpy.Delete_management([tempRivPresRaster,tempRivPres])

        else:
            # Set areas to zero for this layer
            arrayRivArea = numpy.zeros(arrayTop.shape,dtype=numpy.float32)

            # Write to temporary raster
            write_raster(arrayRivArea, tempRivArea+layStrForm%lay, rasterIbound, numpy.NaN)

            # Clean up
            del arrayRivArea 

    if params['useGHB']:
        if params['ghbSpecCond']:
            # if conductance is specified directly, you don't need area
            # Create a temporary drain presence grid for this layer
            create_modflow_raster(sources[srcGhbPres], lay, tempGhbPresRaster, outputFeature=tempGhbPres)

            # Intersect drains feature with the model grid feature, export drain area (temporary raster)
            intersect_grid_feature(os.path.join(outDir,nameOutIboundFeature),tempGhbPres,addAttribs=[],\
                rasterAttrib='Shape_area',rasterPath=tempGhbArea+layStrForm%lay,templateRaster=rasterIbound)

            # Clean up
            arcpy.Delete_management([tempGhbPresRaster,tempGhbPres])

        else:
            # Set areas to zero for this layer
            arrayGhbArea = numpy.zeros(arrayTop.shape,dtype=numpy.float32)

            # Write to temporary raster
            write_raster(arrayGhbArea, tempGhbArea+layStrForm%lay, rasterIbound, numpy.NaN)

            # Clean up
            del arrayGhbArea

    #------------------------------------------------------------------------------
    # Calculate evapotranspiration package surfaces
    #------------------------------------------------------------------------------
    if params['useEVT']:
        # Create the et surface grid
        if params['evtSurfFeature']:
            # Write the the feature directly to the output
            create_modflow_raster(sources[srcEvtExtDepth], range(1,numLay+1), os.path.join(outDir,nameOutETSurface+outExtRaster))
        else: 
            # Calculate the et surface
            arrayETSurface = arrayDEM - arrayWetDepth

            # If null values, replace with surface
            arrayETSurfClean = numpy.where(numpy.isnan(arrayETSurface),arrayDEM,arrayETSurface)

            # Constrain this to be above the 1st layer bottom <--I don't think I actually want this
            #arrayETSurfConstrain = numpy.where(arrayETSurfClean >= (arrayLayBot+params['drnLayBotDiff']),\
            #                                arrayETSurfClean,(arrayLayBot+params['drnLayBotDiff']))
            
            # Write it out
            write_raster(arrayETSurfClean, os.path.join(outDir,nameOutETSurface+outExtRaster), rasterIbound, numpy.NaN)

            # Clean up
            del rasterWetDepth
            del arrayWetDepth, arrayETSurface, arrayETSurfClean
            #del arrayETSurfConstrain
            arcpy.Delete_management(tempCoarseWetDepth)

            if not params['useDRN']:
                del rasterDEM, rasterDEMFill

        # Log this output
        if lhm:
            log_lhm_output(nameOutETSurface+outExtRaster,outDir,inputDB,printResult)

        # Create the et extinction depth grid
        create_modflow_raster(sources[srcEvtExtDepth], 1, os.path.join(outDir,nameOutETDepth+outExtRaster))
        if lhm:
            log_lhm_output(nameOutETDepth+outExtRaster,outDir,inputDB,printResult)       


    #------------------------------------------------------------------------------
    # Calculate DRN, RIV, GHB, and CHD presence/exclusion
    #------------------------------------------------------------------------------
    # Do CHD first, to make sure we have all specified head cells included
    if params['useCHD']:
        # Create the chd presence layer
        create_modflow_raster(sources[srcChdPres], range(1,numLay+1), os.path.join(outDir,nameOutChdPres+outExtRaster))

        # Log this output
        if lhm:
            log_lhm_output(nameOutChdPres+outExtRaster,outDir,inputDB,printResult)

        # Check for table inputs
        if source_field_specified(sources,srcChdPres,'table'):
            # Read the table
            dfTable = read_table(sources[srcChdPres], params)

            # Assure that the 'value' field is integer
            dfTable['value'] = dfTable['value'].astype(int)

            # Write this out
            dfTable.to_csv(os.path.join(outDir,nameOutChdPres+outExtTable),index=False)

            if lhm:
                log_lhm_output(nameOutChdPres+outExtTable,outDir,inputDB,printResult)
            

    # Initialize the drainSet number
    drnSet = 0

    # Now, for each layer
    for lay in range(1,numLay+1):
         # Bring in the CHD presence layer, if present
        if params['useCHD']:
            rasterCHDPres = arcpy.Raster(os.path.join(outDir,nameOutChdPres+layStrForm%lay+outExtRaster))
        else:
            rasterCHDPres = arcpy.sa.CreateConstantRaster(0, "INTEGER", outExtent, outSize)
        
        # Bring in the ibound for this layer
        rasterIboundLay = read_layer(arcpy,outDir,nameOutIbound,lay,outExtRaster,'raster')
        rasterConstHead = arcpy.sa.Con(rasterIboundLay == -1, 1, 0)

        # Overlay them
        rasterIboundCHD = rasterConstHead + rasterCHDPres
        
        # Also, bring the ibound lay in for convenience if needed below
        arrayIboundLay = read_raster(rasterIboundLay,noDataToValue=0)

        # Expand this raster by the specified number of cells
        rasterIboundExpand = arcpy.sa.Expand(rasterIboundCHD,1,params['specHeadBuffCells'])
        arrayIboundExpand = read_raster(rasterIboundExpand,noDataToValue=0)

        # Clean up
        del rasterIboundLay, rasterConstHead, rasterIboundCHD, rasterIboundExpand

        ## Now, add in the exclusion mask, if specified
        if params['useRIV']:
            # Check to see if using the expanded ibound exclude or not
            if params['rivBuffSpecHead']:
                arrayIboundTemp = arrayIboundExpand
            else:
                arrayIboundTemp = arrayIboundLay

            # Make the exclusion layer, if specified
            if source_field_specified(sources,srcRivExcMask,'method'):
                create_modflow_raster(sources[srcRivExcMask],lay,tempRivExclude)
                arrayRivExcludeTemp = read_raster(tempRivExclude,noDataToValue=0)

                # Composite with the ibound
                arrayRivExclude = arrayIboundTemp + arrayRivExcludeTemp

                # Clean up
                arcpy.Delete_management(tempRivExclude)
                del arrayRivExclTemp
            else:
                arrayRivExclude = arrayIboundTemp.copy()
        
            # Clean up
            del arrayIboundTemp

        if params['useDRN']:
            # Check to see if using the expanded ibound exclude or not
            if params['drnBuffSpecHead']:
                arrayIboundTemp = arrayIboundExpand
            else:
                arrayIboundTemp = arrayIboundLay

            # Make the exclusion layer, if specified
            if source_field_specified(sources,srcDrnExcMask,'method'):
                create_modflow_raster(sources[srcDrnExcMask],lay,tempDrnExclude)
                arrayDrnExcludeTemp = read_raster(tempDrnExclude,noDataToValue=0)

                # Composite with the ibound
                arrayDrnExclude = arrayIboundLay + arrayDrnExcludeTemp

                # Clean up
                arcpy.Delete_management(tempDrnExclude)
                del arrayDrnExclTemp
            else:
                arrayDrnExclude = arrayIboundLay.copy()

            # Clean up
            del arrayIboundTemp

        if params['useGHB']:
            # Check to see if using the expanded ibound exclude or not
            if params['ghbBuffSpecHead']:
                arrayIboundTemp = arrayIboundExpand
            else:
                arrayIboundTemp = arrayIboundLay

            # Make the exclusion layer, if specified
            if source_field_specified(sources,srcGhbExcMask,'method'):
                create_modflow_raster(sources[srcGhbExcMask],lay,tempGhbExclude)
                arrayGhbExcludeTemp = read_raster(tempGhbExclude,noDataToValue=0)

                # Composite with the ibound
                arrayGhbExclude = arrayIboundLay + arrayGhbExcludeTemp

                # Clean up
                arcpy.Delete_management(tempGhbExclude)
                del arrayGhbExclTemp
            else:
                arrayGhbExclude = arrayIboundLay.copy()

            # Clean up
            del arrayIboundTemp


        ## Finally, create the presence/absence ([postive integer]/0) grid for each layer, and the area
        if params['useRIV']:
            # Create the raw riv presence layer
            create_modflow_raster(sources[srcRivPres], lay, tempRivPres)
            arrayRivPresRaw = read_raster(tempRivPres,noDataToValue=0)

            # Overlay with the exclusion layer
            arrayRivPres = numpy.where(arrayRivExclude == 1, 0, arrayRivPresRaw)

            # Write out the riv presence layer
            write_raster(arrayRivPres,os.path.join(outDir,nameOutRivPres+layStrForm%lay+outExtRaster),rasterIbound)

            # Log if lhm and last layer
            if lhm and (lay == numLay):
                log_lhm_output(nameOutRivPres+outExtRaster,outDir,inputDB,printResult)

            # Check for table inputs, only on last layer
            if source_field_specified(sources,srcRivPres,'table') and (lay == numLay):
                # Read the table
                dfTable = read_table(sources[srcRivPres], params)

                # Assure that the 'value' field is integer
                dfTable['value'] = dfTable['value'].astype(int)

                # Write this out
                dfTable.to_csv(os.path.join(outDir,nameOutRivPres+outExtTable),index=False)

                if lhm:
                    log_lhm_output(nameOutRivPres+outExtTable,outDir,inputDB,printResult)

            # Clean up
            del arrayRivPresRaw, arrayRivPres
            arcpy.Delete_management(tempRivPres)

        if params['useDRN']:
            # Increment the drnSet, basically stream and lake hydrology drains are always set 1, if present
            if lay==1: 
                drnSet += 1

            # Create the raw drn presence layer, either specified by a feature, or computed right from LHM
            if source_field_specified(sources,srcDrnPres,'method'):
                delTempDrnPres = True #flag for cleanup
                create_modflow_raster(sources[srcDrnPres], lay, tempDrnPres)
                arrayDrnPresRaw = read_raster(tempDrnPres,noDataToValue=0)
            elif (lay==1) and lhm: # rely on LHM for raw presence/absence for stream network drains, only the first cell
                # Read in the raw drn area layer
                arrayDrnArea = read_raster(tempDrnArea+layStrForm%lay,noDataToValue=0)

                # Calculate presence/absence from drain area
                arrayDrnPresRaw = numpy.where(arrayDrnArea > 0, 1, 0)
                
                # Clean up
                del arrayDrnArea
            elif lhm:
                arrayDrnPresRaw = numpy.zeros(arrayIboundLay.shape,dtype=numpy.int32)

            # Overlay with the exclusion layer
            arrayDrnPres = numpy.where(arrayDrnExclude == 1, 0, arrayDrnPresRaw)

            # Write out the drn presence layer
            write_raster(arrayDrnPres,os.path.join(outDir,nameOutDrnPres+setStrForm%drnSet+layStrForm%lay+outExtRaster),rasterIbound)

            # Log if lhm and last layer
            if lhm and (lay == numLay):
                log_lhm_output(nameOutDrnPres+outExtRaster,outDir,inputDB,printResult)

            # Check for table inputs, only on last layer
            if source_field_specified(sources,srcDrnPres,'table') and (lay == numLay):
                # Read the table
                dfTable = read_table(sources[srcDrnPres], params)

                # Assure that the 'value' field is integer
                dfTable['value'] = dfTable['value'].astype(int)

                # Write this out
                dfTable.to_csv(os.path.join(outDir,nameOutDrnPres+outExtTable),index=False)

                if lhm:
                    log_lhm_output(nameOutDrnPres+outExtTable,outDir,inputDB,printResult)

            # Save the drain exclusion layer and ibound arrays if needed for other drain types
            if params['useSurfaceDRN'] or params['useTileDRN'] or params['useCanalDRN'] or params['useSeepDRN']:
                write_raster(arrayDrnExclude,tempDrnExclude+layStrForm%lay,rasterIbound)
                delDrnExclude = True
            else:
                delDrnExclude = False

            # Clean up
            del arrayDrnPresRaw, arrayDrnPres
            if delTempDrnPres:
                arcpy.Delete_management(tempDrnPres)
        
        if params['useGHB']:
            # Create the raw ghb presence layer
            create_modflow_raster(sources[srcGhbPres], lay, tempGhbPres)
            arrayGhbPresRaw = read_raster(tempGhbPres,noDataToValue=0)

            # Overlay with the exclusion layer
            arrayGhbPres = numpy.where(arrayGhbExclude == 1, 0, arrayGhbPresRaw)

            # Write out the ghb presence layer
            write_raster(arrayGhbPres,os.path.join(outDir,nameOutGhbPres+layStrForm%lay+outExtRaster),rasterIbound)

            # Log if lhm and last layer
            if lhm and (lay == numLay):
                log_lhm_output(nameOutGhbPres+outExtRaster,outDir,inputDB,printResult)

            # Check for table inputs, only on last layer
            if source_field_specified(sources,srcGhbPres,'table') and (lay == numLay):
                # Read the table
                dfTable = read_table(sources[srcGhbPres], params)

                # Assure that the 'value' field is integer
                dfTable['value'] = dfTable['value'].astype(int)

                # Write this out
                dfTable.to_csv(os.path.join(outDir,nameOutGhbPres+outExtTable),index=False)

                if lhm:
                    log_lhm_output(nameOutGhbPres+outExtTable,outDir,inputDB,printResult)

            # Clean up
            del arrayGhbPresRaw, arrayGhbPres
            arcpy.Delete_management(tempGhbPres)

        ## Clean up
        del arrayIboundExpand, arrayIboundLay
        if params['useRiv']:
            del arrayRivExclude
        # Don't clear out the drn exclude, I need it later
        if params['useGhb']:
            del arrayGhbExclude


    #------------------------------------------------------------------------------
    # Calculate DRN, RIV, GHB, and CHD elevation surfaces (and bottom for RIV)
    #------------------------------------------------------------------------------
    # First rivers
    if params['useRIV']:
        for lay in range(1,numLay+1):
            # Bring in the presence/absence layer
            arrayRivPres = read_raster(os.path.join(outDir,nameOutRivPres+layStrForm%lay+outExtRaster),noDataToValue=0)

            # Convert elevation to raster, matching ibound, enforcing environment
            create_modflow_raster(sources[srcRivElev],lay,tempRivElev)
            arrayRivElev = read_raster(tempRivElev,noDataToValue=numpy.NaN)

            # Mask the elevation with the presence/absence layer, write it out
            arrayRivElevPres = numpy.where(arrayRivPres == 1, arrayRivElev, numpy.NaN)
            write_raster(arrayRivElevPres, os.path.join(outDir,nameOutRivElev+layStrForm%lay+outExtRaster), rasterIbound, numpy.NaN)

            # Use the river bottom depth to calculate the area of the river feature in the cell
            create_modflow_raster(sources[srcRivDepth],1,tempRivDepth)
            arrayRivDepth = read_raster(tempRivDepth,noDataToValue=numpy.NaN)
    
            # Calculate the river bottom elevation
            arrayRivBot = arrayRivElev - arrayRivDepth
            
            # Mask the bottom with the presence/absence layer, write it out
            arrayRivBotPres = numpy.where(arrayRivPres == 1, arrayRivBot, numpy.NaN)
            write_raster(arrayRivBot, os.path.join(outDir,nameOutRivBot+layStrForm%lay+outExtRaster), rasterIbound, numpy.NaN)

            # Clean up after this step
            del arrayRivPres, arrayRivElev, arrayRivElevPres, arrayRivDepth, arrayRivBot, arrayRivBotPres
            arcpy.Delete_management([tempRivElev,tempRivDepth])
            
        if lhm:
            log_lhm_output(nameOutRivElev+outExtRaster,outDir,inputDB,printResult)
            log_lhm_output(nameOutRivBot+outExtRaster,outDir,inputDB,printResult)
        
        # Check for table inputs for elevation
        if source_field_specified(sources,srcRivElev,'table'):
            # Read the table
            dfTable = read_table(sources[srcRivElev], params)

            # Write this out
            dfTable.to_csv(os.path.join(outDir,nameOutRivElev+outExtTable),index=False)

            if lhm:
                log_lhm_output(nameOutRivElev+outExtTable,outDir,inputDB,printResult)


    # Now drains
    if params['useDRN']:        
        for lay in range(1,numLay+1):
            # Bring in the presence/absence layer
            arrayDrnPres = read_raster(os.path.join(outDir,nameOutDrnPres+layStrForm%lay+outExtRaster),noDataToValue=0)

            if params['drnElevFeature']:
                #Convert elevation to raster, matching ibound, enforcing environment
                create_modflow_raster(sources[srcDrnElev],lay,tempDrnElev)
                arrayDrnElev = read_raster(tempDrnElev,noDataToValue=numpy.NaN)

                # Mask the elevation with the presence/absence layer, write it out
                arrayDrnElevPres = numpy.where(arrayDrnPres == 1, arrayDrnElev, numpy.NaN)
                write_raster(arrayDrnElevPres, os.path.join(outDir,nameOutDrnElev+setStrForm%drnSet+layStrForm%lay+outExtRaster), rasterIbound, numpy.NaN)

                # Clean up
                del arrayDrnElev, arrayDrnElevPres
                arcpy.Delete_management(tempDrnElev)
            if lay == 1:
                # internally-drained areas use the filled elvations, externally-drained areas use the DEM minimum
                arrayDrnElevInit = numpy.where(numpy.isnan(arrayIntDrain), arrayDEM,\
                                            numpy.where(arrayIntDrain==1,arrayDEMFill,arrayDEM))

                # Mask for presence/absence
                arrayDrnElevPres = numpy.where(arrayDrnPres == 1, arrayDrnElevInit, numpy.NaN)

                # Set the minimum drain elevation
                if params['drnMinElev']:
                    arrayDrnElevMin = numpy.where(arrayDrnElevPres > params['drnMinElev'], arrayDrnElevPres,\
                                                params['drnMinElev'])
                else:
                    arrayDrnElevMin = arrayDrnElevPres.copy()

                # Assure that all drain elevations are above the first layer bottom
                if params['drnLayBotDiff']:
                    arrayDrnElev = numpy.where(arrayDrnElevMin >= (arrayLayBot+params['drnLayBotDiff']),\
                                                arrayDrnElevMin, arrayLayBot+params['drnLayBotDiff'])
                else:
                    arrayDrnElev = arrayDrnElevMin.copy()

                del arrayDrnElev, arrayDrnElevInit, arrayDrnElevPres, arrayDrnElevMin, arrayIntDrain, \
                    arrayDEM, arrayDEMFill
                arcpy.Delete_management([tempCoarseDEM, tempCoarseDEMFill, tempCoarseIntDrain])
            else:
                arrayDrnElev = numpy.zeros_like(arrayTop) * numpy.NaN

            # Write it out
            write_raster(arrayDrnElev, os.path.join(outDir,nameOutDrnElev+setStrForm%drnSet+layStrForm%lay+outExtRaster), rasterIbound, numpy.NaN)

            # Clean up
            del arrayDrnElev, arrayDrnPres

        # Log this
        if lhm:
            log_lhm_output(nameOutDrnElev+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)

        # Check for table inputs for elevation
        if source_field_specified(sources,srcDrnElev,'table'):
            # Read the table
            dfTable = read_table(sources[srcDrnElev], params)

            # Write this out
            dfTable.to_csv(os.path.join(outDir,nameOutDrnElev+setStrForm%drnSet+outExtTable),index=False)

            if lhm:
                log_lhm_output(nameOutDrnElev+setStrForm%drnSet+outExtTable,outDir,inputDB,printResult)


    # Now GHB
    if params['useGHB']:
        for lay in range(1,numLay+1):
            # Bring in the presence/absence layer
            arrayGhbPres = read_raster(os.path.join(outDir,nameOutGhbPres+layStrForm%lay+outExtRaster),noDataToValue=0)

            #Convert elevation to raster, matching ibound, enforcing environment
            create_modflow_raster(sources[srcGhbElev],lay,tempGhbElev)
            arrayGhbElev = read_raster(tempGhbElev,noDataToValue=numpy.NaN)

            # Mask the elevation with the presence/absence layer, write it out
            arrayGhbElevPres = numpy.where(arrayGhbPres == 1, arrayGhbElev, numpy.NaN)
            write_raster(arrayGhbElevPres, os.path.join(outDir,nameOutGhbElev+layStrForm%lay+outExtRaster), rasterIbound, numpy.NaN)

            # Clean up
            del arrayGhbPres, arrayGhbElev, arrayGhbElevPres
            arcpy.Delete_management(tempGhbElev)

        # Write out and log, if LHM
        if lhm:
            # Log this output
            log_lhm_output(nameOutGhbElev+outExtRaster,outDir,inputDB,printResult)

        # Check for table inputs for elevation
        if source_field_specified(sources,srcGhbElev,'table'):
            # Read the table
            dfTable = read_table(sources[srcGhbElev], params)

            # Write this out
            dfTable.to_csv(os.path.join(outDir,nameOutGhbElev+outExtTable),index=False)

            if lhm:
                log_lhm_output(nameOutGhbElev+outExtTable,outDir,inputDB,printResult)


    if params['useCHD']:
        # Convert elevation to raster, matching ibound, enforcing environment
        create_modflow_raster(sources[srcChdElev],range(1,numLay+1),os.path.join(outDir,nameOutChdElev+outExtRaster))

        # Write out and log, if LHM
        if lhm:
            # Log this output
            log_lhm_output(nameOutChdElev+outExtRaster,outDir,inputDB,printResult)
        
        # Check for table inputs for elevation
        if source_field_specified(sources,srcChdElev,'table'):
            # Read the table
            dfTable = read_table(sources[srcChdElev], params)

            # Write this out
            dfTable.to_csv(os.path.join(outDir,nameOutChdElev+outExtTable),index=False)

            if lhm:
                log_lhm_output(nameOutChdElev+outExtTable,outDir,inputDB,printResult)

    #------------------------------------------------------------------------------
    # Calculate DRN, RIV, and GHB conductance
    #------------------------------------------------------------------------------
    # Note, these are all identical, except drains use the set and lay strForm and
    # GHB doesn't have a conductivity specification

    if params['useRIV']:
        for lay in range(1,numLay+1):
            # Bring in the presence/absence layer
            arrayRivPres = read_raster(os.path.join(outDir,nameOutRivPres+layStrForm%lay+outExtRaster),noDataToValue=0)

            # First, see if conductance is directly specified
            if params['rivBedCond']:
                # Create the conductance/specific condcutance array
                create_modflow_raster(sources[srcRivBedCond],lay,tempRivCond)

                # If using specific conductance, multiply by aread to get conductance
                if params['rivSpecCond']:
                    # Get the area
                    arrayRivArea = read_raster(tempRivArea+layStrForm%lay,noDataToValue=0)

                    # Get the specific conductance
                    arrayRivSpecCond = read_raster(tempRivCond,noDataToValue=0)

                    # Compute the conductance
                    arrayRivCond = arrayRivSpecCond * arrayRivArea

                    # Clean up
                    del arrayRivSpecCond, arrayRivArea
                    arcpy.Delete_management([tempRivArea+layStrForm%lay])
                else:
                    # Read it conductance directly
                    arrayRivCond = read_raster(tempRivCond)
            else:            
                # Get the conductivity
                create_modflow_raster(sources[srcRivBedK],lay,tempRivCond)
                arrayRivConductivity = read_raster(tempRivCond,noDataToValue=0)
                
                # Get the area
                arrayRivArea = read_raster(tempRivArea+layStrForm%lay,noDataToValue=0)

                # Now the thickness
                create_modflow_raster(sources[srcRivBedThick],lay,tempRivThick)
                arrayRivThick = read_raster(tempRivThick,noDataToValue=0)

                # Calculate the conductance
                arrayRivCond = arrayRivConductivity * arrayRivArea / arrayRivThick
                arrayRivCond = numpy.nan_to_num(arrayRivCond)
                
                # Clean up
                del arrayRivConductivity, arrayRivThick, arrayRivArea
                arcpy.Delete_management([tempRivThick,tempRivArea+layStrForm%lay])

            # Mask with presence
            arrayRivCond = numpy.where(arrayRivPres >= 1, arrayRivCond, numpy.NaN)

            # Write it out
            write_raster(arrayRivCond, os.path.join(outDir,nameOutRivCond+layStrForm%lay+outExtRaster), \
                rasterIbound, numpy.NaN)
            
            # Clean up
            del arrayRivCond, arrayRivPres
            arcpy.Delete_management(tempRivCond)
                
        # Log this
        if lhm:
            log_lhm_output(nameOutRivCond+outExtRaster,outDir,inputDB,printResult)

            

    if params['useDRN']:
        for lay in range(1,numLay+1):
            # Bring in the presence/absence layer
            arrayDrnPres = read_raster(os.path.join(outDir,nameOutDrnPres+layStrForm%lay+outExtRaster),noDataToValue=0)

            if params['drnBedCond']:
                # Create the conductance/specific condcutance array
                create_modflow_raster(sources[srcRivBedCond],lay,tempDrnCond)

                # If using specific conductance, multiply by aread to get conductance
                if params['drnSpecCond']:
                    # Get the area
                    arrayDrnArea = read_raster(tempDrnArea+layStrForm%lay,noDataToValue=0)

                    # Get the specific conductance
                    arrayDrnSpecCond = read_raster(tempDrnCond,noDataToValue=0)

                    # Compute the conductance
                    arrayDrnCond = arrayDrnSpecCond * arrayDrnArea

                    # Clean up
                    del arrayDrnSpecCond, arrayDrnArea
                    arcpy.Delete_management([tempDrnArea+layStrForm%lay])
                else:
                    # Read it conductance directly
                    arrayDrnCond = read_raster(tempDrnCond)
            else:
                # Get the conductivity
                create_modflow_raster(sources[srcDrnBedK],lay,tempDrnCond)
                arrayDrnConductivity = read_raster(tempDrnCond,noDataToValue=0)
                
                # Get the area
                arrayDrnArea = read_raster(tempDrnArea+layStrForm%lay,noDataToValue=0)

                # Now the thickness
                create_modflow_raster(sources[srcDrnBedThick],lay,tempDrnThick)
                arrayDrnThick = read_raster(tempDrnThick,noDataToValue=0)

                # Calculate the conductance
                arrayDrnCond = arrayDrnConductivity * arrayDrnArea / arrayDrnThick
                arrayDrnCond = numpy.nan_to_num(arrayDrnCond)
                
                # Clean up
                del arrayDrnConductivity, arrayDrnThick, arrayDrnArea
                arcpy.Delete_management([tempDrnThick,tempDrnArea+layStrForm%lay])

            # Mask with presence
            arrayDrnCond = numpy.where(arrayDrnPres >= 1, arrayDrnCond, numpy.NaN)

            # Write it out
            write_raster(arrayDrnCond, os.path.join(outDir,nameOutDrnCond+setStrForm%drnSet+layStrForm%lay+outExtRaster), \
                rasterIbound, numpy.NaN)
            
            # Clean up
            del arrayDrnCond, arrayDrnPres
            arcpy.Delete_management(tempDrnCond)
                
        # Log this
        if lhm:
            log_lhm_output(nameOutDrnCond+outExtRaster,outDir,inputDB,printResult)

           
    if params['useGHB']:
        for lay in range(1,numLay+1):
            # Bring in the presence/absence layer
            arrayGhbPres = read_raster(os.path.join(outDir,nameOutGhbPres+layStrForm%lay+outExtRaster),noDataToValue=0)

            # Create the conductance/specific condcutance array
            create_modflow_raster(sources[srcGhbCond],lay,tempGhbCond)

            # If using specific conductance, multiply by aread to get conductance
            if params['ghbSpecCond']:
                # Get the area
                arrayGhbArea = read_raster(tempGhbArea+layStrForm%lay,noDataToValue=0)

                # Get the specific conductance
                arrayGhbSpecCond = read_raster(tempGhbCond,noDataToValue=0)

                # Compute the conductance
                arrayGhbCond = arrayGhbSpecCond * arrayGhbArea

                # Clean up
                del arrayGhbSpecCond, arrayGhbArea
                arcpy.Delete_management([tempGhbArea+layStrForm%lay])
            else:
                # Read it conductance directly
                arrayGhbCond = read_raster(tempGhbCond)

            # Mask with presence
            arrayGhbCond = numpy.where(arrayGhbPres >= 1, arrayGhbCond, numpy.NaN)

            # Write it out
            write_raster(arrayGhbCond, os.path.join(outDir,nameOutGhbCond+layStrForm%lay+outExtRaster), \
                rasterIbound, numpy.NaN)
            
            # Clean up
            del arrayGhbCond, arrayGhbPres
            arcpy.Delete_management(tempGhbCond)
                
        # Log this
        if lhm:
            log_lhm_output(nameOutGhbCond+outExtRaster,outDir,inputDB,printResult)

    #------------------------------------------------------------------------------
    # Create surface drains -- drains set at the surface elevation
    #------------------------------------------------------------------------------
    if params['useSurfaceDRN']:
        drnSet += 1

        for lay in range(1,numLay+1):
            if lay == 1:
                # Read in the drain exclusion mask for this layer
                arrayDrnExclude = read_raster(tempDrnExclude+layStrForm%lay)

                # Create surface drain presence/absence, excluding the exclude area
                arraySurfPres = numpy.where(arrayDrnExclude!=1,1,0)

                # Now, save the drain presence/absence grid
                write_raster(arraySurfPres, os.path.join(outDir,nameOutDrnPres+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, -99)

                # Set the surface drains elevation as the upland elevation of each cell, excluding the exclude area
                arraySurfElev = numpy.where(arraySurfPres==1,arrayDEMUpland,numpy.NaN)
                write_raster(arraySurfElev, os.path.join(outDir,nameOutDrnElev+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)
                
                # Set the conductance equal to the cell area (thus Q=delta_H*A), only for ibound=1 cells
                cellArea = rasterIbound.meanCellWidth * rasterIbound.meanCellHeight
                arrayArea = arraySurfPres * cellArea
                arrayArea[arrayArea==0] = numpy.NaN # set to NaN for conductance

                # Write out the conductance
                write_raster(arrayArea, os.path.join(outDir,nameOutDrnCond+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)
                
                # Clean up
                del arrayDrnExclude, arraySurfPres, arraySurfElev, arrayArea
            else:
                # Define a zeros grid for floating point and integer
                arrayZerosInt = numpy.zeros(arrayTop.shape,dtype=numpy.uint32)
                arrayZerosFloat = numpy.zeros(arrayTop.shape,dtype=arrayTop.dtype) * numpy.NaN

                # Write each of the presence/absence, elevation, and conductance arrays
                write_raster(arrayZerosInt, os.path.join(outDir,nameOutDrnPres+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, -99)
                write_raster(arrayZerosFloat, os.path.join(outDir,nameOutDrnElev+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)
                write_raster(arrayZerosFloat, os.path.join(outDir,nameOutDrnCond+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)

                # Clean up
                del arrayZerosInt, arrayZerosFloat
        
        # Log these outputs
        if lhm:
            log_lhm_output(nameOutDrnPres+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)
            log_lhm_output(nameOutDrnElev+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)
            log_lhm_output(nameOutDrnCond+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)


    #------------------------------------------------------------------------------
    # Create lateral seepage faces -- lateral seepage faces
    #------------------------------------------------------------------------------
    if params['useSeepDRN']:
        drnSet += 1

        # Write a temporary kernel file for the calculation
        kernelTxt = '3 3\n0 1 0\n1 0 1\n0 1 0'
        kernelFile = os.path.join(outDir,tempKernel+'.txt')
        with open(kernelFile,'w') as f:
            f.write(kernelTxt)

        # For each cell, compute the lowest elevation of the top of the surrounding four cells
        rasterTop = arcpy.Raster(os.path.join(outDir,nameOutTop+outExtRaster))
        arcpy.sa.FocalStatistics(rasterTop, kernelFile, "MINIMUM", "DATA").save(tempFocalMin)
        arrayFocalMin = read_raster(tempFocalMin,noDataToValue=numpy.nan)

        # Loop through layers, writing zeros for things before the first seepage layer
        for lay in range(1,numLay+1):
            if lay < params['seepFirstLay']:
                # Define a zeros grid for floating point and integer
                arrayZerosInt = numpy.zeros(arrayTop.shape,dtype=numpy.uint32)
                arrayZerosFloat = numpy.zeros(arrayTop.shape,dtype=arrayTop.dtype) * numpy.NaN

                # Write each of the presence/absence, elevation, and conductance arrays
                write_raster(arrayZerosInt, os.path.join(outDir,nameOutDrnPres+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, -99)
                write_raster(arrayZerosFloat, os.path.join(outDir,nameOutDrnElev+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)
                write_raster(arrayZerosFloat, os.path.join(outDir,nameOutDrnCond+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)

                # Clean up
                del arrayZerosInt, arrayZerosFloat
            else:
                # Read in the drain exclusion mask for this layer
                arrayDrnExclude = read_raster(tempDrnExclude+layStrForm%lay)

                # Read in the layer top as a numpy array
                if lay == 1:
                    arrayLayTop = arrayTop
                else:
                    # Read in the bottom of the previous layer for the top of this layer
                    arrayLayTop = read_layer(arcpy,outDir,nameOutBot,lay-1,outExtRaster,'array',noDataToValue=numpy.nan)
                    arrayLayBot = read_layer(arcpy,outDir,nameOutBot,lay,outExtRaster,'array',noDataToValue=numpy.nan)

                # Create seepage face presence/absence, excluding the exclude area, then write it out
                arrayThisLay = numpy.logical_and(arrayFocalMin<=arrayLayTop,arrayFocalMin>arrayLayBot)
                arraySeepPres = numpy.where(numpy.logical_and(arrayThisLay,arrayDrnExclude!=1),1,0)
                write_raster(arraySeepPres, os.path.join(outDir,nameOutDrnPres+layStrForm%lay+setStrForm%drnSet+outExtRaster), rasterIbound, -99)

                # Determine the seepage face elevation, then write
                arraySeepElev = numpy.where(arraySeepPres==1, arrayFocalMin, numpy.NaN)
                write_raster(arraySeepElev, os.path.join(outDir,nameOutDrnElev+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)

                # Calculate seeage face conductance, then write
                # Fill the seep conductivity
                arrayHK = read_layer(arcpy,outDir,nameInHorizCond,lay,outExtRaster,'array',noDataToValue=numpy.nan)
                arraySeepHK = numpy.where(arraySeepPres, arrayHK,0)

                # Determine the seepage face conductance. This conductance formula was calculated using an assumption
                # of a Dupuit-Forcheimer steady-state unsaturated zone water table profile that reaches the top of the model
                # at one edge. See Heerspink et al. (2024) for the derivation.
                arraySeepCond = 3/4 * arraySeepHK * (arrayTop - arraySeepElev)
                arraySeepCondPres = numpy.where(arraySeepPres==1, arraySeepCond,numpy.NaN)
                write_raster(arraySeepCondPres, os.path.join(outDir,nameOutDrnCond+setStrForm%drnSet+outExtRaster), rasterIbound, numpy.NaN)

                # Clean up
                del arrayLayTop, arrayLayBot, arrayThisLay, arraySeepPres, arraySeepElev, \
                    arraySeepCond, arraySeepCondPres, arraySeepHK, arrayHK, arrayDrnExclude

        # Log these outputs
        if lhm:
            log_lhm_output(nameOutDrnPres+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)
            log_lhm_output(nameOutDrnElev+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)
            log_lhm_output(nameOutDrnCond+setStrForm%drnSet+outExtRaster,outDir,inputDB,printResult)

        # Clean up after this step
        del arrayFocalMin
        del rasterTop
        os.remove(kernelFile)

    #------------------------------------------------------------------------------
    # Create canal drains -- drains dug for artificial drainage
    #------------------------------------------------------------------------------
    if params['useCanalDRN']:
        drnSet += 1
        pass

    #------------------------------------------------------------------------------
    # Create tile drains -- agricultural tile drainage
    #------------------------------------------------------------------------------
    if params['useTileDRN']:
        drnSet += 1
        pass
    
    # Clean up after all drains
    del arrayLayBot, arrayTop 
    if delDrnExclude:
        for lay in range(1,numLay+1):
            arcpy.Delete_management(tempDrnExclude+layStrForm%lay)
    
    # Clean up the riv, drn, and ghb areas, if needed

    #------------------------------------------------------------------------------
    # Finalize
    #------------------------------------------------------------------------------
    # Clean up after everything
    del rasterIbound

    # Print result:
    if lhm and printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('MODFLOW internal boundary conditions are prepared for the <strong>%s</strong> region '%regionName))
        else:
            (thisPath,subModel) = os.path.split(pathConfig)
            regionName = os.path.split(os.path.split(thisPath)[0])[1]
            display(HTML('MODFLOW internal boundary conditions are prepared for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))
    elif printResult:
        display(HTML('MODFLOW internal boundary conditions are prepared'))



def collapse_modflow_layers(inputDB, pathConfig, params, subModel=False, lhm=True, altSources=[], altConfig=[], printResult=True):
    """This function is a thin wrapper around the raster.collapse_single_layer_rasters
    that facilitates doing this as a last step of MODFLOW raster preparation"""

    import os
    import lhm_frontend
    from raster import collapse_single_layer_rasters
    if lhm:
        from IPython.display import display, HTML

    # Get the inputs
    if lhm:
        (sources,inMatchExtent,outDir) = lhm_frontend.lhm_groundwater_inputs(inputDB,pathConfig,subModel)
    else:
        outDir = altConfig['outDir']

    # Run the function
    collapse_single_layer_rasters(outDir,layPattern='_lay')
    collapse_single_layer_rasters(outDir,layPattern='_set')

    # Write the result
    # Print result:
    if lhm and printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('MODFLOW multi-layer rasters are prepared for the <strong>%s</strong> region '%regionName))
        else:
            (thisPath,subModel) = os.path.split(pathConfig)
            regionName = os.path.split(os.path.split(thisPath)[0])[1]
            display(HTML('MODFLOW multi-layer rasters are prepared for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))
    elif printResult:
        display(HTML('MODFLOW multi-layer rasters are prepared'))
