'''This module contains all of the functions needed to produce inputs for the unsaturated
zone module in LHM

For each function, call as:
    throughflow_prep(inputDB, pathConfig, params, subModel=False),
either omitting the subModel=False, or specifying the subModel name as a string if using.

You will also need LHM's helper modules "raster" and "gis" in your path.
'''

import lhm_frontend
import os

def throughflow_routing(inputDB, pathConfig, params=dict(), subModel=False, printResult=True):
    """This script creates all grids necessary for LHM runoff calculations.

    Params are:
        slopeMinVal: minimum value of slope used for runoff calculations (m/m), default is 0.0001
        roughMinVal: minimum roughness value allowed, default is 0.025
        maxProcesses: the maximum number of processes to use for tiled flowlength calculations, the best value is an
            integer divisor of your number of tiles, default is 3
        tileBufferCells: the number of cells to buffer the individual flowaccumualtion tiles, default is 100
    """
    


    # Import modules
    import compression
    from raster import create_surface_raster
    from gis import tile_flowlength
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Script defaults, shouldn't need to change these
    #-------------------------------------------------------------------------------
    (prefixMannings,dirLanduse,dirRoughness,
        nameFlowdir,nameFlowslope,nameRadius,nameStreams,nameSinuosity,\
        nameSpecYield,nameHorizCond,nameGauges,outSubFlowtime,rowFlowaccTiles,\
        outExtension) = \
        lhm_frontend.names_frontend(['prefixMannings','dirLanduse','dirRoughness',\
        'prepDEMFillDir','prepDEMFillSlope','prepStrRadius','prepStrPres','prepStrSin',\
        'modGndSpecYield','modGndHorizCond','prepRunGauges','prepRunSubTime','srcFlowaccBasins',\
        'outExtRaster'])

    # Get the standard input locations
    (surfSources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)
    outDirGnd = lhm_frontend.lhm_groundwater_inputs(inputDB,pathConfig,subModel)[2]

    # Get the full paths for these
    inputBase = inputDB + '/%s'
    inFlowdir = inputBase%nameFlowdir
    inFlowslope = inputBase%nameFlowslope
    inRadius = inputBase%nameRadius
    inStreams = inputBase%nameStreams
    inSinuosity = inputBase%nameSinuosity
    inGauges = inputBase%nameGauges

    # Parse the input params structure
    paramDefaults = {'slopeMinVal':0.0001, 'roughMinVal':0.025, 'maxProcesses':3, 'tileBufferCells':100}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Specify temporary names
    tempRoughRescale = 'temp_roughscale'
    tempHKRescale = 'temp_hkscale'
    tempSpecYieldRescale = 'temp_specyieldscale'
    tempFlowtimeStr = 'temp_flowtime_str'
    tempFlowtimeSub ='temp_flowtime_subsurface'
    tempFocalMax = 'temp_focal_max'
    tempFlowaccTiles = 'temp_flowacc_tiles'
    tempTilesClip = 'temp_tiles_clip'
    tempFlowdirGauge = 'temp_flowdir_gauge'
    tempFlowdirStr = 'temp_flowdir_str'
    tempFlowdirStrGauge = 'temp_flowdir_str_gauge'
    tempCost = 'temp_cost'
    tempStrCost = 'temp_stream_cost'
    tempSubCost = 'temp_subsurface_cost'

    # Make a list of all temporary grids so that I can delete all at once, below
    tempGrids = [tempRoughRescale,tempHKRescale,tempSpecYieldRescale,tempFlowtimeStr,tempFlowtimeSub,\
        tempFocalMax,tempFlowaccTiles,tempTilesClip,tempFlowdirGauge,tempFlowdirStr,tempFlowdirStrGauge,\
        tempCost,tempStrCost,tempSubCost]

    #-------------------------------------------------------------------------------
    # Set up the geoprocessing environment and get layer names
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment(inFlowdir,inputDB,inputBdry)

    # Get the cellSize
    cellSize = float(arcpy.env.cellSize)

    # Full path to roughness grid
    inDirRoughness = os.path.join(os.path.split(inputDB)[0], dirLanduse+'/'+dirRoughness)

    #-------------------------------------------------------------------------------
    # Resample HK and porosity grids to the same resolution as the surface model
    # import necessary raster objects, refine roughness
    #-------------------------------------------------------------------------------
    arcpy.Resample_management(os.path.join(outDirGnd,nameHorizCond+outExtension), tempHKRescale, cellSize, "NEAREST")
    arcpy.Resample_management(os.path.join(outDirGnd,nameSpecYield+outExtension), tempSpecYieldRescale, cellSize, "NEAREST")
    rasterHK = arcpy.Raster(tempHKRescale)
    rasterPorosity = arcpy.Raster(tempSpecYieldRescale)

    # Import these rasters
    rasterRadius = arcpy.Raster(inRadius)
    rasterSinuosity = arcpy.Raster(inSinuosity)
    rasterStreams = arcpy.Raster(inStreams)
    rasterGauge = arcpy.Raster(inGauges)

    # Get the roughness grid, this can be any of the previously-created ones, because the 
    # stream portion of those roughness grids is constant
    arcpy.env.workspace = inDirRoughness
    roughnessGrids = arcpy.ListRasters(wild_card = prefixMannings+'*')
    arcpy.env.workspace = inputDB
    roughness = os.path.join(inDirRoughness,roughnessGrids[0])
    
    # Rescale roughness grid to match environment resolution
    arcpy.Resample_management(roughness,tempRoughRescale,cellSize,'BILINEAR')

    # Make sure there are no zero roughness values
    rasterRough = arcpy.Raster(tempRoughRescale)
    rasterRoughMin = arcpy.sa.Con(rasterRough>=params['roughMinVal'], rasterRough, params['roughMinVal'])

    # Make sure that there are no zero slope values, and convert to m/m slope
    rasterSlope = arcpy.Raster(inFlowslope)
    rasterSlopeMin = arcpy.sa.Con(rasterSlope/100<params['slopeMinVal'], params['slopeMinVal'], rasterSlope/100)

    # Read in the flowaccumulation tiles grid
    create_surface_raster(surfSources[rowFlowaccTiles],tempFlowaccTiles)

    # Clip to the surface boundary
    rasterTiles = arcpy.sa.ExtractByMask(tempFlowaccTiles,inputBdry)

    # Convert to integer
    rasterTilesInt = arcpy.sa.Int(rasterTiles)
    rasterTilesInt.save(tempTilesClip)

    # Build the raster attribute table for the tiles
    arcpy.BuildRasterAttributeTable_management(tempTilesClip)

    #------------------------------------------------------------------------------
    # Prepare flowdirection grids needed
    #------------------------------------------------------------------------------
    # Prepare flowdirection raster with gauges, calculate flowtimes
    # Imprint the gauge values on the flowdirection raster as NoData
    rasterFlowdirGauge = arcpy.sa.Con(arcpy.sa.IsNull(rasterGauge),inFlowdir)
    rasterFlowdirGauge.save(tempFlowdirGauge)

    # Here, set all cells except streams to noData
    rasterFlowdirStrGauge = arcpy.sa.Con(rasterStreams==1,rasterFlowdirGauge)
    rasterFlowdirStrGauge.save(tempFlowdirStrGauge)

    # Imprint streams as no data on the flowdir array
    rasterFlowdirStr = arcpy.sa.Con(rasterStreams==0,inFlowdir)
    rasterFlowdirStr.save(tempFlowdirStr)

    del rasterFlowdirGauge
    arcpy.Delete_management([tempFlowdirGauge])
    
    #------------------------------------------------------------------------------
    # Prepare flowtimes
    #------------------------------------------------------------------------------
    # First -----------------------------------------------------------------------
    # calculate flowtime in streams only 
    # Calculate the flow cost in streams (Manning's Equation)
    # Units are s/m
    rasterStrCost = rasterRoughMin * (rasterRadius**-0.6666667) * (rasterSlopeMin**-0.5) * rasterSinuosity
    rasterStrCost.save(tempStrCost)

    # Get the in-stream cost only, 0 elsewhere
    rasterCost = arcpy.sa.Con(rasterStreams==0,0,rasterStrCost)
    rasterCost.save(tempCost)

    # Calculate the flowtime from stream cells to the downstream gauge
    # Only calculate this once, doesn't change with roughness grids
    rasterFlowtimeStrOnly = tile_flowlength(arcpy,params,rasterFlowdirStrGauge,rasterTilesInt,rasterCost)
    rasterFlowtimeStrOnly.save(tempFlowtimeStr)

    # Clean up raster objects
    del rasterRough, rasterRoughMin, rasterRadius, rasterSinuosity, rasterStrCost, rasterCost

    # Second ----------------------------------------------------------------------
    # get the flowtime to the downstream gauge from each gauge location 
    # Will ultimately overwrite flowtimes at gauges with the maximum flowtime around it, assuming this represents
    # flowtime to the downstream gauge
    # Assign 0 values to nulls
    rasterFlowTimeStr01 = arcpy.sa.Con(arcpy.sa.IsNull(rasterFlowtimeStrOnly),0,rasterFlowtimeStrOnly)

    # # Relating gauges to next downstream gauge
    # Calculate focalmax on the streams to pick flowtime to downstream gauge
    rasterFocalMax = arcpy.sa.FocalStatistics(rasterFlowTimeStr01,'RECTANGLE 3 3 CELL','MAXIMUM','DATA')
    rasterFocalMax.save(tempFocalMax)

    # Clean up
    del rasterFlowTimeStr01

    # Third ----------------------------------------------------------------------
    # calculate upland subsurface flowtime to streams 
    # Calculate the subsurface interflow cost (cost = 1/velcoity,
    # velocity = surface gradient times hydraulic conductivity, divided by porosity)
    # Units are s/m
    # Use the .bands()[0] to handle cases where we've already stacked the single-band rasters for the 
    # groundwater module
    rasterSubCost =  rasterPorosity.bands()[0] / (rasterSlopeMin * rasterHK.bands()[0])
    rasterSubCost.save(tempSubCost)

    # Calculate the total flowtime to the downstream gauge
    rasterFlowtimeSubOnly = tile_flowlength(arcpy,params,rasterFlowdirStr,rasterTilesInt,rasterSubCost)
    rasterFlowtimeSubOnly.save(tempFlowtimeSub)

    # Clean up
    del rasterSubCost

    # Fourth ----------------------------------------------------------------------
    # composite upland subsurface + stream, add in focalmax for gauges 
    # Add the focalmax for each gauge to the flowtime grid
    rasterFlowtime = arcpy.sa.Con(rasterStreams==1,rasterFlowtimeStrOnly,rasterFlowtimeSubOnly)
    rasterFlowtimeGauge = arcpy.sa.Con(arcpy.sa.IsNull(rasterGauge),rasterFlowtime,\
        arcpy.sa.Con(rasterGauge>0,rasterFocalMax,rasterFlowtime))

    # Now, convert to a 32 bit integer to reduce the size of this output
    arcpy.CopyRaster_management(rasterFlowtimeGauge,outSubFlowtime,pixel_type='32_BIT_UNSIGNED')

    # Finally, compress the raster
    compression.compress_file_raster(outSubFlowtime)

    # Log this output
    lhm_frontend.log_lhm_output(outSubFlowtime,inputDB,inputDB,printResult)

    # Clean up
    del rasterFlowtimeSubOnly, rasterFocalMax, rasterFlowtimeStrOnly, rasterStreams, \
        rasterFlowtime, rasterFlowtimeGauge

    # Clean up
    [arcpy.Delete_management(tempGrid) for tempGrid in tempGrids]

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Throughflow routing flowtimes are calculated for the <strong>%s</strong> region'%regionName))