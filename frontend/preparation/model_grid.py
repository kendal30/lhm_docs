'''
--------------------------------------------------------------------------------
Create model grid
--------------------------------------------------------------------------------
'''
def create_model_grid(inputDB, pathConfig, config, subModel=False, printResult=True):
    # This script creates model-resolution dependent static inputs.

    # Import system modules
    import os, pandas, numpy
    import arcpy
    import gis, lhm_frontend

    raster_vals_to_points = gis.raster_vals_to_points

    if printResult:
        from IPython.display import display, HTML

    #------------------------------------------------------------------------------
    # Specify inputs
    #------------------------------------------------------------------------------
    # Get the model grid
    if subModel:
        strReqLay = 'dirSubmodelLayers'
    else:
        strReqLay = 'dirModelLayers'
        
    (dirLayers,dirSurface,nameModelGrid,nameLatGrid,nameLonGrid,nameBdrySurf,outExtension) = lhm_frontend.names_frontend(\
        [strReqLay,'dirSurface','modSurfGrid','modSurfLatGrid','modSurfLonGrid','prepBdry','outExtRaster'])

    # Define the location of the model grid and boundary files
    if not subModel:
        subDirSurface = os.path.split(inputDB)[0]+'/%s/%s'%(dirLayers,dirSurface)
        inBoundary = nameBdrySurf
    else:
        # This is the case where we are using a submodel
        subDirSurface = os.path.split(inputDB)[0]+('/%s/'+subModel+'/%s')%(dirLayers,dirSurface)
        inBoundary = nameBdrySurf+'_'+subModel

    # Fields
    fieldActive = 'ibound'
    fieldLon = 'longitude'
    fieldLat = 'latitude'

    # Names
    modelGrid = nameModelGrid + outExtension
    modelResLat = nameLatGrid + outExtension
    modelResLon = nameLonGrid + outExtension
    nameOutFishnet = nameModelGrid + '.shp'

    # Temporary -- working in a directory
    tempPoint = 'temp_point.shp'
    tempFeature = 'temp_feature.shp'
    tempProject = 'temp_project.shp'
    tempProjectBack = 'temp_project_back.shp'
    tempGrid = 'temp_grid.tif'

    # Temporary -- working in a DB
    tempFishnet = 'tempFishnet'
    tempFishnetPts = 'tempFishnet_label' # this is generated automatically by the CreateFishnet_management routine
    tempAttribs = 'tempFishnet_attribs'

    #------------------------------------------------------------------------------
    # Set up the environment
    #------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment('',subDirSurface,'')

    #------------------------------------------------------------------------------
    # Do the steps
    #------------------------------------------------------------------------------
    # 1: Import or Create the model grid if necessary
    if config['method'] == 'feature':
        if (config['data'] is None) or (config['data'] == ''):
            # Assign the standard LHM boundary here
            config['data'] = os.path.join(inputDB,inBoundary)

        # Set the environment settings
        arcpy.env.cellSize = config['fieldVal']
        arcpy.env.outputCoordinateSystem = config['data']
        arcpy.env.extent = config['data']

        # Now reassign fieldVal to work for the ibound value
        config['fieldVal'] = fieldActive # reassign this

        # Copy the feature and add a conversion field
        arcpy.CopyFeatures_management(config['data'],tempFeature)

        # Add the ibound field and calculate its value
        arcpy.AddField_management(tempFeature,config['fieldVal'],'SHORT')
        arcpy.CalculateField_management(tempFeature,config['fieldVal'],1)

        # Create the temporary raster
        arcpy.FeatureToRaster_conversion(tempFeature,config['fieldVal'],modelGrid,arcpy.env.cellSize)

        # Finish the environment settings
        arcpy.env.snapRaster = modelGrid

        # Clean up this step
        arcpy.Delete_management(tempFeature)

    elif config['method'] == 'grid':
        # Set the environment settings
        arcpy.env.cellSize = config['data']
        arcpy.env.outputCoordinateSystem = config['data']
        arcpy.env.extent = config['data']
        arcpy.env.snapRaster = config['data']

        # Import model grid into model layers folder
        arcpy.CopyRaster_management(config['data'],os.path.join(subDirSurface,modelGrid))

    # 2: Create Lat/Lon grids for climate preparation scripts
    # Execute RasterToPoint
    arcpy.RasterToPoint_conversion(modelGrid, tempPoint, 'VALUE')

    # NEXT: Assign each point a Latitude and Longitude.
    #    --> This requires a Geographic coordinate system to be set.
    # Set environment settings
    sr = arcpy.SpatialReference("WGS 1984")

    # Project into the correct spatial reference
    arcpy.Project_management(tempPoint, tempProject, sr)

    # Switch coordinate systems for the lat/lon grids and add XY
    arcpy.env.outputCoordinateSystem = sr
    arcpy.AddXY_management(tempProject)

    # Project it back to the correct spatial reference
    arcpy.env.outputCoordinateSystem = modelGrid
    arcpy.env.extent = modelGrid
    arcpy.env.snapRaster = modelGrid
    arcpy.Project_management(tempProject,tempProjectBack,arcpy.env.outputCoordinateSystem)

    # LAST: Re-assign each point back to 2 individual Rasters, 1 for Latitude and 1 for Longitude.
    #    --> Use a snap raster to make sure the rasters created match exactly with your original model grid
    # Execute PointToRaster
    arcpy.PointToRaster_conversion(tempProjectBack, "POINT_X", tempGrid)
    rasterOutApplyLon = arcpy.sa.ApplyEnvironment(tempGrid)
    rasterOutApplyLon.save(modelResLon)

    arcpy.PointToRaster_conversion(tempProjectBack, "POINT_Y", tempGrid)
    rasterOutApplyLat = arcpy.sa.ApplyEnvironment(tempGrid)
    rasterOutApplyLat.save(modelResLat)

    # Clean up
    del rasterOutApplyLat,rasterOutApplyLon
    [arcpy.Delete_management(thisTemp) for thisTemp in [tempPoint, tempProject, tempProjectBack, tempGrid]]

    #------------------------------------------------------------------------------
    # Create a 2-D model grid in shapefile format
    #------------------------------------------------------------------------------
    # Set the workspace to our scratchDB for the steps below
    arcpy.env.workspace = inputDB
    scratchDB = inputDB

    # Get the properties of the first ibound layer grid for making the shapefile below
    inDescribe = arcpy.Describe(os.path.join(subDirSurface,modelGrid))

    # Create a fishnet at the required resolution
    # note that there is a discrepancy in the documentation (arcmap 10.7 and arcgis pro 1.4) but
    # the code below is correct, and width and height are switched
    arcpy.CreateFishnet_management(tempFishnet,str(inDescribe.extent.XMin)+' '+str(inDescribe.extent.YMin),\
        str(inDescribe.extent.XMin)+' '+str(inDescribe.extent.YMin+1000),\
        inDescribe.meanCellWidth,inDescribe.meanCellHeight,inDescribe.height,inDescribe.width,\
        str(inDescribe.extent.XMax)+' '+str(inDescribe.extent.YMax),'LABELS','','POLYGON')

    # Add a cellid field
    arcpy.AddField_management(tempFishnet,'cellid','long')
    arcpy.CalculateField_management(tempFishnet,'cellid','!OID!')

    # Extract the model grid values for the fishnet centroid points
    raster_vals_to_points(tempFishnetPts,os.path.join(subDirSurface,modelGrid),fieldActive)
    raster_vals_to_points(tempFishnetPts,os.path.join(subDirSurface,modelResLon),fieldLon)
    raster_vals_to_points(tempFishnetPts,os.path.join(subDirSurface,modelResLat),fieldLat)

    # Add x and y coordinates to the fishnet label points
    arcpy.AddField_management(tempFishnetPts,'X','float')
    arcpy.AddField_management(tempFishnetPts,'Y','float')
    arcpy.CalculateField_management(tempFishnetPts,'X','!Shape.firstPoint.X!')
    arcpy.CalculateField_management(tempFishnetPts,'Y','!Shape.firstPoint.Y!')

    # Bring in the attribute table of the points so that we can calculate i,j
    dfPts = pandas.DataFrame(arcpy.da.TableToNumPyArray(tempFishnetPts,['OID@',fieldActive,fieldLon,fieldLat,'X','Y'],null_value=-9999))
    dfPts = dfPts.rename(columns={'OID@':'cellid'})

    # Replace the null value with something useful
    dfPts.loc[dfPts[fieldActive]==-9999,fieldActive] = 0
    dfPts.loc[dfPts[fieldLon]==-9999,fieldLon] = numpy.NaN
    dfPts.loc[dfPts[fieldLat]==-9999,fieldLat] = numpy.NaN

    # Calculate i and j, modflow origin is top-left
    originX = dfPts['X'].min()
    originY = dfPts['Y'].max()
    dfPts['row'] = (numpy.floor((originY - dfPts['Y']) / inDescribe.meanCellHeight) + 1).astype(int)
    dfPts['col'] = (numpy.floor((dfPts['X'] - originX)/ inDescribe.meanCellWidth) + 1).astype(int)

    # Export this table back into the geodatabase
    recOut = dfPts.to_records()
    arrayOut = numpy.array(recOut,dtype=recOut.dtype)
    pathTable = os.path.join(scratchDB,tempAttribs)
    if arcpy.Exists(pathTable):
        arcpy.Delete_management(pathTable)
    arcpy.da.NumPyArrayToTable(arrayOut,pathTable)

    # Add an index
    arcpy.AddIndex_management(tempAttribs,['cellid'],'cellid')

    # Join fields from the attribs table
    arcpy.JoinField_management(tempFishnet, 'cellid', tempAttribs, 'cellid', \
        [fieldActive,fieldLon,fieldLat,'X','Y','row','col'])

    # Copy this to a permanent location
    outFishnet = os.path.join(subDirSurface,nameOutFishnet)
    arcpy.CopyFeatures_management(tempFishnet, outFishnet)

    # Clean up from this step
    [arcpy.Delete_management(thisTemp) for thisTemp in [tempFishnet, tempFishnetPts, tempAttribs]]

    # Print result:
    if printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('Model grid is prepared for the <strong>%s</strong> region '%regionName))
        else:
            regionName = os.path.split(os.path.split(os.path.split(pathConfig)[0])[0])[1]
            display(HTML('Model grid is prepared for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))


'''
--------------------------------------------------------------------------------
Master function for rescaling static grids
--------------------------------------------------------------------------------
'''
def rescale_static_grids(inputDB, pathConfig, subModel=False, rescaleOnly=[], soilNumLay=4, fillGrid=True, printResult=True):
    '''This script rescales all of the model input grids to a fixed resolution and extent
    using a proper method (specific to each input). It also masks the data to the model
    grid, but does not do anything with nodata values'''

    # Import modules, this one is not fully modernized to use arcpy
    import os
    import lhm_frontend
    if printResult:
        from IPython.display import display, HTML
    
    # Get the model input locations for the soils grids
    (rowSoils,nameRunWshed,nameRunDSWshed,nameRunGauges,nameRunFlowlen,nameRunFlowslope,\
        nameRunIntBasin,nameRunIntSink,nameStrVel,nameStrDepth,nameStrFrac,\
        nameRunSubTime) = lhm_frontend.names_frontend(['srcSoils',\
        'modSurfRunWshed','modSurfRunDSWshed','modSurfRunGauges','modSurfRunOverFlowlen','modSurfRunOverFlowslope',\
        'modSurfRunIntBasin','modSurfRunIntSink','modSurfStrVel','modSurfStrDepth','modSurfStrFrac',\
        'modSubRunTime'])
    
    inputPaths = lhm_frontend.lhm_surface_inputs(inputDB, pathConfig)[0]
    inputSoils = inputPaths[rowSoils]

    # Create the sources dictionary
    sources = dict()

    #TODO: complete migration to names defined in names_frontend, put all names in the documentation for reference
    #      and mention this in the prepare_model notebook
    # Stream-related grids
    sources[nameRunWshed] = {'inPath':inputDB,'inName':'prep_runoff_watersheds','method':'majority','numLay':1}
    sources[nameRunDSWshed] = {'inPath':inputDB,'inName':'prep_runoff_downstream_watersheds','method':'minimum','numLay':1}
    sources[nameRunGauges] = {'inPath':inputDB,'inName':'prep_runoff_gauges','method':'minimum','numLay':1}
    sources[nameRunFlowlen] = {'inPath':inputDB,'inName':'prep_runoff_overland_flowlength','method':'mean','numLay':1} # this is for the sediment code, flowlength to stream
    sources[nameRunFlowslope] = {'inPath':inputDB,'inName':'prep_runoff_overland_flowslope','method':'mean','numLay':1} # this is for the sediment code, flowlength to stream
    sources[nameRunIntBasin] = {'inPath':inputDB,'inName':'prep_intern_drain_basin_id','method':'maximum','numLay':1}
    sources[nameRunIntSink] = {'inPath':inputDB,'inName':'prep_intern_drain_sink_id','method':'maximum','numLay':1}
    sources[nameRunSubTime] = {'inPath':inputDB,'inName':'prep_runoff_subsurface_flowtimes','method':'mean','numLay':1}
    sources[nameStrVel] = {'inPath':inputDB,'inName':'prep_stream_velocity','method':'mean','numLay':1}
    sources[nameStrDepth] = {'inPath':inputDB,'inName':'prep_stream_depth','method':'mean','numLay':1}
    sources[nameStrFrac] = {'inPath':inputDB,'inName':'prep_stream_fraction','method':'fraction','numLay':1}

    # Wetland-related grids
    sources['wetland_bottom_depth'] = {'inPath':inputDB,'inName':'prep_wetland_depth','method':'mean','numLay':1}
    sources['wetland_complex'] = {'inPath':inputDB,'inName':'prep_wetland_complex','method':'maximum','numLay':1}
    sources['fraction_wetland'] = {'inPath':inputDB,'inName':'prep_wetland_present','method':'fraction','numLay':1}

    # Soils properties grids
    sources['soil_texture'] = {'inPath':inputSoils,'inName':'Class_ID','method':'majority','numLay':soilNumLay}
    sources['soil_lay_top'] = {'inPath':inputSoils,'inName':'laytop','method':'mean','numLay':soilNumLay}
    sources['soil_lay_bot'] = {'inPath':inputSoils,'inName':'laybot','method':'mean','numLay':soilNumLay}
    sources['soil_bub_press'] = {'inPath':inputSoils,'inName':'bub_press','method':'mean','numLay':soilNumLay}
    sources['soil_field_cap'] = {'inPath':inputSoils,'inName':'field_cap','method':'mean','numLay':soilNumLay}
    sources['soil_infil_cap'] = {'inPath':inputSoils,'inName':'infil_cap_lay1','method':'mean','numLay':1}
    sources['albedo_soil_dry'] = {'inPath':inputSoils,'inName':'dry_albedo_lay1','method':'mean','numLay':1}
    sources['soil_ksat'] = {'inPath':inputSoils,'inName':'ksat','method':'mean','numLay':soilNumLay}
    sources['soil_k0'] = {'inPath':inputSoils,'inName':'k0','method':'mean','numLay':soilNumLay}
    sources['soil_L'] = {'inPath':inputSoils,'inName':'L','method':'mean','numLay':soilNumLay}
    sources['soil_pore_size'] = {'inPath':inputSoils,'inName':'pore_size','method':'mean','numLay':soilNumLay}
    sources['soil_theta_r'] = {'inPath':inputSoils,'inName':'theta_resid','method':'mean','numLay':soilNumLay}
    sources['soil_porosity'] = {'inPath':inputSoils,'inName':'theta_sat','method':'mean','numLay':soilNumLay}
    sources['soil_alpha'] = {'inPath':inputSoils,'inName':'alpha','method':'mean','numLay':soilNumLay}
    sources['soil_N'] = {'inPath':inputSoils,'inName':'n','method':'mean','numLay':soilNumLay}
    sources['soil_wilting_point'] = {'inPath':inputSoils,'inName':'wilt_pt','method':'mean','numLay':soilNumLay}
    sources['soil_pct_fraggt10'] = {'inPath':inputSoils, 'inName':'fraggt10_r', 'method':'mean', 'numLay':soilNumLay}
    sources['soil_pct_frag3to10'] = {'inPath':inputSoils, 'inName':'frag3to10_r', 'method':'mean', 'numLay':soilNumLay}
    sources['soil_pct_org'] = {'inPath':inputSoils, 'inName':'om_r', 'method':'mean', 'numLay':soilNumLay}
    sources['soil_pct_sand'] = {'inPath':inputSoils, 'inName':'sandtotal_r', 'method':'mean', 'numLay':soilNumLay}
    sources['soil_pct_clay'] = {'inPath':inputSoils, 'inName':'claytotal_r', 'method':'mean', 'numLay':soilNumLay}

    # Model geometry-related grids
    sources['slope'] = {'inPath':inputDB,'inName':'prep_dem_fill_slope','method':'mean','numLay':1}
    sources['aspect'] = {'inPath':inputDB,'inName':'prep_dem_aspect','method':'mean','numLay':1}
    sources['elevation_min'] = {'inPath':inputDB,'inName':'prep_dem_raw','method':'minimum','numLay':1}
    sources['elevation_mean'] = {'inPath':inputDB,'inName':'prep_dem_raw','method':'mean','numLay':1}
    sources['elevation_up'] = {'inPath':inputDB,'inName':'prep_dem_upland','method':'mean','numLay':1}
    sources['elevation_wet'] = {'inPath':inputDB,'inName':'prep_dem_wetland','method':'mean','numLay':1}
    sources['index_internally_drained'] = {'inPath':inputDB,'inName':'prep_intern_drain_grid','method':'majority','numLay':1}

    # State grids
    sources['starting_heads'] = {'inPath':inputDB,'inName':'prep_start_heads','method':'mean','numLay':1}

    # Remove any grids not in rescaleOnly, if not empty
    if len(rescaleOnly)>0:
        sourcesOrig = sources
        sources = dict()

        # Make into a list, if not already
        if isinstance(rescaleOnly,str):
            rescaleOnly = [rescaleOnly]

        for thisRescale in rescaleOnly:
            for source in sourcesOrig.keys():
                if source.find(thisRescale)>=0:
                    sources[source] = sourcesOrig[source]

    # Call the rescale functions
    rescale_grids_common(inputDB, sources, subModel, fillGrid)

    # Print result:
    if printResult:
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('Static grids are rescaled for the <strong>%s</strong> region '%regionName))
        else:
            regionName = os.path.split(os.path.split(os.path.split(pathConfig)[0])[0])[1]
            display(HTML('Static grids are rescaled for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))

    # Return the list of resampled rasters
    return sources.keys()

'''
--------------------------------------------------------------------------------
Master function for rescaling observation grids
--------------------------------------------------------------------------------
'''
def rescale_obs_grids(inputDB, pathConfig, inputObsGrids, subModel=False, fillGrid=False, printResult=True):
    '''Prepares the list of input obs grids and then calls the rescale function'''

    import os
    if printResult:
        from IPython.display import display, HTML

    # Create the sources dictionary
    sources = dict()

    # Format the inputObsGrids into the sources dict
    for obsGrid in inputObsGrids:
        (gridPath,gridFile) = os.path.split(obsGrid)
        gridName = os.path.splitext(gridFile)[0]
        sources[gridName] = dict()
        sources[gridName]['inPath'] = gridPath
        sources[gridName]['inName'] = gridFile
        sources[gridName]['method'] = 'majority'
        sources[gridName]['numLay'] = 1

    # Call the rescale functions
    rescale_grids_common(inputDB, sources, subModel, fillGrid)

    # Print result:
    if printResult:
        
        if not subModel:
            regionName = os.path.split(pathConfig)[1]
            display(HTML('Obs grids are rescaled for the <strong>%s</strong> region '%regionName))
        else:
            regionName = os.path.split(os.path.split(os.path.split(pathConfig)[0])[0])[1]
            display(HTML('Obs grids are rescaled for the <strong>%s</strong> region <strong>%s</strong> submodel'%(regionName,subModel)))

    # Return the list of resampled rasters
    return sources.keys()

'''
--------------------------------------------------------------------------------
Create and rescale a slice array
--------------------------------------------------------------------------------
'''
def create_rescale_slice(inputDB, modelSlice, subModel=False, fillGrid=True):
    """
    modelSlice is a dictionary of the same form as the configSources dictionaries
    """
    import os
    import arcpy
    import lhm_frontend
    from raster import create_surface_raster

    # Specify temporary names
    tempSlice = 'tempSlice'

    # Set the environment
    arcpy.env.workspace = inputDB
    if modelSlice['method']=='grid':
        arcpy.env.cellSize = arcpy.Describe(modelSlice['data']).MeanCellHeight
    else:
        # Get the model grid
        if subModel:
            strReqLay = 'dirSubmodelLayers'
        else:
            strReqLay = 'dirModelLayers'
            
        (dirLayers,dirSurface,nameModelGrid,modExtension) = lhm_frontend.names_frontend(\
            [strReqLay,'dirSurface','modSurfGrid','outExtRaster'])

        # Define the location of the model grid
        if not subModel:
            relPathLayers = os.path.split(inputDB)[0]+'/%s/%s'%(dirLayers,dirSurface)
        else:
            # This is the case where we are using a submodel
            relPathLayers = os.path.split(inputDB)[0]+('/%s/'+subModel+'/%s')%(dirLayers,dirSurface)
        
        modelGrid = os.path.join(relPathLayers,nameModelGrid+modExtension)
        
        # Define cellsize from this
        arcpy.env.cellSize = arcpy.Describe(modelGrid).MeanCellHeight
    
    # Create the model slice raster from the input
    create_surface_raster(modelSlice,tempSlice,noDataToValue=0)

    # Now rescale
    sources = dict()
    sources['slice'] = {'inPath':inputDB,'inName':tempSlice,'method':'minimum','numLay':1}
    rescale_grids_common(inputDB, sources, subModel, fillGrid)

    # Clean up
    arcpy.Delete_management(tempSlice)


'''
--------------------------------------------------------------------------------
Collapse rescaled rasters
--------------------------------------------------------------------------------
'''
def collapse_rescale_layers(inputDB, subModel=False):
    '''This function is a thin wrapper around raster.collapse_single_layer_rasters that is
    run after the rescaling operation'''

    import os
    from raster import collapse_single_layer_rasters

    # Define the location of the model grid
    if not subModel:
        relPathLayers = os.path.split(inputDB)[0]+'/Model_Layers/Surface'
    else:
        # This is the case where we are using a submodel
        relPathLayers = os.path.split(inputDB)[0]+'/Submodel_Layers/'+subModel+'/Surface'

    # Run the operation
    collapse_single_layer_rasters(relPathLayers, inExt='.tif')
    collapse_single_layer_rasters(relPathLayers, inExt='.img')

'''
--------------------------------------------------------------------------------
Common rescaling functions
--------------------------------------------------------------------------------
'''
def rescale_grids_common(inputDB, sources, subModel, fillGrid=True):
    '''Actually does the work of rescaling the model grids'''

    import os, arcpy, warnings
    from tqdm.notebook import tqdm
    import lhm_frontend
    from gis import resample_coarse

    # Get the names from the lhm_frontend
    if subModel:
        strReqLay = 'dirSubmodelLayers'
    else:
        strReqLay = 'dirModelLayers'
    (dirLayers,dirSurface,nameModelGrid,outExtension) = lhm_frontend.names_frontend(\
        [strReqLay,'dirSurface','modSurfGrid','outExtRaster'])

    # Define the location of the model grid
    if not subModel:
        relPathLayers = os.path.split(inputDB)[0]+'/%s/%s'%(dirLayers,dirSurface)
    else:
        # This is the case where we are using a submodel
        relPathLayers = os.path.split(inputDB)[0]+('/%s/'+subModel+'/%s')%(dirLayers,dirSurface)

    modelGrid = os.path.join(relPathLayers,nameModelGrid+outExtension)
    
    # Define a temporary variable
    outTemp = 'outTemp.tif'

    #-------------------------------------------------------------------------------
    # Prepare the environment -- don't use standard environment here
    #-------------------------------------------------------------------------------
    # Check out a spatial analyst extension license
    arcpy.CheckOutExtension("Spatial")

    # Set the projection to match
    arcpy.env.outputCoordinateSystem = modelGrid

    # Set the workspace to the output directory for model-resolution grids
    arcpy.env.workspace = os.path.split(modelGrid)[0]

    # Set overwrite output on
    arcpy.env.overwriteOutput = 1

    # Determine the output grid cellsize and extent
    outSize = arcpy.Describe(modelGrid).MeanCellHeight
    outExtent = arcpy.Describe(modelGrid).extent

    arcpy.env.cellSize = outSize
    arcpy.env.extent = outExtent
    arcpy.env.snapRaster = modelGrid
    arcpy.env.compression = 'LZ77'

    #---------------------------------------------------------------------------
    # Resize the Grids
    #---------------------------------------------------------------------------
    # Bring in a raster of the model grid
    rasterModel = arcpy.Raster(modelGrid)

    # Create a constant grid for filling in
    rasterZeros = arcpy.sa.CreateConstantRaster(0,'FLOAT')

    # Loop through the grids
    for grid in tqdm(sorted(sources.keys())):
        # Determine the output grids and aggregate methods
        inGridBase = os.path.join(sources[grid]['inPath'],sources[grid]['inName'])
        aggMethod = sources[grid]['method']
        numLay = sources[grid]['numLay']

        for layer in range(0,numLay):
            # Update the layer numbering of the grid
            if numLay > 1:
                inGrid = inGridBase + '_lay' + str(layer+1)
                outGrid = grid + '_lay'+ str(layer+1) + outExtension
            else:
                inGrid = inGridBase
                outGrid = grid + outExtension

            # Check to see if this grid exists, if so, resample, otherwise copy
            # the model grid as output if fillGrid=True
            if arcpy.Exists(inGrid):
                # Rescale and create output grid at model resolution
                resample_coarse(inGrid, outTemp, outExtent, outSize, aggMethod, arcpy.env.snapRaster)

                # Multiply by model grid to mask
                rasterOutTemp = arcpy.Raster(outTemp)
                rasterMask = rasterOutTemp * rasterModel
                rasterMask = arcpy.sa.ApplyEnvironment(rasterMask)
                arcpy.CopyRaster_management(rasterMask,outGrid) # to assure compression

                # Clean up
                del rasterOutTemp, rasterMask
                arcpy.Delete_management(outTemp)

            elif fillGrid:
                warnings.warn('Missing grid %s, copying zeros grid to %s'%(inGrid,outGrid),UserWarning)

                # Copy the model grid to the missing output grid position
                arcpy.Copy_management(rasterZeros,outGrid)

            else:
                raise Exception('Missing grid %s, and fillGrid=False, stopping')

            # Rename the band
            #TODO: this code below doesn't seem to work for some reason
            arcpy.Rename_management(os.path.join(arcpy.env.workspace,outGrid+'/Band_1'),\
                os.path.join(arcpy.env.workspace,outGrid+'/lay_%d'%(layer+1)),'RasterBand')

    # Clean up
    del rasterModel, rasterZeros
