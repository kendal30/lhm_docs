def prep_observations(inputDB, pathConfig, subModel, params, printResult=True):
    # Function imports
    import pandas, arcpy
    import lhm_frontend
    import obs, os

    if printResult:
        from IPython.display import display, HTML

    # Get standardized names or param values
    (subDirObs,subDirSurface,dirModLay,dirSubmodLay,extRaster,extTable,extFeature,nameIbound,\
        obsFieldStn,obsFieldPts) = \
        lhm_frontend.names_frontend(['dirObs','dirSurface','dirModelLayers',\
            'dirSubmodelLayers','outExtRaster','outExtTable','outExtFeature',\
            'modSurfGrid','obsFieldStations','obsFieldPoints'])
    
    # Get the observations specification
    inputObs = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig,subModel=subModel,section='Obs')[0]

    # Make a list of observations categories to process
    obsFields = [obsFieldStn,obsFieldPts] 

    # Get the path to the model/submodel ibound grid, and watersheds file name
    if not subModel:
        dirSurface = '%s/%s'%(dirModLay,subDirSurface)
        dirObs = '%s/%s'%(dirModLay,subDirObs)
        outNameTemplate = '%s%s'
    else:
        dirSurface = '%s/%s/%s'%(dirSubmodLay,subModel,subDirSurface)
        dirObs = '%s/%s/%s'%(dirSubmodLay,subModel,subDirObs)
        outNameTemplate = '%s_'+subModel+'%s'
    dirSurface = os.path.join(os.path.split(inputDB)[0],dirSurface)
    dirObs = os.path.join(os.path.split(inputDB)[0],dirObs)

    # Now, get the proper ibound location and band
    ibound = os.path.join(dirSurface,'%s%s'%(nameIbound,extRaster))
    bandStr = '/Band_1'

    if not os.path.exists(ibound): #may not have already been collapsed
        ibound = os.path.join(dirSurface,'%s_lay1%s'%(nameIbound,extRaster))
        ibound = os.path.join(os.path.split(inputDB)[0],ibound)
        bandStr = ''

    # Get the model grid shapefile
    gridShape = os.path.join(dirSurface,'%s%s'%(nameIbound,extFeature))

    # Add the cellSize to the params
    params['cellSize'] = arcpy.Describe(ibound+bandStr).meanCellHeight

    # Set the output directory
    outDir = dirObs

    # -------------------------------------------------------------------------------
    # Prepare the environment
    # -------------------------------------------------------------------------------
    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,ibound)

    # -------------------------------------------------------------------------------
    # Do the processing
    # -------------------------------------------------------------------------------
    # Keep track of the data types we are writing, to make sure that the starting ID is correct
    idStart = dict()

    # Loop through the specified datasets, both 'point' and 'station'
    for obsField in obsFields:
        if not inputObs[obsField]:
            continue

        # Get the observation specification
        thisObs = inputObs[obsField]

        # Loop through the datasets
        for thisDataset in params['datasets']:
            # Check if this dataset is in the observations specification
            if thisDataset not in thisObs.keys():
                continue

            #1. Read in the observations data table
            dfData = obs.read_table(thisObs[thisDataset], arcpy)

            # Subset by date range, if present
            if 'dateRange' in params.keys():
                dfData = dfData[(dfData['time_start']>=params['dateRange'][0]) & (dfData['time_end']<=params['dateRange'][1])]

            #2. Read in the observations stations
            # First, create our temporary features
            dfFeatures = obs.read_features(thisObs[thisDataset], arcpy, tempFeature=None, returnFeature=True)

            #3. Create the spatial dataset with observations locations
            # Create the spatial_id field using the index
            dfFeatures['grid_id'] = dfFeatures.index + 1

            # Call the grid creation function
            outRasterName = outNameTemplate%(thisDataset,extRaster)
            outRaster = os.path.join(outDir,outRasterName)
            dfFeatures = obs.create_grid(dfFeatures, gridShape, ibound, arcpy, outRaster)

            #4. Create the new formatted table
            # Handle z_bot and z_top which may have been specified in either the data or feature table
            # Use the data table if it is present, otherwise use the feature table
            if 'z_top' in dfData.columns:
                dfFeatures = dfFeatures.drop('z_top',axis=1)
            if 'z_bot' in dfData.columns:
                dfFeatures = dfFeatures.drop('z_bot',axis=1)
                
            # Merge the data and features
            dfData = pandas.merge(dfData,dfFeatures,how='left',on='loc_id')

            # Assign the fields not yet defined
            dfData['type'] = obsField
            dfData['set_name'] = thisDataset
            thisVar = thisObs[thisDataset]['mapping']['variable']
            dfData['variable'] = thisVar
            dfData['grid'] = outRasterName

            # Get the starting ID for this dataset
            if thisVar not in idStart.keys():
                idStart[thisVar] = 0

            # Reset the index, and rename it to 'id', and add the starting ID + 1
            dfData = dfData.reset_index()
            dfData = dfData.rename(columns={'index':'id'})
            dfData['id'] = dfData['id'] + idStart[thisVar] + 1

            # Update the starting ID for this data type
            idStart[thisVar] = dfData['id'].max()

            # Reorder the columns to match the template datasets
            dfTemplate = obs.create_obs_table_df()
            dfData = dfData[dfTemplate.columns]

            # Set the time_start and time_end fields to string objects with correct date format
            dfData['time_start'] = dfData['time_start'].dt.strftime('%Y-%m-%d %H:%M:%S')
            dfData['time_end'] = dfData['time_end'].dt.strftime('%Y-%m-%d %H:%M:%S')

            # Write the dataframe out to a table, making sure to use correct date format for the time_start and time_end fields
            nameCsv = outNameTemplate%(thisDataset,extTable)
            outTable = os.path.join(outDir,nameCsv)
            dfData.to_csv(outTable,index=False)

            # Log this
            lhm_frontend.log_lhm_output(nameCsv, outDir, inputDB)

    # Print result:
    if printResult:
        display(HTML('General point observations finished processing'))