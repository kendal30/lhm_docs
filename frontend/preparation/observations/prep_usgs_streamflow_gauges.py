import lhm_frontend


'''
--------------------------------------------------------------------------------
Select USGS Streamflow Gauges
--------------------------------------------------------------------------------
'''
def select_gauges(inputDB, pathConfig, params, printResult=True):
    #This function creates a shapefile with an inventory of the USGS gauges within the model
    #boundary and specified time period, it does not snap them to a flowaccumulation grid
    #that will happen later

    import arcpy, os, pandas, numpy
    from datetime import datetime
    import lhm_frontend, nwisweb
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    #Standardized Inputs
    #-------------------------------------------------------------------------------
    # Get the names
    (rowStates,outSelect) = lhm_frontend.names_frontend(['srcStates','prepSelGauges'])
    
    # Get the observations specification
    (inputPaths,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    #Some NWISWEB stuff
    dischParamName = 'discharge'
    fieldBeginDate = 'begin_date'
    fieldEndDate = 'end_date'
    fieldLat = 'dec_lat_va'
    fieldLon = 'dec_long_va'
    fieldSiteName = 'station_nm'
    fieldSiteNum = 'site_no'
    fieldBasinArea = 'drain_area_va'
    fieldAgencyCode = 'agency_cd'

    #Specify a date format
    dateFormat = '%Y-%m-%d'

    #Conversion factor
    convMi2Km2 = 2.58999
    
    #Temporary names (shouldn't need to change these)
    tempStates = 'temp_states'
    tempInv = 'temp_inventory'
    tempXY = 'temp_xy_layer'
    tempFeature = 'temp_gauges'

    #Out projection
    outPrj = arcpy.SpatialReference(104145) #WKID code for NAD 1983_2011

    #-------------------------------------------------------------------------------
    #Set up the environment
    #-------------------------------------------------------------------------------
    arcpy = lhm_frontend.set_environment('',inputDB,inputBdry) #don't need cellsize

    #TIGER states (for state IDs)
    inTigerStates = inputPaths[rowStates]['feature']
    fieldState = inputPaths[rowStates]['field']
    
    #-------------------------------------------------------------------------------
    #Determine which states to process
    #-------------------------------------------------------------------------------
    #Intersect the overlay of states with the project mask
    arcpy.Intersect_analysis([inTigerStates,inputBdry],tempStates)

    #Bring in these features and make a list of unique states
    dfStates = pandas.DataFrame(arcpy.da.TableToNumPyArray(tempStates,[fieldState]))
    states = pandas.unique(dfStates[fieldState].values)
 
    #Clean up
    arcpy.Delete_management(tempStates)

    #-------------------------------------------------------------------------------
    #Loop through the states
    #-------------------------------------------------------------------------------
    #Pre-process some things
    beginDate = datetime.strptime(params['dateRange'][0],dateFormat)
    endDate = datetime.strptime(params['dateRange'][1],dateFormat)

    #Loop through states, get a master inventory
    for state in states:
        #Fetch the inventory
        (thisInventory,dataTypes) = nwisweb.nwisweb_inventory(state,'sw',dischParamName,\
                                                              expandedOption=True,returnTypes=True)
        
        # Subset our columns
        thisInventorySub = thisInventory[[fieldSiteNum,fieldSiteName,fieldAgencyCode,fieldLon,\
                                          fieldLat,fieldBeginDate,fieldEndDate,fieldBasinArea]].copy()

        # Handle empty lat/lon fields first
        thisInventorySub = thisInventorySub.loc[thisInventorySub[fieldLon]!='']
        thisInventorySub = thisInventorySub.loc[thisInventorySub[fieldLat]!='']
        thisInventorySub[fieldLon] = thisInventorySub[fieldLon].astype(float)
        thisInventorySub[fieldLat] = thisInventorySub[fieldLat].astype(float)
        
        # Now, limit dates
        thisInventorySub = thisInventorySub.loc[thisInventorySub[fieldBeginDate]<endDate]
        thisInventorySub = thisInventorySub.loc[thisInventorySub[fieldEndDate]>beginDate]
        
        # Convert the basin area field to km2
        indEmpty = thisInventorySub[fieldBasinArea] != ''
        thisInventorySub = thisInventorySub.loc[indEmpty,:]
        thisInventorySub[fieldBasinArea] = thisInventorySub[fieldBasinArea].astype(float) * convMi2Km2 #convert mi2 to km2
        
        if list(states).index(state)==0:
            inventory = thisInventorySub #copy this to a permanent inventory object
        else:
            inventory = pandas.concat([inventory,thisInventorySub],axis=0)

    # Create the output dtype array
    # Get the datalength of the original datafields
    indNum = thisInventory.columns.to_list().index(fieldSiteNum)
    indName = thisInventory.columns.to_list().index(fieldSiteName)
    indAgency = thisInventory.columns.to_list().index(fieldAgencyCode)
    
    # Create the output dtypes array
    outDtype = numpy.dtype([('index', '<i8'), (fieldSiteNum, '<U'+dataTypes[indNum][:-1]),\
                          (fieldSiteName, '<U'+dataTypes[indName][:-1]), \
                          (fieldAgencyCode,'<U'+dataTypes[indAgency][:-1]), (fieldLon, '<f8'),\
                          (fieldLat, '<f8'), (fieldBeginDate, 'M8[us]'), (fieldEndDate, 'M8[us]'),\
                          (fieldBasinArea, '<f8')])

    
    # Now, write out the feature
    recOut = inventory.to_records()
    arrayOut = numpy.array(recOut,dtype=outDtype)
    pathTable = os.path.join(inputDB,tempInv)
    if arcpy.Exists(pathTable):
        arcpy.Delete_management(pathTable)
    arcpy.da.NumPyArrayToTable(arrayOut,pathTable)

    #Convert this to a temporary event layer (which will be clipped next)
    arcpy.MakeXYEventLayer_management(tempInv,fieldLon,fieldLat,tempXY,outPrj)
    arcpy.CopyFeatures_management(tempXY,tempFeature)

    #Clip to the boundary
    arcpy.Intersect_analysis([tempFeature,inputBdry],outSelect)

    #Clean up
    [arcpy.Delete_management(thisFeature) for thisFeature in [tempFeature, tempXY, tempInv]]

    #Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('USGS gauge sites have been selected for the <strong>%s</strong> region'%regionName))


'''
--------------------------------------------------------------------------------
Pull USGS streamflow
--------------------------------------------------------------------------------
'''
def prep_observations(inputDB, pathConfig, subModel, params=dict(), watersheds=True, printResult=True):
    '''
    This function processes USGS stream gauge data into LHM observations. It generates watersheds
    for each point, selects only those watersheds totally within the groundwater model's IBOUND
    array, and then generates a .csv observations data file for LHM import.

    The params dictionary requires:
     - "dateRange" of format yyyy-mm-dd, two-item list, first is minimum, second maximum
    Optional parameters are:
     - "edgeDist", default value is -100, specify the distance to buffer around
       the edge of the ibound array for selecting gauges, negative values
       indicate a buffer inside the feature
     - "weight", default value is 1, specify the datapoint weight. This would
       be used for calibrating parameter values in the model
     - "idStart", default value is 1, only set other than 1 if you know you
       have a reason to (i.e. another similar datatype as a different set)
     - "timeZone", default value is 0, really shouldn't matter much for daily streamflows
    Optionally, specify a boolean value for "watersheds" which, if False, will skip watershed generation.
        This would be used if you have already computed them and are re-running the flow data step.
    '''

    import arcpy, os, datetime, numpy, pandas, tqdm
    from tqdm.notebook import tqdm

    if printResult:
        from IPython.display import display, HTML

    # -------------------------------------------------------------------------------
    # Dataset specific inputs
    # -------------------------------------------------------------------------------
    # Input user gauge locations-- these must come from the prep_runoff_model snap_combine_gauges function
    inGauges = 'prep_combine_gauges'
    fieldsGauges = {'agency':'agency','loc_id':'loc_id','loc_name':'loc_name','grid_id':'site_num',\
                    'x':'POINT_X','y':'POINT_Y'}

    # Get standardized names or param values
    (flowdir,nameCsv,nameWatersheds,subDirObs,nameIbound,subDirGndwater,dirModLay,dirSubmodLay,extRaster,obsField) = \
        lhm_frontend.names_frontend([\
        'prepDEMFillDir','obsCsvUSGS','obsWshedsUSGS','dirObs','modGndIbound','dirGndwater',\
        'dirModelLayers','dirSubmodelLayers','outExtRaster','obsFieldStations'])

    # Prepare the other parameters if they've not been specified
    paramDefaults = {'edgeDist':-100,'weight':1,'idStart':1,'timeZone':0}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Get the path to the model/submodel ibound grid, and watersheds file name
    if not subModel:
        dirGndwater = '%s/%s'%(dirModLay,subDirGndwater)
        dirObs = '%s/%s'%(dirModLay,subDirObs)
        outNameWsheds = nameWatersheds
    else:
        dirGndwater = '%s/%s/%s'%(dirSubmodLay,subModel,subDirGndwater)
        dirObs = '%s/%s/%s'%(dirSubmodLay,subModel,subDirObs)
        outNameWsheds = nameWatersheds+'_'+subModel
    dirGndwater = os.path.join(os.path.split(inputDB)[0],dirGndwater)
    dirObs = os.path.join(os.path.split(inputDB)[0],dirObs)

    # Now, get the proper ibound location and band
    ibound = os.path.join(dirGndwater,'%s%s'%(nameIbound,extRaster))
    bandStr = '/Band_1'

    if not os.path.exists(ibound): #may not have already been collapsed
        ibound = os.path.join(dirGndwater,'%s_lay1%s'%(nameIbound,extRaster))
        ibound = os.path.join(os.path.split(inputDB)[0],ibound)
        bandStr = ''

    # Add the cellSize to the params
    params['cellSize'] = arcpy.Describe(ibound+bandStr).meanCellHeight

    # Set the output directory
    outDir = dirObs

    # -------------------------------------------------------------------------------
    # Set internal variables
    # -------------------------------------------------------------------------------
    # Specify a name for this observation set
    setName = 'USGS_streamflows'
    agencyName = 'USGS'
    setVar = 'streamflow'
    setAgg = 'mean'
    setUnits = 'm^3s^-1'

    # Convert from cfs to cms
    convFac = 0.028317

    # Specify output base names
    gridBase = 'usgs_'

    # Temporary names
    layerShape = 'layer_shape'
    layerShapeAgency = 'layer_shape_agency'
    layerWatersheds = 'layer_watersheds'

    tempBoundary = 'temp_boundary'
    tempBdryBuffer = 'temp_bdry_buffer'
    tempGauges = 'temp_gauges'
    tempGaugesAgency = 'temp_gauges_agency'
    tempGaugesDates = 'temp_gauges_dates'
    tempWatershedsAll = 'temp_watersheds_all'

    # NWISWEB stuff
    dischParamName = 'discharge'
    fieldBeginDate = 'begin_date'
    fieldEndDate = 'end_date'
    dateFormatRange = '%Y-%m-%d'

    # -------------------------------------------------------------------------------
    # Prepare the environment
    # -------------------------------------------------------------------------------
    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,ibound)

    # Add the helper paths and import
    import nwisweb
    from gis import batch_create_watersheds_feature
    from obs import create_obs_table_df

    # -------------------------------------------------------------------------------
    # Limit gauges to those within ibound array, in specified time period, only "USGS" agency
    # -------------------------------------------------------------------------------
    # Create a temporary feature with only points within the active boundary extracted
    rasterIbound = arcpy.Raster(ibound)
    rasterIboundNoData = arcpy.sa.Con(rasterIbound>0,1)
    arcpy.RasterToPolygon_conversion(rasterIboundNoData,tempBoundary,'NO_SIMPLIFY')
    arcpy.Buffer_analysis(tempBoundary,tempBdryBuffer,params['edgeDist'])
    arcpy.Clip_analysis(os.path.join(inputDB,inGauges),tempBdryBuffer,tempGauges)
    
    if watersheds:
        # Select only those gauges that are USGS agency
        arcpy.MakeFeatureLayer_management(tempGauges,layerShapeAgency)
        whereClause = fieldsGauges['agency'] + ' = \''+agencyName+'\''
        arcpy.SelectLayerByAttribute_management(layerShapeAgency,'NEW_SELECTION',whereClause)
        arcpy.CopyFeatures_management(layerShapeAgency,tempGaugesAgency)

        # Now, loop through gauges, checking that they fall within the desired time period
        # Create a feature layer for selecting
        arcpy.MakeFeatureLayer_management(tempGaugesAgency,layerShape)

        # Create the search cursor
        rowsGauges = arcpy.SearchCursor(tempGaugesAgency)

        # Loop through the rows
        for row in rowsGauges:
            # Get info on this gauge
            gaugeInfo = dict()
            for field in fieldsGauges.keys():
                gaugeInfo[field] = row.getValue(fieldsGauges[field])

            # Determine if this gauge has data during the period of interest
            # Fetch the inventory for this station
            thisInventory = nwisweb.nwisweb_inventory([gaugeInfo['loc_id']],'sw',dischParamName)

            # Now test for date range, continue if within
            if numpy.any(thisInventory[fieldBeginDate]<=datetime.datetime.strptime(params['dateRange'][1],dateFormatRange)) and \
                numpy.any(thisInventory[fieldEndDate]>=datetime.datetime.strptime(params['dateRange'][0],dateFormatRange)):
                # Select only this gauge
                whereClause = '"' + fieldsGauges['loc_id'] + '" = \'' + gaugeInfo['loc_id'] + '\''
                arcpy.SelectLayerByAttribute_management(layerShape,'ADD_TO_SELECTION',whereClause)
        # Clean up the search cursor
        del row, rowsGauges

        # Copy these features to a new temporary feature
        arcpy.CopyFeatures_management(layerShape,tempGaugesDates)
    
        # Batch create the watersheds, set extent to flowdirection first
        arcpy.env.extent = os.path.join(inputDB,flowdir)
        arcpy.env.cellSize = os.path.join(inputDB,flowdir)
        batch_create_watersheds_feature(tempGaugesDates,tempWatershedsAll,os.path.join(inputDB,flowdir),fieldsGauges['grid_id'])

        # Select only those features that fall entirely within the boundary of the groundwater model
        arcpy.MakeFeatureLayer_management(tempWatershedsAll,layerWatersheds)
        arcpy.SelectLayerByLocation_management(layerWatersheds,'COMPLETELY_WITHIN',tempBoundary)
        arcpy.CopyFeatures_management(layerWatersheds,outNameWsheds)

        # Log this
        lhm_frontend.log_lhm_output(outNameWsheds, inputDB, inputDB)

        # Clean up
        [arcpy.Delete_management(thisLayer) for thisLayer in (layerShapeAgency, layerShape, layerWatersheds)]
        [arcpy.Delete_management(thisTemp) for thisTemp in (tempGaugesAgency, tempGaugesDates, tempWatershedsAll)]

    # -------------------------------------------------------------------------------
    # Loop through the selected watersheds, performing all operations
    # -------------------------------------------------------------------------------
    # Get a search cursor for the input shapefile
    rowsWsheds = arcpy.SearchCursor(outNameWsheds)

    # Make feature layer for selection
    arcpy.MakeFeatureLayer_management(outNameWsheds,layerWatersheds)

    # Set extent to ibound
    arcpy.env.extent = ibound
    arcpy.env.cellSize = params['cellSize']

    # Loop through the rows of watersheds, selecting each gauge and populating the table
    outCreated = False
    totalLoop = int(arcpy.GetCount_management(outNameWsheds)[0])
    for rowWshed in tqdm(rowsWsheds,total=totalLoop,desc='Downloading observations'):
        # Get the gauge for this watershed
        thisWshed = rowWshed.getValue(fieldsGauges['grid_id'])
        rowsGauges = arcpy.SearchCursor(tempGauges)
        for rowGauge in rowsGauges:
            if rowGauge.getValue(fieldsGauges['grid_id'])==thisWshed:
                # Get info on this gauge
                gaugeInfo = dict()
                for field in fieldsGauges.keys():
                    gaugeInfo[field] = rowGauge.getValue(fieldsGauges[field])
        del rowsGauges

        # For this gauge, define output names
        gridName = gridBase + gaugeInfo['loc_id'] + extRaster

        # Convert current watershed feature into a grid
        if watersheds or not (arcpy.Exists(os.path.join(outDir,gridName))):
            whereClause = '"' + fieldsGauges['grid_id'] +'" = ' + str(thisWshed)
            arcpy.SelectLayerByAttribute_management(layerWatersheds,'NEW_SELECTION',whereClause)
            arcpy.PolygonToRaster_conversion(layerWatersheds,fieldsGauges['grid_id'],\
                os.path.join(outDir,gridName),"","",params['cellSize'])

        # ---------------------------------------------------------------------------
        # Populate the output table for this gauge
        # ---------------------------------------------------------------------------
        # Get the daily flow data for this gauge
        dfFlow = nwisweb.nwisweb_flow_daily(gaugeInfo['loc_id'],params['dateRange'],['discharge'])
        
        # Fill the dataframe with missing dates
        dfAll = pandas.DataFrame(pandas.date_range(start=params['dateRange'][0], end=params['dateRange'][1], freq='1D'),\
            columns=['datetime'])
        if not isinstance(dfFlow,bool):
            dfAll = dfAll.merge(dfFlow,how='left',on=['datetime'])
            dfAll['discharge'] = dfAll['discharge_mean'] * convFac
        else:
            dfAll['discharge'] = numpy.NaN
        
        # Prepare the output dataframe for this gauge
        dfObsRow = pandas.DataFrame(dfAll['discharge'])
        dfObsRow = dfObsRow.rename(columns={'discharge':'value'})

        # Write static row values
        dfObsRow.loc[:,'id'] = params['idStart'] + numpy.arange(0,len(dfObsRow),dtype=numpy.int32) 
        dfObsRow.loc[:,'type'] = obsField
        dfObsRow.loc[:,'loc_id'] = gaugeInfo['loc_id']
        dfObsRow.loc[:,'loc_name'] = gaugeInfo['loc_name']
        dfObsRow.loc[:,'set_name'] = setName
        dfObsRow.loc[:,'variable'] = setVar
        dfObsRow.loc[:,'x'] = gaugeInfo['x']
        dfObsRow.loc[:,'y'] = gaugeInfo['y']
        dfObsRow.loc[:,'z_top'] = 0
        dfObsRow.loc[:,'z_bot'] = 0
        dfObsRow.loc[:,'time_zone'] = params['timeZone']
        dfObsRow.loc[:,'aggregate'] = setAgg
        dfObsRow.loc[:,'units'] = setUnits
        dfObsRow.loc[:,'grid'] = gridName
        dfObsRow.loc[:,'grid_id'] = gaugeInfo['grid_id']
        dfObsRow.loc[:,'weight'] = params['weight']

        # Write the times
        dfObsRow.loc[:,'time_start'] = dfAll['datetime'].dt.strftime('%Y-%m-%d %H:%M:%S')
        dfObsRow.loc[:,'time_end'] = (dfAll['datetime'] + pandas.Timedelta(days=1, minutes=-1)).\
            dt.strftime('%Y-%m-%d %H:%M:%S')

        # Append
        if not outCreated:
            dfObs = dfObsRow.copy()
            outCreated = True
        else:
            dfObs = pandas.concat([dfObs,dfObsRow],axis=0,ignore_index=True)

    # Delete the rowsShape and locks
    del rowsWsheds

    # Rearrange columns into final order
    dfTemplate = create_obs_table_df()
    listCols = dfTemplate.columns
    dfObs = dfObs[listCols]

    # Write the observations table
    dfObs.to_csv(os.path.join(outDir,nameCsv),index=False)

    # Log this
    lhm_frontend.log_lhm_output(nameCsv, outDir, inputDB)

    # Clean up
    del rasterIbound, rasterIboundNoData
    arcpy.Delete_management(layerWatersheds)
    [arcpy.Delete_management(thisTemp) for thisTemp in (tempGauges, tempBdryBuffer, tempBoundary)]

    # Print result:
    if printResult:
        display(HTML('USGS observations finished processing'))