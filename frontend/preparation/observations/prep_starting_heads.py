"""
This module handles starting heads conditions for the surface and groundwater models
it uses the standard 'grid', 'feature', and 'constant' options.

If a grid is specified, then it will simply apply the environment to that grid and clip
it to the buffered boundary.

If a feature is specified, then it will interpolate water levels if the feature is a point
type, and use the specified polygons if a polygon type. The interpolation routine
will take additional options.
"""

import lhm_frontend

'''
--------------------------------------------------------------------------------
Module Names
--------------------------------------------------------------------------------
'''
def names_module(reqList):
    """These inputs are used only within the module below
    """

    names = dict()
    names['fieldIDNum'] = 'ID_num'
    names['fieldIDName'] = 'ID_name'
    names['fieldDate'] = 'constDate'
    names['fieldVal'] = 'fieldVal'
    names['fieldElev'] = 'demElev_m'
    names['fieldWTElev'] = 'wt_elev_m'
    names['fieldDTW'] = 'dtw_m'
    names['fieldScreenFrm'] = 'scrn_top_elev_m'
    names['fieldScreenTo'] = 'scrn_bot_elev_m'

    names['tempKrigRound1'] = 'tempKrigRound_1'

    # Return what is requested
    if reqList is None:
        return names
    elif isinstance(reqList,list):
        if len(reqList) > 1:
            return [names[thisReq] for thisReq in reqList]
        else:
            return names[reqList[0]]
    elif isinstance(reqList,str):
        return names[reqList]


'''
--------------------------------------------------------------------------------
Main Module Function
--------------------------------------------------------------------------------
'''
def starting_heads(inputDB, pathConfig, params=dict(), validate=False, clean=False, printResult=True):
    """This function decides which routine is called to create the starting heads
    raster based on whether the user specifies 'grid', 'feature', or 'constant' options.

    Optional params for the start_heads function are:
        - cellsize: either a number or a string, if a string, should be a raster
        - other options are passed to the interpolate_water_levels function if called
    """
    import os
    import raster
    create_surface_raster = raster.create_surface_raster

    if printResult:
        from IPython.display import display, HTML

    # Get standardized names or param values
    (srcStartHeads,nameOutStartHeads,nameOutWells) = lhm_frontend.names_frontend(['srcStartHeads','prepStartHeads','prepInterpWells'])

    # Get the outputs for this instance
    (sources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB, pathConfig)

    # Prepare the cellsize parameter if it's not been specified
    paramDefaults = {'cellSize':30}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,inputBdry)

    # If data need to be interpolated, run that routine
    if sources[srcStartHeads]['method'] == 'feature':
        # Check to see if the data type is point or polygon
        objDescribe = arcpy.Describe(sources[srcStartHeads]['data'])
        if objDescribe.shapeType == 'Point':
            # Filter by attributes and location
            if clean:
                inData = sources[srcStartHeads]['data']
                # If needed
                if params['filter']['filterLoc']:
                    print('Filtering wells by location')
                    inData = filter_by_location(inputDB,pathConfig,params,inData) #returns inData as prepInterpWells
                # If needed
                if params['filter']['filterAttrib']:
                    print('Filtering wells by attribute')
                    inData = filter_by_attributes(inputDB,pathConfig,params,inData) #returns inData as prepInterpWells
            else:
                arcpy.CopyFeatures_management(sources[srcStartHeads]['data'],nameOutWells)

            # Prepare the interpolation fields
            print('Preparing data fields for interpolation')
            prepare_interpolation_fields(inputDB,pathConfig,params,inData)

            # Filter the outliers
            if clean:
                # Here, this filter function operates on the prepare interpolation feature
                if params['filter']['filterOutliers']:
                    print('Filtering wells by spatial outliers in depth to water')
                    filter_outliers(params)

            # Interpolate water levels
            print('Interpolating water levels')
            interpolate_water_levels(inputDB, pathConfig, params, validate)

            # Validate in chosen
            if validate:
                print('Validating the interpolation')
                dfValPts = validate_interpolation(inputDB, pathConfig, params)

        elif objDescribe.shapeType == 'Polygon':
            create_surface_raster(sources[srcStartHeads],nameOutStartHeads)
    else:
        create_surface_raster(sources[srcStartHeads],nameOutStartHeads)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('Starting heads are prepared for the <strong>%s</strong> region'%regionName))


'''
--------------------------------------------------------------------------------
Filter by Location
--------------------------------------------------------------------------------
'''

def filter_by_location(inputDB,pathConfig,params,inputWells):
    """This filter function removes wells that lie inside water bodies, as well as those that lie outside the model domain
    """

    #-------------------------------------------------------------------------------
    # Specify inputs
    #-------------------------------------------------------------------------------
    # Get standardized names or param values
    (srcHydroPolys,nameOutWells) = lhm_frontend.names_frontend(['srcHydroPolys','prepInterpWells'])

    # Get the outputs for this instance
    (sources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Finish preparing input paths
    inputHydroPolys = sources[srcHydroPolys]

    # Specify the config file information
    tempClipName = 'temp_clip_wells'

    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,inputBdry)

    #-------------------------------------------------------------------------------
    # Run the filters
    #-------------------------------------------------------------------------------
    # First, run the include filter
    # Clip the dataset
    arcpy.Clip_analysis(inputWells,inputBdry,tempClipName)

    # Then, run the exclude filter(s)
    # Erase points that fall within polygons
    arcpy.Erase_analysis(tempClipName,inputHydroPolys,nameOutWells)

    # Clean up
    arcpy.Delete_management(tempClipName)

    # Count the number of features remaining
    count = arcpy.GetCount_management(inputWells)[0]
    print('\t%s wells remain after filtering by location'%count)

    # Return the name of this object to the next step
    return nameOutWells

'''
--------------------------------------------------------------------------------
Filter by Attributes
--------------------------------------------------------------------------------
'''
def filter_by_attributes(inputDB,pathConfig,params,inputWells):
    # Define these for convenience
    dateRange = params['filter']['dateRange']
    dtwRange = params['filter']['dtwRange']
    fieldInclude = params['filter']['fieldInclude']
    fieldExclude = params['filter']['fieldExclude']

    # Date field
    fieldDate = params['fields']['date']['name']
    fieldDTW = params['fields']['dtw']['name']

    # Define local names
    tempSelectName = 'temp_select_wells'

    # Get standardized names or param values
    (nameOutWells) = lhm_frontend.names_frontend(['prepInterpWells'])

    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,'')

    #Determine the field designation symbol
    if arcpy.env.workspace[-4:] == ".gdb":
        strFieldOpen = '"'
        strFieldClose = '"'
        strDateSel = 'date'
    else:
        strFieldOpen = '['
        strFieldClose = ']'
        strDateSel = ''

    #-------------------------------------------------------------------------------
    # Select Features
    #-------------------------------------------------------------------------------
    # Copy the input wells to a temporary feature
    if arcpy.Exists(tempSelectName):
        arcpy.Delete_management(tempSelectName)

    if inputWells != nameOutWells:
        arcpy.CopyFeatures_management(inputWells,tempSelectName)
        nameOutWells = inputWells
    else:  
        arcpy.Rename_management(inputWells,tempSelectName)

    # Initialize SQL string for where clause
    whereClause = ''

    # Specifically include features
    for field in fieldInclude:
        whereClause += '('
        for value in fieldInclude[field]:
            whereClause += (strFieldOpen + field + strFieldClose +' = \'' + value + '\' OR ')
        whereClause = whereClause[:-4]
        whereClause += ') AND '

    # Specifically exclude features
    for field in fieldExclude:
        whereClause += '('
        for value in fieldExclude[field]:
            whereClause += (strFieldOpen + field + strFieldClose +' <> \'' + value + '\' OR ')
        whereClause = whereClause[:-4]
        whereClause += ') AND '

    # If Geodatabase
     #Select a date range
    if 'min' in dateRange:
        whereClause += '(' + strFieldOpen + fieldDate + strFieldClose + ' >= ' + strDateSel + ' \'' + dateRange['min'] + '\') AND '
    if 'max' in dateRange:
        whereClause += '(' + strFieldOpen + fieldDate + strFieldClose + ' <= ' + strDateSel + ' \'' + dateRange['max'] + '\') AND '

    # Select a DTW range
    if 'min' in dtwRange:
        whereClause += '(' + strFieldOpen + fieldDTW + strFieldClose + ' >= ' + dtwRange['min'] + ') AND '
    if 'max' in dtwRange:
        whereClause += '(' + strFieldOpen + fieldDTW + strFieldClose + ' <= ' + dtwRange['max'] + ') AND '

    # Remove the final ' AND '
    whereClause = whereClause[:-5]

    # Run the select operation
    arcpy.Select_analysis(tempSelectName,inputWells,whereClause)

    # Count the number of features remaining
    count = arcpy.GetCount_management(inputWells)[0]
    print('\t%s wells remain after filtering by attributes'%count)

    # Delete the temporary feature
    arcpy.Delete_management(tempSelectName)

    # Return the name of this object to the next step
    return inputWells


'''
--------------------------------------------------------------------------------
Prepare Interpolation Fields
--------------------------------------------------------------------------------
'''
def prepare_interpolation_fields(inputDB, pathConfig, params, featureInterp):
    import gis
    raster_vals_to_points = gis.raster_vals_to_points

    # Get the field for the DEM source data
    (srcDEM) = lhm_frontend.names_frontend(['srcDEM'])

    # Get the module names
    (fieldElev,fieldWTElev,fieldDTWm,fieldScreenFrmM,fieldScreenToM,fieldIDNum,fieldIDName,fieldDate) = names_module(\
                ['fieldElev','fieldWTElev','fieldDTW','fieldScreenFrm','fieldScreenTo','fieldIDNum','fieldIDName','fieldDate'])
    
    # Get the outputs for this instance
    (sources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Get the input DEM
    inputDEM = sources[srcDEM]

    # List the input fields
    fieldDTW = params['fields']['dtw']['name']
    convDTW = params['fields']['dtw']['mult']
    fieldScreenFrm = params['fields']['screen_top']['name']
    convScreenFrm = params['fields']['screen_top']['mult']
    fieldScreenTo = params['fields']['screen_bot']['name']
    convScreenTo = params['fields']['screen_bot']['mult']
    fieldIDIn = params['fields']['id']['name']
    fieldDateIn = params['fields']['date']['name']

    # Types of those fields
    typeFloat = 'FLOAT'
    typeIDNum = 'LONG'
    typeName = 'TEXT'
    typeDate = 'DATE'

    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,inputBdry)

    #-------------------------------------------------------------------------------
    # Add DEM points to the shapefile, and calculate WT elevation field, convert other units
    #-------------------------------------------------------------------------------
    # Add a numeric ID field, calculated from the FID
    fieldOID = arcpy.Describe(featureInterp).OIDFieldName
    arcpy.AddField_management(featureInterp,fieldIDNum,typeIDNum)
    arcpy.CalculateField_management(featureInterp,fieldIDNum,'!%s!'%fieldOID,'PYTHON')
    
    # Add a name ID field, calculated from the provided ID field
    arcpy.AddField_management(featureInterp,fieldIDName,typeName)
    arcpy.CalculateField_management(featureInterp,fieldIDName,'!%s!'%fieldIDIn,'PYTHON')

    # Add a construction date field
    arcpy.AddField_management(featureInterp,fieldDate,typeDate)
    arcpy.CalculateField_management(featureInterp,fieldDate,'!%s!'%fieldDateIn,'PYTHON')

    # Add the DEM points to the shapefile
    raster_vals_to_points(featureInterp,inputDEM,fieldElev,interpolate=True)

    # Add a new WT elevation field
    arcpy.AddField_management(featureInterp,fieldWTElev,typeFloat)

    # Calculate the value of this field
    calcExpression = '!' + fieldElev+ '! - !' + fieldDTW + '! * ' + str(convDTW)
    arcpy.CalculateField_management(featureInterp,fieldWTElev,calcExpression,'PYTHON')

    # Convert screen depths
    arcpy.AddField_management(featureInterp,fieldScreenFrmM,typeFloat)
    arcpy.AddField_management(featureInterp,fieldScreenToM,typeFloat)

    codeBlock = """def float_handle_empty(inVal):
                if inVal in (u' ', ' ' ,u'', ''):
                    return 0
                elif inVal is None:
                    return 0
                else:
                   return float(inVal)"""

    calcExpression = 'float_handle_empty(!' + fieldScreenFrm + '!) * ' + str(convScreenFrm)
    arcpy.CalculateField_management(featureInterp,fieldScreenFrmM,calcExpression,'PYTHON',codeBlock)

    calcExpression = 'float_handle_empty(!' + fieldScreenTo + '!) * ' + str(convScreenTo)
    arcpy.CalculateField_management(featureInterp,fieldScreenToM,calcExpression,'PYTHON',codeBlock)

    # Convert DTW
    arcpy.AddField_management(featureInterp,fieldDTWm,typeFloat)

    calcExpression = 'float_handle_empty(!' + fieldDTW + '!) * ' + str(convDTW)
    arcpy.CalculateField_management(featureInterp,fieldDTWm,calcExpression,'PYTHON',codeBlock)


'''
--------------------------------------------------------------------------------
Filter Outliers
--------------------------------------------------------------------------------
'''
def filter_outliers(params):
    import arcpy
    import gis
    iterative_spatial_outlier_removal = gis.iterative_spatial_outlier_removal

    # Get module standardized names
    (fieldWTElev) = names_module(['fieldWTElev'])
    (featureInterp) = lhm_frontend.names_frontend(['prepInterpWells'])

    # Separate these out for convenience
    outlierParams = params['filter']
    outlierParams['fieldData'] = fieldWTElev

    # Run the iterative spatial outlier removal helper function
    iterative_spatial_outlier_removal(featureInterp,featureInterp,outlierParams)

    # Count the number of features remaining
    count = arcpy.GetCount_management(featureInterp)[0]
    print('\t%s wells remain after filtering spatial outliers'%count)


'''
--------------------------------------------------------------------------------
Interpolate Water Levels
--------------------------------------------------------------------------------
'''
def interpolate_water_levels(inputDB, pathConfig, params, validate):
    # -*- coding: utf-8 -*-
    """
    This function interpolates water levels in wells, including surface water features in the interpolation.

    Created on Tue Jul 19 13:31:53 2016

    @author: Kendal30
    """

    import random, numpy
    import gis
    raster_vals_to_points = gis.raster_vals_to_points

    #-------------------------------------------------------------------------------
    # Specify inputs
    #-------------------------------------------------------------------------------
    # Get standardized names or param values
    (nameInBdry,srcDEM,srcHydroLines,nameInterpWells,nameOutKrig) = \
            lhm_frontend.names_frontend(['prepBdryBuff','srcDEM','srcHydroLines','prepInterpWells','prepStartHeads'])

    # Get module standardized names
    (fieldWTElev,fieldVal,tempKrigRound1) = names_module(['fieldWTElev','fieldVal','tempKrigRound1'])

    # Get the outputs for this instance
    (sources,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig)

    # Finish preparing input paths
    inputBdry = inputDB + '/' + nameInBdry
    inputDEM = sources[srcDEM]
    inputHydroLines = sources[srcHydroLines]

    # Specify local temporary names
    tempKrigWells = 'temp_krig_wells'
    tempBdryRaster = 'temp_boundary_raster'
    tempMaskFeature = 'temp_mask_wt_above_surf'
    tempHydroClip = 'temp_hydro_lines_clip'
    tempHydroPoints = 'temp_hydro_points'
    tempHydroPointsKrig = 'temp_hydro_points_krig'
    tempKrigPointsMerge = 'temp_krig_merge_points'
    tempKrigRound2 = 'temp_krig_round_2'

    # Specify local layer names
    layerWellsSelect = 'temp_select_layer'

    # Specify local cleanup list
    cleanList = [tempBdryRaster, tempMaskFeature, tempHydroClip, tempHydroPoints, tempHydroPointsKrig,\
                 tempKrigPointsMerge,tempKrigRound2,layerWellsSelect]
    if not validate:
        cleanList.append(tempKrigRound1)

    #-------------------------------------------------------------------------------
    # Import helper module, set environment settings
    #-------------------------------------------------------------------------------
    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,inputBdry)
    arcpy.CheckOutExtension("GeoStats")

    ## Create the observations dataset
    #-------------------------------------------------------------------------------
    # Add a field for for validation wells, 0/1
    #-------------------------------------------------------------------------------
    # Add field to indicate validation data
    arcpy.AddField_management(nameInterpWells,fieldVal,'SHORT')
    arcpy.CalculateField_management(nameInterpWells,fieldVal,'0','PYTHON') #default value is 0 (use for interp)

    # Select random OIDs
    objectIDs = [str(oid[0]) for oid in arcpy.da.SearchCursor(nameInterpWells, ['OID@'])] #get the list of OIDs as strings
    numPtsTotal = len(objectIDs)
    numPtsSample = int(numPtsTotal*params['val']['fraction'])
    randomIDs = random.sample(objectIDs,numPtsSample)

    # Create the random selection
    fieldOID = arcpy.Describe(nameInterpWells).OIDFieldName
    selectQuery = '"{0}" IN ({1})'.format(fieldOID, ','.join(randomIDs))#build the selection query
    arcpy.MakeFeatureLayer_management(nameInterpWells,layerWellsSelect)
    arcpy.SelectLayerByAttribute_management(layerWellsSelect,'NEW_SELECTION',selectQuery) #select the random points

    # Calculate the validation field
    arcpy.CalculateField_management(nameInterpWells,fieldVal,'1','PYTHON')

    # Switch to the wells for kriging, copy to a new temp feature
    arcpy.SelectLayerByAttribute_management(layerWellsSelect,'SWITCH_SELECTION') #switch that selection to remaining points
    arcpy.CopyFeatures_management(layerWellsSelect,tempKrigWells)
    arcpy.Delete_management(layerWellsSelect)

    # Run the kriging steps
    # -------------------------------------------------------------------------------
    # Kriging steps
    # -------------------------------------------------------------------------------
    # Run initial krig
    arcpy.EmpiricalBayesianKriging_ga(tempKrigWells,fieldWTElev,out_raster=tempKrigRound1,\
        semivariogram_model_type='EXPONENTIAL',transformation_type='EMPIRICAL')
    rasterKrigingInit = arcpy.Raster(tempKrigRound1)
    
    # Create the mask for the interpolation boundary domain
    arcpy.PolygonToRaster_conversion(inputBdry,'OBJECTID',tempBdryRaster)
    rasterBoundary = arcpy.Raster(tempBdryRaster)
    rasterBoundary = arcpy.sa.ApplyEnvironment(rasterBoundary)

    # # Create mask for areas where WT>DEM
    rasterDEM = arcpy.Raster(inputDEM)
    rasterMask = arcpy.sa.Con(rasterKrigingInit > rasterDEM,1) * rasterBoundary
    arcpy.RasterToPolygon_conversion(rasterMask,tempMaskFeature,'NO_SIMPLIFY')

    # Clip hydro features
    arcpy.Clip_analysis(inputHydroLines,tempMaskFeature,tempHydroClip)

    # Convert hydro features to points
    arcpy.FeatureVerticesToPoints_management(tempHydroClip,tempHydroPoints)

    # Thin as needed, using random selection as above
    areaBoundary = numpy.sum([area[0] for area in arcpy.da.SearchCursor(inputBdry,['SHAPE@AREA'])]) #determine total boundary area
    areaMask = numpy.sum([area[0] for area in arcpy.da.SearchCursor(tempMaskFeature,['SHAPE@AREA'])]) #determine area of mask
    hydroObjectIDs = [str(oid[0]) for oid in arcpy.da.SearchCursor(tempHydroPoints, ['OID@'])]
    numHydroPts = len(hydroObjectIDs)
    numHydroPtsMax = numPtsTotal * areaMask / areaBoundary
    if numHydroPts > numHydroPtsMax:
        hydroRandomIDs = random.sample(hydroObjectIDs,int(numHydroPtsMax))
        fieldOID = arcpy.Describe(tempHydroPoints).OIDFieldName
        selectQuery = '"{0}" IN ({1})'.format(fieldOID, ','.join(hydroRandomIDs))#build the selection query
        arcpy.MakeFeatureLayer_management(tempHydroPoints,layerWellsSelect)
        arcpy.SelectLayerByAttribute_management(layerWellsSelect,'NEW_SELECTION',selectQuery) #select the random points
        arcpy.CopyFeatures_management(layerWellsSelect,tempHydroPointsKrig) #export to a feautre
    else:
        arcpy.Copy_management(tempHydroPoints,tempHydroPointsKrig)

    # Assign DEM elevations to the points using raster vals to points
    raster_vals_to_points(tempHydroPointsKrig,inputDEM,fieldWTElev,interpolate=True)

    # Create composite dataset with hydro
    arcpy.Merge_management([tempKrigWells,tempHydroPointsKrig],tempKrigPointsMerge)

    # Re-krig
    arcpy.EmpiricalBayesianKriging_ga(tempKrigPointsMerge,fieldWTElev,out_raster=tempKrigRound2,\
        semivariogram_model_type='EXPONENTIAL',transformation_type='EMPIRICAL')
    rasterKrigingSecond = arcpy.Raster(tempKrigRound2)

    # Restrict to the surface value
    rasterKrigingClean = arcpy.sa.Con(rasterKrigingSecond > rasterDEM, rasterDEM, rasterKrigingSecond)
    rasterKrigingClean.save(nameOutKrig)

    ## Finally, clean up
    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    # Delete the raster objects
    del rasterDEM, rasterMask, rasterKrigingInit, rasterKrigingSecond, rasterKrigingClean

    # Delete temporary rasters and layers
    [arcpy.Delete_management(delTemp) for delTemp in cleanList]


'''
--------------------------------------------------------------------------------
Validation Function
--------------------------------------------------------------------------------
'''
def validate_interpolation(inputDB,pathConfig, params):
    """This script calculates and plots validation data on the interpolated water table.
    It writes out interpolation_validation.csv to the Import_Working folder
    """
    import pandas, os
    import gis
    raster_vals_to_points = gis.raster_vals_to_points

    #-------------------------------------------------------------------------------
    # Specify inputs
    #-------------------------------------------------------------------------------
    # Get standardized names or param values
    (nameInBdry,nameWorkingDir,nameInterpWells,nameOutKrig) = \
            lhm_frontend.names_frontend(['prepBdryBuff','dirWorking','prepInterpWells','prepStartHeads'])

    # Get module standardized names
    (fieldWTElev,fieldVal,tempKrigRound1) = names_module(['fieldWTElev','fieldVal','tempKrigRound1'])

    # Get the output location and name for the validation .csv
    outDir = os.path.join(os.path.split(inputDB)[0],nameWorkingDir)
    outName = 'interpolation_validation.csv'
    outFile = os.path.join(outDir,outName)

    # Finish preparing input paths
    inputBdry = inputDB + '/' + nameInBdry

    # Specify local names
    tempValWells = 'temp_validate_wells'
    layerWellsSelect = 'temp_select_layer'

    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,inputBdry)

    #-------------------------------------------------------------------------------
    # Validation steps
    #-------------------------------------------------------------------------------
    # Select the wells that are validation points, export to a new temporary feature
    selectQuery = '"%s" = 1'%fieldVal
    arcpy.MakeFeatureLayer_management(nameInterpWells,layerWellsSelect)
    arcpy.SelectLayerByAttribute_management(layerWellsSelect,'NEW_SELECTION',selectQuery) #select the random points
    arcpy.CopyFeatures_management(layerWellsSelect,tempValWells)

    # Add raster values to points for the validation points
    raster_vals_to_points(tempValWells,tempKrigRound1,'krig_elev_init',interpolate=True)
    raster_vals_to_points(tempValWells,nameOutKrig,'krig_elev_final',interpolate=True)

    # Bring this in as a numpy array
    arrayValPts = arcpy.da.TableToNumPyArray(tempValWells,['OBJECTID',fieldWTElev,\
        'krig_elev_init','krig_elev_final'],skip_nulls=True)
    dfValPts = pandas.DataFrame(arrayValPts)

    # Calculated needed fields
    dfValPts['resid_init'] = dfValPts['krig_elev_init'] - dfValPts[fieldWTElev]
    dfValPts['resid_final'] = dfValPts['krig_elev_final'] - dfValPts[fieldWTElev]

    # Write out the dataframe
    dfValPts.to_csv(outFile)

    # Clean up
    arcpy.Delete_management(tempValWells)
    arcpy.Delete_management(layerWellsSelect)
    arcpy.Delete_management(tempKrigRound1) # clean up the intermediate kriging layer here


'''
--------------------------------------------------------------------------------
Observations .csv file creation
--------------------------------------------------------------------------------
'''
def prep_observations(inputDB, pathConfig, subModel, params, printResult=True):
    '''
    This observations routine uses drinking water wells prepared by the drinking water 
    wells interpolation routine.

    The params dictionary requires:
     - "dateRange" of format yyyy-mm-dd, two-item list, first is minimum, second maximum
    Optional parameters are:
     - "edgeDist", default value is -100, specify the distance to buffer around
       the edge of the ibound array for selecting points, negative values
       indicate a buffer inside the feature
     - "weight", default value is 1, specify the datapoint weight. This would
       be used for calibrating parameter values in the model
     - "idStart", default value is 1, only set other than 1 if you know you
       have a reason to (i.e. another similar datatype as a different set)
     - "timeZone", default value is 0, really shouldn't matter much for daily streamflows
    '''


    import arcpy, os, pandas

    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Dataset specific inputs
    #-------------------------------------------------------------------------------
    # Get standardized names or param values
    (nameCsv,dirObs,nameIbound,dirGndwater,dirModLay,dirSubmodLay,gridExt,inName,outName,setType) = lhm_frontend.names_frontend(\
        ['obsCsvDrinking','dirObs','modGndIbound','dirGndwater','dirModelLayers',\
        'dirSubmodelLayers','outExtRaster','prepInterpWells','obsFeatDrinking',\
        'obsFieldPoints'])
    (fieldIDNum,fieldIDName,fieldWTElev,fieldTime,fieldScreenTopDepth,fieldScreenBotDepth) = names_module(\
        ['fieldIDNum','fieldIDName','fieldWTElev','fieldDate','fieldScreenFrm','fieldScreenTo'])

    # Prepare the other parameters if they've not been specified
    if 'edgeDist' not in params.keys():
        params['edgeDist'] = -100
    if 'weight' not in params.keys():
        params['weight'] = 1
    if 'idStart' not in params.keys():
        params['idStart'] = 1
    if 'timeZone' not in params.keys():
        params['timeZone'] = 0

    # Get the path to the model/submodel ibound grid
    if not subModel:
        subDirGndwater = '%s/%s'%(dirModLay,dirGndwater)
    else:
        subDirGndwater = '%s/%s/%s'%(dirSubmodLay,subModel,dirGndwater)
        outName = '%s_%s'%(outName,subModel)
    ibound = os.path.join(subDirGndwater,'%s%s'%(nameIbound,gridExt))
    ibound = os.path.join(os.path.split(inputDB)[0],ibound)

    if not os.path.exists(ibound): # may not have already been collapsed
        ibound = os.path.join(subDirGndwater,'%s_lay1%s'%(nameIbound,gridExt))
        ibound = os.path.join(os.path.split(inputDB)[0],ibound)

    # Commented out because I want a grid of drinking water wells, but don't know at the moment
    # if it should be the surface or groundwater grid. For now, leave alone.
    # # Get the model grid shapefile
    # gridShape = os.path.join(dirSurface,'%s%s'%(nameIbound,extFeature))

    # Set the output directory
    outDir = os.path.join(os.path.split(os.path.split(ibound)[0])[0],dirObs)

    #-------------------------------------------------------------------------------
    # Set internal variables
    #-------------------------------------------------------------------------------
    # Specify the name of this observations set, will be used in the table file name
    # TODO: get this from sources instead
    setName = 'drinking_water_wells'
    setVar = 'head'
    setAgg = 'mean'
    setUnits = 'm'

    # Specify fields to import from the obs feature in the prep database
    listImport = [fieldIDNum,fieldIDName,fieldScreenTopDepth,fieldScreenBotDepth,fieldTime,fieldWTElev,\
        'POINT_X','POINT_Y']

    # Temporary names, shouldn't require changing
    tempObsDepthTime = 'temp_wells_obs_time'
    tempObsSpace = 'temp_wells_obs_space'
    tempBoundary = 'temp_boundary'
    tempBdryBuffer = 'temp_bdry_buffer'

    #-------------------------------------------------------------------------------
    # Prepare the environment
    #-------------------------------------------------------------------------------
    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment('',inputDB,'')

    import obs 

    #-------------------------------------------------------------------------------
    # Select only the good records from the input table
    #-------------------------------------------------------------------------------
    # Select a date range
    whereClause = '"' + fieldTime + '" >= date \'' + params['dateRange'][0] + '\' AND ' +\
        '"' + fieldTime + '" <= date \'' + params['dateRange'][1] + '\''

    # Select the records into a new temporary feature
    arcpy.Select_analysis(inName,tempObsDepthTime,whereClause)

    # Create a temporary shapefile with only points within the active boundary extracted
    rasterIbound = arcpy.Raster(ibound)
    rasterIboundNoData = arcpy.sa.Con(rasterIbound>0,1)
    arcpy.RasterToPolygon_conversion(rasterIboundNoData,tempBoundary,'NO_SIMPLIFY')
    arcpy.Buffer_analysis(tempBoundary,tempBdryBuffer,params['edgeDist'])

    # Select the records into another shapefile
    arcpy.Clip_analysis(tempObsDepthTime,tempBdryBuffer,tempObsSpace)

    # Compute a new numeric ID field
    arcpy.AddField_management(tempObsSpace,fieldIDNum,'LONG')
    fieldOID = arcpy.Describe(tempObsSpace).OIDFieldName
    arcpy.CalculateField_management(tempObsSpace,fieldIDNum,'!{0:s}! + {1:d}'.format(fieldOID,params['idStart']),'PYTHON')

    # Add x and y locations, creates POINT_X and POINT_Y
    arcpy.AddXY_management(tempObsSpace)

    # Save output, delete feature if it already exists
    if arcpy.Exists(outName):
        arcpy.Delete_management(outName)
    arcpy.Rename_management(tempObsSpace,outName)

    # # Call the grid creation function
    # outRasterName = outNameTemplate%(thisDataset,extRaster)
    # outRaster = os.path.join(outDir,outRasterName)
    # dfFeatures = obs.create_grid(dfFeatures, gridShape, ibound, arcpy, outRaster)

    #-------------------------------------------------------------------------------
    # Read in the table, comput new fields, and write out as .csv
    #-------------------------------------------------------------------------------
    dfObs = pandas.DataFrame(arcpy.da.TableToNumPyArray(outName,listImport))

    # Rename columns
    dfObs = dfObs.rename(columns={fieldIDNum:'id', fieldIDName:'loc_id', 'POINT_X':'x', 'POINT_Y':'y',\
        fieldTime:'time_start', fieldWTElev:'value'})

    # Add the observation type
    dfObs.loc[:,'type'] = setType

    # Add an empty loc_name field, not useful for wells
    dfObs.loc[:,'loc_name'] = ''

    # Add set and variable
    dfObs.loc[:,'set_name'] = setName
    dfObs.loc[:,'variable'] = setVar

    # Calculate depth
    dfObs.loc[:,'z_top'] = -dfObs[fieldScreenTopDepth]
    dfObs.loc[:,'z_bot'] = -dfObs[fieldScreenBotDepth]

    # Calculate time start and time end
    dfObs.loc[:,'time_start'] = pandas.to_datetime(dfObs['time_start'],format='%Y-%m-%d %H:%M:%S')
    dfObs.loc[:,'time_end'] = dfObs['time_start'] + pandas.Timedelta(days=1,minutes=-1)
    dfObs.loc[:,'time_zone'] = params['timeZone']
    
    # Set the dataset aggregation, units, and weight field
    dfObs.loc[:,'aggregate'] = setAgg
    dfObs.loc[:,'units'] = setUnits
    dfObs.loc[:,'weight'] = params['weight']

    # Set the spatial and spatial_id fields
    dfObs.loc[:,'grid'] = 'n/a'
    dfObs.loc[:,'grid_id'] = 0

    # Format times for writing to csv
    dfObs.loc[:,'time_start'] = dfObs['time_start'].dt.strftime('%Y-%m-%d %H:%M:%S')
    dfObs.loc[:,'time_end'] = dfObs['time_end'].dt.strftime('%Y-%m-%d %H:%M:%S')

    # Rearrange columns into final order
    dfTemplate = obs.create_obs_table_df()
    listCols = dfTemplate.columns
    dfObs = dfObs[listCols]

    # Write the observations table
    dfObs.to_csv(os.path.join(outDir,nameCsv),index=False)

    # Log this
    lhm_frontend.log_lhm_output(nameCsv, outDir, inputDB)

    #-------------------------------------------------------------------------------
    # Clean up
    #-------------------------------------------------------------------------------
    # Delete temporary raster objects
    del rasterIbound, rasterIboundNoData

    # First list variables/objects/files to delete
    delLayers = [thisVar for thisVar in dir() if thisVar[0:5]=='layer']
    delTemps = [thisVar for thisVar in dir() if thisVar[0:4]=='temp']

    # Layers
    for delLayer in delLayers:
        arcpy.Delete_management(eval(delLayer))

    # Delete temporary rasters and features
    for delTemp in delTemps:
        arcpy.Delete_management(eval(delTemp))

    # Print result:
    if printResult:
        display(HTML('Drinking water well observations finished processing'))