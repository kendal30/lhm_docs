def nwisweb_levels_daily(siteNum,dateRange,params,returnTypes=False):
    """This function fetches daily NWISWEB data from the USGS
    Descriptions of Input Variables:
    siteNum:    the site code as a string
    dateRange:  a 2-item list containing the date range to fetch, for all dates just
                enter a blank list [].  The date should be of format "yyyy-mm-dd"
    params:     a list of strings containing the names of parameters
                to fetch.  Currently supported: "dtw", "level_navd88", "level_ngvd29".
                Aternately, any valid parameter code can be used input as a number.
    returnTypes:a True/False option to return the dataType fields from the original query

    Descriptions of Output Variables:
    Output is a dataframe with the site number, times, and params/stats requested
    """

    from datetime import datetime, timedelta
    import pandas
    from numpy import ceil, floor
    import warnings

    # Specify some constants
    maxRequest = 35000
    dateFormat = '%Y-%m-%d'

    # First, get the parameter strings and codes
    (paramsString,paramsCode) = param_codes(params)

    # Then, build the query string, without dates
    queryURL = build_query(siteNum,'gw',paramsString)

    # If the date range is blank, retieve date range from the inventory
    # Otherwise, parse dateRange
    if len(dateRange) == 0:
        siteInv = nwisweb_inventory(siteNum,'gw',params,False)
        beginDate = siteInv['begin_date'][0]
        endDate = siteInv['end_date'][0]
    else:
        beginDate = datetime.strptime(dateRange[0],dateFormat)
        endDate = datetime.strptime(dateRange[1],dateFormat)

    # Test to see if the number of dates requested exceeds the 35,000 limit set
    # by the NWISWEB interface
    delta = endDate - beginDate
    numChunks = floor(delta.days/maxRequest) + 1
    daysChunk = timedelta(days=ceil(delta.days/numChunks))

    #  Fetch the data via chunks
    chunkBegin = beginDate
    for m in range(0,int(numChunks)):
        if m < numChunks-1:
            chunkEnd = chunkBegin + daysChunk
        else:
            chunkEnd = endDate

        # Fetch the data for this chunk
        try:
            (thisData,dataTypes) = fetch_data(queryURL,beginDate.strftime(dateFormat),chunkEnd.strftime(dateFormat))
            if m == 0:
                dfData = thisData.copy()
            else:
                dfData = pandas.concat([dfData,thisData],axis=0)
        except:
            warnings.warn('Site '+ siteNum + ' failed to download',UserWarning)
            return False

        # Update chunkBegin
        chunkBegin = chunkEnd + timedelta(1)

    #  Update the datatype for groundwater level
    dfData['lev_va'] = dfData['lev_va'].astype(float)

    if returnTypes:
        return (dfData, dataTypes)
    else:
        return dfData


def nwisweb_flow_daily(siteNum,dateRange,params,stats=['mean'],returnTypes=False):
    """This function fetches daily NWISWEB data from the USGS
    Descriptions of Input Variables:
    siteNum:    the site code as a string
    dateRange:  a 2-item list containing the dates to fetch, for all dates just
                enter a blank list [].  The date should be of format 'yyyy-mm-dd'
    params:     a list of strings containing the names of parameters
                to fetch.  Currently supported: 'discharge','gauge_height',
                and 'temperature'. Aternately, any valid parameter code can be used
                input as a number.
    stats:      a list of strings containing the names of daily statistics, currently
                supported are 'mean', 'max', and 'min'. This function will only
                return one statistics value per requested parameter currently.
                Alternately, any valid statistics code may be input numerically.
    returnTypes:a True/False option to return the dataType fields from the original query

    Descriptions of Output Variables:
    Output is a dataframe with the site number, times, and params/stats requested
    """

    from datetime import datetime, timedelta
    import pandas
    from numpy import ceil, floor
    import warnings

    # Specify some constants
    maxRequest = 35000
    dateFormat = '%Y-%m-%d'

    # First, get the parameter strings and codes
    (paramsString,paramsCode) = param_codes(params)

    # Second, get the statistcs strings and codes
    (statsString,statsCodes) = stats_codes(stats)

    # Then, build the query string, without dates
    queryURL = build_query(siteNum,'sw',paramsString,statsString)

    # If the date range is blank, retieve date range from the inventory
    # Otherwise, parse dateRange
    if len(dateRange) == 0:
        siteInv = nwisweb_inventory(siteNum,'sw',params,False)
        beginDate = siteInv['begin_date'][0]
        endDate = siteInv['end_date'][0]
    else:
        beginDate = datetime.strptime(dateRange[0],dateFormat)
        endDate = datetime.strptime(dateRange[1],dateFormat)

    # Test to see if the number of dates requested exceeds the 35,000 limit set
    # by the NWISWEB interface
    delta = endDate - beginDate
    numChunks = floor(delta.days/maxRequest) + 1
    daysChunk = timedelta(days=ceil(delta.days/numChunks))

    #  Fetch the data via chunks
    chunkBegin = beginDate
    for m in range(0,int(numChunks)):
        if m < numChunks-1:
            chunkEnd = chunkBegin + daysChunk
        else:
            chunkEnd = endDate

        # Fetch the data for this chunk
        try:
            (thisData,dataTypes) = fetch_data(queryURL,chunkBegin.strftime(dateFormat),chunkEnd.strftime(dateFormat))
            if m == 0:
                dfData = thisData.copy()
            else:
                dfData = pandas.concat([dfData,thisData],axis=0)
        except:
            warnings.warn('Site '+ siteNum + ' failed to download',UserWarning)
            return False

        # Update chunkBegin
        chunkBegin = chunkEnd + timedelta(1)

    # Rename the columns to be more helpful
    headers = list(dfData.columns)

    for (ind,header) in enumerate(headers):
        headerParse = header.split('_')
        if len(headerParse)>=3:
            headers[ind] = params[paramsCode.index(headerParse[1])] + '_' + \
                stats[statsCodes.index(headerParse[2])]
        if len(headerParse)==4:
            headers[ind] += '_code'
    # Now, rename the columns
    dfData.columns = headers

    if returnTypes:
        return (dfData,dataTypes)
    else:
        return dfData


def nwisweb_inventory(sitesOrState,dataType,params,expandedOption=False,returnTypes=False):
    """This function fetches the inventory of a list of stations from NWISWEB from
    the USGS or from a two-letter state code
    Description of Input Variables:
    sitesOrState: List of site numbers as strings, single site number as string, or single state string
    dataType:   either "sw" or "gw"
    params:      Name of the parameters to return station inventories. Currently
                supported: "discharge","gauge_height", and "temperature". Alternately
                a numeric parameter code may be used. Can be a string or an array of strings
    expandedOption: optional, false (default) or true, returns additional information about the sites
    returnTypes:a True/False option to return the dataType fields from the original query

    Description of Output Variables:
    Output is a single dataframe
    """

    # Test to see what mode we are operating in, and properly format input
    if isinstance(sitesOrState,str):
        if len(sitesOrState) == 2:
            invType = 'state'
        else:
            sitesOrState =[sitesOrState]
            invType = 'sites'
    elif isinstance(sitesOrState,list):
        invType = 'sites'
    else:
        raise(TypeError,'sitesOrState must be a string or a list of strings')

    # Now for the input parameters
    if isinstance(params,str):
        params = [params]
    elif isinstance(params,list):
        pass
    else:
        raise(TypeError,'param must be a string or a list of strings')

    # Finally, assert dataType
    if dataType not in ['sw','gw']:
        raise(ValueError,'dataType must be either sw or gw')

    # Do the actual retrievals
    # Get the pameter string and codes
    (paramsString,paramsCode) = param_codes(params)

    # Build the query string
    queryURL = build_query_inv(sitesOrState,dataType,invType)

    # Fetch the inventory
    (dfInventory,dataTypes) = fetch_inventory(queryURL,paramsCode)

    # If the expanded option is desired, run that
    if expandedOption:
        # Build the query string
        queryURL = build_query_inv(sitesOrState,dataType,invType,expandedOption)

        # Fetch the inventory
        dfExpanded = fetch_inventory(queryURL)[0]

        # Delete duplicate columns
        fieldsInventory = dfInventory.columns
        fieldsExpanded = dfExpanded.columns
        fieldsDup = [thisField for thisField in fieldsExpanded if (thisField in fieldsInventory)]
        fieldsDup = [thisField for thisField in fieldsDup if (thisField not in ['site_no'])]
        dfExpanded = dfExpanded.drop(columns=fieldsDup)

        # Join
        dfInventory = dfInventory.merge(dfExpanded,how='left',on='site_no')
    
    if returnTypes:
        return (dfInventory,dataTypes)
    else:
        return dfInventory


'''
#------------------------------------------------------------------------------
Lower Level Functions
#------------------------------------------------------------------------------
'''

def fetch_data(queryURL,beginDate,endDate):
    import urllib, re, gzip, pandas

    # First, build the complete query string
    queryURL = '&'.join([queryURL,'startDT='+beginDate,'endDT='+endDate])

    # Now, fetch the data
    request = urllib.request.Request(queryURL,headers={'Accept-Encoding':'gzip, compress'})
    query = urllib.request.urlopen(request).read()
    queryString = gzip.decompress(query).decode("utf-8")

    # Remove the file commented headerlines
    split = re.split('#\n',queryString)
    data = split[-1]

    # Replace all status code values with ''
    data = decode_data(data)

    # Split the file into lines
    lines = re.split('\n',data)

    if lines[0].find('No sites/data')==0: # search did not find any data
        dfData = False
    else:
        # Interpret the column headers
        headers = re.split('\t',lines[0])

        # Get the column datatypes
        dataTypes = re.split('\t',lines[1])

        # Build the regular expression
        regex = ''
        for m in range(0,len(headers)-1):
            regex += '([^\t]*)\t'
        regex += '([^\n]*)\n'

        dataArray = re.findall(regex,'\n'.join(lines[3:]))

        # Create the dataframe
        dfData = pandas.DataFrame(dataArray,columns=headers)

        # Fix the types
        for (ind,thisType) in enumerate(dataTypes):
            if 'd' in thisType:
                dfData.iloc[:,ind] = pandas.to_datetime(dfData.iloc[:,ind])
            elif 'n' in thisType:
                dfData.iloc[:,ind] = dfData.iloc[:,ind].astype(float)

    return (dfData,dataTypes)


def fetch_inventory(queryURL,paramsCode=None):
    """
    Input is queryURL, a properly formed query to the USGS REST interfaces

    Optionally, paramsCode may be

    Output is an array of arrays of inventory outputs, along with the headers
    and data types
    """

    import urllib, re, gzip, pandas

    # Fetch the data
    request = urllib.request.Request(queryURL,headers={'Accept-Encoding':'gzip, compress'})
    query = urllib.request.urlopen(request).read()
    queryString = gzip.decompress(query).decode("utf-8")

    # Now, find the total number of headerlines, first entry should be blank,
    # last contains all remaining lines
    split = re.split('#\n',queryString)
    data = split[-1]

    # Break this into lines
    lines = re.split('\n',data)

    if lines[0].find('No sites/data')==0: # search did not find any data
        dfInventory = False
    else:
        # Interpret the column headers
        headers = re.split('\t',lines[0])

        # Get the column datatypes
        dataTypes = re.split('\t',lines[1])

        # Build the regular expression
        regexStr = ''
        for m in range(0,len(headers)-1):
            regexStr += '([^\t]*)\t'
        regexStr += '([^\n]*)\n'

        inventory = re.findall(regexStr,'\n'.join(lines[2:]))

        # Create the dataframe
        dfInventory = pandas.DataFrame(inventory,columns=headers)

        # Fix the types
        for (ind,thisType) in enumerate(dataTypes):
            if 'd' in thisType:
                dfInventory.iloc[:,ind] = pandas.to_datetime(dfInventory.iloc[:,ind])
            elif 'n' in thisType:
                dfInventory.iloc[:,ind] = dfInventory.iloc[:,ind].astype(float)

        if paramsCode is not None:
            # Limit to only the observed paramsCode
            dfInventory = dfInventory.loc[dfInventory['parm_cd'].isin(paramsCode),:].copy()

    return (dfInventory,dataTypes)


def build_query(siteNum,dataType,paramsString,statsString=None):
    """Inputs are:
     - siteNum: must be an array of strings
     - dataType: string, supported arguments for dataType are "gw" and "sw"
     - paramsString: string output from the param_codes function
     - statsString: optional, string output from the stats_codes function
    """

    # Build the sitesString string
    sitesString = 'sites=' + siteNum

    # Establish the base url and format string
    if dataType == 'gw':
        baseURL = 'https://waterservices.usgs.gov/nwis/gwlevels?'
    elif dataType == 'sw':
        baseURL = 'https://waterservices.usgs.gov/nwis/dv?'
    format = 'format=rdb'

    # Now, composite the query string
    if statsString is None:
        queryURL = '&'.join((baseURL,sitesString,format,paramsString))
    else:
        queryURL = '&'.join((baseURL,sitesString,format,paramsString,statsString))

    return queryURL


def build_query_inv(sitesOrState,dataType,invType,expandedOption=False):
    """Inputs are:
     - sitesOrState: either an array of strings of site numbers,
        or state code string
     - dataType: string, supported arguments for dataType are "gw" and "sw"
     - invType: string, supported arguments for invType are "sites" and "state"
     - expandedOption: defaults False, whether to get the expanded inventory
    """

    # First, establish the base url
    baseURL = 'https://waterservices.usgs.gov/nwis/site/?format=rdb'

    # Build the site numString
    if invType == 'sites':
        sitesString = 'sites=' + ','.join(sitesOrState)
    elif invType == 'state':
        sitesString = 'stateCd=' + sitesOrState

    # Determine the data type to get inventory for
    if dataType == "sw":
        dataString = "dv"
    elif dataType == "gw":
        dataString = "gw"

    # Other options, and composite the queryURL
    if not expandedOption:
        option1 = 'seriesCatalogOutput=true'
        option2 = 'outputDataTypeCd=' + dataString
        option3 = 'hasDataTypeCd=' + dataString
        queryURL = '&'.join((baseURL,sitesString,option1,option2,option3))
    elif expandedOption:
        option1 = 'hasDataTypeCd=' + dataString
        option2 = 'siteOutput=expanded'
        queryURL = '&'.join((baseURL,sitesString,option1,option2))

    return queryURL


def param_codes(params):
    """
    Input params must be an array of strings can use both parameter names (see supported below),
    or 5-digit parameter codes

    Param codes depend on whether gw or sw data are being queried

    For gw, supported parameter names are: "dtw", "level_navd88", "level_ngvd29"
    For sw, supported parameter names are: "temperature", "discharge", "gauge_height"
    """
    codeList = {'dtw':'72019','level_navd88':'62611','level_ngvd29':'62610',\
                'temperature':'00010','discharge':'00060','gauge_height':'00065'}

    paramsCodes = []
    for param in params:
        if param in codeList.keys():
            paramsCodes.append(codeList[param])
        else:
            paramsCodes.append(param)

    paramsString = 'parameterCd=' + ','.join(paramsCodes)

    return (paramsString,paramsCodes)


def stats_codes(stats):
    """
    Input stats must be an array of strings can use both parameter names (see supported below),
    or 5-digit parameter codes. This function is only called for sw data values.

    Supported parameter names are: "mean", "max", "min"
    """

    import warnings
    codeList = {'mean':'00003','max':'00001','min':'00002'}

    # For now, can only handle one stats code
    if len(stats) > 1:
        warnings.warn('Only one stats code can be used at this time',UserWarning)
        stats = stats[0]

    statsCodes = []
    for stat in stats:
        if isinstance(stat,str):
            statsCodes.append(codeList[stat])
        else:
            statsCodes.append('%05d'%stat)

    statsString = 'statCd=' + ','.join(statsCodes)

    return(statsString,statsCodes)


def decode_data(data):
    # This function replaces text codes with empty values instead
    codeTable = {'Ssn':'Parameter monitored seasonally','Bkw':'Flow affected by backwater',\
    'Ice':'Ice affected','Pr':'Partial-record site','Rat':'Rating being developed or revised',\
    'Eqp':'Equipment malfunction','Fld':'Flood damage','Dry':'Dry','Dis':'Data-collection discontinued',\
    '--':'Parameter not determined','Mnt':'Maintenance in progress','ZFl':'Zero flow','***':'Temporarily unavailable'};

    for code in codeTable:
        data.replace(code,'')
    return(data)


def try_nodata(val):
    # NaN value
    nanval = '-99999'
    if val == '':
        return nanval
    else:
        return val
