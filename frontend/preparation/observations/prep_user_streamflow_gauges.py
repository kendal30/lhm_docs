# This script creates user-created gauges that will be added to the automatically-selected
# USGS gauges by the combine_gauges.py script, later, observations import scripts
# will need to be run in order to get output from those gauges.
# 
# Note, any gauge type "USER" will be automatically assigned 0-flow values by

import os
import lhm_frontend
import obs

'''
--------------------------------------------------------------------------------
Prepare USER Streamflow Gauges
--------------------------------------------------------------------------------
'''

def prep_gauges(inputDB, pathConfig, printResult=True):

    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Standardized Inputs
    #-------------------------------------------------------------------------------
    # Get the names
    (obsField,outName) = lhm_frontend.names_frontend(['obsFieldStations','prepUserGauges'])

    # Get the model standard input locations
    (inputObs,inputBdry) = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig,section='Obs')
    inputObs = inputObs[obsField]

    # Some USER gauge stuff
    agencyName = 'USER'
    
    outFieldNumID = 'ID_temp_gauges'
    outFieldAgency = 'agency'

    # Set up the environment
    arcpy = lhm_frontend.set_environment('', inputDB, inputBdry) # no argument for cellSize needed   
    
    #-------------------------------------------------------------------------------
    # Do the processing
    #-------------------------------------------------------------------------------
    if agencyName not in inputObs.keys():
        raise ValueError('No "USER" dataset specified in observations')
    
    # Get the dictionary for the USER gauges
    userDict = inputObs[agencyName]

    # Read the obs features
    obs.read_features(userDict, arcpy, outName)

    # Make new fields
    arcpy.AddField_management(outName,outFieldAgency,'TEXT','','','12')
    arcpy.AddField_management(outName,outFieldNumID,'LONG')

    # Calculate those fields
    arcpy.CalculateField_management(outName,outFieldAgency,'\''+ agencyName + '\'','PYTHON')
    arcpy.CalculateField_management(outName,outFieldNumID,'!OBJECTID!','PYTHON')

    # Log the output
    lhm_frontend.log_lhm_output(outName,inputDB,inputDB,printResult)

    # Print result:
    if printResult:
        regionName = os.path.split(pathConfig)[1]
        display(HTML('USER gauge sites have been prepared for the <strong>%s</strong> region'%regionName))


'''
--------------------------------------------------------------------------------
Create user gauge observations file
--------------------------------------------------------------------------------
'''
def prep_observations(inputDB, pathConfig, subModel, params, watersheds=True, printResult=True):
    '''
    This function processes USER stream gauge data into LHM observations. It generates watersheds
    for each point, selects only those watersheds totally within the groundwater model's IBOUND
    array, and then generates a .csv observations data file for LHM import.

    The params dictionary requires:
     - "dateRange" of format yyyy-mm-dd, two-item list, first is minimum, second maximum
    Optional parameters are:
     - "edgeDist", default value is -100, specify the distance to buffer around
       the edge of the ibound array for selecting gauges, negative values
       indicate a buffer inside the feature
     - "weight", default value is 1, specify the datapoint weight. This would
       be used for calibrating parameter values in the model
     - "idStart", default value is 1, only set other than 1 if you know you
       have a reason to (i.e. another similar datatype as a different set)
     - "timeZone", default value is 0, really shouldn't matter much for daily streamflows
    Optionally, specify a boolean value for "watersheds" which, if False, will skip watershed generation.
        This would be used if you have already computed them and are re-running the flow data step.
    '''

    import arcpy
    import numpy, pandas
    from tqdm.notebook import tqdm
    if printResult:
        from IPython.display import display, HTML

    #-------------------------------------------------------------------------------
    # Dataset specific inputs
    #-------------------------------------------------------------------------------
    # Input user gauge locations-- these must come from the combine_gauges.py script
    fieldsGauges = {'agency':'agency','loc_id':'loc_id','loc_name':'loc_name','grid_id':'Site_Num',\
                    'x':'POINT_X','y':'POINT_Y'}

    # Get standardized names or param values
    (flowdir,nameCsv,nameWatersheds,subDirObs,nameIbound,subDirGndwater,dirModLay,dirSubmodLay,extRaster,\
        obsField,inGauges) = \
        lhm_frontend.names_frontend(['prepDEMFillDir','obsCsvUser','obsWshedsUSER','dirObs',\
            'modGndIbound','dirGndwater','dirModelLayers','dirSubmodelLayers','outExtRaster',\
            'obsFieldStations','prepCombGauges'])

    # Get the observations specification
    inputObs = lhm_frontend.lhm_surface_inputs(inputDB,pathConfig,subModel=subModel,section='Obs')[0]
    inputObs = inputObs[obsField]

    # Prepare the other parameters if they've not been specified
    paramDefaults = {'edgeDist':-100,'weight':1,'idStart':1,'timeZone':0}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Get the path to the model/submodel ibound grid, and watersheds file name
    if not subModel:
        dirGndwater = '%s/%s'%(dirModLay,subDirGndwater)
        dirObs = '%s/%s'%(dirModLay,subDirObs)
        outNameWsheds = nameWatersheds
    else:
        dirGndwater = '%s/%s/%s'%(dirSubmodLay,subModel,subDirGndwater)
        dirObs = '%s/%s/%s'%(dirSubmodLay,subModel,subDirObs)
        outNameWsheds = nameWatersheds+'_'+subModel
    dirGndwater = os.path.join(os.path.split(inputDB)[0],dirGndwater)
    dirObs = os.path.join(os.path.split(inputDB)[0],dirObs)

    # Now, get the proper ibound location and band
    ibound = os.path.join(dirGndwater,'%s%s'%(nameIbound,extRaster))
    bandStr = '/Band_1'

    if not os.path.exists(ibound): #may not have already been collapsed
        ibound = os.path.join(dirGndwater,'%s_lay1%s'%(nameIbound,extRaster))
        ibound = os.path.join(os.path.split(inputDB)[0],ibound)
        bandStr = ''

    # Add the cellSize to the params
    params['cellSize'] = arcpy.Describe(ibound+bandStr).meanCellHeight

    # Set the output directory
    outDir = dirObs

    # -------------------------------------------------------------------------------
    # Set internal variables
    # -------------------------------------------------------------------------------
    # Specify a name for this observation set
    setName = 'USER_streamflows'
    agencyName = 'USER'
    setVar = 'streamflow'
    setAgg = 'mean'
    setUnits = 'm^3s^-1'

    # Specify output base names
    gridBase = 'user_'

    # Temporary names
    layerShape = 'layer_shape'
    layerShapeAgency = 'layer_shape_agency'
    layerWatersheds = 'layer_watersheds'

    tempBoundary = 'temp_boundary'
    tempBdryBuffer = 'temp_bdry_buffer'
    tempGauges = 'temp_gauges'
    tempGaugesAgency = 'temp_gauges_agency'
    tempWatershedsAll = 'temp_watersheds_all'

    # -------------------------------------------------------------------------------
    # Prepare the environment
    # -------------------------------------------------------------------------------
    # Prepare the arcpy environment
    arcpy = lhm_frontend.set_environment(params['cellSize'],inputDB,ibound)

    # Add the helper paths and import
    from gis import batch_create_watersheds_feature
    
    # Get the dictionary for the USER gauges
    userDict = inputObs[agencyName]

    #-------------------------------------------------------------------------------
    # Limit gauges to those within ibound array, in specified time period, only "user" agency
    #-------------------------------------------------------------------------------
    # Create a temporary feature with only points within the active boundary extracted
    rasterIbound = arcpy.Raster(ibound)
    rasterIboundNoData = arcpy.sa.Con(rasterIbound>0,1)
    arcpy.RasterToPolygon_conversion(rasterIboundNoData,tempBoundary,'NO_SIMPLIFY')
    arcpy.Buffer_analysis(tempBoundary,tempBdryBuffer,params['edgeDist'])
    arcpy.Clip_analysis(os.path.join(inputDB,inGauges),tempBdryBuffer,tempGauges)

    if watersheds:
        # Select only those gauges that are USGS agency
        arcpy.MakeFeatureLayer_management(tempGauges,layerShapeAgency)
        whereClause = fieldsGauges['agency'] + ' = \''+agencyName+'\''
        arcpy.SelectLayerByAttribute_management(layerShapeAgency,'NEW_SELECTION',whereClause)
        arcpy.CopyFeatures_management(layerShapeAgency,tempGaugesAgency)

        # Batch create the watersheds, set extent to flowdirection first
        arcpy.env.extent = os.path.join(inputDB,flowdir)
        arcpy.env.cellSize = os.path.join(inputDB,flowdir)
        batch_create_watersheds_feature(tempGaugesAgency,tempWatershedsAll,os.path.join(inputDB,flowdir),fieldsGauges['grid_id'])

        # Select only those features that fall entirely within the boundary of the groundwater model
        arcpy.MakeFeatureLayer_management(tempWatershedsAll,layerWatersheds)
        arcpy.SelectLayerByLocation_management(layerWatersheds,'COMPLETELY_WITHIN',tempBoundary)
        arcpy.CopyFeatures_management(layerWatersheds,outNameWsheds)

        # Log this
        lhm_frontend.log_lhm_output(outNameWsheds, inputDB, inputDB)

        # Clean up
        [arcpy.Delete_management(thisLayer) for thisLayer in (layerShapeAgency, layerShape, layerWatersheds)]
        [arcpy.Delete_management(thisTemp) for thisTemp in (tempGaugesAgency, tempWatershedsAll)]

    #-------------------------------------------------------------------------------
    # Loop through the selected watersheds, performing all operations
    #-------------------------------------------------------------------------------
    # Get a search cursor for the input shapefile
    rowsWsheds = arcpy.SearchCursor(outNameWsheds)

    # Make feature layer for selection
    arcpy.MakeFeatureLayer_management(outNameWsheds,layerWatersheds)

    # Set extent to ibound
    arcpy.env.extent = ibound
    arcpy.env.cellSize = params['cellSize']

    # Get the observations values for the USER gauges dataset
    if userDict['table'] is not None:
        dfObs = obs.read_table(userDict,arcpy)
    else:
        dfObs = obs.create_obs_table_df()

    # Loop through the rows of watersheds, selecting each gauge and populating the table
    outCreated = False
    totalLoop = int(arcpy.GetCount_management(outNameWsheds)[0])
    for rowWshed in tqdm(rowsWsheds,total=totalLoop,desc='Creating grids'):
        # Get the gauge for this watershed
        thisWshed = rowWshed.getValue(fieldsGauges['grid_id'])
        rowsGauges = arcpy.SearchCursor(tempGauges)
        for rowGauge in rowsGauges:
            if rowGauge.getValue(fieldsGauges['grid_id'])==thisWshed:
                # Get info on this gauge
                gaugeInfo = dict()
                for field in fieldsGauges.keys():
                    gaugeInfo[field] = rowGauge.getValue(fieldsGauges[field])
        del rowsGauges

        # For this gauge, define output names
        gridName = gridBase + gaugeInfo['loc_id'] + extRaster

        # Convert current watershed feature into a grid
        if watersheds or not (arcpy.Exists(os.path.join(outDir,gridName))):
            whereClause = '"' + fieldsGauges['grid_id'] +'" = ' + str(thisWshed)
            arcpy.SelectLayerByAttribute_management(layerWatersheds,'NEW_SELECTION',whereClause)
            arcpy.PolygonToRaster_conversion(layerWatersheds,fieldsGauges['grid_id'],\
                os.path.join(outDir,gridName),"","",params['cellSize'])

        # ---------------------------------------------------------------------------
        # Populate the output table for this gauge
        # ---------------------------------------------------------------------------
        # Get the daily flow data for this gauge
        dfFlow = dfObs.loc[dfObs['loc_id']==gaugeInfo['loc_id']]

        # Fill the dataframe with missing dates
        dfAll = pandas.DataFrame(pandas.date_range(start=params['dateRange'][0], end=params['dateRange'][1], freq='1D'),\
            columns=['time_start'])
        dfAll['time_end'] = dfAll['time_start'] + pandas.Timedelta(days=1,minutes=-1)
        if len(dfFlow)>1:
            dfAll = dfAll.merge(dfFlow,how='left',on=['time_start'])
        else:
            dfAll['value'] = numpy.NaN

        # Prepare the output dataframe for this gauge
        dfObsRow = pandas.DataFrame(dfAll['value'])

        # Write static row values
        dfObsRow.loc[:,'id'] = params['idStart'] + numpy.arange(0,len(dfObsRow),dtype=numpy.int32) 
        dfObsRow.loc[:,'type'] = obsField
        dfObsRow.loc[:,'loc_id'] = gaugeInfo['loc_id']
        dfObsRow.loc[:,'loc_name'] = gaugeInfo['loc_name']
        dfObsRow.loc[:,'set_name'] = setName
        dfObsRow.loc[:,'variable'] = setVar
        dfObsRow.loc[:,'x'] = gaugeInfo['x']
        dfObsRow.loc[:,'y'] = gaugeInfo['y']
        dfObsRow.loc[:,'z_top'] = 0
        dfObsRow.loc[:,'z_bot'] = 0
        dfObsRow.loc[:,'units'] = setUnits
        dfObsRow.loc[:,'grid'] = gridName
        dfObsRow.loc[:,'grid_id'] = gaugeInfo['grid_id']

        # time_zone
        if not 'time_zone' in dfAll.columns:
            dfObsRow.loc[:,'time_zone'] = params['timeZone']
        else:
            dfObsRow.loc[:,'time_zone'] = dfAll['time_zone']
        # aggregate
        if not 'aggregate' in dfAll.columns:
            dfObsRow.loc[:,'aggregate'] = setAgg
        else:
            dfObsRow.loc[:,'aggregate'] = dfAll['aggregate']
        # weight
        if not 'weight' in dfAll.columns:
            dfObsRow.loc[:,'weight'] = params['weight']
        else:
            dfObsRow.loc[:,'weight'] = dfAll['weight']

        # Write the times
        dfObsRow.loc[:,'time_start'] = dfAll['time_start'].dt.strftime('%Y-%m-%d %H:%M:%S')
        dfObsRow.loc[:,'time_end']  = dfAll['time_end'].dt.strftime('%Y-%m-%d %H:%M:%S')

        # Append
        if not outCreated:
            dfObs = dfObsRow.copy()
            outCreated = True
        else:
            dfObs = pandas.concat([dfObs,dfObsRow],axis=0,ignore_index=True)

    # Delete the rowsShape and locks
    del rowsWsheds

    # Rearrange columns into final order
    dfTemplate = obs.create_obs_table_df()
    listCols = dfTemplate.columns
    dfObs = dfObs[listCols]

    # Write the observations table
    dfObs.to_csv(os.path.join(outDir,nameCsv),index=False)

    # Log this
    lhm_frontend.log_lhm_output(nameCsv, outDir, inputDB)

    # Clean up
    del rasterIbound, rasterIboundNoData
    arcpy.Delete_management(layerWatersheds)
    [arcpy.Delete_management(thisTemp) for thisTemp in (tempGauges, tempBdryBuffer, tempBoundary)]

    # Print result:
    if printResult:
        display(HTML('USER observations finished processing'))