def create_upgrade_model(pathModeling, regionName, subDir=None, subModel=False, defaultDirs=True, printSuccess=True):
    '''This function creates a blank database template and sets up the directory
    structures used by the model. It takes two required inputs:

    1) pathModeling, which is the location of your modeling folder, under which the
    function will establish default subdirectories (unless you set defaultDirs=False,
    in which case should have subfolders Config, Init, Input, Output, Prep, and
    Postprocess already created, and;

    2) regionName, which will be the name of the folders created
    underneath Config, Init, Prep, and Postprocess, but not necessarily the name
    that you will give to individual model runs later on when you initialize your model runs.

    There are four optional inputs:
    - subDir, which will put the newly created Config,
      Init, Prep, and Postprocess folders for this model within a subdirectory of the master
      Config, Init, Prep, and Postprocess folders. This can be used to, for instance,
      store models in subdirectories by year, with the argument subDir = '2014' for
      example;
    - subModel, which will create a submodel using a main model;
    - defaultDirs, which will create a default project structure for
      you (unless the folders are already present), and;
    - printSuccess, which will determine whether or not to print messages'''

    #--------------------------------------------------------------------------
    # LHM Defaults -- when you edit these, make sure they are consistent with frontend_names
    # Transactions Table
    tableTransact = 'Transactions'
    fieldsTransact = {'ID':{'type':'LONG','length':''},'Datetime':{'type':'DATE','length':''},\
        'Name':{'type':'TEXT','length':255},'Type':{'type':'TEXT','length':255},\
        'Domain':{'type':'TEXT','length':255},'Chain':{'type':'TEXT','length':255},\
        'Supersedes':{'type':'LONG','length':''}}

    # Relative paths for config files and modeling folders
    dirRelLHMTemplates = 'frontend\\templates'
    subDirsLHMTempConfig = ['notebooks_config','params_tables','notebooks_optim'] #match those below in subSubDirsTemplates
    subDirsLHMTempPost = ['postprocess_scripts','notebooks_postprocess']
    dirRelModelConfig = 'Config'
    dirRelModelPrep = 'Prep'
    dirRelModelInit = 'Init'
    dirRelModelOutput = 'Output'
    dirRelModelInput = 'Input'
    dirRelModelPost = 'Postprocess'

    # Subdirectory names - Prep
    subDirLAI = 'LAI_Clipped'
    subDirLAIBackup = 'LAI_Clipped_Backup'
    subDirLUTop = 'LU_Clipped_Reclass'
    subSubDirsLU = ['Landuse','Roughness','Impervious','Flowtimes']
    subDirObsTop = 'Observations_Prep'
    subDirUseTop = 'Water_Use'
    subSubDirsUse = ['Irrigation_limits','Irrigation_technology','Irrigation_gw_frac','Irrigation_canal_leakage',\
                     'Irrigation_canal_evap', 'Irrigation_sw_POD_id', 'Irrigation_sw_POD_shed']
    subDirLayTop = 'Model_Layers'
    subDirSubmodelLayTop = 'Submodel_Layers'
    subSubDirsLay = ['Surface','Groundwater','Observations']
    subDirWorking = 'Import_Working'

    # Subdirectory names - Config
    subDirTemplates = 'Templates'
    subDirSubmodel= 'Submodel'

    # Model prep geodatabase
    namePrepDB = 'model_prep.gdb'

    # Specify template information
    listTemplateExtensions = ['.csv','.m','.py','.ipynb']
    exclFilesTemplates = []
    extTemplate = '.xyz'

    # Specify model frontend files
    exclFilesFrontend = ['LHM_Frontend.ipynb'] # Exclude these template files from copying
    subDirLHMTempFrontend = 'notebooks_frontend'

    # Configuration file
    oldConfigFile = 'notebook.cfg'
    oldSourceDatasets = 'old_source_datasets.csv'
    configFile = 'notebook.yml'
    extOld = '.old'

    #--------------------------------------------------------------------------
    # Set up the environment
    import arcpy, os, shutil, pandas
    from IPython.display import display, HTML

    # Define path names used below
    # These do not depend on the other organization
    inInitDir = os.path.join(pathModeling,dirRelModelInit)
    inInputDir = os.path.join(pathModeling,dirRelModelInput)
    inOutputDir = os.path.join(pathModeling,dirRelModelOutput)
    inPrepBaseDir = os.path.join(pathModeling,dirRelModelPrep)
    inConfigBaseDir = os.path.join(pathModeling,dirRelModelConfig)
    inPostBaseDir = os.path.join(pathModeling,dirRelModelPost)

    # Test to make sure that subDir is valid
    if subDir:
        if len(subDir) == 0:
            raise ValueError('subDir argument must be a string or the boolean value False')

    # Test to make sure that subModel is valid
    if subModel:
        if len(subModel) == 0:
            raise ValueError('subModel argument must be a string or the boolean value False')


    # Check to make sure that the baseDirs are present
    if not os.path.exists(inPrepBaseDir):
        os.mkdir(inPrepBaseDir)
    if not os.path.exists(inConfigBaseDir):
        os.mkdir(inConfigBaseDir)
    if not os.path.exists(inPostBaseDir):
        os.mkdir(inPostBaseDir)

    # These depend on the organization structure input
    if not subDir:
        inPrepDir = os.path.join(inPrepBaseDir,regionName)
        inConfigDir = os.path.join(inConfigBaseDir,regionName)
        inPostDir = os.path.join(inPostBaseDir,regionName)

    else:
        # Get the subDir names
        inPrepSubDir = os.path.join(inPrepBaseDir,subDir)
        inConfigSubDir = os.path.join(inConfigBaseDir,subDir)
        inPostSubDir = os.path.join(inPostBaseDir,subDir)
        # Check that they exist, create if not
        if not os.path.exists(inPrepSubDir):
            os.mkdir(inPrepSubDir)
        if not os.path.exists(inConfigSubDir):
            os.mkdir(inConfigSubDir)
        if not os.path.exists(inPostSubDir):
            os.mkdir(inPostSubDir)
        # Then set the final paths
        inPrepDir = os.path.join(inPrepSubDir,regionName)
        inConfigDir = os.path.join(inConfigSubDir,regionName)
        inPostDir = os.path.join(inPostSubDir,regionName)

    # Define geodatabase name
    inGeoDBName = regionName + '_' + namePrepDB

    # Get the LHM input directory
    inLHMDir = os.path.normpath(os.path.dirname(os.path.realpath(__file__))+'/..')

    #--------------------------------------------------------------------------
    # Do the steps
    # Organization-independent directories
    if defaultDirs:
        for thisDir in [inInitDir,inInputDir,inOutputDir]:
            if not os.path.exists(thisDir):
                os.mkdir(thisDir)

    # Check to make sure that the directories have already been created if this is a subModel
    if subModel:
        if not os.path.exists(inPrepDir):
            raise RuntimeError('If using a subModel, the base model must already have been created')

    # 1: Create the directories
    if not subModel:
        # Prep group
        if not os.path.exists(inPrepDir):
            containerUpgrade = False
            os.mkdir(inPrepDir)
        else:
            containerUpgrade = True
        # LAI
        if not os.path.exists(os.path.join(inPrepDir,subDirLAI)):
            os.mkdir(os.path.join(inPrepDir,subDirLAI))
        if not os.path.exists(os.path.join(inPrepDir,subDirLAIBackup)):
            os.mkdir(os.path.join(inPrepDir,subDirLAIBackup))
        # LU
        if not os.path.exists(os.path.join(inPrepDir,subDirLUTop)):
            os.mkdir(os.path.join(inPrepDir,subDirLUTop))
        for thisSubSubDir in subSubDirsLU:
            if not os.path.exists(os.path.join(os.path.join(inPrepDir,subDirLUTop),thisSubSubDir)):
                os.mkdir(os.path.join(os.path.join(inPrepDir,subDirLUTop),thisSubSubDir))
        # Observations
        if not os.path.exists(os.path.join(inPrepDir,subDirObsTop)):
            os.mkdir(os.path.join(inPrepDir,subDirObsTop))
        # Water Use
        if not os.path.exists(os.path.join(inPrepDir,subDirUseTop)):
            os.mkdir(os.path.join(inPrepDir,subDirUseTop))
        for thisSubSubDir in subSubDirsUse:
            if not os.path.exists(os.path.join(os.path.join(inPrepDir,subDirUseTop),thisSubSubDir)):
                os.mkdir(os.path.join(os.path.join(inPrepDir,subDirUseTop),thisSubSubDir))
        if not os.path.exists(os.path.join(inPrepDir,subDirWorking)):
            os.mkdir(os.path.join(inPrepDir,subDirWorking))
        if not os.path.exists(os.path.join(inPrepDir,subDirLayTop)):
            os.mkdir(os.path.join(inPrepDir,subDirLayTop))
        for thisSubSubDir in subSubDirsLay:
            if not os.path.exists(os.path.join(os.path.join(inPrepDir,subDirLayTop),thisSubSubDir)):
                os.mkdir(os.path.join(os.path.join(inPrepDir,subDirLayTop),thisSubSubDir))

        # Config group
        if not os.path.exists(inConfigDir):
            os.mkdir(inConfigDir)
        if not os.path.exists(os.path.join(inConfigDir,subDirTemplates)):
            os.mkdir(os.path.join(inConfigDir,subDirTemplates))

        # Postprocess group
        if not os.path.exists(inPostDir):
            os.mkdir(inPostDir)
        if not os.path.exists(os.path.join(inPostDir,subDirTemplates)):
            os.mkdir(os.path.join(inPostDir,subDirTemplates))
    else:
        # Prep group
        dirBaseLayTop = os.path.join(inPrepDir,subDirSubmodelLayTop)
        if not os.path.exists(dirBaseLayTop):
            os.mkdir(dirBaseLayTop)
        dirSubmodelLayTop = os.path.join(dirBaseLayTop,subModel)
        if not os.path.exists(dirSubmodelLayTop):
            os.mkdir(dirSubmodelLayTop)
            containerUpgrade = False
        else:
            containerUpgrade = True
        for thisSubSubDir in subSubDirsLay:
            if not os.path.exists(os.path.join(dirSubmodelLayTop,thisSubSubDir)):
                os.mkdir(os.path.join(dirSubmodelLayTop,thisSubSubDir))

        # Config group
        dirBaseConfig = os.path.join(inConfigDir,subDirSubmodel)
        if not os.path.exists(dirBaseConfig):
            os.mkdir(dirBaseConfig)
        inConfigDir = os.path.join(dirBaseConfig,subModel)
        if not os.path.exists(inConfigDir):
            os.mkdir(inConfigDir)
        if not os.path.exists(os.path.join(inConfigDir,subDirTemplates)):
            os.mkdir(os.path.join(inConfigDir,subDirTemplates))


    if not subModel:
        # 2: Create geodatabase
        inGeoDB = os.path.join(inPrepDir,inGeoDBName)
        if not os.path.exists(inGeoDB):
            arcpy.CreateFileGDB_management(inPrepDir,inGeoDBName)

        # 2.5: Upgrade the model, if Source_Datasets table is present
        if containerUpgrade:
            if arcpy.Exists('Source_Datasets'):
                # Copy this to a .csv
                dfSources = pandas.DataFrame(arcpy.da.TableToNumPyArray('Source_Datasets','*'))
                dfSources.to_csv(os.path.join(inConfigDir,oldSourceDatasets))
                display(HTML('Wrote <strong>%s</strong> to %s'%(oldSourceDatasets,inConfigDir)))
                display(HTML('Manually add these sources to your <strong>%s</strong>'%('config_sources.ipynb')))

                # Now, delete the table
                arcpy.Delete_management('Source_Datasets')

        # 3: Create transactions table
        arcpy.env.workspace = inGeoDB
        if not arcpy.Exists(tableTransact):
            arcpy.CreateTable_management(inGeoDB,tableTransact)
            for field in fieldsTransact.keys():
                thisDict = fieldsTransact[field]
                arcpy.AddField_management(tableTransact,field,thisDict['type'],'','',thisDict['length'])


    # 3a: Copy config files templates to new location - config
    # Check to see first if we need to upgrade the previous model structure
    existsOld = os.path.exists(os.path.join(inConfigDir,'Templates/Config'))

    for thisSubDirLHM in subDirsLHMTempConfig:
        # Now, determine the input config templates directory and output model structure directories
        thisLHMDir = os.path.join(os.path.join(inLHMDir,dirRelLHMTemplates),thisSubDirLHM)
        configFiles = os.listdir(thisLHMDir)
        thisModelDir = os.path.join(inConfigDir,subDirTemplates)

        # Copy the template files over
        for thisFile in configFiles:
            # Want only certain types
            if os.path.splitext(thisFile)[1].lower() in listTemplateExtensions:
                if existsOld:
                    # Copy the template files from their old location to the new one
                    if os.path.exists(os.path.join(inConfigDir+'/Templates/Config',thisFile+extTemplate)):
                        shutil.move(os.path.join(inConfigDir+'/Templates/Config',thisFile+extTemplate),\
                                    os.path.join(thisModelDir,thisFile+extTemplate))
                if not thisFile in exclFilesTemplates:
                    # Now, copy over the template file from the code directory
                    if not os.path.exists(os.path.join(thisModelDir,thisFile+extTemplate)):
                        shutil.copyfile(os.path.join(thisLHMDir,thisFile),os.path.join(thisModelDir,thisFile+extTemplate))

    # 3b: Copy config files templates to new location - postprocess
    if not subModel:
        for thisSubDirLHM in subDirsLHMTempPost:
            # Now, determine the input config templates directory and output model structure directories
            thisLHMDir = os.path.join(os.path.join(inLHMDir,dirRelLHMTemplates),thisSubDirLHM)
            configFiles = os.listdir(thisLHMDir)
            thisModelDir = os.path.join(inPostDir,subDirTemplates)

            # Copy the template files over
            for thisFile in configFiles:
                # Want only certain types
                if os.path.splitext(thisFile)[1].lower() in listTemplateExtensions:
                    if existsOld:
                        # Copy the template files from their old location to the new one
                        if os.path.exists(os.path.join(inConfigDir+'/Templates/Config',thisFile+extTemplate)):
                            shutil.move(os.path.join(inConfigDir+'/Templates/Config',thisFile+extTemplate),\
                                        os.path.join(thisModelDir,thisFile+extTemplate))
                    # Now, copy over the template file from the code directory
                    if not os.path.exists(os.path.join(thisModelDir,thisFile+extTemplate)):
                        shutil.copyfile(os.path.join(thisLHMDir,thisFile),os.path.join(thisModelDir,thisFile+extTemplate))

    # 4: Copy the notebook.yml file to the model config and postprocess directories, rename old .cfg if present
    # Rename the old, if present
    if os.path.exists(os.path.join(inConfigDir,oldConfigFile)):
        os.rename(os.path.join(inConfigDir,oldConfigFile),os.path.join(inConfigDir,oldConfigFile+extOld))

    # Now copy the new one
    shutil.copyfile(os.path.join(pathModeling,configFile),os.path.join(inConfigDir,configFile))
    shutil.copyfile(os.path.join(pathModeling,configFile),os.path.join(inPostDir,configFile))

    # 4b: Setup the other model frontend files
    # Get the template directory to pull from and the list of frontend files
    thisLHMDir = os.path.join(os.path.join(inLHMDir,dirRelLHMTemplates),subDirLHMTempFrontend)
    frontendFiles = os.listdir(thisLHMDir)

    # Add to list of excluded frontend files, based on whether it is a submodel
    if subModel:
        exclFilesFrontend += 'model_frontend.ipynb'
    else:
        exclFilesFrontend += 'submodel_frontend.ipynb'

    # Copy the template files over, this overwrites any existing
    for thisFile in frontendFiles:
        # Want only certain types
        if os.path.splitext(thisFile)[1].lower() in listTemplateExtensions:
            if not thisFile in exclFilesFrontend:
                shutil.copyfile(os.path.join(thisLHMDir,thisFile),os.path.join(inConfigDir,thisFile))

    # 5: Run collected model upgrade routines
    upgrade_model(pathModeling,inLHMDir,regionName,subDir,subModel)

    # Display the link
    if printSuccess:
        if containerUpgrade:
            if not subModel:
                display(HTML('Model container for region <strong>%s</strong> was upgraded'%(regionName)))
                if existsOld:
                    display(HTML('Please remove the directory %s manually'%(inConfigDir+'/Templates/Config')))
            else:
                display(HTML('Sub-model container for region <strong>%s</strong> submodel <strong>%s</strong> was upgraded'\
                             %(regionName,subModel)))
        else:
            if not subModel:
                display(HTML('An empty model container for region <strong>%s</strong> was created'%(regionName)))
            else:
                display(HTML('An empty sub-model container for region <strong>%s</strong> submodel <strong>%s</strong> was created'\
                             %(regionName,subModel)))

'''
--------------------------------------------------------------------------------
Upgrade an existing model
--------------------------------------------------------------------------------
'''
def upgrade_model(pathModeling, codeDir, regionName, subDir=None, subModel=None, defaultDirs=True, printSuccess=True):
    import compression, lhm_frontend

    # Get the location of the model paths
    pathFrontendCode = codeDir+'/frontend'
    pathConfig = get_config_path(pathModeling, regionName, subDir, subModel)
    pathLayers = get_layers_path(pathModeling, regionName, subDir, subModel)

    # Upgrade model notebooks
    if not subModel:
        listNotebooks = ['config_sources.ipynb','prepare_data.ipynb','prepare_model.ipynb',\
                         'import_LAND.ipynb','import_CLIM.ipynb','import_STAT_OBS.ipynb',\
                         'config_model.ipynb','config_params.ipynb','config_run.ipynb',\
                         'initialize_run.ipynb']
    else:
        listNotebooks = ['config_submodel_sources.ipynb','prepare_submodel.ipynb',\
                         'import_LAND.ipynb','import_CLIM.ipynb','import_STAT_OBS.ipynb',
                         'config_model.ipynb','config_params.ipynb','config_run.ipynb',\
                         'initialize_run.ipynb']
    for thisNotebook in listNotebooks:
        lhm_frontend.merge_notebooks(pathConfig, pathFrontendCode, thisNotebook, subModel, backupDir = True)

    # Get a list of all rasters to be checked
    rasterList = compression.recursive_workspace_raster_list(pathLayers)

    # Now check if those rasters need to be compressed and converted
    for raster in rasterList:
        compression.compress_file_raster(raster)

'''
--------------------------------------------------------------------------------
Import model and submodel boundaries
--------------------------------------------------------------------------------
'''
def import_boundary(inputDB,pathConfig,regionName,optionalSubDir,subModel=False,printSuccess=True):
    import os, arcpy
    from IPython.display import display, HTML

    # Get names
    [configSources,confSectionModel,confSection,confItemBdry,confItemBdryBuff,\
        featureBoundary,featureBndBuff,dirSurface] = names_frontend(['srcFile','srcSectionModel','srcSectionSurface',\
        'srcBdry','srcBdryBuff','prepBdry','prepBdryBuff','dirSurface'])
    if subModel:
        confItemSubmodelName = names_frontend(['srcSubmodel'])

    # Specify the default buffer distance, if not provided, currently for submodels only
    defaultBuffer = 10000 

    # Get the path names
    pathModeling = get_modeling_path(pathConfig)
    pathPrep = get_prep_path(pathModeling, regionName, optionalSubDir)
    pathLayers = os.path.join(get_layers_path(pathModeling, regionName, optionalSubDir, subModel),dirSurface)

    # Read in the configuration file
    configDict = read_config_dict(os.path.join(pathConfig,configSources),confSection)
    modelDict = read_config_dict(os.path.join(pathConfig,configSources),confSectionModel)

    # Get some inputs from the config file
    inBoundary = configDict[confItemBdry]
    if not subModel:
        inBoundaryBuff = configDict[confItemBdryBuff]
    if subModel:
        inSubmodelName = modelDict[confItemSubmodelName]
        featureBoundaryDB = featureBoundary + '_' + inSubmodelName
        featureBndBuffDB = featureBndBuff + '_' + inSubmodelName
    else:
        featureBoundaryDB = featureBoundary
        featureBndBuffDB = featureBndBuff

    #-------------------------------------------------------------------------------
    #Set up the environment
    #-------------------------------------------------------------------------------
    arcpy.env.overwriteOutput = 1
    arcpy.env.workspace = os.path.join(pathPrep,inputDB)

    # Check coordinate system
    if not subModel:
        spatialRef = arcpy.Describe(inBoundary).spatialReference
    else:
        spatialRef = arcpy.Describe(featureBoundary).spatialReference # use the main model coordinate system

    if spatialRef.linearUnitName == 'Meter':
        arcpy.env.outputCoordinateSystem = spatialRef
    else:
        raise ValueError('Linear coordinates must be meters')

    # Set the extent
    if not subModel:
        arcpy.env.extent = inBoundaryBuff #set to boundary buffer
    else:
        arcpy.env.extent = inBoundary
    
    # Repair geometry
    arcpy.RepairGeometry_management(inBoundary)
    if not subModel:
        arcpy.RepairGeometry_management(inBoundaryBuff)

    # Write boundary and boundary buffer to database
    arcpy.CopyFeatures_management(inBoundary,featureBoundaryDB)
    if not subModel:
        arcpy.CopyFeatures_management(inBoundaryBuff,featureBndBuffDB)
    else:
        # Create a buffer for this feature, for purposes of the climate model 
        arcpy.Buffer_analysis(featureBoundaryDB,featureBndBuffDB,defaultBuffer)

    # Write boundary and boundary buffer to model layers
    arcpy.CopyFeatures_management(featureBoundaryDB,os.path.join(pathLayers,featureBoundary+'.shp'))
    arcpy.CopyFeatures_management(featureBndBuffDB,os.path.join(pathLayers,featureBndBuff+'.shp'))
        
    # Write boundary and buffer in GCS coordinates to model layers
    sr = arcpy.SpatialReference("WGS 1984")
    arcpy.Project_management(featureBoundaryDB, os.path.join(pathLayers,featureBoundary+'_WGS84.shp'), sr)
    arcpy.Project_management(featureBndBuffDB, os.path.join(pathLayers,featureBndBuff+'_WGS84.shp'), sr)

    # Log this
    log_lhm_output(featureBoundary, inputDB, inputDB)
    log_lhm_output(featureBndBuff, inputDB, inputDB)

    # Print success
    if printSuccess:
        if not subModel:
            display(HTML('Boundaries imported for region <strong>%s</strong>'%(regionName)))
        else:
            display(HTML('Boundaries imported for region <strong>%s</strong> submodel <strong>%s</strong>'\
                         %(regionName,subModel)))


'''
--------------------------------------------------------------------------------
Config file utilities
--------------------------------------------------------------------------------
'''
def copy_config_notebook(pathConfig,notebookConfig,subModel=False,printSuccess=True):
    # Copy the configuration template into the main Config folder for this region
    import os, shutil
    from IPython.display import display, HTML

    relPathTemplates = '/Templates'
    if not os.path.exists(os.path.join(pathConfig,notebookConfig)):
        shutil.copyfile(os.path.join(pathConfig+relPathTemplates,notebookConfig+'.xyz'),
                        os.path.join(pathConfig,notebookConfig))
    else:
        if printSuccess:
            display(HTML('%s already exists at %s, skipping copy'%(notebookConfig,pathConfig)))

    # Determine if this is a notebook or a .csv file
    testExt = os.path.splitext(notebookConfig)[1]
    if testExt == '.csv':
        fileType = 'File'
    elif testExt == '.ipynb':
        fileType = 'Notebook'

    # Display the link
    if printSuccess:
        (relPathRegion,regionName) = get_rel_path_config(pathConfig,subModel)
        if not subModel:
            #Display the link
            display(HTML('<a href="/notebooks%s/%s'%(relPathRegion,notebookConfig) +
                         '">Open the %s <em>%s</em> for the <strong>%s</strong> region</a>'%\
                         (fileType,notebookConfig,regionName)))
        else:
            #Display the link
            display(HTML('<a href="/notebooks%s/%s'%(relPathRegion,notebookConfig) +
                        '">Open the %s <em>%s</em> for the <strong>%s</strong> region submodel <strong>%s</strong></a>'\
                        %(fileType,notebookConfig,regionName,subModel)))


def write_config_yml(pathConfig,nameConfig,configDict,subfolder=None,printSuccess=True):
    """This function writes the configuration .yml file, carefully merging any existing values
    with those specified this time."""

    import yaml, os
    from IPython.display import display, HTML

    filenameConfig = nameConfig + '.yml'

    # Use a subfolder if needed
    if subfolder is not None:
        pathConfig = os.path.join(pathConfig,subfolder)

        # Check to see if subfolder exists
        if not os.path.exists(pathConfig):
            os.mkdir(pathConfig)

    def merge_sources(exist,new):
        if isinstance(new,dict):
            if isinstance(exist,dict):
                for field in exist.keys():
                    if field in new.keys():
                        exist[field] = merge_sources(exist[field],new[field])
                for field in new.keys():
                    if field not in exist.keys():
                        exist[field] = new[field]
            else:
                exist = new
        elif isinstance(new,(str,list)):
            exist = new
        else:
            exist = new

        return exist

    # Check to see if the configuration file already exists
    fileConfig = os.path.join(pathConfig,filenameConfig)
    if os.path.exists(fileConfig):
        # Read it in
        existDict = yaml.safe_load(open(fileConfig))

        # Merge them
        configDict = merge_sources(existDict,configDict)

    # Write the output
    yaml.dump(configDict,open(fileConfig,'wt'),default_flow_style=False)

    if printSuccess:
        # Print this
        display(HTML('Configuration file <em>%s</em> written at %s'%(filenameConfig,pathConfig)))


def read_config_dict(pathConfig,configSection):
    # This returns a single configuration section as a flat dictionary object
    import yaml

    # Read in the file
    configFile = yaml.safe_load(open(pathConfig))

    # Return just the section requested
    configDict = configFile[configSection]

    return configDict


def update_config_sources(pathConfig,section,item,val):
    # This updates a single row in a section of a config file

    import yaml

    # Read in the config file
    configDict = yaml.safe_load(open(pathConfig))

    # Update the section and item indicated
    configDict[section][item] = val

    # Write the updated file
    yaml.dump(configDict,open(pathConfig,'wt'),default_flow_style=False)

'''
--------------------------------------------------------------------------------
Frontend path functions
--------------------------------------------------------------------------------
'''
def get_modeling_path(pathConfig):
    import os
    configFile = 'notebook.yml'

    # Get the modeling path by reading the config file
    dictConfig = read_config_dict(os.path.join(pathConfig,configFile),'frontend')
    pathModeling= dictConfig['pathModel']
    return pathModeling

def get_config_path(pathModeling, regionName, subDir=None, subModel=None):
    import os

    if subDir:
        pathConfig = pathModeling+'/Config/'+subDir+'/'+regionName
    else:
        pathConfig = pathModeling+'/Config/'+regionName

    if subModel:
        pathConfig += '/Submodel/'+subModel

    return os.path.normpath(pathConfig)


def get_layers_path(pathModeling, regionName, subDir=None, subModel=None):
    import os

    if not subModel:
        if subDir:
            pathLayers = pathModeling+'/Prep/'+subDir+'/'+regionName+'/Model_Layers'
        else:
            pathLayers = pathModeling+'/Prep/'+regionName+'/Model_Layers'
    else:
        if subDir:
            pathLayers = pathModeling+'/Prep/'+subDir+'/'+regionName+'/Submodel_Layers/'+subModel
        else:
            pathLayers = pathModeling+'/Prep/'+regionName+'/Submodel_Layers/'+subModel

    return os.path.normpath(pathLayers)


def get_prep_path(pathModeling, regionName, subDir=None):
    import os

    if subDir:
        pathPrep = pathModeling+'/Prep/'+subDir+'/'+regionName
    else:
        pathPrep = pathModeling+'/Prep/'+regionName

    return os.path.normpath(pathPrep)


def get_rel_path_config(pathConfig,subModel=False):
    import os

    if not subModel:
        #Determine the relative path to the config folder relative to the main modeling folder
        (pathModeling, regionName) = os.path.split(pathConfig)
        testOptionalSubDir = os.path.split(pathModeling)[1]
        if testOptionalSubDir.lower() != 'config':
            relPathRegion = '/Config/'+testOptionalSubDir+'/'+regionName
        else:
            relPathRegion = '/Config/'+regionName
    else:
        #Determine the relative path to the config folder relative to the main modeling folder
        (pathConfigParent, regionName) = os.path.split(os.path.split(os.path.split(pathConfig)[0])[0])
        testOptionalSubDir = os.path.split(pathConfigParent)[1]
        if testOptionalSubDir.lower() != 'config':
            relPathRegion = '/Config/'+testOptionalSubDir+'/'+regionName+'/Submodel/'+subModel
        else:
            relPathRegion = '/Config/'+regionName+'/Submodel/'+subModel

    return relPathRegion, regionName


'''
--------------------------------------------------------------------------------
Logging and transactions
--------------------------------------------------------------------------------
'''
def log_lhm_output(outObj, outPath, inputDB, printResult=True):
    """ This function is a method wrapper for logging written outputs
    
    outType: string, with values 'copyFeatures'
    """
    import datetime, inspect
    if printResult:
        from IPython.display import display, HTML

    # Get the current time
    timestamp = datetime.datetime.now()

    # Print if desired
    if printResult:
        display(HTML(' - Object <strong>{0}</strong> written to <em>{1}</em> on {2:%Y-%m-%d} at {3:%H:%M:%S}'.format(\
                outObj,outPath,timestamp,timestamp)))

    # Get the calling module, function
    caller = inspect.currentframe().f_back
    module = caller.f_globals['__name__']
    function = caller.f_code.co_name
    time = str(timestamp)

    # TODO: add to transactions database

def write_transaction(geoDB,transaction):
    '''Transaction is a dictionary with fields: Name, Type, Domain, and Chain'''
    import arcpy, pandas, numpy
    from datetime import datetime

    #Specify the table name
    tableTransactions = 'Transactions'
    fieldsList = ['ID','Datetime','Name','Type','Domain','Chain','Supersedes']

    #Set the workspace
    arcpy.env.workspace = geoDB

    #Read in the transactions table
    dfTransactions = pandas.DataFrame(arcpy.da.TableToNumPyArray(tableTransactions,fieldsList))

    #Get the ID for this transaction
    if len(dfTransactions)>0:
        transaction['ID'] = dfTransactions['ID'].max() + 1
    else:
        transaction['ID'] = 1

    #Set the datetime
    transaction['Datetime'] = datetime.strftime(datetime.now(),'%Y/%m/%d %H:%M:%S')

    #Check to see if this supersedes any existing transactions with the same name and domain
    supersedesID = dfTransactions.loc[numpy.logical_and(dfTransactions['Name']==transaction['Name'],
                                         dfTransactions['Domain']==transaction['Domain']),'ID']
    if len(supersedesID) > 0:
        transaction['Supersedes'] = supersedesID.max()
    else:
        transaction['Supersedes'] = 0

    #Write this entry to the db
    cursor = arcpy.da.InsertCursor(tableTransactions,fieldsList)
    cursor.insertRow([transaction[thisField] for thisField in fieldsList])
    del cursor


'''
--------------------------------------------------------------------------------
Notebook utility functions
--------------------------------------------------------------------------------
'''
def get_notebook_server(pathLHM):
    import os
    from notebook import notebookapp

    # Get current running servers
    servers = list(notebookapp.list_running_servers())

    # Determine which is running LHM
    pathLHM = os.path.normpath(pathLHM)
    serverURL = False
    for server in servers:
        pathNotebook = os.path.normpath(server['notebook_dir'])
        if pathLHM == pathNotebook:
            serverURL = server['url']
    return serverURL

def merge_notebooks(pathConfig,pathFrontendCode,notebookMerge,subModel=False,backupDir=False,printSuccess=True):
    import subprocess, os, shutil, time, sysconfig
    from IPython.display import display, HTML

    pathBase = pathConfig + '/Templates'
    pathLocal = pathConfig
    pathRemote = pathFrontendCode + '/templates/notebooks_config'

    existsBase = os.path.exists(os.path.join(pathBase,notebookMerge+'.xyz'))
    if existsBase:
        existsLocal = os.path.exists(os.path.join(pathLocal,notebookMerge))
        if existsLocal:
            if backupDir:
                # Create the backup directory
                pathBackup = pathLocal + '/Backup'
                if not os.path.exists(pathBackup):
                    os.mkdir(pathBackup)

                # Copy the notebook to the backup directory, add today's date
                backupName = os.path.splitext(notebookMerge)[0] + '_' + time.strftime('%y%m%d') + '.ipynb'
                shutil.copyfile(os.path.join(pathLocal,notebookMerge),os.path.join(pathBackup,backupName))

            # Do the merge operation
            args = ['nbmerge',os.path.join(pathBase,notebookMerge+'.xyz'),os.path.join(pathLocal,notebookMerge),\
                    os.path.join(pathRemote,notebookMerge),'--out',os.path.join(pathLocal,notebookMerge)]
            # Add the Scripts directory to the PATH for this subprocess call
            env = {}
            env = os.environ
            env['PATH'] += sysconfig.get_path('scripts') +';'
            #Make the call
            subprocess.call(args,env=env)
    else:
        existsLocal = False

    # Overwrite (or copy for the first time) the template .xyz notebook (Base) with the new repo template (Remote)
    shutil.copyfile(os.path.join(pathRemote,notebookMerge),os.path.join(pathBase,notebookMerge+'.xyz'))

    # Display the link
    if printSuccess and existsLocal:
        #Determine the relative path to the config folder relative to the main modeling folder
        (relPathRegion,regionName) = get_rel_path_config(pathConfig,subModel)

        #Display the link
        display(HTML('<a href="/notebooks%s/%s'%(relPathRegion,notebookMerge) +
                     '">Open the Notebook <em>%s</em> to check the merge</a>'%(notebookMerge)))

'''
--------------------------------------------------------------------------------
Map the imported boundary
--------------------------------------------------------------------------------
'''
def map_boundary(pathConfig,regionName,optionalSubDir,subModel=False):
    import os
    from matplotlib import pyplot
    import cartopy
    #import contextily as ctx
    import geopandas as gpd

    # Workaround for current version of pyproj = 1.9.6
    import sysconfig
    basePath = sysconfig.get_path('data')
    os.environ['PROJ_LIB'] = basePath + r'\Library\share'

    [boundaryFeature,bufferedFeature,dirModelLayers,dirSubmodelLayers,dirSurface] = \
        names_frontend(['prepBdry','prepBdryBuff','dirModelLayers','dirSubmodelLayers','dirSurface'])
    
    relPathLayers = '/%s/%s'%(dirModelLayers,dirSurface)
    if subModel:
        relPathSubmodelLayers = ('/%s/'+subModel+'/%s')%(dirSubmodelLayers,dirSurface)

    #Get the path names
    pathModeling = get_modeling_path(pathConfig)
    pathPrep = get_prep_path(pathModeling, regionName, optionalSubDir)

    # Open up the model boundary
    gdfBdry = gpd.read_file(os.path.join(pathPrep+relPathLayers,boundaryFeature+'.shp'))
    gdfBuff = gpd.read_file(os.path.join(pathPrep+relPathLayers,bufferedFeature+'.shp'))
    if subModel:
        # Open the submodel boundary
        getBdrySub = gpd.read_file(os.path.join(pathPrep+relPathSubmodelLayers,boundaryFeature+'.shp'))

    # Get the outExtent, in lat/lon, set the plot projection
    minx, miny, maxx, maxy = gdfBuff.to_crs(epsg=4326).geometry.total_bounds
    centralLon = minx + (maxx - minx) / 2
    centralLat = miny + (maxy - miny) / 2
    plotProj = cartopy.crs.AlbersEqualArea(centralLon, centralLat)

    # Create the plot
    pyplot.figure(figsize=(9, 9))
    ax = pyplot.axes(projection=plotProj)

    # Add Features using Cartopy's Natural Earth Data interface
    naturalEarthFeature = cartopy.feature.NaturalEarthFeature
    ocean_10m = naturalEarthFeature('physical', 'ocean', '10m',\
                                    edgecolor='blue', facecolor=cartopy.feature.COLORS['water'])
    land_10m = naturalEarthFeature('physical', 'land', '10m',\
                                   edgecolor=None, facecolor=cartopy.feature.COLORS['land'])
    rivers_10m = naturalEarthFeature('physical', 'rivers_lake_centerlines', '10m',\
                                     edgecolor='xkcd:blue', facecolor='None')
    rivers_NA_10m = naturalEarthFeature('physical', 'rivers_north_america', '10m',\
                                     edgecolor='xkcd:azure', facecolor='None')
    lakes_10m = naturalEarthFeature('physical', 'lakes', '10m',\
                                    edgecolor='blue', facecolor=cartopy.feature.COLORS['water'])
    states_10m = naturalEarthFeature('cultural', 'admin_1_states_provinces_lakes', '10m',\
                                    edgecolor='grey', facecolor='None')
    countries_10m = naturalEarthFeature('cultural', 'admin_0_countries', '10m',\
                                    edgecolor='black', facecolor='None')
    ax.add_feature(ocean_10m, zorder=1)
    ax.add_feature(land_10m, zorder=2)
    ax.add_feature(rivers_10m, zorder=3)
    ax.add_feature(rivers_NA_10m, zorder=3)
    ax.add_feature(lakes_10m, zorder=4)
    ax.add_feature(states_10m, zorder=5)
    ax.add_feature(countries_10m, zorder=6)

    # Plot the model boundary, using 3857 (Web Mercator) for mapping
    ax = gdfBuff.to_crs(epsg=4326).plot(alpha=0.3, facecolor='k', edgecolor='k', ax=ax,\
        transform=cartopy.crs.PlateCarree(),zorder=7)
    gdfBdry.to_crs(epsg=4326).plot(alpha=0.3, facecolor='b', edgecolor='b', ax=ax,\
        transform=cartopy.crs.PlateCarree(),zorder=8)

    if subModel:
        getBdrySub.to_crs(epsg=4326).plot(alpha=0.5, facecolor='r', edgecolor='r', ax=ax, \
            transform=cartopy.crs.PlateCarree(),zorder=9)

    # Style
    ax.set_axis_off()
    ax.gridlines()

    # Show
    pyplot.show()


'''
-------------------------------------------------------------------------------
 Frontend Widget Stuff
-------------------------------------------------------------------------------
'''
def frontend_model_select(pathModeling):
    import glob, os
    from ipywidgets.widgets import HBox, VBox
    from ipywidgets import widgets

    def parse_modelNames(thisNotebook):
            (upPath,regionName) = os.path.split(os.path.split(thisNotebook)[0])
            if os.path.split(upPath)[1]!='Config':
                optDirName = os.path.split(upPath)[1]
                modelName = os.path.join(optDirName,regionName)
            else:
                modelName = regionName
            return modelName, regionName, optDirName

    def parse_submodelNames(thisNotebook):
        (upPath,submodelName) = os.path.split(os.path.split(thisNotebook)[0])
        return submodelName

    def list_models(baseDir):
        listNotebooks = glob.glob(os.path.join(baseDir,'Config/**/model_frontend.ipynb'),recursive=True)
        listModels = [parse_modelNames(thisNotebook)[0] for thisNotebook in listNotebooks]
        listRegions = [parse_modelNames(thisNotebook)[1] for thisNotebook in listNotebooks]
        listOptDir = [parse_modelNames(thisNotebook)[2] for thisNotebook in listNotebooks]
        listPaths = [os.path.split(thisNotebook)[0] for thisNotebook in listNotebooks]
        return listModels, listRegions, listOptDir, listPaths

    def dict_properties(listModels,listRegions,listOptDirs,listPaths):
        dictSubmodels = dict()
        dictRegions = dict()
        dictOptDirs = dict()
        for thisModel in listModels:
            indModel = listModels.index(thisModel)
            thisModelPath = listPaths[indModel]
            dictSubmodels[thisModel] = ['']
            listNotebooks = glob.glob(os.path.join(thisModelPath,'Submodel/**/submodel_frontend.ipynb'),recursive=True)
            dictSubmodels[thisModel] += [parse_submodelNames(thisNotebook) for thisNotebook in listNotebooks]
            dictRegions[thisModel] = listRegions[indModel]
            dictOptDirs[thisModel] = listOptDirs[indModel]
        return dictSubmodels, dictRegions, dictOptDirs

    # Build the list
    (listModels,listRegions,listOptDirs,listPaths) = list_models(os.getcwd())
    (dictSubmodels,dictRegions,dictOptDirs) = dict_properties(listModels,listRegions,listOptDirs,listPaths)

    # Create the widgets
    menuModel = widgets.Dropdown(options=listModels,description='Model')
    menuSubmodel = widgets.Dropdown(options=dictSubmodels[menuModel.value],description='Submodel')
    buttonOpen = widgets.Button(description='Open Model')
    buttonUpgrade = widgets.Button(description='Upgrade Model')
    out = widgets.Output()

    # Define functions for widget actions
    def update_submodel(Submodel):
        pass

    def update_model(Model):
        menuSubmodel.options = dictSubmodels[Model]

    def open_model(_):
        import webbrowser
        url = get_notebook_server(pathModeling)
        url = url + 'notebooks/Config'
        thisModel = menuModel.value
        url += '/%s'%thisModel

        # Add the submodel
        thisSubmodel = menuSubmodel.value
        if len(thisSubmodel) == 0:
            thisSubmodel = False
        if thisSubmodel:
            url += '/Submodel/%s/submodel_frontend.ipynb'%thisSubmodel
        else:
            url += '/model_frontend.ipynb'

        # Open the link
        webbrowser.open_new_tab(url)

    def upgrade_model(_):
        regionName = dictRegions[menuModel.value]
        optionalSubDir = dictOptDirs[menuModel.value]
        submodelName = menuSubmodel.value
        if len(submodelName) == 0:
            submodelName = False
        create_upgrade_model(pathModeling, regionName, optionalSubDir, submodelName)

    # Specify how elements interact and display
    buttonOpen.on_click(open_model)
    buttonUpgrade.on_click(upgrade_model)
    interactSubmodel = widgets.interactive(update_submodel, Submodel=menuSubmodel)
    interactModel = widgets.interactive(update_model, Model=menuModel)
    VBox([VBox([interactModel,interactSubmodel,HBox([buttonOpen,buttonUpgrade])]),out])

'''
--------------------------------------------------------------------------------
Prep Noteboks Standard Functions
--------------------------------------------------------------------------------
'''
def names_frontend(reqList=None):
    '''
    This function stores the standard names for the LHM Frontend, returning either the requested names
    (reqList should be a list), or the whole standard Frontend names list if nothing is requested
    '''
    import os, pandas

    nameConfig = 'notebook.yml'
    nameSection = 'frontend'
    nameEntry = 'pathFrontend'
    nameFile = 'frontend_names.csv'

    # Get the path to the notebook.yml file
    configFrontend = read_config_dict(os.path.join(os.getcwd(),nameConfig),nameSection)
    pathCode = configFrontend[nameEntry]

    # Read the frontend file
    dfNames = pandas.read_csv(os.path.join(pathCode,nameFile),names=['name','value'],skipinitialspace=True,\
        skip_blank_lines=True,comment='#').set_index(['name'])
    names = dfNames.to_dict('index')
      
    if isinstance(reqList,str):
        reqList = [reqList]

    # Return what is requested
    if reqList is None:
        return names
    elif isinstance(reqList,list):
        if len(reqList) > 1:
            return [names[thisReq]['value'] for thisReq in reqList]
        else:
            return names[reqList[0]]['value']


def set_environment(cellSize, workspaceDB, inMatchExtent):
    """Defines the arcpy environment
    """
    import arcpy

    # All functions use this
    arcpy.CheckOutExtension("Spatial")
    arcpy.env.overwriteOutput = 1
    arcpy.env.compression = 'LZ77'
    arcpy.env.pyramid = 'NONE'

    # Set the workspace for processing
    arcpy.env.workspace = workspaceDB
    arcpy.env.scratchWorkspace = arcpy.env.workspace
    
    # Set the cellsize and snap raster
    if isinstance(cellSize,str):
        if len(cellSize) > 1:
            arcpy.env.cellSize = arcpy.Describe(cellSize).meanCellHeight
        else:
            arcpy.env.cellSize = cellSize #this will be a blank
        arcpy.env.snapRaster = cellSize
    else:
        arcpy.env.cellSize = cellSize
        arcpy.env.snapRaster = ''

    # Set the extent and coordinate system
    arcpy.env.outputCoordinateSystem = inMatchExtent
    arcpy.env.extent = inMatchExtent

    return arcpy


def lhm_surface_inputs(inputDB, pathConfig, subModel=False, section='Surface'):
    # options for section are 'Surface', 'WaterUse', 'Obs'
    import os

    srcSection = 'srcSection'+section

    # Get standard names
    (configFileSources,configSection,inputBdry) = \
        names_frontend(['srcFile',srcSection,'prepBdryBuff'])

    if subModel:
        inputBdry = inputBdry+'_'+subModel

    # Get the model input locations
    sources = read_config_dict(os.path.join(pathConfig,configFileSources),configSection)

    # Set the extent and coordinate system
    inMatchExtent = os.path.join(inputDB,inputBdry)

    return sources, inMatchExtent


def lhm_groundwater_inputs(inputDB, pathConfig, subModel):

    import lhm_frontend, os

    (configFileSources,configSectionGndwater,modelGridSurf,outExtension,srcTop,srcStartHeads,nameStartHeads,nameDEM,\
        dirModelLayers,dirSubmodelLayers,dirGroundwater,dirSurface) = \
        lhm_frontend.names_frontend(['srcFile','srcSectionGndwater','modSurfGrid','outExtRaster',\
        'srcTop','srcStartHeads','prepStartHeads','prepDEMRaw','dirModelLayers','dirSubmodelLayers',\
        'dirGndwater','dirSurface'])

    # Get the model input locations
    sources = lhm_frontend.read_config_dict(os.path.join(pathConfig,configFileSources),configSectionGndwater)

    # Locations of default directories
    if not subModel:
        subDirGndwater = '%s/%s'%(dirModelLayers,dirGroundwater)
        subDirSurface = '%s/%s'%(dirModelLayers,dirSurface)
    else:
        subDirGndwater = ('%s/'+subModel+'/%s')%(dirSubmodelLayers,dirGroundwater)
        subDirSurface = ('%s/'+subModel+'/%s')%(dirSubmodelLayers,dirSurface)

    # Set the extent and coordinate system
    inMatchExtent = os.path.join(os.path.split(inputDB)[0]+'/'+subDirSurface,modelGridSurf+outExtension)
    
    # Determine the output directory
    outDir = os.path.join(os.path.split(inputDB)[0],subDirGndwater)

    # Check to see if 'top' and 'startHeads' are specified, otherwise set to
    # model default
    if srcTop not in sources.keys():
        sources[srcTop] = {'method':'grid','fieldVal':'','fieldLay':''}
        sources[srcTop]['data'] = [os.path.join(inputDB + '/%s'%nameDEM)]
    if srcStartHeads not in sources.keys():
        sources[srcStartHeads] = {'method':'grid','fieldVal':'','fieldLay':''}
        sources[srcStartHeads]['data'] = [os.path.join(inputDB + '/%s'%nameStartHeads)]

    return sources, inMatchExtent, outDir