function rmsr = rms(simOrRes,obs)
%RANGE  Calculates the root mean square residual
%   varargout = range(simOrRes,obs)
%   varagrout = range(simOrRes)
%
%   This function uses nanmean, rather than mean.
%
%   Descriptions of Input Variables:
%   simOrRes:   simulated values, or the already-calculated residual.  If
%               two arguments are passed to this function, then it assumes
%               this array is simulated values.  If only one argument, then
%               the values are assumed to be residuals.
%   obs:        observed values, only include if the first array is
%               simulated values, and not a residual.
%
%   Descriptions of Output Variables:
%   rmsr:  Root mean square residual
%   
%
%   Example(s):
%   >> [rmsResid] = rms(residual);
%   >> [rmsResid] = rms(simVals,obsVals);

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-05-04
% Copyright 2009 Michigan State University.

%First, calculate the residual
if nargin == 2
    simOrRes = simOrRes - obs;
end

%Then, calculate the root-mean-square
rmsr = sqrt(nanmean(simOrRes.^2));
