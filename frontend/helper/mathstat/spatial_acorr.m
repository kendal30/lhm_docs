function [sacf] = spatial_acorr(data,maxLag,minLag)
%SPATIAL_ACORR  Calculates the spatial autocorrelation function of a grid
%   output = spatial_acorr(input)
%
%   Note, right now, this function makes some simplifying assumptions:
%   - The grid is rectangular
%   - Spatial autocorrelation is calculated using Moran's I
%   - Only inverse-distance weighting is used
%
%
%   This is probably not correct quite yet because it does not look at just
%   a specific lag bin, but rather at everything up to a specific lag
%   distance.
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   sacf:   Output spatial autocorrelation function in lags
%
%   Example(s):
%   >> spatial_acorr
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-02-25
% Copyright 2010 Michigan State University.

if nargin < 2
    error('A maximum number of lags must be specified')
end
if nargin < 3
    minLag = 1;
end

%Calculate number of lags
lags = (minLag:maxLag);
sacf = zeros(length(lags));

%Calculate the autocorrelation function
for m = 1:length(lags)
%     weights = build_weights_inv_dist(lags(m));
    weights = build_weights_flat(lags(m));
    sacf(m) = morans_I(data,weights);
end

end

function [weights] = build_weights_inv_dist(lag)
num = 1+2*lag;
center = (num - 1)/2 + 1;
xPos = repmat((1:num),num,1);
yPos = repmat((num:-1:1)',1,num);
dist = sqrt((xPos - xPos(center,center)).^2 + (yPos - yPos(center,center)).^2);
invDist = 1./dist;
invDist(center,center) = 0;
weights = invDist / sum(invDist(:));
end

function [weights] = build_weights_flat(lag)
num = 1+2*lag;
center = (num - 1)/2 + 1;
weights = ones(num,num);
weights(center,center) = 0;
weights = weights ./ sum(weights(:));
end