function [testStat,pValue,testDescription] = kolmogorov_smirnov(x,y,altHypothesis,exact)
%KOLMOGOROV_SMIRNOV [testStat,pValue,testDescription] = kolmogorov_smirnov(x1,x2,altHypothesis,exact)
%
%   This function uses R to calculate the kolmogorov-smirnov test
%   statistic.  It accepts two input data vectors, and performs a two-sided
%   null hypothesis test.  Alternatively, it can test for "less" or
%   "greater" than hypotheses.
%
%   Descriptions of Input Variables:
%   x:     First dataset, must be a vector of data
%   y:     Two options: 1) Second dataset, must be a vector matching the
%           row-/column-orientation of x, or 2) a cell array containing:
%           character string defining a distribution function, and all of
%           the necessary parameters defining that distribution.  See R
%           documentation for more information.
%   altHypothesis:  Optional character array with three values "two.sided",
%                   "less", "greater".  See the R documentation for more
%                   information.
%   exact:  String with value of "NULL", "FALSE", or "TRUE"
%
%   Descriptions of Output Variables:
%   testStat:   this is the test statistic value
%   pValue:     p-value of the test
%   testDescription:    a cell array of dimensions 2x1, containing the
%                       alternative hypothesis and the type of test
%
%   Example(s):
%
%
%   See also:

%Test the altHypothesis to see if it's properly formed
if nargin == 2
    altHypothesis = 'two.sided';
    exact = 'TRUE';
elseif nargin > 2
    if isempty(altHypothesis)
        altHypothesis = 'two.sided';
    end
    if nargin == 3
        exact = 'TRUE';
    end
end
%Test the inputs
assert(any(strcmp(altHypothesis,{'two.sided','less','greater'})),'Illegal value of altHypothesis, see HELP')
assert(any(strcmp(exact,{'NULL','FALSE','TRUE'})),'Illegal value of exact, see HELP')

%Open an R session
openR

%Put the data in the R workspace
putRdata('x',x)
if isnumeric(y)
    putRdata('y',y);
end

%Build the R command
if isnumeric(y)
    arg = 't <- ks.test(x,y,';
elseif iscell(y)
    arg = ['t <- ks.test(x,"',char(y{1}),'",'];
    for m = 2:length(y)
        arg = [arg,char(y{m}),','];
    end
end
%Add on the altHypothesis
arg = [arg,'alternative = "',altHypothesis,'", exact = ',exact,')'];

%Evaluate the R command
evalR(arg);

%Fetch the variable
t = getRdata('t');

%Parse into output
testStat = t{1};
pValue = t{2};
testDescription = t(3:4);

%Close the connection
closeR
