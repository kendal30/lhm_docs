function varargout = multiple_regression(mode,firstArg,varargin)
%MULTIPLE_REGRESSION Compute multiple linear regression and ANOVA in R.
%   varargout = multiple_regression(mode,firstArg,varargin)
%
%   model = multiple_regression('calculate',y,{var1,var2,...varN})
%   [model,anova] = multiple_regression('calculate',y,{var1,var2,...varN})
%   [models,anova] = multiple_regression('calculate',y,{var1,var2,...varN},{varN+1,varN+2,...varN+M},...)
%   testModel = multiple_regression('evaluate',model,var1,var2,var3,...)
%
%   This function uses R to calculate multiple regression using a linear
%   model.  By grouping sets of 'x' variables in cell arrays, this function
%   can fit multiple hierarchical models, and optionally calculate the anova
%   table.  
%
%   It will also evaluate a multiple_regression model.
%
%   Note, the anova isn't perfect yet, particularly the DF seems wrong.
%
%   Descriptions of Input Variables:
%   mode:   Character arguments, 'calculate', or 'evaluate'
%   firstArg:      Regression data, or the 'model' struct returned by this
%           function.  If the 'model' struct is passed, then this function
%           will calculate the multiple regression model using the input
%           'x' variables in varargin.
%   varargin:     Any number of input vectors of size equal to y, these are
%           the 'x' variables, as a cell array.  Group them in separate cell arrays to
%           calculate nested models, and optionally calculate an anova
%           table. If evaluating previously-calculated models, 
%
%   Descriptions of Output Variables:
%   model:  A summary of information from R, including fields for
%           coefficients, r^2 and residuals. Order of coefficients is
%           [intercept,var1,var2,...].  Alternatively, if the function was
%           called with the 'model' struct instead of a 'y' variable, this
%           will be the model-predicted values of y evaluated with the
%           inputs in varargin.  If there are multiple input cells of 'x'
%           variables, this will be a cell array of model structs.
%   anova:  If requested, this function will calculate the anova table for
%           the multiple regression model in R.  If there is only a single
%           model, the anova table will represent successive addition or
%           removal of variables within that single model.  So, use groups
%           of variables to group the analysis.
%
%   Example(s):
%   >> modelLU = multiple_regression(uplandR,{calibUrb,calibAg,calibFor});
%   %calculates a multiple regression model for upland recharge using
%   %urban, agriculture, and forest landcovers
%
%   >> testR = multiple_regression(modelLU,testUrb,testAg,testFor);
%   %evaluates the multiple regression model 'modelLU' with the three
%   %supplied 'x' vectors.
%
%   See also:

%Check to see which function to call
switch lower(mode)
    case 'evaluate'
        varargout{1} = evaluate_model(firstArg,varargin{:});
    case 'calculate'
        %Test
        assert(iscell(varargin{1}),'Input arguments must be grouped as a single (or multiple) cell array(s)');
        
        %Open an R session
        openR
        
        %Now, calculate hierarchically nested models
        varNumStart = 0;
        models = cell(1,length(varargin));
        for m = 1:length(varargin)
            try
                models{m} = calculate_model(firstArg,varargin{m},m,varNumStart);
            catch %#ok<CTCH>
                %Close the connection
                closeR
                return
            end
            varNumStart = varNumStart + length(varargin{m});
        end
        
        %Build the output
        if m == 1
            varargout{1} = models{1};
        else
            varargout{1} = models;
        end
        
        %Check to see if anova table is requested
        if nargout == 2
            try
                anova = calculate_anova(m);
                varargout{2} = anova;
            catch %#ok<CTCH>
                %Close the connection
                closeR
                return
            end
        end
        
        %Close the connection
        closeR
end
end


function model = calculate_model(y,newVars,modelNum,varNumStart)
%Put the data in the R workspace
if modelNum == 1 %If this is the first model
    putRdata('y',y)
end
%Put the new x variables in the workspace
for m = 1:length(newVars)
    putRdata(['x_',num2word(m+varNumStart)],newVars{m});
end

%Build the R command
modelName = ['model_',num2word(modelNum)];
arg = [modelName,' <- lm(y ~ '];
for m = 1:(length(newVars)+varNumStart)
    arg = [arg,'x_',num2word(m),' + ']; %#ok<AGROW>
end
arg = [arg(1:end-3),')'];

%Evaluate the R commands
evalR(arg);
evalR(['modelCoef <- summary(',modelName,')$coefficients'])
evalR(['modelR2 <- summary(',modelName,')$r.squared'])
evalR(['modelResid <- summary(',modelName,')$residuals'])

%Retrieve information from the model
model = struct();
%Get coefficients
allCoef = getRdata('modelCoef');
model.coef.vals = allCoef(:,1);
model.coef.stderr = allCoef(:,2);
model.coef.tval = allCoef(:,3);
model.coef.pval = allCoef(:,4);

%Get residuals and R^2
model.residuals = getRdata('modelResid');
model.rsquared = getRdata('modelR2');
end

function table = calculate_anova(numModels)
%Build the argument
arg = 'modelAnova <- anova(';
for m = 1:numModels
    arg = [arg,'model_',num2word(m),', ']; %#ok<AGROW>
end
arg = [arg(1:end-2),')'];

%Evaluate the R commands
evalR(arg);
if numModels > 1
    evalR('ssr <- modelAnova$RSS')
    evalR('rmsr <- sqrt(ssr / length(modelResid))')
    evalR('df <- length(modelResid) - modelAnova$Res.Df + 1')
else
    evalR('ssr <-modelAnova$"Sum Sq"')
    evalR('rmsr <- sqrt(ssr / length(modelResid))')
    evalR('df <- modelAnova$Df')
end
%Retrieve the anova table as 
table.ssr = getRdata('ssr');
table.rmsr = getRdata('rmsr');
table.df = getRdata('df');
end
function y = evaluate_model(model,varargin)

%Initialize the y variable with the intercept
y = model.coef.vals(1);

%Loop through the variables, adding them to y
coef = model.coef.vals(2:end);
for m = 1:length(coef)
    y = y + coef(m) * varargin{m};
end
end

function [word] = num2word(input)
words = {'one','two','three','four','five','six','seven','eight','nine','ten',...
    'eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen',...
    'nineteen','twenty','twentyone','twentytwo','twentythree','twentyfour','twentyfive'};
if input <= length(words)
    word = words{input};
else
    error(['To support more than ',num2str(input),' variables, modify this function']);
end
end