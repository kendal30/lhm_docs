function [outY,model] = loess(inX,inY,outX,span)
%LOESS  Calculates local linear regression using R
%   output = loess(input)
%
%   Descriptions of Input Variables:
%   input:
%   span: the span variable, usually somethign around 0.5 - 1
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> loess
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-03-12
% Copyright 2010 Michigan State University.

%Open the R interface
openR

%Data should be a single column
assert(all(size(inX)==size(inY)),'Input dataset sizes must match')
if size(inX,2)~=1
    inX = inX(:);
    inY = inY(:);
    outX = outX(:);
    transFlag = true;
else
    transFlag = false;
end
%Put data into R
putRdata('inX',inX);
putRdata('inY',inY);
putRData('outX',outX);

%Run the model
evalR(['modelLoess <- loess(inY ~ inX, span = ',num2str(span),')']);
model = getRdata('modelLoess');

%Predict the result
evalR('predictLoess <- predict(modelLoess, outX)');
outY = getRdata('predictLoess');
outY = outY'; %to match single column

if iscell(outY)
    warning('There were errors in evaluating the outX array within R')
end

%Close the R interface
closeR

%Transpose output to match input
if transFlag
    outY = outY';
end