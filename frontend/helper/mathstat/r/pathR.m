function [pathBin] = pathR()
%This function returns the path of the R binary installation on this
%computer

pathBin = 'C:\Program_Files\R\R-3.0.2\bin\x64\Rscript.exe';

