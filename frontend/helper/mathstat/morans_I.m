function [globalM,localM] = morans_I(grid,W,s)
% PURPOSE: calculate global Moran's I for an input grid (matrix) by calculating all 
%          local Moran's I for a given moving windows size using a weight matrix. 
% -------------------------------------------------------------------
% USAGE: [globalM,localM] = moransI(grid, W, s);
% where: [grid] is the matrix to analyse
%        [W] is the normalized weight matrix of the size the local Moran's
%            I will be calculated for (uneven sized!)
%        [s] is an optional flag to use zscores of input values for
%        calculation. Set to 'true' if zscores of local grid should be
%        calculated. Leave blank if not desired or input values are already
%        standardized. 
% -------------------------------------------------------------------------
% OUTPUTS:
%        [M] matrix of all local Moran's I 
% -------------------------------------------------------------------
% NOTES: Weight matrix needs to be 'moving window' style, not contiguity
%        matrix: Moran's I is calculated and weighted for neighbours to center cell.
%        Matrix needs to be normalized (weights sum to 1) and center cell weight 
%        will be set to 0 if not already. Uses localmoran.m
%        -> Use nanmean(M(:)) to get the average global Moran's I.
%
% See Anselin (1995, 'LISA.', Geogr. Analysis 27(2),p.93f) for details on 
% standardized variables in calculation of local Moran's I. 
%
% EXAMPLE:  M = moransI(rand(20,20),ones(5,5),'true')
%
% Felix Hebeler, Geography Dept.,de University Zurich, March 2006.

%% Check if standardising should be done
if exist('s','var')
    if strcmp(s,'true');
        grid=zscore(grid);
    elseif strcmp(s,'false')
        %do nothing
    else
        error('Invalid option for s: set [true] to calculated zscores to determine local Moran or leave blank if values are already standardized.');
    end
end
if (mod(size(W,1),2)| mod(size(W,2),2))~=1
   error('Weight matrix W needs to have uneven size (eg. 5x5)') 
end
%% Do local Morans I calc of the grid.
localM = NaN(size(grid,1),size(grid,2));
[ncols,nrows] = size(W);
wsx=floor(ncols/2);
wsy=floor(ncols/2);

%Get mean-removed data
gridRemMean = grid - nanmean(grid(:));
% Do local morans I calc for moving window ws
for row=1+wsy:1:size(grid,1)-wsy;
    for col=1+wsx:1:size(grid,2)-wsx;
        localM(row,col) = get_moran(gridRemMean(row-wsx:row+wsx,col-wsy:col+wsy),W,ncols,nrows);
    end
end
%% Calculate global Moran's I
globalM = nanmean(localM(:)) / nanvar(grid(:));
%% calculate local Moran's I
function m=get_moran(raster,W,ncols,nrows)
%  value of center cell (note: no weight applied!)
zi = raster(ceil(nrows/2),ceil(ncols/2));
if (isnan(zi));
    m=NaN; 
    return; 
end;
% Weight values in window
raster = raster.* W;
%set center cell to zero to exclude zi from sum
raster(ceil(nrows/2),ceil(ncols/2))=0; 
% calculate local Moran's I and return
m = nansum(zi * raster(:));
