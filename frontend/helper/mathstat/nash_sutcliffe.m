function efficiency = nash_sutcliffe(sim,obs,option)
%NASH_SUTCLIFFE  Calculates the Nash-Sutcliffe model efficiency
%   efficiency = nash_sutcliffe(sim,obs,option)
%
%   Nash-Sutcliffe model efficiency is calculated as:
%   efficiency = 1 - sum(obs - sim)^2./var(obs)
%
%   Descriptions of Input Variables:
%   sim: simulated time series
%   obs: observed time series
%   option: this optional field can specify modifications of the efficiency
%       calculation, for instance with the 'log' operator which will then
%       log-transform the data prior to calculating the efficiency
%
%   Descriptions of Output Variables:
%   efficiency: Nash-Sutcliffe model efficiency
%
%   Example(s):
%   >> E = nash_sutcliffe(simVal,obsVal,'log'); %this is the
%   log-transformed version of the calculation
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-06-26
% Copyright 2008 Michigan State University.

if nargin > 2
    switch lower(option)
        case 'log'
            test = (sim > 0) & (obs > 0);
            sim = log10(sim(test));
            obs = log10(obs(test));
    end
end

assert(all(size(obs) == size(sim)),'Sizes of simulated and observed time series must be equal');

%Calculate the Nash-Sutcliffe Efficiency
% efficiency = 1 - nansum((obs - sim).^2)/(nanvar(obs)*length(obs));
efficiency = 1 - nansum((obs-sim).^2)/nansum((obs-nanmean(obs)).^2);