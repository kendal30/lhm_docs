function [outY] = loess(inX,inY,outX,span)
%LOESS  Calculates local linear regression using R
%   output = loess(input)
%
%   Descriptions of Input Variables:
%   input:
%   span: the span variable, usually somethign around 0.5 - 1
%
%   Descriptions of Output Variables:
%   output:
%
%   Example(s):
%   >> loess
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-03-12
% Copyright 2010 Michigan State University.

%Data should be a single column
if size(inX,2)~=1
    inX = inX(:);
    inY = inY(:);
    outX = outX(:);
    transFlag = true;
else
    transFlag = false;
end
assert((size(inX,1)==size(inY,1)),'Input dataset num records must match')

%Save the data array in R format
saveR('tempData.R','inX','inY','outX','span');

%Get the script directory and switch slashes
[scriptDir,~,~] = fileparts(which('loessR.m'));
scriptDir = strrep(scriptDir,'\','/');

%Run the script in R
[status,result] = system([pathR,' ',scriptDir,'/loessR.R']);

%Retrieve the data from R
if status>0
    error(result);
    return
end
load('tempResults.mat');

%Clean up
delete('tempData.R','tempResults.mat');

%Transpose output to match input
if transFlag
    outY = outY';
end