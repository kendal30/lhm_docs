function varargout = minmax(vals,dim)
%RANGE  Calculates the minimum and maximum values on an array
%   varargout = range(vals,dim)
%
%   Descriptions of Input Variables:
%   vals:   an array of arbitrary dimensions
%   dim:    optional, dimension to calculate range.  If omitted, will rely
%           on defaults of functions min and max (first non-singleton
%           dimension)
%
%   Descriptions of Output Variables:
%   varargout:  two options: 1) minimum and maximum values along the specified dimension,
%               concatenated along the highest dimension, and then
%               squeezed, or 2) minimum values and maximum values as
%               separate arrays.  If nargout==1, option 1 will be used, if
%               nargout == 2, option 2
%   
%
%   Example(s):
%   >> [rangeVals] = range(testVals); %returns a single array, with
%   singleton dimensions squeezed out
%   >> [rangeMin,rangeMax] = range(testVals); %returns two arrays, each
%   with dimensions give by min and max functions
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-03-05
% Copyright 2009 Michigan State University.

%First, identify the cat dimension
dimCat = ndims(vals)+1;

%Then, run the min and max functions
if nargin < 2
    minVals = min(vals);
    maxVals = max(vals);
else
    minVals = min(vals,[],dim);
    maxVals = max(vals,[],dim);
end

if nargout < 2
    %Now, concatenate the output
    varargout{1} = squeeze(cat(dimCat,minVals,maxVals));
elseif nargout==2
    varargout{1} = minVals;
    varargout{2} = maxVals;
end