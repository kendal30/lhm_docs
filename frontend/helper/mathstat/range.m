function xOut = range(xIn,dim)
%This is just a functional replicate of the MATLAB stats toolbox package. 

if nargin == 1
    xOut = max(xIn) - min(xIn);
else
    xOut = max(xIn,[],dim) - min(xIn,[],dim);
end
