function [out]=moving_average(data,num)
out=zeros(size(data));
for m=1:num
    out(m)=mean(data(1:m));
    out(num+1:end)=out(num+1:end)+data(num+1-m:end-m);
end
out(num+1:end)=out(num+1:end)/num;
