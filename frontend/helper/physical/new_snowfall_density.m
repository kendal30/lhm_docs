function [rho] = new_snowfall_density(airTemp)
%NEW_SNOWFALL_DENSITY  One-line description here, please.
%   [rho] = new_snowfall_density(airTemp)
%
%   Calculates new snowfall density, same as in CLM manual, v.3.0
%
%   Descriptions of Input Variables:
%   airTemp: temperature of the air (C)
%
%   Descriptions of Output Variables:
%   rho: density of the snowfall (kg/m^3)
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-02
% Copyright 2008 Michigan State University.

%Assume constant density above 2 degrees C, and below 15
airTemp = limit_vals([-15,2],airTemp);

%Calculate the density of snowfall
rho = 50 + 1.7 * (airTemp + 15).^(1.5);
