function [pressure] = barometric_formula(temperature,elevation,seaLevelPress)
%BAROMETRIC_FORMULA  Calculate static air pressure.
%   pressure = barometric_formula(temperature,elevation)
%
%   This function outputs the air pressure at a specified temperature and elevation
%   assuming an exponential distribution from the barometric formula.
%
%   Descriptions of Input Variables:
%   temperature: temperature in degrees C, can be an array
%   elevation: elevation in m, can be an array, must match size of temperature unless
%       scalar
%   seaLevelPress: optionally, sea level pressure in Pa
%
%   Descriptions of Output Variables:
%   pressure: static air pressure in Pa
%
%   Example(s):
%   >> pressure = barometric_formula(temperature,elevation);
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

dryGasConst = 287.05; %J/kg/K
gravConst = 9.80665; %m/s^2 at sea-level and 44.5N latitude

if nargin < 3
    seaLevelPress = 101325; %Pa
end
    
pressure = seaLevelPress * exp(-gravConst * elevation ./ ...
    (dryGasConst*unit_conversions(temperature,'C','K')));

