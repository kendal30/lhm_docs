function rho = water_density(temperature)
%WATER_DENSITY  Calculate temperature-dependent freshwater density
%   rho = water_density(waterTemp)
%
%   Formula from Hostetler and Bartlein, 1990
%
%   Descriptions of Input Variables:
%   temperature: temperature of the water, in C
%
%   Descriptions of Output Variables:
%   rho: density of the water, kg/m^3
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-30
% Copyright 2008 Michigan State University.

%Declare the lookup table as a persistent variable
persistent lookup

%If the input data are a vector, check to see if the output will have to be
%transposed, as by default the lookup table produces row vectors
transp = false;
if isvector(temperature)
    if size(temperature,2) == 1,transp = true;end
end

if isstruct(lookup)
    valsIndex = uint32((temperature - lookup.minVal) * lookup.mult) + 1;
    %Lookup the water density
    rho = lookup.values.rho(valsIndex);
    if transp,rho = rho';end
else
    %Create the lookup table
    lookup = lookup_table_creation();
    
    %Run this function again
    [rho] = water_density(temperature);
end

end

function [table] = lookup_table_creation()
%Set the temperature range for the lookup table
rangeSpacing = 0.001;
temperature = (-30:rangeSpacing:50);

%Build the lookup table
table.minVal = temperature(1);
table.mult = 1 / rangeSpacing;

%Convert to K, which is what all these equations work in
temperature = unit_conversions(temperature,'C','K');

%Finish building the lookup table
table.values.rho = 1000 * (1 - 1.9549e-5 * abs(temperature - 277).^1.68);
end