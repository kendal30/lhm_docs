function [days,tauCloud] = bristow_campbell(time,temp,params)
%BRISTOW_CAMPBELL  Calculate Bristow-Campbell cloudy transmissivity
%   [days,tauCloud] = bristow_campbell(time,temp,params)
%
%   Equations of form, from Tarboton and Luce 1996 (UEB docs.):
%   b = b1 * exp(-b2 .* deltaTBar);
%   tauCloud = 1 - exp(-b .* deltaT .^ c);
%
%   Descriptions of Input Variables:
%   time: input times, a single vector of size Mx1
%   temp: input temperature, of size MxN
%   params: an optional input of form [b1,b2,c] for the bristow campbell
%           parameters
%
%   Descriptions of Output Variables:
%   days: matlab datenumber vector of size Px1
%   tauCloud: transmissivity due to cloudiness, of size PxN
%
%   Example(s):
%   >> bristow_campbell
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-01-14
% Copyright 2010 Michigan State University.

%Specify Bristow-Campbell parameters
if nargin == 2
    b1 = 0.036;
    b2 = 0.154;
    c = 2.4;
else
    b1 = params(1);
    b2 = params(2);
    c = params(3);
end

%Specify a couple of of parameters that aren't really intended to be
%changed
minDeltaT = 0.5;

%Create anonymous functions to do work
diffRange = @(x)diff([min(x,[],1);max(x,[],1)]);
daysMostHours = @(x)sum(isnan(x),1)<=6;

%Calculate daily values
[days,deltaT,dailyMask] = time_series_aggregate(time,temp,'days',{diffRange,daysMostHours});
%[days,dailyMin,dailyMax,dailyMask] = time_series_aggregate(time,temp,'days',{'min','max',daysMostHours});
%dailyMinAvg = zeros(size(dailyMin));
%dailyMinAvg(2:end,:) = (dailyMin(1:end-1,:) + dailyMin(2:end,:)) / 2;
%deltaT = dailyMax - dailyMinAvg;
deltaT(~dailyMask) = NaN;
deltaT(deltaT<minDeltaT) = minDeltaT;

%Calculate monthly values
[months,monthlyRange] = time_series_aggregate(days,deltaT,'months','mean');

%Replicate monthly array
daysDatevec = datevec(days);
monthsDatevec = datevec(months);
deltaTBar = zeros(size(deltaT));
numDays = size(daysDatevec,1);
numMonths = size(monthsDatevec,1);
for m = min(daysDatevec(:,1)):max(daysDatevec(:,1))
    for n = 1:12
        daysInd = all(daysDatevec(:,1:2) == repmat([m,n],numDays,1),2);
        monthsInd = all(monthsDatevec(:,1:2) == repmat([m,n],numMonths,1),2);
        if any(daysInd)
            deltaTBar(daysInd,:) = repmat(monthlyRange(monthsInd,:),sum(daysInd),1);
        end
    end
end
    
%Calculate cloudy transmissivity
b = b1 * exp(-b2 .* deltaTBar);
tauCloud = 1 - exp(-b .* deltaT .^ c);

end