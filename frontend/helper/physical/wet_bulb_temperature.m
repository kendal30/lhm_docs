function [wetBulb] = wet_bulb_temperature(dryBulb,relHum)
%WET_BULB_TEMPERATURE  Calculates the wet-bulb temperature from relative humidity and dry-bulb temperature.
%   [wetBulb] = wet_bulb_temperature(dryBulb,relHum)
%
%   This solution for the wet bulb temperature is from ???
%
%   Descriptions of Input Variables:
%   temperature: drybulb temperature in (C)
%   relHum: fractional relative humidity, defined as relHum = vapPress/satVapPress
%
%   Descriptions of Output Variables:
%   wetBulb: wet bulb temperature in (C)
%
%   Example(s):
%   none
%
%   See also: dewpoint_rel_hum

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-21
% Copyright 2008 Michigan State University.

%Make first guess of wetBulb
wetBulb = 0.7 * dryBulb;

%Use Newton-Rhaphson iteration to iterate for the wet bulb temperature
tol = 0.0001;
fff = 0.999;
error = 1;
niter = 0;

while (error>tol) && (niter<20)
    wetBulbPrev = wetBulb;
    f1 = relHum - relative_humidity_calc(dryBulb,wetBulb);
    f2 = relHum - relative_humidity_calc(dryBulb,wetBulb*fff);
    wetBulb = wetBulb - ((fff-1) * wetBulb .* f1) ./ (f2 - f1);
    %Here, NaN values can appear if f2 and f1 are too similar, if so,
    %simply fix wetBulb as the previous value
    wetBulb(isnan(wetBulb)) = wetBulbPrev(isnan(wetBulb));
    error = mean(abs(wetBulb(:) - wetBulbPrev(:)));
    niter = niter + 1; 
end

end

function relHumPredict = relative_humidity_calc(dryBulb,wetBulb)
relHumPredict = (6.112 .* exp(17.67 * wetBulb ./ (wetBulb + 243.5)) - 0.92 .* (dryBulb - wetBulb) .*...
    0.00066 .* (1 + 0.00115 * wetBulb)) ./ (6.112 .* exp(17.67 * dryBulb ./ (dryBulb + 243.5)));
end