function relHum = dewpoint_rel_hum(dewpoint,temperature)
%DEWPOINT_REL_HUM  Calculate relative humidity from dewpoint data
%   relHum = dewpoint_rel_hum(dewpoint,temperature)
%
%   This equation is from Lawrence, 2005, Bull. Amer. Met. Soc.
%
%   Descriptions of Input Variables:
%   dewpoint: dewpoint in (C)
%   temperature: drybulb temperature in (C)
%
%   Descriptions of Output Variables:
%   relHum: relative humidity in %
%
%   Example(s):
%   none
%
%   See also: rel_hum_dewpoint

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-21
% Copyright 2008 Michigan State University.

gasConst = 461.495; %J/kg/K
latentHeatEvap = latent_heat_evap(temperature);
dewpoint = unit_conversions(dewpoint,'C','K');
temperature = unit_conversions(temperature,'C','K');
relHum = exp(-latentHeatEvap ./ (gasConst * dewpoint .* temperature) .* (temperature - dewpoint));
relHum = unit_conversions(relHum,'dec','pcent');


