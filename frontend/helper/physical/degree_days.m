function [days,ddCum,ddDaily] = degree_days(timeIn,tempIn,threshold,heatorcool,algorithm)
%DEGREE_DAYS Calculates heating/cooling degree days with arbitrary threshold
%   [days,ddCum,ddDaily] = degree_days(timeIn,tempIn,threshold,heatorcool,algorithm)
%
%   Calculates growing degree days according to two formulae:
%   1) 'maxmin':   DD = +/-(Tmaxdaily + TminDaily)/2 +/- Tthreshold;
%   2) 'continuous': DD = sum(+/-T +/- Tthreshold)/24;  for hourly data
%
%   This function will begin accumulating growing degree days at the start
%   of the input time series.  So, if the user desires to have the ddCum
%   reset at the first of the year, or the minimum winter temperature, it's
%   their responsibility to provide only that input data.  
%
%   Descriptions of Input Variables:
%   timeIn:     input time vector, MATLAB serial datenumber format.  Input
%               data must be either daily or hourly, size Mx1.
%   tempIn:     temperature in units of the threshold temperature.  If
%               hourly data are used, this must be MxN array, where M is the
%               number of hours, and N is the number of vectors to operate 
%               over (i.e. cells in a model). If daily inputs are used,
%               then the size of this depends on the algorithm chosen. If
%               "maxmin" is used, then this must be an cell array with two
%               MxN vectors, where M is the number of days. If the
%               algorithm is "continuous" then this input must be an array
%               of size MxN.
%   threshold:  A scalar or 1x2 vector of threshold temperatures to use for degree
%               daycalculations. Of the form [Threshold,Saturate].  For
%               some degree day calculations, like growing degree days, any
%               temperatures in excess of a certain value (say 30C) do not
%               increase growth rates.
%   heatorcool: If heating degree days (or growing) are desired, set this
%               equal to 1.  If cooling is desired, set it equal to -1.
%   algorithm:  an optional character array specifying which algorithm to
%               use for calculations: 'maxmin' or 'continuous'.  If
%               'maxmin' is used, either daily or hourly inputs can be
%               specified.  If 'continuous' is used, then the input
%               temperatures cannot be of form [Tmax,Tmin], but would
%               rather then be a daily average only.
%
%   Descriptions of Output Variables:
%   days:      MATLAB serial time vector of the output days
%   ddCum:     Cumulative growing degree days
%   ddDaily:   Daily values of dd. ddCum is derived from this as
%               ddCum = cumsum(ddDaily)
%
%   Example(s):
%   [days04,ddCum04,ddDaily04] = degree_days(hours04, temp04, [10,30],1,'maxmin');
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-01-26
% Copyright 2009 Michigan State University.

%Check to see if inputs are daily or hourly
if (timeIn(2) - timeIn(1)) < 1 %these are hourly data
    timeType = 'hourly';
    if strcmpi(algorithm,'maxmin')
        %calculate the daily max/min temperature
        [days,dailyMax,dailyMin] = time_series_aggregate(timeIn,tempIn,'days',{'max','min'});
    end
else %these are daily data
    timeType = 'daily';
    days = timeIn;
    if strcmpi(algorithm,'maxmin')
        assert(iscell(tempIn),'If maxmin is chosen with daily temperature, then tempIn must be a cell array');
        dailyMax = tempIn{1};
        dailyMin = tempIn{2};
    elseif strcmpi(algorithm,'continuous')
        assert(~iscell(tempIn), 'If continuous algorithm is used with daily values, input must be an MxN array')
    end
end

%Determine saturation threshold
if length(threshold)== 1
    if heatorcool == 1
        threshold(2) = Inf;
    elseif heatorcool == -1
        threshold(2) = -Inf;
    end
end

%Determine the constraints
if heatorcool == 1
    constraint = [threshold(1),threshold(2)];
elseif heatorcool == -1
    constraint = [threshold(2),threshold(1)];
end

%Run limit_vals
if strcmpi(algorithm,'maxmin')
    [dailyMax,dailyMin] = limit_vals(constraint,dailyMax,dailyMin);
elseif strcmpi(algorithm,'continuous')
    [tempIn] = limit_vals(constraint,tempIn);
end


%Calculate degree days
if strcmpi(algorithm,'maxmin')
    %Calculate the daily degree days
    ddDaily = heatorcool * (dailyMax + dailyMin)/2 - heatorcool * threshold(1);
    
elseif strcmpi(algorithm,'continuous')
    if strcmpi(timeType,'hourly')
        %Calculate hourly degree days
        ddHourly = (heatorcool * tempIn - heatorcool * threshold(1)) / 24;
        
        %Sum these daily
        [days,ddDaily] = time_series_aggregate(timeIn,ddHourly,'days','sum');
    elseif strcmpi(timeType,'daily')
        %Calculate daily degree days
        ddDaily = (heatorcool * tempIn - heatorcool * threshold(1));
    end
end

%Finally, calculate the cumulative dd
ddCum = cumsum(ddDaily);