function [tauBClear,tauDClear] = yang_koike_clear_sky(sun,climate,location)
%YANG_KOIKE_CLEAR_SKY  Clear-sky transmissivity from Yang and Koike 2005
%   [tauBClear,tauDClear] = yang_koike_clear_sky(sun,climate,location)
%
%   Calculates clear-sky transmissivity based on Yang and Koike 2005.
%   Currently does not account for topographic effects.
%
%   Outputs are in W/m^2
%
%   Descriptions of Input Variables:
%   sun: an input structure with fields 'zenith', and 'time',
%       with all arrays having dimensions (mxn)
%   climate: an input structure with fields 'relHum', 'airTemp',
%       'pressure'
%   location: an input structure with fields 'latitude', 'longitude', and
%       'altitude'.  Each field is of dimension (mxn) as above
%
%   Descriptions of Output Variables:
%   tauBClear: beam clear-sky transmissivity
%   tauDClear: diffuse clear-sky transmissivity
%
%   Example(s):
%   >> yang_koike_clear_sky
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-01-14
% Copyright 2010 Michigan State University.

%Necessary constants
p0 = 1.013 * 10^5; %sea-level pressure (Pa)

%Calculate for convenience
h = unit_conversions(90 - sun.zenith,'deg');
h = h .* (sun.zenith < 90); %enforce the condition that the sun is not beneath the horizon
z = location.altitude; %z is surface elevation (m)
rh = climate.relHum; %rh is relative humidity in %
T = unit_conversions(climate.airTemp,'C','K'); %T is surface air temperature
phi = location.latitude;
p = climate.pressure;
dayOfYear = date2doy(datenum(sun.time));

%now calculate Yang and Koike model parameters
w = 0.00493 * rh ./ T .* exp(26.23 - 5416 ./ T); %w is precipitable water
d = (dayOfYear < 300) .* dayOfYear + (dayOfYear >= 300) .* (dayOfYear - 366);
l = 0.44 - 0.16 * sqrt(((abs(phi) - 80) / 60).^2 + ((d - 120) ./ (263 - abs(phi))).^2); %l is thickness of the ozone
beta = (0.025 + 0.1 * cos(unit_conversions(phi,'deg')).^2) .* exp(-0.7 * z / 1000); %beta is the angstrom turbidity coefficient
m = 1 ./ (sin(h) + 0.15 * (57.296 * h + 3.885).^-1.253); %relative air mass
mPrime = m .* p / p0; %pressure-corrected air mass

%Calculate Transmissivity components
tauGas = exp(-0.0117 * mPrime.^(0.3139));
tauRay = exp(-0.008735 * mPrime .* (0.547 + 0.014 * mPrime - 0.00038 * mPrime.^2 + 4.6 * 10^-6 * mPrime.^3).^-4.08);
tauWater = min(1.0,0.909 - 0.036 * log(m .* w));
tauOzone = exp(-0.0365 * (m .* l).^0.7136);
tauAero = exp(-m .* beta .* (0.6777 + 0.1464 * (m .* beta) - 0.00626 * (m .* beta).^2).^-1.3);

%Calculate beam and diffuse transmissivity
tauBClear = max(0,tauOzone .* tauWater .* tauGas .* tauRay .* tauAero - 0.013);
tauDClear = 0.5 * (tauOzone .* tauGas .* tauWater .* (1 - tauAero .* tauRay) + 0.013);
end