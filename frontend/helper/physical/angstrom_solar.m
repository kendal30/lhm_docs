function output = angstrom_solar(input,period,option,params)
%ANGSTROM_SOLAR  One-line description here, please.
%   output = angstrom_solar(input,period,option)
%
%   Descriptions of Input Variables:
%   input:  either a clear fraction (fraction of day or hour with clear sky,
%           or, for 'forward' option) or a cloudy transmissivity (for
%           'inverse' option).
%   period: time period, either 'daily' or 'hourly'
%   option: either 'forward' or 'inverse'. Forward will calculate cloudy
%           transmissivity from clear fraction data, inverse will calculate
%           clear fraction from transmissivity.
%   params: optiononally, specify model parameters, of form [b1,b2,b3] if
%           daily, or [a1,a2,a3,a4] if hourly
%
%   Descriptions of Output Variables:
%   output: if option is 'forward', output will be cloudy transmissivity,
%           if 'inverse', output will be clear fraction
%
%   Example(s):
%   >> angstrom_solar
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-01-14
% Copyright 2010 Michigan State University.

if nargin == 3
    switch lower(period)
        case 'hourly'
            %Set angstrom model constants here, from Yang and Koike paper
            a1 = 0.4560; %angstrom coefficient 1, hourly
            a2 = 0.3566; %angstrom coefficient 2, hourly
            a3 = 0.1874; %angstrom coefficient 3, hourly
            a4 = 0.2640; %angstrom coefficient 4, hourly, completely overcast
        case 'daily'
            b1 = 0.2491; %angstrom coefficient 1, daily
            b2 = 1.3182; %angstrom coefficient 2, daily
            b3 = -0.5673; %angstrom coefficient 3, daily
    end
elseif nargin == 4
    switch lower(period)
        case 'hourly'
            [a1,a2,a3,a4] = deal(params);
        case 'daily'
            [b1,b2,b3] = deal(params);
    end    
else
    error('Number of input arguments is incorrect')
end

switch lower(option)
    case 'forward'
        switch lower(period)
            case 'hourly'
                output = (input > 0) .* (a1 + a2 * input + a3 * (input).^2) + (input == 0) * (a4);%hourly
            case 'daily'
                output = b1 + b2 * (input) + b3 * (input)^2; %daily
            otherwise
                error('period argument not recognized')
        end
    case 'inverse'
        switch lower(period)
            case 'hourly'
                error('hourly inverse not yet implemented')
            case 'daily'
                output = zeros(size(input));
                input = input(:);
                for m = 1:length(input)
                    if ~isnan(input(m))
                        p = [b3,b2,b1-input(m)];
                        r = roots(p);
                        test = (r>0 & r<1) & real(r);
                        if ~any(test)
                            output(m) = 0;
                        else
                            output(m) = r(r>0 & r<1);
                        end
                    else
                        output(m) = NaN;
                    end
                end
            otherwise
                error('period argument not recognized')
        end         
    otherwise
        error('option argument not recognized')
end
