function [rain,snow] = rain_snow_partition(precip,airTemp)
%RAIN_SNOW_PARTITION  One-line description here, please.
%   [rain,snow] = rain_snow_partition(precip,airTemp)
%
%   Applies rain/snow partitioning across multiple stations using a single
%   airTemp time series.
%
%   Descriptions of Input Variables:
%   precip: precipitation rate (units don't matter).  Size is MxN
%   airTemp: air temperature in (C), size is Mx1 or MxN
%
%   Descriptions of Output Variables:
%   rain: rate of rainfall (same units as precip). Size is MxN
%   snow: rate of snowfall (Same units as precip). Size is MxN
%
%   Example(s):
%   >> [rain,snow] = rain_snow_partition(precip,airTemp);
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-04-03
% Copyright 2008 Michigan State University.

thresholdTemp = 3; %(C), modified from CLM manual

fracLiquid = zeros(size(airTemp));
%See CLM documentation, Offline Mode section
indLiquidAll = (airTemp >= thresholdTemp);
indFixedFrac = (airTemp >= 2) & (airTemp < thresholdTemp);
indVarFrac = (airTemp > 0) & (airTemp < 2);
%Populate array
fracLiquid = populate_array(fracLiquid,1,0.4,(-54.629 + 0.2 * unit_conversions(airTemp,'C','K')),...
    indLiquidAll,indFixedFrac,indVarFrac);
%Partition precip, loop through stations
[rain,snow] = deal(zeros(size(precip)));
if size(airTemp,2) == 1
    for m = 1:size(precip,2);
        rain(:,m) = fracLiquid .* precip(:,m);
        snow(:,m) = precip(:,m) - rain(:,m);
    end
else
    rain = fracLiquid .* precip;
    snow = precip - rain;
end

