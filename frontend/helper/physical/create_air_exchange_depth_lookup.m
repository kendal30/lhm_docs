%% Depth of fluctuation
%This requires relatively modern (say R2015+) version of MATLAB

%This script will have these loops
%log10(Kair) = (-13:0.5:-3.5);
%T, in 10 degree increments from -10:50 C (will get 7)
%theta, in 3 percent increments from 0.02 to 0.59 (range of my data, will give 20)
%A0, in 50 Pa increments from 100 to 600 (will give 11)

%Set up the parallel job
parWorkers = 4;
p = parpool('local',parWorkers); 

%For convenience
Klimits = [-13,-3.5];
Tlimits = [-10,50];
P0limits = [100,600];
Thetalimits = [0.02,0.59];

%Set up the parameters                
Kloop = (Klimits(1):0.5:Klimits(2));
Tloop = (Thetalimits(1):10:Tlimits(2));
P0loop = (P0limits(1):50:P0limits(2));
Thetaloop = (Thetalimits(1):0.03:Thetalimits(2));
exchDepth = zeros([length(Kloop),length(Tloop),length(P0loop),length(Thetaloop)]);
loopSize = size(exchDepth);
more off
% bar = waitbar(0,'Outermost Loop');


%% Do the parallel loop
%Make the interpolation grid
[Kaxis,Taxis,P0axis,Thetaaxis] = ndgrid(Kloop,Tloop,P0loop,Thetaloop);

%Make this parallel to increase the speed
exchDepthFlat = exchDepth(:)+NaN;
paramsTable = table(Kaxis(:),Taxis(:),P0axis(:),Thetaaxis(:),'VariableNames',{'K','T','P0','Theta'});

loopSizeFlat = prod(loopSize);
parfor ind = 1:loopSizeFlat
    exchDepthFlat(ind) = calc_exchange_depth(10^paramsTable.K(ind),273.15+paramsTable.T(ind),paramsTable.P0(ind),paramsTable.Theta(ind));
end
exchDepth = reshape(exchDepthFlat,size(Kaxis));
% save('exchange_depth.mat','paramsTable','exchDepthFlat','exchDepth','*axis');
save('exchange_depth.mat','exchDepth','*axis','*limits');

%% Optimize a nonlinear function for exchange depth -- this didn't turn out well
%Subset the table for values > 0 < 0.1
indSubset = (exchDepthFlat > 0) & (exchDepthFlat < 0.1);

%Add exchDepthFlat as a variable
paramsFit = paramsTable;
paramsFit.exchDepth = exchDepthFlat;
paramsFit = paramsFit(indSubset,:);
paramsFit.T = paramsFit.T + 273.15; %to get to K

%Order of variables in the Table is {'lnK'  'T'  'P0'  'Theta'}
% modelFun = @(b,x)b(1)+b(2)*exp(x(:,1)*b(3))+b(4)*x(:,2)+b(5)*x(:,3)+b(6)*exp(x(:,4)*b(7));
% b0 = [0,1,1,0,1,1,20];
modelFun = @(b,x)b(1)+b(2)*exp(x(:,1)*b(3)+x(:,4)*b(6))+b(4)*x(:,2)+b(5)*x(:,3);
b0 = [0,1,1,1,1,20];
% modelFun = @(b,x)b(1)+b(2)*exp(x(:,1)*b(3))+b(4)*x(:,2)+b(5)*x(:,3);
% b0 = [0,1,1,1,1];
% modelFun = @(b,x)b(1)+x(:,4)*b(2);%+x(:,4)*b(3);
% b0 = [1,15];%,15];
model = fitnlm(paramsFit,modelFun,b0);
plot(model.predict(paramsFit),paramsFit.exchDepth,'.')

%% Plot stuff
%Site-specific parameters
% Ksat = 1.48e-6; %Hydraulic conductivity, loam, matched properties below
% thetaResid = 0.057;
% thetaSat = 0.40;
% alpha = 0.892;
% N = 1.517;
% L = -0.12095;
% k0 = 3.55e-7;

Ksat = 5.4e-5; %Hydraulic conductivity, sand, matched properties below
thetaResid = 0.04965;
thetaSat = 0.3791;
alpha = 0.3491;
N = 2.880;
L = -0.88417;
k0 = 2.84e-6;

%State variables
T = 290; %K
P0 = 200; %amplitude of diurnal variability
theta = 0.1;

%Physical constants
muW = 8.9e-4;%Pa*s
muAir = 1.81e-5; %Pa*s
rhoW = 1000; %kg*m-3
Rair = 285.05; %J*kg-1*K-1
g = 9.8066; %m*s-2

%Calculate composite variables
thetaAir = (thetaSat - theta);
kappaSat = Ksat * muW / (rhoW * g);
% kappaTheta = kappaSat .* thetaAir / (thetaSat - thetaResid);
Kair = van_genuchten_model(thetaAir,thetaSat,thetaResid,alpha,N,Ksat,k0,L);
kappaTheta = Kair * muW / (rhoW * g);

%Calculate composite values
% a = sqrt(Rair * T * Kair * muW / (rhoW * g));
a = sqrt(Rair * T * kappaTheta);
w = 2*pi()/86400; %in s^-1
d = (2*a/w)^0.5; %units are now m

%Transient
delZ = 0.00001; %meters
delt = 10; %seconds
t = 0:delt:86400*2;

startInd = (2:floor(length(t)/39):length(t));
zAir = zeros(length(t),40)+0.06;
%Now, calculate depth of air movement
for p = 1:size(zAir,2)
    for f = startInd(p):length(t)
        P1 = P0 * exp(-zAir(f-1)/d) .* sin(2*pi()*t(f)/86400 - zAir(f-1)/d - pi()/2);
        P2 = P0 * exp(-(zAir(f-1)+delZ)/d) .* sin(2*pi()*t(f)/86400 - (zAir(f-1)+delZ)/d - pi()/2);
        Pgrad = (P2-P1)/delZ;
        %     vAir = -Kair * muW / (muAir * rhoW * g * thetaAir) * Pgrad; %viscous only
        vAir = -(Kair * muW / (muAir * rhoW * g * thetaAir) + ...
            8/(3*sqrt(pi())) * sqrt(Kair * muW/(rhoW * g * Rair *T)))* Pgrad;
        zAir(f,p) = zAir(f-1,p)+vAir*delt;
        if zAir(f,p) < 0;zAir(f,p) = 0;end
    end
end
            
plot(t/86400*24,zAir)
set(gca,'Ydir','reverse')
ylabel('Depth (m)');
xlabel('Hours');

%% Plot stuff
figure
set(gca,'YDir','reverse')
set(gca,'XLim',[-A0,A0]);
set(gca,'YLim',[0,10]);
ylabel('Depth (m)');
xlabel('Gauge Pressure (Pa)')
ax = gca;
ax.NextPlot = 'replaceChildren';



t = [0:100:86400];
z = [0:0.01:10];
frames = length(t);
F(frames) = struct('cdata',[],'colormap',[]);
for f = 1:length(t)
    P = A0 * exp(-z/d) .* sin(2*pi()*t(f)/86400 - z/d - pi()/2);
    plot(P,z);set(gca,'YDir','Reverse');
    drawnow
    F(f) = getframe;
end