function [exchDepth] = calc_exchange_depth(Kair,T,P0,thetaAir)

%Physical constants
muW = 8.9e-4;%Pa*s
muAir = 1.81e-5; %Pa*s
rhoW = 1000; %kg*m-3
Rair = 285.05; %J*kg-1*K-1
g = 9.8066; %m*s-2

%Transient
delZ = 0.00001; %meters
delt = 10; %seconds
t = 0:delt:86400*2;
                
%Calculate composite values
a = sqrt(Rair * T * Kair * muW / (rhoW * g));
w = 2*pi()/86400; %in s^-1
d = (2*a/w)^0.5; %units are now m

% %Solve for P(t) at all depths
% z = [0:0.001:1];
% t = 86400/8;
% P = P0 * exp(-z/d) .* sin(2*pi()*t/86400 - z/d - pi()/2);

%Calculate proportion of time that particles reach the surface
startInd = (8*3600/delt:1800/delt:20*3600/delt);
zStart = (0:0.001:0.1);
zSurf = zeros(length(zStart),length(startInd));

for o = 1:size(zSurf,1)
    testNoSurf = true;
    for p = 1:size(zSurf,2)
        zAirLast = zStart(o);
        for f = startInd(p):length(t)
            P1 = P0 * exp(-zAirLast/d) .* sin(2*pi()*t(f)/86400 - zAirLast/d - pi()/2);
            P2 = P0 * exp(-(zAirLast+delZ)/d) .* sin(2*pi()*t(f)/86400 - (zAirLast+delZ)/d - pi()/2);
            Pgrad = (P2-P1)/delZ;
            %     vAir = -Kair * muW / (muAir * rhoW * g * thetaAir) * Pgrad; %viscous only
            vAir = -(Kair * muW / (muAir * rhoW * g * thetaAir) + ...
                8/(3*sqrt(pi())) * sqrt(Kair * muW/(rhoW * g * Rair *T)))* Pgrad;
            zAir = zAirLast+vAir*delt;
            if zAir <= 0
                zSurf(o,p) = 1;
                testNoSurf = false;
                break;
            end
            zAirLast = zAir;
        end
    end
    if testNoSurf %particles never reached the surface at that layer, so they won't deeper either
        break;
    end
end

%Find depth at which particles from half of this period can reach the surface
depthInd = find(sum(zSurf,2)>=size(zSurf,2)/2,1,'last');
if ~isempty(depthInd)
    exchDepth = zStart(depthInd);
else
    exchDepth = 0;
end

end