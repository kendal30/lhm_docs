function hourlyIrr = hourly_irradiance(sun,irradiance,option)
%HOURLY_IRRADIANCE  Calculates hourly irradiance from a constant or
%lower-resolution timeseries
%   output = hourly_irradiance(sun,irradiance,option)
%
%   Irradiance input must currently be constant, annual average, or daily.
%
%   Descriptions of Input Variables:
%   sun:    structured array containing variables 'distance', 'zenith',
%           'time', and 'zenithCos'
%   irradiance: value or array of irradiance corresponding to option
%   option: either a character array 'constant' or array with datenumbers
%           to be interpolated
%
%   Descriptions of Output Variables:
%   hourlyIrr: hourly values of exoatmospheric irradiance in (W/m^2)
%
%   Example(s):
%   >> hourly_irradiance(sun,1366,'constant')
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-01-14
% Copyright 2010 Michigan State University.

%First, create the input irradiance array
if ischar(option) %The input is constant
    irrTime = [sun.time(1),sun.time(end)];
    irradiance = [irradiance,irradiance];
    dailyFlag = false;
else
    assert(size(irradiance) == size(option),'Input irradiance and datenumber sizes must match');
    irrTime = option;
    
    %Check to see if this is annual or daily
    startVec = datevec(irrTime(1));
    endVec = datevec(irrTime(end));
    numYears = endVec(1) - startVec(1) + 1;
    numDays = irrTime(end) - irrTime(1) + 1;
    
    %Check to see if this is annual
    if length(irrTime) == numYears
        dailyFlag = false;
    elseif length(irrTime) == numDays
        dailyFlag = true;
    else
        error('Input irradiance timeseries must be either daily or hourly');
    end 
end

%Calculate hourly irradiance on a horizontal surface
%Now, handle this a couple of ways, if irradiance is daily, then skip a
%step
if dailyFlag
    solarNormal = interp1(irrTime,irradiance,sun.time);
else
    solarNormal = interp1(irrTime,irradiance,sun.time) .* sun.distance.^2; %correct for variation in sun-earth distance
end

%Correct for incidince angle
hourlyIrr = solarNormal .* sun.zenithCos;
hourlyIrr = hourlyIrr .* (sun.zenith < 90);

    