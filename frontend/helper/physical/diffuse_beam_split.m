function [diffFrac,beamFrac] = diffuse_beam_split(radShort,sunZenithCos)
%DIFFUSE_BEAM_SPLIT  Partitions shortwave radiation between diffuse and beam
%components
%   [diffFrac,beamFrac] = diffuse_beam_split(radShort,sunZenith)
%
%   Descriptions of Input Variables:
%   radShort: input shortwave radiation in W/m^2
%   sunZenithCos: cosine of the sun zenith angle
%
%   Descriptions of Output Variables:
%   diffFrac: diffuse radiation fraction (beamFrac is 1-diffFrac)
%   beamFrac: 1 - diffFrac
%
%   Example(s):
%   >> diffuse_beam_split
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-01-14
% Copyright 2010 Michigan State University.

%[[This doesn't work correctly!]]

solarConst = 1366; %W/m^2
%This is from Chen et al. 2005
r = radShort ./ (solarConst .* sunZenithCos);
r(isnan(r)) = 0;
r(r<0) = 0;

diffFrac = zeros(size(radShort)) + 0.13; %initialize the diffuse fraction
ind = (r < 0.8);
diffFrac(ind) = 0.943 + 0.734 .* r(ind) - 4.9 .* r(ind).^2 + 1.796 .* r(ind).^3 + 2.058 .* r(ind).^4;

if nargout == 2
    beamFrac = 1 - diffFrac;
end

end