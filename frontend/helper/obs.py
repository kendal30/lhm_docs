def create_obs_table(outPath,outName):
    """This helper function creates an empty observations table, and this is the
    documentation for the observations file structure
    fieldname(Type):
        id(LONG) - must be unique within all observations
        type(TEXT 12) - observation type, currently: point, station, zone, and grid
        loc_id(TEXT 24) - text location ID
        loc_name(TEXT 255) - text location name, used for user analysis
        set_name(TEXT 24) - used for user analysis to group observations
        variable(TEXT 36) - observation type, currently allowed are flow, gauge flow, head
        x(DOUBLE) - x location in model coordinates, can be "n/a" for gridded values
        y(DOUBLE) - y location in model coordinates, can be "n/a" for gridded values
        z_top(DOUBLE) - z location of the top of the observation relative to model datum, set to 0 if not applicable
        z_bot(DOUBLE) - z location of the top of the observation relative to model datum, set to 0 if not applicable
        time_start(TEXT 24) - beginning time in '%Y-%m-%d %H:%M:%S'
        time_end(TEXT 24) - ending time in '%Y-%m-%d %H:%M:%S'
        time_zone(SHORT) - offset relative to UTC
        aggregate(TEXT 24) - temporal stat to apply across time interval, MATLAB function name i.e. 'mean', set to "n/a" if not applicable
        value(DOUBLE) - observation value
        units(TEXT 24) - units of the observation, for reference only right now
        weight(DOUBLE) - weight to apply on observation for subsequent analysis or optimization, enter 1 by default
        grid(TEXT 36) - spatial grid that identifies the spatial location/zone of the observation, set to "n/a" if location is defined by x,y solely
        grid_id(LONG) - value within the grid to which each measurement corresponds, set to "0" if not applicable
    """

    from arcpy import CreateTable_management, AddField_management
    from os import path

    #Create the table
    outFile = path.join(outPath,outName)
    CreateTable_management(outPath,outName)

    #Add table fields
    AddField_management(outFile,'id','LONG') #this must be unique within observation variables
    AddField_management(outFile,'type','TEXT','','','12') 
    AddField_management(outFile,'loc_id','TEXT','','','24')
    AddField_management(outFile,'loc_name','TEXT','','','255')
    AddField_management(outFile,'set_name','TEXT','','','24')
    AddField_management(outFile,'variable','TEXT','','','36') 
    AddField_management(outFile,'x','DOUBLE')
    AddField_management(outFile,'y','DOUBLE')
    AddField_management(outFile,'z_top','DOUBLE')
    AddField_management(outFile,'z_top','DOUBLE')
    AddField_management(outFile,'time_start','TEXT','','','24')
    AddField_management(outFile,'time_end','TEXT','','','24')
    AddField_management(outFile,'time_zone','SHORT')
    AddField_management(outFile,'aggregate','TEXT','','','24')
    AddField_management(outFile,'value','DOUBLE')
    AddField_management(outFile,'units','TEXT','','','24')
    AddField_management(outFile,'weight','DOUBLE')
    AddField_management(outFile,'grid','TEXT','','','36')
    AddField_management(outFile,'grid_id','LONG')


def create_obs_table_df():
    """This helper function creates an empty observations table in pandas format
    fieldname(Type):
        id(int) - must be unique within all observations
        type(str) - observation type, currently: point, station, zone, and grid
        loc_id(str) - text location ID
        loc_name(str) - text location name, used for user analysis
        set_name(str) - used for user analysis to group observations
        variable(str) - observation type, currently allowed are flow, gauge flow, head
        x(float) - x location in model coordinates, can be "n/a" for gridded values
        y(float) - y location in model coordinates, can be "n/a" for gridded values
        z_top(float) - z location of the top of the observation relative to model datum, set to 0 if not applicable
        z_bot(float) - z location of the top of the observation relative to model datum, set to 0 if not applicable
        time_start(str) - beginning time in '%Y-%m-%d %H:%M:%S'
        time_end(str) - ending time in '%Y-%m-%d %H:%M:%S'
        time_zone(float) - offset relative to UTC
        aggregate(str) - temporal stat to apply across time interval, MATLAB function name i.e. 'mean', set to "n/a" if not applicable
        value(float) - observation value
        units(str) - units of the observation, for reference only right now
        weight(float) - weight to apply on observation for subsequent analysis or optimization, enter 1 by default
        grid(str) - spatial grid that identifies the spatial location/zone of the observation, set to "n/a" if location is defined by x,y solely
        grid_id(int) - value within the grid to which each measurement corresponds, set to "0" if not applicable
    """

    import pandas

    columns = {'id':int(),'loc_id':str(),'loc_name':str(),'set_name':str(),'variable':str(),'type':str(),\
        'x':float(),'y':float(),'z_top':float(),'z_bot':float(),'time_start':str(),'time_end':str(),'time_zone':float(),\
        'aggregate':str(),'value':float(),'units':str(),'weight':float(),'grid':str(),'grid_id':int()}
    dfObs = pandas.DataFrame(data=columns,index=[])

    return dfObs


def read_features(obsSpec, arcpy, tempFeature=None, returnFeature=False):
    """This function reads observations datasets specified in the config notebook, and returns
    a temporary feature created from the input feature or table. The original fields are then mapped using
    the standard observation dictionary, documented in the config_sources notebook. Output fields are:
    'loc_id', 'loc_name', 'x', 'y', 'z_top', 'z_bot'

    Pass the dictionary specifying the complete observation, with fields "feature", "table" and "mapping"

    Also, pass the arcpy instance being used.

    If tempFeature is specified, provide the complete path to where it should be written.
    """

    import os, pandas, numpy

    # Get the output directory
    outDir = arcpy.env.workspace

    # Specify field names to keep
    keepFields = ['loc_id','loc_name','x','y','z_top','z_bot']

    # Specify temporary datatypes for the numpy to array
    tempTypes = {'loc_id':'<U24','loc_name':'<U255','x':'<f8','y':'<f8','z_top':'<f8','z_bot':'<f8'}        

    # Determine if this is a geodatabase, if not will handle differently below
    workspaceType = arcpy.Describe(outDir).workspaceType
    if workspaceType in [u'FileSystem','FileSystem']:
        extFeature = '.shp'
        extTable = '.csv'
        keepFields += ['FID','Shape']
    else:
        extFeature = ''
        extTable = ''
        keepFields += ['OBJECTID','Shape']

    #-----------------------------------------------------------------------------------------------------
    # Helper functions
    #-----------------------------------------------------------------------------------------------------
    def rename_field(feature,origField,newName):
        """This just simplifies renaming for different workspace types
        """
        if workspaceType in [u'FileSystem','FileSystem']: 
            # Get the type of the original field
            fieldObj = arcpy.ListFields(feature,origField)           
            arcpy.AddField_management(tempFeature,newName,fieldObj[0].type)
            arcpy.CalculateField_management(tempFeature,newName,'!%s!'%origField)
            arcpy.DeleteField_management(tempFeature,origField)
        else:
            arcpy.AlterField_management(tempFeature,origField,newName,newName)


    def field_mapping_str(tempFeature,obsSpec,fieldName,defaultValue='""',minFieldLength=24):
        fieldCreated = False
        if fieldName in obsSpec['mapping'].keys():
            if obsSpec['mapping'][fieldName] is not None:
                if obsSpec['mapping'][fieldName] != '':
                    rename_field(tempFeature,obsSpec['mapping'][fieldName],fieldName)
                    fieldCreated = True

        if not fieldCreated:
            arcpy.AddField_management(tempFeature,fieldName,'TEXT','','','%d'%minFieldLength)
            if defaultValue == '':
                defaultValue = '""'
            arcpy.CalculateField_management(tempFeature,fieldName,defaultValue)

    def field_mapping_num(tempFeature,obsSpec,fieldName,defaultValue=0):
        # Determine which fields exist in the feature
        fieldList = [f.name for f in arcpy.ListFields(tempFeature)]

        fieldCreated = False
        if fieldName in obsSpec['mapping'].keys():
            if obsSpec['mapping'][fieldName] is not None:
                if isinstance(obsSpec['mapping'][fieldName],dict):
                    if 'conv' in obsSpec['mapping'][fieldName].keys():
                        arcpy.AddField_management(tempFeature,fieldName,'FLOAT')
                        origField = obsSpec['mapping'][fieldName]['field']
                        if origField in fieldList:
                            convFactor = obsSpec['mapping'][fieldName]['conv']
                            arcpy.CalculateField_management(tempFeature,fieldName,'!%s! * %f'%(origField,convFactor))
                            fieldCreated = True
                    else:
                        origField = obsSpec['mapping'][fieldName]
                        if origField in fieldList:
                            rename_field(tempFeature,obsSpec['mapping'][fieldName]['field'],fieldName)
                            fieldCreated = True
                elif isinstance(obsSpec['mapping'][fieldName],(int,float)):
                    arcpy.AddField_management(tempFeature,fieldName,'FLOAT')
                    arcpy.CalculateField_management(tempFeature,fieldName,'%f'%obsSpec['mapping'][fieldName])
                    fieldCreated = True
                else:
                    raise TypeError('"mapping" field %s must be a dictionary or float'%fieldName)
        
        if not fieldCreated:
            arcpy.AddField_management(tempFeature,fieldName,'FLOAT')
            arcpy.CalculateField_management(tempFeature,fieldName,defaultValue)


    #-----------------------------------------------------------------------------------------------------
    # Processing
    #-----------------------------------------------------------------------------------------------------
    # Determine the output filename
    if tempFeature is None:
        tempName = 'temp_obs_feature'+extFeature
        tempFeature = os.path.join(outDir,tempName)
    tempTable = 'temp_table'+extTable

    # Determine whether we are reading in a feature or mapping features from a table
    tableToFeature = True # this is the default, unless a feature is specified
    if obsSpec['spatial'] is not None:
        if (obsSpec['spatial'] != '') and (os.path.splitext(obsSpec['spatial'])[1] not in ['.csv','.txt']):
            # A feature is specified which contains the input spatial locations
            tableToFeature = False

            # Copy the input feature to the output temporary feature
            arcpy.CopyFeatures_management(obsSpec['spatial'],tempFeature)
        else:
            # Replace the obsSpec['table'] with the feature
            obsSpec['table'] = obsSpec['spatial']

    # If mapping from a table, do that
    if tableToFeature:
        # For convenience
        fieldX = obsSpec['mapping']['x']['field']
        fieldY = obsSpec['mapping']['y']['field']

        # Read in the table in order to drop duplicates
        dfTable = pandas.read_csv(obsSpec['table'])
        dfTable.drop_duplicates(inplace=True, subset=[fieldX,fieldY])

        # Drop fields, use the keepFields list to determine which to keep
        # Also, create the output dtypes for the numpy array
        keepCols = []
        keepDtypes = []
        for field in keepFields:
            if field not in obsSpec['mapping']:
                continue
            thisMap =  obsSpec['mapping'][field]
            if isinstance(thisMap,dict):
                if 'field' in thisMap.keys():
                    thisMap = thisMap['field']
            if thisMap != '':
                if thisMap is not None:
                    if thisMap in dfTable.columns:
                        keepCols.append(thisMap)
                        keepDtypes.append((thisMap,tempTypes[field]))
        dfTable = dfTable[keepCols]
        keepDtypes = numpy.dtype(keepDtypes)

        # Save this to a temporary table
        pathTable = os.path.join(outDir,tempTable)
        if arcpy.Exists(pathTable):
            arcpy.Delete_management(pathTable)
        recOut = dfTable.to_records(index=False)
        arrayOut = numpy.array(recOut,dtype=keepDtypes)
        arcpy.da.NumPyArrayToTable(arrayOut,pathTable)

        # Determine the spatial reference to use
        if obsSpec['mapping']['x']['conv'] == 'lat/lon':
            sr = arcpy.SpatialReference("WGS 1984")
        else:
            sr = arcpy.env.outputCoordinateSystem
        
        # Now, create the feature
        arcpy.XYTableToPoint_management(tempTable,tempFeature,fieldX,fieldY,coordinate_system=sr)

    # Add x, y coordinates -- creates POINT_X, POINT_Y
    arcpy.AddXY_management(tempFeature)
    rename_field(tempFeature,'POINT_X','x')
    rename_field(tempFeature,'POINT_Y','y') 
    rename_field(tempFeature,obsSpec['mapping']['loc_id'],'loc_id')
    
    # Handle loc_name and z fields
    field_mapping_str(tempFeature,obsSpec,'loc_name','',255)
    field_mapping_num(tempFeature,obsSpec,'z_top',0)
    field_mapping_num(tempFeature,obsSpec,'z_bot',0)     
    
    # Remove all other fields
    listFields = arcpy.ListFields(tempFeature)
    for field in listFields:
        if field.name not in keepFields:
            arcpy.DeleteField_management(tempFeature,field.name)
            
    if not returnFeature:
        return tempFeature
    else:
        # Read in the feature as a dataframe, don't want last two fields, objectid and shape
        dfFeature = pandas.DataFrame(arcpy.da.TableToNumPyArray(tempFeature,keepFields[:-2]))

        # Delete the temporary feature
        arcpy.Delete_management(tempFeature)
        
        return dfFeature



def read_table(obsSpec, arcpy):
    """This function reads observations datasets specified in the config notebook, and returns
    a dataframe of the site data containing fields: 
    'loc_id', 'time_start', 'time_end', 'time_zone', 'aggregate', 'value', 'units', 'weight'

    Pass the dictionary specifying the complete observation, with fields "feature", "table" and "mapping"

    Also, pass the arcpy instance being used.
    """

    import pandas, numpy

    # Specify field names to keep
    keepFields = ['loc_id','time_start','time_end','time_zone','aggregate','value','units','weight']

    #-----------------------------------------------------------------------------------------------------
    # Helper functions
    #-----------------------------------------------------------------------------------------------------
    def field_mapping_str(dfIn,obsSpec,fieldName,defaultValue=''):
        if fieldName in obsSpec['mapping'].keys():
            if obsSpec['mapping'][fieldName] is not None:
                if obsSpec['mapping'][fieldName] != '':
                    dfIn[fieldName] = obsSpec['mapping'][fieldName]
        
        if fieldName not in dfIn.columns:
            if defaultValue is not None:
                dfIn[fieldName] = defaultValue

        return dfIn

    def field_mapping_date(dfIn,obsSpec,fieldName):
        if fieldName in obsSpec['mapping'].keys():
            if obsSpec['mapping'][fieldName] is not None:
                if obsSpec['mapping'][fieldName] != '':
                    if obsSpec['mapping'][fieldName]['field'] is not None:
                        origField = obsSpec['mapping'][fieldName]['field']
                        format = obsSpec['mapping'][fieldName]['format']
                        # Rename the field and convert to datetime type            
                        dfIn[fieldName] = pandas.to_datetime(dfIn[origField],format=format)
  
        # Special handling for the default time_end field behavior
        if (fieldName=='time_end') and ('time_end' not in dfIn.columns):
            format = obsSpec['mapping']['time_start']['format']
            # Determine if the format string has hours in it
            if 'H' in format:
                # If so, then we need to add 1 hour - 1 minute to the time_start
                dfIn[fieldName] = dfIn['time_start'] + pandas.Timedelta(hours=1,minutes=-1)
            else:
                # Otherwise, we just need to add 1 day - 1 minute
                dfIn[fieldName] = dfIn['time_start'] + pandas.Timedelta(days=1,minutes=-1)
        
        return dfIn

    def field_mapping_num(dfIn,obsSpec,fieldName,defaultValue=0):
        if fieldName in obsSpec['mapping'].keys():
            if obsSpec['mapping'][fieldName] is not None:
                if obsSpec['mapping'][fieldName] != '':
                    if isinstance(obsSpec['mapping'][fieldName],dict):
                        origField = obsSpec['mapping'][fieldName]['field']
                        if origField in dfIn.columns:
                            if 'conv' in obsSpec['mapping'][fieldName].keys():
                                if obsSpec['mapping'][fieldName]['conv'] is not None:
                                    convFactor = obsSpec['mapping'][fieldName]['conv']
                                    dfIn[fieldName] = dfIn[origField] * convFactor
                                    dfIn = dfIn.drop(columns=[origField])
                                else:
                                    dfIn = dfIn.rename(columns={origField:fieldName}) 
                            else:
                                dfIn = dfIn.rename(columns={origField:fieldName})
                        else:
                            raise ValueError('Field %s not found in table'%origField) 
                    elif isinstance(obsSpec['mapping'][fieldName],(int,float)):
                        dfIn[fieldName] = obsSpec['mapping'][fieldName]
                    else:
                            raise TypeError('"mapping" field %s must be a dictionary or float'%fieldName)
        
        if fieldName not in dfIn.columns:
            if defaultValue:
                dfIn[fieldName] = defaultValue

        return dfIn

    #-----------------------------------------------------------------------------------------------------
    # Processing
    #-----------------------------------------------------------------------------------------------------
    # Determine what to read from
    featureToTable = False
    if obsSpec['table'] is None:
        if obsSpec['spatial'] != '':
            featureToTable = True

    if not featureToTable:
        # Read the obs table
        dfTable = pandas.read_csv(obsSpec['table'])

    else:
        # Read the feature table
        fieldList = [f.name for f in arcpy.ListFields(obsSpec['spatial']) if f.name.lower() not in ['shape']]
        dfTable = pandas.DataFrame(arcpy.da.TableToNumPyArray(obsSpec['spatial'],fieldList))

    # Now, do the fields mapping
    dfTable = dfTable.rename(columns={obsSpec['mapping']['loc_id']:'loc_id'})
    dfTable = field_mapping_date(dfTable,obsSpec,'time_start')
    dfTable = field_mapping_date(dfTable,obsSpec,'time_end')
    dfTable = field_mapping_num(dfTable,obsSpec,'time_zone',0) 
    dfTable = field_mapping_str(dfTable,obsSpec,'aggregate','mean')
    dfTable = field_mapping_num(dfTable,obsSpec,'value',numpy.NaN)
    dfTable = field_mapping_str(dfTable,obsSpec,'units','')
    dfTable = field_mapping_num(dfTable,obsSpec,'weight',1)

    # Finally, keep only our desired fields
    origCols = dfTable.columns
    [dfTable.drop(inplace=True,columns=thisCol) for thisCol in origCols if thisCol not in keepFields]

    return dfTable

def create_grid(dfIn, gridShape, ibound, arcpy, outPath):
    # Function imports
    import geopandas, os

    # Define some temporary names
    tempRasterName = 'tempRaster'
    tempFeatureName = 'tempFeature.shp' #needs to be .shp for geopandas export

    # Read in the grid shapefile
    gdfShape = geopandas.read_file(gridShape)

    # Convert the dataframe to a geodataframe, and set the spatial reference to the grid
    gdfIn = geopandas.GeoDataFrame(dfIn, geometry=geopandas.points_from_xy(dfIn.x, dfIn.y), crs=gdfShape.crs)

    # Intersect the two geodataframes
    gdfIntersect = geopandas.sjoin(gdfIn, gdfShape, how='left', predicate='intersects')

    # Create spatial_id as an integer for each group of 'cellid' values, starting at 1
    gdfIntersect['grid_id'] = gdfIntersect.groupby('cellid').ngroup() + 1

    # Prepare the output workspace, check to see if it's a geodatabase, if so, then move up a level
    tempDir = arcpy.env.scratchWorkspace
    if arcpy.env.scratchWorkspace[-4:]=='.gdb':
        tempDir = os.path.split(arcpy.env.scratchWorkspace)[0]

    # Write out a shapefile
    tempFeature = os.path.join(tempDir,tempFeatureName)
    gdfIntersect.to_file(tempFeature)

    # Convert to a raster
    arcpy.FeatureToRaster_conversion(tempFeature, 'grid_id', tempRasterName, ibound)

    # Bring in this raster in order to apply environment
    rasterTemp = arcpy.Raster(tempRasterName)
    rasterApply = arcpy.sa.ApplyEnvironment(rasterTemp)

    # Copy this raster to the output path
    arcpy.CopyRaster_management(rasterApply, outPath)

    # Add spatial_id back to the dfIn
    dfIn['grid_id'] = gdfIntersect['grid_id']

    # Drop the geometry column if it exists
    if 'geometry' in dfIn.columns:
        dfIn = dfIn.drop(columns=['geometry'])

    # Clean up our temporary files
    arcpy.Delete_management(tempFeature)
    arcpy.Delete_management(tempRasterName)

    # Return the dataframe
    return dfIn