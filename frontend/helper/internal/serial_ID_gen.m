function [ID]=serial_ID_gen(type,name,version,dateIn)
%SERIAL_ID_GEN  Generates a serial ID using the input descriptors.
%   [ID] = serial_ID_gen(type,name,version,date)
%
%   Descriptions of Input Variables:
%   Type: a 4 letter case-sensitive code describing the type of data
%   Name: an aribitrary length string, preferably short, uniquely identifying
%       this dataset within its type.  Do not include any forward slashes in the
%       name as those are reserved for the ID code itself.
%   Version: an alphanumeric string indicating which revision this is of the
%       particular named dataset in this type, use only alphabetic or numeric
%       characters.
%   DateIn: a matlab-format serial date indicating the timestamp for this ID
%
%   Descriptions of Output Variables:
%   ID: the serial ID as a character array
%
%   Example(s):
%   >> id = serial_ID_gen('MREM','urbbase','2040',datenum('12/20/2006'));
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-11
% Copyright 2008 Michigan State University.

assert(ischar(type) & ischar(name) & ischar(version),'Inputs must be alphanumeric');
assert(~ismember('/',type),~ismember('/',name) && ~ismember('/',version),'Inputs cannot contain slashes');

ID=[type,'/',name,'/',version,'/',datestr(dateIn,'yyyymmdd')];
