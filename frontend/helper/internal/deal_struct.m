function varargout = deal_struct(inputStruct,varargin)
%DEAL_STRUCT  Deals select structure fields
%   varargout = deal_struct(inputStruct,varargin)
%
%   Note, this function does not have any error checks for performance
%   purposes.
%
%   Descriptions of Input Variables:
%   inputStruct: The input structured array to deal select fields
%   varargin: List of character field names to deal
%
%   Descriptions of Output Variables:
%   varargout: Output variables for fields
%
%   Example(s):
%   >> deal_struct
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2011-0901
% Copyright 2011 Michigan State University.

if (nargout == 1) && (length(varargin) > 1)
    for m = 1:length(varargin)
        varargout{1}.(varargin{m}) = inputStruct.(varargin{m});
    end
else
    for m = 1:length(varargin)
        varargout{m} = inputStruct.(varargin{m});
    end
end
