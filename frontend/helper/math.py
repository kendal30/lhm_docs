def rotate_coord(x,y,theta):
    '''This function rotates x and y about the point 0,0 with the specified direction
    theta (in radians), defined as positive in the counter-clockwise direction.'''

    from numpy import cos,sin

    xprime = cos(theta) * x + sin(theta) * y
    yprime = -sin(theta) * x + cos(theta) * y
    return (xprime,yprime)
