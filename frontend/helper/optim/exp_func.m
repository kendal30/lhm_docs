function x = exp_func(x,params)
x = params(1) * exp(params(2)* x);