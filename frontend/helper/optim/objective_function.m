function obj = objective_function(firstArg,funcName,evalData,obs,objName)
%OBJECTIVE_FUNCTION  Calculates the objective function value of any input function and set of evaluation points.
%   obj = objective_function(firstArg,funcName,evalData,obs,objName)
%
%   Descriptions of Input Variables:
%   firstArg: this will be either the string 'initialize', or an array of
%       values that will be passed to the function for evaluation.  Use
%       'initialize' prior to optimization to set the values of funcName, obs,
%       and objName.  Then, during either manual calibration or
%       optimization, this variable, an array, will be passed to funcName along with vals.
%       The output of this function will then be used, along with obs, to
%       calculated the objective function value specified by objName.
%   funcName: the name of the function to be evaluated.  Input to this
%       function must be of the form funcName(vals,[a,b,c...]) where a,b,and c
%       are the parameters to be optimized.
%   evalData: the points at which funcName will be evaluated.
%   obs: 'observed' values at vals, to which the output of funcName will be
%       compared.
%   objName: name of the objective function to be used, current options are
%       'SSR': sum of squared residuals, and 'MAE': mean absolute error,
%       and 'MPAE': mean percent absolute error, and 'LMAE': log mean
%       absolute error, 'MEDAE': median of absolute error, 'NSE': nash
%       sutcliffe efficiency (modified so that 0 = perfect), 'LNSE': log10
%       of NSE value
%
%   Descriptions of Output Variables:
%   obj: objective function value, a single scalar quantity
%
%   Example(s):
%   >> objective_function('initialize','gamma_distribution',obsLocs,obsVals,'MAE');
%   followed by:
%   >> objVal = objective_function([0.1,0.2]); this will evaluate
%   'gamma_distribution' with these two parameters at obsLocs, and return
%   the mean absolute error objective function value by comparison to
%   obsVals
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-05-16
% Copyright 2008 Michigan State University.

persistent data

%Define the accepted objName values
if ischar(firstArg)
    switch lower(firstArg)
        case 'initialize'
            %Return the function handle of the input function name, does
            %not test for validity of the name
            if ~isa(funcName,'function_handle')
                funcHandle = str2func(funcName);
            else
                funcHandle = funcName;
            end
            %Determine if the input objective function value is valid, and
            %if so, return the function handle for that objective function
            objOptions= {'mae','medae','ssr','mpae','lmae','nse','lnse','nsemae'};
            if any(strcmpi(objName,objOptions))
                objHandle = str2func(lower(objName));
            else
                error('Unrecognized objective function name, see HELP');
            end
            if ~iscell(evalData)
                evalData = {evalData};
            end
            data = struct('funcHandle',funcHandle);
            data.eval = evalData;
            data.obs = obs;
            data.objHandle = objHandle;
        otherwise
            error('Unrecognized option for firstArg, see HELP')
    end
else
    sim = data.funcHandle(data.eval{:},firstArg);
    obj = data.objHandle(sim,data.obs);
end
end

function output = ssr(sim,obs)
output = nansum((sim(:)-obs(:)).^2);
end

function output = mae(sim,obs)
output = nanmean(abs(sim(:)-obs(:)));
end

function output = medae(sim,obs)
output = nanmedian(abs(sim(:)-obs(:)));
end

function output = mpae(sim,obs)
output = nanmean(abs(sim(:)-obs(:))./obs(:));
end

function output = lmae(sim,obs)
output = nanmean(abs(log(sim(:)) - log(obs(:))));
end

function output = nse(sim,obs)
output = nansum((obs - sim).^2) / nansum((obs - mean(obs)).^2);
end

function output = lnse(sim,obs)
output = log10(nansum((obs - sim).^2) / nansum((obs - mean(obs)).^2));
end

function output = nsemae(sim,obs)
outMAE = nanmean(abs(sim(:)-obs(:)));
outNSE = nansum((obs - sim).^2) / nansum((obs - mean(obs)).^2);
output = (outMAE + outNSE) / 2;
end