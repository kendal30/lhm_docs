function [member,loc] = ismember_dates(testDates,inDates,precision,roundFlag)
%ISMEMBER_DATES  Works just like ismember, but uses MATLAB dates
%   [member,loc] = ismember_dates(testDates,inDates,precision,roundFlag)
%
%   Descriptions of Input Variables:
%   testDates: complete list of MATLAB dates to compare inDates against
%               (Mx1)
%   inDates: input MATLAB datenumber vector (Nx1)
%   precision: number from 1-6, indicating years, months, days, hours,
%               minutes, or seconds, these are the columns from the datevec
%               function.  This will determine how precise a date match is
%               required.
%   roundFlag: a logical flag that, if true, will round the dates toward the
%           nearest value in the precision column.  This only makes sense
%           for hours, minutes, and seconds so will only operate for
%           3 <= precision < 6
%
%   Descriptions of Output Variables:
%   member: logical array of dimension Mx1, same as ismember output
%   loc:    location of the first instance of testDate within inDate, see
%           ismember output
%
%   Example(s):
%   >> ismember_dates
%
%   See also: ismember

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-02
% Copyright 2008 Michigan State University.

%Run a few checks on the inputs
assert(isvector(testDates),'testDates must be a vector');
assert(isvector(inDates),'inDates must be a vector');
assert(isscalar(precision),'precision must be a scalar');
assert(precision >= 1 && precision <= 6,'precision must be a value between 1 and 6');

%Convert input datenum format to datevec format
testDatevec = datevec(testDates);
inDatevec = datevec(inDates);

if nargin < 4
    roundFlag = false;
end

%Round the values, if necessary
if roundFlag && precision < 6 && precision >= 3
    testDatevec = round_dates(testDatevec,precision);
    inDatevec = round_dates(inDatevec,precision);
end

%For all columns of higher precision than specified, change value to zero
if precision < 6
    testDatevec(:,precision+1:end) = 0;
    inDatevec(:,precision+1:end) = 0;
else %round seconds
    testDatevec(:,6) = round(testDatevec(:,6));
    inDatevec(:,6) = round(inDatevec(:,6));
end

%Convert the dates back to a datenum
testDates = datenum(testDatevec);
inDates = datenum(inDatevec);

%Finally, run ismember
[member,loc] = ismember(testDates,inDates);

function [vec] = round_dates(vec,precision)
roundBreak = [0,0,0,12,30,30];
indRound = vec(:,precision+1) >= roundBreak(precision+1);
vec(indRound,precision) = vec(indRound,precision) + 1;
