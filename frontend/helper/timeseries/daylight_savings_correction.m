function [standard_time]=daylight_savings_correction(daylight_time)
%this function takes time input as daylight time and converts it to
%standard time using the table below as a reference for transitions
%both input and output are matlab serial date format

%Note: the transform table is valid from 1990 - 2015

%transpose input to column vector if necessary
if size(daylight_time,2)==1
    daylight_time=daylight_time';
    transpose=1;
else
    transpose=0;
end

table.year=[1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,....
    2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015]';
table.forward=datenum({'4-1-1990','4-7-1991','4-5-1992','4-4-1993','4-3-1994',...
    '4-2-1995','4-7-1996','4-6-1997','4-5-1998','4-4-1999','4-2-2000',...
    '4-1-2001','4-7-2002','4-6-2003','4-4-2004','4-3-2005','4-2-2006',...
    '3-11-2007','3-9-2008','3-8-2009','3-14-2010','3-13-2011','3-11-2012',...
    '3-10-2013','3-09-2014','3-08-2015'})+2/24;
table.backward=datenum({'10-28-1990','10-27-1991','10-25-1992','10-31-1993','10-30-1994',...
    '10-29-1995','10-27-1996','10-26-1997','10-25-1998','10-31-1999','10-29-2000',...
    '10-28-2001','10-27-2002','10-26-2003','10-31-2004','10-30-2005','10-29-2006',...
    '11-4-2007','11-2-2008','11-7-2009','11-07-2010','11-06-2011','11-04-2012',...
    '11-03-2013','11-02-2014','11-01-2015'})+2/24;

[year,~,~,~,~,~]=datevec(daylight_time);
[test,year_index]=ismember(year,table.year);
input_years=unique(year_index);
input_years(input_years==0) = []; %skip 0

%check to make sure that the table covers the date range of the entire
%dataset
if sum(test==0)>0
    warning('Not all dates will be converted, please extend the table to cover the range of input dates')
end

shift=[];
for i=1:length(input_years)
    current_year=find(year_index==input_years(i));
    temp_shift=((daylight_time(current_year)>=table.forward(input_years(i))) & ...
        daylight_time(current_year)<=table.backward(input_years(i)));
    temp_shift=current_year(temp_shift);
    shift = [shift,temp_shift]; %#ok<AGROW>
end

standard_time=daylight_time;
standard_time(shift)=standard_time(shift)-1/24;

if transpose==1
    standard_time=standard_time';
end