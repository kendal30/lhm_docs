function [resetVals] = temporal_reset_values(inTime,inVals,timePeriod)
%This function will reset a time series back to 0 at the specified
%intervals. The input timePeriod is passed to time_series_aggregate, which
%is used to split the data

[~,splitVals] = time_series_aggregate(inTime,inVals,timePeriod,'split');
inSize = size(inVals);
resetVals = zeros(inSize);
lastInd = 0;
for m = 1:length(splitVals)
    thisLength = size(splitVals{m},1);
    if m > 1
        lastVals = repmat(splitVals{m-1}(end,:),thisLength,1);
    else
        lastVals = zeros(thisLength,inSize(2));
    end
    resetVals(lastInd+1:lastInd+thisLength,:) = splitVals{m}-lastVals;
    lastInd = lastInd + thisLength;
end

%I like this way better, below, but it's slower, sadly
% %Create a logical array for only the last value in each time period
% funcLast = @(x)(x(end,:));
% [~,periodLast] = time_series_aggregate(inTime,inVals,timePeriod,funcLast); %get the last value from each period
% diffPeriod = periodLast(2:end,:) - periodLast(1:end-1,:);
% diffPeriod = cat(1,periodLast(1,:),diffPeriod);
% [~,indLast] = time_series_aggregate(inTime,(1:length(inTime))',timePeriod,'max'); %find the index of the last element of each time period
% indFirst = indLast + 1;
% if indFirst(end) > length(inTime)
%     indFirst(end) = [];
%     diffPeriod(end,:) = [];
% end
% 
% %Make a logical array 
% subtractVals = zeros(size(inVals));
% subtractVals(indFirst,:) = diffPeriod;
% subtractVals = cumsum(subtractVals);
% 
% %Now, subtract and reset the values
% resetVals = inVals - subtractVals;
