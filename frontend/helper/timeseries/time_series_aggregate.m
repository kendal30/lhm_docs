function [tOut,varargout] = time_series_aggregate(tIn,xIn,aggUnit,aggStat,crossUnit,crossStat)
%TIME_SERIES_AGGREGATE  Aggregates time series data for analysis.
%   [tOut,varargout] = time_series_aggregate(tIn,xIn,aggUnit,aggStat,crossUnit,crossStat)
%
%   This function has become something of a swiss army knife of time-series
%   data aggregation and analysis.  The user must specify an aggregation
%   aggUnit, that is, how the data should be lumped together for purposes of
%   calculating aggregated statistics.  Then, the user specifies what
%   statistic, or statistics, should be applied.  Finally, the user can
%   specify an option of "cross" which will compare the a basic
%   aggregation crossStat across a second specified period.
%
%   Any function can be used, provided that it takes a vector or 2-D array
%   and operates down columns (along the 1st dimension) of that array.
%   Also, anonymous functions can be passed, such as '@(x)percentile(x,5)', 
%   or function handles as well.
%
%   There is a somewhat "special" mode of operation.  If the user specifies
%   "split" as the aggStat, then the return will be a cell array of times
%   and values.  Essentially, this cell array contains the original time
%   series split into chunks as specified by aggUnit.  In this case "split"
%   must be the only aggStat requested. 'split' can be a function, but if
%   so, call this using '@split', or @split.
%
%   For instance, 'days' can be the aggregation period, and any combination
%   of a number of statistics, including say 'mean' could be used for
%   summary.  If the user wanted to see how the daily aggregated values
%   varied across years, they could specify 'years' as the cross aggregation period.
%
%   This function ignores NaNs in calculations if one of the specified
%   input functions is also found in the nansuite.  Those are
%   'max','mean','median','min','sem','std','sum','var'.  'sem' is the
%   standard error, and 'var' is the variance. In all other cases, NaN will
%   be handled as in the native function. This can be explicitly overridden
%   by passing the function handle for the original version of the
%   function.
%   
%   For best results when using the fraction or integer number of days, make
%   sure there aren't gaps in the data equal to or larger than your
%   aggregation period.
%
%   It's the user's responsibility to make sure that the aggregation
%   statistic(s) make sense for the input data.  For instance, if 'sum' is
%   used, then the data must be evenly sampled in time, otherwise the 'sum'
%   is meaningless across aggregation units.
%
%   It is also the user's responsibility to make sure that the data are
%   evenly sampled.  Some operations, for istance the decadal and seasonal
%   aggregation will not perform very well for highly unevenly sampled
%   data.
%
%   This function will not truncate the data at the beginning or end in
%   order to account for partial aggregation periods.  If this is
%   important, the user must pre-truncate the data prior to this function
%   being called.
%
%   Descriptions of Input Variables:
%   tIn:    an Mx1 or 1xM array of MATLAB datenumbers, except when 'split'
%           requested, in which case it will be a cell array of dimensions
%           equal to the number of aggUnits, each cell containing a vector
%           or times
%   xIn:    an MxN or NxM vector, where N represents the number of
%           commonly-sampled time series, except when 'split' is requested,
%           in which case it will be a cell array of dimensions equal to
%           the number of aggUnits, each cell containing an array of values
%   aggUnit: options are 'decades','years','months','weeks','days','hours',
%           'minutes','seconds','water years','seasons','quarters', 
%           a simple fraction of a day (i.e. the numerator of the fraction 
%           must be a factor of 24), an integer number of days, a 
%           numerical array specifying arbitrary dividers of temporal bins;
%           or a range of months, separated by a single dash (i.e. April -
%           August, capitalization and spaces don't matter).
%           In the bin usage, the first entry of the array is the start of the
%           first bin, and the last entry is the end of the last bin. So
%           there are nBins+1 edges. In the range of months, all data not
%           included in the range will be ignored in the calculations.
%           Note: you may use a single month range (i.e. july-july).
%
%           For string options, add the word 'full' with a space before the
%           regular option to have the output trimmed to only use full
%           years, months, weeks, etc.  i.e. 'full months' will omit any
%           partial months of the dataset.
%
%   aggStat: the statistic(s) to be applied, this can be the name of any
%           MATLAB-recognized function such as 'min', 'mean', 'max', etc.,
%           or any user-defined function such as 'my_fun'.  The only
%           requirement is that the returned result of the function(s) be a
%           scalar value for each aggregated time unit column.  In other words
%           the input to the function will be of dimension OxM, where M is
%           the same as in xIn.  The output from the function must be of
%           dimension 1xM. 
%           
%           To specify multiple aggregation statistics,
%           enclose them in a cell array. An equal number of output arguments
%           must also be requested or an error will be returned. 
%           
%           Function handles and anonymous functions can be passed as well.
%           They can be passed either as strings or as function handles.
%
%   crossUnit: the unit of time to cross-aggregate the data, options are
%           'years', and 'days'.  If 'years' is chosen, then the aggUnit
%           must be one of 'months','days', or 'hours'. If 'days' is chosen, 
%           then the only aggregation unit supported is 'hours'.
%   crossStat: the statistic to be calculated for the cross-aggregation.
%           Only one type is allowed here, but it will be calculated for
%           all of the aggStat(s) that were specified.  Like aggStat, this
%           can be any function addressable by a function handle call
%           (anything on the search path not internal to another function).
%
%   Descriptions of Output Variables:
%   tOut: the time vector representing the aggregated data
%   varargout: the aggregated output data, one vector for each aggregation
%       statistic input
%
%   Example(s):
%   >> time_series_aggregate
%
%   See also:

% Author: Anthony Kendall
% Contact: kendal30 [at] msu [dot] edu
% Created: 2008-07-31
% Last Updated: 2013-02-25
% Copyright 2013 Michigan State University.

%Go through the pre-processing sub-functions
%--------------------------------------------------------------------------
%Validate and prepare the inputs
if nargin < 5
    crossStat = false;
end
[tIn,xIn,flagTransp,numStat,aggStat,crossStat] = validate_inputs(xIn,tIn,aggStat,crossStat);

%Parse the input aggregation unit
[dateCol,aggUnit,testTrim,startTrim,endTrim,precisTrim] = process_agg_unit(aggUnit);

%Trim the dates, if requsted, to the full period only. Alternately it will
%pad the data for use with explicitly-defined bins
if testTrim
    [tIn,xIn] = trim_pad_data(tIn,xIn,startTrim,endTrim,precisTrim);
end

%Process the aggregation boundaries
[unitStart,unitEnd,unitDates] = process_agg_boundaries(tIn,aggUnit,dateCol);

%Split the data if needed
if any(strcmpi(aggStat,'split'))
    [tOut,varargout{1}] = process_split(tIn,xIn,unitStart,unitEnd,numStat);
    return
end

%Calculate statistics on the aggregated data
%--------------------------------------------------------------------------
%Check to see if the number of output arguments matches this
assert((nargout - 1) == numStat,'Number of requested aggregation statistics does not match output argument');

%Determine if any aggregation is required
testAgg = any(unitEnd - unitStart > 0);
if ~testAgg
    if islogical(crossStat)
        warning('TimeSeriesAggregate:no_aggregation','Nothing to be done'); 
        tOut = tIn;
        for m = 1:(nargout - 1)
            varargout{m} = xIn;
        end
        return
    else
        %Create the unitVals array for the cross operations
        unitVals = repmat(xIn,[1,1,numStat]);
    end
else
    %Create the output array
    unitVals = zeros(size(unitDates,1),size(xIn,2),numStat);
    
    %Calculate the statistics
    for m=1:numStat
        for n = 1:size(unitVals,1)
            thisStat = aggStat{m}(xIn(unitStart(n):unitEnd(n),:));
            if ~isempty(thisStat)
                unitVals(n,:,m) = aggStat{m}(xIn(unitStart(n):unitEnd(n),:));
            end
        end
    end
end

%Calculate the cross aggregation, if specified
%--------------------------------------------------------------------------
if ~islogical(crossStat)
    %Process the cross-aggregation units
    [crossUnits,aggCol] = process_cross_agg(crossUnit,aggUnit,unitDates);
    
    %Create the output array
    crossVals = zeros(length(crossUnits),size(xIn,2),numStat);

    %Calculate the cross aggregation
    for m = 1:numStat
        for n = 1:size(crossUnits,1)
            thisStat = aggStat{m}(xIn(unitStart(n):unitEnd(n),:));
            if ~isempty(thisStat)
                crossVals(n,:,m) = crossStat{1}(unitVals((aggCol == crossUnits(n)),:,m));
            end
        end
    end 
end

%Create output arrays
%--------------------------------------------------------------------------
%tOut
if islogical(crossStat)
    tOut = datenum(unitDates);
else
    tOut = crossUnits;
end
if flagTransp(1),tOut=tOut';end
%xOut
for m = 1:numStat
    if islogical(crossStat)
        varargout{m} = unitVals(:,:,m);
    else
        varargout{m} = crossVals(:,:,m);
    end
    if flagTransp(2),varargout{m}=varargout{m}';end
end
end


%--------------------------------------------------------------------------
%Processing Functions
%--------------------------------------------------------------------------
function [tIn,xIn,flagTransp,numStat,aggStat,crossStat] = validate_inputs(xIn,tIn,aggStat,crossStat)
%I've arbitrarily chosen a column vector as the direction of inputs.
if size(tIn,1)==1
    tIn = tIn';
    flagTransp(1) = true;
    if size(tIn,1) ~= size(xIn,1)
        if size(tIn,1) ~= size(xIn,2)
            error('Input time vector dimension must match either the first or second dimension of input data array');
        end
        xIn = xIn';
        flagTransp(2) = true;
    else
        flagTransp(2) = false;
    end
else
    flagTransp = [false;false];
end

%Demand that the input times be monotonically increasing
assert(all(diff(tIn)>=0),'Input time vector must be monotonically increasing');

%Determine the number of specified aggregation statistics to calculate,
%process functions
if iscell(aggStat)
    numStat = length(aggStat);
else
    numStat = 1;
    aggStat = {aggStat};
end
aggStat = process_agg_funcs(aggStat);

%Process the cross statistics
if ~islogical(crossStat)
    if iscell(crossStat)
        assert(length(crossStat)==1,'Only one cross-aggregation statistic can be used')
    else
        crossStat = {crossStat};
    end
    crossStat = process_agg_funcs(crossStat);
end
end

function [aggStat] = process_agg_funcs(aggStat)
nansuite = {'max','mean','median','min','sem','std','sum','var'};
for m = 1:length(aggStat)
    %Check for function handles, anonymous functions, functions called with '@'
    testHandle = isa(aggStat{m},'function_handle');
    if ~testHandle
        if strcmp(aggStat{m}(1),'@')
            aggStat{m} = eval(aggStat{m}); %use this here because str2func throws a warning
        elseif ismember(aggStat{m},nansuite)
            aggStat{m} = str2func(['nan',aggStat{m}]);
        elseif strcmpi(aggStat{m},'split')
            aggStat{m} = aggStat{m}; %don't do anything with this
        else
            aggStat{m} = str2func(aggStat{m});
        end
    end
end
end

function [dateCol,aggUnit,testTrim,startTrim,endTrim,precisTrim] = process_agg_unit(aggUnit)
if ischar(aggUnit)
    %First, check for the modifier, 'full'
    [testTrim,aggUnit] = regexpi(aggUnit,'full ','start','split');
    %Convert testTrim to a logical
    testTrim = logical(testTrim);
    if isempty(testTrim),testTrim = false;end
    %Extract the useful part of aggUnit
    aggUnit = aggUnit{end};
    
    switch lower(aggUnit)
        case 'years'
            dateCol = 1;
            startTrim = [1,1,0,0,0];
            endTrim = [12,31,23,59,59];
            precisTrim = 1;
        case 'months'
            dateCol = 2;
            startTrim = [1,0,0,0];
            endTrim = [31,23,59,59];
            precisTrim = 1;
        case 'days'
            dateCol = 3;
            startTrim = [0,0,0];
            endTrim = [23,59,59];
            precisTrim = 1/24;
        case 'hours'
            dateCol = 4;
            startTrim = [0,0];
            endTrim = [59,59];
            precisTrim = 1/24/60;
        case 'minutes'
            dateCol = 5;
            startTrim = 0.000;
            endTrim = 59.999;
            precisTrim = 1/24/60/60;
        case 'seconds'
            dateCol = 6;
            startTrim = [];
            endTrim = [];
            precisTrim = [];
            if testTrim
                warning('"full " option currently not supported for this aggUnit'); %#ok<*WNTAG>
                testTrim = false;
            end
        case 'weeks'
            %This will be treated as 7 days
            dateCol = -3;
            aggUnit = 7;
            startTrim = [];
            endTrim = [];
            precisTrim = [];
            if testTrim
                warning('"full " option currently not supported for this aggUnit');
                testTrim = false;
            end
        case 'water years'
            dateCol = -5;
            startTrim = [10,1,0,0,0];
            endTrim = [9,30,23,59,59];
            precisTrim = 1;
        case 'decades'
            dateCol = -4;
            startTrim = [];
            endTrim = [];
            precisTrim = [];
            if testTrim
                warning('"full " option currently not supported for this aggUnit');
                testTrim = false;
            end
        case 'seasons'
            dateCol = -6;
            startTrim = [];
            endTrim = [];
            precisTrim = [];
            if testTrim
                warning('"full " option currently not supported for this aggUnit');
                testTrim = false;
            end
        case 'quarters'
            dateCol = -7;
            startTrim = [];
            endTrim = [];
            precisTrim = [];
            if testTrim
                warning('"full " option currently not supported for this aggUnit');
                testTrim = false;
            end
        otherwise
            if strfind(aggUnit,'-')
                dateCol = -9;
                startTrim = [];
                endTrim = [];
                precisTrim = [];
                if testTrim
                    warning('"full " option currently not supported for this aggUnit');
                    testTrim = false;
                end
            else
                error('Enter one of the supported temporal aggregation units')
            end
    end
elseif length(aggUnit)>1
    dateCol = -8; %specified time bin left edges
    if size(aggUnit,1)==1
        aggUnit = aggUnit';
    end
    testTrim = true;
    startTrim = aggUnit(1);
    endTrim = aggUnit(end);
    precisTrim = false; %in this case, just use the exact definition for trimming
else
    %Do not trim the data
    [testTrim,startTrim,endTrim,precisTrim] = deal(false);
    if aggUnit==1
        dateCol = 3; %this indicates that daily aggregation should be used instead
    else
        dateCol = -3; %otherwise, handle as a general-length aggUnit
    end
end
end
    
function [tTrim,xTrim] = trim_pad_data(tIn,xIn,startTrim,endTrim,precisTrim)
if precisTrim
    %Determine the start and end dates
    startDate = datevec(tIn(1));
    endDate = datevec(tIn(end));
    
    %Determine the full startTrim and endTrim vectors
    trimCol = 6 - length(startTrim);
    startTrim = [startDate(1:trimCol),startTrim];
    endTrim = [endDate(1:trimCol),endTrim];
    
    %Test to see if the startDate is more than 1 day less than the startTrim,
    %if it is, then use one year greater than the start year
    offsetStart = 0;
    if (tIn(1) - datenum(startTrim)) > precisTrim
        offsetStart = 1;
    end
    %Test the startDate the same way
    offsetEnd = 0;
    if (datenum(endTrim) - tIn(end)) > precisTrim
        offsetEnd = 1;
    end
    
    %Now, bracket the index values
    bracketInd = bracket_index(tIn,datenum([startTrim(1:trimCol)+offsetStart,startTrim(trimCol+1:end)]),...
        datenum([endTrim(1:trimCol)-offsetEnd,endTrim(trimCol+1:end)]));
    
    %Finally, trim the tIn and xIn vectors
    tTrim = tIn(bracketInd(1):bracketInd(end),1);
    xTrim = xIn(bracketInd(1):bracketInd(end),:);
else
    %Get the sampling frequency
    minFrequency = min(diff(tIn));
    
    %Create a resampling array
    tTrim = (startTrim:minFrequency:endTrim)';
    xTrim = zeros(length(tTrim),size(xIn,2)) + NaN;
    
    %Determine the precision to use for ismember_dates
    switch minFrequency
        case minFrequency >= 1
            precisCol = 3; %days
        case (minFrequency < 1) && (minFrequency >= 1/24)
            precisCol = 4; %hours
        case (minFrequency < 1) && (minFrequency < 1/24) && (minFrequency >= 1/24/60);
            precisCol = 5; %minutes
        otherwise
            precisCol = 6; %seconds
    end
    
    %Determine where the original data are within the tTrim array
    [trimPresent,origPosition] = ismember_dates(tTrim,tIn,precisCol,true);
    
    %Place values in array
    xTrim(trimPresent,:) = xIn(origPosition(trimPresent),:);
end
end

function  [unitStart,unitEnd,unitDates] = process_agg_boundaries(tIn,aggUnit,dateCol)
%Now, prepare the dates for boundary detection
dateVector = datevec(tIn);
dateDiff = diff(dateVector);
sampleRate = mean(diff(tIn));

%Determine the end points of the aggregation units, method varies by
%aggregation unit
if dateCol>0 %this works for years, months, days, hours, minutes, seconds
    unitEnd = find(dateDiff(:,dateCol)~=0);
    if isempty(unitEnd) %for the case when are all within aggregation period
        unitEnd = size(tIn,1);
    end
else %Handle all other cases
    if dateCol==-3  %this is for aggUnits of arbitrary floating/integer values
        aggPeriod = floor((tIn - floor(tIn(1))) / aggUnit) + 1;
        unitEnd = find(diff(aggPeriod)>0);
    elseif dateCol==-4 %decades
        dateVectorMod(:,1) = floor(dateVector(:,1)/10);
        dateDiffMod = diff(dateVectorMod);
        unitEnd = find(dateDiffMod(:,1)~=0);
    elseif dateCol==-5 %water years
        unitEnd = find(diff(dateVector(:,2)<=9)==-1);
        unitEnd(unitEnd==0) = []; %In case the dataset starts on the first day of the water year
    elseif dateCol==-6 %seasons
        djf = find(ismember(dateVector(:,2),[12,1,2]));
        mam = find(ismember(dateVector(:,2),[3,4,5]));
        jja = find(ismember(dateVector(:,2),[6,7,8]));
        son = find(ismember(dateVector(:,2),[9,10,11]));
        unitEndWin = [djf(diff(djf)>180/sampleRate);djf(end)];
        unitEndSpr = [mam(diff(mam)>180/sampleRate);mam(end)];
        unitEndSum = [jja(diff(jja)>180/sampleRate);jja(end)];
        unitEndFall = [son(diff(son)>180/sampleRate);son(end)];
        unitEnd = sort([unitEndWin;unitEndSpr;unitEndSum;unitEndFall]);
    elseif dateCol==-7 %quarters
        first = find(ismember(dateVector(:,2),[1,2,3]));
        second = find(ismember(dateVector(:,2),[4,5,6]));
        third = find(ismember(dateVector(:,2),[7,8,9]));
        fourth = find(ismember(dateVector(:,2),[10,11,12]));
        unitEndFirst = [first(diff(first)>180/sampleRate);first(end)];
        unitEndSecond = [second(diff(second)>180/sampleRate);second(end)];
        unitEndThird = [third(diff(third)>180/sampleRate);third(end)];
        unitEndFourth = [fourth(diff(fourth)>180/sampleRate);fourth(end)];
        unitEnd = sort([unitEndFirst;unitEndSecond;unitEndThird;unitEndFourth]);
    elseif dateCol==-8 %bin edges are specified
        if length(aggUnit) > 2
            aggUnit = aggUnit(1:end-1); %last was just used for specifying end of the dataset for trimming
            diffAll = repmat(tIn,1,size(aggUnit,1)) - repmat(aggUnit',size(tIn,1),1);
            diffAll(diffAll<0) = -1;
            unitStartTemp = sum(diffAll==-1,1)'+1;
            unitEnd = unitStartTemp-1;
            unitEnd(unitEnd==0) = [];
        else
            unitEnd = size(tIn,1);
        end
    elseif dateCol==-9 %range of months specified
        %Parse the input
        startEnd = regexp(aggUnit,'-','split');
        monthStruct = struct('jan',1,'feb',2,'mar',3,'apr',4,'may',5,'jun',6','jul',7,'aug',8,'sep',9,'oct',10,'nov',11,'dec',12);
        for m = 1:2
            startEnd{m} = lower(strtrim(startEnd{m}));
            if length(startEnd{m}) > 3
                startEnd{m} = startEnd{m}(1:3);
            end
            startEnd{m} = monthStruct.(startEnd{m});
        end
        %Now, identify the unit start points
        unitStart = find(dateVector(:,2)==startEnd{1});
        unitEnd = find(dateVector(:,2)==startEnd{2});

        if unitEnd(1) < unitStart(1);
            unitStart = [1;unitStart];
        end
        if length(unitStart) > length(unitEnd)
            unitEnd = [unitEnd;size(tIn,1)];
        elseif length(unitEnd) > length(unitStart)
            unitStart = [1;unitStart];
        end
    end
end
assert(~isempty(unitEnd),'There was an error calculating the aggregation units, check that the period is not finer than the input sampling resolution');

if dateCol ~= -9
    %Include the last unit, even if it is incomplete
    if unitEnd(end)~=size(tIn,1)
        unitEnd(end+1) = size(tIn,1);
    end
    %The start point for each aggregation unit is then
    if size(unitEnd,1)==1
        unitEnd = unitEnd';
    end
    unitStart = [1;unitEnd(1:end-1)+1];
end

%The date of the aggUnit to be reported
if dateCol == -5 %a water year is named for the year including 9 months of the water year
    unitDates = dateVector(unitEnd);
    unitDates(:,2:3) = 1;
    unitDates(:,4:6) = 0;
elseif dateCol == -9 %for monthly aggregated data that can span multiple years, report the year of the end of each period
    unitDates = dateVector(unitEnd);
    unitDates(:,2:3) = 1;
    unitDates(:,4:6) = 0;
else %all other aggregation types
    unitDates = dateVector(unitStart,1:6);
end
end

function [tOut,varargout] = process_split(tIn,xIn,unitStart,unitEnd,numStat)
if numStat > 1
    warning('If "split" is requested, all other aggStats will be ignored');
end
numUnits = size(unitStart,1);
[tOut,xOut] = deal(cell(numUnits,1));
for m = 1:numUnits
    tOut{m} = tIn(unitStart(m):unitEnd(m),1);
    xOut{m} = xIn(unitStart(m):unitEnd(m),:);
end
varargout{1} = xOut;
% if numStat > 1
%     for m = 2:numStat
%         varargout{m} = [];
%     end
% end
end

function [crossUnits,aggCol] = process_cross_agg(crossUnit,aggUnit,unitDates)
%Determine compatibility
assert(ischar(crossUnit),'The cross aggregation unit must be a character array');
assert(any(strcmpi(crossUnit,{'years','days'})),'The cross aggregation unit must be one of the allowed types');
switch lower(crossUnit)
    case 'years'
        if ischar(aggUnit)
            switch lower(aggUnit)
                case 'years'
                    aggCol = unitDates(:,1);
                case 'quarters'
                    aggCol = unitDates(:,2);
                    aggCol(aggCol < 4) = 1;
                    aggCol(aggCol>=4 & aggCol<7) = 2;
                    aggCol(aggCol>=7 & aggCol<10) = 3;
                    aggCol(aggCol>=10 & aggCol<12) = 4;
                case 'months'
                    aggCol = unitDates(:,2);
                case 'days'
                    doy = date2doy(datenum(unitDates));
                    aggCol = floor(doy);
                case 'hours'
                    doy = date2doy(datenum(unitDates));
                    aggCol = floor(doy) + round(rem(doy,1)*24)/24;
                otherwise
                    error('The specified cross aggregation unit is incompatible with the basic aggregation unit');
            end
        elseif isnumeric(aggUnit) %(this could be 'weeks' or any numerically-specified grouping)
            doy = date2doy(datenum(unitDates));
            aggCol = floor(doy / aggUnit);
        else
            error('The specified cross aggregation unit is incompatible with the basic aggregation unit');
        end
        
    case 'days'
        switch lower(aggUnit)
            case 'hours'
                aggCol = unitDates(:,4);
            otherwise
                error('The specified cross aggregation unit is incompatible with the basic aggregation unit');
        end
end
%Determine the unique cross aggregation units
crossUnits = count_unique(aggCol);
end
    

%--------------------------------------------------------------------------
%Helper Functions
%--------------------------------------------------------------------------
%Function to calculate the decimal day of year
function [doy] = date2doy(inputDate)

%Parse the inputDate
[dateVector] = datevec(inputDate);

%Set everything in the date vector to 0 except for the year
dateVector(:,2:end) = 0;
dateYearBegin = datenum(dateVector);

%Calculate the day of the year
doy = inputDate - dateYearBegin;
end

function indBracket = bracket_index(inVals,valStart,valEnd)
%Check that the inVals array is sorted, throw an error otherwise
assert(issorted(inVals),'The input values vector must be sorted');

%Determine the first index of inVals with a value greater than or equal to
%valStart
if ~isempty(valStart)
    indStart = find(inVals >= valStart, 1, 'first');
else
    indStart = 1;
end

%Determine the last index of inVals with a value less than or equal to
%valEnd
if nargin < 3
    valEnd = [];
end
if ~isempty(valEnd)
    indEnd = find(inVals <= valEnd, 1, 'last');
else
    indEnd = length(inVals);
end

%Four possibilities
if isempty(indStart) && ~isempty(indEnd) %in this case, all values are greater than the inVals
    indBracket = [];
elseif ~isempty(indStart) && isempty(indEnd) %in this case, all values are less than the inVals
    indBracket = [];
elseif indStart > indEnd %In this case, there is a gap in inVals greater than the distance between the start and end vals
    indBracket = [];
else
    indBracket = [indStart,indEnd];
end
end

function [member,loc] = ismember_dates(testDates,inDates,precision,roundFlag)
%ISMEMBER_DATES  Works just like ismember, but uses MATLAB dates
%   varargout = ismember_dates(inDates,precision)
%
%   Descriptions of Input Variables:
%   testDates: complete list of MATLAB dates to compare inDates against
%               (Mx1)
%   inDates: input MATLAB datenumber vector (Nx1)
%   precision: number from 1-6, indicating years, months, days, hours,
%               minutes, or seconds, these are the columns from the datevec
%               function.  This will determine how precise a date match is
%               required.
%   roundFlag: a logical flag that, if true, will round the dates toward the
%           nearest value in the precision column.  This only makes sense
%           for hours, minutes, and seconds so will only operate for
%           3 <= precision < 7
%
%   Descriptions of Output Variables:
%   member: logical array of dimension Mx1, same as ismember output
%   loc:    location of the first instance of testDate within inDate, see
%           ismember output
%

%Run a few checks on the inputs
assert(isvector(testDates),'testDates must be a vector');
assert(isvector(inDates),'inDates must be a vector');
assert(isscalar(precision),'precision must be a scalar');
assert(precision >= 1 && precision <= 6,'precision must be a value between 1 and 6');

%Convert input datenum format to datevec format
testDatevec = datevec(testDates);
inDatevec = datevec(inDates);

%Round the values, if necessary
if roundFlag && precision <= 6 && precision >= 3
    testDatevec = round_dates(testDatevec,precision);
    inDatevec = round_dates(inDatevec,precision);
end

%For all columns of higher precision that specified, change value to zero
if precision < 6
    testDatevec(:,precision+1:end) = 0;
    inDatevec(:,precision+1:end) = 0;
end

%Convert the dates back to a datenum
testDates = datenum(testDatevec);
inDates = datenum(inDatevec);

%Finally, run ismember
[member,loc] = ismember(testDates,inDates);

    function [vec] = round_dates(vec,precision)
        roundBreak = [0,0,0,12,30,30];
        if precision < 6
            indRound = vec(:,precision+1) >= roundBreak(precision+1);
            vec(indRound,precision) = vec(indRound,precision) + 1;
        else
            vec(:,precision) = round(vec(:,precision));
        end
        
    end
end

%--------------------------------------------------------------------------
%Override built-in functions here, if necessary
%--------------------------------------------------------------------------