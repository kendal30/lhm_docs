function time = parse_time(inTime)
%This is much slower than datenum, but it works more reliably
%The input times should be of the form: yyyy-mm-dd HH:MM:SS or yyyy-mm-dd
%HH:MM:SS.SSS

timeVec = zeros(1,6);

%Break the remaining part of the time string into pieces
[tok] = regexpi(inTime,'([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+.?[0-9]+)','tokens');

for m = 1:size(inTime,1);
    timeVec(m,:) = str2double(tok{m}{1}(:));
end

time = datenum(timeVec);

end