function [package] = drn_write(path,package,structure,params,constants,mult,grids) % #ok<INUSL>

% Determine which cells are drain cells
indDrains = grids.drn > 0;
numSets = size(grids.drn,3); % can have multiple drain sets

% Calculate: mxdrn
mxdrn=sum(indDrains(:));
package.inputs.drn_mxdrn = mxdrn;

% Generate: arrays for drain file
% [lay,row,col,drn_elev,drn_cond]
% initialize arrays
[row,col,~] = ind2sub([structure.num_row,structure.num_col,numSets],...
    (1:structure.num_row*structure.num_col*numSets));
    
% Assemble the drnArray
drnArray = [grids.drn_layer(indDrains)';row(indDrains);col(indDrains);...
    grids.drn_elev(indDrains)';grids.drn_cond(indDrains)'];

% write out DRN file
% Create the format strings
format1 = '% d % d\n';
format1a = '% d % d';
format1b = '% 8s\n';
format3 = '% 1d % 4d % 4d % 6f % 12f\n';

% Print the DRN file
fid = fopen([path,filesep,package.name],'wt');
fprintf(fid,format1a,[mxdrn,package.inputs.drn_budget]);
fprintf(fid,format1b,package.inputs.drn_print_string);
fprintf(fid,format1,[mxdrn,package.inputs.drn_npar]);
fprintf(fid,format3,drnArray);
if structure.nper>1
    for m = 1:structure.nper % needs to be changed if an SS ramp-up is added
        fprintf(fid,format1,[-1 0]);
    end
end
fclose(fid);