function [package] = oc_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

%write the file
format1='%s';
format2a='\n%s';
format2b1=' %d';
format2b2=' %s';
format2c='\n%15s';
fid=fopen([path,filesep,package.name],'wt');
fprintf(fid,format1,package.inputs.head_format);
fprintf(fid,format2a,package.inputs.head_save_unit_text);
fprintf(fid,format2b1,package.inputs.hed_unit);
fprintf(fid,format2a,package.inputs.bud_format);
for i=1:structure.nper
    for j=1:structure.nstp(i)
        if structure.save_budget{i}(j)>=1 || structure.save_head{i}(j)>=1
            fprintf(fid,format2a,package.inputs.per_label);
            fprintf(fid,format2b1,i);
            fprintf(fid,format2b2,package.inputs.step_label);
            fprintf(fid,format2b1,j);
            if structure.save_budget{i}(j)>=1
                fprintf(fid,format2c,package.inputs.save_budget_string);
            end
            if structure.save_head{i}(j)>=1
                fprintf(fid,format2c,package.inputs.save_head_string);
            end
        end
    end
end
fclose(fid);
