function [package] = nam_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

format1='%-13s';
format2='%2d';
format_temp=max(size(structure.model_name))+9; %adds on 1 for the period, 3 for the extension, 4 for '_rch', and 1 in front
format3=strcat('%',num2str(format_temp),'s\n');

fid1=fopen([path,filesep,package.name],'wt');
packages = fieldnames(package.inputs);
%Write out packages in unit number order
units = [];
for i = 1:length(packages)
    if package.inputs.(packages{i}).active && ~strcmpi(packages{i},'nam')
        units = [units;i,package.inputs.(packages{i}).unit]; %#ok<AGROW>
    end
end
%Determine the sort order
sortOrder = sortrows(units,2);
%Now, write out the packages in unit number order
for i = 1:size(sortOrder,1)
    fprintf(fid1,format1,package.inputs.(packages{sortOrder(i)}).type);
    fprintf(fid1,format2,package.inputs.(packages{sortOrder(i)}).unit);
    fprintf(fid1,format3,package.inputs.(packages{sortOrder(i)}).name);
end
fclose(fid1);
