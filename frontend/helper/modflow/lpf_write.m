function [package] = lpf_write(path,package,structure,params,constants,mult,grids)

% Reformat some inputs
fields = fieldnames(package.inputs);
for m = 1:length(fields)
    if iscell(package.inputs.(fields{m}))
        package.inputs.(fields{m}) = cell2mat(package.inputs.(fields{m}));
    end
end

% Convert scalar mult values into vectors of length num_lay
if isfield(mult,'hk')
    if isscalar(mult.hk.curr)
        mult.hk.curr = repmat(mult.hk.curr,1,structure.num_lay);
    end
end
if isfield(mult,'vani')
    if isscalar(mult.vani.curr)
        mult.vani.curr = repmat(mult.vani.curr,1,structure.num_lay);
    end
end

%Create format strings
format1 = '%2d %8.3f %2d\n';
format2 = [repmat('%d ',1,structure.num_lay-1),'%d\n'];
format2b = '%f %d %d\n';
format3a1 = '%s';
format3a2 = ' %s';
format3b = ' %.10f %d\n';
format3c = '%s\n';
format4a = '%d ';
format4c = ' %d\n';
format6 = [repmat('%.10f ',1,structure.num_col-1),'%.10f\n'];

%Print LPF file
fid1=fopen([path,filesep,package.name],'wt');
if isfield(params,'hk') && isfield(params,'vani')
    fprintf(fid1,format1,[package.inputs.lpf_budget,structure.hdry,length(params.hk.code)+length(params.vani.code)]);
else
    fprintf(fid1,format1,[package.inputs.lpf_budget,structure.hdry,0]);
end
fprintf(fid1,format2,package.inputs.laytyp);
fprintf(fid1,format2,package.inputs.layavg);
fprintf(fid1,format2,package.inputs.chani);
fprintf(fid1,format2,package.inputs.vka);
fprintf(fid1,format2,package.inputs.laywet);

%If wetting is active
if any(package.inputs.laywet == 1)
    fprintf(fid1,format2b,[package.inputs.wetfct,package.inputs.iwetit,package.inputs.ihdwet]);
end

for h=1:structure.num_lay
    if isfield(params,'hk')
        %First, write the HK params
        layInd = find(params.hk.lay==h);
        for i = layInd
            parnam=strcat(package.inputs.partype_hk,'_',num2str(abs(params.hk.code(i))));
            fprintf(fid1,format3a1,parnam);
            fprintf(fid1,format3a2,package.inputs.partype_hk);
            fprintf(fid1,format3b,[params.hk.val(i),package.inputs.nclu]);
            fprintf(fid1,format4a,h);
            fprintf(fid1,format3a1,package.inputs.mltarr);
            fprintf(fid1,format3a2,[package.inputs.zonarr_name_hk,num2str(h)]);
            fprintf(fid1,format4c,params.hk.code(i));
        end
    end
    if isfield(params,'vani')
        %Then, write the VANI params
        layInd = find(params.vani.lay==h);
        for i = layInd
            parnam=strcat(package.inputs.partype_vani,'_',num2str(abs(params.vani.code(i))));
            fprintf(fid1,format3a1,parnam);
            fprintf(fid1,format3a2,package.inputs.partype_vani);
            fprintf(fid1,format3b,[params.vani.val(i),package.inputs.nclu]);
            fprintf(fid1,format4a,h);
            fprintf(fid1,format3a1,package.inputs.mltarr);
            fprintf(fid1,format3a2,[package.inputs.zonarr_name_vani,num2str(h)]);
            fprintf(fid1,format4c,params.vani.code(i));
        end
    end
end
for j=1:structure.num_lay
    if isfield(params,'hk')
        fprintf(fid1,format4c,package.inputs.hk_printcode);
    else
        if isfield(constants,'hk')
            %Write out the HK constant
            fprintf(fid1,format3a1,package.inputs.constant_header);
            fprintf(fid1,format4c,constants.hk);
        else
            %Get the array header for HK
            if isfield(mult,'hk')
                if isstruct(mult.hk)
                    arrayHeader = sprintf(package.inputs.array_header,num2str(mult.hk.curr(j),'%.10f'));
                else
                    arrayHeader = sprintf(package.inputs.array_header,num2str(mult.hk,'%.10f'));
                end
            else
                arrayHeader = sprintf(package.inputs.array_header,num2str(1,'%d'));
            end

            %Write out the HK array
            fprintf(fid1,format3c,arrayHeader);
            fprintf(fid1,format6,grids.hk(:,:,j)');
        end
    end
    if isfield(params,'vani')
        fprintf(fid1,format4c,package.inputs.vani_printcode);
    else
        if isfield(constants,'vani')
            %Write out the VANI constant
            fprintf(fid1,format3a1,package.inputs.constant_header);
            fprintf(fid1,format4c,constants.vani);
        else
            %Get the array header for VANI
            if isfield(mult,'vani')
                if isstruct(mult.vani)
                    arrayHeader = sprintf(package.inputs.array_header,num2str(mult.vani.curr(j),'%.10f'));
                else
                    arrayHeader = sprintf(package.inputs.array_header,num2str(mult.vani,'%.10f'));
                end
            else
                arrayHeader = sprintf(package.inputs.array_header,num2str(1,'%d'));
            end

            %Write out the VANI array
            fprintf(fid1,format3c,arrayHeader);
            fprintf(fid1,format6,grids.vani(:,:,j)');
        end
    end
    if structure.nper > 1
        %this is only relevant for a transient simulation
        if isfield(constants,'spec_stor')
            %Write out the specific storage constant
            fprintf(fid1,format3a1,package.inputs.constant_header);
            fprintf(fid1,format4c,constants.spec_stor);
        else
            %Write out the specific storage array
            fprintf(fid1,format3c,sprintf(package.inputs.array_header,num2str(1,'%d')));
            fprintf(fid1,format6,grids.spec_stor(:,:,j)');
        end
        if isfield(constants,'spec_yield')
            %Write out the specific yield constant
            fprintf(fid1,format3a1,package.inputs.constant_header);
            fprintf(fid1,format4c,constants.spec_yield);
        else
            %Write out the specific yield array
            fprintf(fid1,format3c,sprintf(package.inputs.array_header,num2str(1,'%d')));
            fprintf(fid1,format6,grids.spec_yield(:,:,j)');
        end
    end
    if package.inputs.laywet(j) == 1 %If wetting is active for this layer
        fprintf(fid1,format3a1,package.inputs.constant_header);
        fprintf(fid1,format4c,package.inputs.wetdry);
    end
end
fclose(fid1);