function [package] = hob_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

format1 = '%d %d %d %d %g %s\n';
format2 = '%g %g\n';
format3a = '%12s ';
format3b = '%d %d %d %d %g %g %g %g %g %d %d\n';

%Print the OBS file
fid1 = fopen([path,filesep,package.name],'wt');
fprintf(fid1,format1,[package.inputs.nh, package.inputs.mobs, package.inputs.maxm,...
    package.inputs.iuhobsv, package.inputs.hobdry, package.inputs.hob_print_string]);
fprintf(fid1,format2,[package.inputs.tomulth, package.inputs.evh]);
if length(package.inputs.stat_flag)==1
    package.inputs.stat_flag = repmat(package.inputs.stat_flag,1,package.inputs.nh);
end
for m = 1:package.inputs.nh
    fprintf(fid1,format3a,package.inputs.obsnam{m});
    fprintf(fid1,format3b,[package.inputs.layer(m),...
        package.inputs.row(m),package.inputs.column(m),package.inputs.irefsp(m),...
        package.inputs.toffset(m),package.inputs.roff(m),package.inputs.coff(m),...
        package.inputs.hobs(m),package.inputs.statistic(m),package.inputs.stat_flag(m),...
        package.inputs.plot_symbol(m)]);
end
fclose(fid1);
