function [package] = lmg_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

format1='%g %g %g %d\n';
format2='%d %d %g %g %d\n';
format3='%g g';
fid=fopen([path,filesep,package.name],'wt');
fprintf(fid,format1,[package.inputs.STOR1,package.inputs.STOR2,package.inputs.STOR3,package.inputs.ICG]);
fprintf(fid,format2,[package.inputs.MXITER,package.inputs.MXCYC,package.inputs.BCLOSE,...
    package.inputs.DAMP,package.inputs.IOUTAMG]);
if package.inputs.DAMP == -2
    fprintf(format3,[package.inputs.DUP,package.inputs.DFLOW]);
end
fclose(fid);
