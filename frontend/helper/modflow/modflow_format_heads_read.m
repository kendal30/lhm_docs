function headsStruct = modflow_format_heads_read(inFile)
%MODFLOW_FORMAT_HEADS_READ  Reads formatted heads from MODFLOW
%   headsStruct = modflow_format_heads_read(inFile)
%
%   Note, this will read in the entire inFile, which can require lots of
%   time and memory for larger files.
%
%   Descriptions of Input Variables:
%   inFile: full path to the input file
%
%   Descriptions of Output Variables:
%   headsStruct: output structured array containing heads and model time
%   arrays
%
%   Example(s):
%   >> modflow_format_heads_read
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-03-14
% Copyright 2009 Michigan State University.


%Open the input file
fid = fopen(inFile);

%Specify the format of the array information line
format1 = '%d %d %f %f %s %d %d %d %s';
%fields are: nstep, nper, tper, tmodel, type, ncol, nrow, laynum, format

%Loop through the grids, reading them to a structured array
loopStruct = struct();
maxLay = 1;
loopInd = 0;
loop = true;
while loop
    %Read in the first line
    header = textscan(fid,format1,1);
    
    %Test to see if this failed (i.e. end of file is reached)
    if isempty(header{1}),break;end
    loopInd = loopInd + 1;
   
    %Now, read the array
    array = textscan(fid,'%f32',header{6} * header{7});
    
    %Save to the structure
    loopStruct.(['array',num2str(loopInd)]).time = header{4};
    loopStruct.(['array',num2str(loopInd)]).lay = header{8};
    loopStruct.(['array',num2str(loopInd)]).array = reshape(array{1},header{6},header{7})'; %Reshape the array
    
    %Check to see if the current layer is the highest laynum so far
    maxLay = max(maxLay, header{8});
end
fclose(fid);

%Now, restructure this output into a 4-D array
numSteps = loopInd / maxLay;
array = zeros([size(loopStruct.array1.array),maxLay,numSteps],'single');
time = zeros(numSteps,1);

loopInd = 0;
for m = 1:numSteps
    for n = 1:maxLay
        loopInd = loopInd + 1;
        array(:,:,n,m) = loopStruct.(['array',num2str(loopInd)]).array;
    end
    time(m) = loopStruct.(['array',num2str(loopInd)]).time;
end

%Now, create the output
headsStruct.time = time;
headsStruct.heads = array;