function [package] = riv_write(path,package,structure,params,constants,mult,grids) % #ok<INUSL,INUSD>

% Determine which cells are drain cells
indRivers = grids.riv > 0;

% Calculate: mxriv
mxriv=sum(indRivers(:));

% Generate: arrays for drain file
% [lay,row,col,riv_elev,riv_cond,riv_bot]
% initialize arrays
lay = zeros(mxriv,1) + 1; % all in layer one for now
[row,col] = ind2sub([structure.num_row,structure.num_col],(1:structure.num_row*structure.num_col));

% Assemble the rivArray
rivArray = [grids.drn_layer(indDrains)',row(indRivers),col(indRivers),...
    grids.riv_elev(indRivers)',grids.riv_cond(indRivers)',grids.riv_bot(indRivers)']';

% write out riv file
% Create the format strings
format1 = '% g % g\n';
format1a = '% g % g';
format1b = '% 8s\n';
format3 = '% 1d % 4d % 4d % 6f % 12f % 6f\n';

% Print the riv file
fid = fopen([path,filesep,package.name],'wt');
fprintf(fid,format1a,[mxriv,package.inputs.riv_budget]);
fprintf(fid,format1b,package.inputs.riv_print_string);
fprintf(fid,format1,[mxriv,package.inputs.riv_npar]);
fprintf(fid,format3,rivArray);
if structure.nper>1
    for m = 1:structure.nper % needs to be changed if an SS ramp-up is added
        fprintf(fid,format1,[-1 0]);
    end
end
fclose(fid);