function [package] = zon_write(path,package,structure,params,constants,mult,grids) %#ok<INUSL,INUSD>
%Create format strings
format2='%g\n';
format3 = [repmat('%g ',1,structure.num_col-1),'%g\n'];
format3c='%s\n';

%Print the ZON file
fid1=fopen([path,filesep,package.name],'wt');
fprintf(fid1,format2,package.inputs.nzn);
for j=1:structure.num_lay
    hk_zone=strcat(package.inputs.zonarr_name_hk,num2str(j));
    fprintf(fid1,format3c,hk_zone);
    fprintf(fid1,format3c,package.inputs.array_header);
    fprintf(fid1,format3,grids.hk(:,:,j)');
    vani_zone=strcat(package.inputs.zonarr_name_vani,num2str(j));
    fprintf(fid1,format3c,vani_zone);
    fprintf(fid1,format3c,package.inputs.array_header);
    fprintf(fid1,format3,grids.vani(:,:,j)');
end
fclose(fid1);