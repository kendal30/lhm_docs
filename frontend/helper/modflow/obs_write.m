function [package] = obs_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

format1a = '%s ';
format1b = '%d';

%Print the OBS file
fid1 = fopen([path,filesep,package.name],'wt');
fprintf(fid1,format1a,structure.model_name);
fprintf(fid1,format1b,package.inputs.iscals);
fclose(fid1);
