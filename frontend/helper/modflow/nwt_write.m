function [package] = nwt_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

format1='%g %d %d %g %d %d %d %s';
format1a = '%s';
fid=fopen([path,filesep,package.name],'wt');
fprintf(fid,format1,[package.inputs.HEADTOL,package.inputs.FLUXTOL,package.inputs.MAXITEROUT,...
    package.inputs.THICKFACT,package.inputs.LINMETH,package.inputs.IPRNWT,package.inputs.IBOTAV]);
fprintf(fid,format1a,package.inputs.OPTIONS);
fclose(fid);