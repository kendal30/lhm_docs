function [package] = bas_write(path,package,structure,params,constants,mult,grids) %#ok<INUSL>

%Create output format strings
format2='%s\n';
format3 = [repmat('%g ',1,structure.num_col - 1),'%g\n'];
format4='%6.1f\n';

%Print the BAS file
fid1=fopen([path,filesep,package.name],'wt');
fprintf(fid1,format2,package.inputs.progress_option);
for m=1:structure.num_lay
    fprintf(fid1,format2,package.inputs.array_header);
    fprintf(fid1,format3,grids.ibound(:,:,m)');
end
fprintf(fid1,format4,structure.hnoflo);
for m=1:structure.num_lay
    fprintf(fid1,format2,package.inputs.array_header);
    fprintf(fid1,format3,grids.start_heads(:,:,m)');
end
fclose(fid1);