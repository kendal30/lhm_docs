function [package] = pcg_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

format1='%d %d %d\n';
format2='%g %g %g %d %d %d %g\n';
fid=fopen([path,filesep,package.name],'wt');
fprintf(fid,format1,[package.inputs.MXITER,package.inputs.ITER1,package.inputs.NPCOND]);
fprintf(fid,format2,[package.inputs.HCLOSE,package.inputs.RCLOSE,package.inputs.RELAX,...
    package.inputs.NPBOL,package.inputs.IPRPCG,package.inputs.MUTPCG,package.inputs.DAMP]);
fclose(fid);
