function [package] = dis_write(path,package,structure,params,constants,mult,grids) %#ok<INUSL>

% Reformat some inputs
package.inputs.laycbd = cell2mat(package.inputs.laycbd);

%Create format strings
format2='%g %g %g %g %g %g\n';
format3 = [repmat('%g ',1,structure.num_lay-1),'%g\n'];
format4='%s';
format4a=' %13.9f\n';
format5 = [repmat('%g ',1,structure.num_col-1),'%g\n'];
format5c='%s\n';
format6='%g %g %g ';
format6a='%s\n';

%Print the DIS file
fid1=fopen([path,filesep,package.name],'wt');
fprintf(fid1,format2,[structure.num_lay, structure.num_row, structure.num_col,...
    structure.nper,structure.timeunits,structure.lenunits]);
fprintf(fid1,format3,package.inputs.laycbd);
fprintf(fid1,format4,package.inputs.constant_header);
fprintf(fid1,format4a,structure.delr);
fprintf(fid1,format4,package.inputs.constant_header);
fprintf(fid1,format4a,structure.delc);
fprintf(fid1,format5c,package.inputs.array_header);
fprintf(fid1,format5,grids.model_top');
for j=1:structure.num_lay
    fprintf(fid1,format5c,package.inputs.array_header);
    fprintf(fid1,format5,grids.bottom(:,:,j)');
end
for j=1:structure.nper
    fprintf(fid1,format6,[structure.perlen(j),structure.nstp(j),structure.tsmult(j)]);
    fprintf(fid1,format6a,structure.stress_period_type{j});
end
fclose(fid1);