function [package] = rch_write(path,package,structure,params,constants,mult,grids) %#ok<INUSD>

%Create output format strings
format1='%g %g\n';
format3a='%s\n'; 

%Print out the RCH file
fid1=fopen([path,filesep,package.name],'wt');

fprintf(fid1,format1,[package.inputs.nrchop,package.inputs.rch_budget]);
%Currently, this uses an external binary file for the recharge
for n = 1:structure.nper
    fprintf(fid1,format1,[package.inputs.inrech,package.inputs.inirch]);
    fprintf(fid1,format3a,package.inputs.array_header_ext);
end

fclose(fid1);

%Other options for using RCH params or RCH grids (not updated for new structure):
% 
% %Write out RCH file
% cd(model_path)
% fid1=fopen(rch_name,'wt');
% if rch_method{2}==1 %use params for RCH
%     %Create format strings
%     format1='%s';
%     format1a=' %g\n';
%     format2='%g %g\n';
%     format3a1='%s';
%     format3a2=' %s';
%     format3b=' %g %g\n';
%     format4a='%g ';
%     format4c=' %g\n';
%     format5='%g\n';
%     format6='%s';
% 
%     %Print the RCH file
%     fprintf(fid1,format1,param_string);
%     fprintf(fid1,format1a,sum(num_rch_params));
%     fprintf(fid1,format2,[nrchop rch_budget]);
%     zonarr=strcat(zonarr_name_rech,num2str(1)); %same zones each stress period
%     for h=1:nper
%         for i=1:num_rch_params(h)
%             parnam=strcat(num2str(rch_param_names(h,i)));%strcat(partype_rech,'_',num2str(abs(rch_param_names(h,i))));
%             fprintf(fid1,format3a1,parnam);
%             fprintf(fid1,format3a2,partype_rech);
%             fprintf(fid1,format3b,[rch_param_vals(h,i),nclu]);
%             fprintf(fid1,format3a1,mltarr);
%             fprintf(fid1,format3a2,zonarr);
%             fprintf(fid1,format4c,rch_zone_names(i));
%         end
%     end
%     rch_index=0;
%     for j=1:nper
%         inrech=num_rch_params(j);
%         fprintf(fid1,format5,inrech);
%         rch_param_names_per=rch_param_names(rch_index+1:rch_index+num_rch_params(h));
%         rch_index=rch_index+num_rch_params(h);
%         for k=1:num_rch_params(j)
%             parnam=strcat(num2str(abs(rch_param_names(j,k))));%strcat(partype_rech,'_',num2str(abs(rch_param_names(j,k))));
%             fprintf(fid1,format6,parnam);
%             fprintf(fid1,format4c,[-1]); %to avoid printing recharge parameters to LISTing file
%         end
%     end
% else %write arrays to file directly
%     %Create the format strings
%     format1='%g %g\n';
%     format2=[repmat('%g ',1,num_col-1),'%g\n'];
%     format3a3='%s\n';
%     %Print the RCH file
%     fprintf(fid1,format1,[nrchop rch_budget]);
%     inrech=1; %see modflow docs, specifies that a new grid will be written for each stress period
%     inirch=1; %see modflow docs
%     for h=1:nper
%         fprintf(fid1,format1,[inrech,inirch]);
%         fprintf(fid1,format3a3,'INTERNAL 1.0 (free) 0');  %Added 7-1-05 NRW
%         fprintf(fid1,format2,recharge_vals{h});
%     end
% end
% 
% fclose(fid1);
% cd(script_path)