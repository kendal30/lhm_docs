function [package] = evt_write(path,package,structure,params,constants,mult,grids) %#ok<INUSL,INUSD>

%Create output format strings
format1 = '%g %g\n';
format1a = '%g %g %g\n';
format2 = [repmat('%g ',1,structure.num_col-1),'%g\n'];
format3a = '%s\n'; 

%Print out the EVT file
fid1=fopen([path,filesep,package.name],'wt');

fprintf(fid1,format1,[package.inputs.et_layer_option,package.inputs.evt_budget]);

%Print the stress periods, 1st stress period specify surf and exdp arrays,
%plus external ET rate, all others just external ET rate
for n = 1:structure.nper
    if n == 1
        fprintf(fid1,format1a,[1 1 1]); %array is [INSURF, INEVTR, INEXDP]
        %First, print the ET surface grid
        fprintf(fid1,format3a,package.inputs.array_header);
        fprintf(fid1,format2,grids.et_surf');
        %Next, print the external header for the rate grid
        fprintf(fid1,format3a,package.inputs.array_header_ext);
        %Third, print the ET extinction depth grid
        fprintf(fid1,format3a,package.inputs.array_header);
        fprintf(fid1,format2,grids.et_extinct_depth');
    else
        fprintf(fid1,format1a,[-1 1 -1]); %reuse SURF and EXDP, newr EVTR
        fprintf(fid1,format3a,package.inputs.array_header_ext);
    end
end
fclose(fid1);