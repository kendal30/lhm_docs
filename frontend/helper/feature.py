def read_feature_class(pathFeature,addAttribs=[],tableOnly=False):
    """This function reads the geometries of a feature class directly into a
    dataframe, along with additional desired attributes, specified as the second
    argument, which must be a list"""
    import ast, arcpy, pandas

    if tableOnly:
        if len(addAttribs) == 0:
            addAttribs = '*'

        tableFeature = arcpy.da.TableToNumPyArray(pathFeature,addAttribs)
        dfFeature = pandas.DataFrame(tableFeature)
    else:
        # Start by reading in the feature class directly to a numpy array, then to a dataframe
        readAttribs = ['OID@','SHAPE@JSON']
        readAttribs.extend(addAttribs)

        tableFeature = arcpy.da.FeatureClassToNumPyArray(pathFeature,readAttribs)
        dfFeature = pandas.DataFrame(tableFeature)

        # Read in the geometries to fix 'SHAPE@JSON' which is truncated by the FeatureClassToNumPyArray operation
        with arcpy.da.SearchCursor(pathFeature,['OID@','SHAPE@JSON']) as cursor:
            for row in cursor:
                dfFeature.loc[dfFeature['OID@']==row[0],'SHAPE@JSON'] = row[1]

        # Interpet those geometries
        jsonToList = lambda x: [[ast.literal_eval(x['SHAPE@JSON'])['rings'][0]]]
        dfFeature['SHAPE@JSON'] = dfFeature.apply(jsonToList,axis=1)

        # Rename fields, and drop OID
        dfFeature = dfFeature.rename({'SHAPE@JSON':'shape'},axis=1)
        dfFeature = dfFeature.drop('OID@',axis=1)

    return dfFeature

def export_feature_subset(pathInputFeature,pathOutputFeature,subField,subValue):
    import arcpy

    # Check the input type
    if isinstance(subValue,float):
        convertString = '%f'
    elif isinstance(subValue,int):
        convertString = '%d'
    else:
        convertString = '%s'

    #Create selection layer
    arcpy.MakeFeatureLayer_management(pathInputFeature,'select_layer',('%s='+convertString)%(subField,subValue))

    #Copy this to the output
    arcpy.CopyFeatures_management('select_layer',pathOutputFeature)

def intersect_grid_feature(pathGrid,pathFeature,addAttribs=[],rasterAttrib=None,\
    rasterPath=None,templateRaster=None,flopy=False):
    """This function intersects the shapefile grid with a feature of interest,
    to determine which cells are within that feature, and what area is contained
    of each cell is within the feature. It returns a dataframe.

    The grid feature specified by pathGrid must contain fields "row" and "col"
    which are the row and column number of the grid, or the original grid from
    which a refined grid was created with gridgen.

    Also present must be a cell or node id fields, either "cellid" or "nodenumber",
    the latter of which is the output from the USGS gridgen code.

    Optionally, you may specify a rasterAttrib that will create a raster of that
    attribute from the intersected feature, matching another specified input,
    templateRaster.

    If calling from flopy, specify flopy=True to return 0-based cellid."""

    import arcpy, pandas, numpy

    #Determine if this is a geodatabase, if not will handle differently below
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType

    # Intersect the shapefile grid, store as a temporary feature in the scratch directory
    if workspaceType in [u'FileSystem','FileSystem']:
        thisTempFeature = 'tempIntersect.shp'
    else:
        thisTempFeature = 'tempIntersect'

    # Intersect the shapefile grid, store as a temporary feature in the scratch directory
    arcpy.Intersect_analysis([pathGrid,pathFeature],thisTempFeature)

    # Determine the shape type of the intersected features
    shapeType = arcpy.Describe(thisTempFeature).shapeType.lower()

    # Compute the area of the intersected features, a geodatabase will already have this
    if workspaceType in [u'FileSystem','FileSystem']:
        # Compute a field called 'Shape_area'
        try:
            if shapeType == 'polygon':
                arcpy.AddField_management(thisTempFeature,'Shape_area','float')
            elif shapeType == 'polyline':
                arcpy.AddField_management(thisTempFeature,'Shape_len','float')

        except:
            pass

        # Compute the area
        if shapeType == 'polygon':
            arcpy.CalculateField_management(thisTempFeature,'Shape_area','!shape.area!')
        elif shapeType == 'polyline':
            arcpy.CalculateField_management(thisTempFeature,'Shape_len','!shape.length!')


    # Export a raster from the feature, if specified
    if rasterAttrib:
        # See if the templateRaster is a Raster object or a string
        if isinstance(templateRaster,str):
            templateRaster = arcpy.Raster(templateRaster)

        # Make sure that we handle the coordinate system correctly -- and set compression
        priorCoord = arcpy.env.outputCoordinateSystem
        priorExtent = arcpy.env.extent
        priorCompression = arcpy.env.compression

        arcpy.env.outputCoordinateSystem = templateRaster
        arcpy.env.extent = templateRaster
        arcpy.env.compression = 'LZ77'

        # Run feature to raster
        arcpy.FeatureToRaster_conversion(thisTempFeature,rasterAttrib,rasterPath,templateRaster.meanCellWidth)

        # Clean up
        del templateRaster

        # Restore environment settings
        arcpy.env.outputCoordinateSystem = priorCoord
        arcpy.env.compression = priorCompression
        arcpy.env.extent = priorExtent

    # Determine whether we are working with a grid generated by Gridgen
    listFields = [f.name for f in arcpy.ListFields(thisTempFeature)]
    if 'cellid' in listFields:
        fieldCellID = 'cellid'
        testRename = False
    elif 'nodenumber' in listFields:
        fieldCellID = 'nodenumber'
        testRename = True
    elif 'node' in listFields:
        fieldCellID = 'node'
        testRename = True
    else:
        raise ValueError('no cell or node ID field present in model grid feature')

    #Determine which attributes to read
    if shapeType == 'polygon':
        readAttribs = ['Shape_area',fieldCellID, 'row','col']
    elif shapeType == 'polyline':
        if workspaceType in [u'FileSystem','FileSystem']:
            readAttribs = ['Shape_len',fieldCellID, 'row','col']
        else:
            readAttribs = ['Shape_length',fieldCellID,'row','col']
    elif shapeType == 'point':
        readAttribs = [fieldCellID, 'row','col']
    else:
        print(shapeType)
        return
    readAttribs.extend(addAttribs)

    # Get the attribute table
    tableAttrib = arcpy.da.TableToNumPyArray(thisTempFeature,readAttribs)
    dfAttrib = pandas.DataFrame(tableAttrib)

    # Get the cellid field as an integer, may need to rename it or 0-base it
    if testRename:
        dfAttrib['cellid'] = dfAttrib[fieldCellID]
        dfAttrib = dfAttrib.drop([fieldCellID],axis=1)
    if flopy:
        dfAttrib['cellid'] = dfAttrib['cellid'] - 1 #flopy needs 0-based
        dfAttrib['row'] = dfAttrib['row'] - 1
        dfAttrib['col'] = dfAttrib['col'] - 1
    dfAttrib['cellid'] = dfAttrib['cellid'].astype(numpy.int64)

    if workspaceType in [u'FileSystem','FileSystem']:
        dfAttrib = dfAttrib.rename(columns={'Shape_len':'Shape_length'})
        
    #Clean up the intersected feature
    arcpy.Delete_management(thisTempFeature)

    return dfAttrib
