function asciiwrite(inputArray,outputName,headerData,options)
%ASCIIWRITE  Write out a MATLAB array as an ASCII-format Grid
%   asciiwrite(inputArray,outputName,headerData,options)
%
%   Descriptions of Input Variables:
%   - inputArray: a MATLAB vector or array to be output.  Total number of
%   output cell values are O=MxN.  Dimensions of input array can be Ox1,
%   1xO, MxN, or NxM.
%   - outputName: the name of the output ascii file to be written.  If the
%   file is not to be written into the current directory, this should
%   include the entire path to the output file location.  If no extension
%   is specified, a default extension will be applied as appropriate for
%   each output type.
%   - headerData: a structured array with fields 'cols', 'rows', 'left',
%   'bottom', 'cellsize', and 'noData'.  'left' is the x-coordinate of
%   the edge of the grid cell, similarly for bottom.  This differs from
%   a notation in which the grid node location is specified.
%   - options: a structured array with fields 'int' (a logical value
%   indicating whether the output is an integer array), or 'type' with
%   possible values of 'arc' or 'surfer'. This is an optional field, and if
%   not (completely) specified, default values are int=false, type='arc'
%
%   Descriptions of Output Variables:
%   none
%
%   Example(s):
%   >> asciiwrite(matlabArray,'test.asc',struct('cols',50,'rows',50,'left',...
%   10000,'yllcorner',10000,'cellsize,100,'noData',-9999),struct('int',false,'type','arc'));
%
%   See also: asciiread

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-02-25
% Copyright 2008 Michigan State University.

%Set the default values of options
intFlag = false;
gridType = 'arc';

%If input flags are specified, parse
if nargin==4
    optFields = fieldnames(options);
    for m=1:length(optFields)
        switch lower(optFields{m})
            case 'int'
                intFlag = options.(optFields{m});
            case 'type'
                gridType = options.(optFields{m});
            otherwise
                warning('Unrecognized flag type'); %#ok<WNTAG>
        end
    end
end

%Check to see if the output name has an extension, if not, append default
%extension
testName = strtok(outputName,'.');
if strcmp(testName,outputName)
    switch gridType
        case 'arc'
            defaultExt = '.asc';
        case 'surfer'
            defaultExt = '.grd';
        otherwise
            error('Unrecognized output type')
    end
    outputName=[outputName,defaultExt];
end

%Handle alternate specifications of structure
if isfield(headerData,'num_col')
    headerData.cols = headerData.num_col;
end
if isfield(headerData,'num_row')
    headerData.rows = headerData.num_row;
end
if isfield(headerData,'bot')
    headerData.bottom = headerData.bot;
end

%Check to make sure that the number of elements input matches the specified grid dimensions
if numel(inputArray)~=(headerData.cols*headerData.rows)
    error('Size of the input array must match the specified size of the output grid')
end
%Reshape the array into a grid with the proper dimensions
inputSize = size(inputArray);
if sum(inputSize==[1,1])>0 %then the array is either a column or row vector, reshape
    inputArray = reshape(inputArray,headerData.rows,headerData.cols);
elseif inputSize(1)==headerData.cols %the array needs to be transposed
    inputArray = inputArray';
elseif sum(inputSize==[1,1])==0 && ~inputSize(1)==headerData.rows
    error('The input array size cannot be readily transformed to match the specified output dimensions')
end
%Now at this point, the grid is oriented to display correctly in MATLAB,
%reorientation for fprintf is done on a type-specific basis below

%reassign NOData value
switch gridType
    case 'arc'
        headerData.noData = -9999;
    case 'surfer'
        headerData.noData = 1.70141e38;
    otherwise
        error('Unrecognized output type')
end
inputArray(isnan(inputArray))=headerData.noData; 

%specify format strings for integer or float grids
if intFlag
    formatBase = '%i';
else
    formatBase = '%14.9g';
end

%build a format string of the length ncols, the last one must end in a carriage return
formatString = [repmat([formatBase,' '],1,headerData.cols-1),[formatBase,'\n']];
    
%write out file
fid=fopen(outputName,'wt');
switch gridType
    case 'arc'
        headerStrings = {'ncols';'nrows';'xllcorner';'yllcorner';'cellsize';'NODATA_value'};
        headerFields = {'cols','rows','left','bottom','cellsize','noData'};
        %Parse required header fields properly
        if ~isfield(headerData,'cellsize')
            if isfield(headerData,'cellsizeX')
                headerData.cellsize = headerData.cellsizeX;
            end
        end
    
        %write out the header
        for i=1:6
            fprintf(fid,'%-14s',headerStrings{i});
            fprintf(fid,'%14.14g\n',headerData.(headerFields{i}));
        end
        inputArray = inputArray'; %ARC reads in from the top down
    case 'surfer' %see the surfer documentation for a description of these variables
        headerStrings = {'DSAA'};        
        fprintf(fid,'%s\n',headerStrings{1});
        fprintf(fid,'%d %d\n',[headerData.cols,headerData.rows]);
        xlo = headerData.left + headerData.cellsize/2;
        xhi = headerData.left + headerData.cellsize*headerData.cols - headerData.cellsize/2;
        fprintf(fid,'%14.9f %14.9f\n',[xlo,xhi]);
        ylo = headerData.bottom + headerData.cellsize/2;
        yhi = headerData.bottom + headerData.cellsize*headerData.rows - headerData.cellsize/2;
        fprintf(fid,'%14.9f %14.9f\n',[ylo,yhi]);
        zlo = min(min(inputArray));
        zhi= max(max(inputArray(inputArray~=headerData.noData)));
        fprintf(fid,'%14.9f %14.9f\n',[zlo,zhi]);
        inputArray = transpose(flipud(inputArray)); %surfer reads in from the bottom up
end

%write out the array values
fprintf(fid,formatString,inputArray);
fclose(fid);
