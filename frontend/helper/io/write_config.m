function paramsStruct = write_config(configFile,paramsStruct)
%READ_CONFIG  Writes and merges config files to .yml format
%   paramsStruct = write_config(configFile,paramsStruct)
%
%   Reads configuration files and outputs them properly. Calls itself
%   recursively.
%
%   Descriptions of Input Variables:
%   configFile: a string for the full path to configuration file to be written to,
%               will be merged if sections/keys already exist
%   paramsStruct: LHM params structure to be written to config file
%
%   Descriptions of Output Variables:
%   paramsStruct: LHM params structure, merged from both config files
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] msu [dot] edu
% Created: 2022-09-01
% Updated: 
% Copyright 2022 Michigan State University.

% Parse and validate the inputs
assert(ischar(configFile),'Input must be a cell array or a string')
assert(isstruct(paramsStruct),'Input must be a structure')

% Check to see if the config file already exists
if exist(configFile,'file')
    % If it does, read it in
    paramsStructExist = read_config(configFile);

    % Merge the new paramsStruct with the existing one
    paramsStruct = merge_structs(paramsStructExist,paramsStruct);
end

% Write the merged config file
yaml.WriteYaml(configFile,paramsStruct,false);
end


function exist = merge_structs(exist,new)
if isstruct(new)
    if isstruct(exist)
        fieldsExist = fieldnames(exist);
        fieldsNew = fieldnames(new);
        for m = 1:length(fieldsExist)
            if any(strcmp(fieldsExist{m},fieldsNew))
                exist.(fieldsExist{m}) = merge_structs(exist.(fieldsExist{m}),new.(fieldsExist{m}));
            end
        end
        for m = 1:length(fieldsNew)
            if ~any(strcmp(fieldsNew{m},fieldsExist))
                exist.(fieldsNew{m}) = new.(fieldsNew{m});
            end
        end
    else
        exist = new;
    end
elseif ischar(new) || iscell(new)
    exist = new;
else
    exist = new;
end
end