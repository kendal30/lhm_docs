function stat_obs_write_grid_group(outFile,outGroup,outGrids)

%Write the output group, create the file if necessary
h5groupcreate(outFile,outGroup);

outGroupGrids = fieldnames(outGrids);
for m = 1:length(outGroupGrids)
    outGroupGrid = outGroupGrids{m};
    thisOutGroup = [outGroup,'/',outGroupGrid];
    thisOutGrid = outGrids.(outGroupGrid);
    %Create the output group
    h5groupcreate(outFile,thisOutGroup);
    %Write the grid data
    attribs = struct('class',class(thisOutGrid.data));
    h5datawrite(outFile,[thisOutGroup,'/data'],thisOutGrid.data,attribs);
    %Write the source and header fields
    outSubStructs = {'source','header'};
    for n = 1:length(outSubStructs)
        outSubStruct = outSubStructs{n};
        thisOutGroupStruct = [thisOutGroup,'/',outSubStruct];
        thisOutStruct = thisOutGrid.(outSubStruct);
        %Create the output group
        h5groupcreate(outFile,thisOutGroupStruct);
        %Write the structured array
        if ~islogical(thisOutStruct)
            outFields = fieldnames(thisOutStruct);
            for o = 1:length(outFields)
                outField = outFields{o};
                attribs = struct('class',class(thisOutStruct.(outField)));
                h5datawrite(outFile,[thisOutGroupStruct,'/',outField],thisOutStruct.(outField),attribs);
            end
        else
            attribs = struct('class',class(thisOutStruct));
            h5datawrite(outFile,[thisOutGroupStruct,'/',outField],thisOutStruct,attribs);
        end
    end
end

end