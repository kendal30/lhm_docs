function overwriteOut = get_overwrite_pref(outFile,outGroup)

persistent overwriteAll

%First, check to see if the dataset already exists, if not, prompt the user
if isempty(overwriteAll)
    overwriteAll = prompt_user(outFile,outGroup);
else
    if ~overwriteAll %last time, user elected to only overwrite one, not all
        overwriteAll = prompt_user(outFile,outGroup);
    end
end

%Write output
overwriteOut = overwriteAll;

end

function overwriteAll = prompt_user(outFile,outGroup)
if usejava('desktop')
    promptString = ['Group ', outGroup, ' in outFile ', strrep(outFile,'\','\\'),' already exists, rename existing file to old_*? Yes/No/All (y/n/a)'];
    overwritePrompt = input(promptString,'s');
    switch lower(overwritePrompt)
        case 'y'
            overwriteAll = false;
        case 'n'
            error('Input file and group already exist and were not overwritten, stopping');
        case 'a'
            overwriteAll = true;
        otherwise
            warning('Unrecognized selection, please choose only (y/n/a)')
            overwriteAll = get_overwrite_pref(outFile,outGroup);
    end
else
    overwriteAll = true;
end
end
