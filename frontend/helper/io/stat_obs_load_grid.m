function gridStruct = stat_obs_load_grid(gridName,source,nanFill)

% Default to not filling NaN values
if nargin < 3
    nanFill = false;
end

% Get the grid, allow it to be in .tif, .img. or ARC grid formats
if exist([source.path,filesep,gridName,'.tif'],'file')>0
    tempGridName = [source.path,filesep,gridName,'.tif'];
elseif exist([source.path,filesep,gridName,'.img'],'file')>0
    tempGridName = [source.path,filesep,gridName,'.img'];
else
    tempGridName = [source.path,filesep,gridName];
end

% Load the grid and the header
if exist(tempGridName,'file')
    % Read in the grid
    [tempGrid,tempHeader] = import_grid(tempGridName);

    % Convert the grids to double-precision
    tempClass = class(tempGrid);
    tempGrid = double(tempGrid);

    % Convert the nan values to NaNs
    tempGrid = convert_nans(tempGrid,tempClass);

    % Fill NaNs with layer means
    if nanFill
        if isfield(source,'nanFillVal')
            testNan = isnan(tempGrid);
            tempGrid(testNan) = source.nanFillVal;
        else
            % Fill NaNs with layer means
            tempGrid = fill_nans(tempGrid,tempClass);
        end
    end

    % Get the source data structure
    tempSource = source_struct(tempGridName);

    % Save the output structured array
    gridStruct = struct('data',tempGrid, 'header',tempHeader, 'source',tempSource);
else
    % Grid not present, print a warning
    warning('LHM:gridNotFound','Grid % s not found',gridName);

    % Fill out gridStruct
    gridStruct.data = false;
    gridStruct.header = false;
    gridStruct.source = false;
end
end

function grid = convert_nans(grid,thisClass)
switch thisClass
    case {'uint8','uint16','uint32'}
        grid(grid>=(intmax(thisClass)-1)) = NaN;
        if strcmpi(thisClass,'uint8') && sum(isnan(grid(:)))==0
            % assume that this is an instance of the character array type in arcgis
            % that doesn't seem to read in correctly
            grid(grid==128) = NaN;
        end
    case {'int8','int16','int32'}
        grid(abs(grid)>=(intmax(thisClass)-1)) = NaN;
    case {'single','double'}
        grid(abs(grid)>=(realmax(thisClass)*(1-eps(thisClass)))) = NaN;
        grid(isinf(grid)) = NaN;
    otherwise
        disp('Need to test NaN vals for this dataset');
end

end

function grid = fill_nans(grid,thisClass)
% Fill by layer
numLay = size(grid,3);
for m = 1:numLay
    thisLay = grid(:,:,m);
    testNan = isnan(thisLay);

    if any(testNan(:))
        % Get the fill value
        switch thisClass
            case {'uint8','uint16','uint32','int8','int16','int32'}
                fillVal = median(thisLay(~testNan));
            case {'single','double'}
                fillVal = mean(thisLay(~testNan));
        end

        % Fill
        thisLay(testNan) = fillVal;
        grid(:,:,m) = thisLay;
    end
end
end

function source = source_struct(inPath)
source.name = inPath;
% The MD5 hash seems to be problematic, so omit for now
% source.MD5 = mMD5(inPath);

tempDir = dir(inPath);

source.date = tempDir(1).date;
end
