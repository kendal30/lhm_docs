function output = textread_struct(colNames,filename,format,varargin) %#ok<VANUS,INUSD>
%TEXTREAD_TO_STRUCT  Uses textread but outputs a structured array
%   output = textread_to_struct(colNames,filename,format,varargin)
%
%   Descriptions of Input Variables:
%   colNames: cell array containing column header names
%   filename: filename (including path if necessary) of the input file
%   format: character array specifying the read format
%   varargin: optional arguments to pass directly to textread
%
%   Descriptions of Output Variables:
%   output: structured array containing the output
%
%   Example(s):
%   >> textread_to_struct
%
%   See also: textread

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-15
% Copyright 2008 Michigan State University.

%Build the output arrays string
outStr = '[';
for m = 1:length(colNames)-1
    outStr = [outStr,colNames{m},',']; %#ok<AGROW>
end
outStr = [outStr,colNames{end},']'];
    
%Now, build the function call string
callStr = ' = textread(filename,format,varargin{:});';

%Append the two in eval
eval([outStr,callStr]);

%Process the outputs of that eval statement into the structured array
output = struct();
for m = 1:length(colNames)
    output.(colNames{m}) = eval(colNames{m});
end