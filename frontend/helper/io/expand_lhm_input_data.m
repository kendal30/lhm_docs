function [outData,outTimes] = expand_lhm_input_data(varargin)
%EXPAND_LHM_INPUT_DATA  Expands LHM-formatted input data to the full model
%grid
%
%   This function will take either a structured output of h5datareadlhm
%   (or similar structure, with fields data, index, lookup) or individual
%   inputs index, data, and lookup, to expand the data to the LHM model
%   grid.
%
%   Descriptions of Input Variables:
%   inDataStruct: structured array from h5datareadlhm (or similar fields)
%   OR
%   index:  Nx2/3 array, where N is the number of data timestesps
%   data:   NxM array, where M is the number of input data columns
%   lookup: OxPxQ array, where O and P are the model dimensions, with values
%           referring to the M cells in data, Q dimension is referred to by
%           the third column of index (if present)
%
%   Descriptions of Output Variables:
%   outData:    the expanded output data, of dimension NxOxP
%   outTimes:  times for the output data, of dimension N
%
%   Example(s):
%   >> expand_lhm_input_data
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] msu [dot] edu
% Created: 2015-06-22
% Modified:
% Copyright 2015 Michigan State University.

if nargin==1
    if ~isstruct(varargin{1})
        error('If using as single input, must be a structured array with fields data, index, and lookup');
    else
        inData = varargin{1};
    end
elseif nargin>=3
    inData = struct();
    inData.index = varargin{1};
    inData.data = varargin{2};
    inData.lookup = varargin{3};
    if nargin==4
        inData.zeros = varargin{4};
    else
        inData.zeros = false([size(inData.index,1),1]);
    end
else
    error('Either 1 or 3 inputs are required');
end

%Create the output data array
numSteps = size(inData.index,1);
numRows = size(inData.lookup,1);
numCols = size(inData.lookup,2);
outData = zeros([numSteps,numRows,numCols]);

%If no lookup column present in index, add one of all ones
if size(inData.index,2) == 2
    inData.index = cat(2,inData.index,ones(numSteps,1));
end

% Trim up the inData to match with input dataset
if size(inData.index,1) > size(inData.data,1)
    inData.index = inData.index(~inData.zeros,:);
end
    
%Loop through lookup sets
numLookups = size(inData.lookup,3);
if numLookups > 1, h = waitbar(0,'Expanding data'); end
for m = 1:numLookups
    if numLookups > 1, waitbar(m/numSteps,h); end

    %Identify which times this loop addresses
    thisTimes = inData.index(:,3) == m;
    thisData = inData.data(thisTimes,:);

    %Build the row and column subscripts
    colSub = double(reshape(inData.lookup(:,:,m),[1,size(inData.lookup(:,:,m))])); %single timestep
    colSub = repmat(colSub,[length(thisTimes),1,1]); %all timesteps
    rowSub = repmat((1:size(thisData,1))',[1,size(colSub,2),size(colSub,3)]);

    %Create the final lookup table and then populate the output dataset
    thisLookup = sub2ind(size(thisData),rowSub,colSub);
    clear rowSub colSub
    thisExpand = thisData(thisLookup);
    outData(thisTimes,:,:) = thisExpand;
    clear thisExpand
end
if nargout == 2
    outTimes = inData.index(:,1);
end

if numLookups > 1
    if ishandle(h); close(h); end
end
