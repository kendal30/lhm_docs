function stat_obs_write_table_group(outFile,outGroup,outTables)

% The group and variable are already created by stat_obs_write_grid_group
    
outVars = fieldnames(outTables);
for m = 1:length(outVars)
    % Get the variable name, build the output table group
    outVar = outVars{m};
    outTableGroup = [outGroup,'/',outVar,'/table'];
    h5groupcreate(outFile,outGroup);
    
    % Now, write the table data
    outTable = outTables.(outVar);
    fields = fieldnames(outTable);
    for n = 1:length(fields)
        attribs = struct('class',class(outTable.(fields{n})));
        h5datawrite(outFile,[outTableGroup,'/',fields{n}],outTable.(fields{n}),attribs);
    end
end

end