function outStruct = read_excel(xlsFile,sheetName,type)
%READ_EXCEL  Reads inputs from ILHM-format Excel files, outputs structure
%   outStruct = read_excel(xlsFile,sheetName,type)
%
%   This function reads inputs from the ILHM .xls input file and converts them
%   to structured output
%
%   Descriptions of Input Variables:
%   xlsFile:    name of the ILHM format .xls file, must be on the path or a
%               full path name
%   sheetName:  string with the name of the sheet to read in from xlsFile
%   type:       optional input, either 'list' or 'table'. 'list' is the
%               default value. 
%
%   Descriptions of Output Variables:
%   outStruct:  structured array containing the output
%
%   Example(s):
%   none
%
%   See also: xlsread cell2struct

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2011-03-17
% Copyright 2011 Michigan State University.

%assign default type
if nargin < 3
    type = 'list';
end

%Read in the excel file
[~,~,inCell] = xlsread(xlsFile,sheetName);

%Strip out NaN rows if present, will only be at the end
emptyRow = false(size(inCell,1),1);
for m = 1:size(inCell,1)
    emptyRow(m) = all(isnan(inCell{m,1}));
end
inCell(emptyRow,:) = [];

%Strip out NaN columns if present, will only be at the end
emptyCol = false(size(inCell,2),1);
for m = 1:size(inCell,2)
    emptyCol(m) = all(isnan(inCell{1,m}));
end
inCell(:,emptyCol) = [];

%Convert to structured array
switch type
    case 'list'%Get header names, make sure only acceptable characters
        headers = regexprep(inCell(2:end,1),'[^a-zA-Z0-9_\s]','');
        headers = regexprep(headers,' ','_');
        outStruct = cell2struct(inCell(2:end,2),headers,1);
    case 'table'
        %Get header names, make sure only acceptable characters
        headers = regexprep(inCell(1,:),'[^a-zA-Z0-9_\s]','');
        headers = regexprep(headers,' ','_');
        outStruct = cell2struct(inCell(2:end,:),headers,2);
end
