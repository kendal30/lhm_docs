function [inData] = subset_lhm_input_data(inData,startTime,endTime)
%Temporally subsets an LHM input data structure

%

% Get the index subset
subInd = bracket_index(inData.index(:,1),startTime,endTime);

% Prepare the data
inData.index = inData.index(subInd(1):subInd(2),:);
inData.zeros = inData.zeros(subInd(1):subInd(2));
inData.events = inData.events(subInd(1):subInd(2));
startData = min(inData.index(inData.events,2));
endData = max(inData.index(inData.events,2));
inData.data = inData.data(startData:endData,:);
inData.index(:,2) = inData.index(:,2) - startData + 1;
inData.index(~inData.events,2) = 0;
inData.index(inData.zeros,2) = -1;