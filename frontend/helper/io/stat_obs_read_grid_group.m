function outStruct = stat_obs_read_grid_group(inFile,inGroup)

%Determine the structure of the file
info = h5info(inFile);

%Iterate through top-level groups
outStruct = struct();
for m = 1:length(info.Groups)
    %Pick out just the group we want
    if strcmpi(info.Groups(m).Name(2:end),inGroup)
        thisGroup = info.Groups(m);
        %Now, read in the grids within this group
        for n = 1:length(thisGroup.Groups)
            thisGrid = thisGroup.Groups(n);
            thisGridName = fliplr(strtok(fliplr(thisGrid.Name),'/'));
            outStruct.(thisGridName).data = h5dataread(inFile,[thisGrid.Name,'/data'],true);
            %Now, read in the sub-structures
            for o = 1:length(thisGrid.Groups)
                thisStruct = thisGrid.Groups(o);
                thisStructName = fliplr(strtok(fliplr(thisStruct.Name),'/'));
                outStruct.(thisGridName).(thisStructName) = struct();
                for p = 1:length(thisStruct.Datasets)
                    thisDataset = thisStruct.Datasets(p);
                    thisDatasetName = fliplr(strtok(fliplr(thisDataset.Name),'/'));
                    outStruct.(thisGridName).(thisStructName).(thisDatasetName) = ...
                        h5dataread(inFile,[thisStruct.Name,'/',thisDataset.Name],true);
                end

                % If this is a 'table' substructure, convert it to a table
                if strcmpi(thisStructName,'table')
                    outStruct.(thisGridName).(thisStructName) = ...
                        struct2table(outStruct.(thisGridName).(thisStructName));
                end
            end
        end
    end
end