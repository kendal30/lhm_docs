function [dates,values] = read_monthly_table(inFile,sumCol)
%READ_MONTHLY_TABLE  Reads in a table of monthly values
%   [dates,values] = read_monthly_table(inFile,sumCol)
%
%   Note, the first column must be the year as yyyy (i.e. 1994), and the
%   first row are column headers.
%
%   Descriptions of Input Variables:
%   inFile: complete path to the input file (unless it's on the path)
%   sumCol: set this flag true if there is a column with annual summaries, 
%           if there is none, this can be omitted
%
%   Descriptions of Output Variables:
%   dates:  output matlab serial datenumber vector
%   values: output monthly values
%
%   Example(s):
%   >>
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-02-24
% Copyright 2009 Michigan State University.

%Read the data from the file
[values] = textread(inFile,'%f','headerlines',1);

%Remove the summary column if necessary
if nargin > 2
    if sumCol
        try
            values(14:12:end) = [];
        catch %#ok<CTCH>
            disp('There does not appear to be a summary column')
        end
    end
end

%Extract the years
years = values(1:13:end);
values(1:13:end) = [];

%Now, build the entire dates array
dates = datenum(repmat(years,1,12),repmat((1:12),length(years),1),repmat(1,length(years),12));
dates = reshape(dates',[],1);
