function paramsStruct = read_config(configFile,configSection,option)
%READ_CONFIG  Reads configuration files
%   paramsStruct = read_config(configFile,configSection)
%
%   Reads configuration files and outputs them properly. Calls itself
%   recursively.
%
%   Descriptions of Input Variables:
%   configFile: either a string or a cell array of strings
%   configSection: name of the section to output, if specified as empty, or not specified
%                   will return the entire structured array
%   option: can specify 'map' to get an table of mapped parameters output formatted as a table
%
%   Descriptions of Output Variables:
%   paramsStruct: LHM params structure
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] msu [dot] edu
% Created: 2014-09-25
% Updated: 2019-06-19
% Copyright 2014 Michigan State University.

% Parse and validate the inputs
assert(ischar(configFile) || iscell(configFile),'Input must be a cell array or a string')

if nargin < 2
    configSection = '';
    mapOption = false;
end
if nargin == 3
    if ischar(option)
        if strcmpi(option,'map')
            mapOption = true;
        else
            error(sprintf('Unrecognized option %s, only "map" is supported',option));
        end
    elseif islogical(option)
        mapOption = option;
    end
else
    mapOption = false;
end

%Initialize this array
if ischar(configFile)     
    try
        if ~mapOption
            % Read the config file
            paramsStruct = yaml.ReadYaml(configFile);
            
            % If a single section should be returned
            if length(configSection) > 0
                paramsStruct = paramsStruct.(configSection);
            end
            
        else
            % Read in the table
            paramsTable = readtable(configFile,'ReadVariableNames',true);
            
            %Convert to a structured array, which is needed by the LHM internals
            paramsStruct = table2struct(paramsTable);
        end
        
        
    catch
        paramsStruct = struct();
    end
else
    %Read in first config file
    paramsStruct = read_config(configFile{1},configSection,mapOption);
    
    %Read in remainder and merge, overwriting original param vals
    for m = 2:length(configFile)
        thisStruct = read_config(configFile{m},configSection,mapOption);
        paramsStruct = merge_struct(paramsStruct,thisStruct,'',true);
    end
end
