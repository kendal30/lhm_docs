function [varargout] = import_grid(file,options,box)
%import_grid  Imports ARC grids into MATLAB format
%   [varargout] = import_grid(file,options)
%
%   This function is intended to be an interface that allows LHM to be
%   developed independent of the particular interface used to read in ARC
%   grids.
%
%   Currently, it uses readgdal, a MEX file that accesses the GDAL library
%
%   Descriptions of Input Variables:
%   file:   a character array that defines the path and file of an ARC grid
%           that can be accessed by MATLAB.
%   options: a character array, or cell array.  Currently, the options
%           are: 
%               'header'.  'header' will read in just the header metadata,
%                   and not the grid itself.
%               'box'. This will read in a bounding box defind by the
%                   structure with fields 'left', 'right', 'bottom', 'top'.
%                   This structure is then the third argument.
%   box:    optional structure, used when the 'box' option is invoked.
%
%   Descriptions of Output Variables:
%   varargout: If two outputs are requested without any specified options,
%           they will be: [grid,header].  If only one is specified, then it
%           will be [grid].  If an option is specified, then the outputs will
%           be modified.  The header information will have already been
%           converted into the format that ILHM recognizes.
%
%   Example(s):
%   >> [ibound,header] = import_grid(gridPath);
%   >> [header] = import_grid(gridPath,'header');
%   >> [ibound] = import_grid(gridPath);
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-09-23
% Copyright 2008 Michigan State University.

% Parse the input options
if nargin >= 2
    if ischar(options)
        [optionsStr,headerTest] = parse_options(options);
    elseif iscell(options)
        if length(options) == 1
            [optionsStr,headerTest] = parse_options(options{1});
        else
            error('Currently, only one option string is supported')
        end
    else
        error('Options must be a character or cell array');
    end
    
else
    headerTest = false;
    boxTest = false;
end
    
if nargin == 3
    if strcmp(optionsStr,'-R')
        optionsStr = sprintf([optionsStr,'%f/%f/%f/%f'],box.left,box.right,...
            box.bottom,box.top);
        boxTest = true;
    else
        error('Only use the third argument with the box option, see HELP');
    end
elseif nargin == 2
    if strcmp(optionsStr,'-R')
        error('The box must be specified with the box option, see HELP');
    end
elseif nargin > 3
    error('Only check input arguments, see HELP');
end

% Check to see if both header and data are requested as outputs
if nargout == 2
    bothTest = true;
else
    bothTest = false;
end


% Read in the data, switching for the various options
if headerTest
    header = gdalread(file,optionsStr);
elseif boxTest
    if bothTest
        [data,header] = gdalread(file,optionsStr);
    else
        data = gdalread(file,optionsStr);
    end
else
    if bothTest
        [data,header] = gdalread(file);
    else
        data = gdalread(file);
    end
end

% Parse the header, if retrieved
if headerTest || bothTest
    header = gdal_header_parse(header);
end

% Prepare the outputs
if bothTest
    varargout{1} = data;
    varargout{2} = header;
else
    if headerTest
        varargout{1} = header;
    else
        varargout{1} = data;
    end
end

end

%Helper Functions
function [optionStr,headerTest] = parse_options(inputStr)
switch lower(inputStr)
    case 'header'
        optionStr = '-M';
        headerTest = true;
    case 'box'
        optionStr = '-R';
        headerTest = false;
    otherwise
        error('Unrecognized input option');
end
end

function geoLoc = gdal_header_parse(gdalHeader)
geoLoc=struct('top',gdalHeader.GMT_hdr(4),'left',gdalHeader.GMT_hdr(1),...
    'bottom',gdalHeader.GMT_hdr(3),'right',gdalHeader.GMT_hdr(2),...
    'cellsizeX',gdalHeader.GeoTransform(2),'cellsizeY',-gdalHeader.GeoTransform(6),...
    'rows',gdalHeader.RasterYSize,'cols',gdalHeader.RasterXSize,...
    'proj',gdalHeader.ProjectionRef);

%If square cells
if geoLoc.cellsizeX == geoLoc.cellsizeY,geoLoc.cellsize = geoLoc.cellsizeX;end

% Nan value
geoLoc.nan = gdalHeader.Band.NoDataValue;
end