function [weight] = euclidian_weight(gridX,gridY,stationXY,power,maxNeighbors)
%IDW_CALC  Calculate inverse distance weight grid
%   [weight] = idw_calc(gridX,gridY,stationXY,power,maxNeighbors)
%
%   Calculate Euclidian (distance) weight.  w = distance^power ./ sum(distance^power)
%
%   Note, this is normalized weight.
%
%   Descriptions of Input Variables:
%   gridX:  an array of dimension (1xnumCol) containing the x values of the
%           grid
%   gridY:  an array of dimension (1xnumRow) containing the y values of the
%           grid
%   stationXY:  an array of dimension (nx2) of the form (xi,yi)
%   power:  an argument specifying the power of the distance in the
%           denominator
%   maxNeighbors:   maximum number of neighbors to include, optional
%
%   Descriptions of Output Variables:
%   weight: a 3-D array of size (numRow x numCol x numStations)
%
%   Example(s):
%   >> [weightGrid] = euclidian_weight(gridX,gridY,stationsXY,-2);
%   %inverse-squared distance weight
%
%   See also: idw

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2009-03-20
% Copyright 2008 Michigan State University.

if size(stationXY,2)~=2
    error('Station XY array must be of form (xi,yi) with dimension nx2');
end

%Reshape gridX and gridY to column vectors
gridX = reshape(gridX,[],1);
gridY = reshape(gridY,[],1);

%generate preliminary station_distance, weight, and station index stacks
numStations = size(stationXY,1);
numX = length(gridX);
numY = length(gridY);

stationDist = zeros(numY,numX,numStations);
for i=1:numStations
    stationDist(:,:,i)=sqrt(repmat((gridX - stationXY(i,1))',numY,1).^2 + ...
        repmat((gridY - stationXY(i,2)),1,numX).^2);
end

%Replace 0s
test = (stationDist==0);
stationDist(test) = min(stationDist(test))*0.00000001; %don't allow any zeros here for division's sake

%Determine the total distance to stations within each cell
totalDist = repmat(sum(stationDist.^power,3),[1,1,numStations]);

%Now calculate the idw weights
weight = stationDist .^ power ./ totalDist;

%If requested, limit weights to nearest maxNeighbors
if nargin == 5
    %To save memory
    clear totalDist
    weight = single(weight);
    
    %Set top maxNeighbors weights to 0
    %Sort the limitWeight array in the 3d dimension
    limitWeight = sort(weight,3,'descend');
    
    %Now, select the threshold weight
    threshWeight = limitWeight(:,:,maxNeighbors);
    
    %To save memory
    clear limitWeight
    
    %Determine indeces of those weights less than the threshold in each
    %cell
    threshWeight = repmat(threshWeight,[1,1,size(weight,3)]);
    test = weight < threshWeight;
    
    %Set all weights below this threshold to 0
    weight(test) = 0;
    
    %To save memory
    clear threshWeight
    
    %Renormalize
    weight = weight ./ repmat(sum(weight,3),[1,1,size(weight,3)]);
    weight = double(weight);
end