function [outStats,precalc] = zonal_statistics(extractFeatures,gridDim,gridData,statType,fracHandling,precalc)
%ZONAL_STATISTICS  Calculates zonal statistics of raster data with polygons
%   [outStats,precalc] = zonal_statistics(extractFeatures,gridDim,gridData,statType,fracHandling,precalc)
%
%   Some notes, in lieu of complete documentation: extractFeatures are like
%   the output from mexshape. gridDim is like in cell_statistics, gridData
%   is 2-D for now, but could easily be extended to 3-D for
%   space-invariant, time-varying data, other inputs and behavior are like
%   cell_statistics
%
%   supported statTypes are: 'sum', 'mean', 'max', and 'min'
%
%   fracHandling can be: 'any', 'majority', or 'weight' and is
%   precalculated, so use different precalculations if different
%   fracHandling is needed. Cell_statistics allows multiple simultaneous
%   statistics, but I don't want to write it at the moment.
%
%   Descriptions of Input Variables:
%   input:
%
%   Descriptions of Output Variables:
%   outStats: Nx1 array
%
%   Example(s):
%   >> zonal_statistics
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] msu [dot] edu
% Created: 2013-11-06
% Modified:
% Copyright 2013 Michigan State University.

%Parse the input statType
switch statType
    case {'sum','mean','max','min'}
        statType = [statType,'_weighted']; %for functions below
    otherwise
        error('Unsupported statType: %s',statType);
end


if nargin<6 %no precalc data has been supplied
    precalc = struct();
    % Provide the right fields if not all present
    if ~isfield(gridDim,'cellsizeX')
        gridDim.cellsizeX = gridDim.cellsize;
    end
    if ~isfield(gridDim,'cellsizeY')
        gridDim.cellsizeY = gridDim.cellsize;
    end
    if ~isfield(gridDim,'cols')
        gridDim.cols = gridDim.num_col;
    end
    if ~isfield(gridDim,'rows')
        gridDim.rows = gridDim.num_row;
    end
    if ~isfield(gridDim,'top')
        gridDim.top = gridDim.bot + gridDim.cellsize * gridDim.num_row;
    end
    
    %Create gridX and gridY arrays for gridDim if not supplied
    if ~isfield(gridDim,'gridX')
        gridDim.x = (gridDim.left:gridDim.cellsizeX:gridDim.left+gridDim.cellsizeX*(gridDim.cols-1));
        gridDim.y = (gridDim.top:-gridDim.cellsizeY:gridDim.top-gridDim.cellsizeY*(gridDim.rows-1));
        [gridDim.gridX,gridDim.gridY] = meshgrid(gridDim.x,gridDim.y');
    end
    precalc.gridDim = gridDim;

    %Get the number of extraction features
    precalc.numFeaturesAgg = length(extractFeatures.Shape);

    %Pre-allocate the precalcWeights array
    sumCells = zeros([size(gridDim.gridX),precalc.numFeaturesAgg]);

    %Create a very-fine grid, based on the NLDAS resolution, for
    %weighting the polygons and boundaries
    veryFineDim = gridDim;
    veryFineDim.cellsizeX = gridDim.cellsizeX/10;
    veryFineDim.cellsizeY = gridDim.cellsizeY/10;
    veryFineDim.rows = gridDim.rows*10;
    veryFineDim.cols = gridDim.cols*10;
    veryFineDim.nan = -9999;
    veryFineDim.x = (gridDim.left-gridDim.cellsizeX/2:veryFineDim.cellsizeX:...
        gridDim.x(end)+gridDim.cellsizeX/2);
    veryFineDim.y = (gridDim.top+gridDim.cellsizeY/2:-veryFineDim.cellsizeY:...
        gridDim.y(end)-gridDim.cellsizeY/2);
    [veryFineDim.gridX,veryFineDim.gridY] = meshgrid(veryFineDim.x,veryFineDim.y');

    %Call cell statistics to get the weighting
    [~,~,tempWeightsPrecalc] = cell_statistics(gridDim,veryFineDim,ones(size(veryFineDim.gridX)),'sum','any');

    h = waitbar(0,'Creating polygon weight grids');
    for m = 1:precalc.numFeaturesAgg
       waitbar(m/precalc.numFeaturesAgg,h);

       %Run points in polygons to create polygon outlines on raster
       thisPolyRaster = inpolygon(veryFineDim.gridX,veryFineDim.gridY,...
           extractFeatures.Shape(m).x,extractFeatures.Shape(m).y);

       %Do cell statistics to get the sum of cells at the grid resolution
       thisSum = cell_statistics(gridDim,veryFineDim,thisPolyRaster,'sum','any',tempWeightsPrecalc);

       %Write to the final arrays
       sumCells(:,:,m) = thisSum;
    end
    if ishandle(h); close(h); end

    %Now adjust weights for fracHandling
    precalc.weights = sumCells;
    switch fracHandling
        case 'any'
            test = (sumCells > 0);
            precalc.weights(test) = 1/nansum(test(:));
        case 'majority'
            test = (sumCells > 0.5);
            precalc.weights(test) = 1/nansum(test(:));
        case 'weight'
            for m = 1:precalc.numFeaturesAgg
                thisSum = sumCells(:,:,m);
                precalc.weights(:,:,m) = thisSum ./ nansum(thisSum(:));
            end
        otherwise
            error('Unsupported fracHandling: %s',fracHandling);
    end

    %To reduce space in memory
    clear sumCells
    precalc.weights = single(precalc.weights);
end

%Get the function to call
thisFunc = str2func(statType);

%Now, calculate the zonal statistics
outStats = zeros(precalc.numFeaturesAgg,1);
for m = 1:precalc.numFeaturesAgg
    outStats(m) = thisFunc(gridData,precalc.weights(:,:,m));
end

end

function [outStat] = mean_weighted(inData,weight)
outStat = nansum(inData(:).*weight(:));
end

function [outStat] = sum_weighted(inData,weight) %this is a weighted sum, kinda weird
maxWeight = max(weight(:));
outStat = nansum(inData(:).*weight(:)./maxWeight);
end

function [outStat] = max_weighted(inData,weight) %ignores weighting, just masks
outStat = nanmax(inData(:).*(weight(:)>0));
end

function [outStat] = min_weighted(inData,weight) %ignores weighting, just masks
outStat = nanmin(inData(:).*(weight(:)>0));
end
