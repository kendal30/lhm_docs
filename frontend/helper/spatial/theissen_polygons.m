function [polygons] = theissen_polygons(gridHeader,points)
%NEAREST_NEIGHBOR  Computes a Theissen polygon index array for a grid using a supplied set of input points.
%   [polygons] = nearest_neighbor(gridHeader,points)
%
%   Output will be an array of size ROWSxCOLS (from gridHeader).  The
%   values of this array are the row indeces of the points array.  One
%   could then map, for instance, an array of precipitation gauge
%   observations (of dimensions #stationsx1) to a grid using the syntax: 
%   precipGrid = precipGauge(polygons)
%
%   If there are too few points, or delaunay triangulation fails for some
%   reason, then direct distance minimization will be used
%
%   Descriptions of Input Variables:
%   gridHeader: structured array of geolocation information for the input data
%               containing fields 'top','left','cellsizeRow','cellsizeCol',
%               'rows',and 'cols'.  'top' is the y-coordinate of the top
%               row, and 'left' is the x-coordinate of the left-most row.
%   points: an array of M points, with columns of x and y locations.
%           Dimensions are then Mx2.
%
%   Descriptions of Output Variables:
%   polygons: a grid of dimensions ROWSxCOLS (from the gridHeader) whos
%           values are row indeces of the points array.
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-29
% Copyright 2008 Michigan State University.

points = double(points);

%--------------------------------------------------------------------------
%Compute the delaunay triangulation of the inputs points
%--------------------------------------------------------------------------
%Try the delaunay method first
try
%     tri = delaunay(points(:,1),points(:,2)); %deprecated in MATLAB R2010b
    tri = DelaunayTri(points(:,1),points(:,2));
    direct = false;
catch
    direct = true;
end
if isempty(tri.Triangulation)
    direct = true;
end

%--------------------------------------------------------------------------
%Create a grid of the x and y values of each grid cell
%--------------------------------------------------------------------------
%First, calculate the x and y coordinates of the rows and columns
coordX = gridHeader.left + gridHeader.cellsizeX * (1:gridHeader.cols);  %runs from left to right
coordY = gridHeader.top - gridHeader.cellsizeY * (1:gridHeader.rows);  %runs from top to bottom
%Then, create arrays of these coordinates
gridX = repmat(coordX,gridHeader.rows,1);
gridY = repmat(coordY',1,gridHeader.cols);
%Finally, reshape these to arrays for use in dsearch
gridX = reshape(gridX',[],1);
gridY = reshape(gridY',[],1);

%--------------------------------------------------------------------------
%Compute the theissen polygons array
%--------------------------------------------------------------------------
if ~direct
%     polygons = dsearch(points(:,1),points(:,2),tri,gridX,gridY); %deprecated in MATLAB R2010b
    polygons = nearestNeighbor(tri,gridX,gridY);
else %Calculate the polygons array directly
    %First, calculate the total distance to each grid cell from every point
    distTot = zeros(length(gridX),size(points,1));
    for m = 1:size(points,1)
        distX = gridX - points(m,1);
        distY = gridY - points(m,2);
        distTot(:,m) = sqrt(distX.^2 + distY.^2);
    end
    %Then, select the point closest to each grid cell
    minDist = repmat(min(distTot,[],2),1,m);
    isMin = (distTot == minDist);
    polygons = zeros(length(gridX),1);
    for m = 1:size(points,1)
        polygons(isMin(:,m)) = m;
    end
end

%--------------------------------------------------------------------------
%Reshape the theissen polygons array to the specified grid dimensions
%--------------------------------------------------------------------------
polygons = reshape(polygons,gridHeader.cols,gridHeader.rows)';