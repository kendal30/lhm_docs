function data = scale_offset_transform(data,attribs,forward)
%SCALE_OFFSET_TRANSFORM  Applies scale, offset, and transform of HDF5 output data
%   data = scale_offset_transform(data,attribs,forward)
%
%   This function applies these three operations in the order scale,
%   offset, and then transform, or the reverse, depending on whether the
%   data are being written to HDF5 file (forward = true), or read from HDF5
%   file (forward = false);
%
%   Forward: data = transform(data*scale + offset);
%   Reverse: data = (inverse_transform(data) - offset) / scale;
%   These are completely arbitrary definitions, but will remain consistent
%   in ILHM.
%
%   The output of this function is a double precision array regardless of
%   the input type.
%
%   Descriptions of Input Variables:
%   data:       data to be manipulated
%   attribs:    structured array specifying types of scale, offsets, and
%               transforms.  Currently, only 'log10', and 'logical' are 
%               acceptable transforms.
%   forward:    logical flag; if true indicates that data are being written
%               to HDF5 and the operations should be done in the forward
%               order; if false, the operations will be done in the reverse
%               order.
%
%   Descriptions of Output Variables:
%   data:       manipulated data, of class double
%
%   Example(s):
%   >> scale_offset_transform(data,struct('scale','10','offset',100'),false);
%   %Indicates that the data are being read in from HDF5 file, and should be scaled and offset in the reverse order.
%
%   See also: h5dataread h5datawrite

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-01
% Copyright 2008 Michigan State University.

if ~strcmp(class(data),'double')
    data = double(data);
end

assert(isstruct(attribs),'attribs must be a structured array, see HELP');

if forward
    %First, apply a scale factor multiplier
    if isfield(attribs,'scale')
        assert(ischar(attribs.scale),'All attribs values must be character arrays');
        if ~strcmp(attribs.scale,'1')
            data = data * str2double(attribs.scale);
        end
    end

    %Next, apply an offset
    if isfield(attribs,'offset')
        assert(ischar(attribs.offset),'All attribs values must be character arrays');
        if ~strcmp(attribs.offset,'0')
            data = data + str2double(attribs.offset);
        end
    end

    %Finally, apply a transform, in reality this will rarely be combined with
    %the other two
    if isfield(attribs,'transform')
        assert(ischar(attribs.transform),'All attribs values must be character arrays');
        switch attribs.transform
            case 'log10'
                data = log10(data);
            case 'logical'
                data = uint8(data);
            case 'none'
            otherwise
                error(['Unrecognized transform type ',attribs.transform]);
        end
    end
else
    %First apply the inverse transform
    if isfield(attribs,'transform')
        assert(ischar(attribs.transform),'All attribs values must be character arrays');
        switch attribs.transform
            case 'log10'
                data = 10.^(data);
            case 'logical'
                data = logical(data);
            case 'none'
            otherwise
                error(['Unrecognized transform type ',attribs.transform]);
        end
    end

    %Next, remove the offset
    if isfield(attribs,'offset')
        assert(ischar(attribs.offset),'All attribs values must be character arrays');
        if ~strcmp(attribs.offset,'0')
            data = data - str2double(attribs.offset);
        end
    end
    
    %Finally, divide by the scale factor
    if isfield(attribs,'scale')
        assert(ischar(attribs.scale),'All attribs values must be character arrays');
        if ~strcmp(attribs.scale,'1')
            data = data / str2double(attribs.scale);
        end
    end    
end


