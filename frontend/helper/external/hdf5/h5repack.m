function h5repack(hfile,origFlag)
%H5REPACK  Repacks an HDF5 file with compression and chunking to save space
%   h5repack(hfile)
%
%   This currently does not support types other than strings and arrays
%   (not vlen arrays).  It also doesn't support links.
%
%   Descriptions of Input Variables:
%   hfile:      full path to the hd5 file to repack
%   origFlag:   optional logical flag that, if set true, will keep the
%               original file, but rename it with the prefix 'orig_'.  The
%               default value is 'true'.
%
%   Descriptions of Output Variables:
%   none
%
%   Example(s):
%   >> h5repack
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-12-11
% Copyright 2008 Michigan State University.

%Parse the inputs
if nargin < 2
    origFlag = true;
end

%Test to see if the full path to the file is already specified, if not,
%build it
[hdir,hfilename,hfileext] = fileparts(hfile);
if ~exist(hdir,'dir')
    %Get the complete path to the file
    hfile = which(hfile);
    [hdir,hfilename,hfileext] = fileparts(hfile);
end

%Store some internal variables
tempfilename = 'temp_repack.h5';
tempfile = [hdir,filesep,tempfilename];

options.compressionLevel = 3;
options.chunkSize = 1024;
options.maxArraySize = 200e7; %bytes
oldPrefix = 'orig_';

%First, get the the complete info of the file
info = hdf5info(hfile);

%Create the temporary repack file
fail = h5filecreate(tempfile);
if fail
    error(['Temporary file ',tempfile,' already exists'])
end

%Call the recursive array repacking function
recurse_repack(hfile,tempfile,info.GroupHierarchy,options);

%Rename the original output file
if origFlag
    system(['ren ',hfile,' ',oldPrefix,hfilename,hfileext]);
else
    delete(hfile);
end

%Rename the temporary file
system(['ren ',tempfile,' ',hfilename,hfileext]);
end

function recurse_repack(hfile,tempfile,info,options)

%First, check to see if the current group has any attributes, if so,
%re-create them
for m = 1:length(info.Attributes)
    thisAttrib = info.Attributes(m);
    
    %Repack this attribute
    repack_attributes(hfile,tempfile,thisAttrib);
end   

%Next, check to see if any datasets need to be repacked
for m = 1:length(info.Datasets)
    thisDataset = info.Datasets(m);
   
    %Check to if the dataset will fit in memory
    memEstimate = estimate_memory(thisDataset);
    
    %Now, repack the dataset
    if isempty(memEstimate) || memEstimate < options.maxArraySize
        repack_all(hfile,tempfile,thisDataset)
    else
        numChunks = ceil(memEstimate/options.maxArraySize);
        repack_chunks(hfile,tempfile,thisDataset,numChunks);
    end
end

%Finally, loop through the subgroups recursively
for m = 1:length(info.Groups)
    %Create the subgroup
    h5groupcreate(tempfile,info.Groups(m).Name);
    %Recurse into this subgroup
    recurse_repack(hfile,tempfile,info.Groups(m),options)
end

end

function repack_attributes(hfile,tempfile,info)
%First, get the current attribute
attrib = h5attget(hfile,info.Name);

%Then, write the attribute
h5attput(tempfile,info.Name,attrib);
end

function repack_chunks(hfile,tempfile,info,numChunks)
%Note, these chunks are not the HDF5 chunks, but rather chunks sized for
%MATLAB to read and write
%The order of dimensions is reversed
info.Dims = fliplr(info.Dims);

%Determine the total dimensions
numDims = length(info.Dims);

%Check to see if the highest dimension has more elements than numChunks,
%otherwise reduce numChunks--may cause memory failure...
%Also, if the highest dimension size is smaller than 16, use numChunks ==
%highest dimension size
if info.Dims(numDims) <= 15 || info.Dims(numDims) < numChunks
    numChunks = info.Dims(numDims);
end

%Create the chunk size array, slicing only the highest dimension
chunkSize = info.Dims;
highDimSize = ceil(chunkSize(numDims) / numChunks);
chunkSize(numDims) = highDimSize;

%Determine the size of the last chunk
lastChunk = chunkSize;
lastChunk(numDims) = info.Dims(numDims) - highDimSize * (numChunks - 1);

for m = 1:numChunks
    %Read in only the part of the dataset that will fit in memory
    %First, determine where to start reading the array
    start = zeros(size(chunkSize));
    start(numDims) = highDimSize * (m - 1);
    %Determine the count
    if m == numChunks
        count = lastChunk;
    else
        count = chunkSize;
    end
    %Determine the stride
    stride = ones(size(chunkSize));
    %Then, read in the array chunk
    [chunk,attribs] = h5dataread(hfile,info.Name,false,start,count,stride);
    
    %If this is the first chunk read in, create the output variable
    %note here, this is not fully optimized
    %and still wastes some space.  A better alternative would be to
    %create only the size of dataset I need, but make it extendible and
    %extend it on write.
    if m == 1
        %Create an empty dataset
        outClass = class(chunk);
        %Replace 'logical' class by uint8
        if strcmpi(outClass,'logical'),outClass='uint8';end %#ok<ISLOG>
        h5varcreate(tempfile,info.Name,info.Dims,outClass,true);
        %Write its attributes
        attribList = fieldnames(attribs);
        for n = 1:length(attribList)
            h5attput(tempfile,[info.Name,'/',attribList{n}],attribs.(attribList{n}));
        end
    end
    
    %Now, write the chunk
    h5datawrite(tempfile,info.Name,chunk,attribs,false,start,count,stride);
    
    %Now, clear the chunk from memory
    clear chunk
end    
end

function repack_all(hfile,tempfile,info)
%Read in the complete dataset with attributes
[data,attribs] = h5dataread(hfile,info.Name,false);
%Write out the complete dataset with attributes
h5datawrite(tempfile,info.Name,data,attribs,false);
end

function [memEstimate] = estimate_memory(info)
%Get the total number of elements
elements = prod(info.Dims);

%Determine the number of bytes per element
if ~isempty(strfind(info.Datatype.Class,'STRING'))
    bytesPer = [];
else
    %Check to see if this computer is little or big endian
    [str,maxsize,endian] = computer; %#ok<ASGLU>
    testExp = ['[0-9]?[0-9](?=[',endian,'][E])'];
    strType = regexp(info.Datatype.Class,testExp,'match');
    bytesPer = str2double(strType) / 8;
end
        
%Determine the estimate of memory required to read in
memEstimate = elements * bytesPer;
end