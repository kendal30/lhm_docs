function data = h5vargetcell(hfile,varname)
% This function reads a variable length string array type to a cell array
% from the HDF5 file.
%
% H5VARGETCELL(hfile,varname) reads an entire dataset to the variable given
% by VARNAME.

%
% Open file and dataset.
%
file = H5F.open (hfile, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');
dset = H5D.open (file, varname);

%
% Get dataspace and allocate memory for read buffer.
%
space = H5D.get_space (dset);
%[~, dims, ~] = H5S.get_simple_extent_dims (space);
%dims = fliplr(dims);

%
% Create the memory datatype.
%
memtype = H5T.copy ('H5T_C_S1');
H5T.set_size (memtype, 'H5T_VARIABLE');
%
% Read the data.
%
data = H5D.read (dset, memtype, 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');
data = data';
%
% Close and release resources.
%
H5D.close (dset);
H5S.close (space);
H5T.close (filetype);
H5T.close (memtype);
H5F.close (file);