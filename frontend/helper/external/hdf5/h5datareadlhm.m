function out = h5datareadlhm(h5File,dataGroup,flagTransform)
%H5DATAREADLHM  Reads a complete LHM input data group
%   out = h5datareadlhm(h5File,dataGroup)
%
%   Descriptions of Input Variables:
%   h5File:    hdf5 file to read from
%   dataGroup: data group to read from 
%   flagTransf:flag value (true/false) indicating if data should be
%               transformed to doubles and NaNs from its storage format,
%               default is true
%
%   Descriptions of Output Variables:
%   out:       output structure
%
%   Example(s):
%   >> h5datareadlhm
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2010-02-03
% Copyright 2010 Michigan State University.

if nargin < 3
    flagTransform = true;
end

% Check to see if the file even exists, return falso if not
if ~exist(h5File,'file')
    out = false;
    return
end

% Read in the data and the attributes, assuming non-subgrid data
[data,dataAttribs] = h5dataread(h5File,[dataGroup,'/data'],flagTransform);

% Read in the lookup table
lookup = h5dataread(h5File,[dataGroup,'/lookup'],false); %keep lookup table as integer

% Read in the index
index = h5dataread(h5File,[dataGroup,'/index'],flagTransform);

% Build the output structure
out = struct('data',data, 'attribs',dataAttribs, 'lookup',lookup, 'index',index);

if data == false
    % Read in the data and the attributes, assuming subgrid data
    [weights,weightsAttribs] = h5dataread(h5File,[dataGroup,'/weights'],flagTransform);
    
    % Read in the majority dataset
    majority = h5dataread(h5File,[dataGroup,'/majority'],flagTransform);
    
    % Read in the names
    numWeights = size(weights,3);
    weightNames = cell(numWeights,1);
    for m = 1:numWeights
        weightNames{m} = strip(h5dataread(h5File,[dataGroup,'/names/',num2str(m)],false));
    end
    
    % Add to the output structure
    out = rmfield(out,'data');
    out.weights = weights;
    out.attribs = weightsAttribs;
    out.majority = majority;
    out.names = weightNames;
    
elseif min(index(:,2)) > min(index(:,1))
    % This is a dataset with an LU-type index, but not a weights dataset, don't do anything more
else
    % This is a climate dataset
    % Read in the stationsXY table, if present
    stationsXY = h5dataread(h5File,[dataGroup,'/stationsXY'],flagTransform);

    %Create the events index, which indicates when data are available
    if ~islogical(index)
        events = index(:,2) > 0;
        allZeros = index(:,2) == -1;
    else
        events = false;
        allZeros = false;
    end
    
    % Add to the output structure
    out.stationsXY = stationsXY;
    out.events = events;
    out.zeros = allZeros;
end
