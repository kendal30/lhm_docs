function existFlag = h5filecreate(h5file)
%h5file: contains the full pathname to the h5file to create, if the file
%        already exists, this function will return a value of true, if it
%        does not already exist, this will return a value of false.
%Open the file for read/write access
if exist(h5file,'file')
    existFlag = true;
else
    file_id = H5F.create(h5file,'H5F_ACC_TRUNC','H5P_DEFAULT','H5P_DEFAULT');
    existFlag = false;
    %Clean up
    H5F.close(file_id);
end