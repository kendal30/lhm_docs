function h5datawrite(hfile,hvar,data,attribs,reformat,start,count,stride)
%H5DATAWRITE  Reads HD5 data using the ILHM data/attribute standards
%   h5datawrite(hfile,hvar,data,attribs,reformat,start,count,stride)
%
%   This is a high-level wrapper for the H5VARPUT function that writes a
%   dataset and sets its attributes using a standard scheme.  
%   This function will:
%   1) Convert the datatype to the value specified in the attribs struct
%       under 'class'.  Currently, 'class' can be any of the integer or
%       real dataypes, or logical as well.  If it is unspecified, the
%       datatype will not be changed.
%   2) Convert any MATLAB NaN values to a value amenable to HDF5, either
%       that specified in attribs, or a default value of intmax(class(data)),
%       or realmax(class(data)).   If the user specifies NaN values, and
%       they are present without any additional MATLAB values, this
%       function will convert those specified NaN values to MATLAB values
%       for the remainder of the function.  It's best not to pre-convert
%       NaNs.
%   3) Convert logical arrays to uint8, in order to preserve NaNs in
%       output. The corresponding h5dataread function will perform the 
%       inverse operation.
%   4) Apply scale and offset values specified in attribs.  h5dataread will
%       perform the inverse operation.
%
%   If the specified output dataset already exists, it will be written to,
%   otherwise it will be created using the specified attributes.  
%
%   This function WILL NOT: 
%   1) Convert units
%   2) Convert any NaN values if user-specified attribute NaN values are
%       already present.  This is likely a bug in upstream code and will
%       return an error.
%
%   Descriptions of Input Variables:
%   hfile:  the hdf5 file to read inputs from
%   hvar:   the complete address of the output dataset in the HDF5 file.
%           If the address does not exist at that location, a new dataset
%           will be created.
%   data:   dataset to be written.  The class of the dataset will be
%           written to the HDF5 file, so be sure to reclass the data as
%           needed.
%   attribs:a structured array containing fields 'class', 'units', 'scale',
%           'offset', 'nan', and 'transform' OR an empty string or array.  
%           If any of these fields are missing, a default 
%           value will be assigned.  Any additional fields are acceptable, 
%           and will be written. If the array is empty, a default set of 
%           attributes will be created.  'class' will not be written to the
%           HDF5 file, but is used instead to convert the datatype prior to
%           writing.
%   reformat:   a logical true/false value that, if true will execute the
%           attribute-specified scale, offset, and transform forward
%           operations, and output a double-precision array.  If false, the
%           HDF array will have the same format as was passed to this function.
%           If this value is missing or empty, a TRUE value will be assumed.
%   start:  an optional array indicating the index to start writing to.
%           Note, HDF5 indeces are 0-based, whereas MATLAB's are 1-based.
%           The array must be of length equal to the dataset dimension.
%   count:  an optional array specifying the number of elements to written in
%           each dimension.
%   stride: an optional scalar specifying the inter-element distance to
%           write in each direction.
%
%   Descriptions of Output Variables:
%   none
%
%   Example(s):
%   none
%
%   See also: h5varget h5dataread

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-01
% Copyright 2008 Michigan State University.

%--------------------------------------------------------------------------
%Fill in default values if not specified
%--------------------------------------------------------------------------
switch nargin
    case 3
        warning('No attributes were specified, all default values will be used');
        attribs = struct('class',false);
        [start,count,stride] = deal([]);
        reformat = true;
    case 4
        if isempty(attribs)
            attribs = struct('class',false);
        end
        [start,count,stride] = deal([]);
        reformat = true;
    case 5
        [start,count,stride] = deal([]);
    case 6
        error('If START is specified, COUNT must be specified as well');
    case 7
        if isempty(attribs)
            attribs = struct('class',false);
        end
        stride = [];
end
if isempty(reformat),reformat = true;end
assert(islogical(reformat),'Input "reformat" must be a logical value, see HELP');

%Strip the 'class' field from attribs
if islogical(attribs.class)
    outType = class(data);
else
    outType = attribs.class;
end
attribs = rmfield(attribs,'class');

if iscell(data) || isstring(data)
    h5varputcell(hfile,hvar,data);
else
    %--------------------------------------------------------------------------
    %Handle NaNs properly
    %--------------------------------------------------------------------------
    %Test to see if NaN values are present
    areNaN = isnan(data);
    testNaN = any(areNaN(:));

    if isfield(attribs,'nan')
        %if a user-specified NaN attribute exists, make sure that there aren't
        %user-specified NaN values along with MATLAB NaNs, this is likely
        %an error in upstream code
        areAttribNaN = (data == str2double(attribs.nan));
        testAttribNaN = any(areAttribNaN(:));
        if testNaN && testAttribNaN
            error('There are NaN values in the array, along with values assigned to NaN in the attribs array, this is likely a bug');
        elseif testAttribNaN 
            areNaN = areAttribNaN;
            clear areAttribNaN
        end
    elseif testNaN
        attribs.nan = h5default_attribs('nan',outType);
    end

    %--------------------------------------------------------------------------
    %Rescale, offset, transform, and reclass the data
    %--------------------------------------------------------------------------
    %Determine if the input dataset is logical, and must be transformed to
    %uint8
    if islogical(data)
        attribs.transform = 'logical';
        outType = 'uint8';
    end

    %Apply scale, offset, and transform to the data
    if reformat
        if isfield(attribs,'scale') || isfield(attribs,'offset') || isfield(attribs,'transform')
            data(~areNaN) = h5scale_offset_transform(data(~areNaN),attribs,true);
        end
    end

    %Reclass the data if necessary
    if ~strcmp(class(data),outType)
        fhandle = str2func(outType);
        data = fhandle(data);
    end

    %--------------------------------------------------------------------------
    %Create the dataset, if necessary
    %--------------------------------------------------------------------------
    %Test to see if the dataset already exists
    dataExists = h5varcreate(hfile,hvar);

    if ~dataExists %The output dataset does not exist, prepare attributes
        %Determine the output datatype
        if strcmp(outType,'single'),outType = 'float';end
        outType = upper(outType);
        if ~any(strcmp(outType,{'INT8', 'UINT8', 'INT16', 'UINT16', 'INT32', ...
                'UINT32', 'INT64', 'UINT64','FLOAT','DOUBLE','CHAR'}))
            error(['The dataType ',lower(outType),' is not currently supported by this function']);
        end

        %Check to see if the dataset is large enough to warrant compression
        varInfo = whos('data');
        if varInfo.bytes > 1024
            compression = true;
        else
            compression = false;
        end

        %Create the output dataset
        h5varcreate(hfile,hvar,size(data),outType,compression);

        %Write the attributes, if necessary
        attribList = fieldnames(attribs);
        if ~isempty(attribList)
            for m = 1:length(attribList)
                assert(ischar(attribs.(attribList{m})),'All attribute values must be strings');
                h5attput(hfile,[hvar,'/',attribList{m}],attribs.(attribList{m}));
            end
        end
    end

    %--------------------------------------------------------------------------
    %Convert NaNs, and write dataset
    %--------------------------------------------------------------------------
    %Convert NaN values, if any are present
    if testNaN
        data(areNaN) = str2double(attribs.nan);
    end

    clear areNaN %to save memory 

    % Finally, write the data, but first, check to see how much memory is
    % available as a transpose is required on write.  If not enough memory is
    % available then write the dataset in chunks.
    if ~isempty(count) || ~isempty(stride)
        h5varput(hfile,hvar,start,count,stride,data)
        h5varput(hfile,hvar,start,count,stride,data)
    else
        testMem = memory;
        dataMem = whos('data');
        numChunks = ceil(dataMem.bytes*2 / testMem.MaxPossibleArrayBytes);
        if numChunks > 1
            start = zeros(1,ndims(data));
            stride = ones(1,ndims(data));
            prevIndex = 0;
            for m = 1:numChunks
                count = size(data);
                if count(end) > 1
                    count(end) = floor(count(end) / numChunks);
                elseif count(end-1) > 1
                    count(end-1) = floor(count(end-1) / numChunks);
                end
                indexArray = prod(count) + prevIndex;
                h5varput(hfile,hvar,start,count,stride,reshape(data(prevIndex+1:indexArray),count))
                start(end) = count(end);
                prevIndex = indexArray;
            end
        else
            h5varput(hfile,hvar,start,count,stride,data)
        end
    end
end