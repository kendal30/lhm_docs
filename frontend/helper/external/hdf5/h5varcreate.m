function existFlag = h5varcreate(h5file,varname,dataSize,datatype,compression,chunkSize)
%datatype: options are 'INT8', 'UINT8', 'INT16', 'UINT16', 'INT32',
%           'UINT32', 'INT64', 'UINT64','FLOAT','DOUBLE', 'CHAR'
%compression: true/false
%This function will return false if the dataset already exists, and true if
%it did not exist and was created by this function.  If only two inputs
%are specified, then this function will act as a test for existence of a
%given dataset, but will not create that dataset.



%Open the file for read/write access
if exist(h5file,'file')
    file_id = H5F.open(h5file,'H5F_ACC_RDWR','H5P_DEFAULT');
else
    error('The specified HDF5 file does not exist');
end

%Try to open the dataset to see if it already exists
try
    dataset_id = H5D.open(file_id,varname);
    existFlag = true;
    %Clean up
    H5D.close(dataset_id);
catch %the dataset must be created
    existFlag = false;
    if nargin > 2
        %Validate and correct input datatype, in case the MATLAB class is specified
        datatype = upper(datatype);
        if strcmpi(datatype,'single'),datatype = 'FLOAT';end

        %Create the dataset property list
        plist_id = H5P.create('H5P_DATASET_CREATE');

        if ~strcmp(datatype,'CHAR')
            %If compression is desired, set the compression level
            if compression
                H5P.set_deflate(plist_id,3);
            end
            
            %Assign the chunk size for the array
            rank = length(dataSize);
            
            if nargin < 6
                chunkSize = min([1024 1024 ones(1,rank-2)],dataSize);
            end
            
            H5P.set_chunk(plist_id,chunkSize);
            
            %Create the file space ID
            file_space_id = H5S.create_simple(rank,dataSize,repmat({'H5S_UNLIMITED'},1,rank));
            
            %Assign a datatype
            datatype_id = H5T.copy(['H5T_NATIVE_',datatype]);
        else
            %Create the file space ID
            file_space_id = H5S.create('H5S_SCALAR');
            
            %Assign a datatype
            datatype_id = H5T.copy('H5T_C_S1');
            
            %Set the size of the string
            H5T.set_size(datatype_id,prod(dataSize));
        end
        %Create the variable in the file
        dataset_id = H5D.create(file_id,varname,datatype_id,file_space_id,plist_id);

        %Clean up
        H5T.close(datatype_id);
        H5P.close(plist_id);
        H5S.close(file_space_id);
        H5D.close(dataset_id);
    end
end
%Finish clean up
H5F.close(file_id);