function attrib = h5default_attribs(attribName,dataType)
%DEFAULT_ATTRIBS  Returns the default value of the specified attribute name
%   attrib = default_attribs(attribName,dataType)
%
%   Currently, the defined default attributes are 'units', 'scale', 'offset',
%   'nan', and 'transform'. 
%
%   Attribute names are case sensitive!
%
%   Descriptions of Input Variables:
%   attribName: a character array specifying one of the default attribute
%               names
%   dataType:   optional, unless attribName == 'nan'.  If so, datatype must
%               be an integer or real datatype.
%                
%   Descriptions of Output Variables:
%   attrib:     the default value of the attribute, a character array
%
%   Example(s):
%   [defaultAttrib] = default_attribs('nan','uint8');
%
%   See also: h5datawrite h5dataread

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-08-01
% Copyright 2008 Michigan State University.

assert(ischar(attribName),'Attribute name must be a character array');
switch attribName
    case 'units'
        attrib = '1';
    case 'scale'
        attrib = '1';
    case 'offset'
        attrib = '0';
    case 'nan'
        if contains(dataType,'int')
            attrib = num2str(intmax(dataType));
        elseif any(strcmp(dataType,{'single','double'}))
            attrib = '-99999';
        else
            error('The specified datatype has no default attribute value')
        end
    case 'transform'
        attrib = 'none';
    otherwise
        error(['Currently, no default value is specified for attribute: ',attribName])
end
end