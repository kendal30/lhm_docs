function h5varput( varargin )
% H5VARPUT:  writes an HDF5 dataset
%
% H5VARPUT(HDFFILE,VARNAME,DATA) writes an entire dataset to the variable given
% by VARNAME.
%
% H5VARPUT(HDF5FILE,VARNAME,START,COUNT,DATA) writes a contiguous portion of a
% dataset.
%
% H5VARPUT(HDF5FILE,VARNAME,START,COUNT,STRIDE,DATA) writes a non-contiguous
% portion of a dataset.
%
% Revision History:
% ADK 2018-04-04: Removed the use of deprecated H5ML functions

%Check arguments and validate inputs
narginchk(3,6);
nargoutchk(0,0);
[hfile,varname,offset,count,stride,data] = parse_h5_varput_options ( varargin{:} ); 

% Just use the defaults for opening the file
flags           = 'H5F_ACC_RDWR';
plist_id        = 'H5P_DEFAULT';

%Open the file and the dataset
file_id = H5F.open(hfile,flags,plist_id);
dataset_id = H5D.open(file_id,varname);

%In case the dataset doesn't exist
if dataset_id==-1
    H5F.close(file_id);
    return
end
datatype_id = H5ML.get_mem_datatype(dataset_id); 

% Create the appropriate mem dataspace
if isempty(offset) && isempty(count) && isempty(stride)
    filespace_id = 'H5S_ALL';
    count = size(data);
else
    % define the memory hyperslab
    filespace_id = H5D.get_space(dataset_id);
    H5S.select_hyperslab(filespace_id, 'H5S_SELECT_SET', ...
        offset, stride, count, ...
        ones(1,length(offset)));
    rank = length(count);
    
    %Check to see if the dataset should be extended
    [~,spaceDims,~] = H5S.get_simple_extent_dims(filespace_id);
    spaceDims = spaceDims(1:rank);
    [~,boundsEnd] = H5S.get_select_bounds(filespace_id);
    boundsEnd = boundsEnd(1:rank);
    
    indExtend = boundsEnd > (spaceDims - 1);
    if any(indExtend)
        newDims = max(spaceDims,boundsEnd+1);
        H5S.close(filespace_id);
        H5D.extend(dataset_id,newDims);
        filespace_id = H5D.get_space(dataset_id);
        H5S.select_hyperslab(filespace_id, 'H5S_SELECT_SET', ...
            offset, stride, count, ones(1,rank));
    end
end

% Create the appropriate output dataspace. Make it an extendible array
if ischar(data)
    memspace_id = 'H5S_ALL';
    %Commented out to try and speed up the function
    H5D.write(dataset_id, datatype_id, memspace_id, filespace_id, plist_id, data');
else
    memspace_id = H5S.create_simple(ndims(data),count,repmat(H5ML.get_constant_value('H5S_UNLIMITED'),1,ndims(data)));
    H5D.write(dataset_id, datatype_id, memspace_id, filespace_id, plist_id, permute(data,(ndims(data):-1:1)));
    H5S.close(memspace_id);
end

%Clean up
H5D.close(dataset_id);
H5F.close(file_id);
end



%===============================================================================
function [hfile,varname,start,count,stride,data] = parse_h5_varput_options ( varargin )
%
% Have to be able to check the following signatures.

% H5_VARGET(HFILE,VARIABLE,DATA)
% H5_VARGET(HFILE,VARIABLE,START,COUNT,DATA)
% H5_VARGET(HFILE,VARIABLE,START,COUNT,STRIDE,DATA)

% First argument should be the filename.
assert(ischar(varargin{1}),'MATLAB:H5VARPUT:badInput', 'File argument must be character')
hfile = varargin{1};

% 2nd argument should be the variable name.
assert(ischar(varargin{2}),'MATLAB:H5VARPUT:badInput', 'Variable name argument must be character')
varname = varargin{2};


switch nargin
    case 3
        start = [];
        stride = [];
        count = [];
        data = varargin{3};
    case 4
        error ( 'MATLAB:H5VARPUT:badInput', 'Cannot have 4 input arguments.')
    case 5
        % Given the start, stride, and count.
        assert(isnumeric(varargin{3}),'MATLAB:H5VARPUT:badInput', 'Start argument must be numeric')
        start = varargin{3};
  
        assert(isnumeric(varargin{4}),'MATLAB:H5VARPUT:badInput', 'Count argument must be numeric')
        count = varargin{4};
        
        stride = [];
        data = varargin{5};
    case 6
        % Given the start, stride, and count.
        assert(isnumeric(varargin{3}),'MATLAB:H5VARPUT:badInput', 'Start argument must be numeric')
        start = varargin{3};
        
        assert(isnumeric(varargin{4}),'MATLAB:H5VARPUT:badInput', 'Count argument must be numeric')
        count = varargin{4};
        
        assert(isnumeric(varargin{5}),'MATLAB:H5VARPUT:badInput', 'Stride argument must be numeric')
        stride = varargin{5};
        
        data = varargin{6};
    otherwise
        error ( 'MATLAB:H5VARPUT:badInput', 'Bad number of input arguments.')
        
end
%Make sure this is true
count = double(count);
start = double(start);
stride = double(stride);
end