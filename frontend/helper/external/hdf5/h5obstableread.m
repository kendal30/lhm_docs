function outTable = h5obstableread(h5File,obsGroup,optStruct)
%h%OBSTABLEREAD  Reads a complete LHM observations group 
%   out = h5obstableread(h5File,obsGroup)
%
%   Descriptions of Input Variables:
%   h5File:    hdf5 file to read from
%   obsGroup: data group to read from 
%   optStruct: optionally, output a structured array instead of a table, default is false
%
%   Descriptions of Output Variables:
%   outTable:       output table
%
%   Example(s):
%   >> 
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2022-10-31
% Copyright 2022 Michigan State University.

if nargin < 3
    optStruct = false;
end

% Check to see if the file even exists, return false if not
if ~exist(h5File,'file')
    outTable = false;
    return
end

% List the elements in the group
if strcmpi(obsGroup(1),'\')
    obsGroup = obsGroup(2:end);
end
if ~strcmpi(obsGroup(1),'/')
    obsGroup = ['/',obsGroup];
end

% Read the data
try
    info = h5info(h5File,obsGroup);
catch
    outTable = false;
    return
end

% Read in the fields within the group
obsStruct = struct();
for m = 1:length(info.Datasets)
    obsStruct.(info.Datasets(m).Name) = h5dataread(h5File,[obsGroup,'/',info.Datasets(m).Name],true);
end

% Convert this to a table, or not if specified as such
if ~optStruct
    outTable = struct2table(obsStruct);
else
    outTable = obsStruct;
end