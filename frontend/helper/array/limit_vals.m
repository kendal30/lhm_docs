function [varargout] = limit_vals(bounds,varargin)
%LIMIT_VALS  Limits values in an array to specified lower and upper bounds.
%   [varargout] = limit_vals(bounds,varargin)
%
%   This function applies upper and lower boundaries to the input arrays
%   in varargin.  All arrays are given the same boundaries.
%
%   Descriptions of Input Variables:  
%   bounds: a 1x2 vector [lb,ub], specify -Inf or Inf if no boundary is desired
%   varargin: input arrays
%
%   Descriptions of Output Variables:
%   varargout: output arrays with bounds applied
%
%   Example(s):
%   none
%
%   See also: 

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-04
% Copyright 2008 Michigan State University.

%Assert that the second bound is greater than the first
assert(bounds(2) >= bounds(1),'The upper bound must be greater than or equal to the lower bound');

for m = 1:(nargin-1)
    varargout{m} = apply_bounds(varargin{m},bounds);
end
end

function [array] = apply_bounds(array,bounds)
test = ~isinf(bounds);
if test(1)
    array(array<bounds(1)) = bounds(1);
end
if test(2)
    array(array>bounds(2)) = bounds(2);
end
end