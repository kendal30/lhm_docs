function [outStruct] = split_struct(inStruct,splitField)
%Splits an input structured array into a set of new structures based on a
%splitField value, must be a cell array field

%Find the unique entries in the splitField
splitEntries = unique(inStruct.(splitField));

%List fields in inStruct other than the splitField
groupFields = fieldnames(inStruct);
groupFields(strcmpi(groupFields,splitField)) = [];

%Loop through the splitEntries, creating the output structured array
outStruct = struct();
for m = 1:length(splitEntries)
    thisGroup = strcmpi(inStruct.(splitField),splitEntries{m});
    for n = 1:length(groupFields)
        outStruct.(splitEntries{m}).(groupFields{n}) = inStruct.(groupFields{n})(thisGroup);
        inStruct.(groupFields{n})(thisGroup) = [];
    end
    inStruct.(splitField)(thisGroup) = [];
end
end