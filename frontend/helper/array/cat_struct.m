function struct1 = cat_struct(struct1,struct2,catDim)
%CAT_STRUCT  Concatenates structured arrays of matrices. 
%   struct1 = cat_struct(struct1,struct2,catDim)
%
%   Concatenates structured arrays containing matrices along the optionally
%   specified dimension. Fieldnames must be identical
%
%   Descriptions of Input Variables:
%   struct1: structured array
%   struct2: structured array
%   catDim:  dimension to concatenate matrices along, defaults to 1
%
%   Descriptions of Output Variables:
%   struct1: the expanded struct1, with struct2 concatenated with it
%
%   Example(s):
%   none
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-07-21
% Updated: 2016-05-19
% Copyright 2008 Michigan State University.


assert(isstruct(struct1) && isstruct(struct2),'First two inputs must be structs');
fields = fieldnames(struct2);
origFields = fieldnames(struct1);
assert(all(ismember(fields,origFields)),'Fieldnames must be identical')
if nargin == 2
    catDim = 1;
end

for m = 1:length(fields)
    struct1.(fields{m}) = cat(catDim,struct1.(fields{m}),struct2.(fields{m}));
end

end