function [array] = populate_array(array,varargin)
%POPULATE_ARRAY Populates an array using index/value pairs
%   [array] = populate_array(array,varargin)
%
%   Descriptions of Input Variables:
%   array: the array to be populated
%   varargin: contains an arbitrary number of input index/value array pairs 
%       (see the example below).  Value arrays can be scalar as well.  Index
%       arrays can be either logical values, in which case their size must
%       match the dimension of the input array. Or, the index arrays can be
%       specific indeces, in which case their size can be arbitrary.
%
%   Descriptions of Output Variables:
%   array: input array filled with input index/value pairs
%
%   Example(s):
%   >> array = populate_array(array,value1,value2,...,valueN,index1,index2,...,indexn);
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-03-27
% Copyright 2008 Michigan State University.

numPairs = (nargin - 1) / 2;
% assert_LHM(rem(numPairs,1)==0,'The input must consist of index/value pairs')
for m=1:numPairs
    if any(varargin{numPairs+m}(:))
        if numel(varargin{m})>1
%             assert_LHM(all(size(varargin{m}) == size(varargin{numPairs+m})),'Sizes of index/value pairs must be equal if value is not scalar');
            array(varargin{numPairs+m}) = varargin{m}(varargin{numPairs+m});
        else
            array(varargin{numPairs+m}) = varargin{m};
        end
    end
end
end