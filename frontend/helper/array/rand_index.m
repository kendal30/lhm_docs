function randIndex = rand_index(arraySize,randDim,numSelect,options)
%RANDINDEX  Randomly selects a set of indexes of an array
%   randIndex = rand_index(arraySize,randDim,numSelect,options)
%
%   Descriptions of Input Variables:
%   arraySize:  array similar to output of the size() function on the
%               desired dataset
%   randDim:    a scalar indicating the dimension to randomly select
%               indexes from, set as empty [] if points are to be randomly
%               selected from all dimensions (i.e. the array is flattened
%               first)
%   numSelect:  number of indexes to randomly select.  If more points are
%               desired than exist in the input set, the output indexes
%               will be truncated and simply return all index values.
%   options:    if specified, the value 'logical' can be used to output a
%               logical index array instead of numerical indexing
%
%   Descriptions of Output Variables:
%   randIndex: an array containing the randomly selected index
%
%   Example(s):
%   >> randIndex = rand_index([530,10],1,120);
%
%   See also:

% Author: Anthony Kendall
% Contact: anthony [dot] kendall [at] gmail [dot] com
% Created: 2008-10-12
% Copyright 2008 Michigan State University.

%Validate the inputs
error(nargchk(3,4,nargin));
assert(isempty(randDim) || randDim <= length(arraySize),'The specified dimension is greater the number of dimensions input');
if nargin == 4
    if strcmpi(options,'logical')
        logicFlag = true;
    else
        error('The specified value of "options" is not allowed, see HELP');
    end
else
    logicFlag = false;
end

%Determine the size of the desired dimension
if isempty(randDim)
    numPoints = prod(arraySize);
else
    numPoints = arraySize(randDim);
end

%Check to see if the number of desired outputs exceeds the total
%number of input points
if (numSelect >= numPoints) || isinf(numSelect)
    randIndex = (1:numPoints);
    return
end

%Use randperm, and select desired number
randSet = randperm(numPoints);
randIndex = randSet(1:numSelect);

%Create a logical array of output
if logicFlag
    tempIndex = zeros(arraySize);
    tempIndex(randIndex) = 1;
    randIndex = logical(tempIndex);
end