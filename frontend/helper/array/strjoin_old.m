function s = strjoin(inputCell,delimiter)
%STRJOIN  Joins strings with the specified delimiter
%   s = strjoin(inputCell,delimiter)
%
%   Descriptions of Input Variables:
%   inputCell:  an input cell arrays of strings (must be
%               vectors) and character arrays of strings (must be vectors)
%   delimiter:  a character array specifying a delimiter to use to join the
%               strings, or a cell array with length up to
%               length(inputCell) - 1 specifying a variable set of
%               delimiters
%
%   Descriptions of Output Variables:
%   s:          the output joined string
%
%   Example(s):
%   >> joinStr = strjoin({{'this','is','just'},'an', ['easy';'test'],
%           {'example';'of'},'functionality'},'&');
%   joinStr =
%   this&is&just&an&easy&test&example&of&functionality
%   See also:

if nargin < 2
    delimiter = ' ';
end

if ~iscell(delimiter)
    delimiter = {delimiter};
end

n = length(inputCell)-1;
if length(delimiter) == 1
    delimiter = repmat(delimiter(1),n,1);
end

%ADK 9/8/2017: MATLAB now has a builtin called strjoin, so for now don't
%include this additional functionality, I wasn't really using it anyway
% 
% sTemp = cell(1,n);
% for m = 1:n
%     if iscell(inputCell{m})
%         sTemp{m} = join_cell(delimiter{m},inputCell{m});
%     elseif ischar(inputCell{m}) && size(inputCell{m},1)>1
%         sTemp{m} = join_cell(delimiter{m},cellstr(inputCell{m}));
%     elseif ischar(inputCell{m}) && size(inputCell{m},1)==1
%         sTemp{m} = inputCell{m};
%     else
%         error('strjoin: Input must be a cell array of strings or character arrays')
%     end
% end

%Finally, join these terms together
s = join_cell(delimiter,inputCell);

end

function s = join_cell(d,terms)
assert(isvector(terms),'strjoin: Input cell arrays must be vectors')
n = length(terms);
if n == 1
    s = terms{1};
else
    sTemp = cell(1,2*n-1);
    sTemp(1:2:end) = terms(:);
    sTemp(2:2:end) = d;
    s = [sTemp{:}];
end
end




        