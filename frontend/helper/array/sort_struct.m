function struct = sort_struct(struct,sortField)
%This function recursively traverses a structured array, sorting
%non-structured end members based on a sortField.  All members must have
%equal lengths and not be structures.

%List all fields
fields = fieldnames(struct);

%Check to see if sortField is present at this level
sortLevel = any(strcmpi(fields,sortField));

if sortLevel
    %Make sure this isn't a coincidence, which can occur when a field name at
    %another level has the same name as sortField, all fields must have
    %same length and not be a structured array
    sortLength = length(struct.(sortField));
    for m = 1:length(fields)
        if isstruct(struct.(fields{m})) || (length(struct.(fields{m}))~=sortLength)
            coincidence = true;
            break
        end
        coincidence = false; 
    end
    
    %If not a coincidence
    if ~coincidence
        %Determine the sort order
        [struct.(sortField),sortOrder] = sort(struct.(sortField));
        
        %Remove the sortField from further consideration
        fields = fields(~strcmpi(fields,sortField));
        
        %Now, sort the remaining fields
        for m = 1:length(fields)
            struct.(fields{m}) = struct.(fields{m})(sortOrder);
        end
    else
        sortLevel = false;
    end
end

%Otherwise, recurse through any structures present at this level
if ~sortLevel
    for m = 1:length(fields)
        if isstruct(struct.(fields{m}))
            struct.(fields{m}) = sort_struct(struct.(fields{m}),sortField);
        end
    end
end
end