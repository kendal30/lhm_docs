'''
-------------------------------------------------------------------------------
Resample grids to coarse
-------------------------------------------------------------------------------
'''
def resample_coarse(inGrid,outGrid,outExtent,outSize,aggMethod,outSnapRaster=''):
    """Resamples grids to a coarser resolution specified by outSize.  inGrid must
    be the full path to the input Grid, while outGrid is just the output grid name
    (it will be stored in the workspace. aggMethod is a string specifying either
    'SUM', 'MEAN', 'MINIMUM', 'MAXIMUM'
    'MEDIAN','MAJORITY', 'MAJORITY_LOGICAL_NODATA', 'FRACTION', and 'CLIP'

    returns a raster object of the output grid.

    Note, this requires the spatial analyst extension to be checked out"""

    # Import required modules
    import os, random, string
    from numpy import floor
    import arcpy

    # Determine if this is a geodatabase, if not will handle differently below
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType
    if workspaceType in [u'FileSystem','FileSystem']:
        extRaster = '.tif'
    else:
        extRaster = ''
        
    # Set a few temp names here for convenience
    randomString = ''.join(random.choices(string.ascii_lowercase+string.digits+string.ascii_uppercase,k=5))
    tempGrid1 = 'tempGrid1_' + randomString + extRaster
    tempStep1 = 'tempStep1_' + randomString + extRaster
    tempStep2 = 'tempStep2_' + randomString + extRaster
    tempOut = 'tempOut_' + randomString + extRaster
    
    # Get environment settings before, will reset these
    priorExtent = arcpy.env.extent
    priorCellSize = arcpy.env.cellSize
    priorSnapRaster = arcpy.env.snapRaster

    # Reset these so this analysis functions correctly
    arcpy.env.extent = ''
    arcpy.env.cellSize = ''
    arcpy.env.snapRaster = ''

    # Clip the grid to the appropriate extent, reduces computational time later
    strEnvelope = '%f %f %f %f'%(outExtent.XMin, outExtent.YMin, outExtent.XMax, outExtent.YMax)
    arcpy.Clip_management(inGrid,strEnvelope,tempStep1)
    rasterTemp1 = arcpy.Raster(tempStep1)

    # Now, calculate the aggregation factor (this makes sure we have the input data in the right coordinate system)
    inSize = arcpy.Describe(tempStep1).MeanCellHeight
    sizeRatio = float(outSize) / float(inSize)
    aggFactor = int(floor(sizeRatio))

    # Then, determine if the grid needs to be rescaled at all
    rescaleFlag = sizeRatio > 1.001

    if aggMethod.upper() in ['SUM','MEAN','MEDIAN','MINIMUM','MAXIMUM']:
        if aggMethod.upper() in ['MEAN']:
            # Float the grid
            rasterTemp2 = arcpy.sa.Float(rasterTemp1)
        else:
            rasterTemp2 = rasterTemp1
        rasterTemp2.save(tempStep2)

        if rescaleFlag:
            # This may still need to be adjusted with apply environment later
            rasterOut = arcpy.sa.Aggregate(rasterTemp2,aggFactor,aggMethod,'EXPAND','DATA')
        else:
            # Copy the clipped and floated grid to output
            rasterOut = rasterTemp2

        # Force it to save to disk to avoid some strange issues
        #arcpy.CopyRaster_management(rasterOut,tempOut)
        rasterOut.save(tempOut)

        # Clean up after this step
        del rasterTemp2
        arcpy.Delete_management(tempStep2)

    elif aggMethod.upper() in ['MAJORITY']:
        # Convert the grid to an integer type
        rasterTemp2 = arcpy.sa.Int(rasterTemp1)
        rasterTemp2.save(tempStep2)

        if rescaleFlag:
            # Resample directly and use the majority method
            arcpy.Resample_management(rasterTemp2,tempGrid1,outSize,aggMethod)

            # Make raster object for later use
            rasterOut = arcpy.Raster(tempGrid1)
        else:
            # Copy the clipped and int'd grid to output
            rasterOut = rasterTemp2

        # Force it to save to disk to avoid some strange issues
        rasterOut.save(tempOut)

        # Clean up after this step
        del rasterTemp2
        arcpy.Delete_management(tempStep2)
        arcpy.Delete_management(tempGrid1)

    elif aggMethod.upper() in ['MAJORITY_LOGICAL_NODATA']:
        # This method allow nodata values to be ignored in calculating the majority
        # logical (0/1) value

        # Convert the grid to an integer type
        rasterTemp2 = arcpy.sa.Int(rasterTemp1)

        if rescaleFlag:
            # Aggregate the grid
            rasterTemp3 = arcpy.sa.Aggregate(rasterTemp2,aggFactor,'MEAN','EXPAND','DATA')
            rasterTemp3.save(tempStep2)
            # Determine if cell value is should be 0 or 1
            rasterOut = arcpy.sa.Con(rasterTemp3 > 0.5, 1, 0)
        else:
            # Copy the clipped and int'd grid to output
            rasterOut = rasterTemp2

        # Force it to save to disk to avoid some strange issues
        rasterOut.save(tempOut)

        # Clean up after this step
        del rasterTemp2

        if rescaleFlag:
            del rasterTemp3
            arcpy.Delete_management(tempStep2)

    elif aggMethod.upper() in ['FRACTION']:
        if rescaleFlag:
            # Perform the aggregate operation
            rasterTemp2 = arcpy.sa.Aggregate(rasterTemp1,aggFactor,'SUM','EXPAND','DATA')

            # Convert this grid to floating point
            rasterTemp3 = arcpy.sa.Float(rasterTemp2)

            # Divide by the square of the aggregation factor
            rasterOut = rasterTemp3 / (aggFactor**2)

            # Force it to save to disk to avoid some strange issues
            rasterOut.save(tempOut)

            # Clean up after this step
            del rasterTemp2, rasterTemp3
        else:
            print('FRACTION method only makes sense if output cellsize is larger than input')

    elif aggMethod.upper() in ['CLIP']:
        rasterOut = rasterTemp1

        # Force it to save to disk to avoid some strange issues
        rasterOut.save(tempOut)

    else:
        print('aggMethod not recognized')

    # Apply environment to get extent and cellsize to match
    arcpy.env.extent = outExtent
    arcpy.env.cellSize = outSize
    arcpy.env.snapRaster = outSnapRaster
    rasterOutApply = arcpy.sa.ApplyEnvironment(rasterOut)
    rasterOutApply.save(outGrid)

    # Return environment settings
    arcpy.env.extent = priorExtent
    arcpy.env.cellSize = priorCellSize
    arcpy.env.snapRaster = priorSnapRaster

    # Clean up
    del rasterTemp1, rasterOut
    arcpy.Delete_management(tempStep1)
    arcpy.Delete_management(tempOut)

    return rasterOutApply

'''
-------------------------------------------------------------------------------
Extract raster values to points
-------------------------------------------------------------------------------
'''
def raster_vals_to_points(feature,raster,newField,interpolate=False,overwrite=False,newFeature=False):
    """Simplifies the process of adding raster values to a point feature class. Specify boolean
    values for interpolate and overwrite. Interpolate passes the value to ExtractValuesToPoints
    while overwrite allows overwriting the field if already present in feature.
    You can specify whether a new feature is created by entering a string as the newFeature
    input (optional, default value is False, so values will be added to the same
    feature class)"""

    tempFeature = 'tempFeature'
    import arcpy

    # Determine if this is a geodatabase, if not will handle differently below
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType

    # Check to see if the field name already exists, stop if so
    listFields = arcpy.ListFields(feature)
    listFields = [field.name for field in listFields]
    if newField in listFields:
        if overwrite:
            arcpy.DeleteField_management(feature, newField)
        else:
            raise RuntimeError('The desired newField "%s" already exists'%newField)

    # Add the raster values to the feature
    arcpy.sa.ExtractValuesToPoints(feature,raster,tempFeature,interpolate_values=interpolate)

    if not newFeature:
        # Delete the original feature
        arcpy.Delete_management(feature)

    # Rename the field
    if workspaceType in [u'FileSystem','FileSystem']:
        tempFeature += '.shp'
        arcpy.AddField_management(tempFeature,newField,'float')
        arcpy.CalculateField_management(tempFeature,newField,'!RASTERVALU!')
        arcpy.DeleteField_management(tempFeature,'RASTERVALU')
    else:
        # Alter field name
        arcpy.AlterField_management(tempFeature,'RASTERVALU',newField,newField)

    if not newFeature:
        # Copy tempFeature to feature
        arcpy.Rename_management(tempFeature,feature)
    else:
        # Make this a new feature
        arcpy.Rename_management(tempFeature,newFeature)

'''
-------------------------------------------------------------------------------
Snap points flowaccumulation feature
-------------------------------------------------------------------------------
'''
def snap_points_flowaccum_feature(inputPtsFeature,outPtsFeature,inFlowaccum,snapDistance,fieldID,threshold = False):
    """This helper function adds to the capacity of arcpy's builtin snappourpoint tool and snaps
    points, then moves the original feature to those snapped points.

    It has an optional feature called threshold, above which the point will not be snapped. If the
    flowaccumulation value, expressed as an area (in square units of the flowaccumulation raster) is above
    the threshold then it will not be snapped. For instance, if the point drains greater than 1 km2, with a 30 meter
    cellsize, it would have a flowaccumulation value greater than 1111. So a threshold value of 1e6 would result
    in points with values already greater than that not being snapped.
    """

    import arcpy

    # Define temporary features
    tempSnapRaster = 'temp_snap_raster'
    tempSnapPts = 'temp_snap_points'
    tempPtsThresh = 'temp_pts_thresh'

    # Determine if this is a geodatabase, if not, add extension to temps
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType
    if workspaceType in [u'FileSystem']:
        tempSnapRaster += '.tif'
        tempSnapPts += '.shp'
        tempPtsThresh += '.shp'

    # Create a copy of the the inputPtsFeature to match current environment
    arcpy.Project_management(inputPtsFeature, outPtsFeature, arcpy.env.outputCoordinateSystem)

    # Delete the POINT_X and POINT_Y fields, if present
    fields = arcpy.ListFields(outPtsFeature)
    for field in fields:
        if field.name in ['POINT_X','POINT_Y']:
            arcpy.DeleteField_management(outPtsFeature,field.name)

    # Run the snap pour points tool
    snapPoints = arcpy.sa.SnapPourPoint(outPtsFeature, inFlowaccum, snapDistance, fieldID)
    snapPoints.save(tempSnapRaster)

    # Convert the snap pour points raster back to points
    arcpy.RasterToPoint_conversion(tempSnapRaster, tempSnapPts)

    # Write X,Y coordinates to tempSnapPoints
    arcpy.AddXY_management(tempSnapPts)

    # Join snapped points to output points
    arcpy.JoinField_management(outPtsFeature,fieldID,tempSnapPts,"grid_code")

    # If the threshold is not false, create a 'threshold' field and populate as appropriate
    if threshold is not False:
        # Get a flowaccumulation value corresponding to the threshold size
        cellSize = arcpy.Describe(inFlowaccum).MeanCellHeight
        threshAccum = int(threshold / (cellSize ** 2))

        # Classify the flowaccumulation raster, 1 if less than threshold, 0 otherwise
        rasterAccum = arcpy.Raster(inFlowaccum)
        rasterClassify = arcpy.sa.Con(rasterAccum < threshAccum , 1, 0)

        # Extract this raster values to the outPtsFeature
        arcpy.Rename_management(outPtsFeature,tempPtsThresh)
        arcpy.sa.ExtractValuesToPoints(tempPtsThresh,rasterClassify,outPtsFeature)

        # Rename the field, don't use AlterField in case outPtsFeature isn't in a geodatabase
        arcpy.AddField_management(outPtsFeature,'thresh','SHORT')
        arcpy.CalculateField_management(outPtsFeature,'thresh','!RASTERVALU!','PYTHON')
        arcpy.DeleteField_management(outPtsFeature,'RASTERVALU')

        # Clean up
        arcpy.Delete_management(tempPtsThresh)
        del rasterAccum, rasterClassify
    else:
        # Add a threshold field where all values are 1 (i.e. all points get moved)
        arcpy.AddField_management(outPtsFeature,'thresh','SHORT')
        arcpy.CalculateField_management(outPtsFeature,'thresh',1,'PYTHON')

    # Read the new coordinates from the snapped pour point field and write them to the previous pour points
    # only do this for pour points that need to be moved according to the threshold
    pourPoints = arcpy.UpdateCursor(outPtsFeature)
    for pourPoint in pourPoints:
        if not pourPoint.isNull("POINT_X") and (pourPoint.thresh == 1):
            newPoint = arcpy.Point()
            # Update the shape field
            newPoint.X = pourPoint.POINT_X
            newPoint.Y = pourPoint.POINT_Y
            # Update the row
            pourPoint.Shape = arcpy.PointGeometry(newPoint)
            pourPoints.updateRow(pourPoint)

    # Clean up this step
    del pourPoints, pourPoint, newPoint, snapPoints

    # Remove unnecessary fields from the output
    arcpy.DeleteField_management(outPtsFeature,"pointid")
    arcpy.DeleteField_management(outPtsFeature,"grid_code")
    arcpy.DeleteField_management(outPtsFeature,"POINT_X")
    arcpy.DeleteField_management(outPtsFeature,"POINT_Y")
    arcpy.DeleteField_management(outPtsFeature,"Id")
    arcpy.DeleteField_management(outPtsFeature,"thresh")

    # Clean up temps
    arcpy.Delete_management(tempSnapRaster)
    arcpy.Delete_management(tempSnapPts)

    # Print message to user
    print('User may want to check snapping of ' + outPtsFeature)

'''
-------------------------------------------------------------------------------
Batch create watersheds feature
-------------------------------------------------------------------------------
'''
def batch_create_watersheds_feature(inPourPoints,outSheds,inFlowdir,fieldID):
    """This helper function creates a new feature layer containing watersheds for all input
    features.
    
    Input fieldID can be of either a numeric or string type
    """

    import arcpy, os
    from tqdm.auto import tqdm

    # Define some temporary names
    layerSelect = 'layer_select'
    tempPourPoints = 'temp_pour_points'
    tempRasterPourPt = 'temp_raster_pour_pt'
    tempRasterWshed = 'temp_raster_wshed'
    tempFeatureWshed = 'temp_feature_wshed'
    tempFeatureDissolve = 'temp_feature_dissolve'

    # Determine if this is a geodatabase, if not, add extension to temps
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType
    if workspaceType in [u'FileSystem']:
        tempRasterPourPt += '.tif'
        tempRasterWshed += '.tif'
        tempFeatureWshed += '.shp'
        tempFeatureDissolve += '.shp'
        tempPourPoints += '.shp'
        # fieldGridcode =

    # Numeric ID field name, will join back to the original field ID
    fieldNumID = 'Numeric_ID'
    
    # Get the flowaccumulation cell size
    flowSize = arcpy.Describe(inFlowdir).MeanCellHeight


    # Make a copy of the input points
    arcpy.CopyFeatures_management(inPourPoints,tempPourPoints)
    
    # Add a new numeric ID field to do the watershed operation
    arcpy.AddField_management(tempPourPoints,fieldNumID,'LONG')
    
    # Calculate the value of the new field
    fieldOID = arcpy.Describe(tempPourPoints).oidFieldName
    arcpy.CalculateField_management(tempPourPoints,fieldNumID,'!%s!'%fieldOID)
    
    # Create a feature layer for selecting
    arcpy.MakeFeatureLayer_management(tempPourPoints,layerSelect)
    
    # Create the search cursor
    rows = arcpy.SearchCursor(tempPourPoints)

    # Loop through the rows, creating watersheds for each
    appendFeatureCreated = False
    totalLoop = int(arcpy.GetCount_management(tempPourPoints)[0])
    # The two lines below are only needed because of the watershed lock issue
    countLoop = 1
    tempRasterPourPtBase = 'temp_raster_pour_pt'
    for row in tqdm(rows,total=totalLoop,desc='Generating Watersheds'):
        # Select only this gauge
        whereClause = fieldNumID + ' = ' + str(row.getValue(fieldNumID))
        arcpy.SelectLayerByAttribute_management(layerSelect,'NEW_SELECTION',whereClause)

        # The pour point file must be a raster
        arcpy.FeatureToRaster_conversion(layerSelect,fieldNumID,tempRasterPourPt,flowSize) # tempRasterPourPt

        # Run the watershed command
        rasterWatershed = arcpy.sa.Watershed(inFlowdir,tempRasterPourPt,'Value') # This doesn't seem to be releasing a lock on the pour pts
        # rasterWatershed.save(tempRasterWshed)

        # Convert to a temporary polygon, dissolve by grid_code, and then append to the overall output
        arcpy.RasterToPolygon_conversion(rasterWatershed,tempFeatureWshed,'NO_SIMPLIFY')
        arcpy.Dissolve_management(tempFeatureWshed,tempFeatureDissolve,'GRIDCODE')

        if appendFeatureCreated:
            arcpy.Append_management(tempFeatureDissolve,outSheds)
        else:
            arcpy.CopyFeatures_management(tempFeatureDissolve,outSheds)
            appendFeatureCreated = True

        # Clean up
        del rasterWatershed
        # arcpy.Delete_management(tempRasterWshed)
        arcpy.Delete_management(tempFeatureWshed)
        arcpy.Delete_management(tempFeatureDissolve)

        # The try: except: is only needed beecause the version 2.2 of ArcGIS Pro has an issue releasing a lock on the watersheds
        try:
            arcpy.Delete_management(tempRasterPourPt)
        except:
            print('Unable to delete %s, manually delete from %s'%(tempRasterPourPt,arcpy.env.workspace))
            countLoop += 1
            tempRasterPourPt = tempRasterPourPtBase + '_%s'%(str(countLoop))
            if workspaceType in [u'FileSystem']:
                tempRasterPourPt = tempRasterPourPt + '.tif'


    # Join the original field ID back to the outSheds
    arcpy.JoinField_management(outSheds,'GRIDCODE',tempPourPoints,fieldNumID,[fieldID])
    arcpy.DeleteField_management(outSheds,'GRIDCODE')
    
    # Clean up the search cursor
    del row, rows
    arcpy.Delete_management(layerSelect)
    arcpy.Delete_management(tempPourPoints)


'''
-------------------------------------------------------------------------------
Iterative spatial outlier removal
-------------------------------------------------------------------------------
'''
def iterative_spatial_outlier_removal(inFeature,outFeature,params):
    """This helper function iteratively removes spatial outliers using point statistics.
    params is a dictionary that looks like this:
    params['neighbSize'] = 1000 # Specify size of neighborhood to examine outliers, in model units
    params['nsigmaThresh'] = 3 # Number of standard deviations above which to exclude points
    params['nsigmaLargeDevThresh'] = 1 # this is the threshold for nsigma under large std conditions
    params['largeDevThresh'] = 10 # in output units (meters), this is the threshold for neighborhoods with a large standard deviation
    params['nIter'] = 3 # Specify the number of iterations
    params['fieldData'] = = fieldWTElev['name']"""

    import arcpy
    # Specify inputs
    baseIterName = ['temp_iter','_outliers_removed']
    fieldStd = {'name':'neighstd','type':'DOUBLE'}
    fieldMean = {'name':'neighmean','type':'DOUBLE'}
    fieldNSigma = {'name':'nsigma','type':'DOUBLE'}
    fieldData = {'name':params['fieldData'],'type':'DOUBLE'}

    tempIterName = inFeature # initialize the variable, will be altered in the loop
    for iter in range(0,params['nIter']):
        # Add the mean and standard deviation values in the neighborhood of each point the the shapefile
        # Calculate the standard deviation and mean value within a neighborhood grid
        rasterStd = arcpy.sa.PointStatistics(tempIterName,fieldData['name'],params['neighbSize'],'RECTANGLE 3 3','STD')
        rasterMean = arcpy.sa.PointStatistics(tempIterName,fieldData['name'],params['neighbSize'],'RECTANGLE 3 3','MEAN')

        # Add these values to the table
        raster_vals_to_points(tempIterName,rasterStd,fieldStd['name'])
        raster_vals_to_points(tempIterName,rasterMean,fieldMean['name'])

        # Add a new field for std deviations from the mean
        arcpy.AddField_management(tempIterName,fieldNSigma['name'],fieldNSigma['type'])

        # Calculate the value of this field
        codeBlock = """def normalize(val,mean,std):
               if std > 0:
                   return abs(val-mean) / std
               else:
                   return 0"""
        expression = 'normalize(!' + fieldData['name'] + '!,!'+ fieldMean['name'] + '!,!' + fieldStd['name'] +'!)'
        arcpy.CalculateField_management(tempIterName,fieldNSigma['name'],expression,'PYTHON',codeBlock)

        # Get the file name of the features to use
        prevName = tempIterName
        tempIterName = baseIterName[0] + str(iter+1) + baseIterName[1]

        # Build where clause
        whereClause = '("' + fieldNSigma['name'] + '" < ' + str(params['nsigmaLargeDevThresh']) + \
            ') OR ("' + fieldStd['name'] + '" < ' + str(params['largeDevThresh']) + ' AND "' + \
            fieldNSigma['name'] + '" < ' + str(params['nsigmaThresh']) + ')'
        # Select records matching whereClause
        arcpy.Select_analysis(prevName,tempIterName,whereClause)

        if iter < (params['nIter'] - 1): # This isn't the last iteration
            # Delete fields from tempIterName
            arcpy.DeleteField_management(tempIterName,[fieldStd['name'],fieldMean['name'],fieldNSigma['name']])

    # Copy the final iteration to a permanent output
    arcpy.Copy_management(tempIterName,outFeature)

    # Now, clean up
    # Delete temporary raster objects
    del rasterStd,rasterMean

    # Identify iteration features to delete
    delTemps = arcpy.ListFeatureClasses(baseIterName[0]+'*','All')
    for delTemp in delTemps:
        arcpy.Delete_management(delTemp)


'''
-------------------------------------------------------------------------------
Tiled flowlength
-------------------------------------------------------------------------------
'''
def worker_flowlength(jobNum, params, pathFlowdir, pathTiles, pathCost):
    import arcpy, os

    # Temporary names
    tempWorkspace = 'temp_workspace_%d'
    tempFlowlength = 'temp_flowlength_%d'
    tempZone = 'temp_zone_%d'
    tempZoneFeature = 'temp_zone_feature_%d'
    tempZoneBuff = 'temp_zone_buffer_%d'
    tempExtract = 'temp_extract_%d'

    # Create a local workspace for this worker
    baseWorkspace = params['env']['workspace']
    if baseWorkspace[-3:].lower() == 'gdb':
        baseDir = os.path.split(baseWorkspace)[0]
    
    # Get output names
    workerDatabase = os.path.join(baseDir,tempWorkspace%jobNum+'.gdb')
    outName = tempFlowlength%jobNum
    outPath = os.path.join(workerDatabase,outName)

    # Check to see if database already exists, if does, assume this is restarted (really only for debugging)
    if not arcpy.Exists(workerDatabase):
        arcpy.CreateFileGDB_management(baseDir,tempWorkspace%jobNum)

        # Set the environment
        arcpy.env.workspace = workerDatabase
        arcpy.env.compression = params['env']['compression']
        arcpy.env.pyramid = params['env']['pyramid']
        arcpy.env.overwriteOutput = params['env']['overwriteOutput']

        # Pull in the raster objects
        rasterFlowdir = arcpy.Raster(pathFlowdir)
        rasterTiles = arcpy.Raster(pathTiles)
        if pathCost is not None:
            rasterCost = arcpy.Raster(pathCost)
        else:
            rasterCost = pathCost #pass None to the FlowLength algorithm

        # Create a raster for just this zone
        rasterThisZone = arcpy.sa.Con(rasterTiles==int(jobNum),rasterTiles)
        rasterThisZone.save(tempZone%jobNum)

        # Convert this to a feature
        arcpy.RasterToPolygon_conversion(tempZone%jobNum,tempZoneFeature%jobNum)

        # Buffer this feature
        arcpy.Buffer_analysis(tempZoneFeature%jobNum,tempZoneBuff%jobNum,params['tileBuffDist'])

        # Extract out the flow direction
        rasterExtract = arcpy.sa.ExtractByMask(rasterFlowdir,tempZoneBuff%jobNum)
        rasterExtract.save(tempExtract%jobNum)

        # Run flowlength on this raster and save to the base workspace, use copyraster to enforce compression
        rasterFlowlength = arcpy.sa.FlowLength(rasterExtract,'DOWNSTREAM',rasterCost)
        arcpy.CopyRaster_management(rasterFlowlength,outName)

        # Clean up, delete everything except the final raster
        del rasterFlowlength, rasterExtract, rasterThisZone
        delTemps = [tempZone, tempZoneFeature, tempZoneBuff, tempExtract]
        [arcpy.Delete_management(thisTemp%jobNum) for thisTemp in delTemps]
        arcpy.env.workspace = baseWorkspace

    return (outPath,workerDatabase)

def tile_flowlength(arcpy, params, rasterFlowdir, rasterTiles, rasterCost=None):
    import random, string, concurrent, pandas, numpy

    tempFlowlength = 'temp_flowlength'

    # Set default parameters
    paramDefaults = {'tileBuffDist':3000,'maxProceses':3}
    for thisParam in paramDefaults.keys():
        if thisParam not in params.keys():
            params[thisParam] = paramDefaults[thisParam]

    # Add some environment parameters for the child processes to set up properly
    params['env'] = dict()
    params['env']['workspace'] = arcpy.env.workspace
    params['env']['compression'] = arcpy.env.compression
    params['env']['pyramid'] = arcpy.env.pyramid
    params['env']['overwriteOutput'] = arcpy.env.overwriteOutput

    # Make tempFlowlength a random name on disk
    randomString = ''.join(random.choices(string.ascii_lowercase+string.digits+string.ascii_uppercase,k=5))
    tempFlowlengthRand = tempFlowlength +'_' + randomString

    # Get the values of the tile zones
    dfTiles = pandas.DataFrame(arcpy.da.TableToNumPyArray(rasterTiles.catalogPath,['Value','Count']))
    tiles = dfTiles['Value'].unique()

    # Set up the simultaneous processing executor
    executor = concurrent.futures.ProcessPoolExecutor(max_workers = numpy.minimum(len(tiles),params['maxProcesses']))

    # Set up arguments to pass
    pathFlowdir = rasterFlowdir.catalogPath
    pathTiles = rasterTiles.catalogPath
    if rasterCost is not None:
        pathCost = rasterCost.catalogPath
    else:
        pathCost = rasterCost

    threads = [executor.submit(worker_flowlength, jobNum, params, pathFlowdir, pathTiles, pathCost) for jobNum in tiles]
    wait = concurrent.futures.wait(threads)

    # Run it and return the raster names
    try: 
        listMosaic = [thread.result()[0] for thread in wait.done]
        listWorkerGDBs = [thread.result()[1] for thread in wait.done]
    except Exception as e:
        # Fail gracefully
        executor.shutdown()

        # Print the exception and raise an error
        print(e)
        raise RuntimeError('Tiled flowlength failed')

    # Close the thread pool executor
    executor.shutdown()

    # Copy the first raster
    arcpy.CopyRaster_management(listMosaic.pop(0),tempFlowlengthRand)

    if len(listMosaic) > 0:
        # Mosaic the list of results
        arcpy.Mosaic_management(listMosaic,tempFlowlengthRand,mosaic_type='MAXIMUM')

    # Delete the worker GDBs
    [arcpy.Delete_management(thisGDB) for thisGDB in listWorkerGDBs]

    # Enforce the environment settings
    rasterFlowtime = arcpy.sa.ApplyEnvironment(tempFlowlengthRand)

    return rasterFlowtime