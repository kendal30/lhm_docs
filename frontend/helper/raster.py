"""
Helper functions for the MODFLOW capabilities in the LHM Frontend.

Created on Thu Feb 14 06:47:08 2019

@author: kendal30
"""

def create_modflow_raster(sources,numLay,outPathFormat,nanMask=None,startRaster=False,noDataToValue=None,outputFeature=None):
    """
    This is a special create_modflow_raster function because ibound inputs can have multiple
    features per layer

    Instead of having a NoData background value, the output is default to 0.

    If using a feature 'method' type, you can also specify an input startRaster. This is primarily
    used to create an IBOUND array from the model grid, but could conceivably have other uses as well.
    startRaster should be a string or an arcpy Raster object. Overprinted feature values will only be used
    in areas of the startRaster that are equal to 1. This will require, essentially, that features lie
    within the specified starting model domain.

    numLay: is the layer number or range of layers (i.e. range(1,5)) to write, from the top down
    This function assumes the scratch/workspace directory (if set) is a GDB. The layer numbering
    are 1-based here, not Python 0-based.

    The outPathFormat string specifies either full name or the output name but with a %d for the layer number

    If nanMask is specified, it will require that there are no Nan values within the
    true values in that array. nanMask is a 0/1 grid

    You can optionally specify noDataToValue which will set the noData values in the raster to 
    the provided value

    You can optionally specify a name to return the feature used to create the grid. Default is with outputFeature=None, 
    but if you set outputFeature = string, it will save the properly-created feature for the last layer specified in numLay.
    """

    import arcpy, numpy
    import gis
    resample_coarse = gis.resample_coarse

    # Make sure that numLay is iterable
    if isinstance(numLay,int):
        numLay = [numLay]

    # Set the workspace if it's not already set
    resetWorkspace = False
    if arcpy.env.workspace is None:
        arcpy.env.workspace = arcpy.env.scratchWorkspace
        resetWorkspace = True

    # Determine if this is a geodatabase, if not will handle differently below
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType
    if workspaceType in [u'FileSystem','FileSystem']:
        extRaster = '.tif'
    else:
        extRaster = ''
    
    # Specify temporary names
    tempFeatureLayer = 'temp_feature_layer'

    tempGrid = 'temp_grid' + extRaster
    tempGridConv = 'temp_grid_convert' + extRaster
    tempGridAdd = 'temp_grid_add' + extRaster
    tempGridMerge = 'temp_grid_merge' + extRaster
    tempGridCleaning = 'temp_grid_cleaning' + extRaster

    # Handle if input is just a string. Possibilities are str or num
    if isinstance(sources['spatial'],(str,int,float)):
        sources['spatial'] = [sources['spatial']]
    
    # Test for how layering is specified
    if (sources['layer'] is None) or (sources['layer']==''):
        if len(sources['spatial'])>1:
            layerMethod = 'list'
        elif len(sources['value'])>1:
            layerMethod = 'list'
        else:
            layerMethod = 'all'
    elif isinstance(sources['layer'],str):
        layerMethod = 'field'
    elif isinstance(sources['layer'],list):
        layerMethod = 'list'
    elif isinstance(sources['value'],list):
        layerMethod = 'list'
    elif sources['layer']==-1:
        layerMethod = 'all'
    else:
        raise ValueError('Layering specification: "layer" must be None, empty string, str, list, or -1')

    # Check for a multiplier field, if not present, specify as 1
    if 'mult' not in sources.keys():
        sources['mult'] = 1
    elif sources['mult'] is None:
        sources['mult'] = 1
    elif sources['mult'] == '':
        sources['mult'] = 1

    # Loop through the layers
    indLay = 0
    outNames = [] 
    for thisLay in numLay:
        # Build the output name
        if outPathFormat.find('%')>0:
            thisOutPath = outPathFormat%(thisLay)
        else:
            thisOutPath = outPathFormat

        # Test for how layering is specified
        if sources['method'] == 'feature':
            if layerMethod == 'field':
                # Decide if we are starting with a blank raster or not
                if startRaster:
                    rasterFeature = arcpy.Raster(startRaster)
                    rasterFeature.save(tempGrid)
                    del rasterFeature
                    limitBdry = True
                    blankRaster = False
                else:
                    blankRaster = True
                    limitBdry = False
                
                totNumFeatures = 0

                for (ind,thisFeature) in enumerate(sources['spatial']):
                    # Select features that correspond to this layer, or are flagged for all layers (-1)
                    arcpy.MakeFeatureLayer_management(thisFeature,tempFeatureLayer)
                    arcpy.SelectLayerByAttribute_management(tempFeatureLayer,'NEW_SELECTION','"%s" = %d'%(sources['layer'],thisLay))
                    arcpy.SelectLayerByAttribute_management(tempFeatureLayer,'ADD_TO_SELECTION','"%s" = -1'%sources['layer'])
                    numFeatures = arcpy.GetCount_management(tempFeatureLayer)

                    # Set the nanVal based on the field data type
                    if ind==0:
                        # Get the data type
                        thisFields = arcpy.ListFields(thisFeature)
                        allFields = []
                        for thisField in thisFields:
                            allFields.append(thisField.name)
                            if thisField.name==sources['value']:
                                if thisField.type in ('SmallInteger','Integer'):
                                    nanVal = -9999
                                    blankType = 'INTEGER'
                                elif thisField.type in ('Single','Double'):
                                    nanVal = -9999.0
                                    blankType = 'FLOAT'
                                else:
                                    raise ValueError('Field {} must be of an integer or float/double types'.format(sources['value']))
                        if sources['value'] not in allFields:
                            raise ValueError('Field {} not found in {}'.format(sources['value'],thisFeature))

                    if int(numFeatures[0]) > 0:
                        # Keep track of total number of features
                        totNumFeatures += int(numFeatures[0])
                        
                        # Make sure to create rasterFeature if it hasn't already been done
                        if blankRaster:
                            # Create a blank array of nanVal
                            rasterFeature = arcpy.sa.CreateConstantRaster(nanVal,blankType)
                            rasterFeature.save(tempGrid)
                            del rasterFeature
                            blankRaster = False

                        # Convert features to raster
                        arcpy.FeatureToRaster_conversion(tempFeatureLayer,sources['value'],tempGridAdd,arcpy.env.cellSize)

                        # Now merge them
                        rasterFeature = arcpy.Raster(tempGrid)
                        rasterFeatureAdd = arcpy.Raster(tempGridAdd)
                        rasterFeatureMergeTemp = arcpy.sa.Con(arcpy.sa.IsNull(rasterFeatureAdd),rasterFeature,rasterFeatureAdd)
                        if limitBdry:
                            rasterFeatureMerge = arcpy.sa.Con(rasterFeature!=0,rasterFeatureMergeTemp,0)
                        else:
                            rasterFeatureMerge = rasterFeatureMergeTemp
                        rasterFeatureMerge.save(tempGridMerge)
                        
                        # Clean up
                        del rasterFeature, rasterFeatureMerge, rasterFeatureMergeTemp, rasterFeatureAdd

                        # Delete the original rasterFeature, rename the merged
                        arcpy.Delete_management(tempGrid)
                        arcpy.Delete_management(tempGridAdd)
                        arcpy.Rename_management(tempGridMerge,tempGrid)

                    # Clean up
                    if outputFeature is not None:
                        arcpy.CopyFeatures_management(tempFeatureLayer,outputFeature)
                    arcpy.Delete_management(tempFeatureLayer)
                
                if totNumFeatures==0:
                    raise ValueError('There are no features in the specified layers for %s'%tempGrid) 

                # Set nanVal to NoData
                if arcpy.Exists(tempGridCleaning):
                    arcpy.Delete_management(tempGridCleaning)
                arcpy.Rename_management(tempGrid,tempGridCleaning)
                rasterClean = arcpy.sa.Con(arcpy.Raster(tempGridCleaning)!=nanVal,tempGridCleaning)
                rasterClean.save(tempGrid)

                # Clean up
                arcpy.Delete_management(tempGridCleaning)

            elif layerMethod in ('list','all'):
                # Update the layer index, otherwise use previous
                if layerMethod == 'list': #indLay is 0 for 'all'
                    if thisLay <= len(sources['spatial']):
                        indLay = thisLay - 1 #layer numbers are 1-based

                thisSpatial = sources['spatial'][indLay]
                if isinstance(sources['value'],list) and len(sources['value'])>1:
                    thisField = sources['value'][indLay]
                elif isinstance(sources['layer'],list):
                    if thisLay in sources['layer']:
                        thisField = sources['value']
                    else:
                        thisField = None
                        # Create an empty raster for this layer
                        rasterConstant = arcpy.sa.CreateConstantRaster(1,'FLOAT')
                        rasterNull = arcpy.sa.SetNull(rasterConstant==1,0)
                        rasterNull.save(tempGrid)

                        # Clean up
                        del rasterConstant, rasterNull
                else:
                    thisField = sources['value']
                         
                # Create the temporary feature
                if thisField is not None:
                    arcpy.FeatureToRaster_conversion(thisSpatial,thisField,tempGrid,arcpy.env.cellSize)

        elif sources['method'] == 'constant':
            # Update the layer index, otherwise create blank raster
            if layerMethod == 'list': #indLay is 0 for 'all'
                if thisLay <= len(sources['value']):
                    indLay = thisLay - 1 #layer numbers are 1-based   
                    thisValue = sources['value'][indLay]
                else:
                    thisValue = None
            else:
                thisValue = sources['value']

            if thisValue is not None:
                # Create a constant raster for this layer
                rasterConstant = arcpy.sa.CreateConstantRaster(thisValue,'FLOAT')
            else:
                # Create an empty raster for this layer
                rasterTemp = arcpy.sa.CreateConstantRaster(1,'FLOAT')
                rasterConstant = arcpy.sa.SetNull(rasterTemp==1,0)

                # Clean up
                del rasterTemp

            # Save as tempGrid
            rasterConstant.save(tempGrid)

            # Clean up
            del rasterConstant

        elif sources['method'] == 'grid':
            # Update the layer index, otherwise use previous
            if layerMethod == 'list': #indLay is 0 for 'all'
                if thisLay <= len(sources['spatial']):
                    indLay = thisLay - 1 #layer numbers are 1-based

            # Get the grid for this layer
            thisSpatial = sources['spatial'][indLay]

            # Get a raster object
            rasterInput = arcpy.Raster(thisSpatial)

            # Determine if the input grid is finer than our resolution, if so, resample
            if rasterInput.meanCellHeight < float(arcpy.env.cellSize):
                resample_coarse(rasterInput,tempGrid,arcpy.env.extent,arcpy.env.cellSize,'MEAN',arcpy.env.snapRaster)

            else:
                arcpy.Copy_management(thisSpatial,tempGrid)

            # Clean up
            del rasterInput

        else:
            raise ValueError('"method" must be "feature", "grid", or "constant"')

        
        # If this is a double precision grid, convert to single precision
        rasterGrid = arcpy.Raster(tempGrid)
        if rasterGrid.pixelType == 'F64':
            arcpy.CopyRaster_management(rasterGrid,tempGridConv,pixel_type='32_BIT_FLOAT')
            del rasterGrid
            arcpy.Delete_management(tempGrid)
            arcpy.Rename_management(tempGridConv,tempGrid)
        else:
            del rasterGrid

        # Apply our environment settings to this grid
        rasterApply = arcpy.sa.ApplyEnvironment(tempGrid)
        
        # Set the noData value if specified
        if noDataToValue is not None:
            rasterNoData = arcpy.sa.Con(arcpy.sa.IsNull(rasterApply),noDataToValue,rasterApply)
        else:
            rasterNoData = rasterApply
            
        # Apply the multiplier value if specified
        if sources['mult'] != 1:
            rasterMult = rasterNoData * sources['mult']
        else:
            rasterMult = rasterNoData

        # Use CopyRaster_management to enforce compression and other environment settings
        arcpy.CopyRaster_management(rasterMult,thisOutPath)
        
        # Clean up
        del rasterApply, rasterNoData, rasterMult
        arcpy.Delete_management(tempGrid)

        # Add to the output list
        outNames.append(thisOutPath)

    # Reset the workspace if need be
    if resetWorkspace:
        arcpy.env.workspace = None

    return outNames

def create_surface_raster(sources,outPath,noDataToValue=None):
    """
    This function creates rasters according to the LHM frontend specification.
    
    You can optionally specify noDataToValue=None which will set the noData values in the raster to 
    the provided value
    """

    import arcpy, numpy
    import gis
    resample_coarse = gis.resample_coarse
    #nanVal = -999 #This will be used internally

    # Set the workspace if it's not already set
    resetWorkspace = False
    if arcpy.env.workspace is None:
        arcpy.env.workspace = arcpy.env.scratchWorkspace
        resetWorkspace = True

    # Determine if this is a geodatabase, if not will handle differently below
    workspaceType = arcpy.Describe(arcpy.env.workspace).workspaceType
    if workspaceType in [u'FileSystem','FileSystem']:
        extRaster = '.tif'
    else:
        extRaster = ''
        
    # Temporary grid
    tempFeature = 'temp_feature'
    tempGrid = 'temp_grid' + extRaster
    tempGridConv = 'temp_grid_convert' + extRaster
    tempGridAdd = 'temp_grid_add' + extRaster
    tempGridMerge = 'temp_grid_merge' + extRaster
    tempGridVal = 'temp_grid_val' + extRaster

    # Set the workspace if it's not already set
    arcpy.env.compression = 'LZ77'
    arcpy.env.overwriteOutput = True
    resetWorkspace = False
    if arcpy.env.workspace is None:
        arcpy.env.workspace = arcpy.env.scratchWorkspace
        resetWorkspace = True

    # Check for a multiplier field, if not present, specify as 1
    if 'mult' not in sources.keys():
        sources['mult'] = 1
    elif sources['mult'] is None:
        sources['mult'] = 1
    elif sources['mult'] == '':
        sources['mult'] = 1

    # Now process the layer into a raster
    if sources['method'] == 'feature':
        # Handle if input is just a string
        if type(sources['spatial']) is str:
            sources['spatial'] = [sources['spatial']]

        # Set this here
        firstFeature = True
        for thisFeature in sources['spatial']:
            # Handle if input fieldVal is empty
            if (sources['value'] == '') or (sources['value'] is None):
                sources['value'] = 1 # Next,
            if isinstance(sources['value'],(int)):
                fieldType = 'short'
            elif isinstance(sources['value'],(float)):
                fieldType = 'float'
            # Create a new field called "grid" with value = fieldVal
            if isinstance(sources['value'],(int,float)):
                if arcpy.Exists(tempFeature):
                    arcpy.Delete_management(tempFeature)
                arcpy.CopyFeatures_management(thisFeature,tempFeature)
                fieldList = [f.name for f in arcpy.ListFields(tempFeature)]
                if 'grid' not in fieldList:
                    arcpy.AddField_management(tempFeature,'grid',fieldType)
                    arcpy.CalculateField_management(tempFeature,'grid',sources['value'],'python')
                thisFeature = tempFeature
                fieldVal = 'grid'
                deleteTemp = True
            else:
                fieldVal = sources['value']
                deleteTemp = False

            # Multiple features can be specified
            if firstFeature:
                arcpy.FeatureToRaster_conversion(thisFeature,fieldVal,tempGrid,arcpy.env.cellSize)

                #arrayFeature = read_raster(tempGrid,noDataToValue=nanVal)
                firstFeature = False
            else:
                arcpy.FeatureToRaster_conversion(thisFeature,fieldVal,tempGridAdd,arcpy.env.cellSize)
                #arrayTempFeature = read_raster(tempGrid,noDataToValue=nanVal)

                # Now merge them
                rasterFeature = arcpy.Raster(tempGrid)
                rasterFeatureAdd = arcpy.Raster(tempGridAdd)
                rasterFeatureMerge = arcpy.sa.Con(arcpy.sa.IsNull(rasterFeatureAdd),rasterFeature,rasterFeatureAdd)
                rasterFeatureMerge.save(tempGridMerge)

                # Delete the original rasterFeature, rename the merged
                del rasterFeature, rasterFeatureMerge, rasterFeatureAdd
                arcpy.Delete_management(tempGrid)
                arcpy.Delete_management(tempGridAdd)
                arcpy.Rename_management(tempGridMerge,tempGrid)

            # Clean up, features loop
            if deleteTemp:
                arcpy.Delete_management(tempFeature)

    elif sources['method'] == 'constant':
        # Create a constant raster
        rasterConstant = arcpy.sa.CreateConstantRaster(sources['value'],'FLOAT')
        rasterConstant.save(tempGrid)

        # Clean up
        del rasterConstant

    elif sources['method'] == 'grid':
      
        rasterInput = arcpy.Raster(sources['spatial'])
        # Determine if the input grid is finer than our resolution, if so, resample
        if rasterInput.meanCellHeight < float(arcpy.env.cellSize):
            resample_coarse(rasterInput,tempGrid,arcpy.env.extent,arcpy.env.cellSize,'MEAN',arcpy.env.snapRaster)
        else:
            arcpy.Copy_management(rasterInput,tempGrid)
            
        if (sources['value'] != '') or (sources['value'] is not None):
            rasterGrid = arcpy.Raster(tempGrid)
            rasterGridVal = arcpy.sa.Con(rasterGrid > 0, sources['value'], rasterGrid)
            rasterGridVal.save(tempGridVal)

            # Clean up
            del rasterGrid, rasterGridVal
            arcpy.Delete_management(tempGrid)
            arcpy.Rename_management(tempGridVal,tempGrid)

        # Clean up
        del rasterInput
    else:
        raise ValueError('"method" must be "feature", "grid", or "constant"')

    # If this is a double precision grid, convert to single precision
    rasterGrid = arcpy.Raster(tempGrid)
    if rasterGrid.pixelType == 'F64':
        arcpy.CopyRaster_management(rasterGrid,tempGridConv,pixel_type='32_BIT_FLOAT')
        del rasterGrid
        arcpy.Delete_management(tempGrid)
        arcpy.Rename_management(tempGridConv,tempGrid)
    else:
        del rasterGrid

    # Apply our environment settings to this grid
    rasterApply = arcpy.sa.ApplyEnvironment(tempGrid)

    # Set the noData value if specified
    if noDataToValue is not None:
        rasterNoData = arcpy.sa.Con(arcpy.sa.IsNull(rasterApply),noDataToValue,rasterApply)
    else:
        rasterNoData = rasterApply
                
    # Apply the multiplier value if specified
    if sources['mult'] != 1:
        rasterMult = rasterNoData * sources['mult']
    else:
        rasterMult = rasterNoData

    # Use CopyRaster_management to enforce compression and other environment settings
    arcpy.CopyRaster_management(rasterMult,outPath)

    # Clean up
    del rasterApply, rasterNoData, rasterMult
    arcpy.Delete_management(tempGrid)

    #Reset the workspace if need be
    if resetWorkspace:
        arcpy.env.workspace = None

    return outPath

def read_raster(pathRaster, noDataToValue=None, useDask=False):
    """Reads in a 2D or 3D raster of the specified path. May also specify a value
    for noData to be set"""
    import arcpy, numpy
    if useDask:
        import dask.array as da

    # First, check to see if the raster is an integer type, if so, cannot assign a numpy.NaN value to it
    if isinstance(pathRaster,str):
        rasterRaster = arcpy.Raster(pathRaster)
    else:
        rasterRaster = pathRaster

    # Test to see if this is an integer grid
    testInt = (rasterRaster.pixelType[0] != 'F')
    numBits = int(rasterRaster.pixelType[1:])

    # Need to set a default value for noDataToValue
    if noDataToValue is None:
        testForceFloat = False
        if testInt:
            noDataToValue = numpy.iinfo('int'+rasterRaster.pixelType[1:]).max
        else:
            noDataToValue = numpy.NaN
    else:
        # If numpy.NaN was requested, force to float
        testForceFloat = numpy.isnan(noDataToValue) & testInt

        # Will have to read as integer first
        if testForceFloat:
            noDataToValue = numpy.iinfo('int'+rasterRaster.pixelType[1:]).max

    # See if the array will be greater than 2 GB, if so, need to read in chunks
    memSize = numBits/8 * rasterRaster.bandCount * rasterRaster.width * rasterRaster.height

    if memSize > 2e9:
        # Get the number of slices, keep them down to like 100 mb each
        numSlice = int(numpy.ceil(memSize / 2e9)) * 20

        # For clarity
        cellsize = rasterRaster.meanCellHeight
        columns = rasterRaster.width
        rows = rasterRaster.height

        # Get the number of columns/slice
        columnsSlice = numpy.ceil(columns/numSlice)

        # Get lower lefts of the slices
        listLowerLeft = [arcpy.Point(rasterRaster.extent.XMin + n*columnsSlice*float(cellsize),\
                               rasterRaster.extent.YMin) for n in range(numSlice)]

        # Now, create a list of smaller arrays
        arraysRead = []
        for (m,lowerLeft) in enumerate(listLowerLeft):
            if useDask:
                arraysRead.append(da.from_array(arcpy.RasterToNumPyArray(pathRaster,\
                                  lowerLeft,columnsSlice,rows,nodata_to_value=noDataToValue)))
            else:
                arraysRead.append(arcpy.RasterToNumPyArray(pathRaster,\
                                  lowerLeft,columnsSlice,rows,nodata_to_value=noDataToValue))

        if useDask:
            # Turn into a single large dask array
            arrayRaster = da.concatenate(arraysRead,axis=1)
        else:
            arrayRaster = numpy.concatenate(arraysRead,axis=1)

    else:
        # Read in the array
        arrayRaster = arcpy.RasterToNumPyArray(pathRaster,nodata_to_value=noDataToValue)

    # Process out the outcome if we ultimately want numpy.NaN but this was an integer array to begin with
    if testForceFloat:
        arrayRaster = arrayRaster.astype(numpy.float)

        # Get the current maximum value
        arrayRaster[arrayRaster==noDataToValue] = numpy.NaN

    del rasterRaster

    return arrayRaster


def write_raster(outArray,outPath,templateRaster,nanVal=None):
    """Writes a raster to disk, using a template raster as a reference."""
    import arcpy, numpy, os

    # Temporary filename
    tempMosaic = 'tempMosaic'

    # See if the templateRaster is a Raster object or a string
    delRaster = False
    if isinstance(templateRaster,str):
        delRaster = True
        templateRaster = arcpy.Raster(templateRaster)

    # Test to see if this is an integer grid
    testInt = numpy.issubdtype(outArray.dtype, numpy.integer)

    # Need to set a default value for noDataToValue
    if nanVal is not None:
        if testInt:
            nanVal = numpy.iinfo(outArray.dtype).max
        else:
            nanVal = numpy.NaN

    # Make sure that we handle the coordinate system correctly -- and set compression
    priorCoord = arcpy.env.outputCoordinateSystem
    priorCompression = arcpy.env.compression

    arcpy.env.outputCoordinateSystem = templateRaster
    arcpy.env.compression = 'LZ77'

    # See if the array will be greater than 2 GB, if so, need to write out chunks
    outShape = [float(dim) for dim in outArray.shape]
    memSize = outArray.dtype.itemsize * numpy.prod(outShape)

    # For clarity
    cellsize = templateRaster.meanCellWidth

    delMosaic = False
    if memSize > 2e9:
        # Get the number of slices, keep them down to like 500 mb each
        numSlice = int(numpy.ceil(memSize / 2e9)) * 4

        # Get the number of columns/slice
        columns = outShape[1]
        columnsSlice = numpy.ceil(columns/numSlice)

        # Get lower lefts of the slices
        listLowerLeft = [arcpy.Point(templateRaster.extent.XMin + n*columnsSlice*float(cellsize),\
                               templateRaster.extent.YMin) for n in range(numSlice)]

        # Now loop through and make smaller rasters
        mosaicList = []
        (outWorkspace,outName) = os.path.split(outPath)
        (baseTemp,outExt) = os.path.splitext(outName)
        tempMosaic = os.path.join(outWorkspace,tempMosaic+outExt)
        for (m,lowerLeft) in enumerate(listLowerLeft):
            # Write out a slice
            colStart = int(m*columnsSlice)
            colEnd = int(numpy.minimum((m+1)*columnsSlice,outShape[1]))
            outSlice = outArray[:,colStart:colEnd]
            rasterSlice = arcpy.NumPyArrayToRaster(outSlice,lowerLeft,\
                          x_cell_size=cellsize,value_to_nodata=nanVal)

            # Save a temp file
            fileTemp = os.path.join(outWorkspace,baseTemp+'_temp_%d'%m+outExt)
            rasterSlice.save(fileTemp)
            mosaicList.append(fileTemp)
            del rasterSlice

        # Mosaic the rasters
        arcpy.Mosaic_management(';'.join(mosaicList[1:]),mosaicList[0])
        if arcpy.Exists(tempMosaic):
            arcpy.Delete_management(tempMosaic)
        arcpy.Rename_management(mosaicList[0],tempMosaic)
        rasterOut = arcpy.Raster(tempMosaic)
        delMosaic = True

        # Clean up the temporary files
        [arcpy.Delete_management(thisTemp) for thisTemp in mosaicList]

    else:
        # Write out the raster, matching the template raster
        rasterOut = arcpy.NumPyArrayToRaster(outArray,\
                 templateRaster.extent.lowerLeft, x_cell_size=cellsize ,\
                 value_to_nodata=nanVal)

    # Finalize
    rasterOut = arcpy.sa.ApplyEnvironment(rasterOut)
    arcpy.CopyRaster_management(rasterOut,outPath) #to assure compression

    # Clean up
    arcpy.Delete_management(rasterOut)
    del rasterOut
    if delRaster:
        del templateRaster
    if delMosaic:
        arcpy.Delete_management(tempMosaic)

    # Restore environment settings
    arcpy.env.outputCoordinateSystem = priorCoord
    arcpy.env.compression = priorCompression

def get_raster_header(pathRaster):
    """This function returns key information as a dict object, including:
    numRow, numCol, xllCorner, yllCorner, yulCorner, cellSize, numLay"""

    import arcpy

    # Get the raster object
    rasterObject = arcpy.Raster(pathRaster)

    # Create the output dictionary and assign the properties
    rasterInfo = dict()
    rasterInfo['xllCorner'] = rasterObject.extent.XMin
    rasterInfo['yllCorner'] = rasterObject.extent.YMin
    rasterInfo['yulCorner'] = rasterObject.extent.YMax
    rasterInfo['cellSize'] = rasterObject.meanCellHeight
    rasterInfo['numRow'] = rasterObject.height
    rasterInfo['numCol'] = rasterObject.width
    rasterInfo['numLay'] = rasterObject.bandCount
    rasterInfo['EPSG'] = arcpy.Describe(rasterObject).spatialReference.factoryCode

    return rasterInfo

def collapse_single_layer_rasters(inDir, inExt='.tif', deleteOrig=True, layPattern='_lay', outDir=None, buildAttrib=True):
    """ This function collapses all of the single-band rasters corresponding to layer
    inputs in a directory into multi-band rasters.
    
    The layPattern has to be immediately followed by a number."""

    import glob, os, re, numpy, arcpy

    # Set a temporary output name
    tempOut = 'temp_out'

    # Specify an intermediate layerComp pattern
    layPatternComp = layPattern+'Comp'

    # Compute some variables for convenience
    matchPattern = '*%s[0-9]*%s'%(layPattern,inExt)
    if outDir is None:
        outDir = inDir

    # Set some environment settings
    arcpy.env.compression = 'LZ77'
    arcpy.env.pyramid = 'NONE'
    arcpy.env.overwriteOutput = True

    ## Determine which rasters we will process
    # List all the eligible layers in the directory
    listRasters = glob.glob(os.path.join(inDir,matchPattern))

    # Strip the directories
    listRasterNames = [os.path.split(thisRaster)[1] for thisRaster in listRasters]

    # Strip the extensions
    listRasterNames = [os.path.splitext(thisRaster)[0] for thisRaster in listRasterNames]

    # Strip out the numbers, but leave in the layer match pattern so this function cannot try to composite an
    # already multi-band raster
    listRasterNames = [re.sub(r'%s\d+'%layPattern, layPatternComp, thisRaster) for thisRaster in listRasterNames]

    # Finally, get the unique Names
    uniqueNames = numpy.unique(listRasterNames)
    #print(uniqueNames)

    # Now, go through and create our composites
    arrayRasters = numpy.string_(listRasters)
    for thisName in uniqueNames:
        # First, identify the matching names
        indRasters = numpy.char.find(listRasters,thisName.replace(layPatternComp,''))>0
        thisRasters = arrayRasters[indRasters]

        # Sort this array
        thisRasters = numpy.sort(thisRasters).tolist()

        if len(thisRasters)>0:
            # Optionally, add attribute tables
            if buildAttrib:
                for thisRaster in thisRasters:
                    try:
                        arcpy.BuildRasterAttributeTable_management(thisRaster)
                    except:
                        pass # only integer rasters work

            # Now, feed this into the arcpy composite function
            outPath = os.path.join(outDir,thisName.replace(layPatternComp,'')+inExt)
            outTemp = os.path.join(outDir,tempOut+inExt)
            try:
                arcpy.CompositeBands_management(thisRasters,outTemp)
            except:
                pass # sometimes this function just seems to fail

            # Enforce the environment settings
            rasterTemp = arcpy.sa.ApplyEnvironment(outTemp)
            rasterTemp.save(outPath)

            # Clean up
            del rasterTemp
            arcpy.Delete_management(outTemp)

            # Optionally, delete the input rasters
            if deleteOrig:
                [arcpy.Delete_management(thisRaster) for thisRaster in thisRasters]

            
