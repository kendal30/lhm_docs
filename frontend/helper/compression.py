# -*- coding: utf-8 -*-
"""
This function compresses the images in a geodatabase using lossless format (LZ77).

Just run this file, then call the function compress_geodatabse_rasters(<complete path to geodatabase>)

Don't forget to make the path have proper '\\' or '/' for windows, NOT '\'

Optionally, you can tell it to print the name of each raster after it has been compressed by specifying
optPrint = True

Created on Fri Mar 16 12:59:32 2018

@author: kendal30
"""

import arcpy, tqdm, os, sys

def compress_geodatabase_rasters(inputDB,optProgress = False,optPrint = False):
    #Set some environment settings
    arcpy.env.compression = 'LZ77'
    arcpy.env.pyramid = 'NONE'
    prevWorkspace = arcpy.env.workspace
    arcpy.env.workspace = inputDB

    #Get a list of rasters in the inputDB
    listRasters = arcpy.ListRasters()

    #Loop through the rasters, renaming, then copying to the original name, then deleting the old
    if optProgress:
        iterator = tqdm.tqdm(listRasters,desc='checking compression')
        
        # Function to parse 20-character names for consistent printing
        def name_20_char(inName):
            if len(inName)>20:
                outName = inName[0:13]+'...'+inName[-4:]
            else:
                outName = '{:<20}'.format(inName)

            return outName
    else:
        iterator = listRasters

    #Loop through the rasters, renaming, then copying to the original name, then deleting the old
    for raster in iterator:
        #Test to see if we need to do this one
        try:
            if optProgress:
                iterator.set_description('checking compression')

            describe = arcpy.Describe(raster)

            if describe.compressionType.lower()=='none':
                if optProgress:
                    iterator.set_description(name_20_char(raster))
                elif optPrint:
                    print(raster)
                result = arcpy.Rename_management(raster,raster+'_old')
                result = arcpy.CopyRaster_management(raster+'_old',raster)
                result = arcpy.Delete_management(raster+'_old')

        except:
            pass


    #Now, compact the geodatabase
    try:
        result = arcpy.Compact_management(inputDB)
    except:
        print('Unable to compact %s'%inputDB)
        result = False
    arcpy.env.workspace = prevWorkspace
    return result


def compress_file_raster(inputRaster,optPrint = False):
    #Test for folder-named rasters
    (inFolderRaster,inRasterExt) = os.path.splitext(inputRaster)
    if inRasterExt == '':
        (inRasterDir,inRasterFile) = os.path.split(inFolderRaster)
        inRasterName = inRasterFile
    else:
        (inRasterDir,inRasterFile) = os.path.split(inputRaster)
        (inRasterName,inRasterExt) = os.path.splitext(inRasterFile)

    #Set some environment settings
    if inRasterExt in ['.img','']:
        outRasterExt = '.tif'
        arcpy.env.compression = 'LZ77'
        reformat = True
    elif inRasterExt in ['.tif']:
        outRasterExt = inRasterExt
        arcpy.env.compression = 'LZW'
    else:
        return

    arcpy.env.pyramid = 'NONE'
    prevWorkspace = arcpy.env.workspace
    arcpy.env.workspace = inRasterDir

    #Rename the raster, then copying to the original name, then deleting the old
    #Test to see if we need to do this one
    try:
        describe = arcpy.Describe(inRasterFile)

        if describe.compressionType=='None' or reformat:
            if inRasterExt=='': #have to handle the short maximum name for these rasters
                if len(inRasterName) > 9:
                    oldName = 'old_' + inRasterName[:-4] #I would prefer the `old` as a suffix, but file rasters have a problem with this if they start with numbers
                else:
                    oldName = 'old_' + inRasterName
                outRasterFile = inRasterName+outRasterExt
            else:
                oldName = inRasterName+'_old'+inRasterExt
                outRasterFile = inRasterName+outRasterExt

            result = arcpy.Rename_management(inRasterFile,oldName)
            result = arcpy.CopyRaster_management(oldName,outRasterFile)
            result = arcpy.Delete_management(oldName)
        else:
            result = False

        if optPrint:
            print(inRasterFile)
    except:
        result = False

    arcpy.env.workspace = prevWorkspace
    return result


def recursive_workspace_gdb_list(thisDir):
    version = (float(sys.version_info[0])+float(sys.version_info[1]/10))
    if version >= 3.5:
        import glob

        listGDBs = glob.glob(thisDir+'/**/*.gdb',recursive=True)
    else:
        import fnmatch

        listGDBs = []
        for root, dirnames, filenames in os.walk(thisDir):
            for dirnames in fnmatch.filter(dirnames, '*.gdb'):
                listGDBs.append(os.path.join(root, dirnames))

    #Add thisDir if needed
    if os.path.splitext(thisDir)[1]=='.gdb':
        listGDBs.append(thisDir)

    #Validate that these are all file gdbs
    listGDBs = [thisGDB for thisGDB in listGDBs if os.path.exists(thisGDB+'/gdb')]
    return listGDBs



def recursive_workspace_raster_list(thisDir,listRasters=[],omitTypes = ['.jpg']):
    # Make sure that this isn't a geodatabase
    if os.path.exists(thisDir+'/gdb'):
        return []

    #Get the info from this directory
    prevWorkspace = arcpy.env.workspace
    arcpy.env.workspace = thisDir
    workspaces = arcpy.ListWorkspaces('*','Folder')
    rasters = arcpy.ListRasters()
    if rasters is not None:
        thisRasters = [os.path.join(thisDir,raster) for raster in rasters if not (os.path.splitext(raster)[1] in omitTypes)]
    else:
        thisRasters = []
    #Recursively go deeper into the directory structure
    if workspaces is not None:
        for workspace in workspaces:
            listRasters = recursive_workspace_raster_list(workspace,listRasters)

    #Now extend with the rasters in the current workspace
    listRasters.extend(thisRasters)

    arcpy.env.workspace = prevWorkspace
    return listRasters
