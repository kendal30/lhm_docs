#-*- coding: utf-8 -*-
"""
Created on Sun Mar 25 22:13:17 2018

This script installs or upgrades LHM on this local machine, creating a top-level LHM directory
specific to this machine, and installing a few required packged datasets
used by the model creation script (and others).

Specify an argument when calling this from the command line, like:
    python setup.py install

    or

    python setup.py upgrade

If running from an interactive session, you can use a variety of methods to specify this argument
in spyder, you can use:
    runfile(<path to script>/setup.py, args='install')


@author: kendal30
"""

import sys, os, shutil, sysconfig, subprocess, warnings
# also imports win32com.client

# Test to see if mamba is available, suppress output
test = subprocess.call('mamba --version', stdout=subprocess.DEVNULL)
if test == 0:
    # mamba is available, use it
    callConda = 'mamba'
else:
    # mamba is not available, use conda
    callConda = 'conda'

try:
    import yaml
except:
    # yaml is probably not installed yet
    subprocess.call('%s install pyyaml'%callConda)
    import yaml

# Determine if this is an "install" or an "upgrade" run
if len(sys.argv) > 1:
    if sys.argv[1].lower() == 'install':
        mode = 1
    elif sys.argv[1].lower() == 'upgrade':
        mode = 2
    else:
        raise ValueError('Argument to setup.py must be "install" or "upgrade"')
else:
    raise ValueError('setup.py must be called with "install" or "upgrade" arguments')

#------------------------------------------------------------------------------
# Inputs to this script
#------------------------------------------------------------------------------
nameConfigSetup = 'setup'
nameConfigNotebook = 'notebook'
nameInitFunctions = ['initialize_notebook.py','initialize_notebook.m']

verCheck = ('>=3.6') # a list or a tuple
relPathFrontend = 'frontend' # path relative to this file location, starting with the next lower directory name
relPathCode = 'lhm'
relPathTemplates = 'templates'
relPathNotebooks = 'notebooks_frontend'
notebookFrontend = 'LHM_Frontend.ipynb'

# Set some standard extensions
extTemplate = '.xyz'
extConfig = '.yml'
# extOld = '.old'
# extOldConfig = '.cfg'

# For shortcuts
callCmd = r'%windir%\System32\cmd.exe'
callLab = 'jupyter lab'
callNotebook = 'jupyter notebook'
callSpyder = 'spyder'

# Set the link name
linkNameLab = 'LHM Lab'
linkNameNotebook = 'LHM Notebook'
# linkNameSpyder = 'LHM Spyder'
linkNamePrompt = 'LHM Prompt'

# Options in the .cfg file
configSetupSection = 'setup'
configLHMPath = 'lhmHome'
configMATLABPath = 'matlabDir'
configSectionsNoPrompt = ['version'] # This list will be sections that are not prompted for user input
configSectionsOverride = ['version'] # This will override the user's setup.cfg

configFrontendSection = 'frontend'
configFrontendPath = 'pathFrontend'
configCodePath = 'pathCode'
configModelPath = 'pathModel'

configVersionSection = 'version'
configVersionFrontendPath = 'frontend'
configVersionLHMPath = 'lhm'

#------------------------------------------------------------------------------
# Helper functions
#------------------------------------------------------------------------------
def ensure_python(specs):
    """Given a list of range specifiers for python, ensure compatibility.
    Borrowed from setupbase, a Jupyter tool.
    """
    if not isinstance(specs, (list, tuple)):
        specs = [specs]
    v = sys.version_info
    part = '%s.%s' % (v.major, v.minor)
    numVer = float(sys.version_info[0])+float(sys.version_info[1]/10)
    for spec in specs:
        if part == spec:
            return numVer
        try:
            if eval(part + spec):
                return numVer
        except SyntaxError:
            pass
    raise ValueError('Python version %s unsupported' % part)

def get_upgrade():
    # Assure that the user has permissions
    if os.access(os.getcwd(), os.W_OK):
        upgradeEnv = input('Update environment and packages? (y/n): ').strip('"').strip("'")
        if upgradeEnv.lower() == 'y':
            upgradeEnv = True
        elif upgradeEnv.lower() == 'n':
            upgradeEnv = False
        else:
            print('Option not recognized')
            upgradeEnv = get_upgrade()
    else:
        upgradeEnv = False
    return upgradeEnv


def create_shortcut(pathLink, target, arguments = None, working = None, icon = None):
    # Delete the shortcut if present
    if os.path.exists(pathLink):
        os.remove(pathLink)

    # Make the shortcut
    shell = Dispatch('WScript.Shell')
    shortcut = shell.CreateShortCut(pathLink)
    shortcut.Targetpath = target
    if arguments is not None:
        shortcut.Arguments = arguments
    if working is not None:
        shortcut.WorkingDirectory = working
    if icon is not None:
        shortcut.IconLocation = icon

    shortcut.save()

#------------------------------------------------------------------------------
# Check for dependencies
#------------------------------------------------------------------------------
# Minimal Python version sanity check
ensure_python(verCheck)

#------------------------------------------------------------------------------
# Set up Environment, MATLAB and R
#------------------------------------------------------------------------------
# Determine if we are updating environment and packages or not
upgradeEnv = get_upgrade()

if upgradeEnv:
    # Update the environment
    subprocess.call('%s env update -f environment.yml'%callConda)

#------------------------------------------------------------------------------
# Read in the config file
#------------------------------------------------------------------------------
# Get the path to this script
pathSetup = os.path.split(os.path.realpath(__file__))[0]
pathFrontend = os.path.join(pathSetup,relPathFrontend)
pathCode = os.path.join(pathSetup,relPathCode)

# Specify the path for the setup config
fileConfigSetup = nameConfigSetup+extConfig

# # Check to see if we need to upgrade the setup.cfg to setup.yml
# if mode == 2:
#     oldFileConfigSetup = nameConfigSetup + extOldConfig
#     if os.path.exists(os.path.join(pathSetup,oldFileConfigSetup)):
#         # Set up the configparser as cp
#         import configparser as cp
#         config = cp.ConfigParser()

#         # Read in the old config file
#         config.read(os.path.join(pathSetup,oldFileConfigSetup))

#         # Make a function to lower the first letter of the section
#         funcLowerFirst = lambda x: x[:1].lower() + x[1:] if x else ''

#         # Create a new configDict from this
#         configDict = dict()
#         for section in config.sections():
#             thisSection = funcLowerFirst(section)
#             configDict[thisSection] = dict()
#             for item, val in config.items(section):
#                 print(item)
#                 configDict[thisSection][item] = val

#         # Rename a couple of fields
#         if 'lhmhome' in configDict['setup'].keys():
#             configDict['setup']['lhmHome'] = configDict['setup']['lhmhome']
#             configDict['setup'].pop('lhmhome')
#         if 'matlabdir' in configDict['setup'].keys():
#             configDict['setup']['matlabDir'] = configDict['setup']['matlabdir']
#             configDict['setup'].pop('matlabdir')

#         # Write the new file
#         yaml.dump(configDict,open(os.path.join(pathSetup,fileConfigSetup),'wt'),default_flow_style=False)

#         # Copy the old .cfg file to .old
#         os.rename(os.path.join(pathSetup,oldFileConfigSetup), os.path.join(pathSetup,oldFileConfigSetup+extOld))

# Test to see if we have a finished .yml, raise an warning and skip this section
if os.path.exists(os.path.join(pathSetup,fileConfigSetup)):
    testExistCfg = True
    if mode == 1:
        raise Exception('Config "%s" already present, run setup.py upgrade'%(fileConfigSetup))
else:
    testExistCfg = False
    if mode == 2:
        raise Exception('Config "%s" is missing, run setup.py install'%(fileConfigSetup))

# Make sure that we have a .xyz file, raise an error if not
fileConfigSetupTemplate = nameConfigSetup+extConfig+extTemplate
if not os.path.exists(os.path.join(pathSetup,fileConfigSetupTemplate)):
    raise Exception('Config "%s" is missing, pull Git repository'%(fileConfigSetupTemplate))

# Read in the .xyz config file
configTemplate = yaml.safe_load(open(os.path.join(pathSetup,fileConfigSetupTemplate)))

if mode == 2:
    # Read in their existing config file
    configExist = yaml.safe_load(open(os.path.join(pathSetup,fileConfigSetup)))

    # Make sure that the existing cfg file is complete in terms of sections
    # and items, carefully merge sections
    for section in configTemplate.keys():
        for item in configTemplate[section].keys():
            default = configTemplate[section][item]

            if section in configExist.keys():
                if item in configExist[section].keys():
                    if section in configSectionsOverride:
                        configExist[section][item] = default
                else:
                    configExist[section][item] = default
            else:
                configExist[section] = dict()
                configExist[section][item] = default

    # Now, use the merged configExist
    configDict = configExist
else:
	configDict = configTemplate

# Loop through the values of the config file and then prompt for user
# input, using the default values as given by the .xyz file

# Prompt for user input for each variable
print('Enter inputs for the %s file. Make sure to use double backlashes\n\
or forward-slashes for directories. Hitting enter accepts the value in square\n\
brackets'%(fileConfigSetup))

for section in configDict.keys():
    if section not in configSectionsNoPrompt:
        for item in configDict[section].keys():
            default = configDict[section][item]
            configDict[section][item] = (input('Enter the value for '+item+' ['+default+']: ').strip('"').strip("'") or default)


#------------------------------------------------------------------------------
# Create the lhmHome directory and populate it
#------------------------------------------------------------------------------
# Save these for later
pathLHM = configDict[configSetupSection][configLHMPath]
pathLHM = pathLHM.strip('"').strip("'") # remove the extra quotes

pathMATLAB = configDict[configSetupSection][configMATLABPath]+'/extern/engines/python'
pathMATLAB = pathMATLAB.strip('"').strip("'") # remove the extra quotes

# Make the directory if it doesn't exist
if mode == 1:
    if not os.path.exists(pathLHM):
        try:
            os.mkdir(pathLHM)
        except:
            pathLHM = input('Enter a valid path value for '+configLHMPath+', parent directory must exist ['+pathLHM+']: ').strip('"').strip("'") or default
            configDict[configSetupSection][configLHMPath] = pathLHM

# Write this configuation file, wait to write this to make sure our LHM path works
yaml.dump(configDict,open(os.path.join(pathSetup,fileConfigSetup),'wt'),default_flow_style=False)

# # Check to see if there is an old .cfg version that needs to be renamed
# fileConfigNotebookOld = nameConfigNotebook+extOldConfig
# if os.path.exists(os.path.join(pathLHM,fileConfigNotebookOld)):
#     os.rename(os.path.join(pathLHM,fileConfigNotebookOld), os.path.join(pathLHM,fileConfigNotebookOld+extOld))

# Create a notebook config file in that directory, pointing to the pathFrontend
notebookDict = {configFrontendSection:{}}
notebookDict[configFrontendSection][configFrontendPath] = pathFrontend
notebookDict[configFrontendSection][configCodePath] = pathCode
notebookDict[configFrontendSection][configModelPath] = pathLHM
notebookDict[configVersionSection] = configDict[configVersionSection]
fileConfigNotebook = nameConfigNotebook+extConfig
yaml.dump(notebookDict,open(os.path.join(pathLHM,fileConfigNotebook),'wt'),default_flow_style=False)

# Copy over the notebook_frontend templates to this folder
print('Copying LHM Frontend Notebooks')
pathTemplates = pathFrontend+'/'+relPathTemplates+'/'+relPathNotebooks
notebooksFrontend = [os.path.join(pathTemplates,notebookFrontend)]
[shutil.copy(os.path.join(pathTemplates,os.path.split(thisNotebook)[1]),os.path.join(pathLHM,os.path.split(thisNotebook)[1])) \
    for thisNotebook in notebooksFrontend]

# Copy the initialization function to that directory
for nameInitFunction in nameInitFunctions:
    shutil.copyfile(os.path.join(pathTemplates,nameInitFunction),os.path.join(pathLHM,nameInitFunction))

#------------------------------------------------------------------------------
# Finish setting up the MATLAB portion of the environment
#------------------------------------------------------------------------------
if upgradeEnv:
    # Check to see if we have execution access in the MATLAB path
    if os.access(pathMATLAB, os.W_OK):
        try:
            # Check for MATLAB
            currDir = os.getcwd()
            print(currDir)
            os.chdir(pathMATLAB)
            sys.argv = ['setup.py','install']
            exec(open(sys.argv[0]).read())
            os.chdir(currDir)

            # Install or update imatlab
            if mode==1:
                subprocess.call('python -m pip install jupyter-matlab-proxy')
            else:
                subprocess.call('python -m pip install jupyter-matlab-proxy -q') # run in quiet mode

        except:
            warnings.warn('Python MATLAB engine install/update failed, if needed check admin privileges')



baseDir = sysconfig.get_config_var('base')

# Make sure that this environment is properly listed
subprocess.call('conda config --add envs_dirs %s'%os.path.split(baseDir)[0])

# # Set a workaround for a geopandas issue
# subprocess.call('conda env config vars set SPATIALINDEX_C_LIBRARY="%s\Library\bin"'%os.path.split(baseDir)[0])

#------------------------------------------------------------------------------
# Configure the Jupyterlab launcher shortcut
#------------------------------------------------------------------------------
print('Installing Shortcuts in lhmhome')
# Special import statement down here to allow for environment update above
from win32com.client import Dispatch

# Get the environment name
envs = subprocess.check_output('conda env list',encoding='utf8').splitlines()
envActive = list(filter(lambda s: '*' in str(s), envs))[0]
envName = str(envActive).split('*')[1].strip()
print(envName)

# Get the script path
# pathScripts = sysconfig.get_path('scripts')
pathActivate = subprocess.check_output('where activate.bat').decode(sys.stdout.encoding).split('\r')[0]
# pathActivate = r'%s\activate.bat'%pathScripts

# Build the argument string
strArgument = '"/K" {0} {1} & {2} && exit'
argument = strArgument.format(pathActivate,envName,callLab)

# Name of link file
linkNameFull =  linkNameLab + '.lnk'
pathLink = os.path.join(pathLHM, linkNameFull)

# Target of icon
pathIcon = baseDir + '/Lib/site-packages/notebook/static/base/images/favicon.ico'

# Create the shortcut
create_shortcut(pathLink, callCmd, arguments = argument, working = pathLHM, icon = pathIcon)
# if upgradeEnv:
#    subprocess.call('jupyter labextension install @jupyterlab/toc')


#------------------------------------------------------------------------------
# Create the Jupyter notebook launcher shortcut
#------------------------------------------------------------------------------
# Argument of shortcut
argument = strArgument.format(pathActivate,envName,callNotebook)

# Name of link file
linkNameFull =  linkNameNotebook + '.lnk'
pathLink = os.path.join(pathLHM, linkNameFull)

# Target of icon
pathIcon = baseDir + '/Lib/site-packages/notebook/static/base/images/favicon.ico'

# Create the shortcut
create_shortcut(pathLink, callCmd, arguments = argument, working = pathLHM, icon = pathIcon)

# # Configure nbextensions
# subprocess.call('jupyter contrib nbextension install --user')
# subprocess.call('jupyter nbextension enable scratchpad/main')
# subprocess.call('jupyter nbextension enable toc2/main')
# subprocess.call('jupyter nbextension enable freeze/main')

# #------------------------------------------------------------------------------
# # Create the Spyder launcher shortcut
# #------------------------------------------------------------------------------
# # Target of shortcut
# strCommand = r'%s\pythonw.exe'%baseDir
# strArgument = r'%s\Scripts\spyder-script.py --new-instance'%baseDir

# # Name of link file
# linkNameFull =  linkNameSpyder + '.lnk'
# pathLink = os.path.join(pathLHM, linkNameFull)

# # spyderPython = 'pythonw.exe'
# # spyderScript = 'spyder-script.py'

# # Target of icon
# pathIcon = baseDir + '/Scripts/spyder.ico'

# pathCall = r'%windir%\system32\start.exe'

# # Create the shortcut
# create_shortcut(pathLink, strCommand, arguments = strArgument, working = pathLHM, icon = pathIcon)
# # create_shortcut(pathLink, target, arguments = arguments, working = pathLHM, icon = pathIcon)

#------------------------------------------------------------------------------
# Create the LHM Prompt launcher shortcut
#------------------------------------------------------------------------------
# Target of shortcut
strArgument = '"/K" {0} {1}'
argument = strArgument.format(pathActivate,envName)

# Name of link file
linkNameFull =  linkNamePrompt + '.lnk'
pathLink = os.path.join(pathLHM, linkNameFull)

# Target of icon
pathIcon = r'%windir%\system32\cmd.exe'

# Create the shortcut
create_shortcut(pathLink, callCmd, arguments = argument, working=os.getcwd(), icon=pathIcon)
