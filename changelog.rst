This changelog has two different versioning sections:

.. toctree::
  :maxdepth: 1

  changelog


Frontend Major Versions
=======================

0.1
____
The first version established when versioning started. Major changes for this
version include

- Created the LHM Notebook frontend, along with several notebooks that simplify
  model creation including... DOC TODO
- Established the sources.cfg file to replace the *Source Datasets* table
- Added routines ``setup.py`` and ``upgrade.py`` that do like they say
- Added compatibility with modern MATLAB versions
- Added a *Transactions* table to the geodatabase that it used to log all config
  and preparation actions, and implements an operation chain to help ensure
  input integrity

0.2
____
Major changes:

- Added Jupyter Notebook frontends for climate and landscape import controllers
- Changed installation routine to use ``environment.yml`` file
- Switched to using a specified conda environment for LHM
- Being more careful about compression, and switching to .tif format for disk rasters
- Handling new features for LHM in major version 0.6
- Added new shortcuts for Spyder and RStudio that work in the same environment as LHM Notebook

Detailed Changes:

- Modified ``setup.py`` and ``upgrade.py`` to handle tasks that had been manual previously
- Minor bugfixes to the ``output_data_import`` gui
- Added ``compression.py`` with a variety of utilities to help ensure disk space is kept low
- Modified ``gis.py`` and the function *snap_points_flowaccum_feature* to include a threshold flowaccum
value above which a point will not be snapped--to better preserve small features next to large ones
- Updates to ``lai_climate_change.m`` and ``lai_model_lu.m`` to work with the new ``landscape_import_controller_MODIS.ipynb``
- Updates to ``lu_load.m`` to bring in time-varying flowtimes (see LHM version 0.6 below)
- Similar updates to ``landscape_hdf_write.m``
- Minor update to ``MODIS_load.m`` to hande the new merged TERRA/AQUA 4-day, 500m LAI product
- Edits to ``intialize_buffers.m`` and ``intialize_structre.m`` to handle new time-varying flowtimes
- Modified the Prep folder structure to create separate folders for each LU-related type (i.e. flowtimes, landuse, etc.)
- Reorganization of ``nlcd_clip_reclass.py`` make sure that the new folder structure is used, and compression is enabled
- Added a new ``reclass_lhm_mannings.txt`` table that is used to map LHM LU types to mannings roughnesses
- Modifications to ``reclass_nlcd*.txt`` to account for new woody_wetland class (see LHM version 0.6 below)
- Small changes throughout to enable compression, and to switch to .tif format instead of .img
- Modified ``combine_gauges.py`` to enable to flowaccumulation threshold for snapping
- Modified ``rescale_grids.py`` to handle new subfolder structure for LU related grids in the Prep directory
- Similar changes to ``rescale_slice.py``
- Changes to ``runoff_model_prep.py`` to calculate flowtimes for each roughness grid
- Small changes to ``import_model_grid.py`` and ``import_submodel_grid`` to use .tif
- Updates to config parameters in ``lhm_config_v1.xls`` for new woody_wetland class
- Necessary changes to ``lhm_frontend.py``
- Small updates to many files to better handle the waitbar, changes needed for the Jupyter notebooks
- Small updates to anything in MATLAB requiring the *input* function so that it can work with Jupyter notebooks
- Separated out the *get_overwrite_pref* function from a bunch of different files, and made its use more consistent
and functions properly with Jupyter notebooks, made separate function ``get_overwrite_pref.m``
- Added a section to ``setup.py`` and ``upgrade.py`` that makes a proper shortcut to Spyder, and puts it in the
user's *lhmHome* folder
- Eliminated the separate ``upgrade.py`` function, replaced with *install* and *upgrade*
arguments to ``setup.py``
- Added rstudio to the installed packages to facilitate users working in R

0.3
____
Major Changes:
- Created an ``upgrade_model.ipynb`` tool
- Integrated the use of *nbdime* to difference and merge Jupyter Notebooks
- Created a ``prepare_data.ipynb`` tool
- Converted *OBS* and *STAT* file formats from .mat to .h5
- Created a ``prepare_data_LAI.ipynb`` tool

Detailed Changes:
- Created the function *merge_notebooks* within ``lhm_frontend.py`` to do the merging with *nbdime*
- Made ``lai_model_lu.m`` spatailly explicit for elevation, latitude, and longitude (essentially sun position),
but then had toaverage each of those inputs because computational effort was too high. Something to do in the future.
- Updated ``lai_model_lu.m`` to have a minimum threshold weight to use for LAI simulation,
will simply fail on that LU if this happens
- Added a new category of notebook templates *notebooks_postprocess* with a notebook to plot LAI model outputs
- Modified ``addpath_frontend.py`` to add the *preparation* and *import* recursively to sys.path
- Modified ```setup.py``` to only install MATLAB if execution privileges are present, and to prompt if the user wants
to update the environment or not
- Created a ```static_observations_import_controller.ipynb```
- New HDF5 functions ```h5varputcell.m``` and ```h5vargetcell.m``` for reading/writing
variable length string arrays
- New helper functions ```stat_obs_<read/write>_group.m``` that read and write groups of grids
to the OBS and STAT .H5 files
- Altering *preparation* scripts to be called as functions by notebooks
- Improved ```setup.py install/upgrade``` routine that allows a user to not update an environment
- Rewrote the GetModis_LAI_lhm.r script into a function, called by ``prepare_data_LAI.ipynb``
- Added ``addpath_frontend.R``
- Added the ``prepare_data_LAI.ipynb`` *Config* notebook
- Combined several previous MODIS-related LAI codes into a single Python module ``modis_tools.py``
- Converted each of the data preparation scripts into a callable function from ``prepare_data.ipynb``

0.4
____
Major Changes:
- Changed the ``.cfg`` format to ``.yml`` of ``sources.cfg``, ``notebook.cfg``,  and ``setup.cfg``
- Created the ``prepare_model.ipynb`` tool
- Modernized the groundwater model preparation scripts
- Added a ``Postprocess`` directory to the LHM model folder structure
- Added a depth-dependent hydraulic conductivity and porosity/specific yield calculation to the
MODFLOW prep capabilities
- Added submodel capabilities to the notebooks
- Revamped the mapping capabilities to use the *contextily* package

Detailed Changes:
- Modified the ``lhm_frontend.py`` module for new YAML format, including writing new ``write_sources_yml`` function
to replace the previous ``write_sources_config``
- Updated ``nwisweb.py`` for Python 3
- Added the ``prepare_model.ipynb`` *Config* notebook
- Updated all the groundwater preparation scripts
- Added parameters to the ``config_sources.ipynb`` notebook
- Removed the hard-coded references to datasets in the preparation scripts
- Renamed ``lai_model_lu`` to ``lai_model_calibrate`` to decrease confusion.
- Split out the templates from the ``Config`` folder into ``Postprocess`` as needed
- Modified ``modflow_aquifer_properties_prep`` to add depth-dependence, along with the necessary
parameters within the ``config_sources.ipynb`` notebook
- Added the ``submodel_config`` *Templates* directory
- Added ``create_submodel.ipynb``
- Modified the *create_model*, *merge_notebooks*, *copy_config_notebook*, *import_boundary*, and *map_boundary*, functions
 within ``lhm_frontend.py`` to create submodels if specified
- Consolidated a bunch of functions, refined the *model_grid* module and the rescale methods within it
- Cleaning up the print statements so that they are embedded within each prep function, to simplify the Frontend Notebooks
- Cleaned up the model notebook initialization cells by creating a separate function ``initialize_notebook.py``

0.5
____
Major Changes:
- Created an ``LHM_Frontend.ipynb`` that consolidates ``create_model.ipynb``, ``create_submodel.ipynb``,
and ``upgrade_model.ipynb``. This new notebook also serves as the central launch
point for all other notebooks in the model. This will help organize the model creation
process and later serve as a central point for interacting with the ``transactions.yml``
table that I hope to create for assuring model completeness and integrity.
- Eliminated the .xls files entirely, replaced with .yml
- Eliminated separate *specify* functions, instead rolling them into the new ``config_model.ipynb``.
- Added a small GUI utility to move output files from one computer to another

Detailed Changes:
- Added a bunch of new ``.ipynb`` files, all coherently named and organized.
- Small fixes to *merge_noteboooks*
- Collection of changes to the *initialization* routines to handle new .yml and elimination of the
*specify* functions.

0.6
___
Major Changes:
- Working to improve compatibility with Flopy, so that the eventual transition becomes smoother
- Standardizing array processing and creation capabilities in the Frontend
- Added GLDAS climate inputs
- Created new model/submodel specific frontends, and a simplified ``LHM_Frontend.ipynb`` using pulldown
menus and widgets

Detailed Changes:
- Created a shapefile of the 1st layer of the MODFLOW grid in the ``ibound_prep()`` function within ``modflow.py``
- Collapsed single-layer rasters into a multi-band rasters for each input datatype, in both groundwater and surface layers
- With this change, added ``collapse_single_layer_rasters()`` function to the GIS helper, and modified ``stat_obs_load_grids.m`` and
the ``import_STAT_OBS.ipynb`` accordingly--this actually made the functions much simpler because they could read in the multi-layer
rasters in one operation.
- Also for this change, I modified the ``model_grid.py`` module to have a collapse routine, which is now called by ``prepare_(sub)model.ipynb``
- Adding ``array.py`` a helper module to help standardize array processing
- Improved ``prep_wetlands.py`` to allow more flexible input of lake and wetland depths, using standard
array processing tools in the ``raster.py`` module
- Simplified and streamlined entering of dates of LU arrays in ``import_LAND.ipynb``, added checks to ``LU_load.m`` to make
sure that LU arrays are specified for the entire model period
- Generalized the ``reanalysis_extract_yearly_files.m`` to read in a data descriptor stored with the yearly reanalysis
data. This lets the same function operate on any similarly-structured archive
- Added ``GLDAS_load`` and generalized the ``NLDAS_load`` code so that it's more obvious how to change it in the future
for different reanalysis datasets
- Standardized input validation in the ``stat_obs_load_grids.m`` function, moved some here from ``initialize_params.m``, and added
the capability to specify the fill value (or use mean/median whether the datatype is float/integer)
- Added model/frontend versioning to both the frontend interface and to the individual models, preparing for more
automated updates in the future
- Created ``model_frontend.ipynb`` for models, and ``submodel_frontend.ipynb`` for submodels.
- Renumbered all config notebooks to correspond to the new model/submodel specific frontends
- Added new helper function module ``feature`` and moved some from the ``raster`` module, and ``gis``.
Some of these are really targeted at flopy model preparation.
- Uses single US-wide NHD dataset within ``nhd_merge``

0.65
----
Major Changes:
- Switched to new Google Earth Engine MODIS data retrieval for LAI, removed all
R-based components of the LHM Frontend
- Modernizing and modularizing the ``interpolate_water_levels`` routines
- Updated LAI model calibration

Detailed Changes:
- Stripped R-parts from ``setup.py``, removed ``GetModis_LAI_lhm.r``, removed ``prep_la
.ipynb``, removed ``addpath_frontend.R``
- Changed individual GLB-wells and Wellogic cleaning scripts into a single routine within
``interpolate_water_levels.py``
- Added the same module-level name definitions to that routine as I had created before in
``modflow.py``
- Improvements to ``prep_wetlands`` to handle larger models without error
- Creation of a standard Frontend name list, with the function ``names_frontend`` in the LHM frontend.
This is currently used by the ``prep_water_wells`` and ``modflow`` modules, but I will expand it as I
update functions going forward.
- Updated ``nhd_tools`` utility that processes out irrigation canals/drainage ditches, as well as
reservoirs.
- Added a new linear model tree structure for LAI annual maximum simulation, and improved
LAI model calibration in general.


LHM Major Versions
==================

0.5
____
The first version established when versioning started. Major changes for this
version include

- Improved the model naming scheme: DOC TODO
- Added compatibility with modern MATLAB versions

0.6
____
This version adds a couple of key new features:

- LU-varying roughness, and thus flowtimes. This necessitated reading in flowtimes from the *LAND* file
- Added a new woody_wetland class, splitting out wetlands into herbaceous and woody wetlands
- Added an eroded grain size fraction, which is determined at initialization but then could be used in postprocessing
- Added subsurface drip irrigation functionality
- Added point observation types, no temporal aggregation, but many points
- Added ability to flood irrigate and maintain a certain amount of water
- Updated and improved domain boundary mapping
- Generalized to allow for fewer than 24 sub-daily timesteps--still must be integer number of hours

Small changes include:
- Improved documentation for sedimentation pieces
- Fixed a small bug with changes in how MATLAB reads HDF5 files
- Switched to .H5 format for OBS and STAT files, from .MAT
- Modified ``output_controller.m`` to handle the new point observation type
- Fixed a bug that required observations to be written out for every timestep--if ever requested
- Created a new ``lhm_start.m`` generation routine in the initialization controller that allows
 easy restart
 - Debugging of the UEB snow model to allow for multi-hour timesteps
 - Improved stability in the UEB snow model for iterating surface interface temperature
