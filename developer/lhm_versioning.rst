================================
Version Guidelines
================================

LHM has separate versioning for its *Frontend* and *LHM* codebase sections. Eventually
these may be separated into separate repositories, so it makes sense to version
them separately now.

The version of the code is tracked in the ``setup.cfg.xyz`` file. This needs to be manually
updated with each version increment.

Versioning formally started at:

========= ============= ==============
Component Major Version Date Version
========= ============= ==============
frontend  0.1           20180403
LHM       0.5           20180403
========= ============= ==============

The versioning structure is
``component``.``major version``.``date version``

Major Versions
===================
With each major version update, it will be necesary for users to run the
``upgrade.py`` script in the LHM code directory. This will also necessitate an
entry into the `Changelog <..changelog.rst>`_.

In general, it is up to the developer to decide if the update that they are pushing
necessitates an increment to the Major Version, or rather just an increment to
the Date Version.

Date Versions
===============
These do not need to be documented, but rather should be incremented each time a
non-trivial update to the codebase is made.

The date version format is YYYYMMDD.
