================================
Prep Database Transaction Table
================================
There are two tables within the frontend preparation database: *Sources* and
*Transactions*. These two tables store information for the frontend scripts
and functions.

Sources
========
The sources table has one entry for each data source type in the model, and stores
the original location of the files prior to their use within the Frontend.
