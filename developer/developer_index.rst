=============================================
LHM Developers documentation
=============================================

This documentation contains sections describing:

1. General aspects of LHM code development
2. LHM Frontend-specific developers documentation
3. LHM Model-specific developers documentation
