.. include:: <isonum.txt>

========================================
Setup and Upgrade Scripts
========================================
These is only one script that users should need to run directly from the
LHM code directory. Instead, they will interact with Jupyter notebooks that
lead them through the model creation, model upgrade, data preparation, import,
and model configuration tasks.

Setup Script
=======================================
The ``setup.py`` script will prepare the ``setup.cfg``, ``notebook.cfg`` and Frontend Notebook files.
It also will place shortcuts to launch Jupyter Notebook, Spyder, and RStudio
from the ``lhmHome`` directory.

The setup script can be safely run at any time to repair the users' shortcut and notebook.cfg
files. It is designed to have one of two arguments passed to it, either *install*
or *upgrade*

The setup script has these steps:

1. Determine if we are in *install* or *ugprade* mode
2. Use the template ``setup.cfg.xyz`` and solicit user input on values, writing a
   ``setup.cfg`` file in the LHM code root directory.

   - If there is an existing ``setup.cfg`` file, it will upgrade that to the
     specification of the new ``setup.cfg.xyz`` carefully merging the two,
     defaulting to the user-specified value if present, and the default value in
     the .xyz file if not present

   - The version numbers in the customized ``setup.cfg`` file will be incremented
     to match the ``setup.cfg.xyz``.

3. Create the use-specified lhmHome directory (if needed), and write the ``notebook.cfg`` file as well as to copy
   the Frontend Notebooks to it.

4. Update the R packages needed, and make sure that the *matlabengine* package
   is installed, along with the *imatlab* package

5. Create shortcuts that will launch Jupyter notebook starting from the lhmHome home
   directory.
