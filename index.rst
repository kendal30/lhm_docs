===============================
Landscape Hydrology Model (LHM)
===============================

.. image:: graphics/simplified_LHM_conceptual_diagram.png

We are actively working to develop the documentation for LHM, in the meantime
check out papers related to it at `MSU Hydrogeology <http://hydrogeology.msu.edu>`_.

This work-in-progress documentation contains three separate sections:

.. toctree::
  :maxdepth: 2

  End-User Documentation <enduser/enduser_index.rst>
  Developer Documentation <developer/developer_index.rst>
  Tehcnical Documentation <technical/technical_index.rst>

But, if you'd like to just dive in, you can check out the `Getting Started <getting_started.rst>`_ page.

Check out the `Changelog <changelog.rst>`_ page.
